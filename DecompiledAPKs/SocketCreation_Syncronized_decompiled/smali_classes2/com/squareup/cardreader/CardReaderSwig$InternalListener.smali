.class Lcom/squareup/cardreader/CardReaderSwig$InternalListener;
.super Ljava/lang/Object;
.source "CardReaderSwig.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;
.implements Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;
.implements Lcom/squareup/cardreader/EventLogFeatureLegacy$EventLogFeatureListener;
.implements Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;
.implements Lcom/squareup/cardreader/PowerFeatureLegacy$PowerListener;
.implements Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;
.implements Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;
.implements Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;
.implements Lcom/squareup/cardreader/TamperFeatureLegacy$TamperListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderSwig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InternalListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/CardReaderSwig;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/CardReaderSwig;)V
    .locals 0

    .line 589
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hardwarePinRequested(Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 2

    .line 800
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$xlryUXTCvkoMxyKEb05nc1p62d0;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$xlryUXTCvkoMxyKEb05nc1p62d0;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public synthetic lambda$hardwarePinRequested$27$CardReaderSwig$InternalListener(Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 1

    .line 801
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onHardwarePinRequested(Lcom/squareup/securetouch/SecureTouchPinRequestData;)V

    return-void
.end method

.method public synthetic lambda$onCapabilitiesReceived$23$CardReaderSwig$InternalListener(ZLjava/util/List;)V
    .locals 1

    .line 778
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onCapabilitiesReceived(ZLjava/util/List;)V

    return-void
.end method

.method public synthetic lambda$onChargeCycleCountReceived$22$CardReaderSwig$InternalListener(I)V
    .locals 1

    .line 771
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onChargeCycleCountReceived(I)V

    return-void
.end method

.method public synthetic lambda$onCommsRateUpdated$1$CardReaderSwig$InternalListener(Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;)V
    .locals 1

    .line 601
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onCommsRateUpdated(Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;)V

    return-void
.end method

.method public synthetic lambda$onCommsVersionAcquired$0$CardReaderSwig$InternalListener(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
    .locals 1

    .line 596
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onCommsVersionAcquired(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V

    return-void
.end method

.method public synthetic lambda$onCoreDumpExists$5$CardReaderSwig$InternalListener(Z)V
    .locals 1

    .line 627
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onCoreDumpExists(Z)V

    return-void
.end method

.method public synthetic lambda$onCoreDumpProgress$6$CardReaderSwig$InternalListener(III)V
    .locals 1

    .line 634
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onCoreDumpProgress(III)V

    return-void
.end method

.method public synthetic lambda$onCoreDumpReceived$7$CardReaderSwig$InternalListener([B[B)V
    .locals 1

    .line 640
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onCoreDumpReceived([B[B)V

    return-void
.end method

.method public synthetic lambda$onCoreDumpTriggered$4$CardReaderSwig$InternalListener(Z)V
    .locals 1

    .line 622
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onCoreDumpTriggered(Z)V

    return-void
.end method

.method public synthetic lambda$onEventLogReceived$25$CardReaderSwig$InternalListener(Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;)V
    .locals 1

    .line 788
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->logEvent(Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;)V

    return-void
.end method

.method public synthetic lambda$onFirmwareUpdateError$17$CardReaderSwig$InternalListener(Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V
    .locals 1

    .line 741
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$1100(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;->onFirmwareUpdateError(Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V

    return-void
.end method

.method public synthetic lambda$onFirmwareUpdateProgress$16$CardReaderSwig$InternalListener(I)V
    .locals 1

    .line 730
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$1100(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;->onFirmwareUpdateProgress(I)V

    return-void
.end method

.method public synthetic lambda$onFirmwareVersionReceived$20$CardReaderSwig$InternalListener(Ljava/lang/String;)V
    .locals 1

    .line 759
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onFirmwareVersionReceived(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$onHardwareSerialNumberReceived$21$CardReaderSwig$InternalListener(Ljava/lang/String;)V
    .locals 1

    .line 765
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onHardwareSerialNumberReceived(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$onManifestReceived$15$CardReaderSwig$InternalListener([BZLcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
    .locals 1

    .line 724
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$1100(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;->onManifestReceived([BZLcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V

    return-void
.end method

.method public synthetic lambda$onPinRequested$26$CardReaderSwig$InternalListener(Lcom/squareup/cardreader/PinRequestData;)V
    .locals 1

    .line 794
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onSoftwarePinRequested(Lcom/squareup/cardreader/PinRequestData;)V

    return-void
.end method

.method public synthetic lambda$onPowerReceived$3$CardReaderSwig$InternalListener(Lcom/squareup/cardreader/CardReaderBatteryInfo;)V
    .locals 1

    .line 616
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onPowerStatus(Lcom/squareup/cardreader/CardReaderBatteryInfo;)V

    return-void
.end method

.method public synthetic lambda$onReaderError$24$CardReaderSwig$InternalListener(Lcom/squareup/cardreader/lcr/CrsReaderError;)V
    .locals 1

    .line 783
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onReaderError(Lcom/squareup/cardreader/lcr/CrsReaderError;)V

    return-void
.end method

.method public synthetic lambda$onReaderReady$2$CardReaderSwig$InternalListener(Lcom/squareup/cardreader/lcr/CrCardreaderType;)V
    .locals 1

    .line 606
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onReaderReady(Lcom/squareup/cardreader/lcr/CrCardreaderType;)V

    return-void
.end method

.method public synthetic lambda$onSecureSessionDenied$10$CardReaderSwig$InternalListener(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 1

    .line 671
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onSecureSessionDenied(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V

    return-void
.end method

.method public synthetic lambda$onSecureSessionError$12$CardReaderSwig$InternalListener(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
    .locals 1

    .line 707
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onSecureSessionError(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V

    return-void
.end method

.method public synthetic lambda$onSecureSessionEvent$9$CardReaderSwig$InternalListener(Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;)V
    .locals 1

    .line 660
    sget-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->CR_SECURESESSION_FEATURE_EVENT_TYPE_SESSION_VALID:Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    if-ne p1, v0, :cond_0

    .line 661
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onSecureSessionValid()V

    goto :goto_0

    .line 663
    :cond_0
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onSecureSessionInvalid()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onSecureSessionRevoked$11$CardReaderSwig$InternalListener(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 0

    .line 682
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->onSecureSessionRevokedWorkload(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V

    return-void
.end method

.method public synthetic lambda$onSecureSessionSendToServer$8$CardReaderSwig$InternalListener([B)V
    .locals 1

    .line 652
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onSecureSessionSendToServer([B)V

    return-void
.end method

.method public synthetic lambda$onTamperDataReceived$14$CardReaderSwig$InternalListener([B)V
    .locals 1

    .line 717
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onTamperData([B)V

    return-void
.end method

.method public synthetic lambda$onTamperStatusReceived$13$CardReaderSwig$InternalListener(Lcom/squareup/cardreader/lcr/CrTamperStatus;)V
    .locals 1

    .line 712
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onTamperStatus(Lcom/squareup/cardreader/lcr/CrTamperStatus;)V

    return-void
.end method

.method public synthetic lambda$onTmsCountryCode$19$CardReaderSwig$InternalListener(Ljava/lang/String;)V
    .locals 1

    .line 753
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onTmsCountryCode(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$onVersionInfo$18$CardReaderSwig$InternalListener([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)V
    .locals 1

    .line 747
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onVersionInfo([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)V

    return-void
.end method

.method public synthetic lambda$secureTouchFeatureEvent$28$CardReaderSwig$InternalListener(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V
    .locals 1

    .line 808
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onSecureTouchPinPadEvent(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V

    return-void
.end method

.method public onAudioConnectionTimeout()V
    .locals 3

    .line 611
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$qAJN-HFXEIneh0NhnEHGeB6Bq1U;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$qAJN-HFXEIneh0NhnEHGeB6Bq1U;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onCapabilitiesReceived(ZLjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrsCapability;",
            ">;)V"
        }
    .end annotation

    .line 777
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$Rb1dJIZmbqCHtbXW28lFw04J5x8;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$Rb1dJIZmbqCHtbXW28lFw04J5x8;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;ZLjava/util/List;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onChargeCycleCountReceived(I)V
    .locals 2

    .line 770
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$5ZcsBTeYZrTjMgOtzDSKjhegaw8;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$5ZcsBTeYZrTjMgOtzDSKjhegaw8;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;I)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onCommsRateUpdated(Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;)V
    .locals 2

    .line 601
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$g6QpPCg-_EVzUiepQM8tCTA1Jqk;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$g6QpPCg-_EVzUiepQM8tCTA1Jqk;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onCommsVersionAcquired(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
    .locals 2

    .line 595
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$RmPHH1B6UY2Wy0ApooBTeoETl60;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$RmPHH1B6UY2Wy0ApooBTeoETl60;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Lcom/squareup/cardreader/lcr/CrCommsVersionResult;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onCoreDumpErased()V
    .locals 3

    .line 645
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$nZXwoJzYUrQf5cHTNnnVzrnlbns;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$nZXwoJzYUrQf5cHTNnnVzrnlbns;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onCoreDumpExists(Z)V
    .locals 2

    .line 627
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$WDL1TzVbX3hASN-FAY_go0TyjbA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$WDL1TzVbX3hASN-FAY_go0TyjbA;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Z)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onCoreDumpProgress(III)V
    .locals 2

    .line 633
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$R-fIYvq0ENsmHo1q1pXzjmkeITs;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$R-fIYvq0ENsmHo1q1pXzjmkeITs;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;III)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onCoreDumpReceived([B[B)V
    .locals 2

    .line 639
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$1eRbZ8p7FTli8kqLnF6GrD-zF5I;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$1eRbZ8p7FTli8kqLnF6GrD-zF5I;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;[B[B)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onCoreDumpTriggered(Z)V
    .locals 2

    .line 621
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$-pHIs1WsMRTSy9d3wl1Lxo0wxFw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$-pHIs1WsMRTSy9d3wl1Lxo0wxFw;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Z)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onEventLogReceived(Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;)V
    .locals 2

    .line 788
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$DHUkzsUE24jXSNAqFdOuFMcnJx8;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$DHUkzsUE24jXSNAqFdOuFMcnJx8;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onFirmwareUpdateError(Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V
    .locals 2

    .line 740
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$WIApJ7OOBI3S6flhsy7qqZMrFTM;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$WIApJ7OOBI3S6flhsy7qqZMrFTM;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onFirmwareUpdateProgress(I)V
    .locals 2

    .line 729
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$zM0V3FEETucfLl1iXg0MZWPM5MY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$zM0V3FEETucfLl1iXg0MZWPM5MY;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;I)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onFirmwareUpdateSuccess()V
    .locals 3

    .line 735
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$1100(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$LknfOywoH-wLs13JQ9J3kXenLNQ;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$LknfOywoH-wLs13JQ9J3kXenLNQ;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onFirmwareVersionReceived(Ljava/lang/String;)V
    .locals 2

    .line 758
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$6S10k7XpyD5fISjcEFq4PKWLWWM;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$6S10k7XpyD5fISjcEFq4PKWLWWM;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Ljava/lang/String;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onHardwareSerialNumberReceived(Ljava/lang/String;)V
    .locals 2

    .line 764
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$DmMibYnaC_UQh9sTcFOhTsaas0w;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$DmMibYnaC_UQh9sTcFOhTsaas0w;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Ljava/lang/String;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onManifestReceived([BZLcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
    .locals 2

    .line 723
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$CK0LKCx19uGpNqE87LxFeB0La6Q;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$CK0LKCx19uGpNqE87LxFeB0La6Q;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;[BZLcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onPinRequested(Lcom/squareup/cardreader/PinRequestData;)V
    .locals 2

    .line 793
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$fQg4mSuDXzTkkRHL9aOrMEMv7YE;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$fQg4mSuDXzTkkRHL9aOrMEMv7YE;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Lcom/squareup/cardreader/PinRequestData;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onPowerReceived(Lcom/squareup/cardreader/CardReaderBatteryInfo;)V
    .locals 2

    .line 616
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$3QKLJagguNeiec4Ads2ZMdAVLFc;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$3QKLJagguNeiec4Ads2ZMdAVLFc;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Lcom/squareup/cardreader/CardReaderBatteryInfo;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onReaderError(Lcom/squareup/cardreader/lcr/CrsReaderError;)V
    .locals 2

    .line 783
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$UMJ-KoksuLj6ptTp1pAnSzsqx4s;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$UMJ-KoksuLj6ptTp1pAnSzsqx4s;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Lcom/squareup/cardreader/lcr/CrsReaderError;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onReaderReady(Lcom/squareup/cardreader/lcr/CrCardreaderType;)V
    .locals 2

    .line 606
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$Qju3zJ_TpBW1WyZ_NxfeM9Tz5-M;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$Qju3zJ_TpBW1WyZ_NxfeM9Tz5-M;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Lcom/squareup/cardreader/lcr/CrCardreaderType;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onSecureSessionDenied(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 2

    .line 670
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$KC18P8vH-HhxREkAiVyu0WZ4oKk;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$KC18P8vH-HhxREkAiVyu0WZ4oKk;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onSecureSessionError(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
    .locals 2

    .line 706
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$BkoC1PwzAL5c1sXKYh2CFCNyfLI;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$BkoC1PwzAL5c1sXKYh2CFCNyfLI;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onSecureSessionEvent(Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;)V
    .locals 1

    .line 659
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object p1

    new-instance v0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$CY7j65o9zhOOrpdtjlG6BQmG0O4;

    invoke-direct {v0, p0, p2}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$CY7j65o9zhOOrpdtjlG6BQmG0O4;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;)V

    sget-object p2, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onSecureSessionRevoked(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 2

    .line 681
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$tuWh9LAe_puy6V2593hasspJWeY;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$tuWh9LAe_puy6V2593hasspJWeY;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method onSecureSessionRevokedWorkload(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 2

    .line 691
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onSecureSessionDenied(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V

    .line 694
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$800(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    .line 695
    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$700(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2, p3}, Lcom/squareup/cardreader/ReaderEventLogger;->logSecureSessionRevoked(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V

    .line 700
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderSwig;->access$700(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReader;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->forget()V

    .line 701
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderSwig;->access$900(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderFactory;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {p2}, Lcom/squareup/cardreader/CardReaderSwig;->access$700(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReader;

    move-result-object p2

    invoke-interface {p2}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/cardreader/CardReaderFactory;->destroy(Lcom/squareup/cardreader/CardReaderId;)V

    .line 702
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderSwig;->access$1000(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/BluetoothUtils;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {p2}, Lcom/squareup/cardreader/CardReaderSwig;->access$700(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReader;

    move-result-object p2

    invoke-interface {p2}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/cardreader/BluetoothUtils;->unpairDevice(Ljava/lang/String;)V

    return-void
.end method

.method public onSecureSessionSendToServer(Lcom/squareup/cardreader/lcr/CrSecureSessionMessageType;[B)V
    .locals 1

    .line 651
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object p1

    new-instance v0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$KH2V8bf4CursR8bmUrpsjj0Z9SA;

    invoke-direct {v0, p0, p2}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$KH2V8bf4CursR8bmUrpsjj0Z9SA;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;[B)V

    sget-object p2, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onTamperDataReceived([B)V
    .locals 2

    .line 717
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$LNz0NHE_UBuu6-7cDSnAeda04KQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$LNz0NHE_UBuu6-7cDSnAeda04KQ;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;[B)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onTamperStatusReceived(Lcom/squareup/cardreader/lcr/CrTamperStatus;)V
    .locals 2

    .line 712
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$WFYO7P-ST7STIktkC2ty4Epxo7A;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$WFYO7P-ST7STIktkC2ty4Epxo7A;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Lcom/squareup/cardreader/lcr/CrTamperStatus;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onTmsCountryCode(Ljava/lang/String;)V
    .locals 2

    .line 752
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$x5p9ItqDqSpIY11KviprgfNnO3U;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$x5p9ItqDqSpIY11KviprgfNnO3U;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Ljava/lang/String;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onVersionInfo([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)V
    .locals 2

    .line 746
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$h8MSnKdr77DINMkMKM-XZ_rJKnA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$h8MSnKdr77DINMkMKM-XZ_rJKnA;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;[Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public secureTouchDisabled()V
    .locals 3

    .line 818
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$URBrE1TkJinnreS6VNUkExyawu8;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$URBrE1TkJinnreS6VNUkExyawu8;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public secureTouchEnabled()V
    .locals 3

    .line 813
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$H5XCdk5yHsn-Y73FJztXORg8UqE;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$H5XCdk5yHsn-Y73FJztXORg8UqE;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public secureTouchFeatureEvent(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V
    .locals 2

    .line 807
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$6muPe9UDR5CivjmmIrYjWWncQIg;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalListener$6muPe9UDR5CivjmmIrYjWWncQIg;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method
