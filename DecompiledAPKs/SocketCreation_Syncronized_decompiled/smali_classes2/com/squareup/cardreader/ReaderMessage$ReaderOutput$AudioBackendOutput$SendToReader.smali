.class public final Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;
.super Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SendToReader"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0017\n\u0002\u0008\n\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u00032\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput;",
        "loop",
        "",
        "samples",
        "",
        "(Z[S)V",
        "getLoop",
        "()Z",
        "getSamples",
        "()[S",
        "component1",
        "component2",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final loop:Z

.field private final samples:[S


# direct methods
.method public constructor <init>(Z[S)V
    .locals 1

    const-string v0, "samples"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 246
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->loop:Z

    iput-object p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->samples:[S

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;Z[SILjava/lang/Object;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-boolean p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->loop:Z

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->samples:[S

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->copy(Z[S)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->loop:Z

    return v0
.end method

.method public final component2()[S
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->samples:[S

    return-object v0
.end method

.method public final copy(Z[S)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;
    .locals 1

    const-string v0, "samples"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;

    invoke-direct {v0, p1, p2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;-><init>(Z[S)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;

    iget-boolean v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->loop:Z

    iget-boolean v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->loop:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->samples:[S

    iget-object p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->samples:[S

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getLoop()Z
    .locals 1

    .line 244
    iget-boolean v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->loop:Z

    return v0
.end method

.method public final getSamples()[S
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->samples:[S

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-boolean v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->loop:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->samples:[S

    if-eqz v1, :cond_1

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([S)I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SendToReader(loop="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->loop:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", samples="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;->samples:[S

    invoke-static {v1}, Ljava/util/Arrays;->toString([S)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
