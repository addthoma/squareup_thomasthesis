.class public interface abstract Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;
.super Ljava/lang/Object;
.source "CardReaderHub.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderHub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CardReaderAttachListener"
.end annotation


# virtual methods
.method public abstract onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
.end method

.method public abstract onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
.end method
