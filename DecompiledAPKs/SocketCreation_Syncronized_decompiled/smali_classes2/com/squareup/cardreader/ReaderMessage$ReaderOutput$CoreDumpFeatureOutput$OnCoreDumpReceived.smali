.class public final Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;
.super Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnCoreDumpReceived"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\n\u001a\u00020\u0003H\u00c6\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput;",
        "keyBytes",
        "",
        "dataBytes",
        "([B[B)V",
        "getDataBytes",
        "()[B",
        "getKeyBytes",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dataBytes:[B

.field private final keyBytes:[B


# direct methods
.method public constructor <init>([B[B)V
    .locals 1

    const-string v0, "keyBytes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dataBytes"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 288
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->keyBytes:[B

    iput-object p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->dataBytes:[B

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;[B[BILjava/lang/Object;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->keyBytes:[B

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->dataBytes:[B

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->copy([B[B)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()[B
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->keyBytes:[B

    return-object v0
.end method

.method public final component2()[B
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->dataBytes:[B

    return-object v0
.end method

.method public final copy([B[B)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;
    .locals 1

    const-string v0, "keyBytes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dataBytes"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;

    invoke-direct {v0, p1, p2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;-><init>([B[B)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->keyBytes:[B

    iget-object v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->keyBytes:[B

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->dataBytes:[B

    iget-object p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->dataBytes:[B

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDataBytes()[B
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->dataBytes:[B

    return-object v0
.end method

.method public final getKeyBytes()[B
    .locals 1

    .line 286
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->keyBytes:[B

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->keyBytes:[B

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->dataBytes:[B

    if-eqz v2, :cond_1

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnCoreDumpReceived(keyBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->keyBytes:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", dataBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpReceived;->dataBytes:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
