.class public Lcom/squareup/cardreader/CardReaderHubUtils;
.super Ljava/lang/Object;
.source "CardReaderHubUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;
    }
.end annotation


# static fields
.field public static READER_TYPE_IS_IGNORED:Z = true

.field public static READER_TYPE_IS_SUPPORTED:Z


# instance fields
.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/CardReaderHub;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderHubUtils;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    return-void
.end method

.method private static asCommonReaderGlyph()Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable$Transformer<",
            "Ljava/util/Collection<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
            ">;"
        }
    .end annotation

    .line 234
    sget-object v0, Lcom/squareup/cardreader/-$$Lambda$CardReaderHubUtils$dF98K_-6IfELdJq2rdZxirhL9G8;->INSTANCE:Lcom/squareup/cardreader/-$$Lambda$CardReaderHubUtils$dF98K_-6IfELdJq2rdZxirhL9G8;

    return-object v0
.end method

.method public static connectedReaderCapabilities(Ljava/util/Collection;Lrx/functions/Func1;)Ljava/util/EnumSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Lrx/functions/Func1<",
            "Lcom/squareup/protos/client/bills/CardData$ReaderType;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;"
        }
    .end annotation

    .line 177
    const-class v0, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    .line 178
    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 180
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderInfo;

    if-nez v1, :cond_1

    goto :goto_0

    .line 184
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v2

    invoke-interface {p1, v2}, Lrx/functions/Func1;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 188
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->supportsSwipes()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 189
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->hasValidTmsCountryCode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 190
    sget-object v2, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_SWIPE:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 192
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->supportsDips()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 193
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->hasSecureSession()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 194
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->hasValidTmsCountryCode()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 195
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateBlocking()Z

    move-result v2

    if-nez v2, :cond_4

    .line 196
    sget-object v2, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_DIP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 198
    :cond_4
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTaps()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 199
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->hasSecureSession()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 200
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->hasValidTmsCountryCode()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 201
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateBlocking()Z

    move-result v2

    if-nez v2, :cond_5

    .line 202
    sget-object v2, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_TAP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 205
    :cond_5
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->supportsFelicaCards()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 206
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->hasSecureSession()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 207
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->hasValidTmsCountryCode()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 208
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateBlocking()Z

    move-result v2

    if-nez v2, :cond_6

    .line 209
    sget-object v2, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_FELICA:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 212
    :cond_6
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTaps()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    sget-object v1, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_TAP_AND_IS_IN_PAYMENT:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_7
    return-object v0
.end method

.method private getCardReaderInfos()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation

    .line 221
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 222
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderHubUtils;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReader;

    .line 223
    invoke-interface {v2}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private static glyphForDippableReaderType(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 281
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne p0, v0, :cond_0

    .line 282
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R6_READER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 286
    :cond_0
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R4_READER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method private static glyphForTappableReaderType(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 268
    sget-object v0, Lcom/squareup/cardreader/CardReaderHubUtils$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 276
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 270
    :pswitch_0
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 272
    :pswitch_1
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 274
    :pswitch_2
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R12:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic lambda$asCommonReaderGlyph$5(Lrx/Observable;)Lrx/Observable;
    .locals 1

    .line 234
    sget-object v0, Lcom/squareup/cardreader/-$$Lambda$CardReaderHubUtils$4hyw4gA3wvhDxYmEDBnl0uyZ9VU;->INSTANCE:Lcom/squareup/cardreader/-$$Lambda$CardReaderHubUtils$4hyw4gA3wvhDxYmEDBnl0uyZ9VU;

    invoke-virtual {p0, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    .line 264
    invoke-virtual {p0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$hasPaymentStartedOnContactlessReader$3(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/Boolean;
    .locals 0

    .line 85
    sget-boolean p0, Lcom/squareup/cardreader/CardReaderHubUtils;->READER_TYPE_IS_SUPPORTED:Z

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$isDipSupportedOnConnectedReader$1(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/Boolean;
    .locals 0

    .line 73
    sget-boolean p0, Lcom/squareup/cardreader/CardReaderHubUtils;->READER_TYPE_IS_SUPPORTED:Z

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$isPaymentReadyFelicaReaderConnected$2(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/Boolean;
    .locals 0

    .line 79
    sget-boolean p0, Lcom/squareup/cardreader/CardReaderHubUtils;->READER_TYPE_IS_SUPPORTED:Z

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$isPaymentReadySmartReaderConnected$0(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/Boolean;
    .locals 0

    .line 67
    sget-boolean p0, Lcom/squareup/cardreader/CardReaderHubUtils;->READER_TYPE_IS_SUPPORTED:Z

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$4(Ljava/util/Collection;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 4

    .line 235
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 236
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 237
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReaderInfo;

    .line 238
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTaps()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 239
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 240
    :cond_1
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->supportsDips()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 241
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 246
    :cond_2
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result p0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez p0, :cond_4

    .line 247
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result p0

    if-ne p0, v3, :cond_3

    .line 249
    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object p0

    aget-object p0, p0, v2

    check-cast p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-static {p0}, Lcom/squareup/cardreader/CardReaderHubUtils;->glyphForTappableReaderType(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p0

    goto :goto_1

    :cond_3
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_1
    return-object p0

    .line 255
    :cond_4
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result p0

    if-ne p0, v3, :cond_5

    .line 257
    invoke-interface {v1}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object p0

    aget-object p0, p0, v2

    check-cast p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-static {p0}, Lcom/squareup/cardreader/CardReaderHubUtils;->glyphForDippableReaderType(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p0

    return-object p0

    .line 262
    :cond_5
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R4_READER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method


# virtual methods
.method public canStartNfcMonitoringInOrderTicket()Z
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHubUtils;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    .line 146
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->isSquareTerminal()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public canSwipeChipCardsWithConnectedReaders()Z
    .locals 5

    .line 108
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHubUtils;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    :pswitch_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/CardReader;

    .line 109
    sget-object v4, Lcom/squareup/cardreader/CardReaderHubUtils$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-interface {v3}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    .line 133
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown reader type, not sure if we can swipe chip cards"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const/4 v2, 0x1

    goto :goto_0

    :pswitch_2
    return v1

    :cond_0
    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public glyphForAttachedReaders()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
            ">;"
        }
    .end annotation

    .line 171
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHubUtils;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->cardReaderInfos()Lrx/Observable;

    move-result-object v0

    invoke-static {}, Lcom/squareup/cardreader/CardReaderHubUtils;->asCommonReaderGlyph()Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public hasPaymentStartedOnContactlessReader()Z
    .locals 2

    .line 84
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderHubUtils;->getCardReaderInfos()Ljava/util/Collection;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderHubUtils$MmMvedLAMYPftrTB6Iq9ZalTLfo;->INSTANCE:Lcom/squareup/cardreader/-$$Lambda$CardReaderHubUtils$MmMvedLAMYPftrTB6Iq9ZalTLfo;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/CardReaderHubUtils;->connectedReaderCapabilities(Ljava/util/Collection;Lrx/functions/Func1;)Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_TAP_AND_IS_IN_PAYMENT:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    .line 86
    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isCardInsertedOnAnyContactlessReader()Z
    .locals 3

    .line 93
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHubUtils;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    if-eqz v1, :cond_0

    .line 95
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTaps()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 96
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public isDipSupportedOnConnectedReader()Z
    .locals 2

    .line 73
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderHubUtils;->getCardReaderInfos()Ljava/util/Collection;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderHubUtils$Vn9t5vR6KDjEXuOWAUydgf8E6e0;->INSTANCE:Lcom/squareup/cardreader/-$$Lambda$CardReaderHubUtils$Vn9t5vR6KDjEXuOWAUydgf8E6e0;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/CardReaderHubUtils;->connectedReaderCapabilities(Ljava/util/Collection;Lrx/functions/Func1;)Ljava/util/EnumSet;

    move-result-object v0

    .line 74
    sget-object v1, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_DIP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isPaymentReadyFelicaReaderConnected()Z
    .locals 2

    .line 79
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderHubUtils;->getCardReaderInfos()Ljava/util/Collection;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderHubUtils$kLbAKPRH0p3l9QEn_dd9FQ0uMaA;->INSTANCE:Lcom/squareup/cardreader/-$$Lambda$CardReaderHubUtils$kLbAKPRH0p3l9QEn_dd9FQ0uMaA;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/CardReaderHubUtils;->connectedReaderCapabilities(Ljava/util/Collection;Lrx/functions/Func1;)Ljava/util/EnumSet;

    move-result-object v0

    .line 80
    sget-object v1, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_FELICA:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isPaymentReadySmartReaderConnected()Z
    .locals 2

    .line 67
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderHubUtils;->getCardReaderInfos()Ljava/util/Collection;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderHubUtils$njL6FJPVPeQRucPaeEtrdUY3N-U;->INSTANCE:Lcom/squareup/cardreader/-$$Lambda$CardReaderHubUtils$njL6FJPVPeQRucPaeEtrdUY3N-U;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/CardReaderHubUtils;->connectedReaderCapabilities(Ljava/util/Collection;Lrx/functions/Func1;)Ljava/util/EnumSet;

    move-result-object v0

    .line 68
    sget-object v1, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_DIP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_TAP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public maybeGetAudioReader()Lcom/squareup/cardreader/CardReaderInfo;
    .locals 3

    .line 52
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHubUtils;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    .line 53
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    .line 54
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->isAudio()Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method
