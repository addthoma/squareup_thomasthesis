.class public interface abstract Lcom/squareup/cardreader/CoreDumpFeature;
.super Ljava/lang/Object;
.source "CoreDumpFeature.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\u0012\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J \u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\tH&J\u0018\u0010\u000c\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000eH&J\u0010\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u0006H&\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/cardreader/CoreDumpFeature;",
        "",
        "onCoreDumpErased",
        "",
        "onCoreDumpInfo",
        "isCoredump",
        "",
        "onCoreDumpProgress",
        "bytesReceived",
        "",
        "bytesSoFar",
        "bytesTotal",
        "onCoreDumpReceived",
        "keyBytes",
        "",
        "dataBytes",
        "onCoreDumpTriggered",
        "wasTriggered",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract onCoreDumpErased()V
.end method

.method public abstract onCoreDumpInfo(Z)V
.end method

.method public abstract onCoreDumpProgress(III)V
.end method

.method public abstract onCoreDumpReceived([B[B)V
.end method

.method public abstract onCoreDumpTriggered(Z)V
.end method
