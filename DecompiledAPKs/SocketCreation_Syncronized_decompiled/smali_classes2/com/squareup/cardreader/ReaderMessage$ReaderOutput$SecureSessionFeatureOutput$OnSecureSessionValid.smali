.class public final Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;
.super Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnSecureSessionValid"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\'\u0010\u000e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput;",
        "sessionId",
        "",
        "readerTransactionCount",
        "readerUtcEpochTime",
        "(JJJ)V",
        "getReaderTransactionCount",
        "()J",
        "getReaderUtcEpochTime",
        "getSessionId",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final readerTransactionCount:J

.field private final readerUtcEpochTime:J

.field private final sessionId:J


# direct methods
.method public constructor <init>(JJJ)V
    .locals 1

    const/4 v0, 0x0

    .line 522
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-wide p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->sessionId:J

    iput-wide p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->readerTransactionCount:J

    iput-wide p5, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->readerUtcEpochTime:J

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;JJJILjava/lang/Object;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;
    .locals 7

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-wide p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->sessionId:J

    :cond_0
    move-wide v1, p1

    and-int/lit8 p1, p7, 0x2

    if-eqz p1, :cond_1

    iget-wide p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->readerTransactionCount:J

    :cond_1
    move-wide v3, p3

    and-int/lit8 p1, p7, 0x4

    if-eqz p1, :cond_2

    iget-wide p5, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->readerUtcEpochTime:J

    :cond_2
    move-wide v5, p5

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->copy(JJJ)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->sessionId:J

    return-wide v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->readerTransactionCount:J

    return-wide v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->readerUtcEpochTime:J

    return-wide v0
.end method

.method public final copy(JJJ)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;
    .locals 8

    new-instance v7, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;

    move-object v0, v7

    move-wide v1, p1

    move-wide v3, p3

    move-wide v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;-><init>(JJJ)V

    return-object v7
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;

    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->sessionId:J

    iget-wide v2, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->sessionId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->readerTransactionCount:J

    iget-wide v2, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->readerTransactionCount:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->readerUtcEpochTime:J

    iget-wide v2, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->readerUtcEpochTime:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getReaderTransactionCount()J
    .locals 2

    .line 520
    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->readerTransactionCount:J

    return-wide v0
.end method

.method public final getReaderUtcEpochTime()J
    .locals 2

    .line 521
    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->readerUtcEpochTime:J

    return-wide v0
.end method

.method public final getSessionId()J
    .locals 2

    .line 519
    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->sessionId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->sessionId:J

    invoke-static {v0, v1}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->readerTransactionCount:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->readerUtcEpochTime:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnSecureSessionValid(sessionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->sessionId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", readerTransactionCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->readerTransactionCount:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", readerUtcEpochTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;->readerUtcEpochTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
