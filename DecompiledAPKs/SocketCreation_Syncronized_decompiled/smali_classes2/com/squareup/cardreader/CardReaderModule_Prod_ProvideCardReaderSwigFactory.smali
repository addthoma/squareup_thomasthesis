.class public final Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;
.super Ljava/lang/Object;
.source "CardReaderModule_Prod_ProvideCardReaderSwigFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReaderSwig;",
        ">;"
    }
.end annotation


# instance fields
.field private final bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final completedPaymentListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;",
            ">;"
        }
    .end annotation
.end field

.field private final coreDumpFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CoreDumpFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private final eventLogFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/EventLogFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private final executorServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final firmwareUpdateFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private final firmwareUpdateListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcPaymentListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;",
            ">;"
        }
    .end annotation
.end field

.field private final powerFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PowerFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private final secureSessionFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private final secureSessionRevocationFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionRevocationFeature;",
            ">;"
        }
    .end annotation
.end field

.field private final secureTouchCardReaderFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureTouchFeatureInterface;",
            ">;"
        }
    .end annotation
.end field

.field private final systemFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SystemFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private final tamperFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/TamperFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private final userInteractionFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/UserInteractionFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CoreDumpFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/EventLogFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PowerFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureTouchFeatureInterface;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SystemFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/TamperFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/UserInteractionFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionRevocationFeature;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 88
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->mainThreadProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 89
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->executorServiceProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 90
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->cardReaderFeatureProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 91
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->coreDumpFeatureProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 92
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->eventLogFeatureProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 93
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->firmwareUpdateFeatureProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 94
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->paymentFeatureProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 95
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->powerFeatureProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 96
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->secureSessionFeatureProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 97
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->secureTouchCardReaderFeatureProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 98
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->systemFeatureProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 99
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->tamperFeatureProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 100
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->userInteractionFeatureProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 101
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->cardReaderListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 102
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->firmwareUpdateListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 103
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->paymentListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 104
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->nfcPaymentListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 105
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->completedPaymentListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 106
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->secureSessionRevocationFeatureProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 107
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->cardReaderProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 108
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 109
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 110
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CoreDumpFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/EventLogFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PowerFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureTouchFeatureInterface;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SystemFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/TamperFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/UserInteractionFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionRevocationFeature;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;)",
            "Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    .line 141
    new-instance v24, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;

    move-object/from16 v0, v24

    invoke-direct/range {v0 .. v23}, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v24
.end method

.method public static provideCardReaderSwig(Lcom/squareup/thread/executor/MainThread;Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/CardReaderFeatureLegacy;Ljava/lang/Object;Lcom/squareup/cardreader/EventLogFeatureLegacy;Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/cardreader/PowerFeatureLegacy;Ljava/lang/Object;Lcom/squareup/cardreader/SecureTouchFeatureInterface;Lcom/squareup/cardreader/SystemFeatureLegacy;Lcom/squareup/cardreader/TamperFeatureLegacy;Lcom/squareup/cardreader/UserInteractionFeatureLegacy;Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;Lcom/squareup/cardreader/SecureSessionRevocationFeature;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/cardreader/CardReaderSwig;
    .locals 23

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v4, p4

    move-object/from16 v7, p7

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v16, p16

    move-object/from16 v17, p17

    move-object/from16 v18, p18

    move-object/from16 v19, p19

    move-object/from16 v20, p20

    move-object/from16 v21, p21

    move-object/from16 v22, p22

    .line 158
    move-object/from16 v3, p3

    check-cast v3, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;

    move-object/from16 v5, p5

    check-cast v5, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;

    move-object/from16 v6, p6

    check-cast v6, Lcom/squareup/cardreader/PaymentFeatureLegacy;

    move-object/from16 v8, p8

    check-cast v8, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

    invoke-static/range {v0 .. v22}, Lcom/squareup/cardreader/CardReaderModule$Prod;->provideCardReaderSwig(Lcom/squareup/thread/executor/MainThread;Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/CardReaderFeatureLegacy;Lcom/squareup/cardreader/CoreDumpFeatureLegacy;Lcom/squareup/cardreader/EventLogFeatureLegacy;Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;Lcom/squareup/cardreader/PaymentFeatureLegacy;Lcom/squareup/cardreader/PowerFeatureLegacy;Lcom/squareup/cardreader/SecureSessionFeatureLegacy;Lcom/squareup/cardreader/SecureTouchFeatureInterface;Lcom/squareup/cardreader/SystemFeatureLegacy;Lcom/squareup/cardreader/TamperFeatureLegacy;Lcom/squareup/cardreader/UserInteractionFeatureLegacy;Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;Lcom/squareup/cardreader/SecureSessionRevocationFeature;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/cardreader/CardReaderSwig;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderSwig;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReaderSwig;
    .locals 25

    move-object/from16 v0, p0

    .line 115
    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/thread/executor/MainThread;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->executorServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->cardReaderFeatureProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/cardreader/CardReaderFeatureLegacy;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->coreDumpFeatureProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->eventLogFeatureProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/cardreader/EventLogFeatureLegacy;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->firmwareUpdateFeatureProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->paymentFeatureProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v8

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->powerFeatureProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/cardreader/PowerFeatureLegacy;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->secureSessionFeatureProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v10

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->secureTouchCardReaderFeatureProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/cardreader/SecureTouchFeatureInterface;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->systemFeatureProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/cardreader/SystemFeatureLegacy;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->tamperFeatureProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/cardreader/TamperFeatureLegacy;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->userInteractionFeatureProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->cardReaderListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->firmwareUpdateListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->paymentListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->nfcPaymentListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->completedPaymentListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->secureSessionRevocationFeatureProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/cardreader/SecureSessionRevocationFeature;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->cardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/cardreader/CardReader;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/cardreader/CardReaderFactory;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/cardreader/BluetoothUtils;

    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/cardreader/CardReaderListeners;

    invoke-static/range {v2 .. v24}, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->provideCardReaderSwig(Lcom/squareup/thread/executor/MainThread;Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/CardReaderFeatureLegacy;Ljava/lang/Object;Lcom/squareup/cardreader/EventLogFeatureLegacy;Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/cardreader/PowerFeatureLegacy;Ljava/lang/Object;Lcom/squareup/cardreader/SecureTouchFeatureInterface;Lcom/squareup/cardreader/SystemFeatureLegacy;Lcom/squareup/cardreader/TamperFeatureLegacy;Lcom/squareup/cardreader/UserInteractionFeatureLegacy;Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;Lcom/squareup/cardreader/SecureSessionRevocationFeature;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/cardreader/CardReaderSwig;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->get()Lcom/squareup/cardreader/CardReaderSwig;

    move-result-object v0

    return-object v0
.end method
