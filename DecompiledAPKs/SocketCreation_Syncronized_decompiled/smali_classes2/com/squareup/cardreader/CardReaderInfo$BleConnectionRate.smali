.class public Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;
.super Ljava/lang/Object;
.source "CardReaderInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BleConnectionRate"
.end annotation


# instance fields
.field private connectionInterval:I

.field private mtuSize:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 754
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;I)I
    .locals 0

    .line 754
    iput p1, p0, Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;->mtuSize:I

    return p1
.end method

.method static synthetic access$102(Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;I)I
    .locals 0

    .line 754
    iput p1, p0, Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;->connectionInterval:I

    return p1
.end method


# virtual methods
.method public getConnectionIntervalMillis()I
    .locals 1

    .line 764
    iget v0, p0, Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;->connectionInterval:I

    mul-int/lit8 v0, v0, 0x5

    div-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public getMtuSize()I
    .locals 1

    .line 759
    iget v0, p0, Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;->mtuSize:I

    return v0
.end method
