.class public final enum Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;
.super Ljava/lang/Enum;
.source "CardReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AbortSecureSessionReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

.field public static final enum CLIENT_ERROR:Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

.field public static final enum NETWORK_ERROR:Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

.field public static final enum SERVER_ERROR:Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 19
    new-instance v0, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    const/4 v1, 0x0

    const-string v2, "CLIENT_ERROR"

    invoke-direct {v0, v2, v1}, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->CLIENT_ERROR:Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    .line 20
    new-instance v0, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    const/4 v2, 0x1

    const-string v3, "NETWORK_ERROR"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->NETWORK_ERROR:Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    .line 21
    new-instance v0, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    const/4 v3, 0x2

    const-string v4, "SERVER_ERROR"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->SERVER_ERROR:Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    .line 18
    sget-object v4, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->CLIENT_ERROR:Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->NETWORK_ERROR:Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->SERVER_ERROR:Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->$VALUES:[Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;
    .locals 1

    .line 18
    const-class v0, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->$VALUES:[Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    return-object v0
.end method
