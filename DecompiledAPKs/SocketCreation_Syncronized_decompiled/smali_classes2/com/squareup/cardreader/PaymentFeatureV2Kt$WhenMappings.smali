.class public final synthetic Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I

.field public static final synthetic $EnumSwitchMapping$3:[I

.field public static final synthetic $EnumSwitchMapping$4:[I

.field public static final synthetic $EnumSwitchMapping$5:[I

.field public static final synthetic $EnumSwitchMapping$6:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 18

    invoke-static {}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->values()[Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->TMN_AUDIO_QP_SUCCESS:Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->TMN_AUDIO_ID_SUCCESS:Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->TMN_AUDIO_SUICA_SUCCESS:Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->TMN_AUDIO_SUICA_SUCCESS_2:Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->TMN_AUDIO_FAILURE:Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->ordinal()I

    move-result v1

    const/4 v6, 0x5

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->TMN_AUDIO_RE_TOUCH_LOOP:Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->ordinal()I

    move-result v1

    const/4 v7, 0x6

    aput v7, v0, v1

    invoke-static {}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->values()[Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_CARD_READ_ERROR:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_DISABLED_CARD:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_INVALID_BRAND_SELECTION:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_CANCELLATION:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_INSUFFICIENT_BALANCE:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_WAITING_FOR_RETOUCH:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    const/4 v8, 0x7

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_TMN_CENTER_ERROR:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    const/16 v9, 0x8

    aput v9, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_POLLING_TIMEOUT:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    const/16 v10, 0x9

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_IMPOSSIBLE_OPERATION:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    const/16 v11, 0xa

    aput v11, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_MULTIPLE_CARDS_DETECTED:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    const/16 v12, 0xb

    aput v12, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_EXCEED_THRESHOLD:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    const/16 v13, 0xc

    aput v13, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_CENTER_OPERATION_FAILED:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    const/16 v14, 0xd

    aput v14, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_INVALID_PARAMETER:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    const/16 v15, 0xe

    aput v15, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_SUMMARY_ERROR:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    const/16 v16, 0xf

    aput v16, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_DISABLED_TERMINAL:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    const/16 v17, 0x10

    aput v17, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_ONLINE_PROCESSING_FAILURE:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    const/16 v17, 0x11

    aput v17, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_MIRYO_RESOLUTION_FAILURE:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    const/16 v17, 0x12

    aput v17, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_MIRYO_RESULT_FAILURE:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    const/16 v17, 0x13

    aput v17, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_OTHER:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result v1

    const/16 v17, 0x14

    aput v17, v0, v1

    invoke-static {}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->values()[Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_UNKNOWN_ERROR:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_WAITING:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_PROCESSING_TRANSACTION:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_COMPLETE_NORMAL_SETTLE:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_INSUFFICIENT_BALANCE:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_SEVERAL_SUICA_CARD:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_RETAP_WAITING:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_ONLINE_PROCESSING:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    aput v9, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_COMPLETE_NORMAL_CHECK_BAL:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_READ_ERROR:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    aput v11, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_WRITE_ERROR:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    aput v12, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_INVALID_CARD:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    aput v13, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_CANCEL:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    aput v14, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_DIFFERENT_CARD:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    aput v15, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_COMMON_ERROR:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    aput v16, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_EXCEEDED_LIMIT_AMOUNT:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/16 v10, 0x10

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_EXPIRED_CARD:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/16 v10, 0x11

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_LOCKED_MOBILE_SERVICE:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/16 v10, 0x12

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_WRONG_CARD_ERROR:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/16 v10, 0x13

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_POLLING_TIMEOUT:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/16 v10, 0x14

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_MIRYO_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/16 v10, 0x15

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_MIRYO_RESULT_FAILURE:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/16 v10, 0x16

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_MIRYO_RESULT_UNKNOWN:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/16 v10, 0x17

    aput v10, v0, v1

    invoke-static {}, Lcom/squareup/cardreader/TmnRequestType;->values()[Lcom/squareup/cardreader/TmnRequestType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/cardreader/TmnRequestType;->TMN_REQUEST_TRANSACTION:Lcom/squareup/cardreader/TmnRequestType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/TmnRequestType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/cardreader/TmnRequestType;->TMN_REQUEST_CHECK_BALANCE:Lcom/squareup/cardreader/TmnRequestType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/TmnRequestType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/cardreader/TmnRequestType;->TMN_REQUEST_CANCELLATION:Lcom/squareup/cardreader/TmnRequestType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/TmnRequestType;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/cardreader/TmnRequestType;->TMN_REQUEST_CHECK_HISTORY:Lcom/squareup/cardreader/TmnRequestType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/TmnRequestType;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/cardreader/TmnRequestType;->TMN_REQUEST_ONLINE_TEST:Lcom/squareup/cardreader/TmnRequestType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/TmnRequestType;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/cardreader/TmnRequestType;->TMN_REQUEST_CHECK_RESULT:Lcom/squareup/cardreader/TmnRequestType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/TmnRequestType;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/cardreader/TmnRequestType;->TMN_REQUEST_VOID_UNKNOWN:Lcom/squareup/cardreader/TmnRequestType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/TmnRequestType;->ordinal()I

    move-result v1

    aput v8, v0, v1

    invoke-static {}, Lcom/squareup/cardreader/TmnBrandId;->values()[Lcom/squareup/cardreader/TmnBrandId;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/cardreader/TmnBrandId;->TMN_BRAND_ID_COMMON:Lcom/squareup/cardreader/TmnBrandId;

    invoke-virtual {v1}, Lcom/squareup/cardreader/TmnBrandId;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/cardreader/TmnBrandId;->TMN_BRAND_ID_QUICPAY:Lcom/squareup/cardreader/TmnBrandId;

    invoke-virtual {v1}, Lcom/squareup/cardreader/TmnBrandId;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/cardreader/TmnBrandId;->TMN_BRAND_ID_ID:Lcom/squareup/cardreader/TmnBrandId;

    invoke-virtual {v1}, Lcom/squareup/cardreader/TmnBrandId;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/cardreader/TmnBrandId;->TMN_BRAND_ID_SUICA:Lcom/squareup/cardreader/TmnBrandId;

    invoke-virtual {v1}, Lcom/squareup/cardreader/TmnBrandId;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/cardreader/TmnBrandId;->TMN_BRAND_ID_RAKUTEN:Lcom/squareup/cardreader/TmnBrandId;

    invoke-virtual {v1}, Lcom/squareup/cardreader/TmnBrandId;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/cardreader/TmnBrandId;->TMN_BRAND_ID_WAON:Lcom/squareup/cardreader/TmnBrandId;

    invoke-virtual {v1}, Lcom/squareup/cardreader/TmnBrandId;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/cardreader/TmnBrandId;->TMN_BRAND_ID_NANACO:Lcom/squareup/cardreader/TmnBrandId;

    invoke-virtual {v1}, Lcom/squareup/cardreader/TmnBrandId;->ordinal()I

    move-result v1

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/cardreader/TmnBrandId;->TMN_BRAND_ID_PITAPA:Lcom/squareup/cardreader/TmnBrandId;

    invoke-virtual {v1}, Lcom/squareup/cardreader/TmnBrandId;->ordinal()I

    move-result v1

    aput v9, v0, v1

    invoke-static {}, Lcom/squareup/cardreader/PaymentAccountType;->values()[Lcom/squareup/cardreader/PaymentAccountType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v1, Lcom/squareup/cardreader/PaymentAccountType;->PAYMENT_ACCOUNT_TYPE_DEFAULT:Lcom/squareup/cardreader/PaymentAccountType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/PaymentAccountType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v1, Lcom/squareup/cardreader/PaymentAccountType;->PAYMENT_ACCOUNT_TYPE_SAVINGS:Lcom/squareup/cardreader/PaymentAccountType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/PaymentAccountType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v1, Lcom/squareup/cardreader/PaymentAccountType;->PAYMENT_ACCOUNT_TYPE_DEBIT:Lcom/squareup/cardreader/PaymentAccountType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/PaymentAccountType;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v1, Lcom/squareup/cardreader/PaymentAccountType;->PAYMENT_ACCOUNT_TYPE_CREDIT:Lcom/squareup/cardreader/PaymentAccountType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/PaymentAccountType;->ordinal()I

    move-result v1

    aput v5, v0, v1

    invoke-static {}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->values()[Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_DEFAULT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_SAVINGS:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_DEBIT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_CREDIT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_MAX_NUM:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->ordinal()I

    move-result v1

    aput v6, v0, v1

    return-void
.end method
