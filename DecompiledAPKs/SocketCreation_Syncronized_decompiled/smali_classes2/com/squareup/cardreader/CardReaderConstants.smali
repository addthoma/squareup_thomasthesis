.class public interface abstract Lcom/squareup/cardreader/CardReaderConstants;
.super Ljava/lang/Object;
.source "CardReaderConstants.java"


# virtual methods
.method public abstract appProtocolVersion()I
.end method

.method public abstract epProtocolVersion()I
.end method

.method public abstract transportPauseTimeInMilliseconds()I
.end method

.method public abstract transportProtocolVersion()I
.end method

.method public abstract transportSmallFrameThreshold()I
.end method
