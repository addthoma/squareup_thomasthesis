.class public final Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionStateFactory;
.super Ljava/lang/Object;
.source "GlobalHeadsetModule_ProvideHeadsetConnectionStateFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
        ">;"
    }
.end annotation


# instance fields
.field private final headsetProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/cardreader/GlobalHeadsetModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/GlobalHeadsetModule;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalHeadsetModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionStateFactory;->module:Lcom/squareup/cardreader/GlobalHeadsetModule;

    .line 26
    iput-object p2, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionStateFactory;->headsetProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/GlobalHeadsetModule;Ljavax/inject/Provider;)Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionStateFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalHeadsetModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset;",
            ">;)",
            "Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionStateFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionStateFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionStateFactory;-><init>(Lcom/squareup/cardreader/GlobalHeadsetModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideHeadsetConnectionState(Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/wavpool/swipe/Headset;)Lcom/squareup/wavpool/swipe/HeadsetConnectionState;
    .locals 0

    .line 41
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/GlobalHeadsetModule;->provideHeadsetConnectionState(Lcom/squareup/wavpool/swipe/Headset;)Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/HeadsetConnectionState;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionStateFactory;->module:Lcom/squareup/cardreader/GlobalHeadsetModule;

    iget-object v1, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionStateFactory;->headsetProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/wavpool/swipe/Headset;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionStateFactory;->provideHeadsetConnectionState(Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/wavpool/swipe/Headset;)Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionStateFactory;->get()Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    move-result-object v0

    return-object v0
.end method
