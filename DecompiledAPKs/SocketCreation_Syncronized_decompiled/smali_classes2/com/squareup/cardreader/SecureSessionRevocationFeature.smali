.class public Lcom/squareup/cardreader/SecureSessionRevocationFeature;
.super Ljava/lang/Object;
.source "SecureSessionRevocationFeature.java"

# interfaces
.implements Lcom/squareup/ms/AppFunctionStatusListener;


# instance fields
.field private final globalRevocationState:Lcom/squareup/cardreader/SecureSessionRevocationState;

.field private final minesweeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;"
        }
    .end annotation
.end field

.field private notified:Z

.field private final res:Lcom/squareup/util/Res;

.field private statusListenerBridge:Lcom/squareup/ms/AppFunctionStatusListenerBridge;

.field private swigInternalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;


# direct methods
.method constructor <init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/SecureSessionRevocationState;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Lcom/squareup/cardreader/SecureSessionRevocationState;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->minesweeperProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->res:Lcom/squareup/util/Res;

    .line 44
    iput-object p2, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->globalRevocationState:Lcom/squareup/cardreader/SecureSessionRevocationState;

    const/4 p1, 0x0

    .line 46
    iput-boolean p1, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->notified:Z

    const/4 p1, 0x0

    .line 47
    iput-object p1, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->swigInternalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    .line 48
    iput-object p1, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->statusListenerBridge:Lcom/squareup/ms/AppFunctionStatusListenerBridge;

    return-void
.end method

.method static convertUxHintEnumToSwig(Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;)Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;
    .locals 1

    .line 188
    sget-object v0, Lcom/squareup/cardreader/SecureSessionRevocationFeature$1;->$SwitchMap$com$squareup$protos$client$flipper$AugmentedStatus$UserExperienceHint:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    .line 195
    sget-object p0, Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;->CR_SECURESESSION_FEATURE_SERVER_UX_HINT_NO_SUGGESTED_ACTION:Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;

    return-object p0

    .line 192
    :cond_0
    sget-object p0, Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;->CR_SECURESESSION_FEATURE_SERVER_UX_HINT_SUGGEST_ACTIVATION:Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;

    return-object p0

    .line 190
    :cond_1
    sget-object p0, Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;->CR_SECURESESSION_FEATURE_SERVER_UX_HINT_SUGGEST_RETRY:Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;

    return-object p0
.end method

.method private getDescriptionOrDefault(Lcom/squareup/protos/client/flipper/AugmentedStatus;)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_1

    .line 156
    iget-object v0, p1, Lcom/squareup/protos/client/flipper/AugmentedStatus;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/protos/client/flipper/AugmentedStatus;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/protos/client/flipper/AugmentedStatus;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    .line 159
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 163
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/flipper/AugmentedStatus;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    return-object p1

    .line 160
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/dipper/R$string;->secure_session_revoked_default_desc:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static getFirstHintOrDefault(Lcom/squareup/protos/client/flipper/AugmentedStatus;)Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;
    .locals 1

    if-eqz p0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/squareup/protos/client/flipper/AugmentedStatus;->ux_hint:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 180
    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/client/flipper/AugmentedStatus;->ux_hint:Ljava/util/List;

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    invoke-static {p0}, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->convertUxHintEnumToSwig(Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;)Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;

    move-result-object p0

    return-object p0

    .line 173
    :cond_1
    :goto_0
    sget-object p0, Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;->CR_SECURESESSION_FEATURE_SERVER_UX_HINT_NO_SUGGESTED_ACTION:Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;

    return-object p0
.end method

.method private getTitleOrDefault(Lcom/squareup/protos/client/flipper/AugmentedStatus;)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_1

    .line 140
    iget-object v0, p1, Lcom/squareup/protos/client/flipper/AugmentedStatus;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/protos/client/flipper/AugmentedStatus;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/protos/client/flipper/AugmentedStatus;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    .line 143
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 147
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/flipper/AugmentedStatus;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    return-object p1

    .line 144
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/dipper/R$string;->secure_session_revoked_default_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method getInternalLister()Lcom/squareup/cardreader/CardReaderSwig$InternalListener;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->swigInternalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    return-object v0
.end method

.method getStatusListenerBridge()Lcom/squareup/ms/AppFunctionStatusListenerBridge;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->statusListenerBridge:Lcom/squareup/ms/AppFunctionStatusListenerBridge;

    return-object v0
.end method

.method public initializeFeature(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;)V
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->minesweeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 104
    iput-object p1, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->swigInternalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    .line 105
    new-instance p1, Lcom/squareup/ms/AppFunctionStatusListenerBridge;

    invoke-direct {p1, p0}, Lcom/squareup/ms/AppFunctionStatusListenerBridge;-><init>(Lcom/squareup/ms/AppFunctionStatusListener;)V

    iput-object p1, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->statusListenerBridge:Lcom/squareup/ms/AppFunctionStatusListenerBridge;

    .line 106
    iget-object p1, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->minesweeperProvider:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ms/Minesweeper;

    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->statusListenerBridge:Lcom/squareup/ms/AppFunctionStatusListenerBridge;

    invoke-interface {p1, v0}, Lcom/squareup/ms/Minesweeper;->addStatusListener(Lcom/squareup/ms/NativeAppFunctionStatusListener;)V

    :cond_0
    return-void
.end method

.method public onStatusUpdate(Lcom/squareup/ms/AppFunction;Lcom/squareup/protos/client/flipper/AugmentedStatus;)V
    .locals 3

    .line 60
    sget-object v0, Lcom/squareup/ms/AppFunction;->SECURE_SESSION:Lcom/squareup/ms/AppFunction;

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    .line 65
    iget-boolean v1, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->notified:Z

    if-eqz v1, :cond_1

    const-string v1, "[redundant]"

    goto :goto_0

    :cond_1
    const-string v1, "[first]"

    :goto_0
    aput-object v1, p1, v0

    if-eqz p2, :cond_2

    sget-object v0, Lcom/squareup/protos/client/flipper/AugmentedStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 67
    invoke-virtual {v0, p2}, Lcom/squareup/wire/ProtoAdapter;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    const/4 v1, 0x1

    aput-object v0, p1, v1

    const-string v0, "[SecureSessionRevocationFeature#onStatusUpdate] [%s] status = %s"

    .line 65
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    iget-boolean p1, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->notified:Z

    if-eqz p1, :cond_3

    return-void

    .line 75
    :cond_3
    invoke-direct {p0, p2}, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->getTitleOrDefault(Lcom/squareup/protos/client/flipper/AugmentedStatus;)Ljava/lang/String;

    move-result-object p1

    .line 76
    invoke-direct {p0, p2}, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->getDescriptionOrDefault(Lcom/squareup/protos/client/flipper/AugmentedStatus;)Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-static {p2}, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->getFirstHintOrDefault(Lcom/squareup/protos/client/flipper/AugmentedStatus;)Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;

    move-result-object p2

    .line 81
    iget-object v2, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->globalRevocationState:Lcom/squareup/cardreader/SecureSessionRevocationState;

    invoke-virtual {v2, p1, v0, p2}, Lcom/squareup/cardreader/SecureSessionRevocationState;->setRevoked(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V

    .line 82
    iput-boolean v1, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->notified:Z

    .line 88
    iget-object v1, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->swigInternalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    invoke-virtual {v1, p1, v0, p2}, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;->onSecureSessionRevoked(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V

    return-void
.end method

.method public onStatusUpdateInvalidAppFunction(I)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 96
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "Invalid AppFunction value %d"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onStatusUpdateInvalidData(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 92
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public reset()V
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->minesweeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->statusListenerBridge:Lcom/squareup/ms/AppFunctionStatusListenerBridge;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->minesweeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ms/Minesweeper;

    iget-object v1, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->statusListenerBridge:Lcom/squareup/ms/AppFunctionStatusListenerBridge;

    invoke-interface {v0, v1}, Lcom/squareup/ms/Minesweeper;->removeStatusListener(Lcom/squareup/ms/NativeAppFunctionStatusListener;)V

    :cond_0
    const/4 v0, 0x0

    .line 119
    iput-object v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->statusListenerBridge:Lcom/squareup/ms/AppFunctionStatusListenerBridge;

    .line 120
    iput-object v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->swigInternalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    const/4 v0, 0x0

    .line 121
    iput-boolean v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->notified:Z

    :cond_1
    return-void
.end method
