.class public final Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;
.super Ljava/lang/Object;
.source "LocalCardReaderModule_ProvideLocalCardReaderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/LocalCardReader;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private final firmwareUpdaterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentProcessor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentProcessor;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;->cardReaderInitializerProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;->firmwareUpdaterProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;->paymentProcessorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentProcessor;",
            ">;)",
            "Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideLocalCardReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderInitializer;Lcom/squareup/cardreader/FirmwareUpdater;Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/LocalCardReader;
    .locals 0

    .line 52
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/cardreader/LocalCardReaderModule;->provideLocalCardReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderInitializer;Lcom/squareup/cardreader/FirmwareUpdater;Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/LocalCardReader;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/LocalCardReader;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/LocalCardReader;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v1, p0, Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;->cardReaderInitializerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderInitializer;

    iget-object v2, p0, Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;->firmwareUpdaterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/FirmwareUpdater;

    iget-object v3, p0, Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;->paymentProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;->provideLocalCardReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderInitializer;Lcom/squareup/cardreader/FirmwareUpdater;Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/LocalCardReader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;->get()Lcom/squareup/cardreader/LocalCardReader;

    move-result-object v0

    return-object v0
.end method
