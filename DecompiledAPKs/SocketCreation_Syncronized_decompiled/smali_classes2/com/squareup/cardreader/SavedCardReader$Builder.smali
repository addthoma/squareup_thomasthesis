.class public Lcom/squareup/cardreader/SavedCardReader$Builder;
.super Ljava/lang/Object;
.source "SavedCardReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/SavedCardReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private firmwareVersion:Ljava/lang/String;

.field private hardwareSerialNumber:Ljava/lang/String;

.field private isBluetoothClassic:Z

.field private lastConnectionSuccessUtcMillis:Ljava/lang/Long;

.field private macAddress:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private serialNumberLast4:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/SavedCardReader$Builder;)Ljava/lang/String;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/cardreader/SavedCardReader$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/cardreader/SavedCardReader$Builder;)Ljava/lang/String;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/cardreader/SavedCardReader$Builder;->serialNumberLast4:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/cardreader/SavedCardReader$Builder;)Ljava/lang/String;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/cardreader/SavedCardReader$Builder;->macAddress:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/cardreader/SavedCardReader$Builder;)Z
    .locals 0

    .line 57
    iget-boolean p0, p0, Lcom/squareup/cardreader/SavedCardReader$Builder;->isBluetoothClassic:Z

    return p0
.end method

.method static synthetic access$400(Lcom/squareup/cardreader/SavedCardReader$Builder;)Ljava/lang/Long;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/cardreader/SavedCardReader$Builder;->lastConnectionSuccessUtcMillis:Ljava/lang/Long;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/cardreader/SavedCardReader$Builder;)Ljava/lang/String;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/cardreader/SavedCardReader$Builder;->firmwareVersion:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/cardreader/SavedCardReader$Builder;)Ljava/lang/String;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/cardreader/SavedCardReader$Builder;->hardwareSerialNumber:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/SavedCardReader;
    .locals 2

    .line 67
    new-instance v0, Lcom/squareup/cardreader/SavedCardReader;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/cardreader/SavedCardReader;-><init>(Lcom/squareup/cardreader/SavedCardReader$Builder;Lcom/squareup/cardreader/SavedCardReader$1;)V

    return-object v0
.end method

.method public firmwareVersion(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/cardreader/SavedCardReader$Builder;->firmwareVersion:Ljava/lang/String;

    return-object p0
.end method

.method public hardwareSerialNumber(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/cardreader/SavedCardReader$Builder;->hardwareSerialNumber:Ljava/lang/String;

    return-object p0
.end method

.method public isBluetoothClassic(Z)Lcom/squareup/cardreader/SavedCardReader$Builder;
    .locals 0

    .line 86
    iput-boolean p1, p0, Lcom/squareup/cardreader/SavedCardReader$Builder;->isBluetoothClassic:Z

    return-object p0
.end method

.method public lastConnectionSuccessUtcMillis(Ljava/lang/Long;)Lcom/squareup/cardreader/SavedCardReader$Builder;
    .locals 0

    .line 91
    iput-object p1, p0, Lcom/squareup/cardreader/SavedCardReader$Builder;->lastConnectionSuccessUtcMillis:Ljava/lang/Long;

    return-object p0
.end method

.method public macAddress(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/cardreader/SavedCardReader$Builder;->macAddress:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/squareup/cardreader/SavedCardReader$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public serialNumberLast4(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;
    .locals 0

    .line 76
    iput-object p1, p0, Lcom/squareup/cardreader/SavedCardReader$Builder;->serialNumberLast4:Ljava/lang/String;

    return-object p0
.end method
