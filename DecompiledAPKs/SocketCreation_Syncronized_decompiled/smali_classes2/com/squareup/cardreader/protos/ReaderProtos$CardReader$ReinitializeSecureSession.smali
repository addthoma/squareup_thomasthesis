.class public final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReinitializeSecureSession"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$ProtoAdapter_ReinitializeSecureSession;,
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 879
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$ProtoAdapter_ReinitializeSecureSession;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$ProtoAdapter_ReinitializeSecureSession;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 884
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;-><init>(Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;)V
    .locals 1

    .line 888
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p1}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 901
    :cond_0
    instance-of v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 902
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    .line 903
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 908
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$Builder;
    .locals 2

    .line 893
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$Builder;-><init>()V

    .line 894
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 878
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 913
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReinitializeSecureSession{"

    .line 914
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
