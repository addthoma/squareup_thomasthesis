.class public final enum Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;
.super Ljava/lang/Enum;
.source "ReaderProtos.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SecureSessionState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState$ProtoAdapter_SecureSessionState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

.field public static final enum ABORTED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DENIED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

.field public static final enum INVALID:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

.field public static final enum VALID:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 6625
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    const/4 v1, 0x0

    const-string v2, "INVALID"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->INVALID:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    .line 6627
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    const/4 v2, 0x1

    const-string v3, "ABORTED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->ABORTED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    .line 6629
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    const/4 v3, 0x2

    const-string v4, "VALID"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->VALID:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    .line 6631
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    const/4 v4, 0x3

    const-string v5, "DENIED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->DENIED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    .line 6624
    sget-object v5, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->INVALID:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->ABORTED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->VALID:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->DENIED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    .line 6633
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState$ProtoAdapter_SecureSessionState;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState$ProtoAdapter_SecureSessionState;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 6637
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 6638
    iput p3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 6649
    :cond_0
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->DENIED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    return-object p0

    .line 6648
    :cond_1
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->VALID:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    return-object p0

    .line 6647
    :cond_2
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->ABORTED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    return-object p0

    .line 6646
    :cond_3
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->INVALID:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;
    .locals 1

    .line 6624
    const-class v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;
    .locals 1

    .line 6624
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 6656
    iget v0, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->value:I

    return v0
.end method
