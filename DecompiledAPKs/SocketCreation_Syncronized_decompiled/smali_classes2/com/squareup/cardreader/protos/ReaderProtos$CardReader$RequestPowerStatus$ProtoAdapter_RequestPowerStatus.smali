.class final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus$ProtoAdapter_RequestPowerStatus;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_RequestPowerStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 430
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 445
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus$Builder;-><init>()V

    .line 446
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 447
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 450
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 454
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 455
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 428
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus$ProtoAdapter_RequestPowerStatus;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 440
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 428
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus$ProtoAdapter_RequestPowerStatus;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;)I
    .locals 0

    .line 435
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 428
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus$ProtoAdapter_RequestPowerStatus;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;
    .locals 0

    .line 460
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus$Builder;

    move-result-object p1

    .line 461
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 462
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 428
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus$ProtoAdapter_RequestPowerStatus;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    move-result-object p1

    return-object p1
.end method
