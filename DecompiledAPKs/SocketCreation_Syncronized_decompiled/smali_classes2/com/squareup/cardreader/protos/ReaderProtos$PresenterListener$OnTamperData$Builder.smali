.class public final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public tamper_data:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4379
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;
    .locals 3

    .line 4389
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData$Builder;->tamper_data:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;-><init>(Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4376
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    move-result-object v0

    return-object v0
.end method

.method public tamper_data(Lokio/ByteString;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData$Builder;
    .locals 0

    .line 4383
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData$Builder;->tamper_data:Lokio/ByteString;

    return-object p0
.end method
