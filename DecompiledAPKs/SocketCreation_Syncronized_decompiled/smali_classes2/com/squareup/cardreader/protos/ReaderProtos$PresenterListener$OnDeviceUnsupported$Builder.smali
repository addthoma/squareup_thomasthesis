.class public final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3740
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;
    .locals 2

    .line 3745
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;-><init>(Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3739
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    move-result-object v0

    return-object v0
.end method
