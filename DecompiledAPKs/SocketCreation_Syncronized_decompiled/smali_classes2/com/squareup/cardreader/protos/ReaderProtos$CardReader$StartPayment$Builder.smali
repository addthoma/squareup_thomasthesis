.class public final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount_authorized:Ljava/lang/Long;

.field public current_time_millis:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1246
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount_authorized(Ljava/lang/Long;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;
    .locals 0

    .line 1250
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;->amount_authorized:Ljava/lang/Long;

    return-object p0
.end method

.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;
    .locals 4

    .line 1261
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;->amount_authorized:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;->current_time_millis:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;-><init>(Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1241
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    move-result-object v0

    return-object v0
.end method

.method public current_time_millis(Ljava/lang/Long;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;
    .locals 0

    .line 1255
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;->current_time_millis:Ljava/lang/Long;

    return-object p0
.end method
