.class public final Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public asset:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 2992
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 2993
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted$Builder;->asset:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public asset(Ljava/util/List;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;",
            ">;)",
            "Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted$Builder;"
        }
    .end annotation

    .line 2997
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 2998
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted$Builder;->asset:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;
    .locals 3

    .line 3004
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted$Builder;->asset:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2989
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    move-result-object v0

    return-object v0
.end method
