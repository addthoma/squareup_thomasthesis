.class public final enum Lcom/squareup/cardreader/BatteryLevel;
.super Ljava/lang/Enum;
.source "BatteryLevel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/BatteryLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/BatteryLevel;

.field public static final enum CHARGING:Lcom/squareup/cardreader/BatteryLevel;

.field public static final enum CRITICAL:Lcom/squareup/cardreader/BatteryLevel;

.field public static final enum DEAD:Lcom/squareup/cardreader/BatteryLevel;

.field public static final enum FULL:Lcom/squareup/cardreader/BatteryLevel;

.field public static final enum HIGH:Lcom/squareup/cardreader/BatteryLevel;

.field public static final enum LOW:Lcom/squareup/cardreader/BatteryLevel;

.field private static final LOW_WARNING_CEILING:I = 0xf

.field public static final enum MID:Lcom/squareup/cardreader/BatteryLevel;

.field private static final R12_THRESHOLD_FULL:I = 0x4b

.field private static final R12_THRESHOLD_HIGH:I = 0x32

.field private static final R12_THRESHOLD_LOW:I = 0xa

.field private static final R12_THRESHOLD_MID:I = 0x19

.field private static final THRESHOLD_HIGH:I = 0x43

.field private static final THRESHOLD_MID:I = 0x21

.field public static final enum UNKNOWN:Lcom/squareup/cardreader/BatteryLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 10
    new-instance v0, Lcom/squareup/cardreader/BatteryLevel;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1}, Lcom/squareup/cardreader/BatteryLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/BatteryLevel;->UNKNOWN:Lcom/squareup/cardreader/BatteryLevel;

    .line 11
    new-instance v0, Lcom/squareup/cardreader/BatteryLevel;

    const/4 v2, 0x1

    const-string v3, "CHARGING"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/BatteryLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/BatteryLevel;->CHARGING:Lcom/squareup/cardreader/BatteryLevel;

    .line 12
    new-instance v0, Lcom/squareup/cardreader/BatteryLevel;

    const/4 v3, 0x2

    const-string v4, "CRITICAL"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/BatteryLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/BatteryLevel;->CRITICAL:Lcom/squareup/cardreader/BatteryLevel;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/BatteryLevel;

    const/4 v4, 0x3

    const-string v5, "DEAD"

    invoke-direct {v0, v5, v4}, Lcom/squareup/cardreader/BatteryLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/BatteryLevel;->DEAD:Lcom/squareup/cardreader/BatteryLevel;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/BatteryLevel;

    const/4 v5, 0x4

    const-string v6, "FULL"

    invoke-direct {v0, v6, v5}, Lcom/squareup/cardreader/BatteryLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/BatteryLevel;->FULL:Lcom/squareup/cardreader/BatteryLevel;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/BatteryLevel;

    const/4 v6, 0x5

    const-string v7, "HIGH"

    invoke-direct {v0, v7, v6}, Lcom/squareup/cardreader/BatteryLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/BatteryLevel;->HIGH:Lcom/squareup/cardreader/BatteryLevel;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/BatteryLevel;

    const/4 v7, 0x6

    const-string v8, "MID"

    invoke-direct {v0, v8, v7}, Lcom/squareup/cardreader/BatteryLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/BatteryLevel;->MID:Lcom/squareup/cardreader/BatteryLevel;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/BatteryLevel;

    const/4 v8, 0x7

    const-string v9, "LOW"

    invoke-direct {v0, v9, v8}, Lcom/squareup/cardreader/BatteryLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/BatteryLevel;->LOW:Lcom/squareup/cardreader/BatteryLevel;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/cardreader/BatteryLevel;

    .line 9
    sget-object v9, Lcom/squareup/cardreader/BatteryLevel;->UNKNOWN:Lcom/squareup/cardreader/BatteryLevel;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/cardreader/BatteryLevel;->CHARGING:Lcom/squareup/cardreader/BatteryLevel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/BatteryLevel;->CRITICAL:Lcom/squareup/cardreader/BatteryLevel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/BatteryLevel;->DEAD:Lcom/squareup/cardreader/BatteryLevel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/BatteryLevel;->FULL:Lcom/squareup/cardreader/BatteryLevel;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/BatteryLevel;->HIGH:Lcom/squareup/cardreader/BatteryLevel;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/BatteryLevel;->MID:Lcom/squareup/cardreader/BatteryLevel;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/BatteryLevel;->LOW:Lcom/squareup/cardreader/BatteryLevel;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/cardreader/BatteryLevel;->$VALUES:[Lcom/squareup/cardreader/BatteryLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static forCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/BatteryLevel;
    .locals 3

    .line 71
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    .line 72
    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_0

    .line 73
    sget-object p0, Lcom/squareup/cardreader/BatteryLevel;->UNKNOWN:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->isBatteryDead()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    sget-object p0, Lcom/squareup/cardreader/BatteryLevel;->DEAD:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0

    .line 78
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->getBatteryMode()Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    move-result-object v0

    if-nez v0, :cond_2

    .line 80
    sget-object p0, Lcom/squareup/cardreader/BatteryLevel;->UNKNOWN:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0

    .line 82
    :cond_2
    sget-object v1, Lcom/squareup/cardreader/BatteryLevel$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsBatteryMode:[I

    invoke-virtual {v0}, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_7

    const/4 v2, 0x2

    if-eq v1, v2, :cond_6

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    const/4 p0, 0x4

    if-ne v1, p0, :cond_3

    .line 92
    sget-object p0, Lcom/squareup/cardreader/BatteryLevel;->CRITICAL:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0

    .line 94
    :cond_3
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown battery mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 88
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_5

    .line 89
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->getBatteryPercentage()I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/BatteryLevel;->forR12Percentage(I)Lcom/squareup/cardreader/BatteryLevel;

    move-result-object p0

    return-object p0

    .line 90
    :cond_5
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->getBatteryPercentage()I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/BatteryLevel;->forPercentage(I)Lcom/squareup/cardreader/BatteryLevel;

    move-result-object p0

    return-object p0

    .line 86
    :cond_6
    sget-object p0, Lcom/squareup/cardreader/BatteryLevel;->FULL:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0

    .line 84
    :cond_7
    sget-object p0, Lcom/squareup/cardreader/BatteryLevel;->CHARGING:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0
.end method

.method public static forPercentage(I)Lcom/squareup/cardreader/BatteryLevel;
    .locals 1

    const/16 v0, 0x43

    if-le p0, v0, :cond_0

    .line 34
    sget-object p0, Lcom/squareup/cardreader/BatteryLevel;->FULL:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0

    :cond_0
    const/16 v0, 0x21

    if-le p0, v0, :cond_1

    .line 37
    sget-object p0, Lcom/squareup/cardreader/BatteryLevel;->MID:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0

    :cond_1
    if-lez p0, :cond_2

    .line 40
    sget-object p0, Lcom/squareup/cardreader/BatteryLevel;->LOW:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0

    .line 42
    :cond_2
    sget-object p0, Lcom/squareup/cardreader/BatteryLevel;->DEAD:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0
.end method

.method public static forR12Percentage(I)Lcom/squareup/cardreader/BatteryLevel;
    .locals 1

    const/16 v0, 0x4b

    if-le p0, v0, :cond_0

    .line 51
    sget-object p0, Lcom/squareup/cardreader/BatteryLevel;->FULL:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0

    :cond_0
    const/16 v0, 0x32

    if-le p0, v0, :cond_1

    .line 54
    sget-object p0, Lcom/squareup/cardreader/BatteryLevel;->HIGH:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0

    :cond_1
    const/16 v0, 0x19

    if-le p0, v0, :cond_2

    .line 57
    sget-object p0, Lcom/squareup/cardreader/BatteryLevel;->MID:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0

    :cond_2
    const/16 v0, 0xa

    if-le p0, v0, :cond_3

    .line 60
    sget-object p0, Lcom/squareup/cardreader/BatteryLevel;->LOW:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0

    .line 62
    :cond_3
    sget-object p0, Lcom/squareup/cardreader/BatteryLevel;->DEAD:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0
.end method

.method public static isBatteryLow(Lcom/squareup/cardreader/CardReaderInfo;)Z
    .locals 2

    .line 66
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->getBatteryMode()Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_CHARGING:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    if-eq v0, v1, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->getBatteryPercentage()I

    move-result p0

    const/16 v0, 0xf

    if-gt p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/BatteryLevel;
    .locals 1

    .line 9
    const-class v0, Lcom/squareup/cardreader/BatteryLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/BatteryLevel;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/BatteryLevel;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/cardreader/BatteryLevel;->$VALUES:[Lcom/squareup/cardreader/BatteryLevel;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/BatteryLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/BatteryLevel;

    return-object v0
.end method
