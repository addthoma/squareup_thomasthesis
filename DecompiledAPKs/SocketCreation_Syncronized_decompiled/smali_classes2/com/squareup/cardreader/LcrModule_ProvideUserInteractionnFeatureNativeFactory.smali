.class public final Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionnFeatureNativeFactory;
.super Ljava/lang/Object;
.source "LcrModule_ProvideUserInteractionnFeatureNativeFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionnFeatureNativeFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionnFeatureNativeFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionnFeatureNativeFactory$InstanceHolder;->access$000()Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionnFeatureNativeFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideUserInteractionnFeatureNative()Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/cardreader/LcrModule;->provideUserInteractionnFeatureNative()Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionnFeatureNativeFactory;->provideUserInteractionnFeatureNative()Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionnFeatureNativeFactory;->get()Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;

    move-result-object v0

    return-object v0
.end method
