.class Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;
.super Ljava/lang/Object;
.source "FirmwareUpdateDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FirmwareUpdateState"
.end annotation


# instance fields
.field private final assets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field private currentIndex:I

.field private final isBlocking:Z

.field private final readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;


# direct methods
.method private constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/bills/CardData$ReaderType;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;",
            ">;",
            "Lcom/squareup/protos/client/bills/CardData$ReaderType;",
            ")V"
        }
    .end annotation

    .line 452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 453
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->assets:Ljava/util/List;

    .line 454
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 p2, 0x0

    .line 455
    iput p2, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->currentIndex:I

    .line 458
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;

    .line 459
    iget-boolean v0, v0, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->blocking:Z

    if-eqz v0, :cond_0

    const/4 p2, 0x1

    .line 464
    :cond_1
    iput-boolean p2, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->isBlocking:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$1;)V
    .locals 0

    .line 446
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;-><init>(Ljava/util/List;Lcom/squareup/protos/client/bills/CardData$ReaderType;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;)Z
    .locals 0

    .line 446
    iget-boolean p0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->isBlocking:Z

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;I)I
    .locals 0

    .line 446
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->scale(I)I

    move-result p0

    return p0
.end method

.method static synthetic access$308(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;)I
    .locals 2

    .line 446
    iget v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->currentIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->currentIndex:I

    return v0
.end method

.method static synthetic access$400(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;)Lcom/squareup/protos/client/bills/CardData$ReaderType;
    .locals 0

    .line 446
    iget-object p0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0
.end method

.method private scale(I)I
    .locals 6

    .line 469
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->assets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;

    int-to-long v4, v2

    .line 470
    iget-wide v2, v3, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->size:J

    add-long/2addr v4, v2

    long-to-int v2, v4

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 475
    :goto_1
    iget v3, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->currentIndex:I

    if-ge v1, v3, :cond_1

    .line 476
    iget-object v3, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->assets:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;

    iget-wide v3, v3, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->size:J

    long-to-float v3, v3

    add-float/2addr v0, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    int-to-float p1, p1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr p1, v1

    .line 480
    iget-object v4, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->assets:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;

    iget-wide v3, v3, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->size:J

    long-to-float v3, v3

    mul-float p1, p1, v3

    add-float/2addr v0, p1

    mul-float v0, v0, v1

    int-to-float p1, v2

    div-float/2addr v0, p1

    float-to-int p1, v0

    return p1
.end method
