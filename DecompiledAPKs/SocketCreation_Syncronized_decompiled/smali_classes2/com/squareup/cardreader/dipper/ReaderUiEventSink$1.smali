.class synthetic Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;
.super Ljava/lang/Object;
.source "ReaderUiEventSink.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/dipper/ReaderUiEventSink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$cardreader$CardReader$AbortSecureSessionReason:[I

.field static final synthetic $SwitchMap$com$squareup$cardreader$lcr$CrCardreaderType:[I

.field static final synthetic $SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 591
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrCardreaderType;->values()[Lcom/squareup/cardreader/lcr/CrCardreaderType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$cardreader$lcr$CrCardreaderType:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$cardreader$lcr$CrCardreaderType:[I

    sget-object v2, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_R12:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    invoke-virtual {v2}, Lcom/squareup/cardreader/lcr/CrCardreaderType;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$cardreader$lcr$CrCardreaderType:[I

    sget-object v3, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_R12C:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    invoke-virtual {v3}, Lcom/squareup/cardreader/lcr/CrCardreaderType;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$cardreader$lcr$CrCardreaderType:[I

    sget-object v4, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_R6:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    invoke-virtual {v4}, Lcom/squareup/cardreader/lcr/CrCardreaderType;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v3, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$cardreader$lcr$CrCardreaderType:[I

    sget-object v4, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_X2:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    invoke-virtual {v4}, Lcom/squareup/cardreader/lcr/CrCardreaderType;->ordinal()I

    move-result v4

    const/4 v5, 0x4

    aput v5, v3, v4
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v3, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$cardreader$lcr$CrCardreaderType:[I

    sget-object v4, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_T2:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    invoke-virtual {v4}, Lcom/squareup/cardreader/lcr/CrCardreaderType;->ordinal()I

    move-result v4

    const/4 v5, 0x5

    aput v5, v3, v4
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v3, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$cardreader$lcr$CrCardreaderType:[I

    sget-object v4, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_T2B:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    invoke-virtual {v4}, Lcom/squareup/cardreader/lcr/CrCardreaderType;->ordinal()I

    move-result v4

    const/4 v5, 0x6

    aput v5, v3, v4
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    .line 380
    :catch_5
    invoke-static {}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->values()[Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    :try_start_6
    sget-object v3, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    sget-object v4, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v3, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    sget-object v4, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v4

    aput v1, v3, v4
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    .line 329
    :catch_7
    invoke-static {}, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->values()[Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$cardreader$CardReader$AbortSecureSessionReason:[I

    :try_start_8
    sget-object v3, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$cardreader$CardReader$AbortSecureSessionReason:[I

    sget-object v4, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->CLIENT_ERROR:Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    invoke-virtual {v4}, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    :try_start_9
    sget-object v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$cardreader$CardReader$AbortSecureSessionReason:[I

    sget-object v3, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->NETWORK_ERROR:Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    invoke-virtual {v3}, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->ordinal()I

    move-result v3

    aput v1, v0, v3
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$cardreader$CardReader$AbortSecureSessionReason:[I

    sget-object v1, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->SERVER_ERROR:Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    return-void
.end method
