.class public final Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;
.super Ljava/lang/Object;
.source "ReaderUiEventSink_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/dipper/ReaderUiEventSink;",
        ">;"
    }
.end annotation


# instance fields
.field private final accessibilityManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/view/accessibility/AccessibilityManager;",
            ">;"
        }
    .end annotation
.end field

.field private final activeCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final apiTransactionStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;"
        }
    .end annotation
.end field

.field private final badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final damagedReaderServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/DamagedReaderService;",
            ">;"
        }
    .end annotation
.end field

.field private final dipperEventHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/DipperEventHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final dipperUiErrorDisplayTypeSelectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final emvDipScreenHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final emvSwipePassthroughEnablerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final firmwareUpdateScreenHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final localCardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingDiverterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;"
        }
    .end annotation
.end field

.field private final r12TutorialLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/R12ForceableContentLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final r6VideoLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/R6ForceableContentLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final readerConnectionEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final readerHudConnectionEventHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final readerHudManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;"
        }
    .end annotation
.end field

.field private final readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;"
        }
    .end annotation
.end field

.field private final reportCoredumpServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/ReportCoredumpService;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final smartPaymentFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final speRestartCheckerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/squid/common/SpeRestartChecker;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBus;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/view/accessibility/AccessibilityManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/DipperEventHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/DamagedReaderService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/ReportCoredumpService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/R6ForceableContentLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/R12ForceableContentLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/squid/common/SpeRestartChecker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 131
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->accessibilityManagerProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 132
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 133
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 134
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->dipperEventHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 135
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->damagedReaderServiceProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 136
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 137
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 138
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->emvSwipePassthroughEnablerProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 139
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->swipeBusProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 140
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 141
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->firmwareUpdateScreenHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 142
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 143
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->readerHudManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 144
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->reportCoredumpServiceProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 145
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->resProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 146
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->readerHudConnectionEventHandlerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 147
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 148
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->localCardReaderListenersProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 149
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 150
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->r6VideoLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 151
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 152
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 153
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 154
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 155
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->r12TutorialLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 156
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->readerConnectionEventLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 157
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 158
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 159
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->speRestartCheckerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p30

    .line 160
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p31

    .line 161
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->dipperUiErrorDisplayTypeSelectorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;
    .locals 33
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/view/accessibility/AccessibilityManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/DipperEventHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/DamagedReaderService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/ReportCoredumpService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/R6ForceableContentLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/R12ForceableContentLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/squid/common/SpeRestartChecker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;",
            ">;)",
            "Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    .line 198
    new-instance v32, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;

    move-object/from16 v0, v32

    invoke-direct/range {v0 .. v31}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v32
.end method

.method public static newInstance(Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/DipperEventHandler;Lcom/squareup/server/DamagedReaderService;Ldagger/Lazy;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/wavpool/swipe/SwipeBus;Lcom/squareup/settings/server/Features;Ldagger/Lazy;Lcom/squareup/hudtoaster/HudToaster;Ldagger/Lazy;Lcom/squareup/server/ReportCoredumpService;Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/CardReaderListeners;Ldagger/Lazy;Lcom/squareup/ui/main/R6ForceableContentLauncher;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/onboarding/OnboardingDiverter;Ldagger/Lazy;Lcom/squareup/ui/main/R12ForceableContentLauncher;Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/cardreader/squid/common/SpeRestartChecker;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;)Lcom/squareup/cardreader/dipper/ReaderUiEventSink;
    .locals 33
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/accessibility/AccessibilityManager;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/cardreader/dipper/DipperEventHandler;",
            "Lcom/squareup/server/DamagedReaderService;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            "Lcom/squareup/wavpool/swipe/SwipeBus;",
            "Lcom/squareup/settings/server/Features;",
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;",
            ">;",
            "Lcom/squareup/hudtoaster/HudToaster;",
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Lcom/squareup/server/ReportCoredumpService;",
            "Lcom/squareup/util/Res;",
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;",
            ">;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Lcom/squareup/ui/main/R6ForceableContentLauncher;",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Lcom/squareup/ui/main/R12ForceableContentLauncher;",
            "Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;",
            "Lcom/squareup/api/ApiTransactionState;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lcom/squareup/cardreader/squid/common/SpeRestartChecker;",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            "Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;",
            ")",
            "Lcom/squareup/cardreader/dipper/ReaderUiEventSink;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    .line 220
    new-instance v32, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

    move-object/from16 v0, v32

    invoke-direct/range {v0 .. v31}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;-><init>(Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/DipperEventHandler;Lcom/squareup/server/DamagedReaderService;Ldagger/Lazy;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/wavpool/swipe/SwipeBus;Lcom/squareup/settings/server/Features;Ldagger/Lazy;Lcom/squareup/hudtoaster/HudToaster;Ldagger/Lazy;Lcom/squareup/server/ReportCoredumpService;Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/CardReaderListeners;Ldagger/Lazy;Lcom/squareup/ui/main/R6ForceableContentLauncher;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/onboarding/OnboardingDiverter;Ldagger/Lazy;Lcom/squareup/ui/main/R12ForceableContentLauncher;Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/cardreader/squid/common/SpeRestartChecker;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;)V

    return-object v32
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/dipper/ReaderUiEventSink;
    .locals 33

    move-object/from16 v0, p0

    .line 166
    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->accessibilityManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->dipperEventHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/cardreader/dipper/DipperEventHandler;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->damagedReaderServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/server/DamagedReaderService;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v7

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->emvSwipePassthroughEnablerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->swipeBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/wavpool/swipe/SwipeBus;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->firmwareUpdateScreenHandlerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v12

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->readerHudManagerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v14

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->reportCoredumpServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/server/ReportCoredumpService;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->readerHudConnectionEventHandlerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v17

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->localCardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v20

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->r6VideoLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/ui/main/R6ForceableContentLauncher;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/onboarding/OnboardingDiverter;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v25

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->r12TutorialLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/ui/main/R12ForceableContentLauncher;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->readerConnectionEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/api/ApiTransactionState;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->speRestartCheckerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v30, v1

    check-cast v30, Lcom/squareup/cardreader/squid/common/SpeRestartChecker;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v31, v1

    check-cast v31, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->dipperUiErrorDisplayTypeSelectorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v32, v1

    check-cast v32, Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;

    invoke-static/range {v2 .. v32}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->newInstance(Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/DipperEventHandler;Lcom/squareup/server/DamagedReaderService;Ldagger/Lazy;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/wavpool/swipe/SwipeBus;Lcom/squareup/settings/server/Features;Ldagger/Lazy;Lcom/squareup/hudtoaster/HudToaster;Ldagger/Lazy;Lcom/squareup/server/ReportCoredumpService;Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/CardReaderListeners;Ldagger/Lazy;Lcom/squareup/ui/main/R6ForceableContentLauncher;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/onboarding/OnboardingDiverter;Ldagger/Lazy;Lcom/squareup/ui/main/R12ForceableContentLauncher;Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/cardreader/squid/common/SpeRestartChecker;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;)Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink_Factory;->get()Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

    move-result-object v0

    return-object v0
.end method
