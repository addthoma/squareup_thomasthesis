.class public interface abstract Lcom/squareup/cardreader/SystemFeature;
.super Ljava/lang/Object;
.source "SystemFeature.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0012\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\u0008\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\nH&J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH&J\u0010\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\rH&J\u0010\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\nH&\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/cardreader/SystemFeature;",
        "",
        "onCapabilitiesReceived",
        "",
        "capabilitiesSupported",
        "",
        "capabilityBytes",
        "",
        "onChargeCycleCountReceived",
        "chargeCycleCount",
        "",
        "onFirmwareVersionReceived",
        "firmwareVersion",
        "",
        "onHardwareSerialNumberReceived",
        "hardwareSerialNumber",
        "onReaderErrorReceived",
        "readerErrorInt",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract onCapabilitiesReceived(Z[B)V
.end method

.method public abstract onChargeCycleCountReceived(I)V
.end method

.method public abstract onFirmwareVersionReceived(Ljava/lang/String;)V
.end method

.method public abstract onHardwareSerialNumberReceived(Ljava/lang/String;)V
.end method

.method public abstract onReaderErrorReceived(I)V
.end method
