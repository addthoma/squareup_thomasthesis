.class public Lcom/squareup/cardreader/PaymentCounter;
.super Ljava/lang/Object;
.source "PaymentCounter.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;
.implements Lmortar/Scoped;


# static fields
.field public static final HUD_EVERY_X_TRANSACTIONS:I = 0xa


# instance fields
.field private final cardReaderCounts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/cardreader/CardReaderId;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderHub;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/cardreader/PaymentCounter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 30
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/PaymentCounter;->cardReaderCounts:Ljava/util/Map;

    return-void
.end method

.method private reset(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentCounter;->cardReaderCounts:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public getCardReaderCount(Lcom/squareup/cardreader/CardReaderId;)I
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentCounter;->cardReaderCounts:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    .line 71
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/PaymentCounter;->reset(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentCounter;->cardReaderCounts:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 59
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentCounter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {p1, p0}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 61
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentCounter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReader;

    .line 62
    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/PaymentCounter;->onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentCounter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/CardReaderHub;->removeCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    return-void
.end method

.method public onPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentCounter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentCounter;->cardReaderCounts:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 39
    iget-object v1, p0, Lcom/squareup/cardreader/PaymentCounter;->cardReaderCounts:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public powerHudDisplayed(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/PaymentCounter;->reset(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method public setCardReaderCounts(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/cardreader/CardReaderId;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentCounter;->cardReaderCounts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 84
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentCounter;->cardReaderCounts:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method public shouldDisplayPowerHud(Lcom/squareup/cardreader/CardReaderId;)Z
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentCounter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isSmartReader()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/PaymentCounter;->cardReaderCounts:Ljava/util/Map;

    .line 48
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/16 v0, 0xa

    if-lt p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
