.class public final Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;
.super Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnPaymentComplete"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u001b\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u00011BE\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\t\u0010!\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0005H\u00c6\u0003J\t\u0010#\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\t\u0010%\u001a\u00020\u000bH\u00c6\u0003J\u0014\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0003\u00a2\u0006\u0002\u0010\u001dJ\t\u0010\'\u001a\u00020\u0010H\u00c6\u0003J\\\u0010(\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u000e\u0008\u0002\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u0010H\u00c6\u0001\u00a2\u0006\u0002\u0010)J\u0013\u0010*\u001a\u00020\u00072\u0008\u0010+\u001a\u0004\u0018\u00010,H\u00d6\u0003J\t\u0010-\u001a\u00020.H\u00d6\u0001J\t\u0010/\u001a\u000200H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0019\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\n\n\u0002\u0010\u001e\u001a\u0004\u0008\u001c\u0010\u001dR\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 \u00a8\u00062"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;",
        "data",
        "",
        "paymentResult",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;",
        "approvedOffline",
        "",
        "cardDescription",
        "Lcom/squareup/cardreader/CardDescription;",
        "stdMsg",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;",
        "paymentTimingsArray",
        "",
        "Lcom/squareup/cardreader/PaymentTiming;",
        "cardAction",
        "Lcom/squareup/cardreader/lcr/CrPaymentCardAction;",
        "([BLcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;ZLcom/squareup/cardreader/CardDescription;Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;[Lcom/squareup/cardreader/PaymentTiming;Lcom/squareup/cardreader/lcr/CrPaymentCardAction;)V",
        "getApprovedOffline",
        "()Z",
        "getCardAction",
        "()Lcom/squareup/cardreader/lcr/CrPaymentCardAction;",
        "getCardDescription",
        "()Lcom/squareup/cardreader/CardDescription;",
        "getData",
        "()[B",
        "getPaymentResult",
        "()Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;",
        "getPaymentTimingsArray",
        "()[Lcom/squareup/cardreader/PaymentTiming;",
        "[Lcom/squareup/cardreader/PaymentTiming;",
        "getStdMsg",
        "()Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "([BLcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;ZLcom/squareup/cardreader/CardDescription;Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;[Lcom/squareup/cardreader/PaymentTiming;Lcom/squareup/cardreader/lcr/CrPaymentCardAction;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "PaymentResult",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final approvedOffline:Z

.field private final cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field private final cardDescription:Lcom/squareup/cardreader/CardDescription;

.field private final data:[B

.field private final paymentResult:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

.field private final paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

.field private final stdMsg:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;


# direct methods
.method public constructor <init>([BLcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;ZLcom/squareup/cardreader/CardDescription;Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;[Lcom/squareup/cardreader/PaymentTiming;Lcom/squareup/cardreader/lcr/CrPaymentCardAction;)V
    .locals 1

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentResult"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stdMsg"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentTimingsArray"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardAction"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 366
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->data:[B

    iput-object p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->paymentResult:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    iput-boolean p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->approvedOffline:Z

    iput-object p4, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->cardDescription:Lcom/squareup/cardreader/CardDescription;

    iput-object p5, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->stdMsg:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    iput-object p6, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

    iput-object p7, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;[BLcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;ZLcom/squareup/cardreader/CardDescription;Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;[Lcom/squareup/cardreader/PaymentTiming;Lcom/squareup/cardreader/lcr/CrPaymentCardAction;ILjava/lang/Object;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->data:[B

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->paymentResult:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->approvedOffline:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->cardDescription:Lcom/squareup/cardreader/CardDescription;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->stdMsg:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->copy([BLcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;ZLcom/squareup/cardreader/CardDescription;Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;[Lcom/squareup/cardreader/PaymentTiming;Lcom/squareup/cardreader/lcr/CrPaymentCardAction;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()[B
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->data:[B

    return-object v0
.end method

.method public final component2()Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->paymentResult:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->approvedOffline:Z

    return v0
.end method

.method public final component4()Lcom/squareup/cardreader/CardDescription;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->cardDescription:Lcom/squareup/cardreader/CardDescription;

    return-object v0
.end method

.method public final component5()Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->stdMsg:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    return-object v0
.end method

.method public final component6()[Lcom/squareup/cardreader/PaymentTiming;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

    return-object v0
.end method

.method public final component7()Lcom/squareup/cardreader/lcr/CrPaymentCardAction;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    return-object v0
.end method

.method public final copy([BLcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;ZLcom/squareup/cardreader/CardDescription;Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;[Lcom/squareup/cardreader/PaymentTiming;Lcom/squareup/cardreader/lcr/CrPaymentCardAction;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;
    .locals 9

    const-string v0, "data"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentResult"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stdMsg"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentTimingsArray"

    move-object v7, p6

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardAction"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;

    move-object v1, v0

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v8}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;-><init>([BLcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;ZLcom/squareup/cardreader/CardDescription;Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;[Lcom/squareup/cardreader/PaymentTiming;Lcom/squareup/cardreader/lcr/CrPaymentCardAction;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->data:[B

    iget-object v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->data:[B

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->paymentResult:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    iget-object v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->paymentResult:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->approvedOffline:Z

    iget-boolean v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->approvedOffline:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->cardDescription:Lcom/squareup/cardreader/CardDescription;

    iget-object v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->cardDescription:Lcom/squareup/cardreader/CardDescription;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->stdMsg:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    iget-object v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->stdMsg:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

    iget-object v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    iget-object p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getApprovedOffline()Z
    .locals 1

    .line 361
    iget-boolean v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->approvedOffline:Z

    return v0
.end method

.method public final getCardAction()Lcom/squareup/cardreader/lcr/CrPaymentCardAction;
    .locals 1

    .line 365
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    return-object v0
.end method

.method public final getCardDescription()Lcom/squareup/cardreader/CardDescription;
    .locals 1

    .line 362
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->cardDescription:Lcom/squareup/cardreader/CardDescription;

    return-object v0
.end method

.method public final getData()[B
    .locals 1

    .line 359
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->data:[B

    return-object v0
.end method

.method public final getPaymentResult()Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->paymentResult:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    return-object v0
.end method

.method public final getPaymentTimingsArray()[Lcom/squareup/cardreader/PaymentTiming;
    .locals 1

    .line 364
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

    return-object v0
.end method

.method public final getStdMsg()Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;
    .locals 1

    .line 363
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->stdMsg:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->data:[B

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->paymentResult:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->approvedOffline:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->cardDescription:Lcom/squareup/cardreader/CardDescription;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->stdMsg:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

    if-eqz v2, :cond_5

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnPaymentComplete(data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->data:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->paymentResult:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", approvedOffline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->approvedOffline:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", cardDescription="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->cardDescription:Lcom/squareup/cardreader/CardDescription;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stdMsg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->stdMsg:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentTimingsArray="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", cardAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;->cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
