.class public interface abstract Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;
.super Ljava/lang/Object;
.source "EmvDipScreenHandler.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0008H&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0008H&J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u000cH&J\u0018\u0010\r\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0010\u001a\u00020\u000fH&\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
        "",
        "addEmvCardInsertRemoveProcessor",
        "",
        "emvCardInsertRemoveProcessor",
        "Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;",
        "processEmvCardInserted",
        "cardReaderInfo",
        "Lcom/squareup/cardreader/CardReaderInfo;",
        "processEmvCardRemoved",
        "registerDefaultEmvCardInsertRemoveProcessor",
        "scope",
        "Lmortar/MortarScope;",
        "registerEmvCardInsertRemoveProcessor",
        "removeEmvCardInsertRemoveProcessor",
        "",
        "shouldDisableEmvDips",
        "cardreader-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract addEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V
.end method

.method public abstract processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract registerDefaultEmvCardInsertRemoveProcessor(Lmortar/MortarScope;)V
.end method

.method public abstract registerEmvCardInsertRemoveProcessor(Lmortar/MortarScope;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V
.end method

.method public abstract removeEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)Z
.end method

.method public abstract shouldDisableEmvDips()Z
.end method
