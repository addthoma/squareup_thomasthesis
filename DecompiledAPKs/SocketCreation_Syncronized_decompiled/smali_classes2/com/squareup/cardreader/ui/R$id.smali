.class public final Lcom/squareup/cardreader/ui/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final battery_image:I = 0x7f0a0220

.field public static final battery_message:I = 0x7f0a0221

.field public static final ble_actions:I = 0x7f0a0238

.field public static final cancel_button:I = 0x7f0a0299

.field public static final card_reader_details_help_message_row:I = 0x7f0a02c7

.field public static final connect_reader_button:I = 0x7f0a0390

.field public static final content:I = 0x7f0a03a3

.field public static final device_contactless_help:I = 0x7f0a05b1

.field public static final forget_reader_button:I = 0x7f0a076e

.field public static final identify_reader_button:I = 0x7f0a081e

.field public static final pairing_header_content:I = 0x7f0a0b8f

.field public static final pairing_help_devices:I = 0x7f0a0b90

.field public static final pairing_help_support:I = 0x7f0a0b91

.field public static final pairing_help_video:I = 0x7f0a0b92

.field public static final pairing_list:I = 0x7f0a0b93

.field public static final pairing_retrying_help:I = 0x7f0a0b94

.field public static final pairing_screen_content_container:I = 0x7f0a0b95

.field public static final pairing_screen_content_help:I = 0x7f0a0b96

.field public static final pairing_screen_help:I = 0x7f0a0b97

.field public static final pairing_screen_reader_r12:I = 0x7f0a0b98

.field public static final pairing_screen_reference:I = 0x7f0a0b99

.field public static final permission_enable:I = 0x7f0a0c17

.field public static final permission_explanation:I = 0x7f0a0c18

.field public static final permission_glyph:I = 0x7f0a0c19

.field public static final reader_accepts_row:I = 0x7f0a0ce5

.field public static final reader_battery_glyph:I = 0x7f0a0ce6

.field public static final reader_battery_text:I = 0x7f0a0ce7

.field public static final reader_connection_row:I = 0x7f0a0ce8

.field public static final reader_detail_name_row:I = 0x7f0a0ce9

.field public static final reader_detail_name_row_border:I = 0x7f0a0cea

.field public static final reader_detail_name_row_helper:I = 0x7f0a0ceb

.field public static final reader_detail_name_row_title:I = 0x7f0a0cec

.field public static final reader_firmware_row:I = 0x7f0a0ced

.field public static final reader_list:I = 0x7f0a0cee

.field public static final reader_serial_number_row:I = 0x7f0a0cf2

.field public static final reader_status_advice:I = 0x7f0a0cf3

.field public static final reader_status_headline:I = 0x7f0a0cf4

.field public static final reader_status_row:I = 0x7f0a0cf5

.field public static final reader_warning_bottom_default_button:I = 0x7f0a0cf6

.field public static final reader_warning_glyph_text:I = 0x7f0a0cf7

.field public static final reader_warning_important_message:I = 0x7f0a0cf8

.field public static final reader_warning_top_alternative_button:I = 0x7f0a0cf9


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
