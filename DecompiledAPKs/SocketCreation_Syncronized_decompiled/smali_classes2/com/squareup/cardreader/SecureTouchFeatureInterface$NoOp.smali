.class public Lcom/squareup/cardreader/SecureTouchFeatureInterface$NoOp;
.super Ljava/lang/Object;
.source "SecureTouchFeatureInterface.java"

# interfaces
.implements Lcom/squareup/cardreader/SecureTouchFeatureInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/SecureTouchFeatureInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NoOp"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public initialize(Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;)V
    .locals 0

    return-void
.end method

.method public onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
    .locals 1

    .line 23
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Secure touch feature should not be used in POS"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public reset()V
    .locals 0

    return-void
.end method
