.class public interface abstract Lcom/squareup/cardreader/FirmwareUpdater$Listener;
.super Ljava/lang/Object;
.source "FirmwareUpdater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/FirmwareUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onFirmwareUpdateAssetSuccess(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;)V
.end method

.method public abstract onFirmwareUpdateComplete(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onFirmwareUpdateError(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V
.end method

.method public abstract onFirmwareUpdateProgress(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;I)V
.end method

.method public abstract onFirmwareUpdateStarted(Lcom/squareup/cardreader/CardReaderInfo;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract sendFirmwareManifestToServer(Lcom/squareup/cardreader/CardReader;[BLcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
.end method
