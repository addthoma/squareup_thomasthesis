.class public Lcom/squareup/cardreader/CardReaderPauseAndResumer;
.super Ljava/lang/Object;
.source "CardReaderPauseAndResumer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardReaderPauseAndResumer$StartsAndStops;,
        Lcom/squareup/cardreader/CardReaderPauseAndResumer$Running;
    }
.end annotation


# instance fields
.field private expectConfigurationChange:Z

.field private isRunning:Z

.field private final startsAndResumes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer$StartsAndStops;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->startsAndResumes:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public addStartsAndStops(Lcom/squareup/cardreader/CardReaderPauseAndResumer$StartsAndStops;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->startsAndResumes:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 37
    iget-boolean v0, p0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->isRunning:Z

    if-eqz v0, :cond_0

    .line 38
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderPauseAndResumer$StartsAndStops;->onStart()V

    goto :goto_0

    .line 40
    :cond_0
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderPauseAndResumer$StartsAndStops;->onStop()V

    :goto_0
    return-void
.end method

.method public isRunning()Z
    .locals 1

    .line 73
    iget-boolean v0, p0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->isRunning:Z

    return v0
.end method

.method public onPause(Z)V
    .locals 2

    const/4 v0, 0x0

    .line 61
    iput-boolean v0, p0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->isRunning:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 63
    iput-boolean p1, p0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->expectConfigurationChange:Z

    goto :goto_1

    .line 65
    :cond_0
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->startsAndResumes:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderPauseAndResumer$StartsAndStops;

    .line 66
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReaderPauseAndResumer$StartsAndStops;->onStop()V

    goto :goto_0

    :cond_1
    new-array p1, v0, [Ljava/lang/Object;

    const-string v0, "CardReaderPauseAndResumer#onPause()"

    .line 68
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method public onResume()V
    .locals 3

    const/4 v0, 0x1

    .line 49
    iput-boolean v0, p0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->isRunning:Z

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "CardReaderPauseAndResumer#onResume()"

    .line 50
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 52
    iget-boolean v1, p0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->expectConfigurationChange:Z

    if-nez v1, :cond_0

    .line 53
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->startsAndResumes:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReaderPauseAndResumer$StartsAndStops;

    .line 54
    invoke-interface {v2}, Lcom/squareup/cardreader/CardReaderPauseAndResumer$StartsAndStops;->onStart()V

    goto :goto_0

    .line 57
    :cond_0
    iput-boolean v0, p0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->expectConfigurationChange:Z

    return-void
.end method

.method public removeStartsAndStops(Lcom/squareup/cardreader/CardReaderPauseAndResumer$StartsAndStops;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->startsAndResumes:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method
