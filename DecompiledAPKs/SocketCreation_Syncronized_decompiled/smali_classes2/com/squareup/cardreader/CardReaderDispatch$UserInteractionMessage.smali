.class public Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;
.super Ljava/lang/Object;
.source "CardReaderDispatch.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderDispatch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserInteractionMessage"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final lines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 400
    new-instance v0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$1;

    invoke-direct {v0}, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$1;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->title:Ljava/lang/String;

    .line 397
    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->lines:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/cardreader/CardReaderDispatch$1;)V
    .locals 0

    .line 390
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method private static addLines([Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;
    .locals 4

    .line 466
    new-instance v0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;-><init>()V

    .line 467
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p0, v2

    .line 468
    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;->addLine(Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static buildMessage(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;
    .locals 0

    .line 458
    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->addLines([Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;->title(Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;->build()Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

    move-result-object p0

    return-object p0
.end method

.method public static buildMessage([Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;
    .locals 0

    .line 462
    invoke-static {p0}, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->addLines([Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;->build()Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-eqz p1, :cond_4

    .line 413
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_1

    goto :goto_1

    .line 415
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

    .line 418
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->title:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v2, p1, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    :cond_2
    iget-object v1, p1, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->title:Ljava/lang/String;

    if-eqz v1, :cond_3

    :goto_0
    return v0

    .line 419
    :cond_3
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->lines:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->lines:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_4
    :goto_1
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 423
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->title:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 424
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->lines:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 433
    iget-object p2, p0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 434
    iget-object p2, p0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->lines:Ljava/util/List;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    return-void
.end method
