.class public interface abstract Lcom/squareup/cardreader/PaymentFeature;
.super Ljava/lang/Object;
.source "PaymentFeature.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0015\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u000c\u0008f\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH&J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u000cH&J\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000cH&J\u0018\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u000c2\u0006\u0010\u0011\u001a\u00020\u000cH&J \u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u0014H&J\u0010\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u0019H&J \u0010\u001a\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u00072\u0006\u0010\u001c\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u0019H&J \u0010\u001d\u001a\u00020\u00032\u0006\u0010\u001e\u001a\u00020\u000c2\u0006\u0010\u001f\u001a\u00020\t2\u0006\u0010 \u001a\u00020\tH&J \u0010!\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u00072\u0006\u0010\u001c\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u0019H&J\u001b\u0010\"\u001a\u00020\u00032\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u00020%0$H&\u00a2\u0006\u0002\u0010&JM\u0010\'\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u00072\u0006\u0010(\u001a\u00020\u000c2\u0006\u0010)\u001a\u00020\u00142\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u00192\u0006\u0010\u0011\u001a\u00020\u000c2\u000c\u0010*\u001a\u0008\u0012\u0004\u0012\u00020+0$2\u0006\u0010\u0010\u001a\u00020\u000cH&\u00a2\u0006\u0002\u0010,J\u0018\u0010-\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u00072\u0006\u0010\u0018\u001a\u00020\u0019H&J\u0010\u0010.\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u0007H&J\u0018\u0010/\u001a\u00020\u00032\u0006\u00100\u001a\u00020\t2\u0006\u0010\u001b\u001a\u00020\u0007H&J#\u00101\u001a\u00020\u00032\u0006\u00102\u001a\u00020\u000c2\u000c\u0010*\u001a\u0008\u0012\u0004\u0012\u00020+0$H&\u00a2\u0006\u0002\u00103J \u00104\u001a\u00020\u00032\u0006\u00105\u001a\u00020\u000c2\u0006\u0010\u001f\u001a\u00020\u000c2\u0006\u00106\u001a\u00020\u0007H&\u00a8\u00067"
    }
    d2 = {
        "Lcom/squareup/cardreader/PaymentFeature;",
        "",
        "onAccountSelectionRequired",
        "",
        "accounts",
        "",
        "languageBytes",
        "",
        "applicationId",
        "",
        "onAudioRequest",
        "audioMessageCode",
        "",
        "onAudioVisualRequest",
        "audioVisualId",
        "onCardActionRequired",
        "cardActionCode",
        "stdMsgCode",
        "onCardPresenceChanged",
        "present",
        "",
        "willContinuePayment",
        "presenceWasPreviouslyUnknown",
        "onCardholderNameReceived",
        "cardInfo",
        "Lcom/squareup/cardreader/CardInfo;",
        "onContactlessEmvAuthRequest",
        "data",
        "cardPresenceRequired",
        "onDisplayRequest",
        "displayMessageCode",
        "amount",
        "balance",
        "onEmvAuthRequest",
        "onListApplications",
        "applications",
        "",
        "Lcom/squareup/cardreader/EmvApplication;",
        "([Lcom/squareup/cardreader/EmvApplication;)V",
        "onPaymentComplete",
        "paymentResultCode",
        "approvedOffline",
        "paymentTimingsArray",
        "Lcom/squareup/cardreader/PaymentTiming;",
        "([BIZLcom/squareup/cardreader/CardInfo;I[Lcom/squareup/cardreader/PaymentTiming;I)V",
        "onSwipePassthrough",
        "onTmnAuthRequest",
        "onTmnDataToTmn",
        "transactionId",
        "onTmnTransactionComplete",
        "tmnTransactionResultCode",
        "(I[Lcom/squareup/cardreader/PaymentTiming;)V",
        "onTmnWriteNotify",
        "balanceBefore",
        "miryoData",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract onAccountSelectionRequired([I[BLjava/lang/String;)V
.end method

.method public abstract onAudioRequest(I)V
.end method

.method public abstract onAudioVisualRequest(I)V
.end method

.method public abstract onCardActionRequired(II)V
.end method

.method public abstract onCardPresenceChanged(ZZZ)V
.end method

.method public abstract onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V
.end method

.method public abstract onContactlessEmvAuthRequest([BZLcom/squareup/cardreader/CardInfo;)V
.end method

.method public abstract onDisplayRequest(ILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onEmvAuthRequest([BZLcom/squareup/cardreader/CardInfo;)V
.end method

.method public abstract onListApplications([Lcom/squareup/cardreader/EmvApplication;)V
.end method

.method public abstract onPaymentComplete([BIZLcom/squareup/cardreader/CardInfo;I[Lcom/squareup/cardreader/PaymentTiming;I)V
.end method

.method public abstract onSwipePassthrough([BLcom/squareup/cardreader/CardInfo;)V
.end method

.method public abstract onTmnAuthRequest([B)V
.end method

.method public abstract onTmnDataToTmn(Ljava/lang/String;[B)V
.end method

.method public abstract onTmnTransactionComplete(I[Lcom/squareup/cardreader/PaymentTiming;)V
.end method

.method public abstract onTmnWriteNotify(II[B)V
.end method
