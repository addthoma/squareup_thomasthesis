.class public Lcom/squareup/cardreader/WirelessConnection$Factory;
.super Ljava/lang/Object;
.source "WirelessConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/WirelessConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static forBluetoothDevice(Landroid/bluetooth/BluetoothDevice;)Lcom/squareup/cardreader/WirelessConnection;
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/cardreader/WirelessConnection$Factory$1;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/WirelessConnection$Factory$1;-><init>(Landroid/bluetooth/BluetoothDevice;)V

    return-object v0
.end method
