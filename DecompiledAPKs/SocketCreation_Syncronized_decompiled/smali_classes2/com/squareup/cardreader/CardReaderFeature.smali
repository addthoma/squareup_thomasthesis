.class public interface abstract Lcom/squareup/cardreader/CardReaderFeature;
.super Ljava/lang/Object;
.source "CardReaderFeature.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\n\n\u0002\u0010\u000e\n\u0000\u0008f\u0018\u00002\u00020\u0001J(\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00052\u0006\u0010\u0008\u001a\u00020\u0005H&J\u0008\u0010\t\u001a\u00020\u0003H&J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u0005H&J\u0008\u0010\u000c\u001a\u00020\u0003H&J\u0018\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u0010H&\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/cardreader/CardReaderFeature;",
        "",
        "onCommsVersionAcquired",
        "",
        "resultValue",
        "",
        "transportVersion",
        "appVersion",
        "endpointVersion",
        "onReaderError",
        "onReaderReady",
        "readerType",
        "onRpcCallbackRecvd",
        "reportError",
        "endPoint",
        "message",
        "",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract onCommsVersionAcquired(IIII)V
.end method

.method public abstract onReaderError()V
.end method

.method public abstract onReaderReady(I)V
.end method

.method public abstract onRpcCallbackRecvd()V
.end method

.method public abstract reportError(ILjava/lang/String;)V
.end method
