.class Lcom/squareup/cardreader/CardReaderInitializer$CoreDumpRunner;
.super Ljava/lang/Object;
.source "CardReaderInitializer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderInitializer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CoreDumpRunner"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/CardReaderInitializer;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/CardReaderInitializer;)V
    .locals 0

    .line 511
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$CoreDumpRunner;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 513
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$CoreDumpRunner;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$000(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    sget-object v2, Lcom/squareup/cardreader/CardReaderEventName;->REQUEST_CORE_DUMP:Lcom/squareup/cardreader/CardReaderEventName;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/CardReaderInitializer;->access$400(Lcom/squareup/cardreader/CardReaderInitializer;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderEventName;)V

    .line 514
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$CoreDumpRunner;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$600(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->requestCoreDump()V

    return-void
.end method
