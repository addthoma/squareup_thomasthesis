.class public interface abstract Lcom/squareup/cardreader/CardReaderContextParent;
.super Ljava/lang/Object;
.source "CardReaderContextParent.java"


# virtual methods
.method public abstract application()Landroid/app/Application;
.end method

.method public abstract cardReaderFactory()Lcom/squareup/cardreader/CardReaderFactory;
.end method

.method public abstract cardReaderListeners()Lcom/squareup/cardreader/CardReaderListeners;
.end method

.method public abstract crashnado()Lcom/squareup/crashnado/Crashnado;
.end method

.method public abstract isReaderSdk()Z
    .annotation runtime Lcom/squareup/cardreader/dagger/IsReaderSdkAppForDipper;
    .end annotation
.end method

.method public abstract lcrExecutor()Ljava/util/concurrent/ExecutorService;
.end method

.method public abstract magSwipeFailureFilter()Lcom/squareup/cardreader/MagSwipeFailureFilter;
.end method

.method public abstract mainThread()Lcom/squareup/thread/executor/MainThread;
.end method

.method public abstract nativeLoggingEnabled()Z
    .annotation runtime Lcom/squareup/cardreader/NativeLoggingEnabled;
    .end annotation
.end method

.method public abstract provideNativeBinaries()Lcom/squareup/cardreader/NativeBinaries;
.end method

.method public abstract provideSquidInterfaceScheduler()Lio/reactivex/Scheduler;
.end method

.method public abstract provideTmnTimings()Lcom/squareup/tmn/TmnTimings;
.end method

.method public abstract realCardReaderListeners()Lcom/squareup/cardreader/RealCardReaderListeners;
.end method

.method public abstract touchReporting()Lcom/squareup/securetouch/TouchReporting;
.end method
