.class public Lcom/squareup/cardreader/lcr/SystemFeatureNativePassthrough;
.super Ljava/lang/Object;
.source "SystemFeatureNativePassthrough.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cr_system_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;
    .locals 0

    .line 15
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->cr_system_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_system_mark_feature_flags_ready_to_send(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;
    .locals 0

    .line 37
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->cr_system_mark_feature_flags_ready_to_send(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_system_read_system_info(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;
    .locals 0

    .line 20
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->cr_system_read_system_info(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_system_send_external_charging_state(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Z)Lcom/squareup/cardreader/lcr/CrSystemResult;
    .locals 0

    .line 32
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->cr_system_send_external_charging_state(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Z)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_system_set_hardware_platform_feature(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;)Lcom/squareup/cardreader/lcr/CrSystemResult;
    .locals 0

    .line 26
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->cr_system_set_hardware_platform_feature(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_system_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;
    .locals 0

    .line 10
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->cr_system_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p1

    return-object p1
.end method

.method public system_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;
    .locals 0

    .line 43
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->system_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    move-result-object p1

    return-object p1
.end method

.method public system_set_reader_feature_flag(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Ljava/lang/String;S)Lcom/squareup/cardreader/lcr/CrSystemResult;
    .locals 0

    .line 49
    invoke-static {p1, p2, p3}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->system_set_reader_feature_flag(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Ljava/lang/String;S)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p1

    return-object p1
.end method
