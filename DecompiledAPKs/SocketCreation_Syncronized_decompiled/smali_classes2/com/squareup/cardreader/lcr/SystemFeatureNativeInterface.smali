.class public interface abstract Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;
.super Ljava/lang/Object;
.source "SystemFeatureNativeInterface.java"


# virtual methods
.method public abstract cr_system_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;
.end method

.method public abstract cr_system_mark_feature_flags_ready_to_send(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;
.end method

.method public abstract cr_system_read_system_info(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;
.end method

.method public abstract cr_system_send_external_charging_state(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Z)Lcom/squareup/cardreader/lcr/CrSystemResult;
.end method

.method public abstract cr_system_set_hardware_platform_feature(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;)Lcom/squareup/cardreader/lcr/CrSystemResult;
.end method

.method public abstract cr_system_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;
.end method

.method public abstract system_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;
.end method

.method public abstract system_set_reader_feature_flag(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Ljava/lang/String;S)Lcom/squareup/cardreader/lcr/CrSystemResult;
.end method
