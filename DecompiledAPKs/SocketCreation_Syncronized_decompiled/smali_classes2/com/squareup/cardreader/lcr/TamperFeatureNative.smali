.class public Lcom/squareup/cardreader/lcr/TamperFeatureNative;
.super Ljava/lang/Object;
.source "TamperFeatureNative.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cr_tamper_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
    .locals 2

    .line 21
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/TamperFeatureNativeJNI;->cr_tamper_free(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrTamperResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrTamperResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_tamper_get_tamper_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
    .locals 2

    .line 29
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/TamperFeatureNativeJNI;->cr_tamper_get_tamper_data(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrTamperResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrTamperResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_tamper_get_tamper_status(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
    .locals 2

    .line 25
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/TamperFeatureNativeJNI;->cr_tamper_get_tamper_status(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrTamperResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrTamperResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_tamper_init(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_event_api_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
    .locals 6

    .line 13
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)J

    move-result-wide v0

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)J

    move-result-wide v2

    invoke-static {p2}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_event_api_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_event_api_t;)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/squareup/cardreader/lcr/TamperFeatureNativeJNI;->cr_tamper_init(JJJ)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrTamperResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrTamperResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_tamper_reset_tag(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
    .locals 2

    .line 33
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/TamperFeatureNativeJNI;->cr_tamper_reset_tag(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrTamperResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrTamperResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_tamper_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
    .locals 2

    .line 17
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/TamperFeatureNativeJNI;->cr_tamper_term(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrTamperResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrTamperResult;

    move-result-object p0

    return-object p0
.end method

.method public static tamper_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;
    .locals 3

    .line 37
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/TamperFeatureNativeJNI;->tamper_initialize(JLjava/lang/Object;)J

    move-result-wide p0

    const-wide/16 v0, 0x0

    cmp-long v2, p0, v0

    if-nez v2, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 38
    :cond_0
    new-instance v0, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;-><init>(JZ)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method
