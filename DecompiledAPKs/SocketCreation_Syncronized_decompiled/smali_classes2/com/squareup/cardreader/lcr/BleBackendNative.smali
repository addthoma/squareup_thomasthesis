.class public Lcom/squareup/cardreader/lcr/BleBackendNative;
.super Ljava/lang/Object;
.source "BleBackendNative.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ble_received_data_from_characteristic_ack_vector(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;I)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;
    .locals 3

    .line 34
    new-instance v0, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)J

    move-result-wide v1

    invoke-static {v1, v2, p1}, Lcom/squareup/cardreader/lcr/BleBackendNativeJNI;->ble_received_data_from_characteristic_ack_vector(JI)J

    move-result-wide p0

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;-><init>(JZ)V

    return-object v0
.end method

.method public static ble_received_data_from_characteristic_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;[B)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;
    .locals 3

    .line 30
    new-instance v0, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)J

    move-result-wide v1

    invoke-static {v1, v2, p1}, Lcom/squareup/cardreader/lcr/BleBackendNativeJNI;->ble_received_data_from_characteristic_data(J[B)J

    move-result-wide p0

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;-><init>(JZ)V

    return-object v0
.end method

.method public static ble_received_data_from_characteristic_mtu(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;I)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;
    .locals 3

    .line 38
    new-instance v0, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)J

    move-result-wide v1

    invoke-static {v1, v2, p1}, Lcom/squareup/cardreader/lcr/BleBackendNativeJNI;->ble_received_data_from_characteristic_mtu(JI)J

    move-result-wide p0

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;-><init>(JZ)V

    return-object v0
.end method

.method public static cr_comms_backend_ble_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;
    .locals 5

    .line 13
    invoke-static {}, Lcom/squareup/cardreader/lcr/BleBackendNativeJNI;->cr_comms_backend_ble_alloc()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 14
    :cond_0
    new-instance v2, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;-><init>(JZ)V

    move-object v0, v2

    :goto_0
    return-object v0
.end method

.method public static cr_comms_backend_ble_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;
    .locals 3

    .line 18
    new-instance v0, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/squareup/cardreader/lcr/BleBackendNativeJNI;->cr_comms_backend_ble_free(J)J

    move-result-wide v1

    const/4 p0, 0x1

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;-><init>(JZ)V

    return-object v0
.end method

.method public static cr_comms_backend_ble_shutdown(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)V
    .locals 2

    .line 22
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/BleBackendNativeJNI;->cr_comms_backend_ble_shutdown(J)V

    return-void
.end method

.method public static initialize_backend_ble(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;[BLjava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;
    .locals 3

    .line 26
    new-instance v0, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)J

    move-result-wide v1

    invoke-static {v1, v2, p1, p2}, Lcom/squareup/cardreader/lcr/BleBackendNativeJNI;->initialize_backend_ble(J[BLjava/lang/Object;)J

    move-result-wide p0

    const/4 p2, 0x1

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;-><init>(JZ)V

    return-object v0
.end method
