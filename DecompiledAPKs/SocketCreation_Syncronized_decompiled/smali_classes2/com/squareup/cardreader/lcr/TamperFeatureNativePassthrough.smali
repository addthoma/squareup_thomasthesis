.class public Lcom/squareup/cardreader/lcr/TamperFeatureNativePassthrough;
.super Ljava/lang/Object;
.source "TamperFeatureNativePassthrough.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cr_tamper_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
    .locals 0

    .line 20
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/TamperFeatureNative;->cr_tamper_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_tamper_get_tamper_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
    .locals 0

    .line 30
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/TamperFeatureNative;->cr_tamper_get_tamper_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_tamper_get_tamper_status(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
    .locals 0

    .line 25
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/TamperFeatureNative;->cr_tamper_get_tamper_status(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_tamper_init(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_event_api_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
    .locals 0

    .line 10
    invoke-static {p1, p2, p3}, Lcom/squareup/cardreader/lcr/TamperFeatureNative;->cr_tamper_init(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_event_api_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_tamper_reset_tag(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
    .locals 0

    .line 35
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/TamperFeatureNative;->cr_tamper_reset_tag(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_tamper_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
    .locals 0

    .line 15
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/TamperFeatureNative;->cr_tamper_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;

    move-result-object p1

    return-object p1
.end method

.method public tamper_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;
    .locals 0

    .line 41
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/TamperFeatureNative;->tamper_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    move-result-object p1

    return-object p1
.end method
