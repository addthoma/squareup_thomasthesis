.class public interface abstract Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;
.super Ljava/lang/Object;
.source "PowerFeatureNativeInterface.java"


# virtual methods
.method public abstract cr_power_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;
.end method

.method public abstract cr_power_get_battery_voltage(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;
.end method

.method public abstract cr_power_off(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;
.end method

.method public abstract cr_power_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;
.end method

.method public abstract power_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;
.end method
