.class public final enum Lcom/squareup/cardreader/lcr/CrAudioVisualId;
.super Ljava/lang/Enum;
.source "CrAudioVisualId.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/CrAudioVisualId$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/CrAudioVisualId;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/CrAudioVisualId;

.field public static final enum CR_AUDIO_VISUAL_AMEX_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

.field public static final enum CR_AUDIO_VISUAL_BUZZER_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

.field public static final enum CR_AUDIO_VISUAL_CUP_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

.field public static final enum CR_AUDIO_VISUAL_DISCOVER_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

.field public static final enum CR_AUDIO_VISUAL_EFTPOS_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

.field public static final enum CR_AUDIO_VISUAL_INTERAC_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

.field public static final enum CR_AUDIO_VISUAL_JCB_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

.field public static final enum CR_AUDIO_VISUAL_MASTERCARD_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

.field public static final enum CR_AUDIO_VISUAL_VISA_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "CR_AUDIO_VISUAL_BUZZER_SUCCESS"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/cardreader/lcr/CrAudioVisualId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_BUZZER_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    const/4 v3, 0x2

    const-string v4, "CR_AUDIO_VISUAL_MASTERCARD_SUCCESS"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/cardreader/lcr/CrAudioVisualId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_MASTERCARD_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    const/4 v4, 0x3

    const-string v5, "CR_AUDIO_VISUAL_VISA_SUCCESS"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/cardreader/lcr/CrAudioVisualId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_VISA_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    const/4 v5, 0x4

    const-string v6, "CR_AUDIO_VISUAL_AMEX_SUCCESS"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/cardreader/lcr/CrAudioVisualId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_AMEX_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    const/4 v6, 0x5

    const-string v7, "CR_AUDIO_VISUAL_JCB_SUCCESS"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/cardreader/lcr/CrAudioVisualId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_JCB_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    const/4 v7, 0x6

    const-string v8, "CR_AUDIO_VISUAL_DISCOVER_SUCCESS"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/cardreader/lcr/CrAudioVisualId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_DISCOVER_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    const/4 v8, 0x7

    const-string v9, "CR_AUDIO_VISUAL_CUP_SUCCESS"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/cardreader/lcr/CrAudioVisualId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_CUP_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    const-string v9, "CR_AUDIO_VISUAL_INTERAC_SUCCESS"

    const/16 v10, 0x1a

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/cardreader/lcr/CrAudioVisualId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_INTERAC_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    .line 20
    new-instance v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    const/16 v9, 0x8

    const-string v10, "CR_AUDIO_VISUAL_EFTPOS_SUCCESS"

    const/16 v11, 0x1b

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/cardreader/lcr/CrAudioVisualId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_EFTPOS_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    .line 11
    sget-object v10, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_BUZZER_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_MASTERCARD_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_VISA_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_AMEX_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_JCB_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_DISCOVER_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_CUP_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_INTERAC_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->CR_AUDIO_VISUAL_EFTPOS_SUCCESS:Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->$VALUES:[Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrAudioVisualId$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    iput p3, p0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 44
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrAudioVisualId$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/CrAudioVisualId;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrAudioVisualId;",
            ")V"
        }
    .end annotation

    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 49
    iget p1, p3, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->swigValue:I

    .line 50
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrAudioVisualId$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrAudioVisualId;
    .locals 6

    .line 27
    const-class v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    .line 28
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 29
    aget-object p0, v1, p0

    return-object p0

    .line 30
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 31
    iget v5, v4, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 33
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/CrAudioVisualId;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/CrAudioVisualId;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->$VALUES:[Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/CrAudioVisualId;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 23
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->swigValue:I

    return v0
.end method
