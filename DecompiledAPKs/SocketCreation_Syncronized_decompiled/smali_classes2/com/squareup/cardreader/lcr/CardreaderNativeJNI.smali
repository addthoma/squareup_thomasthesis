.class public Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;
.super Ljava/lang/Object;
.source "CardreaderNativeJNI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native CRS_APP_PROTOCOL_VERSION_get()I
.end method

.method public static final native CRS_EP_PROTOCOL_VERSION_get()I
.end method

.method public static final native CRS_TRANSPORT_PAUSE_TIMEOUT_IN_MS_get()I
.end method

.method public static final native CRS_TRANSPORT_PROTOCOL_VERSION_STAND_get()I
.end method

.method public static final native CRS_TRANSPORT_PROTOCOL_VERSION_get()I
.end method

.method public static final native CRS_TRANSPORT_SMALL_FRAME_THRESHOLD_get()I
.end method

.method public static final native CR_CARDHOLDER_VERIFICATION_PERFORMED_FLAG_CVM_FALLTHROUGH_get()I
.end method

.method public static final native CR_CARDHOLDER_VERIFICATION_PERFORMED_FLAG_RFU_BIT8_get()I
.end method

.method public static final native CR_CARDREADER_RESULT_SUCCESS_get()I
.end method

.method public static final native CR_PAYMENT_ACCOUNT_TYPE_MAX_get()I
.end method

.method public static final native CR_PAYMENT_APP_ADF_NAME_MAX_get()I
.end method

.method public static final native CR_PAYMENT_APP_LABEL_MAX_get()I
.end method

.method public static final native CR_PAYMENT_APP_PREFNAME_MAX_get()I
.end method

.method public static final native CR_PAYMENT_LAST4_LENGTH_get()I
.end method

.method public static final native CR_PAYMENT_MAGSWIPE_DUPLICATE_TIMEOUT_MS_get()I
.end method

.method public static final native CR_PAYMENT_MAGSWIPE_MAX_TIME_BETWEEN_M1_MESSAGES_MS_get()I
.end method

.method public static final native CR_PAYMENT_MAX_ICC_FAILURES_get()I
.end method

.method public static final native CR_PAYMENT_MAX_TIMINGS_get()I
.end method

.method public static final native CR_PAYMENT_NAME_MAX_LENGTH_get()I
.end method

.method public static final native CR_PAYMENT_PAN_IIN_PREFIX_LENGTH_get()I
.end method

.method public static final native CR_PAYMENT_PIN_ENTRY_TIMEOUT_MS_get()I
.end method

.method public static final native CR_PAYMENT_TIMING_LABEL_SIZE_get()I
.end method

.method public static final native cardreader_initialize(JJLjava/lang/Object;)J
.end method

.method public static final native cardreader_initialize_rpc(JLjava/lang/Object;)J
.end method

.method public static final native cr_cardreader_free(J)I
.end method

.method public static final native cr_cardreader_notify_reader_plugged(J)I
.end method

.method public static final native cr_cardreader_notify_reader_unplugged(J)I
.end method

.method public static final native cr_cardreader_term(J)I
.end method

.method public static final native process_rpc_callback()V
.end method
