.class public final enum Lcom/squareup/cardreader/lcr/CrsEventlogSource;
.super Ljava/lang/Enum;
.source "CrsEventlogSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/CrsEventlogSource$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/CrsEventlogSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_BLE:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_CC2640:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_COMMS:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_COREDUMP:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_CRQ_GT4:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_ENCRYPTED_COMMS:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_EVENTLOG:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_FUEL_GAUGE:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_FWUP:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_K400:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_L1:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_L2:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_M1:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_MAX:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_MFGTEST:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_NFC_VOL_CTL:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_PAYMENT:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_POWER:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_SECURITY:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_SQUID_KEY_AGREE:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_STATS:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_STM:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_SYSTEM:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_TAMPER:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_TMN:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_UI:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

.field public static final enum CRS_EVENTLOG_SOURCE_USB:Lcom/squareup/cardreader/lcr/CrsEventlogSource;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/4 v1, 0x0

    const-string v2, "CRS_EVENTLOG_SOURCE_EVENTLOG"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_EVENTLOG:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/4 v2, 0x1

    const-string v3, "CRS_EVENTLOG_SOURCE_L1"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_L1:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/4 v3, 0x2

    const-string v4, "CRS_EVENTLOG_SOURCE_L2"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_L2:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/4 v4, 0x3

    const-string v5, "CRS_EVENTLOG_SOURCE_POWER"

    invoke-direct {v0, v5, v4}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_POWER:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/4 v5, 0x4

    const-string v6, "CRS_EVENTLOG_SOURCE_M1"

    invoke-direct {v0, v6, v5}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_M1:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/4 v6, 0x5

    const-string v7, "CRS_EVENTLOG_SOURCE_COMMS"

    invoke-direct {v0, v7, v6}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_COMMS:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/4 v7, 0x6

    const-string v8, "CRS_EVENTLOG_SOURCE_SECURITY"

    invoke-direct {v0, v8, v7}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_SECURITY:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/4 v8, 0x7

    const-string v9, "CRS_EVENTLOG_SOURCE_STATS"

    invoke-direct {v0, v9, v8}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_STATS:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 20
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v9, 0x8

    const-string v10, "CRS_EVENTLOG_SOURCE_BLE"

    invoke-direct {v0, v10, v9}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_BLE:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 21
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v10, 0x9

    const-string v11, "CRS_EVENTLOG_SOURCE_K400"

    invoke-direct {v0, v11, v10}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_K400:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 22
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v11, 0xa

    const-string v12, "CRS_EVENTLOG_SOURCE_FUEL_GAUGE"

    invoke-direct {v0, v12, v11}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_FUEL_GAUGE:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 23
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v12, 0xb

    const-string v13, "CRS_EVENTLOG_SOURCE_USB"

    invoke-direct {v0, v13, v12}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_USB:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 24
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v13, 0xc

    const-string v14, "CRS_EVENTLOG_SOURCE_COREDUMP"

    invoke-direct {v0, v14, v13}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_COREDUMP:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 25
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v14, 0xd

    const-string v15, "CRS_EVENTLOG_SOURCE_CC2640"

    invoke-direct {v0, v15, v14}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_CC2640:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 26
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v15, 0xe

    const-string v14, "CRS_EVENTLOG_SOURCE_UI"

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_UI:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 27
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const-string v14, "CRS_EVENTLOG_SOURCE_FWUP"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_FWUP:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 28
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const-string v14, "CRS_EVENTLOG_SOURCE_TAMPER"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_TAMPER:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 29
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const-string v14, "CRS_EVENTLOG_SOURCE_NFC_VOL_CTL"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_NFC_VOL_CTL:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 30
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const-string v14, "CRS_EVENTLOG_SOURCE_SYSTEM"

    const/16 v15, 0x12

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_SYSTEM:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 31
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const-string v14, "CRS_EVENTLOG_SOURCE_SQUID_KEY_AGREE"

    const/16 v15, 0x13

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_SQUID_KEY_AGREE:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 32
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const-string v14, "CRS_EVENTLOG_SOURCE_PAYMENT"

    const/16 v15, 0x14

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_PAYMENT:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 33
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const-string v14, "CRS_EVENTLOG_SOURCE_ENCRYPTED_COMMS"

    const/16 v15, 0x15

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_ENCRYPTED_COMMS:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 34
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const-string v14, "CRS_EVENTLOG_SOURCE_TMN"

    const/16 v15, 0x16

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_TMN:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 35
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const-string v14, "CRS_EVENTLOG_SOURCE_STM"

    const/16 v15, 0x17

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_STM:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 36
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const-string v14, "CRS_EVENTLOG_SOURCE_MFGTEST"

    const/16 v15, 0x18

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_MFGTEST:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 37
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const-string v14, "CRS_EVENTLOG_SOURCE_CRQ_GT4"

    const/16 v15, 0x19

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_CRQ_GT4:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 38
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const-string v14, "CRS_EVENTLOG_SOURCE_MAX"

    const/16 v15, 0x1a

    const/16 v13, 0x20

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/cardreader/lcr/CrsEventlogSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_MAX:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v0, 0x1b

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 11
    sget-object v13, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_EVENTLOG:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_L1:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_L2:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_POWER:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_M1:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_COMMS:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_SECURITY:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_STATS:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_BLE:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_K400:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_FUEL_GAUGE:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_USB:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_COREDUMP:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_CC2640:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_UI:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_FWUP:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_TAMPER:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_NFC_VOL_CTL:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_SYSTEM:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_SQUID_KEY_AGREE:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_PAYMENT:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_ENCRYPTED_COMMS:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_TMN:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_STM:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_MFGTEST:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_CRQ_GT4:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->CRS_EVENTLOG_SOURCE_MAX:Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->$VALUES:[Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrsEventlogSource$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 61
    iput p3, p0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 62
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrsEventlogSource$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/CrsEventlogSource;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrsEventlogSource;",
            ")V"
        }
    .end annotation

    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 67
    iget p1, p3, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->swigValue:I

    .line 68
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsEventlogSource$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsEventlogSource;
    .locals 6

    .line 45
    const-class v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    .line 46
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 47
    aget-object p0, v1, p0

    return-object p0

    .line 48
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 49
    iget v5, v4, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 51
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/CrsEventlogSource;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/CrsEventlogSource;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->$VALUES:[Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/CrsEventlogSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/CrsEventlogSource;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 41
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrsEventlogSource;->swigValue:I

    return v0
.end method
