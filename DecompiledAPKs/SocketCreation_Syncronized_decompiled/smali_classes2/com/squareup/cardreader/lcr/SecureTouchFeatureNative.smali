.class public Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;
.super Ljava/lang/Object;
.source "SecureTouchFeatureNative.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeConstants;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cr_secure_touch_mode_feature_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;
    .locals 5

    .line 41
    invoke-static {}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_alloc()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 42
    :cond_0
    new-instance v2, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;-><init>(JZ)V

    move-object v0, v2

    :goto_0
    return-object v0
.end method

.method public static cr_secure_touch_mode_feature_disable_squid_touch_driver_result(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;)V
    .locals 4

    .line 13
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)J

    move-result-wide v0

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;->getCPtr(Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3, p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_disable_squid_touch_driver_result(JJLcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;)V

    return-void
.end method

.method public static cr_secure_touch_mode_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;
    .locals 2

    .line 46
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_free(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_secure_touch_mode_feature_init(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;
    .locals 7

    .line 50
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)J

    move-result-wide v0

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)J

    move-result-wide v2

    invoke-static {p2}, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->getCPtr(Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J

    move-result-wide v4

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_init(JJJLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_secure_touch_mode_feature_regular_set_button_location(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)V
    .locals 4

    .line 17
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)J

    move-result-wide v0

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->getCPtr(Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3, p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_regular_set_button_location(JJLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)V

    return-void
.end method

.method public static cr_secure_touch_mode_feature_sent_pinpad_configs(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmPinPadConfigType;)V
    .locals 2

    .line 21
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrsStmPinPadConfigType;->swigValue()I

    move-result p0

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_sent_pinpad_configs(JI)V

    return-void
.end method

.method public static cr_secure_touch_mode_feature_set_accessibility_configs(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)V
    .locals 4

    .line 33
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)J

    move-result-wide v0

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;->getCPtr(Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3, p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_set_accessibility_configs(JJLcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)V

    return-void
.end method

.method public static cr_secure_touch_mode_feature_start_secure_touch(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)V
    .locals 2

    .line 25
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_start_secure_touch(J)V

    return-void
.end method

.method public static cr_secure_touch_mode_feature_stop_secure_touch(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)V
    .locals 2

    .line 29
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_stop_secure_touch(J)V

    return-void
.end method

.method public static cr_secure_touch_mode_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;
    .locals 2

    .line 54
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_term(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_secure_touch_mode_pin_pad_is_hidden(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmHidePinPadResult;)V
    .locals 4

    .line 37
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)J

    move-result-wide v0

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsStmHidePinPadResult;->getCPtr(Lcom/squareup/cardreader/lcr/CrsStmHidePinPadResult;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3, p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_pin_pad_is_hidden(JJLcom/squareup/cardreader/lcr/CrsStmHidePinPadResult;)V

    return-void
.end method

.method public static secure_touch_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;
    .locals 3

    .line 58
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->secure_touch_initialize(JLjava/lang/Object;)J

    move-result-wide p0

    const-wide/16 v0, 0x0

    cmp-long v2, p0, v0

    if-nez v2, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 59
    :cond_0
    new-instance v0, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;-><init>(JZ)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method
