.class public Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativePassthrough;
.super Ljava/lang/Object;
.source "UserInteractionFeatureNativePassthrough.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cr_user_interaction_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;
    .locals 0

    .line 15
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNative;->cr_user_interaction_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_user_interaction_identify_reader(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;
    .locals 0

    .line 21
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNative;->cr_user_interaction_identify_reader(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_user_interaction_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;
    .locals 0

    .line 9
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNative;->cr_user_interaction_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;

    move-result-object p1

    return-object p1
.end method

.method public user_interaction_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;
    .locals 0

    .line 27
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNative;->user_interaction_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    move-result-object p1

    return-object p1
.end method
