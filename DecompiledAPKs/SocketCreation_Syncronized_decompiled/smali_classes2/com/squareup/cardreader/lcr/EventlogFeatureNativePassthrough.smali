.class public Lcom/squareup/cardreader/lcr/EventlogFeatureNativePassthrough;
.super Ljava/lang/Object;
.source "EventlogFeatureNativePassthrough.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cr_eventlog_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;)Lcom/squareup/cardreader/lcr/CrEventLogResult;
    .locals 0

    .line 14
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/EventlogFeatureNative;->cr_eventlog_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;)Lcom/squareup/cardreader/lcr/CrEventLogResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_eventlog_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;)Lcom/squareup/cardreader/lcr/CrEventLogResult;
    .locals 0

    .line 9
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/EventlogFeatureNative;->cr_eventlog_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;)Lcom/squareup/cardreader/lcr/CrEventLogResult;

    move-result-object p1

    return-object p1
.end method

.method public eventlog_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;
    .locals 0

    .line 20
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/EventlogFeatureNative;->eventlog_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

    move-result-object p1

    return-object p1
.end method
