.class public final synthetic Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$EdTs_9o-reL098EB3XjSKsxRxbg;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic f$0:Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;

.field private final synthetic f$1:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

.field private final synthetic f$2:Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;

.field private final synthetic f$3:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$EdTs_9o-reL098EB3XjSKsxRxbg;->f$0:Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;

    iput-object p2, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$EdTs_9o-reL098EB3XjSKsxRxbg;->f$1:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    iput-object p3, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$EdTs_9o-reL098EB3XjSKsxRxbg;->f$2:Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;

    iput-object p4, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$EdTs_9o-reL098EB3XjSKsxRxbg;->f$3:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$EdTs_9o-reL098EB3XjSKsxRxbg;->f$0:Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;

    iget-object v1, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$EdTs_9o-reL098EB3XjSKsxRxbg;->f$1:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    iget-object v2, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$EdTs_9o-reL098EB3XjSKsxRxbg;->f$2:Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;

    iget-object v3, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$EdTs_9o-reL098EB3XjSKsxRxbg;->f$3:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->lambda$onMagswipeFallbackSuccess$21$CardReaderSwig$InternalPaymentListener(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V

    return-void
.end method
