.class public final Lcom/squareup/bugreport/BatteryStatus$Health$Companion;
.super Ljava/lang/Object;
.source "BatteryStatus.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/bugreport/BatteryStatus$Health;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/bugreport/BatteryStatus$Health$Companion;",
        "",
        "()V",
        "fromBatteryManagerHealthExtra",
        "Lcom/squareup/bugreport/BatteryStatus$Health;",
        "value",
        "",
        "bugreport_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/bugreport/BatteryStatus$Health$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromBatteryManagerHealthExtra(I)Lcom/squareup/bugreport/BatteryStatus$Health;
    .locals 0

    packed-switch p1, :pswitch_data_0

    .line 70
    sget-object p1, Lcom/squareup/bugreport/BatteryStatus$Health;->UNKNOWN:Lcom/squareup/bugreport/BatteryStatus$Health;

    goto :goto_0

    .line 64
    :pswitch_0
    sget-object p1, Lcom/squareup/bugreport/BatteryStatus$Health;->COLD:Lcom/squareup/bugreport/BatteryStatus$Health;

    goto :goto_0

    .line 69
    :pswitch_1
    sget-object p1, Lcom/squareup/bugreport/BatteryStatus$Health;->UNSPECIFIED_FAILURE:Lcom/squareup/bugreport/BatteryStatus$Health;

    goto :goto_0

    .line 68
    :pswitch_2
    sget-object p1, Lcom/squareup/bugreport/BatteryStatus$Health;->OVER_VOLTAGE:Lcom/squareup/bugreport/BatteryStatus$Health;

    goto :goto_0

    .line 65
    :pswitch_3
    sget-object p1, Lcom/squareup/bugreport/BatteryStatus$Health;->DEAD:Lcom/squareup/bugreport/BatteryStatus$Health;

    goto :goto_0

    .line 67
    :pswitch_4
    sget-object p1, Lcom/squareup/bugreport/BatteryStatus$Health;->OVERHEAT:Lcom/squareup/bugreport/BatteryStatus$Health;

    goto :goto_0

    .line 66
    :pswitch_5
    sget-object p1, Lcom/squareup/bugreport/BatteryStatus$Health;->GOOD:Lcom/squareup/bugreport/BatteryStatus$Health;

    :goto_0
    return-object p1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
