.class public final Lcom/squareup/RegisterAppDelegate_MembersInjector;
.super Ljava/lang/Object;
.source "RegisterAppDelegate_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/RegisterAppDelegate;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final anrChaperoneProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/anrchaperone/AnrChaperone;",
            ">;"
        }
    .end annotation
.end field

.field private final appScopedItemsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;>;"
        }
    .end annotation
.end field

.field private final authenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private final badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderOracleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private final deepLinkHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceSettingsSettingsInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceSettingsSettingsInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private final eventStreamV2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v2/EventstreamV2;",
            ">;"
        }
    .end annotation
.end field

.field private final foregroundServiceStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/foregroundservice/ForegroundServiceStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final isReaderSdkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadBlockedLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/MainThreadBlockedLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final msFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MsFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationWrapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapBusBoyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapBusBoy;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final persistentAccountStatusServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;"
        }
    .end annotation
.end field

.field private final playServicesVersionsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/versions/PlayServicesVersions;",
            ">;"
        }
    .end annotation
.end field

.field private final queueServiceStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final readerEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final readerSessionIdsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderSessionIds;",
            ">;"
        }
    .end annotation
.end field

.field private final recorderErrorReporterListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/RecorderErrorReporterListener;",
            ">;"
        }
    .end annotation
.end field

.field private final remoteLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/RemoteLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/anrchaperone/AnrChaperone;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceSettingsSettingsInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v2/EventstreamV2;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/RecorderErrorReporterListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MsFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapBusBoy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/foregroundservice/ForegroundServiceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/MainThreadBlockedLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderSessionIds;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/RemoteLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/versions/PlayServicesVersions;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 120
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 121
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->anrChaperoneProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 122
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->authenticatorProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 123
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->busProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 124
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->cardReaderHubProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 125
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->cardReaderOracleProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 126
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->deviceSettingsSettingsInitializerProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 127
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 128
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->eventStreamV2Provider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 129
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->recorderErrorReporterListenerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 130
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->libraryLoaderProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 131
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->msFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 132
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->ohSnapBusBoyProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 133
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->foregroundServiceStarterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 134
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->queueServiceStarterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 135
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->mainThreadBlockedLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 136
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->readerSessionIdsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 137
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->remoteLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 138
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->deepLinkHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 139
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->cardReaderListenersProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 140
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->readerEventLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 141
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->notificationWrapperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 142
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->appScopedItemsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 143
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->isReaderSdkProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 144
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 145
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 146
    iput-object v1, v0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->playServicesVersionsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 29
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/anrchaperone/AnrChaperone;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceSettingsSettingsInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v2/EventstreamV2;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/RecorderErrorReporterListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MsFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapBusBoy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/foregroundservice/ForegroundServiceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/MainThreadBlockedLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderSessionIds;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/RemoteLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/versions/PlayServicesVersions;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/RegisterAppDelegate;",
            ">;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    .line 171
    new-instance v28, Lcom/squareup/RegisterAppDelegate_MembersInjector;

    move-object/from16 v0, v28

    invoke-direct/range {v0 .. v27}, Lcom/squareup/RegisterAppDelegate_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v28
.end method

.method public static injectAnalytics(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/analytics/Analytics;)V
    .locals 0

    .line 206
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static injectAnrChaperone(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/anrchaperone/AnrChaperone;)V
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->anrChaperone:Lcom/squareup/anrchaperone/AnrChaperone;

    return-void
.end method

.method public static injectAppScopedItems(Lcom/squareup/RegisterAppDelegate;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/RegisterAppDelegate;",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;)V"
        }
    .end annotation

    .line 333
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->appScopedItems:Ljava/util/Set;

    return-void
.end method

.method public static injectAuthenticator(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/account/LegacyAuthenticator;)V
    .locals 0

    .line 217
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    return-void
.end method

.method public static injectBadMaybeSquareDeviceCheck(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V
    .locals 0

    .line 351
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    return-void
.end method

.method public static injectBus(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/badbus/BadBus;)V
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->bus:Lcom/squareup/badbus/BadBus;

    return-void
.end method

.method public static injectCardReaderHub(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/cardreader/CardReaderHub;)V
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    return-void
.end method

.method public static injectCardReaderListeners(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/cardreader/CardReaderListeners;)V
    .locals 0

    .line 314
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    return-void
.end method

.method public static injectCardReaderOracle(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    return-void
.end method

.method public static injectDeepLinkHelper(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/analytics/DeepLinkHelper;)V
    .locals 0

    .line 308
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->deepLinkHelper:Lcom/squareup/analytics/DeepLinkHelper;

    return-void
.end method

.method public static injectDeviceSettingsSettingsInitializer(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/settings/DeviceSettingsSettingsInitializer;)V
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->deviceSettingsSettingsInitializer:Lcom/squareup/settings/DeviceSettingsSettingsInitializer;

    return-void
.end method

.method public static injectEventStreamV2(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/eventstream/v2/EventstreamV2;)V
    .locals 0

    .line 251
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->eventStreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;

    return-void
.end method

.method public static injectForegroundServiceStarter(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/foregroundservice/ForegroundServiceStarter;)V
    .locals 0

    .line 279
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->foregroundServiceStarter:Lcom/squareup/foregroundservice/ForegroundServiceStarter;

    return-void
.end method

.method public static injectIsReaderSdk(Lcom/squareup/RegisterAppDelegate;Z)V
    .locals 0

    .line 339
    iput-boolean p1, p0, Lcom/squareup/RegisterAppDelegate;->isReaderSdk:Z

    return-void
.end method

.method public static injectLibraryLoader(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/cardreader/loader/LibraryLoader;)V
    .locals 0

    .line 263
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    return-void
.end method

.method public static injectMainThreadBlockedLogger(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/log/MainThreadBlockedLogger;)V
    .locals 0

    .line 291
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->mainThreadBlockedLogger:Lcom/squareup/log/MainThreadBlockedLogger;

    return-void
.end method

.method public static injectMsFactory(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/ms/MsFactory;)V
    .locals 0

    .line 268
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->msFactory:Lcom/squareup/ms/MsFactory;

    return-void
.end method

.method public static injectNotificationWrapper(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/notification/NotificationWrapper;)V
    .locals 0

    .line 326
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    return-void
.end method

.method public static injectOhSnapBusBoy(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/log/OhSnapBusBoy;)V
    .locals 0

    .line 273
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->ohSnapBusBoy:Lcom/squareup/log/OhSnapBusBoy;

    return-void
.end method

.method public static injectOhSnapLogger(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/log/OhSnapLogger;)V
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    return-void
.end method

.method public static injectPersistentAccountStatusService(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/accountstatus/PersistentAccountStatusService;)V
    .locals 0

    .line 345
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->persistentAccountStatusService:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    return-void
.end method

.method public static injectPlayServicesVersions(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/firebase/versions/PlayServicesVersions;)V
    .locals 0

    .line 357
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->playServicesVersions:Lcom/squareup/firebase/versions/PlayServicesVersions;

    return-void
.end method

.method public static injectQueueServiceStarter(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/queue/QueueServiceStarter;)V
    .locals 0

    .line 285
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    return-void
.end method

.method public static injectReaderEventLogger(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/log/ReaderEventLogger;)V
    .locals 0

    .line 320
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    return-void
.end method

.method public static injectReaderSessionIds(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/log/ReaderSessionIds;)V
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    return-void
.end method

.method public static injectRecorderErrorReporterListener(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/swipe/RecorderErrorReporterListener;)V
    .locals 0

    .line 257
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->recorderErrorReporterListener:Lcom/squareup/swipe/RecorderErrorReporterListener;

    return-void
.end method

.method public static injectRemoteLogger(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/logging/RemoteLogger;)V
    .locals 0

    .line 302
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->remoteLogger:Lcom/squareup/logging/RemoteLogger;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/RegisterAppDelegate;)V
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectAnalytics(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/analytics/Analytics;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->anrChaperoneProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectAnrChaperone(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/anrchaperone/AnrChaperone;)V

    .line 177
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->authenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/account/LegacyAuthenticator;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectAuthenticator(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/account/LegacyAuthenticator;)V

    .line 178
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/badbus/BadBus;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectBus(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/badbus/BadBus;)V

    .line 179
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderHub;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectCardReaderHub(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/cardreader/CardReaderHub;)V

    .line 180
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->cardReaderOracleProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectCardReaderOracle(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V

    .line 181
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->deviceSettingsSettingsInitializerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/DeviceSettingsSettingsInitializer;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectDeviceSettingsSettingsInitializer(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/settings/DeviceSettingsSettingsInitializer;)V

    .line 182
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/OhSnapLogger;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectOhSnapLogger(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/log/OhSnapLogger;)V

    .line 183
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->eventStreamV2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v2/EventstreamV2;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectEventStreamV2(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/eventstream/v2/EventstreamV2;)V

    .line 184
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->recorderErrorReporterListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/swipe/RecorderErrorReporterListener;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectRecorderErrorReporterListener(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/swipe/RecorderErrorReporterListener;)V

    .line 185
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->libraryLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectLibraryLoader(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/cardreader/loader/LibraryLoader;)V

    .line 186
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->msFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ms/MsFactory;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectMsFactory(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/ms/MsFactory;)V

    .line 187
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->ohSnapBusBoyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/OhSnapBusBoy;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectOhSnapBusBoy(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/log/OhSnapBusBoy;)V

    .line 188
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->foregroundServiceStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/foregroundservice/ForegroundServiceStarter;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectForegroundServiceStarter(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/foregroundservice/ForegroundServiceStarter;)V

    .line 189
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->queueServiceStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/QueueServiceStarter;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectQueueServiceStarter(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/queue/QueueServiceStarter;)V

    .line 190
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->mainThreadBlockedLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/MainThreadBlockedLogger;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectMainThreadBlockedLogger(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/log/MainThreadBlockedLogger;)V

    .line 191
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->readerSessionIdsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/ReaderSessionIds;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectReaderSessionIds(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/log/ReaderSessionIds;)V

    .line 192
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->remoteLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/logging/RemoteLogger;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectRemoteLogger(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/logging/RemoteLogger;)V

    .line 193
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->deepLinkHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/DeepLinkHelper;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectDeepLinkHelper(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/analytics/DeepLinkHelper;)V

    .line 194
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderListeners;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectCardReaderListeners(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/cardreader/CardReaderListeners;)V

    .line 195
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->readerEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/ReaderEventLogger;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectReaderEventLogger(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/log/ReaderEventLogger;)V

    .line 196
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->notificationWrapperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/notification/NotificationWrapper;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectNotificationWrapper(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/notification/NotificationWrapper;)V

    .line 197
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->appScopedItemsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectAppScopedItems(Lcom/squareup/RegisterAppDelegate;Ljava/util/Set;)V

    .line 198
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->isReaderSdkProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectIsReaderSdk(Lcom/squareup/RegisterAppDelegate;Z)V

    .line 199
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectPersistentAccountStatusService(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/accountstatus/PersistentAccountStatusService;)V

    .line 200
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectBadMaybeSquareDeviceCheck(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V

    .line 201
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate_MembersInjector;->playServicesVersionsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/firebase/versions/PlayServicesVersions;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectPlayServicesVersions(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/firebase/versions/PlayServicesVersions;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/RegisterAppDelegate;

    invoke-virtual {p0, p1}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectMembers(Lcom/squareup/RegisterAppDelegate;)V

    return-void
.end method
