.class public final Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;
.super Ljava/lang/Object;
.source "AccountFreezeTutorialRunner.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u0007\u001a\u00020\u0008J\u0006\u0010\t\u001a\u00020\u0008J\u0006\u0010\n\u001a\u00020\u0008R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;",
        "",
        "flow",
        "Lflow/Flow;",
        "balanceAppletGateway",
        "Lcom/squareup/ui/balance/BalanceAppletGateway;",
        "(Lflow/Flow;Lcom/squareup/ui/balance/BalanceAppletGateway;)V",
        "finishPromptScreen",
        "",
        "jumpToDeposits",
        "showPromptScreen",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final balanceAppletGateway:Lcom/squareup/ui/balance/BalanceAppletGateway;

.field private final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/ui/balance/BalanceAppletGateway;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceAppletGateway"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;->balanceAppletGateway:Lcom/squareup/ui/balance/BalanceAppletGateway;

    return-void
.end method


# virtual methods
.method public final finishPromptScreen()V
    .locals 4

    .line 22
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/accountfreeze/AccountFreezeTutorialDialogScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public final jumpToDeposits()V
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;->balanceAppletGateway:Lcom/squareup/ui/balance/BalanceAppletGateway;

    invoke-interface {v0}, Lcom/squareup/ui/balance/BalanceAppletGateway;->activateInitialScreen()V

    return-void
.end method

.method public final showPromptScreen()V
    .locals 2

    .line 14
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/accountfreeze/AccountFreezeTutorialDialogScreen;->INSTANCE:Lcom/squareup/accountfreeze/AccountFreezeTutorialDialogScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
