.class public final Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;
.super Ljava/lang/Object;
.source "RealAccountFreeze_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/accountfreeze/RealAccountFreeze;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;>;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p6, p0, Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;>;)",
            "Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;"
        }
    .end annotation

    .line 53
    new-instance v7, Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Ldagger/Lazy;Lcom/squareup/util/Clock;Lcom/f2prateek/rx/preferences2/Preference;)Lcom/squareup/accountfreeze/RealAccountFreeze;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/balance/BalanceAppletGateway;",
            ">;",
            "Lcom/squareup/util/Clock;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/squareup/accountfreeze/RealAccountFreeze;"
        }
    .end annotation

    .line 59
    new-instance v7, Lcom/squareup/accountfreeze/RealAccountFreeze;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/accountfreeze/RealAccountFreeze;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Ldagger/Lazy;Lcom/squareup/util/Clock;Lcom/f2prateek/rx/preferences2/Preference;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/accountfreeze/RealAccountFreeze;
    .locals 7

    .line 46
    iget-object v0, p0, Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    iget-object v0, p0, Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static/range {v1 .. v6}, Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Ldagger/Lazy;Lcom/squareup/util/Clock;Lcom/f2prateek/rx/preferences2/Preference;)Lcom/squareup/accountfreeze/RealAccountFreeze;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/accountfreeze/RealAccountFreeze_Factory;->get()Lcom/squareup/accountfreeze/RealAccountFreeze;

    move-result-object v0

    return-object v0
.end method
