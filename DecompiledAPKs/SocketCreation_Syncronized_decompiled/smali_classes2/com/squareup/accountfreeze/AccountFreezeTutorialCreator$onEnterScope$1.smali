.class final Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator$onEnterScope$1;
.super Lkotlin/jvm/internal/Lambda;
.source "AccountFreezeTutorialCreator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "shouldShow",
        "",
        "kotlin.jvm.PlatformType",
        "invoke",
        "(Ljava/lang/Boolean;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;


# direct methods
.method constructor <init>(Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator$onEnterScope$1;->this$0:Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator$onEnterScope$1;->invoke(Ljava/lang/Boolean;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Boolean;)V
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator$onEnterScope$1;->this$0:Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;

    invoke-static {v0}, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;->access$getSeeds$p(Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    const-string v1, "shouldShow"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator$Seed;

    iget-object v1, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator$onEnterScope$1;->this$0:Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;

    invoke-static {v1}, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;->access$getTutorialProvider$p(Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator$Seed;-><init>(Ljavax/inject/Provider;)V

    check-cast p1, Lcom/squareup/tutorialv2/TutorialSeed;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/tutorialv2/TutorialSeed;->NONE:Lcom/squareup/tutorialv2/TutorialSeed;

    :goto_0
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
