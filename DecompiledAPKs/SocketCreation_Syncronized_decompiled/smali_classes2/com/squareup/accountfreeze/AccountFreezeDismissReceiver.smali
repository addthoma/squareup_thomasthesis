.class public final Lcom/squareup/accountfreeze/AccountFreezeDismissReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AccountFreezeDismissReceiver.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/accountfreeze/AccountFreezeDismissReceiver$Component;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\tB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/accountfreeze/AccountFreezeDismissReceiver;",
        "Landroid/content/BroadcastReceiver;",
        "()V",
        "onReceive",
        "",
        "context",
        "Landroid/content/Context;",
        "intent",
        "Landroid/content/Intent;",
        "Component",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "intent"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-static {}, Lcom/squareup/AppDelegate$Locator;->getAppDelegate()Lcom/squareup/AppDelegate;

    move-result-object p1

    .line 16
    const-class p2, Lcom/squareup/accountfreeze/AccountFreezeDismissReceiver$Component;

    invoke-interface {p1, p2}, Lcom/squareup/AppDelegate;->getLoggedInComponent(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/accountfreeze/AccountFreezeDismissReceiver$Component;

    .line 17
    invoke-interface {p1}, Lcom/squareup/accountfreeze/AccountFreezeDismissReceiver$Component;->accountFreezeNotificationScheduler()Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;

    move-result-object p1

    .line 18
    invoke-virtual {p1}, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->onNotificationDismissed()V

    return-void
.end method
