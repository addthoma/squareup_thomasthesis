.class public final Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule$Companion;
.super Ljava/lang/Object;
.source "AccountFreezeLoggedInModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0081\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007H\u0007J\u0018\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007H\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule$Companion;",
        "",
        "()V",
        "provideLastDismissedFreezeBanner",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "rxPrefs",
        "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
        "provideLastDismissedFreezeNotification",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideLastDismissedFreezeBanner(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .param p1    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/accountfreeze/LastDismissedFreezeBanner;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "rxPrefs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    .line 36
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "lastDismissedFreezeBanner"

    invoke-virtual {p1, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getLong(Ljava/lang/String;Ljava/lang/Long;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p1

    const-string v0, "rxPrefs.getLong(\"lastDismissedFreezeBanner\", 0)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final provideLastDismissedFreezeNotification(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .param p1    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/accountfreeze/LastDismissedFreezeNotification;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "rxPrefs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    .line 44
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "lastDismissedFreezeNotification"

    invoke-virtual {p1, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getLong(Ljava/lang/String;Ljava/lang/Long;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p1

    const-string v0, "rxPrefs.getLong(\"lastDis\u2026edFreezeNotification\", 0)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
