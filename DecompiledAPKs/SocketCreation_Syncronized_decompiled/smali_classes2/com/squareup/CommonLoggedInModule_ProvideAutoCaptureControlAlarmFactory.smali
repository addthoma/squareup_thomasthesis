.class public final Lcom/squareup/CommonLoggedInModule_ProvideAutoCaptureControlAlarmFactory;
.super Ljava/lang/Object;
.source "CommonLoggedInModule_ProvideAutoCaptureControlAlarmFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/CommonLoggedInModule_ProvideAutoCaptureControlAlarmFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/autocapture/AutoCaptureControlAlarm;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/CommonLoggedInModule_ProvideAutoCaptureControlAlarmFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/CommonLoggedInModule_ProvideAutoCaptureControlAlarmFactory$InstanceHolder;->access$000()Lcom/squareup/CommonLoggedInModule_ProvideAutoCaptureControlAlarmFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideAutoCaptureControlAlarm()Lcom/squareup/autocapture/AutoCaptureControlAlarm;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/CommonLoggedInModule;->provideAutoCaptureControlAlarm()Lcom/squareup/autocapture/AutoCaptureControlAlarm;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/autocapture/AutoCaptureControlAlarm;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/autocapture/AutoCaptureControlAlarm;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/CommonLoggedInModule_ProvideAutoCaptureControlAlarmFactory;->provideAutoCaptureControlAlarm()Lcom/squareup/autocapture/AutoCaptureControlAlarm;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/CommonLoggedInModule_ProvideAutoCaptureControlAlarmFactory;->get()Lcom/squareup/autocapture/AutoCaptureControlAlarm;

    move-result-object v0

    return-object v0
.end method
