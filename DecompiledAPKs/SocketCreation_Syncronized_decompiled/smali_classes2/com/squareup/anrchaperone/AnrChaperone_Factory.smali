.class public final Lcom/squareup/anrchaperone/AnrChaperone_Factory;
.super Ljava/lang/Object;
.source "AnrChaperone_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/anrchaperone/AnrChaperone;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final chaperoneThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/SerialExecutor;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/SerialExecutor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/anrchaperone/AnrChaperone_Factory;->chaperoneThreadProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/anrchaperone/AnrChaperone_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/anrchaperone/AnrChaperone_Factory;->applicationProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/anrchaperone/AnrChaperone_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/SerialExecutor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/anrchaperone/AnrChaperone_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/anrchaperone/AnrChaperone_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/anrchaperone/AnrChaperone_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/util/Clock;Landroid/app/Application;)Lcom/squareup/anrchaperone/AnrChaperone;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/anrchaperone/AnrChaperone;-><init>(Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/util/Clock;Landroid/app/Application;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/anrchaperone/AnrChaperone;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperone_Factory;->chaperoneThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/executor/SerialExecutor;

    iget-object v1, p0, Lcom/squareup/anrchaperone/AnrChaperone_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Clock;

    iget-object v2, p0, Lcom/squareup/anrchaperone/AnrChaperone_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Application;

    invoke-static {v0, v1, v2}, Lcom/squareup/anrchaperone/AnrChaperone_Factory;->newInstance(Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/util/Clock;Landroid/app/Application;)Lcom/squareup/anrchaperone/AnrChaperone;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/anrchaperone/AnrChaperone_Factory;->get()Lcom/squareup/anrchaperone/AnrChaperone;

    move-result-object v0

    return-object v0
.end method
