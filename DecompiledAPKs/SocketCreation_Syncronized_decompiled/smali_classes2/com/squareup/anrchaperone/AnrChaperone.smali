.class public final Lcom/squareup/anrchaperone/AnrChaperone;
.super Ljava/lang/Object;
.source "AnrChaperone.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/anrchaperone/AnrChaperone$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAnrChaperone.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AnrChaperone.kt\ncom/squareup/anrchaperone/AnrChaperone\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,152:1\n250#2,2:153\n*E\n*S KotlinDebug\n*F\n+ 1 AnrChaperone.kt\ncom/squareup/anrchaperone/AnrChaperone\n*L\n120#1,2:153\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB!\u0008\u0017\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008B%\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u000bJ\u0008\u0010\u0016\u001a\u00020\u0013H\u0002J\n\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0002J\u0006\u0010\u0019\u001a\u00020\u001aJ\u0014\u0010\u001b\u001a\u00020\u001a2\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u001dR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000c\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/anrchaperone/AnrChaperone;",
        "",
        "chaperoneThread",
        "Lcom/squareup/thread/executor/SerialExecutor;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "application",
        "Landroid/app/Application;",
        "(Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/util/Clock;Landroid/app/Application;)V",
        "mainThread",
        "Landroid/os/Handler;",
        "(Landroid/os/Handler;Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/util/Clock;Landroid/app/Application;)V",
        "mainThreadLastUpdate",
        "",
        "getMainThreadLastUpdate",
        "()J",
        "setMainThreadLastUpdate",
        "(J)V",
        "stopping",
        "",
        "visibilityTracker",
        "Lcom/squareup/anrchaperone/VisibilityTracker;",
        "isVisible",
        "retrieveProcessInfo",
        "Landroid/app/ActivityManager$RunningAppProcessInfo;",
        "stop",
        "",
        "supervise",
        "callback",
        "Lkotlin/Function0;",
        "Companion",
        "anr-chaperone_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ANR_DELAY_MS:J = 0x1388L

.field public static final CHAPERONE_TICK_MS:J = 0x3e8L

.field public static final CHAPERONE_TICK_TOLERANCE_MS:J = 0x1f4L

.field public static final Companion:Lcom/squareup/anrchaperone/AnrChaperone$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MAIN_THREAD_TICK_MS:J = 0x3e8L

.field public static final MOVE_TO_MAIN_THREAD_FRONT_MS:J = 0xbb8L


# instance fields
.field private final application:Landroid/app/Application;

.field private final chaperoneThread:Lcom/squareup/thread/executor/SerialExecutor;

.field private final clock:Lcom/squareup/util/Clock;

.field private final mainThread:Landroid/os/Handler;

.field private volatile mainThreadLastUpdate:J

.field private volatile stopping:Z

.field private final visibilityTracker:Lcom/squareup/anrchaperone/VisibilityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/anrchaperone/AnrChaperone$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/anrchaperone/AnrChaperone$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/anrchaperone/AnrChaperone;->Companion:Lcom/squareup/anrchaperone/AnrChaperone$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/util/Clock;Landroid/app/Application;)V
    .locals 1

    const-string v0, "mainThread"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chaperoneThread"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "application"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/anrchaperone/AnrChaperone;->mainThread:Landroid/os/Handler;

    iput-object p2, p0, Lcom/squareup/anrchaperone/AnrChaperone;->chaperoneThread:Lcom/squareup/thread/executor/SerialExecutor;

    iput-object p3, p0, Lcom/squareup/anrchaperone/AnrChaperone;->clock:Lcom/squareup/util/Clock;

    iput-object p4, p0, Lcom/squareup/anrchaperone/AnrChaperone;->application:Landroid/app/Application;

    .line 39
    new-instance p1, Lcom/squareup/anrchaperone/VisibilityTracker;

    invoke-direct {p1}, Lcom/squareup/anrchaperone/VisibilityTracker;-><init>()V

    iput-object p1, p0, Lcom/squareup/anrchaperone/AnrChaperone;->visibilityTracker:Lcom/squareup/anrchaperone/VisibilityTracker;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/util/Clock;Landroid/app/Application;)V
    .locals 2
    .param p1    # Lcom/squareup/thread/executor/SerialExecutor;
        .annotation runtime Lcom/squareup/anrchaperone/AnrChaperoneModule$Chaperone;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "chaperoneThread"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "application"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/squareup/anrchaperone/AnrChaperone;-><init>(Landroid/os/Handler;Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/util/Clock;Landroid/app/Application;)V

    return-void
.end method

.method public static final synthetic access$Companion()Lcom/squareup/anrchaperone/AnrChaperone$Companion;
    .locals 1

    sget-object v0, Lcom/squareup/anrchaperone/AnrChaperone;->Companion:Lcom/squareup/anrchaperone/AnrChaperone$Companion;

    return-object v0
.end method

.method public static final synthetic access$getChaperoneThread$p(Lcom/squareup/anrchaperone/AnrChaperone;)Lcom/squareup/thread/executor/SerialExecutor;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/anrchaperone/AnrChaperone;->chaperoneThread:Lcom/squareup/thread/executor/SerialExecutor;

    return-object p0
.end method

.method public static final synthetic access$getClock$p(Lcom/squareup/anrchaperone/AnrChaperone;)Lcom/squareup/util/Clock;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/anrchaperone/AnrChaperone;->clock:Lcom/squareup/util/Clock;

    return-object p0
.end method

.method public static final synthetic access$getMainThread$p(Lcom/squareup/anrchaperone/AnrChaperone;)Landroid/os/Handler;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/anrchaperone/AnrChaperone;->mainThread:Landroid/os/Handler;

    return-object p0
.end method

.method public static final synthetic access$getStopping$p(Lcom/squareup/anrchaperone/AnrChaperone;)Z
    .locals 0

    .line 21
    iget-boolean p0, p0, Lcom/squareup/anrchaperone/AnrChaperone;->stopping:Z

    return p0
.end method

.method public static final synthetic access$isVisible(Lcom/squareup/anrchaperone/AnrChaperone;)Z
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/squareup/anrchaperone/AnrChaperone;->isVisible()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$setStopping$p(Lcom/squareup/anrchaperone/AnrChaperone;Z)V
    .locals 0

    .line 21
    iput-boolean p1, p0, Lcom/squareup/anrchaperone/AnrChaperone;->stopping:Z

    return-void
.end method

.method private final isVisible()Z
    .locals 2

    .line 101
    invoke-direct {p0}, Lcom/squareup/anrchaperone/AnrChaperone;->retrieveProcessInfo()Landroid/app/ActivityManager$RunningAppProcessInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 104
    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v1, 0xc8

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperone;->visibilityTracker:Lcom/squareup/anrchaperone/VisibilityTracker;

    invoke-virtual {v0}, Lcom/squareup/anrchaperone/VisibilityTracker;->getHasVisibleActivities()Z

    move-result v0

    :goto_0
    return v0
.end method

.method private final retrieveProcessInfo()Landroid/app/ActivityManager$RunningAppProcessInfo;
    .locals 5

    .line 110
    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperone;->application:Landroid/app/Application;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v1, 0x0

    .line 113
    :try_start_0
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_3

    .line 119
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    .line 120
    check-cast v0, Ljava/lang/Iterable;

    .line 153
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 121
    iget v4, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v4, v2, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_0

    move-object v1, v3

    .line 154
    :cond_2
    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    :catch_0
    :cond_3
    return-object v1

    .line 110
    :cond_4
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.app.ActivityManager"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final getMainThreadLastUpdate()J
    .locals 2

    .line 35
    iget-wide v0, p0, Lcom/squareup/anrchaperone/AnrChaperone;->mainThreadLastUpdate:J

    return-wide v0
.end method

.method public final setMainThreadLastUpdate(J)V
    .locals 0

    .line 35
    iput-wide p1, p0, Lcom/squareup/anrchaperone/AnrChaperone;->mainThreadLastUpdate:J

    return-void
.end method

.method public final stop()V
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperone;->application:Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/anrchaperone/AnrChaperone;->visibilityTracker:Lcom/squareup/anrchaperone/VisibilityTracker;

    check-cast v1, Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {v0, v1}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    const/4 v0, 0x1

    .line 134
    iput-boolean v0, p0, Lcom/squareup/anrchaperone/AnrChaperone;->stopping:Z

    return-void
.end method

.method public final supervise(Lkotlin/jvm/functions/Function0;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "callback"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperone;->application:Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/anrchaperone/AnrChaperone;->visibilityTracker:Lcom/squareup/anrchaperone/VisibilityTracker;

    check-cast v1, Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {v0, v1}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 51
    new-instance v7, Lcom/squareup/anrchaperone/AnrChaperone$supervise$mainThreadTick$1;

    invoke-direct {v7, p0}, Lcom/squareup/anrchaperone/AnrChaperone$supervise$mainThreadTick$1;-><init>(Lcom/squareup/anrchaperone/AnrChaperone;)V

    .line 60
    invoke-virtual {v7}, Lcom/squareup/anrchaperone/AnrChaperone$supervise$mainThreadTick$1;->run()V

    .line 62
    new-instance v5, Lkotlin/jvm/internal/Ref$LongRef;

    invoke-direct {v5}, Lkotlin/jvm/internal/Ref$LongRef;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, v5, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    .line 63
    new-instance v4, Lkotlin/jvm/internal/Ref$LongRef;

    invoke-direct {v4}, Lkotlin/jvm/internal/Ref$LongRef;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, v4, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    .line 65
    new-instance v0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;

    move-object v2, v0

    move-object v3, p0

    move-object v6, p1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;-><init>(Lcom/squareup/anrchaperone/AnrChaperone;Lkotlin/jvm/internal/Ref$LongRef;Lkotlin/jvm/internal/Ref$LongRef;Lkotlin/jvm/functions/Function0;Lcom/squareup/anrchaperone/AnrChaperone$supervise$mainThreadTick$1;)V

    .line 97
    iget-object p1, p0, Lcom/squareup/anrchaperone/AnrChaperone;->chaperoneThread:Lcom/squareup/thread/executor/SerialExecutor;

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {p1, v0}, Lcom/squareup/thread/executor/SerialExecutor;->post(Ljava/lang/Runnable;)V

    return-void
.end method
