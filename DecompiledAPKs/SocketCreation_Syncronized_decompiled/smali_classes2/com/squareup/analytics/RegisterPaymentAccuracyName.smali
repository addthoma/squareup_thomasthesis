.class public final enum Lcom/squareup/analytics/RegisterPaymentAccuracyName;
.super Ljava/lang/Enum;
.source "RegisterPaymentAccuracyName.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/analytics/RegisterPaymentAccuracyName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/analytics/RegisterPaymentAccuracyName;

.field public static final enum CANCEL_BILL:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

.field public static final enum CANCEL_TENDER:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

.field public static final enum OFFLINE_CHECKMARK_VIEW_FOR_AUTHORIZED:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

.field public static final enum OFFLINE_CHECKMARK_VIEW_FOR_CAPTURED:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

.field public static final enum ONLINE_CHECKMARK_VIEW_FOR_AUTH:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

.field public static final enum ONLINE_CHECKMARK_VIEW_FOR_CAPTURE:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

.field public static final enum RECEIPT_SCREEN:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

.field public static final enum STORED_TRANSACTION:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

.field public static final enum VOID_LAST_AUTH:Lcom/squareup/analytics/RegisterPaymentAccuracyName;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 7
    new-instance v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    const/4 v1, 0x0

    const-string v2, "RECEIPT_SCREEN"

    const-string v3, "Receipt Screen"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/analytics/RegisterPaymentAccuracyName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->RECEIPT_SCREEN:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    .line 8
    new-instance v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    const/4 v2, 0x1

    const-string v3, "CANCEL_BILL"

    const-string v4, "Cancel Bill"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/analytics/RegisterPaymentAccuracyName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->CANCEL_BILL:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    .line 9
    new-instance v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    const/4 v3, 0x2

    const-string v4, "CANCEL_TENDER"

    const-string v5, "Cancel Tender"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/analytics/RegisterPaymentAccuracyName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->CANCEL_TENDER:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    .line 10
    new-instance v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    const/4 v4, 0x3

    const-string v5, "OFFLINE_CHECKMARK_VIEW_FOR_AUTHORIZED"

    const-string v6, "Offline Checkmark View for Authorized"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/analytics/RegisterPaymentAccuracyName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->OFFLINE_CHECKMARK_VIEW_FOR_AUTHORIZED:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    .line 11
    new-instance v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    const/4 v5, 0x4

    const-string v6, "OFFLINE_CHECKMARK_VIEW_FOR_CAPTURED"

    const-string v7, "Offline Checkmark View for Captured"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/analytics/RegisterPaymentAccuracyName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->OFFLINE_CHECKMARK_VIEW_FOR_CAPTURED:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    .line 12
    new-instance v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    const/4 v6, 0x5

    const-string v7, "ONLINE_CHECKMARK_VIEW_FOR_AUTH"

    const-string v8, "Online Checkmark View for Auth"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/analytics/RegisterPaymentAccuracyName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->ONLINE_CHECKMARK_VIEW_FOR_AUTH:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    .line 13
    new-instance v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    const/4 v7, 0x6

    const-string v8, "ONLINE_CHECKMARK_VIEW_FOR_CAPTURE"

    const-string v9, "Online Checkmark View for Capture"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/analytics/RegisterPaymentAccuracyName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->ONLINE_CHECKMARK_VIEW_FOR_CAPTURE:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    .line 14
    new-instance v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    const/4 v8, 0x7

    const-string v9, "STORED_TRANSACTION"

    const-string v10, "Stored transaction"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/analytics/RegisterPaymentAccuracyName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->STORED_TRANSACTION:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    .line 15
    new-instance v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    const/16 v9, 0x8

    const-string v10, "VOID_LAST_AUTH"

    const-string v11, "Void last auth"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/analytics/RegisterPaymentAccuracyName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->VOID_LAST_AUTH:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    .line 6
    sget-object v10, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->RECEIPT_SCREEN:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->CANCEL_BILL:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->CANCEL_TENDER:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->OFFLINE_CHECKMARK_VIEW_FOR_AUTHORIZED:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->OFFLINE_CHECKMARK_VIEW_FOR_CAPTURED:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->ONLINE_CHECKMARK_VIEW_FOR_AUTH:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->ONLINE_CHECKMARK_VIEW_FOR_CAPTURE:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->STORED_TRANSACTION:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->VOID_LAST_AUTH:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->$VALUES:[Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21
    iput-object p3, p0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/analytics/RegisterPaymentAccuracyName;
    .locals 1

    .line 6
    const-class v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/analytics/RegisterPaymentAccuracyName;
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->$VALUES:[Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    invoke-virtual {v0}, [Lcom/squareup/analytics/RegisterPaymentAccuracyName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    return-object v0
.end method
