.class public abstract Lcom/squareup/analytics/BranchHelper$Module;
.super Ljava/lang/Object;
.source "BranchHelper.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/analytics/BranchHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideDeepLinkHelper(Lcom/squareup/analytics/BranchHelper;)Lcom/squareup/analytics/DeepLinkHelper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
