.class public final enum Lcom/squareup/analytics/ReaderEventName;
.super Ljava/lang/Enum;
.source "ReaderEventName.java"

# interfaces
.implements Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/analytics/ReaderEventName;",
        ">;",
        "Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/analytics/ReaderEventName;

.field public static final enum ACL_CONNECTED_FROM_APP_INITIALIZATION:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum ACL_CONNECTED_FROM_BROADCAST:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum ACL_DISCONNECTED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_ALREADY_CONNECTED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_CONNECTION_DISCONNECTED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_CONNECTION_ENQUEUED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_CONNECTION_FAILURE:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_CONNECTION_FORCE_UNPAIR:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_CONNECTION_PROBLEM_FOUND:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_CONNECTION_PROBLEM_RESOLVED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_CONNECTION_SILENT_RETRY:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_CONNECTION_STATE_CHANGED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_CONNECTION_STATE_RECEIVED_ACTION:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_CONNECTION_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_PAIRING_BEGIN_AUTOMATICALLY:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_PAIRING_CONFIRMATION_INCORRECT:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_PAIRING_CONFIRMATION_OK:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_PAIRING_FAILED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_PAIRING_FORGETTING_DURING_AUTO_CONNECT:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_PAIRING_NO_CONFIRMATION_NEEDED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_PAIRING_READER_DISCOVERED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_PAIRING_SCANNING:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_PAIRING_SCANNING_STOPPED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_PAIRING_SELECT_READER:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_PAIRING_SUCCESS_WITH_CONFIRMATION:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLE_REGISTERING_FOR_AUTO_CONNECT:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLUETOOTH_CONNECTION_FAILURE:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLUETOOTH_CONNECTION_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLUETOOTH_DISABLED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLUETOOTH_ENABLED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLUETOOTH_NOT_SUPPORTED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum BLUETOOTH_STATE_CHANGED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CARDHOLDER_NAME_FAIL:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CARDHOLDER_NAME_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CIRQUE_SECURITY_STATUS:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CONTACTLESS_PAYMENT_ACTION_REQUIRED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CONTACTLESS_PAYMENT_CARD_ERROR_TRY_ANOTHER_CARD:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CONTACTLESS_PAYMENT_COMPLETE_APPROVED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CONTACTLESS_PAYMENT_COMPLETE_APPROVED_OFFLINE:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CONTACTLESS_PAYMENT_COMPLETE_DECLINED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CONTACTLESS_PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CONTACTLESS_PAYMENT_COMPLETE_TIMEOUT:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CONTACTLESS_PAYMENT_ERROR_PRESENT_AGAIN:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CONTACTLESS_PAYMENT_FALLBACK_INSERT_OR_SWIPE:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_INSERT_CARD:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_TRY_ANOTHER_CARD:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CONTACTLESS_PAYMENT_MULTIPLE_CARDS:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CONTACTLESS_PAYMENT_STARTING_PAYMENT:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CONTACTLESS_PAYMENT_STARTING_REFUND:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum CONTACTLESS_PAYMENT_UNLOCK_DEVICE:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum FW_UPDATE_CANCELED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum FW_UPDATE_FAILURE:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum FW_UPDATE_FAILURE_REASON:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum FW_UPDATE_REBOOT:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum FW_UPDATE_RECEIVED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum FW_UPDATE_REQUIRED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum FW_UPDATE_SERVER_ERROR:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum FW_UPDATE_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum FW_UPDATE_SUGGESTED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum INIT_CORE_DUMP_FOUND:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum INIT_CORE_DUMP_NOT_FOUND:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum INIT_FULL_COMMS:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum INIT_READER_CONNECTION_TIMEOUT:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum INIT_READY_FOR_PAYMENTS:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum INIT_REGISTER_UPGRADE_REQUIRED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum INIT_SEND_FW_MANIFEST_UP:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum INIT_SEND_SS_CONTENTS:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum INIT_SS_DENIED_BY_SERVER:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum INIT_SS_INVALID:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum INIT_SS_REVOKED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum INIT_SS_VALID:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum INIT_TAMPER_DATA:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum INIT_TAMPER_FLAGGED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum INIT_TAMPER_FOUND:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum INIT_TAMPER_NOT_FOUND:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_APPLICATION_SELECTED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_APPLICATION_SELECTION:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_CARD_ERROR:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_CARD_INSERTED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_CARD_INSERTED_AWAITING_FW_UPDATE:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_CARD_MUST_BE_REINSERTED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_CARD_MUST_TAP:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_CARD_REMOVED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_CARD_REMOVED_DURING_PAYMENT:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_CARD_SWIPED_AWAITING_FW_UPDATE:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_COMPLETE_APPROVED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_COMPLETE_DECLINED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_EVENT:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_MAGSWIPE_FAILURE:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_MAGSWIPE_PASSTHROUGH:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_MAGSWIPE_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_SCHEME_FALLBACK:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_SEND_ARPC_TO_READER:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_SEND_ARQC_TO_SERVER:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_STARTING_PAYMENT_ON_READER:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_TECHNICAL_FALLBACK:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum PAYMENT_USE_CHIP_CARD:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum READER_BATTERY_HUD:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum READER_BATTERY_LEVEL:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum READER_CARRIER_DETECT:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum READER_EVENT_LOG:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum READER_EVENT_TAMPER:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum READER_LOW_BATTERY:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum READER_POWER_OFF:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum READER_SETTINGS_FORGET_READER:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum READER_SETTINGS_IDENTIFY_READER:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum READER_SETTINGS_NAME_CHANGED:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum TMN_TRANSACTION_RESULT:Lcom/squareup/analytics/ReaderEventName;

.field public static final enum WIRELESS_RSSI_SAMPLES:Lcom/squareup/analytics/ReaderEventName;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 16
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const/4 v1, 0x0

    const-string v2, "ACL_CONNECTED_FROM_APP_INITIALIZATION"

    const-string v3, "ACL Connected From App Initialization"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->ACL_CONNECTED_FROM_APP_INITIALIZATION:Lcom/squareup/analytics/ReaderEventName;

    .line 17
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const/4 v2, 0x1

    const-string v3, "ACL_CONNECTED_FROM_BROADCAST"

    const-string v4, "ACL Connected From Broadcast"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->ACL_CONNECTED_FROM_BROADCAST:Lcom/squareup/analytics/ReaderEventName;

    .line 18
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const/4 v3, 0x2

    const-string v4, "ACL_DISCONNECTED"

    const-string v5, "ACL Disconnected"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->ACL_DISCONNECTED:Lcom/squareup/analytics/ReaderEventName;

    .line 19
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const/4 v4, 0x3

    const-string v5, "BLE_CONNECTION_DISCONNECTED"

    const-string v6, "BLE Connection Was Disconnected"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_DISCONNECTED:Lcom/squareup/analytics/ReaderEventName;

    .line 20
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const/4 v5, 0x4

    const-string v6, "BLE_CONNECTION_ENQUEUED"

    const-string v7, "BLE Connection Attempt Was Enqueued"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_ENQUEUED:Lcom/squareup/analytics/ReaderEventName;

    .line 21
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const/4 v6, 0x5

    const-string v7, "BLE_CONNECTION_FAILURE"

    const-string v8, "BLE Connection Attempt Failed"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_FAILURE:Lcom/squareup/analytics/ReaderEventName;

    .line 22
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const/4 v7, 0x6

    const-string v8, "BLE_CONNECTION_FORCE_UNPAIR"

    const-string v9, "BLE Connection Force Un-Paired"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_FORCE_UNPAIR:Lcom/squareup/analytics/ReaderEventName;

    .line 23
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const/4 v8, 0x7

    const-string v9, "BLE_CONNECTION_PROBLEM_FOUND"

    const-string v10, "BLE Connection Problem Found"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_PROBLEM_FOUND:Lcom/squareup/analytics/ReaderEventName;

    .line 24
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const/16 v9, 0x8

    const-string v10, "BLE_CONNECTION_PROBLEM_RESOLVED"

    const-string v11, "BLE Connection Problem Resolved"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_PROBLEM_RESOLVED:Lcom/squareup/analytics/ReaderEventName;

    .line 25
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const/16 v10, 0x9

    const-string v11, "BLE_CONNECTION_SILENT_RETRY"

    const-string v12, "BLE Connection Silent Retry"

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_SILENT_RETRY:Lcom/squareup/analytics/ReaderEventName;

    .line 26
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const/16 v11, 0xa

    const-string v12, "BLE_CONNECTION_STATE_CHANGED"

    const-string v13, "BLE Connection State Changed"

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_STATE_CHANGED:Lcom/squareup/analytics/ReaderEventName;

    .line 27
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const/16 v12, 0xb

    const-string v13, "BLE_CONNECTION_SUCCESS"

    const-string v14, "BLE Connection Attempt Succeeded"

    invoke-direct {v0, v13, v12, v14}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

    .line 28
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const/16 v13, 0xc

    const-string v14, "BLE_CONNECTION_STATE_RECEIVED_ACTION"

    const-string v15, "BLE Connection State Received Action"

    invoke-direct {v0, v14, v13, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_STATE_RECEIVED_ACTION:Lcom/squareup/analytics/ReaderEventName;

    .line 29
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const/16 v14, 0xd

    const-string v15, "BLE_ALREADY_CONNECTED"

    const-string v13, "BLE Detected Already Connected"

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_ALREADY_CONNECTED:Lcom/squareup/analytics/ReaderEventName;

    .line 31
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const/16 v13, 0xe

    const-string v15, "BLE_PAIRING_BEGIN_AUTOMATICALLY"

    const-string v14, "R12 Pairing: Begin Automatically"

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_BEGIN_AUTOMATICALLY:Lcom/squareup/analytics/ReaderEventName;

    .line 32
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v14, "BLE_PAIRING_CONFIRMATION_INCORRECT"

    const/16 v15, 0xf

    const-string v13, "R12 Pairing: Forget Incorrectly Paired Reader"

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_CONFIRMATION_INCORRECT:Lcom/squareup/analytics/ReaderEventName;

    .line 33
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "BLE_PAIRING_CONFIRMATION_OK"

    const/16 v14, 0x10

    const-string v15, "R12 Pairing: Dismiss Confirmation Screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_CONFIRMATION_OK:Lcom/squareup/analytics/ReaderEventName;

    .line 34
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "BLE_PAIRING_FAILED"

    const/16 v14, 0x11

    const-string v15, "R12 Pairing: Failed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_FAILED:Lcom/squareup/analytics/ReaderEventName;

    .line 35
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "BLE_PAIRING_FORGETTING_DURING_AUTO_CONNECT"

    const/16 v14, 0x12

    const-string v15, "R12 Pairing: Automatically Forgetting Reader Because it is No Longer Paired"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_FORGETTING_DURING_AUTO_CONNECT:Lcom/squareup/analytics/ReaderEventName;

    .line 37
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "BLE_PAIRING_NO_CONFIRMATION_NEEDED"

    const/16 v14, 0x13

    const-string v15, "R12 Pairing: Success without Confirmation Screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_NO_CONFIRMATION_NEEDED:Lcom/squareup/analytics/ReaderEventName;

    .line 38
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "BLE_PAIRING_READER_DISCOVERED"

    const/16 v14, 0x14

    const-string v15, "R12 Pairing: Reader Discovered"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_READER_DISCOVERED:Lcom/squareup/analytics/ReaderEventName;

    .line 39
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "BLE_PAIRING_SCANNING"

    const/16 v14, 0x15

    const-string v15, "R12 Pairing: Scanning for Readers"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_SCANNING:Lcom/squareup/analytics/ReaderEventName;

    .line 40
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "BLE_PAIRING_SCANNING_STOPPED"

    const/16 v14, 0x16

    const-string v15, "R12 Pairing: Stopped scanning for Readers"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_SCANNING_STOPPED:Lcom/squareup/analytics/ReaderEventName;

    .line 41
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "BLE_PAIRING_SELECT_READER"

    const/16 v14, 0x17

    const-string v15, "R12 Pairing: Select Reader"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_SELECT_READER:Lcom/squareup/analytics/ReaderEventName;

    .line 42
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "BLE_PAIRING_SUCCESS_WITH_CONFIRMATION"

    const/16 v14, 0x18

    const-string v15, "R12 Pairing: Success with Confirmation Screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_SUCCESS_WITH_CONFIRMATION:Lcom/squareup/analytics/ReaderEventName;

    .line 43
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "BLE_REGISTERING_FOR_AUTO_CONNECT"

    const/16 v14, 0x19

    const-string v15, "R12 Pairing: Registering reader for BLE AutoConnect"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_REGISTERING_FOR_AUTO_CONNECT:Lcom/squareup/analytics/ReaderEventName;

    .line 44
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "BLUETOOTH_CONNECTION_FAILURE"

    const/16 v14, 0x1a

    const-string v15, "Bluetooth Connection Attempt Failed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLUETOOTH_CONNECTION_FAILURE:Lcom/squareup/analytics/ReaderEventName;

    .line 45
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "BLUETOOTH_CONNECTION_SUCCESS"

    const/16 v14, 0x1b

    const-string v15, "Bluetooth Connection Attempt Succeeded"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLUETOOTH_CONNECTION_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

    .line 46
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "BLUETOOTH_DISABLED"

    const/16 v14, 0x1c

    const-string v15, "Bluetooth disabled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLUETOOTH_DISABLED:Lcom/squareup/analytics/ReaderEventName;

    .line 47
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "BLUETOOTH_ENABLED"

    const/16 v14, 0x1d

    const-string v15, "Bluetooth enabled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLUETOOTH_ENABLED:Lcom/squareup/analytics/ReaderEventName;

    .line 48
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "BLUETOOTH_STATE_CHANGED"

    const/16 v14, 0x1e

    const-string v15, "Bluetooth state changed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLUETOOTH_STATE_CHANGED:Lcom/squareup/analytics/ReaderEventName;

    .line 49
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "BLUETOOTH_NOT_SUPPORTED"

    const/16 v14, 0x1f

    const-string v15, "Bluetooth not supported"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->BLUETOOTH_NOT_SUPPORTED:Lcom/squareup/analytics/ReaderEventName;

    .line 50
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CARDHOLDER_NAME_FAIL"

    const/16 v14, 0x20

    const-string v15, "Cardholder Name Fetch: Fail"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CARDHOLDER_NAME_FAIL:Lcom/squareup/analytics/ReaderEventName;

    .line 51
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CARDHOLDER_NAME_SUCCESS"

    const/16 v14, 0x21

    const-string v15, "Cardholder Name Fetch: Success"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CARDHOLDER_NAME_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

    .line 52
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CIRQUE_SECURITY_STATUS"

    const/16 v14, 0x22

    const-string v15, "Cirque Security Status: "

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CIRQUE_SECURITY_STATUS:Lcom/squareup/analytics/ReaderEventName;

    .line 53
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CONTACTLESS_PAYMENT_ACTION_REQUIRED"

    const/16 v14, 0x23

    const-string v15, "Contactless Payment: Action Required"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_ACTION_REQUIRED:Lcom/squareup/analytics/ReaderEventName;

    .line 54
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CONTACTLESS_PAYMENT_CARD_ERROR_TRY_ANOTHER_CARD"

    const/16 v14, 0x24

    const-string v15, "Contactless Payment: Card Error; Try Another Card"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_CARD_ERROR_TRY_ANOTHER_CARD:Lcom/squareup/analytics/ReaderEventName;

    .line 56
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CONTACTLESS_PAYMENT_COMPLETE_APPROVED"

    const/16 v14, 0x25

    const-string v15, "Contactless Payment Complete: Approved"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_APPROVED:Lcom/squareup/analytics/ReaderEventName;

    .line 57
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CONTACTLESS_PAYMENT_COMPLETE_APPROVED_OFFLINE"

    const/16 v14, 0x26

    const-string v15, "Contactless Payment Complete: Approved Offline"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_APPROVED_OFFLINE:Lcom/squareup/analytics/ReaderEventName;

    .line 58
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CONTACTLESS_PAYMENT_COMPLETE_DECLINED"

    const/16 v14, 0x27

    const-string v15, "Contactless Payment Complete: Declined"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_DECLINED:Lcom/squareup/analytics/ReaderEventName;

    .line 59
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CONTACTLESS_PAYMENT_COMPLETE_TERMINATED"

    const/16 v14, 0x28

    const-string v15, "Contactless Payment Complete: Canceled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/analytics/ReaderEventName;

    .line 60
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CONTACTLESS_PAYMENT_COMPLETE_TIMEOUT"

    const/16 v14, 0x29

    const-string v15, "Contactless Payment Complete: Timeout"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_TIMEOUT:Lcom/squareup/analytics/ReaderEventName;

    .line 61
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CONTACTLESS_PAYMENT_ERROR_PRESENT_AGAIN"

    const/16 v14, 0x2a

    const-string v15, "Contactless Payment: Contactless Card Error Present Card Again"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_ERROR_PRESENT_AGAIN:Lcom/squareup/analytics/ReaderEventName;

    .line 63
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CONTACTLESS_PAYMENT_FALLBACK_INSERT_OR_SWIPE"

    const/16 v14, 0x2b

    const-string v15, "Contactless Payment Fallback: Insert Or Swipe Instead"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_FALLBACK_INSERT_OR_SWIPE:Lcom/squareup/analytics/ReaderEventName;

    .line 65
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_TRY_ANOTHER_CARD"

    const/16 v14, 0x2c

    const-string v15, "Contactless Payment: Limit Exceeded Try Another Card"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_TRY_ANOTHER_CARD:Lcom/squareup/analytics/ReaderEventName;

    .line 67
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_INSERT_CARD"

    const/16 v14, 0x2d

    const-string v15, "Contactless Payment: Limit Exceeded Insert Card"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_INSERT_CARD:Lcom/squareup/analytics/ReaderEventName;

    .line 69
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CONTACTLESS_PAYMENT_MULTIPLE_CARDS"

    const/16 v14, 0x2e

    const-string v15, "Contactless Payment: Multiple Cards Presented"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_MULTIPLE_CARDS:Lcom/squareup/analytics/ReaderEventName;

    .line 70
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CONTACTLESS_PAYMENT_STARTING_PAYMENT"

    const/16 v14, 0x2f

    const-string v15, "Starting Contactless Payment on Reader"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_STARTING_PAYMENT:Lcom/squareup/analytics/ReaderEventName;

    .line 71
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CONTACTLESS_PAYMENT_STARTING_REFUND"

    const/16 v14, 0x30

    const-string v15, "Starting Contactless Refund on Reader"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_STARTING_REFUND:Lcom/squareup/analytics/ReaderEventName;

    .line 72
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "CONTACTLESS_PAYMENT_UNLOCK_DEVICE"

    const/16 v14, 0x31

    const-string v15, "Contactless Payment: Unlock Phone To Pay"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_UNLOCK_DEVICE:Lcom/squareup/analytics/ReaderEventName;

    .line 73
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "FW_UPDATE_CANCELED"

    const/16 v14, 0x32

    const-string v15, "Firmware Update Canceled by User"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_CANCELED:Lcom/squareup/analytics/ReaderEventName;

    .line 74
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "FW_UPDATE_FAILURE"

    const/16 v14, 0x33

    const-string v15, "Firmware Update Failed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_FAILURE:Lcom/squareup/analytics/ReaderEventName;

    .line 80
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "FW_UPDATE_FAILURE_REASON"

    const/16 v14, 0x34

    const-string v15, "Firmware Update Failed Due to Reader Reason: "

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_FAILURE_REASON:Lcom/squareup/analytics/ReaderEventName;

    .line 81
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "FW_UPDATE_REBOOT"

    const/16 v14, 0x35

    const-string v15, "Firmware Update Reader Reboot"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_REBOOT:Lcom/squareup/analytics/ReaderEventName;

    .line 82
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "FW_UPDATE_RECEIVED"

    const/16 v14, 0x36

    const-string v15, "Received Firmware Update from Server"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_RECEIVED:Lcom/squareup/analytics/ReaderEventName;

    .line 83
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "FW_UPDATE_REQUIRED"

    const/16 v14, 0x37

    const-string v15, "Firmware Update Required"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_REQUIRED:Lcom/squareup/analytics/ReaderEventName;

    .line 84
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "FW_UPDATE_SERVER_ERROR"

    const/16 v14, 0x38

    const-string v15, "Firmware Update: Error From Server"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_SERVER_ERROR:Lcom/squareup/analytics/ReaderEventName;

    .line 85
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "FW_UPDATE_SUCCESS"

    const/16 v14, 0x39

    const-string v15, "Firmware Successfully Updated on Reader"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

    .line 86
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "FW_UPDATE_SUGGESTED"

    const/16 v14, 0x3a

    const-string v15, "Firmware Update Suggested"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_SUGGESTED:Lcom/squareup/analytics/ReaderEventName;

    .line 87
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "INIT_CORE_DUMP_FOUND"

    const/16 v14, 0x3b

    const-string v15, "Core Dump Found"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_CORE_DUMP_FOUND:Lcom/squareup/analytics/ReaderEventName;

    .line 88
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "INIT_CORE_DUMP_NOT_FOUND"

    const/16 v14, 0x3c

    const-string v15, "Core Dump Not Found"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_CORE_DUMP_NOT_FOUND:Lcom/squareup/analytics/ReaderEventName;

    .line 89
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "INIT_FULL_COMMS"

    const/16 v14, 0x3d

    const-string v15, "Full Comms"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_FULL_COMMS:Lcom/squareup/analytics/ReaderEventName;

    .line 90
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "INIT_READER_CONNECTION_TIMEOUT"

    const/16 v14, 0x3e

    const-string v15, "Connection Timeout"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_READER_CONNECTION_TIMEOUT:Lcom/squareup/analytics/ReaderEventName;

    .line 91
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "INIT_READY_FOR_PAYMENTS"

    const/16 v14, 0x3f

    const-string v15, "Ready for Payments"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_READY_FOR_PAYMENTS:Lcom/squareup/analytics/ReaderEventName;

    .line 92
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "INIT_REGISTER_UPGRADE_REQUIRED"

    const/16 v14, 0x40

    const-string v15, "Register Update Required"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_REGISTER_UPGRADE_REQUIRED:Lcom/squareup/analytics/ReaderEventName;

    .line 93
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "INIT_SEND_FW_MANIFEST_UP"

    const/16 v14, 0x41

    const-string v15, "Send Firmware Manifest to Server"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_SEND_FW_MANIFEST_UP:Lcom/squareup/analytics/ReaderEventName;

    .line 94
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "INIT_SEND_SS_CONTENTS"

    const/16 v14, 0x42

    const-string v15, "Send Secure Session Contents to Server"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_SEND_SS_CONTENTS:Lcom/squareup/analytics/ReaderEventName;

    .line 95
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "INIT_SS_DENIED_BY_SERVER"

    const/16 v14, 0x43

    const-string v15, "Secure Session Denied By Server"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_SS_DENIED_BY_SERVER:Lcom/squareup/analytics/ReaderEventName;

    .line 96
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "INIT_SS_REVOKED"

    const/16 v14, 0x44

    const-string v15, "Secure Session Revoked"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_SS_REVOKED:Lcom/squareup/analytics/ReaderEventName;

    .line 97
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "INIT_SS_INVALID"

    const/16 v14, 0x45

    const-string v15, "Secure Session Invalid"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_SS_INVALID:Lcom/squareup/analytics/ReaderEventName;

    .line 98
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "INIT_SS_VALID"

    const/16 v14, 0x46

    const-string v15, "Secure Session Success"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_SS_VALID:Lcom/squareup/analytics/ReaderEventName;

    .line 99
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "INIT_TAMPER_DATA"

    const/16 v14, 0x47

    const-string v15, "Tamper Data Received"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_TAMPER_DATA:Lcom/squareup/analytics/ReaderEventName;

    .line 100
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "INIT_TAMPER_FLAGGED"

    const/16 v14, 0x48

    const-string v15, "Tamper Flagged"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_TAMPER_FLAGGED:Lcom/squareup/analytics/ReaderEventName;

    .line 101
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "INIT_TAMPER_FOUND"

    const/16 v14, 0x49

    const-string v15, "Tamper Found"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_TAMPER_FOUND:Lcom/squareup/analytics/ReaderEventName;

    .line 102
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "INIT_TAMPER_NOT_FOUND"

    const/16 v14, 0x4a

    const-string v15, "Tamper Not Found"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->INIT_TAMPER_NOT_FOUND:Lcom/squareup/analytics/ReaderEventName;

    .line 103
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_APPLICATION_SELECTED"

    const/16 v14, 0x4b

    const-string v15, "Application Selected"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_APPLICATION_SELECTED:Lcom/squareup/analytics/ReaderEventName;

    .line 104
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_APPLICATION_SELECTION"

    const/16 v14, 0x4c

    const-string v15, "Application Selection"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_APPLICATION_SELECTION:Lcom/squareup/analytics/ReaderEventName;

    .line 105
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_CARD_ERROR"

    const/16 v14, 0x4d

    const-string v15, "Card Error"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_ERROR:Lcom/squareup/analytics/ReaderEventName;

    .line 106
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_CARD_INSERTED"

    const/16 v14, 0x4e

    const-string v15, "Card Inserted"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_INSERTED:Lcom/squareup/analytics/ReaderEventName;

    .line 107
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_CARD_INSERTED_AWAITING_FW_UPDATE"

    const/16 v14, 0x4f

    const-string v15, "Card Inserted; Payments Blocked by Crucial FW Update"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_INSERTED_AWAITING_FW_UPDATE:Lcom/squareup/analytics/ReaderEventName;

    .line 108
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_CARD_MUST_BE_REINSERTED"

    const/16 v14, 0x50

    const-string v15, "Card Must Be Re-Inserted"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_MUST_BE_REINSERTED:Lcom/squareup/analytics/ReaderEventName;

    .line 109
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_CARD_MUST_TAP"

    const/16 v14, 0x51

    const-string v15, "Card Must Be Tapped"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_MUST_TAP:Lcom/squareup/analytics/ReaderEventName;

    .line 110
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_CARD_REMOVED"

    const/16 v14, 0x52

    const-string v15, "Card Removed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_REMOVED:Lcom/squareup/analytics/ReaderEventName;

    .line 111
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_CARD_REMOVED_DURING_PAYMENT"

    const/16 v14, 0x53

    const-string v15, "Card Removed During Payment"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_REMOVED_DURING_PAYMENT:Lcom/squareup/analytics/ReaderEventName;

    .line 112
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_CARD_SWIPED_AWAITING_FW_UPDATE"

    const/16 v14, 0x54

    const-string v15, "Card Swiped; Payments Blocked by Crucial FW Update"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_SWIPED_AWAITING_FW_UPDATE:Lcom/squareup/analytics/ReaderEventName;

    .line 113
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_COMPLETE_APPROVED"

    const/16 v14, 0x55

    const-string v15, "Payment Complete: Approved"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_COMPLETE_APPROVED:Lcom/squareup/analytics/ReaderEventName;

    .line 114
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_COMPLETE_DECLINED"

    const/16 v14, 0x56

    const-string v15, "Payment Complete: Declined"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_COMPLETE_DECLINED:Lcom/squareup/analytics/ReaderEventName;

    .line 115
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_COMPLETE_TERMINATED"

    const/16 v14, 0x57

    const-string v15, "Payment Complete: Canceled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/analytics/ReaderEventName;

    .line 116
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_EVENT"

    const/16 v14, 0x58

    const-string v15, "Payment event"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_EVENT:Lcom/squareup/analytics/ReaderEventName;

    .line 117
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_MAGSWIPE_FAILURE"

    const/16 v14, 0x59

    const-string v15, "Magswipe Failure"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_MAGSWIPE_FAILURE:Lcom/squareup/analytics/ReaderEventName;

    .line 118
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_MAGSWIPE_PASSTHROUGH"

    const/16 v14, 0x5a

    const-string v15, "Magswipe Passthrough"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_MAGSWIPE_PASSTHROUGH:Lcom/squareup/analytics/ReaderEventName;

    .line 119
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_MAGSWIPE_SUCCESS"

    const/16 v14, 0x5b

    const-string v15, "Magswipe Success"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_MAGSWIPE_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

    .line 120
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_SCHEME_FALLBACK"

    const/16 v14, 0x5c

    const-string v15, "Scheme Fallback"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_SCHEME_FALLBACK:Lcom/squareup/analytics/ReaderEventName;

    .line 121
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_SEND_ARPC_TO_READER"

    const/16 v14, 0x5d

    const-string v15, "Send ARPC to Reader"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_SEND_ARPC_TO_READER:Lcom/squareup/analytics/ReaderEventName;

    .line 122
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_SEND_ARQC_TO_SERVER"

    const/16 v14, 0x5e

    const-string v15, "Send ARQC to Server"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_SEND_ARQC_TO_SERVER:Lcom/squareup/analytics/ReaderEventName;

    .line 123
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_STARTING_PAYMENT_ON_READER"

    const/16 v14, 0x5f

    const-string v15, "Starting Payment on Reader"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_STARTING_PAYMENT_ON_READER:Lcom/squareup/analytics/ReaderEventName;

    .line 124
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_TECHNICAL_FALLBACK"

    const/16 v14, 0x60

    const-string v15, "Technical Fallback"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_TECHNICAL_FALLBACK:Lcom/squareup/analytics/ReaderEventName;

    .line 125
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "PAYMENT_USE_CHIP_CARD"

    const/16 v14, 0x61

    const-string v15, "Use Chip Card"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_USE_CHIP_CARD:Lcom/squareup/analytics/ReaderEventName;

    .line 126
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "READER_BATTERY_HUD"

    const/16 v14, 0x62

    const-string v15, "Battery HUD Updated"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->READER_BATTERY_HUD:Lcom/squareup/analytics/ReaderEventName;

    .line 127
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "READER_BATTERY_LEVEL"

    const/16 v14, 0x63

    const-string v15, "Battery Level"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->READER_BATTERY_LEVEL:Lcom/squareup/analytics/ReaderEventName;

    .line 128
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "READER_CARRIER_DETECT"

    const/16 v14, 0x64

    const-string v15, "Reader Carrier Detect"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->READER_CARRIER_DETECT:Lcom/squareup/analytics/ReaderEventName;

    .line 129
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "READER_EVENT_LOG"

    const/16 v14, 0x65

    const-string v15, "Event Log"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->READER_EVENT_LOG:Lcom/squareup/analytics/ReaderEventName;

    .line 130
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "READER_EVENT_TAMPER"

    const/16 v14, 0x66

    const-string v15, "Tamper Info"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->READER_EVENT_TAMPER:Lcom/squareup/analytics/ReaderEventName;

    .line 131
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "READER_LOW_BATTERY"

    const/16 v14, 0x67

    const-string v15, "Low Battery"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->READER_LOW_BATTERY:Lcom/squareup/analytics/ReaderEventName;

    .line 132
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "READER_POWER_OFF"

    const/16 v14, 0x68

    const-string v15, "Power Off"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->READER_POWER_OFF:Lcom/squareup/analytics/ReaderEventName;

    .line 133
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "READER_SETTINGS_FORGET_READER"

    const/16 v14, 0x69

    const-string v15, "Forget Reader"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->READER_SETTINGS_FORGET_READER:Lcom/squareup/analytics/ReaderEventName;

    .line 134
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "READER_SETTINGS_IDENTIFY_READER"

    const/16 v14, 0x6a

    const-string v15, "Identify Reader"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->READER_SETTINGS_IDENTIFY_READER:Lcom/squareup/analytics/ReaderEventName;

    .line 135
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "READER_SETTINGS_NAME_CHANGED"

    const/16 v14, 0x6b

    const-string v15, "Nickname Changed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->READER_SETTINGS_NAME_CHANGED:Lcom/squareup/analytics/ReaderEventName;

    .line 136
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "TMN_TRANSACTION_RESULT"

    const/16 v14, 0x6c

    const-string v15, "TMN Transaction Result"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->TMN_TRANSACTION_RESULT:Lcom/squareup/analytics/ReaderEventName;

    .line 137
    new-instance v0, Lcom/squareup/analytics/ReaderEventName;

    const-string v13, "WIRELESS_RSSI_SAMPLES"

    const/16 v14, 0x6d

    const-string v15, "RSSI Samples"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/ReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->WIRELESS_RSSI_SAMPLES:Lcom/squareup/analytics/ReaderEventName;

    const/16 v0, 0x6e

    new-array v0, v0, [Lcom/squareup/analytics/ReaderEventName;

    .line 12
    sget-object v13, Lcom/squareup/analytics/ReaderEventName;->ACL_CONNECTED_FROM_APP_INITIALIZATION:Lcom/squareup/analytics/ReaderEventName;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->ACL_CONNECTED_FROM_BROADCAST:Lcom/squareup/analytics/ReaderEventName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->ACL_DISCONNECTED:Lcom/squareup/analytics/ReaderEventName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_DISCONNECTED:Lcom/squareup/analytics/ReaderEventName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_ENQUEUED:Lcom/squareup/analytics/ReaderEventName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_FAILURE:Lcom/squareup/analytics/ReaderEventName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_FORCE_UNPAIR:Lcom/squareup/analytics/ReaderEventName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_PROBLEM_FOUND:Lcom/squareup/analytics/ReaderEventName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_PROBLEM_RESOLVED:Lcom/squareup/analytics/ReaderEventName;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_SILENT_RETRY:Lcom/squareup/analytics/ReaderEventName;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_STATE_CHANGED:Lcom/squareup/analytics/ReaderEventName;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_CONNECTION_STATE_RECEIVED_ACTION:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_ALREADY_CONNECTED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_BEGIN_AUTOMATICALLY:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_CONFIRMATION_INCORRECT:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_CONFIRMATION_OK:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_FAILED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_FORGETTING_DURING_AUTO_CONNECT:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_NO_CONFIRMATION_NEEDED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_READER_DISCOVERED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_SCANNING:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_SCANNING_STOPPED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_SELECT_READER:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_SUCCESS_WITH_CONFIRMATION:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_REGISTERING_FOR_AUTO_CONNECT:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLUETOOTH_CONNECTION_FAILURE:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLUETOOTH_CONNECTION_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLUETOOTH_DISABLED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLUETOOTH_ENABLED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLUETOOTH_STATE_CHANGED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLUETOOTH_NOT_SUPPORTED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CARDHOLDER_NAME_FAIL:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CARDHOLDER_NAME_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CIRQUE_SECURITY_STATUS:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_ACTION_REQUIRED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_CARD_ERROR_TRY_ANOTHER_CARD:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_APPROVED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_APPROVED_OFFLINE:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_DECLINED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_TIMEOUT:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_ERROR_PRESENT_AGAIN:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_FALLBACK_INSERT_OR_SWIPE:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_TRY_ANOTHER_CARD:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_INSERT_CARD:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_MULTIPLE_CARDS:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_STARTING_PAYMENT:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_STARTING_REFUND:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_UNLOCK_DEVICE:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_CANCELED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_FAILURE:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_FAILURE_REASON:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_REBOOT:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_RECEIVED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_REQUIRED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_SERVER_ERROR:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_SUGGESTED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_CORE_DUMP_FOUND:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_CORE_DUMP_NOT_FOUND:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_FULL_COMMS:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_READER_CONNECTION_TIMEOUT:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_READY_FOR_PAYMENTS:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_REGISTER_UPGRADE_REQUIRED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_SEND_FW_MANIFEST_UP:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_SEND_SS_CONTENTS:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_SS_DENIED_BY_SERVER:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_SS_REVOKED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_SS_INVALID:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_SS_VALID:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_TAMPER_DATA:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_TAMPER_FLAGGED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_TAMPER_FOUND:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_TAMPER_NOT_FOUND:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_APPLICATION_SELECTED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_APPLICATION_SELECTION:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_ERROR:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_INSERTED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_INSERTED_AWAITING_FW_UPDATE:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_MUST_BE_REINSERTED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_MUST_TAP:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_REMOVED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_REMOVED_DURING_PAYMENT:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_SWIPED_AWAITING_FW_UPDATE:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_COMPLETE_APPROVED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_COMPLETE_DECLINED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_EVENT:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_MAGSWIPE_FAILURE:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_MAGSWIPE_PASSTHROUGH:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_MAGSWIPE_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_SCHEME_FALLBACK:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_SEND_ARPC_TO_READER:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_SEND_ARQC_TO_SERVER:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_STARTING_PAYMENT_ON_READER:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_TECHNICAL_FALLBACK:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_USE_CHIP_CARD:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->READER_BATTERY_HUD:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->READER_BATTERY_LEVEL:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->READER_CARRIER_DETECT:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->READER_EVENT_LOG:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->READER_EVENT_TAMPER:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->READER_LOW_BATTERY:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->READER_POWER_OFF:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->READER_SETTINGS_FORGET_READER:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->READER_SETTINGS_IDENTIFY_READER:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->READER_SETTINGS_NAME_CHANGED:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->TMN_TRANSACTION_RESULT:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->WIRELESS_RSSI_SAMPLES:Lcom/squareup/analytics/ReaderEventName;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/analytics/ReaderEventName;->$VALUES:[Lcom/squareup/analytics/ReaderEventName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 142
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 143
    iput-object p3, p0, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/analytics/ReaderEventName;
    .locals 1

    .line 12
    const-class v0, Lcom/squareup/analytics/ReaderEventName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/analytics/ReaderEventName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/analytics/ReaderEventName;
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/analytics/ReaderEventName;->$VALUES:[Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0}, [Lcom/squareup/analytics/ReaderEventName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/analytics/ReaderEventName;

    return-object v0
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    return-object v0
.end method

.method public getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;
    .locals 1

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getValueForCardReader(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/String;
    .locals 1

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
