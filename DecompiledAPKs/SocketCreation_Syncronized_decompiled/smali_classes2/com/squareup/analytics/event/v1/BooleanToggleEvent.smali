.class public Lcom/squareup/analytics/event/v1/BooleanToggleEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "BooleanToggleEvent.java"


# instance fields
.field public final on:Z


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterActionName;Z)V
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 13
    iput-boolean p2, p0, Lcom/squareup/analytics/event/v1/BooleanToggleEvent;->on:Z

    return-void
.end method
