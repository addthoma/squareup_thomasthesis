.class public abstract Lcom/squareup/analytics/NoDeepLinkHelperModule;
.super Ljava/lang/Object;
.source "NoDeepLinkHelperModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/analytics/NoDeepLinkHelperModule;",
        "",
        "()V",
        "provideDeepLinkHelper",
        "Lcom/squareup/analytics/DeepLinkHelper;",
        "helper",
        "Lcom/squareup/analytics/NoDeepLinkHelper;",
        "provideDeepLinkHelper$impl_wiring_release",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract provideDeepLinkHelper$impl_wiring_release(Lcom/squareup/analytics/NoDeepLinkHelper;)Lcom/squareup/analytics/DeepLinkHelper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
