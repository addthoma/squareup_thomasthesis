.class public final Lcom/squareup/common/card/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/common/card/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final buyer_card_brand_amex:I = 0x7f1201e1

.field public static final buyer_card_brand_amex_phrase:I = 0x7f1201e2

.field public static final buyer_card_brand_cup:I = 0x7f1201e3

.field public static final buyer_card_brand_cup_phrase:I = 0x7f1201e4

.field public static final buyer_card_brand_discover:I = 0x7f1201e5

.field public static final buyer_card_brand_discover_phrase:I = 0x7f1201e6

.field public static final buyer_card_brand_gift_card:I = 0x7f1201e7

.field public static final buyer_card_brand_gift_card_phrase:I = 0x7f1201e8

.field public static final buyer_card_brand_interac:I = 0x7f1201e9

.field public static final buyer_card_brand_interac_phrase:I = 0x7f1201ea

.field public static final buyer_card_brand_jcb:I = 0x7f1201eb

.field public static final buyer_card_brand_jcb_phrase:I = 0x7f1201ec

.field public static final buyer_card_brand_mastercard:I = 0x7f1201ed

.field public static final buyer_card_brand_square_capital_card:I = 0x7f1201ee

.field public static final buyer_card_brand_square_capital_card_phrase:I = 0x7f1201ef

.field public static final buyer_card_brand_unknown:I = 0x7f1201f0

.field public static final buyer_card_brand_unknown_phrase:I = 0x7f1201f1

.field public static final buyer_card_brand_visa:I = 0x7f1201f2

.field public static final buyer_card_brand_visa_phrase:I = 0x7f1201f3

.field public static final card_brand_amex:I = 0x7f1202f3

.field public static final card_brand_amex_short:I = 0x7f1202f4

.field public static final card_brand_amex_short_uppercase:I = 0x7f1202f5

.field public static final card_brand_cup:I = 0x7f1202f6

.field public static final card_brand_cup_uppercase:I = 0x7f1202f7

.field public static final card_brand_discover:I = 0x7f1202f8

.field public static final card_brand_discover_uppercase:I = 0x7f1202f9

.field public static final card_brand_eftpos:I = 0x7f1202fa

.field public static final card_brand_felica:I = 0x7f1202fb

.field public static final card_brand_felica_uppercase:I = 0x7f1202fc

.field public static final card_brand_interac:I = 0x7f1202fd

.field public static final card_brand_interac_uppercase:I = 0x7f1202fe

.field public static final card_brand_jcb:I = 0x7f1202ff

.field public static final card_brand_jcb_uppercase:I = 0x7f120300

.field public static final card_brand_mastercard:I = 0x7f120301

.field public static final card_brand_mastercard_uppercase:I = 0x7f120302

.field public static final card_brand_square_capital_card:I = 0x7f120303

.field public static final card_brand_square_capital_card_uppercase:I = 0x7f120304

.field public static final card_brand_unknown:I = 0x7f120305

.field public static final card_brand_unknown_uppercase:I = 0x7f120306

.field public static final card_brand_visa:I = 0x7f120307

.field public static final card_brand_visa_uppercase:I = 0x7f120308

.field public static final card_formatted_first_last:I = 0x7f12030a

.field public static final card_formatted_title_first:I = 0x7f12030b

.field public static final card_formatted_title_first_last:I = 0x7f12030c

.field public static final card_formatted_title_last:I = 0x7f12030d

.field public static final electronic_gift_card:I = 0x7f12098f

.field public static final gift_card:I = 0x7f120af8

.field public static final gift_card_uppercase:I = 0x7f120b29

.field public static final saved_card:I = 0x7f121772


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
