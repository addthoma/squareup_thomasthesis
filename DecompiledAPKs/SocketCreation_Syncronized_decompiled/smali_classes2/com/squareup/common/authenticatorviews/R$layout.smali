.class public final Lcom/squareup/common/authenticatorviews/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/common/authenticatorviews/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final device_code_auth_view:I = 0x7f0d01d7

.field public static final device_code_login_view:I = 0x7f0d01d8

.field public static final email_password_login_view:I = 0x7f0d0244

.field public static final enroll_google_auth_code_view:I = 0x7f0d025c

.field public static final enroll_google_auth_qr_view:I = 0x7f0d025d

.field public static final enroll_sms_view:I = 0x7f0d025e

.field public static final forgot_password_view:I = 0x7f0d0274

.field public static final login_alert_dialog:I = 0x7f0d033a

.field public static final login_alert_dialog_button:I = 0x7f0d033b

.field public static final merchant_picker_view:I = 0x7f0d0352

.field public static final sms_picker_view:I = 0x7f0d04db

.field public static final two_factor_details_picker_view:I = 0x7f0d057b

.field public static final unit_picker_view:I = 0x7f0d057d

.field public static final verification_code_google_auth_view:I = 0x7f0d057f

.field public static final verification_code_sms_view:I = 0x7f0d0580


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
