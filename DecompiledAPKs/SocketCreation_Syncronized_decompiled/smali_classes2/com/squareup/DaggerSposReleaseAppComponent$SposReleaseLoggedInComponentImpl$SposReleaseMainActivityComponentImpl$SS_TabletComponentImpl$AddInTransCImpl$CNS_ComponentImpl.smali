.class final Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/ui/crm/cards/CreateNoteScreen$Component;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CNS_ComponentImpl"
.end annotation


# instance fields
.field private errorsBarPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private presenterProvider:Ljavax/inject/Provider;

.field final synthetic this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;


# direct methods
.method private constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;)V
    .locals 0

    .line 29158
    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29160
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;->initialize()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    .line 29152
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;)V

    return-void
.end method

.method private initialize()V
    .locals 10

    .line 29165
    invoke-static {}, Lcom/squareup/ui/ErrorsBarPresenter_Factory;->create()Lcom/squareup/ui/ErrorsBarPresenter_Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;->errorsBarPresenterProvider:Ljavax/inject/Provider;

    .line 29166
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;->access$112400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;->errorsBarPresenterProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$22900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$14800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$9200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v8

    invoke-static {}, Lcom/squareup/thread/MainThreadModule_ProvideMainThreadEnforcerFactory;->create()Lcom/squareup/thread/MainThreadModule_ProvideMainThreadEnforcerFactory;

    move-result-object v9

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/crm/cards/CreateNoteScreen_Presenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/cards/CreateNoteScreen_Presenter_Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private injectCreateNoteView(Lcom/squareup/ui/crm/cards/CreateNoteView;)Lcom/squareup/ui/crm/cards/CreateNoteView;
    .locals 1

    .line 29183
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/CreateNoteView_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/cards/CreateNoteView;Ljava/lang/Object;)V

    return-object p1
.end method

.method private injectErrorsBarView(Lcom/squareup/ui/ErrorsBarView;)Lcom/squareup/ui/ErrorsBarView;
    .locals 1

    .line 29178
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;->errorsBarPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/ErrorsBarView_MembersInjector;->injectPresenter(Lcom/squareup/ui/ErrorsBarView;Lcom/squareup/ui/ErrorsBarPresenter;)V

    return-object p1
.end method


# virtual methods
.method public inject(Lcom/squareup/ui/ErrorsBarView;)V
    .locals 0

    .line 29171
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;->injectErrorsBarView(Lcom/squareup/ui/ErrorsBarView;)Lcom/squareup/ui/ErrorsBarView;

    return-void
.end method

.method public inject(Lcom/squareup/ui/crm/cards/CreateNoteView;)V
    .locals 0

    .line 29175
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$AddInTransCImpl$CNS_ComponentImpl;->injectCreateNoteView(Lcom/squareup/ui/crm/cards/CreateNoteView;)Lcom/squareup/ui/crm/cards/CreateNoteView;

    return-void
.end method
