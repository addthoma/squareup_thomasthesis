.class public final Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;
.super Ljava/lang/Object;
.source "RealBuyerCheckoutLauncher_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final billTipWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/tip/BillTipWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerCartFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/BuyerCartFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerLanguageSelectionWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final customerCheckoutSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/CustomerCheckoutSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentHudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentInputHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final statusBarEventManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeValidatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/SwipeValidator;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderCompleterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/CustomerCheckoutSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/BuyerCartFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/tip/BillTipWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/SwipeValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 89
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->paymentInputHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 90
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 91
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 92
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->tenderCompleterProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 93
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->customerCheckoutSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 94
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 95
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->buyerCartFormatterProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 96
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 97
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 98
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->statusBarEventManagerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 99
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 100
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->paymentHudToasterProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 101
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 102
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 103
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 104
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->billTipWorkflowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 105
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 106
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->swipeValidatorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 107
    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->buyerLanguageSelectionWorkflowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/CustomerCheckoutSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/BuyerCartFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/tip/BillTipWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/SwipeValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
            ">;)",
            "Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    .line 132
    new-instance v20, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;

    move-object/from16 v0, v20

    invoke-direct/range {v0 .. v19}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v20
.end method

.method public static newInstance(Ljavax/inject/Provider;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/buyercheckout/CustomerCheckoutSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/cart/BuyerCartFormatter;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/buyer/tip/BillTipWorkflow;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;)Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            "Lcom/squareup/buyercheckout/CustomerCheckoutSettings;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/ui/cart/BuyerCartFormatter;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/payment/PaymentHudToaster;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/hudtoaster/HudToaster;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/buyer/tip/BillTipWorkflow;",
            "Lcom/squareup/payment/tender/TenderFactory;",
            "Lcom/squareup/swipe/SwipeValidator;",
            "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
            ")",
            "Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    .line 145
    new-instance v20, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    move-object/from16 v0, v20

    invoke-direct/range {v0 .. v19}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;-><init>(Ljavax/inject/Provider;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/buyercheckout/CustomerCheckoutSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/cart/BuyerCartFormatter;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/buyer/tip/BillTipWorkflow;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;)V

    return-object v20
.end method


# virtual methods
.method public get()Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;
    .locals 21

    move-object/from16 v0, p0

    .line 112
    iget-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->paymentInputHandlerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/Transaction;

    iget-object v3, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/payment/TenderInEdit;

    iget-object v4, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->tenderCompleterProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/tenderpayment/TenderCompleter;

    iget-object v5, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->customerCheckoutSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/buyercheckout/CustomerCheckoutSettings;

    iget-object v6, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/analytics/Analytics;

    iget-object v7, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->buyerCartFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/ui/cart/BuyerCartFormatter;

    iget-object v8, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v8}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v9, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v9}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v10, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->statusBarEventManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v10}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/statusbar/event/StatusBarEventManager;

    iget-object v11, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v11}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v12, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->paymentHudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v12}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/payment/PaymentHudToaster;

    iget-object v13, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v13}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v14, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v14}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v15, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v15}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/squareup/settings/server/Features;

    move-object/from16 v20, v1

    iget-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->billTipWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/ui/buyer/tip/BillTipWorkflow;

    iget-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/payment/tender/TenderFactory;

    iget-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->swipeValidatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/swipe/SwipeValidator;

    iget-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->buyerLanguageSelectionWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;

    move-object/from16 v1, v20

    invoke-static/range {v1 .. v19}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->newInstance(Ljavax/inject/Provider;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/buyercheckout/CustomerCheckoutSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/cart/BuyerCartFormatter;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/buyer/tip/BillTipWorkflow;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;)Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher_Factory;->get()Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    move-result-object v0

    return-object v0
.end method
