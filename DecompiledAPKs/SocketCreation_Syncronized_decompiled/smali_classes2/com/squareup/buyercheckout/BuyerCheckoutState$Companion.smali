.class public final Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;
.super Ljava/lang/Object;
.source "BuyerCheckoutState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/buyercheckout/BuyerCheckoutState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBuyerCheckoutState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BuyerCheckoutState.kt\ncom/squareup/buyercheckout/BuyerCheckoutState$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,196:1\n180#2:197\n*E\n*S KotlinDebug\n*F\n+ 1 BuyerCheckoutState.kt\ncom/squareup/buyercheckout/BuyerCheckoutState$Companion\n*L\n154#1:197\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0004H\u0002J\u000e\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;",
        "",
        "()V",
        "fromSnapshot",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "bytes",
        "Lokio/ByteString;",
        "snapshotState",
        "Lcom/squareup/workflow/Snapshot;",
        "state",
        "startingState",
        "settings",
        "Lcom/squareup/buyercheckout/BuyerCheckoutSettings;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 102
    invoke-direct {p0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$snapshotState(Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;Lcom/squareup/buyercheckout/BuyerCheckoutState;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 102
    invoke-direct {p0, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;->snapshotState(Lcom/squareup/buyercheckout/BuyerCheckoutState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p0

    return-object p0
.end method

.method private final snapshotState(Lcom/squareup/buyercheckout/BuyerCheckoutState;)Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 111
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;

    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;

    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;->getFromState()Lcom/squareup/buyercheckout/BuyerCheckoutState;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;->snapshotState(Lcom/squareup/buyercheckout/BuyerCheckoutState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1

    .line 113
    :cond_0
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion$snapshotState$1;

    invoke-direct {v1, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion$snapshotState$1;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public final fromSnapshot(Lokio/ByteString;)Lcom/squareup/buyercheckout/BuyerCheckoutState;
    .locals 8

    const-string v0, "bytes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 155
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 156
    const-class v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    invoke-direct {v0, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;-><init>(Z)V

    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    goto/16 :goto_0

    .line 158
    :cond_0
    const-class v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;

    .line 159
    sget-object v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData$Companion;->fromSnapshot(Lokio/BufferedSource;)Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object p1

    .line 158
    invoke-direct {v0, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;)V

    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    goto/16 :goto_0

    .line 162
    :cond_1
    const-class v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;

    .line 163
    sget-object v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData$Companion;->fromSnapshot(Lokio/BufferedSource;)Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object p1

    .line 162
    invoke-direct {v0, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;)V

    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    goto/16 :goto_0

    .line 166
    :cond_2
    const-class v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;

    .line 167
    sget-object v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData$Companion;->fromSnapshot(Lokio/BufferedSource;)Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object p1

    .line 166
    invoke-direct {v0, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;)V

    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    goto/16 :goto_0

    .line 170
    :cond_3
    const-class v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    .line 171
    sget-object v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData$Companion;->fromSnapshot(Lokio/BufferedSource;)Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object v1

    .line 172
    sget-object v2, Lcom/squareup/ui/buyer/tip/BillTipWorkflow;->Companion:Lcom/squareup/ui/buyer/tip/BillTipWorkflow$Companion;

    .line 173
    sget-object v3, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v3, p1}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    .line 172
    invoke-virtual {v2, p1}, Lcom/squareup/ui/buyer/tip/BillTipWorkflow$Companion;->legacyHandle(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    .line 170
    invoke-direct {v0, v1, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    goto :goto_0

    .line 177
    :cond_4
    const-class v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;

    .line 178
    sget-object v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData$Companion;->fromSnapshot(Lokio/BufferedSource;)Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object v1

    .line 179
    sget-object v2, Lcom/squareup/buyercheckout/BuyerCheckoutState;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;->fromSnapshot(Lokio/ByteString;)Lcom/squareup/buyercheckout/BuyerCheckoutState;

    move-result-object v2

    .line 180
    sget-object v3, Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;->Companion:Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion;

    .line 181
    sget-object v4, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    invoke-virtual {v3, p1}, Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion;->legacyHandle(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    .line 177
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    goto :goto_0

    .line 184
    :cond_5
    const-class v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;

    .line 185
    sget-object v1, Lcom/squareup/buyercheckout/BuyerCheckoutState;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;->fromSnapshot(Lokio/ByteString;)Lcom/squareup/buyercheckout/BuyerCheckoutState;

    move-result-object v3

    .line 186
    sget-object v1, Lcom/squareup/buyercheckout/BuyerCheckoutState;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;->fromSnapshot(Lokio/ByteString;)Lcom/squareup/buyercheckout/BuyerCheckoutState;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, v0

    .line 184
    invoke-direct/range {v2 .. v7}, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/eventstream/v1/EventStreamEvent;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    :goto_0
    return-object v0

    .line 189
    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 190
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid class name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 189
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final startingState(Lcom/squareup/buyercheckout/BuyerCheckoutSettings;)Lcom/squareup/buyercheckout/BuyerCheckoutState;
    .locals 1

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;

    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutSettings;->getSkipCart()Z

    move-result p1

    invoke-direct {v0, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;-><init>(Z)V

    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    return-object v0
.end method
