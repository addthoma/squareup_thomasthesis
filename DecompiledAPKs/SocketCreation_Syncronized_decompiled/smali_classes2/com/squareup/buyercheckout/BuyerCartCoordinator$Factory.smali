.class public final Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;
.super Ljava/lang/Object;
.source "BuyerCartCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/buyercheckout/BuyerCartCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ$\u0010\u000b\u001a\u00020\u000c2\u001c\u0010\r\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000fj\u0002`\u00120\u000eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;",
        "",
        "formattedTotalProvider",
        "Lcom/squareup/ui/buyer/FormattedTotalProvider;",
        "buyerCartFormatter",
        "Lcom/squareup/ui/cart/BuyerCartFormatter;",
        "res",
        "Lcom/squareup/util/Res;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "(Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/ui/cart/BuyerCartFormatter;Lcom/squareup/util/Res;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V",
        "build",
        "Lcom/squareup/buyercheckout/BuyerCartCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/buyercheckout/BuyerCart$ScreenData;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "Lcom/squareup/buyercheckout/BuyerCartScreen;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerCartFormatter:Lcom/squareup/ui/cart/BuyerCartFormatter;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final formattedTotalProvider:Lcom/squareup/ui/buyer/FormattedTotalProvider;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/ui/cart/BuyerCartFormatter;Lcom/squareup/util/Res;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "formattedTotalProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerCartFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLocaleOverride"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;->formattedTotalProvider:Lcom/squareup/ui/buyer/FormattedTotalProvider;

    iput-object p2, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;->buyerCartFormatter:Lcom/squareup/ui/cart/BuyerCartFormatter;

    iput-object p3, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-void
.end method


# virtual methods
.method public final build(Lio/reactivex/Observable;)Lcom/squareup/buyercheckout/BuyerCartCoordinator;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/buyercheckout/BuyerCart$ScreenData;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            ">;>;)",
            "Lcom/squareup/buyercheckout/BuyerCartCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;

    .line 49
    iget-object v3, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;->formattedTotalProvider:Lcom/squareup/ui/buyer/FormattedTotalProvider;

    iget-object v4, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;->buyerCartFormatter:Lcom/squareup/ui/cart/BuyerCartFormatter;

    iget-object v5, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iget-object v6, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p1

    .line 48
    invoke-direct/range {v1 .. v7}, Lcom/squareup/buyercheckout/BuyerCartCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/ui/cart/BuyerCartFormatter;Lcom/squareup/util/Res;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
