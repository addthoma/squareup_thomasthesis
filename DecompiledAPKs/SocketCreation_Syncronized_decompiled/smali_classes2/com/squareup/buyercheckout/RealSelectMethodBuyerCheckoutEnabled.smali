.class public final Lcom/squareup/buyercheckout/RealSelectMethodBuyerCheckoutEnabled;
.super Ljava/lang/Object;
.source "RealSelectMethodBuyerCheckoutEnabled.kt"

# interfaces
.implements Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\u0008H\u0002J\u0008\u0010\n\u001a\u00020\u0008H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/RealSelectMethodBuyerCheckoutEnabled;",
        "Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;",
        "cardReaderHubUtils",
        "Lcom/squareup/cardreader/CardReaderHubUtils;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/settings/server/Features;)V",
        "isEnabled",
        "",
        "maybeCheckPaymentReadySmartReaderIsAvailable",
        "shouldCheckPaymentReadySmartReaderIsAvailable",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardReaderHubUtils"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/buyercheckout/RealSelectMethodBuyerCheckoutEnabled;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    iput-object p2, p0, Lcom/squareup/buyercheckout/RealSelectMethodBuyerCheckoutEnabled;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private final maybeCheckPaymentReadySmartReaderIsAvailable()Z
    .locals 1

    .line 23
    invoke-direct {p0}, Lcom/squareup/buyercheckout/RealSelectMethodBuyerCheckoutEnabled;->shouldCheckPaymentReadySmartReaderIsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealSelectMethodBuyerCheckoutEnabled;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHubUtils;->isPaymentReadySmartReaderConnected()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method private final shouldCheckPaymentReadySmartReaderIsAvailable()Z
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealSelectMethodBuyerCheckoutEnabled;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SPE_FWUP_WITHOUT_MATCHING_TMS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public isEnabled()Z
    .locals 2

    .line 15
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealSelectMethodBuyerCheckoutEnabled;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BUYER_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/buyercheckout/RealSelectMethodBuyerCheckoutEnabled;->maybeCheckPaymentReadySmartReaderIsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
