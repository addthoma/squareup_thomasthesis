.class Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner$1;
.super Ljava/lang/Object;
.source "UsbBarcodeScannerDiscoverer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;

.field final synthetic val$endpoint:Landroid/hardware/usb/UsbEndpoint;


# direct methods
.method constructor <init>(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;Landroid/hardware/usb/UsbEndpoint;)V
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner$1;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;

    iput-object p2, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner$1;->val$endpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .line 118
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const/16 v2, 0x8

    new-array v3, v2, [B

    .line 123
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner$1;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;

    .line 124
    invoke-static {v4}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->access$000(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner$1;->val$endpoint:Landroid/hardware/usb/UsbEndpoint;

    const/16 v6, 0x3e8

    invoke-virtual {v4, v5, v3, v2, v6}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eq v4, v2, :cond_2

    .line 127
    iget-object v4, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner$1;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;

    invoke-static {v4}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->access$000(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner$1;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;

    invoke-static {v4}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->access$000(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v4

    invoke-virtual {v4}, Landroid/hardware/usb/UsbDeviceConnection;->getRawDescriptors()[B

    move-result-object v4

    if-nez v4, :cond_0

    :cond_1
    new-array v0, v5, [Ljava/lang/Object;

    .line 131
    iget-object v1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner$1;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;

    invoke-static {v1}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->access$100(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;)Landroid/hardware/usb/UsbDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    const-string v1, "Unable to get raw descriptors for %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :cond_2
    const/4 v4, 0x2

    .line 140
    aget-byte v7, v3, v4

    if-nez v7, :cond_3

    goto :goto_0

    .line 152
    :cond_3
    aget-byte v8, v3, v6

    if-ne v8, v4, :cond_4

    const/4 v4, 0x1

    goto :goto_1

    :cond_4
    if-nez v8, :cond_6

    const/4 v4, 0x0

    .line 170
    :goto_1
    invoke-static {v7, v4}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$ScanCodeTranslator;->translateScanCodeToChar(BZ)C

    move-result v4

    .line 180
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    sub-long/2addr v7, v0

    const-wide/16 v9, 0x1f4

    cmp-long v11, v7, v9

    if-gez v11, :cond_5

    goto :goto_2

    :cond_5
    const/4 v5, 0x0

    :goto_2
    if-eqz v4, :cond_0

    if-nez v5, :cond_0

    .line 185
    iget-object v5, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner$1;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;

    invoke-static {v5}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->access$200(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;)Lcom/squareup/barcodescanners/BarcodeScanner$Listener;

    move-result-object v5

    invoke-interface {v5, v4}, Lcom/squareup/barcodescanners/BarcodeScanner$Listener;->characterScanned(C)V

    goto :goto_0

    :cond_6
    new-array v0, v5, [Ljava/lang/Object;

    .line 166
    invoke-static {v8}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    aput-object v1, v0, v6

    const-string v1, "Barcode scanner received an unexpected modifier byte value %s"

    .line 165
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_3
    new-array v0, v5, [Ljava/lang/Object;

    .line 189
    iget-object v1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner$1;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;

    invoke-static {v1}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->access$100(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;)Landroid/hardware/usb/UsbDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    const-string v1, "Barcode scanner loop ended for %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 194
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner$1;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;

    invoke-static {v0}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->access$000(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 195
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner$1;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;

    invoke-static {v0}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->access$000(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDeviceConnection;->close()V

    .line 198
    :cond_7
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner$1;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;

    invoke-static {v0}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->access$200(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;)Lcom/squareup/barcodescanners/BarcodeScanner$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/barcodescanners/BarcodeScanner$Listener;->barcodeScannerDisconnected()V

    return-void
.end method
