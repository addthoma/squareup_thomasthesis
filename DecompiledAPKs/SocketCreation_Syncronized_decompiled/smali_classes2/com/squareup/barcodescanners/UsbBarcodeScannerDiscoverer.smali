.class public Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;
.super Ljava/lang/Object;
.source "UsbBarcodeScannerDiscoverer.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$ScanCodeTranslator;,
        Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScannerListener;,
        Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbDeviceListener;,
        Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;
    }
.end annotation


# static fields
.field private static final CONTROL_ENDPOINT_NUMBER:I = 0x0

.field private static final FORCE_DISCONNECT_USB_DEVICE_FROM_KERNEL:Z = true

.field private static final HID_KEYBOARD_REPORT_SIZE_BYTES:I = 0x8

.field static final HONEYWELL_VENDOR_ID:I = 0xc2e

.field private static final INTERFACE_INDEX:I = 0x0

.field static final SYMBOL_VENDOR_ID:I = 0x5e0


# instance fields
.field private final backgroundThreadExecutor:Ljava/util/concurrent/Executor;

.field private final barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

.field private deviceMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Landroid/hardware/usb/UsbDevice;",
            "Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

.field private final usbManager:Lcom/squareup/hardware/usb/UsbManager;


# direct methods
.method public constructor <init>(Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Ljava/util/concurrent/Executor;Lcom/squareup/util/Res;)V
    .locals 0

    .line 281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282
    iput-object p1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    .line 283
    iput-object p2, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    .line 284
    iput-object p3, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->usbManager:Lcom/squareup/hardware/usb/UsbManager;

    .line 285
    iput-object p4, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->backgroundThreadExecutor:Ljava/util/concurrent/Executor;

    .line 286
    iput-object p5, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->res:Lcom/squareup/util/Res;

    .line 288
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->deviceMap:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)Ljava/util/Map;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->deviceMap:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)Lcom/squareup/hardware/usb/UsbManager;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->usbManager:Lcom/squareup/hardware/usb/UsbManager;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)Ljava/util/concurrent/Executor;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->backgroundThreadExecutor:Ljava/util/concurrent/Executor;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)Lcom/squareup/util/Res;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)Lcom/squareup/barcodescanners/BarcodeScannerTracker;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    return-object p0
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 292
    new-instance p1, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbDeviceListener;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbDeviceListener;-><init>(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$1;)V

    .line 293
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    const/16 v1, 0xc2e

    invoke-virtual {v0, v1, p1}, Lcom/squareup/usb/UsbDiscoverer;->setDeviceListenerForVendorId(ILcom/squareup/usb/UsbDiscoverer$DeviceListener;)V

    .line 294
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    const/16 v1, 0x5e0

    invoke-virtual {v0, v1, p1}, Lcom/squareup/usb/UsbDiscoverer;->setDeviceListenerForVendorId(ILcom/squareup/usb/UsbDiscoverer$DeviceListener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 298
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    const/16 v1, 0xc2e

    invoke-virtual {v0, v1}, Lcom/squareup/usb/UsbDiscoverer;->removeDeviceListenerForVendorId(I)V

    .line 299
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    const/16 v1, 0x5e0

    invoke-virtual {v0, v1}, Lcom/squareup/usb/UsbDiscoverer;->removeDeviceListenerForVendorId(I)V

    return-void
.end method
