.class final Lcom/squareup/BundleKey$5;
.super Lcom/squareup/BundleKey;
.source "BundleKey.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/BundleKey;->string(Ljava/lang/String;)Lcom/squareup/BundleKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/BundleKey<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 173
    invoke-direct {p0, p1, v0}, Lcom/squareup/BundleKey;-><init>(Ljava/lang/String;Lcom/squareup/BundleKey$1;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic get(Landroid/content/Intent;)Ljava/lang/Object;
    .locals 0

    .line 173
    invoke-virtual {p0, p1}, Lcom/squareup/BundleKey$5;->get(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic get(Landroid/os/Bundle;)Ljava/lang/Object;
    .locals 0

    .line 173
    invoke-virtual {p0, p1}, Lcom/squareup/BundleKey$5;->get(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public get(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/squareup/BundleKey$5;->key:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public get(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/BundleKey$5;->key:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic put(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0

    .line 173
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/BundleKey$5;->put(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method public put(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/BundleKey$5;->key:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-void
.end method

.method public bridge synthetic put(Landroid/os/Bundle;Ljava/lang/Object;)V
    .locals 0

    .line 173
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/BundleKey$5;->put(Landroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method

.method public put(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/squareup/BundleKey$5;->key:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
