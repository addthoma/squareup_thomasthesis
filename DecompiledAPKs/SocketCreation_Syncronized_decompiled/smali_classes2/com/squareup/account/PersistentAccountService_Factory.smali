.class public final Lcom/squareup/account/PersistentAccountService_Factory;
.super Ljava/lang/Object;
.source "PersistentAccountService_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/account/PersistentAccountService;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/AccountService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LogInResponseCache;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/singlesignon/SingleSignOn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/AccountService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LogInResponseCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/singlesignon/SingleSignOn;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/account/PersistentAccountService_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/account/PersistentAccountService_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/account/PersistentAccountService_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 31
    iput-object p4, p0, Lcom/squareup/account/PersistentAccountService_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/account/PersistentAccountService_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/AccountService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LogInResponseCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/singlesignon/SingleSignOn;",
            ">;)",
            "Lcom/squareup/account/PersistentAccountService_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/account/PersistentAccountService_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/account/PersistentAccountService_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ldagger/Lazy;Lcom/squareup/account/LogInResponseCache;Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/singlesignon/SingleSignOn;)Lcom/squareup/account/PersistentAccountService;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/server/account/AccountService;",
            ">;",
            "Lcom/squareup/account/LogInResponseCache;",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            "Lcom/squareup/singlesignon/SingleSignOn;",
            ")",
            "Lcom/squareup/account/PersistentAccountService;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/account/PersistentAccountService;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/account/PersistentAccountService;-><init>(Ldagger/Lazy;Lcom/squareup/account/LogInResponseCache;Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/singlesignon/SingleSignOn;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/account/PersistentAccountService;
    .locals 4

    .line 36
    iget-object v0, p0, Lcom/squareup/account/PersistentAccountService_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/account/PersistentAccountService_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/account/LogInResponseCache;

    iget-object v2, p0, Lcom/squareup/account/PersistentAccountService_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/accountstatus/PersistentAccountStatusService;

    iget-object v3, p0, Lcom/squareup/account/PersistentAccountService_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/singlesignon/SingleSignOn;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/account/PersistentAccountService_Factory;->newInstance(Ldagger/Lazy;Lcom/squareup/account/LogInResponseCache;Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/singlesignon/SingleSignOn;)Lcom/squareup/account/PersistentAccountService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/account/PersistentAccountService_Factory;->get()Lcom/squareup/account/PersistentAccountService;

    move-result-object v0

    return-object v0
.end method
