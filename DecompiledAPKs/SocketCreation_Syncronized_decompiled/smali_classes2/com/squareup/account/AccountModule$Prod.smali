.class public Lcom/squareup/account/AccountModule$Prod;
.super Ljava/lang/Object;
.source "AccountModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/account/AccountModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Prod"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideSessionId(Lcom/squareup/account/SessionIdPIIProvider;)Ljava/lang/String;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 17
    invoke-interface {p0}, Lcom/squareup/account/SessionIdPIIProvider;->getSessionIdPII()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
