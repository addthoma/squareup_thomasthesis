.class public final Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;
.super Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;
.source "LoggedInStatusProvider.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoggedIn"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\n2\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\t\u001a\u00020\nX\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;",
        "Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;",
        "authToken",
        "",
        "response",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V",
        "getAuthToken",
        "()Ljava/lang/String;",
        "isLoggedIn",
        "",
        "()Z",
        "getResponse",
        "()Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "component1",
        "component2",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final authToken:Ljava/lang/String;

.field private final isLoggedIn:Z

.field private final response:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 1

    const-string v0, "authToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "response"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, v0}, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->authToken:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->response:Lcom/squareup/server/account/protos/AccountStatusResponse;

    const/4 p1, 0x1

    .line 33
    iput-boolean p1, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->isLoggedIn:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;ILjava/lang/Object;)Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->authToken:Ljava/lang/String;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->response:Lcom/squareup/server/account/protos/AccountStatusResponse;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->copy(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->authToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 1

    iget-object v0, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->response:Lcom/squareup/server/account/protos/AccountStatusResponse;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;
    .locals 1

    const-string v0, "authToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "response"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;

    invoke-direct {v0, p1, p2}, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;-><init>(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;

    iget-object v0, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->authToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->authToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->response:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object p1, p1, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->response:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAuthToken()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->authToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->response:Lcom/squareup/server/account/protos/AccountStatusResponse;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->authToken:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->response:Lcom/squareup/server/account/protos/AccountStatusResponse;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public isLoggedIn()Z
    .locals 1

    .line 33
    iget-boolean v0, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->isLoggedIn:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LoggedIn(authToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->authToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;->response:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
