.class public final Lcom/squareup/account/LoggedInStatusProvider$DefaultImpls;
.super Ljava/lang/Object;
.source "LoggedInStatusProvider.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/account/LoggedInStatusProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static lastLoggedInStatus(Lcom/squareup/account/LoggedInStatusProvider;)Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;
    .locals 0

    .line 21
    invoke-interface {p0}, Lcom/squareup/account/LoggedInStatusProvider;->loggedInStatus()Lio/reactivex/Observable;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/util/rx2/Rx2BlockingSupport;->getValueOrThrow(Lio/reactivex/Observable;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;

    return-object p0
.end method
