.class public Lcom/squareup/account/ServerClock;
.super Ljava/lang/Object;
.source "ServerClock.java"

# interfaces
.implements Lmortar/Scoped;


# static fields
.field private static final THRESHOLD_MILLIS:J = 0xea60L

.field private static final UNRELIABLE_CLOCK_TIME:J


# instance fields
.field private final bootTimeMillis:Ljava/util/concurrent/atomic/AtomicLong;

.field private final clock:Lcom/squareup/util/Clock;

.field private final logger:Lcom/squareup/logging/RemoteLogger;

.field private final serverTimeMinusElapsedRealtime:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/util/Clock;Lcom/squareup/logging/RemoteLogger;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/logging/RemoteLogger;",
            ")V"
        }
    .end annotation

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/account/ServerClock;->serverTimeMinusElapsedRealtime:Lio/reactivex/Observable;

    .line 63
    iput-object p2, p0, Lcom/squareup/account/ServerClock;->clock:Lcom/squareup/util/Clock;

    .line 64
    iput-object p3, p0, Lcom/squareup/account/ServerClock;->logger:Lcom/squareup/logging/RemoteLogger;

    .line 66
    new-instance p1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-interface {p2}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    invoke-interface {p2}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide p2

    sub-long/2addr v0, p2

    invoke-direct {p1, v0, v1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object p1, p0, Lcom/squareup/account/ServerClock;->bootTimeMillis:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method


# virtual methods
.method public getCurrentTime()Ljava/util/Date;
    .locals 9

    .line 83
    iget-object v0, p0, Lcom/squareup/account/ServerClock;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    .line 84
    iget-object v2, p0, Lcom/squareup/account/ServerClock;->bootTimeMillis:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    iget-object v4, p0, Lcom/squareup/account/ServerClock;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v4}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v4

    add-long/2addr v2, v4

    sub-long v4, v2, v0

    .line 85
    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    const-wide/32 v6, 0xea60

    cmp-long v8, v4, v6

    if-gez v8, :cond_0

    move-wide v2, v0

    :cond_0
    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-gez v6, :cond_1

    .line 92
    iget-object v2, p0, Lcom/squareup/account/ServerClock;->logger:Lcom/squareup/logging/RemoteLogger;

    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unexpected system clock value: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/squareup/account/ServerClock;->clock:Lcom/squareup/util/Clock;

    .line 93
    invoke-interface {v6}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v0, v1

    const/4 v1, 0x1

    iget-object v6, p0, Lcom/squareup/account/ServerClock;->clock:Lcom/squareup/util/Clock;

    .line 94
    invoke-interface {v6}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v0, v1

    const-string v1, "currentTimeMillis: %d, elapsedRealtime: %d"

    .line 93
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 92
    invoke-interface {v2, v3, v0}, Lcom/squareup/logging/RemoteLogger;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    move-wide v2, v4

    .line 97
    :cond_1
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public synthetic lambda$onEnterScope$0$ServerClock(Ljava/lang/Long;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 73
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 74
    iget-object v0, p0, Lcom/squareup/account/ServerClock;->bootTimeMillis:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/account/ServerClock;->serverTimeMinusElapsedRealtime:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/account/-$$Lambda$ServerClock$zYs9TK6juWKLdGvn8YSZbYYxwa8;

    invoke-direct {v1, p0}, Lcom/squareup/account/-$$Lambda$ServerClock$zYs9TK6juWKLdGvn8YSZbYYxwa8;-><init>(Lcom/squareup/account/ServerClock;)V

    .line 72
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 70
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
