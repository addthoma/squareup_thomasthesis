.class public final Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedOut;
.super Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;
.source "LoggedInStatusProvider.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoggedOut"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0003\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedOut;",
        "Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;",
        "()V",
        "isLoggedIn",
        "",
        "()Z",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedOut;

.field private static final isLoggedIn:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedOut;

    invoke-direct {v0}, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedOut;-><init>()V

    sput-object v0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedOut;->INSTANCE:Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedOut;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 39
    invoke-direct {p0, v0}, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public isLoggedIn()Z
    .locals 1

    .line 40
    sget-boolean v0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedOut;->isLoggedIn:Z

    return v0
.end method
