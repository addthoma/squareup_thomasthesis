.class public final Lcom/squareup/account/SecretStringKt;
.super Ljava/lang/Object;
.source "SecretString.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "toSecretString",
        "Lcom/squareup/account/SecretString;",
        "",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toSecretString(Ljava/lang/CharSequence;)Lcom/squareup/account/SecretString;
    .locals 1

    const-string v0, "$this$toSecretString"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Lcom/squareup/account/SecretString;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/account/SecretString;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
