.class final Lcom/squareup/salesreport/RealSalesReportWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSalesReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/RealSalesReportWorkflow;->render(Lcom/squareup/salesreport/SalesReportProps;Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
        "+",
        "Lcom/squareup/customreport/data/SalesReport;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/salesreport/SalesReportState;",
        "+",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/salesreport/SalesReportState;",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        "response",
        "Lcom/squareup/customreport/data/SalesReportRepository$Result;",
        "Lcom/squareup/customreport/data/SalesReport;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/salesreport/SalesReportState;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/SalesReportState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$1;->$state:Lcom/squareup/salesreport/SalesReportState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/customreport/data/SalesReportRepository$Result;)Lcom/squareup/workflow/WorkflowAction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "+",
            "Lcom/squareup/customreport/data/SalesReport;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/salesreport/SalesReportState;",
            "Lcom/squareup/salesreport/SalesReportOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    instance-of v0, p1, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 170
    check-cast p1, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    invoke-virtual {p1}, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/customreport/data/SalesReport;

    .line 171
    instance-of v0, p1, Lcom/squareup/customreport/data/NoSalesReport;

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/salesreport/SalesReportState$EmptyReport;

    .line 172
    iget-object v0, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$1;->$state:Lcom/squareup/salesreport/SalesReportState;

    invoke-virtual {v0}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    .line 173
    iget-object v3, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$1;->$state:Lcom/squareup/salesreport/SalesReportState;

    invoke-virtual {v3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v3

    .line 171
    invoke-direct {p1, v0, v3}, Lcom/squareup/salesreport/SalesReportState$EmptyReport;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V

    check-cast p1, Lcom/squareup/salesreport/SalesReportState;

    goto :goto_0

    .line 175
    :cond_0
    instance-of v0, p1, Lcom/squareup/customreport/data/WithSalesReport;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    .line 176
    check-cast p1, Lcom/squareup/customreport/data/WithSalesReport;

    .line 177
    iget-object v3, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$1;->$state:Lcom/squareup/salesreport/SalesReportState;

    invoke-virtual {v3}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v3

    .line 178
    iget-object v4, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$1;->$state:Lcom/squareup/salesreport/SalesReportState;

    invoke-virtual {v4}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v4

    .line 175
    invoke-direct {v0, p1, v3, v4}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;-><init>(Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/salesreport/SalesReportState;

    .line 181
    :goto_0
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 175
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 183
    :cond_2
    instance-of p1, p1, Lcom/squareup/customreport/data/SalesReportRepository$Result$Failure;

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 184
    new-instance v0, Lcom/squareup/salesreport/SalesReportState$NetworkFailure;

    iget-object v3, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$1;->$state:Lcom/squareup/salesreport/SalesReportState;

    invoke-virtual {v3}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$1;->$state:Lcom/squareup/salesreport/SalesReportState;

    invoke-virtual {v4}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Lcom/squareup/salesreport/SalesReportState$NetworkFailure;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V

    .line 183
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 92
    check-cast p1, Lcom/squareup/customreport/data/SalesReportRepository$Result;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$1;->invoke(Lcom/squareup/customreport/data/SalesReportRepository$Result;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
