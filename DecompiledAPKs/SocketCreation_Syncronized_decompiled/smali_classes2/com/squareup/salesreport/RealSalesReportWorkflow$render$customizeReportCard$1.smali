.class final Lcom/squareup/salesreport/RealSalesReportWorkflow$render$customizeReportCard$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSalesReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/RealSalesReportWorkflow;->render(Lcom/squareup/salesreport/SalesReportProps;Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/salesreport/SalesReportState;",
        "+",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/salesreport/SalesReportState;",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        "it",
        "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/salesreport/SalesReportProps;

.field final synthetic $state:Lcom/squareup/salesreport/SalesReportState;

.field final synthetic this$0:Lcom/squareup/salesreport/RealSalesReportWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/RealSalesReportWorkflow;Lcom/squareup/salesreport/SalesReportProps;Lcom/squareup/salesreport/SalesReportState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$customizeReportCard$1;->this$0:Lcom/squareup/salesreport/RealSalesReportWorkflow;

    iput-object p2, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$customizeReportCard$1;->$props:Lcom/squareup/salesreport/SalesReportProps;

    iput-object p3, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$customizeReportCard$1;->$state:Lcom/squareup/salesreport/SalesReportState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/salesreport/customize/CustomizeReportResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/salesreport/SalesReportState;",
            "Lcom/squareup/salesreport/SalesReportOutput;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "it"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    instance-of v2, v1, Lcom/squareup/salesreport/customize/CustomizeReportResult$ReportConfigChanged;

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    .line 140
    check-cast v1, Lcom/squareup/salesreport/customize/CustomizeReportResult$ReportConfigChanged;

    invoke-virtual {v1}, Lcom/squareup/salesreport/customize/CustomizeReportResult$ReportConfigChanged;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    .line 141
    invoke-virtual {v1}, Lcom/squareup/salesreport/customize/CustomizeReportResult$ReportConfigChanged;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/salesreport/util/RangeSelectionsKt;->getDefaultComparisonRange(Lcom/squareup/customreport/data/RangeSelection;)Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object v14

    const/16 v15, 0xff

    const/16 v16, 0x0

    .line 140
    invoke-static/range {v5 .. v16}, Lcom/squareup/customreport/data/ReportConfig;->copy$default(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZZLcom/squareup/customreport/data/EmployeeFiltersSelection;Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/customreport/data/ComparisonRange;ILjava/lang/Object;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    .line 142
    iget-object v2, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$customizeReportCard$1;->$props:Lcom/squareup/salesreport/SalesReportProps;

    invoke-virtual {v2}, Lcom/squareup/salesreport/SalesReportProps;->getDetailLevel()Lcom/squareup/api/salesreport/DetailLevel;

    move-result-object v2

    sget-object v5, Lcom/squareup/api/salesreport/DetailLevel$Basic;->INSTANCE:Lcom/squareup/api/salesreport/DetailLevel$Basic;

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 143
    iget-object v2, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$customizeReportCard$1;->this$0:Lcom/squareup/salesreport/RealSalesReportWorkflow;

    invoke-static {v2}, Lcom/squareup/salesreport/RealSalesReportWorkflow;->access$getBasicSalesReportConfigRepository$p(Lcom/squareup/salesreport/RealSalesReportWorkflow;)Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;->set(Lcom/squareup/customreport/data/ReportConfig;)V

    .line 145
    :cond_0
    iget-object v2, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$customizeReportCard$1;->this$0:Lcom/squareup/salesreport/RealSalesReportWorkflow;

    invoke-static {v2}, Lcom/squareup/salesreport/RealSalesReportWorkflow;->access$getSalesReportAnalytics$p(Lcom/squareup/salesreport/RealSalesReportWorkflow;)Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    move-result-object v2

    iget-object v5, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$customizeReportCard$1;->$state:Lcom/squareup/salesreport/SalesReportState;

    invoke-virtual {v5}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v5

    invoke-interface {v2, v5}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logCustomizeReport(Lcom/squareup/customreport/data/ReportConfig;)V

    .line 146
    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 147
    new-instance v5, Lcom/squareup/salesreport/SalesReportState$LoadingReport;

    .line 149
    iget-object v6, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$customizeReportCard$1;->$state:Lcom/squareup/salesreport/SalesReportState;

    invoke-virtual {v6}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v6

    .line 147
    invoke-direct {v5, v1, v6}, Lcom/squareup/salesreport/SalesReportState$LoadingReport;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V

    .line 146
    invoke-static {v2, v5, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_0

    .line 153
    :cond_1
    instance-of v1, v1, Lcom/squareup/salesreport/customize/CustomizeReportResult$ReportConfigUnchanged;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v2, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$customizeReportCard$1;->$state:Lcom/squareup/salesreport/SalesReportState;

    check-cast v2, Lcom/squareup/salesreport/SalesReportState$CustomizingReport;

    invoke-virtual {v2}, Lcom/squareup/salesreport/SalesReportState$CustomizingReport;->getPreviousState()Lcom/squareup/salesreport/SalesReportState;

    move-result-object v2

    invoke-static {v1, v2, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_2
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 92
    check-cast p1, Lcom/squareup/salesreport/customize/CustomizeReportResult;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$customizeReportCard$1;->invoke(Lcom/squareup/salesreport/customize/CustomizeReportResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
