.class public final Lcom/squareup/salesreport/SalesReportState$InitialState;
.super Lcom/squareup/salesreport/SalesReportState;
.source "SalesReportState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/SalesReportState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InitialState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/SalesReportState$InitialState$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\u0019\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u000fH\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/salesreport/SalesReportState$InitialState;",
        "Lcom/squareup/salesreport/SalesReportState;",
        "reportConfig",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "uiSelectionState",
        "Lcom/squareup/salesreport/SalesReportState$UiSelectionState;",
        "(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V",
        "getReportConfig",
        "()Lcom/squareup/customreport/data/ReportConfig;",
        "getUiSelectionState",
        "()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final reportConfig:Lcom/squareup/customreport/data/ReportConfig;

.field private final uiSelectionState:Lcom/squareup/salesreport/SalesReportState$UiSelectionState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/salesreport/SalesReportState$InitialState$Creator;

    invoke-direct {v0}, Lcom/squareup/salesreport/SalesReportState$InitialState$Creator;-><init>()V

    sput-object v0, Lcom/squareup/salesreport/SalesReportState$InitialState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V
    .locals 1

    const-string v0, "reportConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiSelectionState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 50
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/salesreport/SalesReportState;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/salesreport/SalesReportState$InitialState;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    iput-object p2, p0, Lcom/squareup/salesreport/SalesReportState$InitialState;->uiSelectionState:Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/salesreport/SalesReportState$InitialState;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$InitialState;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$InitialState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$InitialState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object p2

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/salesreport/SalesReportState$InitialState;->copy(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)Lcom/squareup/salesreport/SalesReportState$InitialState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/customreport/data/ReportConfig;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$InitialState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$InitialState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)Lcom/squareup/salesreport/SalesReportState$InitialState;
    .locals 1

    const-string v0, "reportConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiSelectionState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/salesreport/SalesReportState$InitialState;

    invoke-direct {v0, p1, p2}, Lcom/squareup/salesreport/SalesReportState$InitialState;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/salesreport/SalesReportState$InitialState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/salesreport/SalesReportState$InitialState;

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$InitialState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportState$InitialState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$InitialState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportState$InitialState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getReportConfig()Lcom/squareup/customreport/data/ReportConfig;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$InitialState;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    return-object v0
.end method

.method public getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$InitialState;->uiSelectionState:Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$InitialState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$InitialState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InitialState(reportConfig="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$InitialState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", uiSelectionState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$InitialState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$InitialState;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object p2, p0, Lcom/squareup/salesreport/SalesReportState$InitialState;->uiSelectionState:Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    return-void
.end method
