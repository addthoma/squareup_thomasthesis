.class public final Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver_MembersInjector;
.super Ljava/lang/Object;
.source "ShareSheetSelectedAppReceiver_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
            ">;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;",
            ">;"
        }
    .end annotation

    .line 23
    new-instance v0, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAnalytics(Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;)V
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    invoke-static {p1, v0}, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver_MembersInjector;->injectAnalytics(Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver_MembersInjector;->injectMembers(Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;)V

    return-void
.end method
