.class public final Lcom/squareup/CommonAppModule_ProvideUrlRedirectSettingFactory;
.super Ljava/lang/Object;
.source "CommonAppModule_ProvideUrlRedirectSettingFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/http/UrlRedirectSetting;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/CommonAppModule_ProvideUrlRedirectSettingFactory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_ProvideUrlRedirectSettingFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/CommonAppModule_ProvideUrlRedirectSettingFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/CommonAppModule_ProvideUrlRedirectSettingFactory;

    invoke-direct {v0, p0}, Lcom/squareup/CommonAppModule_ProvideUrlRedirectSettingFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideUrlRedirectSetting(Lcom/squareup/settings/server/Features;)Lcom/squareup/http/UrlRedirectSetting;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/CommonAppModule;->provideUrlRedirectSetting(Lcom/squareup/settings/server/Features;)Lcom/squareup/http/UrlRedirectSetting;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/http/UrlRedirectSetting;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/http/UrlRedirectSetting;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/CommonAppModule_ProvideUrlRedirectSettingFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {v0}, Lcom/squareup/CommonAppModule_ProvideUrlRedirectSettingFactory;->provideUrlRedirectSetting(Lcom/squareup/settings/server/Features;)Lcom/squareup/http/UrlRedirectSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/CommonAppModule_ProvideUrlRedirectSettingFactory;->get()Lcom/squareup/http/UrlRedirectSetting;

    move-result-object v0

    return-object v0
.end method
