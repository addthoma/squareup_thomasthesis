.class public Lcom/squareup/LoggedInScopeRunner;
.super Ljava/lang/Object;
.source "LoggedInScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/LoggedInScopeNotifier;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;
    }
.end annotation


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final additionalBusServices:Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;

.field private final additionalServices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation
.end field

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final autoCaptureJobCreator:Lcom/squareup/autocapture/AutoCaptureJobCreator;

.field private final autoCaptureTimerStarter:Lcom/squareup/autocapture/AutoCaptureTimerStarter;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

.field private final cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

.field private final curatedImage:Lcom/squareup/merchantimages/CuratedImage;

.field private final dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

.field private final dipperEventHandler:Lcom/squareup/cardreader/dipper/DipperEventHandler;

.field private final employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final forwardedPaymentManager:Lcom/squareup/payment/offline/ForwardedPaymentManager;

.field private final headsetConnectionListener:Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

.field private final headsetStateDispatcher:Lcom/squareup/cardreader/HeadsetStateDispatcher;

.field private final locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

.field private final loggedInExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

.field private final loggedInSubs:Lio/reactivex/disposables/CompositeDisposable;

.field private final loggedInUrlMonitor:Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;

.field private final notificationManager:Landroid/app/NotificationManager;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private final onLoggedIn:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

.field private final paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

.field private final pendingCaptures:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final pendingCapturesLogger:Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;

.field private final pendingCapturesQueueConformer:Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;

.field private final pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

.field private final picassoMemoryCache:Lcom/squareup/picasso/Cache;

.field private final printJobQueue:Lcom/squareup/print/PrintJobQueue;

.field private final printerScoutScheduler:Lcom/squareup/print/PrinterScoutScheduler;

.field private final queueBertPublicKeyManager:Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

.field private final queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

.field private final readerBusBoy:Lcom/squareup/log/terminal/ReaderEventBusBoy;

.field private final readerEarlyPowerupOpportunist:Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;

.field private final realEmployeeManagement:Lcom/squareup/permissions/RealEmployeeManagement;

.field private final rootBus:Lcom/squareup/badbus/BadBus;

.field private final safetyNetRunner:Lcom/squareup/safetynet/SafetyNetRunner;

.field private final serverClock:Lcom/squareup/account/ServerClock;

.field private final storedPaymentsQueueConformer:Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;

.field private final tasks:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final tasksQueueConformer:Lcom/squareup/queue/redundant/TasksQueueConformer;

.field private final tickets:Lcom/squareup/tickets/Tickets$InternalTickets;

.field private final timeTrackingSettings:Lcom/squareup/permissions/TimeTrackingSettings;

.field private final transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/dipper/DipperEventHandler;Lcom/squareup/cardreader/CardReaderPowerMonitor;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/cardreader/HeadsetStateDispatcher;Landroid/app/NotificationManager;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/picasso/Cache;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/print/PrinterScoutScheduler;Lcom/squareup/tickets/Tickets$InternalTickets;Lcom/squareup/account/ServerClock;Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/print/PrintJobQueue;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/safetynet/SafetyNetRunner;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/payment/offline/QueueBertPublicKeyManager;Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/autocapture/AutoCaptureJobCreator;Lcom/squareup/autocapture/AutoCaptureTimerStarter;Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;Lcom/squareup/queue/redundant/TasksQueueConformer;Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;Lcom/squareup/log/terminal/ReaderEventBusBoy;Lcom/squareup/permissions/PasscodeEmployeeManagement;Ljava/util/Set;Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/TimeTrackingSettings;Lcom/squareup/permissions/RealEmployeeManagement;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            "Lcom/squareup/cardreader/dipper/DipperEventHandler;",
            "Lcom/squareup/cardreader/CardReaderPowerMonitor;",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            "Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;",
            "Lcom/squareup/payment/offline/ForwardedPaymentManager;",
            "Lcom/squareup/cardreader/HeadsetStateDispatcher;",
            "Landroid/app/NotificationManager;",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            "Lcom/squareup/picasso/Cache;",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/cardreader/PaymentCounter;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/print/PrinterScoutScheduler;",
            "Lcom/squareup/tickets/Tickets$InternalTickets;",
            "Lcom/squareup/account/ServerClock;",
            "Lcom/squareup/thread/executor/StoppableSerialExecutor;",
            "Lcom/squareup/print/PrintJobQueue;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            "Lcom/squareup/safetynet/SafetyNetRunner;",
            "Lcom/squareup/merchantimages/CuratedImage;",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;",
            "Lcom/squareup/queue/QueueServiceStarter;",
            "Lcom/squareup/autocapture/AutoCaptureJobCreator;",
            "Lcom/squareup/autocapture/AutoCaptureTimerStarter;",
            "Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;",
            "Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;",
            "Lcom/squareup/queue/redundant/TasksQueueConformer;",
            "Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;",
            "Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;",
            "Lcom/squareup/log/terminal/ReaderEventBusBoy;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;",
            "Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            "Lcom/squareup/permissions/PasscodesSettings;",
            "Lcom/squareup/permissions/TimeTrackingSettings;",
            "Lcom/squareup/permissions/RealEmployeeManagement;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->onLoggedIn:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 124
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->loggedInSubs:Lio/reactivex/disposables/CompositeDisposable;

    move-object v1, p1

    .line 158
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->features:Lcom/squareup/settings/server/Features;

    move-object v1, p3

    .line 159
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->rootBus:Lcom/squareup/badbus/BadBus;

    move-object v1, p4

    .line 160
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    move-object v1, p5

    .line 161
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->dipperEventHandler:Lcom/squareup/cardreader/dipper/DipperEventHandler;

    move-object v1, p6

    .line 162
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    move-object v1, p7

    .line 163
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-object v1, p8

    .line 164
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    move-object v1, p9

    .line 165
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->readerEarlyPowerupOpportunist:Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;

    move-object v1, p10

    .line 166
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->forwardedPaymentManager:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    move-object v1, p11

    .line 167
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->headsetStateDispatcher:Lcom/squareup/cardreader/HeadsetStateDispatcher;

    move-object v1, p12

    .line 168
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->notificationManager:Landroid/app/NotificationManager;

    move-object v1, p13

    .line 169
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    move-object/from16 v1, p14

    .line 170
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->picassoMemoryCache:Lcom/squareup/picasso/Cache;

    move-object/from16 v1, p15

    .line 171
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-object/from16 v1, p16

    .line 172
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->tasks:Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-object/from16 v1, p17

    .line 173
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    move-object/from16 v1, p18

    .line 174
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->pendingCaptures:Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-object/from16 v1, p19

    .line 175
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->printerScoutScheduler:Lcom/squareup/print/PrinterScoutScheduler;

    move-object/from16 v1, p20

    .line 176
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->tickets:Lcom/squareup/tickets/Tickets$InternalTickets;

    move-object/from16 v1, p21

    .line 177
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->serverClock:Lcom/squareup/account/ServerClock;

    move-object/from16 v1, p22

    .line 178
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->loggedInExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    move-object/from16 v1, p23

    .line 179
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->printJobQueue:Lcom/squareup/print/PrintJobQueue;

    move-object/from16 v1, p24

    .line 180
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    move-object/from16 v1, p25

    .line 181
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    move-object/from16 v1, p26

    .line 182
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->safetyNetRunner:Lcom/squareup/safetynet/SafetyNetRunner;

    move-object/from16 v1, p27

    .line 183
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    move-object/from16 v1, p28

    .line 184
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->queueBertPublicKeyManager:Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

    move-object/from16 v1, p29

    .line 185
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    move-object/from16 v1, p30

    .line 186
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->autoCaptureJobCreator:Lcom/squareup/autocapture/AutoCaptureJobCreator;

    move-object/from16 v1, p31

    .line 187
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->autoCaptureTimerStarter:Lcom/squareup/autocapture/AutoCaptureTimerStarter;

    move-object/from16 v1, p32

    .line 188
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->storedPaymentsQueueConformer:Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;

    move-object/from16 v1, p33

    .line 189
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->pendingCapturesQueueConformer:Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;

    move-object/from16 v1, p34

    .line 190
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->tasksQueueConformer:Lcom/squareup/queue/redundant/TasksQueueConformer;

    move-object/from16 v1, p35

    .line 191
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->pendingCapturesLogger:Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;

    move-object/from16 v1, p36

    .line 192
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->loggedInUrlMonitor:Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;

    move-object v1, p2

    .line 193
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    move-object/from16 v1, p41

    .line 194
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->headsetConnectionListener:Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    move-object/from16 v1, p37

    .line 195
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->readerBusBoy:Lcom/squareup/log/terminal/ReaderEventBusBoy;

    move-object/from16 v1, p38

    .line 196
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    move-object/from16 v1, p39

    .line 197
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->additionalServices:Ljava/util/Set;

    move-object/from16 v1, p40

    .line 198
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->additionalBusServices:Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;

    move-object/from16 v1, p42

    .line 199
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    move-object/from16 v1, p43

    .line 200
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    move-object/from16 v1, p44

    .line 201
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->timeTrackingSettings:Lcom/squareup/permissions/TimeTrackingSettings;

    move-object/from16 v1, p45

    .line 202
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->realEmployeeManagement:Lcom/squareup/permissions/RealEmployeeManagement;

    move-object/from16 v1, p46

    .line 203
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-void
.end method

.method private registerOnBus(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/badbus/BadBusRegistrant;",
            ">;)V"
        }
    .end annotation

    .line 307
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/badbus/BadBusRegistrant;

    .line 308
    iget-object v1, p0, Lcom/squareup/LoggedInScopeRunner;->loggedInSubs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/LoggedInScopeRunner;->rootBus:Lcom/squareup/badbus/BadBus;

    invoke-interface {v0, v2}, Lcom/squareup/badbus/BadBusRegistrant;->registerOnBus(Lcom/squareup/badbus/BadBus;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$LoggedInScopeRunner(Lcom/squareup/wavpool/swipe/HeadsetConnectionState;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 260
    invoke-virtual {p1}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->isReaderConnected()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 261
    iget-object p1, p0, Lcom/squareup/LoggedInScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterReaderName;->HEADSET_INSERTED:Lcom/squareup/analytics/RegisterReaderName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logReaderEvent(Lcom/squareup/analytics/RegisterReaderName;)V

    goto :goto_0

    .line 263
    :cond_0
    iget-object p1, p0, Lcom/squareup/LoggedInScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterReaderName;->HEADSET_REMOVED:Lcom/squareup/analytics/RegisterReaderName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logReaderEvent(Lcom/squareup/analytics/RegisterReaderName;)V

    :goto_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/badbus/BadBusRegistrant;

    .line 210
    iget-object v1, p0, Lcom/squareup/LoggedInScopeRunner;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/LoggedInScopeRunner;->readerEarlyPowerupOpportunist:Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/LoggedInScopeRunner;->readerBusBoy:Lcom/squareup/log/terminal/ReaderEventBusBoy;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/LoggedInScopeRunner;->registerOnBus(Ljava/util/List;)V

    .line 214
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->loggedInSubs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/LoggedInScopeRunner;->readerBusBoy:Lcom/squareup/log/terminal/ReaderEventBusBoy;

    invoke-virtual {v1}, Lcom/squareup/log/terminal/ReaderEventBusBoy;->registerOnSwipeBus()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 216
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->additionalBusServices:Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;

    invoke-interface {v0}, Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;->get()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/LoggedInScopeRunner;->registerOnBus(Ljava/util/List;)V

    .line 218
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->updateMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    .line 220
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->serverClock:Lcom/squareup/account/ServerClock;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 221
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 222
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 223
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->dipperEventHandler:Lcom/squareup/cardreader/dipper/DipperEventHandler;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 224
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->forwardedPaymentManager:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 226
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 227
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 228
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->queueBertPublicKeyManager:Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 229
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 230
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->autoCaptureJobCreator:Lcom/squareup/autocapture/AutoCaptureJobCreator;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 231
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->autoCaptureTimerStarter:Lcom/squareup/autocapture/AutoCaptureTimerStarter;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 232
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 233
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 234
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 235
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->timeTrackingSettings:Lcom/squareup/permissions/TimeTrackingSettings;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 236
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->realEmployeeManagement:Lcom/squareup/permissions/RealEmployeeManagement;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 237
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 239
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->safetyNetRunner:Lcom/squareup/safetynet/SafetyNetRunner;

    invoke-virtual {v0}, Lcom/squareup/safetynet/SafetyNetRunner;->runSafetyNetAttestationAsync()V

    .line 241
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->storedPaymentsQueueConformer:Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 242
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->pendingCapturesQueueConformer:Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 243
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->tasksQueueConformer:Lcom/squareup/queue/redundant/TasksQueueConformer;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 244
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->pendingCapturesLogger:Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 246
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->loggedInUrlMonitor:Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 247
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 249
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->additionalServices:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmortar/Scoped;

    .line 250
    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    goto :goto_0

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->onLoggedIn:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 256
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    const-string v1, "Logged in"

    invoke-interface {v0, v1}, Lcom/squareup/queue/QueueServiceStarter;->startWaitingForForeground(Ljava/lang/String;)V

    .line 258
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->headsetConnectionListener:Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    invoke-interface {v0}, Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;->onHeadsetStateChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/-$$Lambda$LoggedInScopeRunner$_yS3yv0si7MwC2F6g0VT_No_hys;

    invoke-direct {v1, p0}, Lcom/squareup/-$$Lambda$LoggedInScopeRunner$_yS3yv0si7MwC2F6g0VT_No_hys;-><init>(Lcom/squareup/LoggedInScopeRunner;)V

    .line 259
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 258
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 270
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->loggedInExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    invoke-interface {v0}, Lcom/squareup/thread/executor/StoppableSerialExecutor;->shutdown()V

    .line 272
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->headsetStateDispatcher:Lcom/squareup/cardreader/HeadsetStateDispatcher;

    invoke-virtual {v0}, Lcom/squareup/cardreader/HeadsetStateDispatcher;->reset()V

    .line 273
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderFactory;->destroyAllReaders()V

    .line 275
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    const-string v1, "Logged out"

    invoke-interface {v0, v1}, Lcom/squareup/queue/QueueServiceStarter;->start(Ljava/lang/String;)V

    .line 277
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->tickets:Lcom/squareup/tickets/Tickets$InternalTickets;

    invoke-interface {v0}, Lcom/squareup/tickets/Tickets$InternalTickets;->close()V

    .line 278
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->printJobQueue:Lcom/squareup/print/PrintJobQueue;

    invoke-interface {v0}, Lcom/squareup/print/PrintJobQueue;->close()V

    .line 279
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->close()V

    .line 284
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->pendingCaptures:Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-interface {v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->close()V

    .line 285
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->tasks:Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-interface {v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->close()V

    .line 287
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->notificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    .line 289
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->loggedInSubs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 292
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->picassoMemoryCache:Lcom/squareup/picasso/Cache;

    invoke-interface {v0}, Lcom/squareup/picasso/Cache;->clear()V

    .line 294
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->printerScoutScheduler:Lcom/squareup/print/PrinterScoutScheduler;

    invoke-virtual {v0}, Lcom/squareup/print/PrinterScoutScheduler;->shutdown()V

    return-void
.end method

.method public onLoggedIn()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 303
    iget-object v0, p0, Lcom/squareup/LoggedInScopeRunner;->onLoggedIn:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method
