.class public final Lcom/squareup/PosAppComponent_Module_ProvideIsReaderSdkFactory;
.super Ljava/lang/Object;
.source "PosAppComponent_Module_ProvideIsReaderSdkFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/PosAppComponent_Module_ProvideIsReaderSdkFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/PosAppComponent_Module_ProvideIsReaderSdkFactory;
    .locals 1

    .line 21
    invoke-static {}, Lcom/squareup/PosAppComponent_Module_ProvideIsReaderSdkFactory$InstanceHolder;->access$000()Lcom/squareup/PosAppComponent_Module_ProvideIsReaderSdkFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideIsReaderSdk()Z
    .locals 1

    .line 25
    invoke-static {}, Lcom/squareup/PosAppComponent$Module;->provideIsReaderSdk()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public get()Ljava/lang/Boolean;
    .locals 1

    .line 17
    invoke-static {}, Lcom/squareup/PosAppComponent_Module_ProvideIsReaderSdkFactory;->provideIsReaderSdk()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/PosAppComponent_Module_ProvideIsReaderSdkFactory;->get()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
