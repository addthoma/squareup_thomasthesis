.class public final Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;
.super Ljava/lang/Object;
.source "CashDrawerTracker_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cashdrawer/CashDrawerTracker;",
        ">;"
    }
.end annotation


# instance fields
.field private final apgVasarioCashDrawerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;",
            ">;"
        }
    .end annotation
.end field

.field private final backgroundThreadExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDrawerSpoolerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;"
        }
    .end annotation
.end field

.field private final hardwarePrinterTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;->apgVasarioCashDrawerProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p2, p0, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;->cashDrawerSpoolerProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p3, p0, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p4, p0, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p5, p0, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;->backgroundThreadExecutorProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p6, p0, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;->mainThreadEnforcerProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p7, p0, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)",
            "Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;"
        }
    .end annotation

    .line 65
    new-instance v8, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrinterStations;Ljava/util/concurrent/Executor;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cashdrawer/CashDrawerTracker;
    .locals 9

    .line 72
    new-instance v8, Lcom/squareup/cashdrawer/CashDrawerTracker;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/cashdrawer/CashDrawerTracker;-><init>(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrinterStations;Ljava/util/concurrent/Executor;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/thread/executor/MainThread;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/cashdrawer/CashDrawerTracker;
    .locals 8

    .line 54
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;->apgVasarioCashDrawerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;->cashDrawerSpoolerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/print/PrintSpooler;

    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/print/HardwarePrinterTracker;

    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/print/PrinterStations;

    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;->backgroundThreadExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;->mainThreadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/thread/executor/MainThread;

    invoke-static/range {v1 .. v7}, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;->newInstance(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrinterStations;Ljava/util/concurrent/Executor;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cashdrawer/CashDrawerTracker;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/cashdrawer/CashDrawerTracker_Factory;->get()Lcom/squareup/cashdrawer/CashDrawerTracker;

    move-result-object v0

    return-object v0
.end method
