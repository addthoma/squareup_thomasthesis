.class public Lcom/squareup/cashdrawer/CashDrawerHudToaster;
.super Ljava/lang/Object;
.source "CashDrawerHudToaster.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private listener:Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    .line 29
    iput-object p2, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 30
    iput-object p3, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->res:Lcom/squareup/util/Res;

    .line 31
    iput-object p4, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->analytics:Lcom/squareup/analytics/Analytics;

    .line 33
    invoke-direct {p0}, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->buildListener()Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->listener:Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cashdrawer/CashDrawerHudToaster;)Lcom/squareup/cashdrawer/CashDrawerTracker;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/cashdrawer/CashDrawerHudToaster;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method private buildListener()Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/cashdrawer/CashDrawerHudToaster$1;

    invoke-direct {v0, p0}, Lcom/squareup/cashdrawer/CashDrawerHudToaster$1;-><init>(Lcom/squareup/cashdrawer/CashDrawerHudToaster;)V

    return-object v0
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 89
    iget-object p1, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->listener:Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;

    invoke-virtual {p1, v0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->addListener(Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    iget-object v1, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->listener:Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;

    invoke-virtual {v0, v1}, Lcom/squareup/cashdrawer/CashDrawerTracker;->removeListener(Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;)V

    return-void
.end method

.method public showCashDrawerConnected()V
    .locals 4

    .line 69
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/hardware/R$string;->cash_drawer_connected:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CASH_DRAWER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    return-void
.end method

.method public showCashDrawerDisconnected(Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V
    .locals 3

    .line 75
    sget-object v0, Lcom/squareup/cashdrawer/CashDrawerHudToaster$2;->$SwitchMap$com$squareup$cashdrawer$CashDrawer$DisconnectReason:[I

    invoke-virtual {p1}, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 80
    iget-object p1, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/hardware/R$string;->cash_drawer_failed_to_connect:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 83
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown disconnect reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_1
    iget-object p1, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/hardware/R$string;->cash_drawer_disconnected:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 85
    :goto_0
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CASH_DRAWER_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    return-void
.end method
