.class public final enum Lcom/squareup/calc/constants/InclusionType;
.super Ljava/lang/Enum;
.source "InclusionType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/calc/constants/InclusionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/calc/constants/InclusionType;

.field public static final enum ADDITIVE:Lcom/squareup/calc/constants/InclusionType;

.field public static final enum INCLUSIVE:Lcom/squareup/calc/constants/InclusionType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 21
    new-instance v0, Lcom/squareup/calc/constants/InclusionType;

    const/4 v1, 0x0

    const-string v2, "ADDITIVE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/calc/constants/InclusionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/calc/constants/InclusionType;->ADDITIVE:Lcom/squareup/calc/constants/InclusionType;

    .line 22
    new-instance v0, Lcom/squareup/calc/constants/InclusionType;

    const/4 v2, 0x1

    const-string v3, "INCLUSIVE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/calc/constants/InclusionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/calc/constants/InclusionType;->INCLUSIVE:Lcom/squareup/calc/constants/InclusionType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/calc/constants/InclusionType;

    .line 20
    sget-object v3, Lcom/squareup/calc/constants/InclusionType;->ADDITIVE:Lcom/squareup/calc/constants/InclusionType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/calc/constants/InclusionType;->INCLUSIVE:Lcom/squareup/calc/constants/InclusionType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/calc/constants/InclusionType;->$VALUES:[Lcom/squareup/calc/constants/InclusionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/calc/constants/InclusionType;
    .locals 1

    .line 20
    const-class v0, Lcom/squareup/calc/constants/InclusionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/calc/constants/InclusionType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/calc/constants/InclusionType;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/calc/constants/InclusionType;->$VALUES:[Lcom/squareup/calc/constants/InclusionType;

    invoke-virtual {v0}, [Lcom/squareup/calc/constants/InclusionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/calc/constants/InclusionType;

    return-object v0
.end method
