.class public final enum Lcom/squareup/calc/constants/ModifyTaxBasis;
.super Ljava/lang/Enum;
.source "ModifyTaxBasis.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/calc/constants/ModifyTaxBasis;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/calc/constants/ModifyTaxBasis;

.field public static final enum DO_NOT_MODIFY_TAX_BASIS:Lcom/squareup/calc/constants/ModifyTaxBasis;

.field public static final enum MODIFY_TAX_BASIS:Lcom/squareup/calc/constants/ModifyTaxBasis;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 7
    new-instance v0, Lcom/squareup/calc/constants/ModifyTaxBasis;

    const/4 v1, 0x0

    const-string v2, "MODIFY_TAX_BASIS"

    invoke-direct {v0, v2, v1}, Lcom/squareup/calc/constants/ModifyTaxBasis;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/calc/constants/ModifyTaxBasis;->MODIFY_TAX_BASIS:Lcom/squareup/calc/constants/ModifyTaxBasis;

    .line 11
    new-instance v0, Lcom/squareup/calc/constants/ModifyTaxBasis;

    const/4 v2, 0x1

    const-string v3, "DO_NOT_MODIFY_TAX_BASIS"

    invoke-direct {v0, v3, v2}, Lcom/squareup/calc/constants/ModifyTaxBasis;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/calc/constants/ModifyTaxBasis;->DO_NOT_MODIFY_TAX_BASIS:Lcom/squareup/calc/constants/ModifyTaxBasis;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/calc/constants/ModifyTaxBasis;

    .line 3
    sget-object v3, Lcom/squareup/calc/constants/ModifyTaxBasis;->MODIFY_TAX_BASIS:Lcom/squareup/calc/constants/ModifyTaxBasis;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/calc/constants/ModifyTaxBasis;->DO_NOT_MODIFY_TAX_BASIS:Lcom/squareup/calc/constants/ModifyTaxBasis;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/calc/constants/ModifyTaxBasis;->$VALUES:[Lcom/squareup/calc/constants/ModifyTaxBasis;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/calc/constants/ModifyTaxBasis;
    .locals 1

    .line 3
    const-class v0, Lcom/squareup/calc/constants/ModifyTaxBasis;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/calc/constants/ModifyTaxBasis;

    return-object p0
.end method

.method public static values()[Lcom/squareup/calc/constants/ModifyTaxBasis;
    .locals 1

    .line 3
    sget-object v0, Lcom/squareup/calc/constants/ModifyTaxBasis;->$VALUES:[Lcom/squareup/calc/constants/ModifyTaxBasis;

    invoke-virtual {v0}, [Lcom/squareup/calc/constants/ModifyTaxBasis;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/calc/constants/ModifyTaxBasis;

    return-object v0
.end method
