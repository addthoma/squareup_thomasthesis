.class public interface abstract Lcom/squareup/calc/order/Item;
.super Ljava/lang/Object;
.source "Item.java"


# virtual methods
.method public abstract appliedDiscounts()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/calc/order/Adjustment;",
            ">;"
        }
    .end annotation
.end method

.method public abstract appliedModifiers()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/calc/order/Modifier;",
            ">;"
        }
    .end annotation
.end method

.method public abstract appliedTaxes()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/calc/order/Adjustment;",
            ">;"
        }
    .end annotation
.end method

.method public abstract baseAmount()J
.end method

.method public abstract price()J
.end method

.method public abstract quantity()Ljava/math/BigDecimal;
.end method
