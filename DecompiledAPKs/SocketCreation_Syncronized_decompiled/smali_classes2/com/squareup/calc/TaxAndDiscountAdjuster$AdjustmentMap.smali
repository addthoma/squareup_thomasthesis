.class Lcom/squareup/calc/TaxAndDiscountAdjuster$AdjustmentMap;
.super Ljava/util/LinkedHashMap;
.source "TaxAndDiscountAdjuster.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/calc/TaxAndDiscountAdjuster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AdjustmentMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap<",
        "Lcom/squareup/calc/order/Item;",
        "Lcom/squareup/calc/AdjustedItem;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 471
    invoke-direct {p0}, Ljava/util/LinkedHashMap;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/calc/TaxAndDiscountAdjuster$1;)V
    .locals 0

    .line 471
    invoke-direct {p0}, Lcom/squareup/calc/TaxAndDiscountAdjuster$AdjustmentMap;-><init>()V

    return-void
.end method


# virtual methods
.method public get(Ljava/lang/Object;)Lcom/squareup/calc/AdjustedItem;
    .locals 2

    .line 473
    invoke-super {p0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/calc/AdjustedItem;

    if-nez v0, :cond_0

    .line 474
    instance-of v1, p1, Lcom/squareup/calc/order/Item;

    if-eqz v1, :cond_0

    .line 475
    new-instance v0, Lcom/squareup/calc/AdjustedItem;

    check-cast p1, Lcom/squareup/calc/order/Item;

    invoke-direct {v0, p1}, Lcom/squareup/calc/AdjustedItem;-><init>(Lcom/squareup/calc/order/Item;)V

    .line 476
    invoke-virtual {p0, p1, v0}, Lcom/squareup/calc/TaxAndDiscountAdjuster$AdjustmentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 471
    invoke-virtual {p0, p1}, Lcom/squareup/calc/TaxAndDiscountAdjuster$AdjustmentMap;->get(Ljava/lang/Object;)Lcom/squareup/calc/AdjustedItem;

    move-result-object p1

    return-object p1
.end method
