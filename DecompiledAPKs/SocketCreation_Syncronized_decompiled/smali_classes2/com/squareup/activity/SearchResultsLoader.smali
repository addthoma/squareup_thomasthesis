.class public Lcom/squareup/activity/SearchResultsLoader;
.super Lcom/squareup/activity/AbstractTransactionsHistoryLoader;
.source "SearchResultsLoader.java"


# static fields
.field static final EVENT_HISTORY_SEARCH:Ljava/lang/String; = "History search"

.field static final PARAM_SEARCH_TYPE:Ljava/lang/String; = "search type"

.field public static final SEARCH_LIMIT:I = 0xf


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final cardConverter:Lcom/squareup/activity/ActivitySearchInstrumentConverter;

.field private cardQuery:Lcom/squareup/Card;

.field private instrumentSearch:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

.field private textQuery:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/activity/BillsList;Lcom/squareup/analytics/Analytics;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Ljava/lang/String;Lcom/squareup/papersignature/TenderStatusCache;Lcom/squareup/settings/server/Features;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/activity/ActivitySearchInstrumentConverter;)V
    .locals 11
    .param p5    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .param p6    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v10, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p10

    move-object/from16 v9, p9

    .line 52
    invoke-direct/range {v0 .. v9}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;-><init>(Lcom/squareup/util/Res;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/activity/BillsList;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Ljava/lang/String;Lcom/squareup/papersignature/TenderStatusCache;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/settings/server/Features;)V

    move-object v0, p4

    .line 54
    iput-object v0, v10, Lcom/squareup/activity/SearchResultsLoader;->analytics:Lcom/squareup/analytics/Analytics;

    move-object/from16 v0, p11

    .line 55
    iput-object v0, v10, Lcom/squareup/activity/SearchResultsLoader;->cardConverter:Lcom/squareup/activity/ActivitySearchInstrumentConverter;

    return-void
.end method


# virtual methods
.method protected getBillListQuery()Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;
    .locals 2

    .line 142
    iget-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->textQuery:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/activity/SearchResultsLoader;->textQuery:Ljava/lang/String;

    .line 144
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->query_string(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->build()Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    move-result-object v0

    return-object v0

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->instrumentSearch:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    return-object v0

    .line 151
    :cond_1
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/activity/SearchResultsLoader;->instrumentSearch:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    .line 152
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->instrument_search(Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;)Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;

    move-result-object v0

    .line 153
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->build()Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    move-result-object v0

    return-object v0
.end method

.method public getQuery()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;
    .locals 3

    .line 123
    iget-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->textQuery:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/activity/SearchResultsLoader;->textQuery:Ljava/lang/String;

    .line 125
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->query_string(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    move-result-object v0

    return-object v0

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->instrumentSearch:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    return-object v0

    .line 132
    :cond_1
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/activity/SearchResultsLoader;->instrumentSearch:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 134
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/activity/SearchResultsLoader;->instrumentSearch:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    .line 135
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;->payment_instrument(Lcom/squareup/protos/client/bills/PaymentInstrument;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;

    move-result-object v1

    .line 136
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    move-result-object v1

    .line 133
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->instrument_search(Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;

    move-result-object v0

    .line 137
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    move-result-object v0

    return-object v0
.end method

.method public getQueryForDisplay()Ljava/lang/String;
    .locals 5

    .line 88
    invoke-virtual {p0}, Lcom/squareup/activity/SearchResultsLoader;->hasTextQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->textQuery:Ljava/lang/String;

    return-object v0

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->cardQuery:Lcom/squareup/Card;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->instrumentSearch:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    return-object v0

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->cardQuery:Lcom/squareup/Card;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->instrumentSearch:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    if-eqz v0, :cond_2

    .line 97
    iget-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistory/R$string;->card:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 100
    :cond_2
    iget-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "search type"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/squareup/activity/SearchResultsLoader;->cardQuery:Lcom/squareup/Card;

    .line 101
    invoke-virtual {v3}, Lcom/squareup/Card;->getInputType()Lcom/squareup/Card$InputType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/Card$InputType;->name()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "History search"

    .line 100
    invoke-interface {v0, v2, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->cardQuery:Lcom/squareup/Card;

    invoke-virtual {v0}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object v0

    .line 104
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 106
    iget-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->cardQuery:Lcom/squareup/Card;

    invoke-virtual {v0}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object v0

    .line 107
    iget-object v1, p0, Lcom/squareup/activity/SearchResultsLoader;->res:Lcom/squareup/util/Res;

    iget v0, v0, Lcom/squareup/text/CardBrandResources;->buyerCardPhrase:I

    invoke-interface {v1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 110
    :cond_3
    iget-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->card_info:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/activity/SearchResultsLoader;->cardQuery:Lcom/squareup/Card;

    .line 111
    invoke-virtual {v1}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object v1

    const-string v2, "unmasked_digits"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSearchLimit()I
    .locals 1

    const/16 v0, 0x10

    return v0
.end method

.method public hasMore()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hasTextQuery()Z
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->textQuery:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected reset()V
    .locals 1

    .line 75
    invoke-super {p0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->reset()V

    const/4 v0, 0x0

    .line 76
    iput-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->instrumentSearch:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    .line 77
    iput-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->cardQuery:Lcom/squareup/Card;

    .line 78
    iput-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->textQuery:Ljava/lang/String;

    return-void
.end method

.method public setQuery(Lcom/squareup/Card;)V
    .locals 1

    .line 64
    invoke-virtual {p0}, Lcom/squareup/activity/SearchResultsLoader;->reset()V

    .line 65
    iput-object p1, p0, Lcom/squareup/activity/SearchResultsLoader;->cardQuery:Lcom/squareup/Card;

    .line 66
    iget-object v0, p0, Lcom/squareup/activity/SearchResultsLoader;->cardConverter:Lcom/squareup/activity/ActivitySearchInstrumentConverter;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/ActivitySearchInstrumentConverter;->instrumentSearchForCard(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/activity/SearchResultsLoader;->instrumentSearch:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    return-void
.end method

.method public setQuery(Ljava/lang/String;)V
    .locals 0

    .line 59
    invoke-virtual {p0}, Lcom/squareup/activity/SearchResultsLoader;->reset()V

    .line 60
    iput-object p1, p0, Lcom/squareup/activity/SearchResultsLoader;->textQuery:Ljava/lang/String;

    return-void
.end method

.method public setQueryInstrumentSearch(Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;)V
    .locals 0

    .line 70
    invoke-virtual {p0}, Lcom/squareup/activity/SearchResultsLoader;->reset()V

    .line 71
    iput-object p1, p0, Lcom/squareup/activity/SearchResultsLoader;->instrumentSearch:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    return-void
.end method
