.class public final Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "RestockOnItemizedRefundCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;,
        Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRestockOnItemizedRefundCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RestockOnItemizedRefundCoordinator.kt\ncom/squareup/activity/refund/RestockOnItemizedRefundCoordinator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,159:1\n1642#2,2:160\n704#2:162\n777#2,2:163\n1360#2:165\n1429#2,3:166\n1360#2:169\n1429#2,3:170\n1550#2,3:173\n*E\n*S KotlinDebug\n*F\n+ 1 RestockOnItemizedRefundCoordinator.kt\ncom/squareup/activity/refund/RestockOnItemizedRefundCoordinator\n*L\n127#1,2:160\n147#1:162\n147#1,2:163\n149#1:165\n149#1,3:166\n150#1:169\n150#1,3:170\n154#1,3:173\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u0008\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u0002$%B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0010\u0010\u001a\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J\u0008\u0010\u001b\u001a\u00020\u001cH\u0002J\u0016\u0010\u001d\u001a\u00020\u00172\u000c\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020 0\u001fH\u0002J\u0018\u0010!\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\"\u001a\u00020\u0006H\u0002J\u000c\u0010#\u001a\u00020\u0017*\u00020\u0019H\u0002R\u000e\u0010\u000b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "eventHandler",
        "Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/activity/refund/RefundData;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;Lio/reactivex/Observable;Lcom/squareup/text/Formatter;)V",
        "MONEY_NOT_APPLICABLE",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "getEventHandler",
        "()Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;",
        "getMoneyFormatter",
        "()Lcom/squareup/text/Formatter;",
        "restockableItemList",
        "Landroid/widget/LinearLayout;",
        "getScreenData",
        "()Lio/reactivex/Observable;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "configureActionBar",
        "isAnyItemizationSelectedForRestock",
        "",
        "itemSelectionChanged",
        "restockableSelectedItemIndices",
        "",
        "",
        "populateRestockableItemList",
        "data",
        "bindViews",
        "EventHandler",
        "Factory",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final MONEY_NOT_APPLICABLE:Lcom/squareup/protos/common/Money;

.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private final eventHandler:Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private restockableItemList:Landroid/widget/LinearLayout;

.field private final screenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;Lio/reactivex/Observable;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    const-string v0, "eventHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->eventHandler:Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;

    iput-object p2, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->screenData:Lio/reactivex/Observable;

    iput-object p3, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 27
    new-instance p1, Lcom/squareup/protos/common/Money;

    const-wide/16 p2, -0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    sget-object p3, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {p1, p2, p3}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    iput-object p1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->MONEY_NOT_APPLICABLE:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public static final synthetic access$configureActionBar(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;Landroid/view/View;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->configureActionBar(Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$getRestockableItemList$p(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;)Landroid/widget/LinearLayout;
    .locals 1

    .line 20
    iget-object p0, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->restockableItemList:Landroid/widget/LinearLayout;

    if-nez p0, :cond_0

    const-string v0, "restockableItemList"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$itemSelectionChanged(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;Ljava/util/List;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->itemSelectionChanged(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$populateRestockableItemList(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;Landroid/view/View;Lcom/squareup/activity/refund/RefundData;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->populateRestockableItemList(Landroid/view/View;Lcom/squareup/activity/refund/RefundData;)V

    return-void
.end method

.method public static final synthetic access$setRestockableItemList$p(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;Landroid/widget/LinearLayout;)V
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->restockableItemList:Landroid/widget/LinearLayout;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 63
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 64
    sget v0, Lcom/squareup/activity/R$id;->restockable_items:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.restockable_items)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->restockableItemList:Landroid/widget/LinearLayout;

    return-void
.end method

.method private final configureActionBar(Landroid/view/View;)V
    .locals 4

    .line 68
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 69
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/activity/R$string;->restock_items:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 70
    new-instance v1, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$configureActionBar$baseConfig$1;

    iget-object v2, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->eventHandler:Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;

    invoke-direct {v1, v2}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$configureActionBar$baseConfig$1;-><init>(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v2, v1}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v1, :cond_0

    const-string v2, "actionBar"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    const-string v2, "actionBar.presenter"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->isAnyItemizationSelectedForRestock()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 76
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v2, Lcom/squareup/common/strings/R$string;->next:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 75
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 78
    new-instance v0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$configureActionBar$1;

    invoke-direct {v0, p0}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$configureActionBar$1;-><init>(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 79
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    goto :goto_0

    :cond_1
    if-nez v2, :cond_2

    .line 82
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v2, Lcom/squareup/activity/R$string;->restock_skip_restock:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 81
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 84
    sget v0, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextColors(I)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 85
    new-instance v0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$configureActionBar$2;

    invoke-direct {v0, p0}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$configureActionBar$2;-><init>(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 86
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 73
    :goto_0
    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void

    .line 86
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final isAnyItemizationSelectedForRestock()Z
    .locals 6

    .line 154
    iget-object v0, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->restockableItemList:Landroid/widget/LinearLayout;

    const-string v1, "restockableItemList"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    const/4 v2, 0x1

    invoke-static {v2, v0}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 173
    instance-of v3, v0, Ljava/util/Collection;

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    move-object v3, v0

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .line 174
    :cond_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    move-object v3, v0

    check-cast v3, Lkotlin/collections/IntIterator;

    invoke-virtual {v3}, Lkotlin/collections/IntIterator;->nextInt()I

    move-result v3

    .line 155
    iget-object v5, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->restockableItemList:Landroid/widget/LinearLayout;

    if-nez v5, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_4

    check-cast v3, Lcom/squareup/activity/refund/ItemizationRow;

    invoke-virtual {v3}, Lcom/squareup/activity/refund/ItemizationRow;->getChecked()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v4, 0x1

    goto :goto_0

    :cond_4
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.squareup.activity.refund.ItemizationRow"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :goto_0
    return v4
.end method

.method private final itemSelectionChanged(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->eventHandler:Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;

    .line 146
    iget-object v1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->restockableItemList:Landroid/widget/LinearLayout;

    const-string v2, "restockableItemList"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    const/4 v3, 0x1

    invoke-static {v3, v1}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 162
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/Collection;

    .line 163
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    move-result v6

    .line 147
    iget-object v7, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->restockableItemList:Landroid/widget/LinearLayout;

    if-nez v7, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v7, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_3

    check-cast v6, Lcom/squareup/activity/refund/ItemizationRow;

    invoke-virtual {v6}, Lcom/squareup/activity/refund/ItemizationRow;->getChecked()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.activity.refund.ItemizationRow"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 164
    :cond_4
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 165
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v4, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 166
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 167
    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->intValue()I

    move-result v5

    sub-int/2addr v5, v3

    .line 149
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 168
    :cond_5
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 169
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 170
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 171
    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    .line 150
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 172
    :cond_6
    check-cast v3, Ljava/util/List;

    .line 145
    invoke-interface {v0, v3}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;->onItemSelectionForRestockChanged(Ljava/util/List;)V

    return-void
.end method

.method private final populateRestockableItemList(Landroid/view/View;Lcom/squareup/activity/refund/RefundData;)V
    .locals 28

    move-object/from16 v8, p0

    .line 94
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/activity/refund/RefundData;->getIndicesOfRestockableSelectedItems()Ljava/util/List;

    move-result-object v9

    .line 95
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/activity/refund/RefundData;->getSelectedIndices()Ljava/util/List;

    move-result-object v10

    .line 96
    new-instance v11, Lcom/squareup/activity/refund/ItemizationRow$Factory;

    iget-object v0, v8, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->restockableItemList:Landroid/widget/LinearLayout;

    const-string v12, "restockableItemList"

    if-nez v0, :cond_0

    invoke-static {v12}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, v8, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-direct {v11, v0, v1}, Lcom/squareup/activity/refund/ItemizationRow$Factory;-><init>(Landroid/widget/LinearLayout;Lcom/squareup/text/Formatter;)V

    .line 99
    move-object v0, v9

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v13, 0x1

    xor-int/2addr v0, v13

    if-eqz v0, :cond_6

    .line 101
    iget-object v0, v8, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->restockableItemList:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    invoke-static {v12}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 104
    new-instance v0, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 105
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/activity/R$string;->restock_select_all_items:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    const-string/jumbo v1, "view.resources.getString\u2026restock_select_all_items)"

    invoke-static {v15, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v8, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->MONEY_NOT_APPLICABLE:Lcom/squareup/protos/common/Money;

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x7fc

    const/16 v27, 0x0

    move-object v14, v0

    move-object/from16 v16, v1

    .line 104
    invoke-direct/range {v14 .. v27}, Lcom/squareup/activity/refund/ItemizationDetails;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/math/BigDecimal;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 103
    invoke-virtual {v11, v0, v13}, Lcom/squareup/activity/refund/ItemizationRow$Factory;->newRow(Lcom/squareup/activity/refund/ItemizationDetails;Z)Lcom/squareup/activity/refund/ItemizationRow;

    move-result-object v14

    .line 110
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/activity/refund/RefundData;->getRestockIndices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v14, v0}, Lcom/squareup/activity/refund/ItemizationRow;->setChecked(Z)V

    .line 113
    move-object v0, v14

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$1;

    invoke-direct {v2, v8, v14, v9}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$1;-><init>(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;Lcom/squareup/activity/refund/ItemizationRow;Ljava/util/List;)V

    check-cast v2, Lrx/functions/Action1;

    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 125
    iget-object v1, v8, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->restockableItemList:Landroid/widget/LinearLayout;

    if-nez v1, :cond_3

    invoke-static {v12}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 127
    move-object v0, v9

    check-cast v0, Ljava/lang/Iterable;

    .line 160
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 128
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/activity/refund/RefundData;->getItemizationDetails()Ljava/util/List;

    move-result-object v1

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 129
    invoke-virtual {v11, v1, v13}, Lcom/squareup/activity/refund/ItemizationRow$Factory;->newRow(Lcom/squareup/activity/refund/ItemizationDetails;Z)Lcom/squareup/activity/refund/ItemizationRow;

    move-result-object v1

    .line 130
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/activity/refund/RefundData;->getRestockIndices()Ljava/util/List;

    move-result-object v2

    .line 131
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/squareup/activity/refund/ItemizationRow;->setChecked(Z)V

    .line 132
    move-object v7, v1

    check-cast v7, Landroid/view/View;

    invoke-static {v7}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v6

    new-instance v16, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;

    move-object/from16 v0, v16

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object v4, v10

    move-object v5, v11

    move-object v13, v6

    move-object v6, v14

    move-object/from16 v18, v10

    move-object v10, v7

    move-object v7, v9

    invoke-direct/range {v0 .. v7}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$populateRestockableItemList$$inlined$forEach$lambda$1;-><init>(Lcom/squareup/activity/refund/ItemizationRow;Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;Lcom/squareup/activity/refund/RefundData;Ljava/util/List;Lcom/squareup/activity/refund/ItemizationRow$Factory;Lcom/squareup/activity/refund/ItemizationRow;Ljava/util/List;)V

    move-object/from16 v0, v16

    check-cast v0, Lrx/functions/Action1;

    invoke-virtual {v13, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 140
    iget-object v0, v8, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->restockableItemList:Landroid/widget/LinearLayout;

    if-nez v0, :cond_4

    invoke-static {v12}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object/from16 v10, v18

    const/4 v13, 0x1

    goto :goto_1

    :cond_5
    return-void

    .line 99
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Check failed."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->bindViews(Landroid/view/View;)V

    .line 52
    new-instance v0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$attach$1;

    iget-object v1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->eventHandler:Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;

    invoke-direct {v0, v1}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$attach$1;-><init>(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->screenData:Lio/reactivex/Observable;

    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 55
    new-instance v1, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$attach$2;-><init>(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screenData.distinctUntil\u2026ActionBar(view)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method

.method public final getEventHandler()Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->eventHandler:Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;

    return-object v0
.end method

.method public final getMoneyFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object v0
.end method

.method public final getScreenData()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;->screenData:Lio/reactivex/Observable;

    return-object v0
.end method
