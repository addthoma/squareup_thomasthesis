.class public interface abstract Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;
.super Ljava/lang/Object;
.source "RefundItemizationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/RefundItemizationCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RefundItemizationEventHandler"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0016\u0010\u0004\u001a\u00020\u00032\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H&J\u0008\u0010\u0008\u001a\u00020\u0003H&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000bH&J\u0010\u0010\u000c\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u000eH&\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;",
        "",
        "onCancelRefundPressed",
        "",
        "onItemsSelected",
        "itemIndices",
        "",
        "",
        "onNextPressed",
        "onRefundAmountChanged",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "onRefundModeSelected",
        "mode",
        "Lcom/squareup/activity/refund/RefundMode;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract onCancelRefundPressed()V
.end method

.method public abstract onItemsSelected(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onNextPressed()V
.end method

.method public abstract onRefundAmountChanged(Lcom/squareup/protos/common/Money;)V
.end method

.method public abstract onRefundModeSelected(Lcom/squareup/activity/refund/RefundMode;)V
.end method
