.class public final synthetic Lcom/squareup/activity/refund/RefundData$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I

.field public static final synthetic $EnumSwitchMapping$3:[I

.field public static final synthetic $EnumSwitchMapping$4:[I

.field public static final synthetic $EnumSwitchMapping$5:[I

.field public static final synthetic $EnumSwitchMapping$6:[I

.field public static final synthetic $EnumSwitchMapping$7:[I

.field public static final synthetic $EnumSwitchMapping$8:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 4

    invoke-static {}, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->values()[Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->OTHER_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    invoke-static {}, Lcom/squareup/activity/refund/RefundMode;->values()[Lcom/squareup/activity/refund/RefundMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->AMOUNT:Lcom/squareup/activity/refund/RefundMode;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->ITEMS:Lcom/squareup/activity/refund/RefundMode;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/activity/refund/RefundMode;->values()[Lcom/squareup/activity/refund/RefundMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->AMOUNT:Lcom/squareup/activity/refund/RefundMode;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->ITEMS:Lcom/squareup/activity/refund/RefundMode;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/activity/refund/RefundMode;->values()[Lcom/squareup/activity/refund/RefundMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->AMOUNT:Lcom/squareup/activity/refund/RefundMode;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->ITEMS:Lcom/squareup/activity/refund/RefundMode;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/activity/refund/RefundMode;->values()[Lcom/squareup/activity/refund/RefundMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->AMOUNT:Lcom/squareup/activity/refund/RefundMode;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->ITEMS:Lcom/squareup/activity/refund/RefundMode;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/activity/refund/RefundMode;->values()[Lcom/squareup/activity/refund/RefundMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->AMOUNT:Lcom/squareup/activity/refund/RefundMode;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->ITEMS:Lcom/squareup/activity/refund/RefundMode;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/activity/refund/RefundMode;->values()[Lcom/squareup/activity/refund/RefundMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->AMOUNT:Lcom/squareup/activity/refund/RefundMode;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->ITEMS:Lcom/squareup/activity/refund/RefundMode;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/activity/refund/RefundMode;->values()[Lcom/squareup/activity/refund/RefundMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$7:[I

    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$7:[I

    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->AMOUNT:Lcom/squareup/activity/refund/RefundMode;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$7:[I

    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->ITEMS:Lcom/squareup/activity/refund/RefundMode;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/billhistory/model/TenderHistory$Type;->values()[Lcom/squareup/billhistory/model/TenderHistory$Type;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$8:[I

    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$8:[I

    sget-object v1, Lcom/squareup/billhistory/model/TenderHistory$Type;->CASH:Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-virtual {v1}, Lcom/squareup/billhistory/model/TenderHistory$Type;->ordinal()I

    move-result v1

    aput v2, v0, v1

    return-void
.end method
