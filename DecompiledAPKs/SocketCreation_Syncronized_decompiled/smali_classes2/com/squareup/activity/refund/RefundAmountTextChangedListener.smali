.class public final Lcom/squareup/activity/refund/RefundAmountTextChangedListener;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "RefundItemizationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRefundItemizationCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RefundItemizationCoordinator.kt\ncom/squareup/activity/refund/RefundAmountTextChangedListener\n*L\n1#1,320:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundAmountTextChangedListener;",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "localeHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "handler",
        "Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;",
        "(Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;)V",
        "afterTextChanged",
        "",
        "editable",
        "Landroid/text/Editable;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final handler:Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;

.field private final localeHelper:Lcom/squareup/money/PriceLocaleHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;)V
    .locals 1

    const-string v0, "localeHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/refund/RefundAmountTextChangedListener;->localeHelper:Lcom/squareup/money/PriceLocaleHelper;

    iput-object p2, p0, Lcom/squareup/activity/refund/RefundAmountTextChangedListener;->handler:Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    const-string v0, "editable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundAmountTextChangedListener;->localeHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 46
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundAmountTextChangedListener;->handler:Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;

    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;->onRefundAmountChanged(Lcom/squareup/protos/common/Money;)V

    :cond_0
    return-void
.end method
