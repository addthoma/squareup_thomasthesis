.class final Lcom/squareup/activity/refund/RefundItemizationCoordinator$addAllItemsRow$1;
.super Ljava/lang/Object;
.source "RefundItemizationCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/activity/refund/RefundItemizationCoordinator;->addAllItemsRow(Lcom/squareup/activity/refund/ItemizationRow$Factory;Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/ItemizationRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRefundItemizationCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RefundItemizationCoordinator.kt\ncom/squareup/activity/refund/RefundItemizationCoordinator$addAllItemsRow$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,320:1\n1642#2,2:321\n*E\n*S KotlinDebug\n*F\n+ 1 RefundItemizationCoordinator.kt\ncom/squareup/activity/refund/RefundItemizationCoordinator$addAllItemsRow$1\n*L\n243#1,2:321\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "call",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $allItemsRow:Lcom/squareup/activity/refund/ItemizationRow;

.field final synthetic this$0:Lcom/squareup/activity/refund/RefundItemizationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/activity/refund/RefundItemizationCoordinator;Lcom/squareup/activity/refund/ItemizationRow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$addAllItemsRow$1;->this$0:Lcom/squareup/activity/refund/RefundItemizationCoordinator;

    iput-object p2, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$addAllItemsRow$1;->$allItemsRow:Lcom/squareup/activity/refund/ItemizationRow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 50
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$addAllItemsRow$1;->call(Lkotlin/Unit;)V

    return-void
.end method

.method public final call(Lkotlin/Unit;)V
    .locals 3

    .line 241
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$addAllItemsRow$1;->$allItemsRow:Lcom/squareup/activity/refund/ItemizationRow;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/ItemizationRow;->toggle()V

    .line 242
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$addAllItemsRow$1;->$allItemsRow:Lcom/squareup/activity/refund/ItemizationRow;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/ItemizationRow;->getChecked()Z

    move-result p1

    .line 243
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$addAllItemsRow$1;->this$0:Lcom/squareup/activity/refund/RefundItemizationCoordinator;

    invoke-static {v0}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->access$getItems$p(Lcom/squareup/activity/refund/RefundItemizationCoordinator;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v1, v0}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 321
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Lkotlin/collections/IntIterator;

    invoke-virtual {v1}, Lkotlin/collections/IntIterator;->nextInt()I

    move-result v1

    .line 244
    iget-object v2, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$addAllItemsRow$1;->this$0:Lcom/squareup/activity/refund/RefundItemizationCoordinator;

    invoke-static {v2}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->access$getItems$p(Lcom/squareup/activity/refund/RefundItemizationCoordinator;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    check-cast v1, Lcom/squareup/activity/refund/ItemizationRow;

    invoke-virtual {v1, p1}, Lcom/squareup/activity/refund/ItemizationRow;->setChecked(Z)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.activity.refund.ItemizationRow"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 246
    :cond_1
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$addAllItemsRow$1;->this$0:Lcom/squareup/activity/refund/RefundItemizationCoordinator;

    invoke-static {p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->access$itemSelectionChanged(Lcom/squareup/activity/refund/RefundItemizationCoordinator;)V

    return-void
.end method
