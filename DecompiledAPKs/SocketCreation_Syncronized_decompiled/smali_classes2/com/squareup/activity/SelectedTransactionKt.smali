.class public final Lcom/squareup/activity/SelectedTransactionKt;
.super Ljava/lang/Object;
.source "SelectedTransaction.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectedTransaction.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectedTransaction.kt\ncom/squareup/activity/SelectedTransactionKt\n+ 2 HistoricalTransaction.kt\ncom/squareup/transactionhistory/historical/HistoricalTransactionKt\n*L\n1#1,429:1\n44#2:430\n*E\n*S KotlinDebug\n*F\n+ 1 SelectedTransaction.kt\ncom/squareup/activity/SelectedTransactionKt\n*L\n428#1:430\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0008\u001a\u0004\u0018\u00010\t*\u00020\n\"\u0017\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0003\u0010\u0004\"\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0004\u00a8\u0006\u000b"
    }
    d2 = {
        "NO_BACKING_SUMMARY",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
        "getNO_BACKING_SUMMARY",
        "()Lcom/squareup/util/Optional;",
        "NO_BACKING_TRANSACTION",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransaction;",
        "getNO_BACKING_TRANSACTION",
        "getBackingTransactionAsBillHistory",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "Lcom/squareup/activity/SelectedTransaction;",
        "bill-history-ui_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final NO_BACKING_SUMMARY:Lcom/squareup/util/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;"
        }
    .end annotation
.end field

.field private static final NO_BACKING_TRANSACTION:Lcom/squareup/util/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 413
    sget-object v0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v0}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    sput-object v0, Lcom/squareup/activity/SelectedTransactionKt;->NO_BACKING_SUMMARY:Lcom/squareup/util/Optional;

    .line 420
    sget-object v0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v0}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    sput-object v0, Lcom/squareup/activity/SelectedTransactionKt;->NO_BACKING_TRANSACTION:Lcom/squareup/util/Optional;

    return-void
.end method

.method public static final getBackingTransactionAsBillHistory(Lcom/squareup/activity/SelectedTransaction;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    const-string v0, "$this$getBackingTransactionAsBillHistory"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 428
    invoke-virtual {p0}, Lcom/squareup/activity/SelectedTransaction;->getBackingTransaction()Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 430
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;->getBillHistory()Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Lcom/squareup/billhistory/model/BillHistory;

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.billhistory.model.BillHistory"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getNO_BACKING_SUMMARY()Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;"
        }
    .end annotation

    .line 413
    sget-object v0, Lcom/squareup/activity/SelectedTransactionKt;->NO_BACKING_SUMMARY:Lcom/squareup/util/Optional;

    return-object v0
.end method

.method public static final getNO_BACKING_TRANSACTION()Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransaction;",
            ">;"
        }
    .end annotation

    .line 420
    sget-object v0, Lcom/squareup/activity/SelectedTransactionKt;->NO_BACKING_TRANSACTION:Lcom/squareup/util/Optional;

    return-object v0
.end method
