.class public final Lcom/squareup/activity/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final add_gift_card_or_swipe:I = 0x7f12007f

.field public static final cannot_process_refund_title:I = 0x7f1202b1

.field public static final cash_drawer_warning_text:I = 0x7f120371

.field public static final contactless_ready_to_tap:I = 0x7f1204b1

.field public static final dialog_dismiss:I = 0x7f120868

.field public static final dialog_try_again:I = 0x7f12086a

.field public static final exchange:I = 0x7f120a91

.field public static final exchange_with_reason:I = 0x7f120a92

.field public static final failed_restock_attempt:I = 0x7f120aa8

.field public static final items_from_multiple_source_bills_warning:I = 0x7f120e6d

.field public static final processing_payments_expiring_many:I = 0x7f1214f2

.field public static final processing_payments_expiring_one:I = 0x7f1214f3

.field public static final processing_payments_text:I = 0x7f1214f7

.field public static final receipt_email:I = 0x7f1215c2

.field public static final receipt_new:I = 0x7f1215c9

.field public static final receipt_paper:I = 0x7f1215ca

.field public static final receipt_paper_formal:I = 0x7f1215cb

.field public static final receipt_reissue:I = 0x7f1215cc

.field public static final receipt_sms:I = 0x7f1215cd

.field public static final refund:I = 0x7f121628

.field public static final refund_amount:I = 0x7f121629

.field public static final refund_amount_help:I = 0x7f12162a

.field public static final refund_amount_max:I = 0x7f12162b

.field public static final refund_complete:I = 0x7f12162f

.field public static final refund_custom_amount:I = 0x7f121632

.field public static final refund_failed:I = 0x7f121633

.field public static final refund_fees:I = 0x7f121634

.field public static final refund_felica_id:I = 0x7f121635

.field public static final refund_felica_quicpay:I = 0x7f121636

.field public static final refund_gift_card_credit_card_hint:I = 0x7f121637

.field public static final refund_gift_card_help:I = 0x7f121638

.field public static final refund_gift_card_hint:I = 0x7f121639

.field public static final refund_gift_card_many_line_items:I = 0x7f12163b

.field public static final refund_gift_card_many_line_items_subtext:I = 0x7f12163c

.field public static final refund_gift_card_split_tender_hint:I = 0x7f12163d

.field public static final refund_help_text_url:I = 0x7f12163e

.field public static final refund_includes_taxes_and_discounts:I = 0x7f121640

.field public static final refund_issue:I = 0x7f121641

.field public static final refund_legal:I = 0x7f121643

.field public static final refund_reason_other_hint:I = 0x7f12164a

.field public static final refund_reason_other_title:I = 0x7f12164b

.field public static final refund_select_all_items:I = 0x7f12164e

.field public static final refund_tap_contactless_card:I = 0x7f121652

.field public static final refund_tap_dip_message:I = 0x7f121653

.field public static final refund_tap_dip_title:I = 0x7f121654

.field public static final refund_tax_help:I = 0x7f121655

.field public static final refund_taxes_and_fees:I = 0x7f121656

.field public static final refund_tender_type_and_amount:I = 0x7f121657

.field public static final refund_with_max_amount_help:I = 0x7f12165a

.field public static final refund_with_max_amount_with_tax_info_help:I = 0x7f12165b

.field public static final refunded_amount:I = 0x7f12165d

.field public static final refunded_section_header:I = 0x7f12165e

.field public static final refunding:I = 0x7f12165f

.field public static final restock_items:I = 0x7f12169c

.field public static final restock_recoverable_failure_message:I = 0x7f12169d

.field public static final restock_select_all_items:I = 0x7f12169e

.field public static final restock_skip_restock:I = 0x7f12169f

.field public static final return_or_exchange:I = 0x7f1216a5

.field public static final tip:I = 0x7f1219ae

.field public static final tip_for_card:I = 0x7f1219b3

.field public static final uppercase_amount_to_refund:I = 0x7f121b06

.field public static final uppercase_card_number:I = 0x7f121b0b

.field public static final uppercase_exchange:I = 0x7f121b27

.field public static final uppercase_gift_card:I = 0x7f121b2c

.field public static final uppercase_refund:I = 0x7f121b72

.field public static final uppercase_refund_failed:I = 0x7f121b73

.field public static final uppercase_refund_pending:I = 0x7f121b74

.field public static final uppercase_refund_reason:I = 0x7f121b75

.field public static final uppercase_refund_to:I = 0x7f121b76

.field public static final view_details:I = 0x7f121ba9


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
