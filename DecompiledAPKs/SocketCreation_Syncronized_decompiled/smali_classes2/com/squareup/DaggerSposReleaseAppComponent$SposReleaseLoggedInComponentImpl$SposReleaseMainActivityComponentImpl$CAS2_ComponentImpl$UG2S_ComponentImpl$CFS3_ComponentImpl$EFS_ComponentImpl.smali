.class final Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "EFS_ComponentImpl"
.end annotation


# instance fields
.field private emptyFilterContentPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private multiOptionFilterContentPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private multiOptionFilterHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/filters/MultiOptionFilterHelper;",
            ">;"
        }
    .end annotation
.end field

.field private presenterProvider:Ljavax/inject/Provider;

.field private singleOptionFilterContentPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private singleOptionFilterHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/filters/SingleOptionFilterHelper;",
            ">;"
        }
    .end annotation
.end field

.field private singleTextFilterContentPresenterProvider:Ljavax/inject/Provider;

.field private singleTextFilterHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/filters/SingleTextFilterHelper;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$5:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;

.field private visitFrequencyFilterContentPresenterProvider:Ljavax/inject/Provider;


# direct methods
.method private constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;)V
    .locals 0

    .line 50193
    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->this$5:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50195
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->initialize()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    .line 50171
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;)V

    return-void
.end method

.method private initialize()V
    .locals 3

    .line 50200
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->this$5:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;->access$177700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->this$5:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->this$5:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/crm/cards/EditFilterScreen_Presenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/cards/EditFilterScreen_Presenter_Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->presenterProvider:Ljavax/inject/Provider;

    .line 50201
    invoke-static {}, Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentPresenter_Factory;->create()Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentPresenter_Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->emptyFilterContentPresenterProvider:Ljavax/inject/Provider;

    .line 50202
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->this$5:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$66700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/crm/filters/MultiOptionFilterHelper_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/crm/filters/MultiOptionFilterHelper_Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->multiOptionFilterHelperProvider:Ljavax/inject/Provider;

    .line 50203
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->multiOptionFilterHelperProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter_Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->multiOptionFilterContentPresenterProvider:Ljavax/inject/Provider;

    .line 50204
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->this$5:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/crm/filters/SingleOptionFilterHelper_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/crm/filters/SingleOptionFilterHelper_Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->singleOptionFilterHelperProvider:Ljavax/inject/Provider;

    .line 50205
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->singleOptionFilterHelperProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter_Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->singleOptionFilterContentPresenterProvider:Ljavax/inject/Provider;

    .line 50206
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->this$5:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/crm/filters/SingleTextFilterHelper_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/crm/filters/SingleTextFilterHelper_Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->singleTextFilterHelperProvider:Ljavax/inject/Provider;

    .line 50207
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->singleTextFilterHelperProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter_Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->singleTextFilterContentPresenterProvider:Ljavax/inject/Provider;

    .line 50208
    invoke-static {}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter_Factory;->create()Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter_Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->visitFrequencyFilterContentPresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private injectEditFilterCardView(Lcom/squareup/ui/crm/cards/EditFilterCardView;)Lcom/squareup/ui/crm/cards/EditFilterCardView;
    .locals 1

    .line 50236
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/EditFilterCardView_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/cards/EditFilterCardView;Ljava/lang/Object;)V

    return-object p1
.end method

.method private injectEmptyFilterContentView(Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;)Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;
    .locals 1

    .line 50242
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->emptyFilterContentPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentPresenter;)V

    return-object p1
.end method

.method private injectMultiOptionFilterContentView(Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;)Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;
    .locals 1

    .line 50248
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->multiOptionFilterContentPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;)V

    return-object p1
.end method

.method private injectSingleOptionFilterContentView(Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;)Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;
    .locals 1

    .line 50254
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->singleOptionFilterContentPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;)V

    return-object p1
.end method

.method private injectSingleTextFilterContentView(Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;)Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;
    .locals 1

    .line 50260
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->singleTextFilterContentPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;Ljava/lang/Object;)V

    return-object p1
.end method

.method private injectVisitFrequencyFilterContentView(Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;)Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;
    .locals 1

    .line 50266
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->visitFrequencyFilterContentPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;Ljava/lang/Object;)V

    return-object p1
.end method


# virtual methods
.method public inject(Lcom/squareup/ui/crm/cards/EditFilterCardView;)V
    .locals 0

    .line 50213
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->injectEditFilterCardView(Lcom/squareup/ui/crm/cards/EditFilterCardView;)Lcom/squareup/ui/crm/cards/EditFilterCardView;

    return-void
.end method

.method public inject(Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;)V
    .locals 0

    .line 50217
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->injectEmptyFilterContentView(Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;)Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;

    return-void
.end method

.method public inject(Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;)V
    .locals 0

    .line 50221
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->injectMultiOptionFilterContentView(Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;)Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;

    return-void
.end method

.method public inject(Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;)V
    .locals 0

    .line 50225
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->injectSingleOptionFilterContentView(Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;)Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;

    return-void
.end method

.method public inject(Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;)V
    .locals 0

    .line 50229
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->injectSingleTextFilterContentView(Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;)Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;

    return-void
.end method

.method public inject(Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;)V
    .locals 0

    .line 50233
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$UG2S_ComponentImpl$CFS3_ComponentImpl$EFS_ComponentImpl;->injectVisitFrequencyFilterContentView(Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;)Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;

    return-void
.end method
