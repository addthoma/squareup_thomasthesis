.class public final Lcom/squareup/FileThreadEnforcer_Factory;
.super Ljava/lang/Object;
.source "FileThreadEnforcer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/FileThreadEnforcer;",
        ">;"
    }
.end annotation


# instance fields
.field private final fileThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/os/HandlerThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/os/HandlerThread;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/FileThreadEnforcer_Factory;->fileThreadProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/FileThreadEnforcer_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/os/HandlerThread;",
            ">;)",
            "Lcom/squareup/FileThreadEnforcer_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/FileThreadEnforcer_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/FileThreadEnforcer_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/os/HandlerThread;)Lcom/squareup/FileThreadEnforcer;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/FileThreadEnforcer;

    invoke-direct {v0, p0}, Lcom/squareup/FileThreadEnforcer;-><init>(Landroid/os/HandlerThread;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/FileThreadEnforcer;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/FileThreadEnforcer_Factory;->fileThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/HandlerThread;

    invoke-static {v0}, Lcom/squareup/FileThreadEnforcer_Factory;->newInstance(Landroid/os/HandlerThread;)Lcom/squareup/FileThreadEnforcer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/FileThreadEnforcer_Factory;->get()Lcom/squareup/FileThreadEnforcer;

    move-result-object v0

    return-object v0
.end method
