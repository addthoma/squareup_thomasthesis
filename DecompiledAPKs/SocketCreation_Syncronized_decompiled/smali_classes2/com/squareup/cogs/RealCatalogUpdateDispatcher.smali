.class public Lcom/squareup/cogs/RealCatalogUpdateDispatcher;
.super Ljava/lang/Object;
.source "RealCatalogUpdateDispatcher.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;


# instance fields
.field private cogsBatchCount:I

.field private connectV2BatchCount:I

.field private deletedCogsObjects:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;"
        }
    .end annotation
.end field

.field private deletedConnectV2Objects:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final eventSink:Lcom/squareup/badbus/BadEventSink;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private updatedCogObjects:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;"
        }
    .end annotation
.end field

.field private updatedConnectV2Objects:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/badbus/BadEventSink;Lcom/squareup/thread/executor/MainThread;)V
    .locals 1

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 23
    iput v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->cogsBatchCount:I

    .line 24
    iput v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->connectV2BatchCount:I

    .line 27
    iput-object p1, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->eventSink:Lcom/squareup/badbus/BadEventSink;

    .line 28
    iput-object p2, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-void
.end method

.method private doCleanUp()V
    .locals 1

    const/4 v0, 0x0

    .line 83
    iput-object v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->updatedCogObjects:Ljava/util/Collection;

    .line 84
    iput-object v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->deletedCogsObjects:Ljava/util/Collection;

    .line 85
    iput-object v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->updatedConnectV2Objects:Ljava/util/Collection;

    .line 86
    iput-object v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->deletedConnectV2Objects:Ljava/util/Collection;

    const/4 v0, 0x0

    .line 87
    iput v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->cogsBatchCount:I

    .line 88
    iput v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->connectV2BatchCount:I

    return-void
.end method

.method private getConnectV2Objects(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
            "*>;>;)",
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;"
        }
    .end annotation

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_1

    .line 120
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;

    .line 121
    instance-of v2, v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    if-eqz v2, :cond_0

    .line 122
    check-cast v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static synthetic lambda$aztDYpDMoUlglhXZDODUclMyEms(Lcom/squareup/cogs/RealCatalogUpdateDispatcher;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->doCleanUp()V

    return-void
.end method

.method private postUpdates()V
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cogs/-$$Lambda$RealCatalogUpdateDispatcher$419Q2WTWQ3z3qPouFgDl1kp2CvQ;

    invoke-direct {v1, p0}, Lcom/squareup/cogs/-$$Lambda$RealCatalogUpdateDispatcher$419Q2WTWQ3z3qPouFgDl1kp2CvQ;-><init>(Lcom/squareup/cogs/RealCatalogUpdateDispatcher;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public cleanUp()V
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cogs/-$$Lambda$RealCatalogUpdateDispatcher$aztDYpDMoUlglhXZDODUclMyEms;

    invoke-direct {v1, p0}, Lcom/squareup/cogs/-$$Lambda$RealCatalogUpdateDispatcher$aztDYpDMoUlglhXZDODUclMyEms;-><init>(Lcom/squareup/cogs/RealCatalogUpdateDispatcher;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public dispatchLocalEdits()V
    .locals 0

    .line 75
    invoke-direct {p0}, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->postUpdates()V

    return-void
.end method

.method public dispatchObjectsToBeChanged(Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
            "*>;>;)V"
        }
    .end annotation

    return-void
.end method

.method public dispatchSyncUpdates()V
    .locals 0

    .line 71
    invoke-direct {p0}, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->postUpdates()V

    return-void
.end method

.method public synthetic lambda$postUpdates$2$RealCatalogUpdateDispatcher()V
    .locals 7

    .line 93
    iget v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->cogsBatchCount:I

    if-nez v0, :cond_1

    iget v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->connectV2BatchCount:I

    if-eqz v0, :cond_0

    goto :goto_0

    .line 94
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no cogs nor Connect V2 updated or deleted objects. Nothing to dispatch."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_1
    :goto_0
    iget v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->cogsBatchCount:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-le v0, v2, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    .line 98
    :goto_1
    iget v3, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->cogsBatchCount:I

    if-lez v3, :cond_3

    .line 99
    iget-object v3, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance v4, Lcom/squareup/cogs/CatalogUpdateEvent;

    iget-object v5, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->updatedCogObjects:Ljava/util/Collection;

    iget-object v6, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->deletedCogsObjects:Ljava/util/Collection;

    invoke-direct {v4, v5, v6, v0}, Lcom/squareup/cogs/CatalogUpdateEvent;-><init>(Ljava/util/Collection;Ljava/util/Collection;Z)V

    invoke-interface {v3, v4}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    .line 103
    :cond_3
    iget v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->connectV2BatchCount:I

    if-le v0, v2, :cond_4

    const/4 v1, 0x1

    .line 104
    :cond_4
    iget v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->connectV2BatchCount:I

    if-lez v0, :cond_5

    .line 105
    iget-object v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->eventSink:Lcom/squareup/badbus/BadEventSink;

    iget-object v2, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->updatedConnectV2Objects:Ljava/util/Collection;

    .line 107
    invoke-direct {p0, v2}, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->getConnectV2Objects(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->deletedConnectV2Objects:Ljava/util/Collection;

    .line 108
    invoke-direct {p0, v3}, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->getConnectV2Objects(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v3

    .line 106
    invoke-static {v2, v3, v1}, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->of(Ljava/util/Collection;Ljava/util/Collection;Z)Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;

    move-result-object v1

    .line 105
    invoke-interface {v0, v1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    .line 112
    :cond_5
    invoke-direct {p0}, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->doCleanUp()V

    return-void
.end method

.method public synthetic lambda$updateCogsObjects$0$RealCatalogUpdateDispatcher(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 2

    .line 34
    iget v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->cogsBatchCount:I

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 35
    iput-object p1, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->updatedCogObjects:Ljava/util/Collection;

    .line 36
    iput-object p2, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->deletedCogsObjects:Ljava/util/Collection;

    goto :goto_0

    :cond_0
    if-ne v0, v1, :cond_1

    .line 39
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->updatedCogObjects:Ljava/util/Collection;

    .line 40
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->deletedCogsObjects:Ljava/util/Collection;

    .line 42
    :cond_1
    :goto_0
    iget p1, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->cogsBatchCount:I

    add-int/2addr p1, v1

    iput p1, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->cogsBatchCount:I

    return-void
.end method

.method public synthetic lambda$updateConnectV2Objects$1$RealCatalogUpdateDispatcher(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 2

    .line 49
    iget v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->connectV2BatchCount:I

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 50
    iput-object p1, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->updatedConnectV2Objects:Ljava/util/Collection;

    .line 51
    iput-object p2, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->deletedConnectV2Objects:Ljava/util/Collection;

    goto :goto_0

    :cond_0
    if-ne v0, v1, :cond_1

    .line 54
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->updatedConnectV2Objects:Ljava/util/Collection;

    .line 55
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->deletedConnectV2Objects:Ljava/util/Collection;

    .line 57
    :cond_1
    :goto_0
    iget p1, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->connectV2BatchCount:I

    add-int/2addr p1, v1

    iput p1, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->connectV2BatchCount:I

    return-void
.end method

.method public updateCogsObjects(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;)V"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cogs/-$$Lambda$RealCatalogUpdateDispatcher$AVy9D4OaKlhNnR_w_ljz08k-h-w;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/cogs/-$$Lambda$RealCatalogUpdateDispatcher$AVy9D4OaKlhNnR_w_ljz08k-h-w;-><init>(Lcom/squareup/cogs/RealCatalogUpdateDispatcher;Ljava/util/Collection;Ljava/util/Collection;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public updateConnectV2Objects(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
            "*>;>;)V"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cogs/-$$Lambda$RealCatalogUpdateDispatcher$vtc5lENYL6zZyKI7nDATkVs01o0;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/cogs/-$$Lambda$RealCatalogUpdateDispatcher$vtc5lENYL6zZyKI7nDATkVs01o0;-><init>(Lcom/squareup/cogs/RealCatalogUpdateDispatcher;Ljava/util/Collection;Ljava/util/Collection;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
