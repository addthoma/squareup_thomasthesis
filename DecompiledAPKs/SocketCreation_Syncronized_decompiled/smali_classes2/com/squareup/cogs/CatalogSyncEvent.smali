.class Lcom/squareup/cogs/CatalogSyncEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "CatalogSyncEvent.java"


# static fields
.field private static final CATALOG_NAME:Ljava/lang/String; = "catalog_sync"


# instance fields
.field private final catalog_sync_database_duration_ms:J

.field private final catalog_sync_database_objects_per_second:J

.field private final catalog_sync_initial_sync:Z

.field private final catalog_sync_number_of_objects:J

.field private final catalog_sync_number_of_objects_deleted:J

.field private final catalog_sync_number_of_objects_updated:J

.field private final catalog_sync_total_duration_ms:J

.field private final catalog_sync_total_object_per_second:J


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;)V
    .locals 2

    const-string v0, "catalog_sync"

    .line 44
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    .line 45
    iget-boolean v0, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->initialSync:Z

    iput-boolean v0, p0, Lcom/squareup/cogs/CatalogSyncEvent;->catalog_sync_initial_sync:Z

    .line 46
    iget v0, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->numberOfObjects:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/squareup/cogs/CatalogSyncEvent;->catalog_sync_number_of_objects:J

    .line 47
    iget v0, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->numberOfObjectsUpdated:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/squareup/cogs/CatalogSyncEvent;->catalog_sync_number_of_objects_updated:J

    .line 48
    iget v0, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->numberOfObjectsDeleted:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/squareup/cogs/CatalogSyncEvent;->catalog_sync_number_of_objects_deleted:J

    .line 49
    iget-wide v0, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->totalDurationMs:J

    iput-wide v0, p0, Lcom/squareup/cogs/CatalogSyncEvent;->catalog_sync_total_duration_ms:J

    .line 50
    iget v0, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->totalObjectsPerSecond:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/squareup/cogs/CatalogSyncEvent;->catalog_sync_total_object_per_second:J

    .line 51
    iget-wide v0, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->databaseDurationMs:J

    iput-wide v0, p0, Lcom/squareup/cogs/CatalogSyncEvent;->catalog_sync_database_duration_ms:J

    .line 52
    iget p1, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->databaseObjectsPerSecond:I

    int-to-long v0, p1

    iput-wide v0, p0, Lcom/squareup/cogs/CatalogSyncEvent;->catalog_sync_database_objects_per_second:J

    return-void
.end method
