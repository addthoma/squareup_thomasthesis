.class public interface abstract Lcom/squareup/cogs/CogsService;
.super Ljava/lang/Object;
.source "CogsService.java"


# virtual methods
.method public abstract syncItems(Lcom/squareup/api/rpc/RequestBatch;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/api/rpc/RequestBatch;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/rpc/RequestBatch;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lokhttp3/ResponseBody;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/items/sync"
    .end annotation

    .annotation runtime Lretrofit2/http/Streaming;
    .end annotation
.end method
