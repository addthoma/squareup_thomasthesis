.class public Lcom/squareup/checkout/OrderModifier;
.super Ljava/lang/Object;
.source "OrderModifier.kt"

# interfaces
.implements Lcom/squareup/calc/order/Modifier;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkout/OrderModifier$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderModifier.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderModifier.kt\ncom/squareup/checkout/OrderModifier\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,413:1\n1651#2,3:414\n1651#2,3:417\n*E\n*S KotlinDebug\n*F\n+ 1 OrderModifier.kt\ncom/squareup/checkout/OrderModifier\n*L\n194#1,3:414\n217#1,3:417\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0016\u0018\u0000 J2\u00020\u0001:\u0001JB5\u0008\u0012\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cB\u000f\u0008\u0002\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0013\u00103\u001a\u00020\t2\u0008\u00104\u001a\u0004\u0018\u000105H\u0096\u0002J\n\u00106\u001a\u0004\u0018\u00010\u0003H\u0016J\u0010\u00107\u001a\u00020$2\u0006\u00108\u001a\u000209H\u0016J\u0006\u0010:\u001a\u00020\tJ\u0008\u0010;\u001a\u00020 H\u0016J\u0008\u0010<\u001a\u00020$H\u0016J\u0008\u0010\u0002\u001a\u00020=H\u0016J\u0008\u0010>\u001a\u00020 H\u0016J\u0010\u0010?\u001a\u00020@2\u0006\u0010A\u001a\u00020@H\u0016J\u0016\u0010B\u001a\u00020\u000e2\u0006\u0010C\u001a\u00020@2\u0006\u0010D\u001a\u00020EJ\u0008\u0010F\u001a\u00020$H\u0016J\u0010\u0010G\u001a\u00020=2\u0006\u0010A\u001a\u00020@H\u0016J\u0014\u0010H\u001a\u00020\u00002\u000c\u0010I\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011R\u001a\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0006\u001a\u00020\u00158F\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0017R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001d\u0010\u001eR\u0014\u0010\u001f\u001a\u00020 8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008!\u0010\"R\u000e\u0010#\u001a\u00020$X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u00020\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008%\u0010&R\u0013\u0010\'\u001a\u0004\u0018\u00010(8F\u00a2\u0006\u0006\u001a\u0004\u0008)\u0010*R\u0011\u0010+\u001a\u00020\t8F\u00a2\u0006\u0006\u001a\u0004\u0008+\u0010&R\u0014\u0010,\u001a\u00020$8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008-\u0010.R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010/\u001a\u00020$8F\u00a2\u0006\u0006\u001a\u0004\u00080\u0010.R\u0014\u00101\u001a\u00020\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u00082\u0010&\u00a8\u0006K"
    }
    d2 = {
        "Lcom/squareup/checkout/OrderModifier;",
        "Lcom/squareup/calc/order/Modifier;",
        "price",
        "Lcom/squareup/protos/common/Money;",
        "displayDetails",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;",
        "backingDetails",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;",
        "hideFromCustomer",
        "",
        "featureDetails",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;",
        "(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;ZLcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;)V",
        "lineItem",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem;",
        "(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)V",
        "appliedConversationalModes",
        "",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;",
        "getAppliedConversationalModes",
        "()Ljava/util/List;",
        "Lcom/squareup/api/items/ItemModifierOption;",
        "getBackingDetails",
        "()Lcom/squareup/api/items/ItemModifierOption;",
        "backingModifierOption",
        "basePrice",
        "displayNameModel",
        "Lcom/squareup/resources/TextModel;",
        "",
        "getDisplayNameModel",
        "()Lcom/squareup/resources/TextModel;",
        "displayQuantity",
        "",
        "getDisplayQuantity",
        "()I",
        "fallbackSharedCartCalculatorId",
        "",
        "getHideFromCustomer",
        "()Z",
        "idPair",
        "Lcom/squareup/protos/client/IdPair;",
        "getIdPair",
        "()Lcom/squareup/protos/client/IdPair;",
        "isFreeModifier",
        "itemModifierName",
        "getItemModifierName",
        "()Ljava/lang/String;",
        "modifierId",
        "getModifierId",
        "onByDefault",
        "getOnByDefault",
        "equals",
        "other",
        "",
        "getBasePriceTimesModifierQuantityOrNull",
        "getDisplayName",
        "res",
        "Lcom/squareup/util/Res;",
        "hasBackingDetails",
        "hashCode",
        "id",
        "",
        "quantity",
        "quantityTimesItemQuantity",
        "Ljava/math/BigDecimal;",
        "itemQuantity",
        "toModifierOptionLineItem",
        "itemizationQuantity",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "toString",
        "totalPrice",
        "withConversationalModes",
        "conversationalModes",
        "Companion",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CONVERSATIONAL_MODE_TO_RES_ID:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final CONVERSATIONAL_MODIFIER_FORMATS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/checkout/OrderModifier$Companion;


# instance fields
.field private final appliedConversationalModes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;",
            ">;"
        }
    .end annotation
.end field

.field private final backingModifierOption:Lcom/squareup/api/items/ItemModifierOption;

.field private final basePrice:Lcom/squareup/protos/common/Money;

.field private final displayDetails:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

.field private final fallbackSharedCartCalculatorId:Ljava/lang/String;

.field private final featureDetails:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

.field private final lineItem:Lcom/squareup/protos/client/bills/ModifierOptionLineItem;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    new-instance v0, Lcom/squareup/checkout/OrderModifier$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkout/OrderModifier$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkout/OrderModifier;->Companion:Lcom/squareup/checkout/OrderModifier$Companion;

    const/4 v0, 0x7

    new-array v0, v0, [Lkotlin/Pair;

    .line 316
    sget-object v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;->ADD:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    sget v2, Lcom/squareup/checkout/R$string;->conversational_mode_add:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 317
    sget-object v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;->EXTRA:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    sget v3, Lcom/squareup/checkout/R$string;->conversational_mode_extra:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    .line 318
    sget-object v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;->SIDE:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    sget v4, Lcom/squareup/checkout/R$string;->conversational_mode_side:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v4, 0x2

    aput-object v1, v0, v4

    .line 319
    sget-object v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;->SUB:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    sget v5, Lcom/squareup/checkout/R$string;->conversational_mode_sub:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v1, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v5, 0x3

    aput-object v1, v0, v5

    .line 320
    sget-object v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;->NO:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    sget v6, Lcom/squareup/checkout/R$string;->conversational_mode_no:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v1, v6}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v6, 0x4

    aput-object v1, v0, v6

    .line 321
    sget-object v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;->ALLERGY:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    sget v7, Lcom/squareup/checkout/R$string;->conversational_mode_allergy:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v1, v7}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v7, 0x5

    aput-object v1, v0, v7

    .line 322
    sget-object v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;->UNKNOWN:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    sget v7, Lcom/squareup/checkout/R$string;->conversational_mode_unknown:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v1, v7}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v7, 0x6

    aput-object v1, v0, v7

    .line 315
    invoke-static {v0}, Lkotlin/collections/MapsKt;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/squareup/checkout/OrderModifier;->CONVERSATIONAL_MODE_TO_RES_ID:Ljava/util/Map;

    new-array v0, v6, [Ljava/lang/Integer;

    .line 326
    sget v1, Lcom/squareup/checkout/R$string;->modifier_no_modes:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    .line 327
    sget v1, Lcom/squareup/checkout/R$string;->modifier_one_mode:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 328
    sget v1, Lcom/squareup/checkout/R$string;->modifier_two_modes:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    .line 329
    sget v1, Lcom/squareup/checkout/R$string;->modifier_three_modes:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    .line 325
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/checkout/OrderModifier;->CONVERSATIONAL_MODIFIER_FORMATS:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)V
    .locals 1

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkout/OrderModifier;->lineItem:Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    .line 106
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "UUID.randomUUID().toString()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkout/OrderModifier;->fallbackSharedCartCalculatorId:Ljava/lang/String;

    .line 109
    iget-object p1, p0, Lcom/squareup/checkout/OrderModifier;->lineItem:Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    iput-object p1, p0, Lcom/squareup/checkout/OrderModifier;->backingModifierOption:Lcom/squareup/api/items/ItemModifierOption;

    .line 111
    iget-object p1, p0, Lcom/squareup/checkout/OrderModifier;->lineItem:Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    iput-object p1, p0, Lcom/squareup/checkout/OrderModifier;->displayDetails:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    .line 113
    iget-object p1, p0, Lcom/squareup/checkout/OrderModifier;->lineItem:Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    if-eqz p1, :cond_1

    iget-object p1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;->modifier_option_money:Lcom/squareup/protos/common/Money;

    if-eqz p1, :cond_1

    goto :goto_1

    .line 114
    :cond_1
    iget-object p1, p0, Lcom/squareup/checkout/OrderModifier;->backingModifierOption:Lcom/squareup/api/items/ItemModifierOption;

    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/squareup/api/items/ItemModifierOption;->price:Lcom/squareup/protos/common/dinero/Money;

    :cond_2
    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    :goto_1
    iput-object p1, p0, Lcom/squareup/checkout/OrderModifier;->basePrice:Lcom/squareup/protos/common/Money;

    .line 116
    iget-object p1, p0, Lcom/squareup/checkout/OrderModifier;->lineItem:Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    iput-object p1, p0, Lcom/squareup/checkout/OrderModifier;->featureDetails:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    .line 130
    iget-object p1, p0, Lcom/squareup/checkout/OrderModifier;->featureDetails:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    if-eqz p1, :cond_3

    iget-object p1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->conversational_mode:Ljava/util/List;

    if-eqz p1, :cond_3

    goto :goto_2

    :cond_3
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_2
    iput-object p1, p0, Lcom/squareup/checkout/OrderModifier;->appliedConversationalModes:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/checkout/OrderModifier;-><init>(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)V

    return-void
.end method

.method private constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;ZLcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;)V
    .locals 4

    .line 54
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;-><init>()V

    .line 55
    new-instance v1, Lcom/squareup/protos/client/IdPair;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->modifier_option_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 56
    sget-object v1, Lcom/squareup/checkout/OrderModifier;->Companion:Lcom/squareup/checkout/OrderModifier$Companion;

    invoke-static {v1, p1}, Lcom/squareup/checkout/OrderModifier$Companion;->access$amounts(Lcom/squareup/checkout/OrderModifier$Companion;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    move-result-object v3

    :cond_0
    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object p1

    .line 57
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->read_only_display_details(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object p1

    .line 58
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->write_only_backing_details(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object p1

    .line 59
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->hide_from_customer(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object p1

    .line 60
    invoke-virtual {p1, p5}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->feature_details(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    move-result-object p1

    const-string p2, "ModifierOptionLineItem.B\u2026s)\n              .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/checkout/OrderModifier;-><init>(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;ZLcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 44
    invoke-direct/range {p0 .. p5}, Lcom/squareup/checkout/OrderModifier;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;ZLcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;)V

    return-void
.end method

.method public static final fromItemizationHistory(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)Lcom/squareup/checkout/OrderModifier;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/checkout/OrderModifier;->Companion:Lcom/squareup/checkout/OrderModifier$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/checkout/OrderModifier$Companion;->fromItemizationHistory(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)Lcom/squareup/checkout/OrderModifier;

    move-result-object p0

    return-object p0
.end method

.method public static final fromOverride(Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;Lcom/squareup/api/items/ItemModifierOptionOverride;ZZ)Lcom/squareup/checkout/OrderModifier;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/checkout/OrderModifier;->Companion:Lcom/squareup/checkout/OrderModifier$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/squareup/checkout/OrderModifier$Companion;->fromOverride(Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;Lcom/squareup/api/items/ItemModifierOptionOverride;ZZ)Lcom/squareup/checkout/OrderModifier;

    move-result-object p0

    return-object p0
.end method

.method public static final fromTicketCart(Lcom/squareup/api/items/ItemModifierOption;Lcom/squareup/api/items/ItemModifierList;Ljava/math/BigDecimal;)Lcom/squareup/checkout/OrderModifier;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/checkout/OrderModifier;->Companion:Lcom/squareup/checkout/OrderModifier$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/squareup/checkout/OrderModifier$Companion;->fromTicketCart(Lcom/squareup/api/items/ItemModifierOption;Lcom/squareup/api/items/ItemModifierList;Ljava/math/BigDecimal;)Lcom/squareup/checkout/OrderModifier;

    move-result-object p0

    return-object p0
.end method

.method public static final fromTicketCart(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)Lcom/squareup/checkout/OrderModifier;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/checkout/OrderModifier;->Companion:Lcom/squareup/checkout/OrderModifier$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/checkout/OrderModifier$Companion;->fromTicketCart(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)Lcom/squareup/checkout/OrderModifier;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 293
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/OrderModifier;

    const/4 v1, 0x1

    if-ne v0, p1, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    .line 294
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/2addr v2, v1

    if-eqz v2, :cond_1

    goto :goto_0

    .line 296
    :cond_1
    instance-of v2, p1, Lcom/squareup/checkout/OrderModifier;

    if-eqz v2, :cond_2

    .line 300
    iget-object v2, p0, Lcom/squareup/checkout/OrderModifier;->basePrice:Lcom/squareup/protos/common/Money;

    check-cast p1, Lcom/squareup/checkout/OrderModifier;

    iget-object v3, p1, Lcom/squareup/checkout/OrderModifier;->basePrice:Lcom/squareup/protos/common/Money;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/checkout/OrderModifier;->displayDetails:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    iget-object v3, p1, Lcom/squareup/checkout/OrderModifier;->displayDetails:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/checkout/OrderModifier;->backingModifierOption:Lcom/squareup/api/items/ItemModifierOption;

    iget-object v3, p1, Lcom/squareup/checkout/OrderModifier;->backingModifierOption:Lcom/squareup/api/items/ItemModifierOption;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->getAppliedConversationalModes()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderModifier;->getAppliedConversationalModes()Ljava/util/List;

    move-result-object p1

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :cond_2
    :goto_0
    return v0
.end method

.method public getAppliedConversationalModes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;",
            ">;"
        }
    .end annotation

    .line 129
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifier;->appliedConversationalModes:Ljava/util/List;

    return-object v0
.end method

.method public final getBackingDetails()Lcom/squareup/api/items/ItemModifierOption;
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifier;->backingModifierOption:Lcom/squareup/api/items/ItemModifierOption;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tried to access backing details when they didn\'t exist. This OrderModifier was probably meant to be used in a read-only flow."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public getBasePriceTimesModifierQuantityOrNull()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 234
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifier;->basePrice:Lcom/squareup/protos/common/Money;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->quantity()I

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->multiply(Lcom/squareup/protos/common/Money;I)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getDisplayName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 7

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->getAppliedConversationalModes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x4

    if-ge v0, v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 189
    iget-object v5, p0, Lcom/squareup/checkout/OrderModifier;->backingModifierOption:Lcom/squareup/api/items/ItemModifierOption;

    aput-object v5, v4, v2

    .line 190
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->getAppliedConversationalModes()Ljava/util/List;

    move-result-object v5

    aput-object v5, v4, v1

    const-string v1, "Can\'t handle more than 3 modes on %s, found %s "

    .line 188
    invoke-static {v3, v1, v4}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 192
    sget-object v1, Lcom/squareup/checkout/OrderModifier;->CONVERSATIONAL_MODIFIER_FORMATS:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 194
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->getAppliedConversationalModes()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 415
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    if-gez v2, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_1
    check-cast v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;

    .line 196
    sget-object v5, Lcom/squareup/checkout/OrderModifier;->CONVERSATIONAL_MODE_TO_RES_ID:Ljava/util/Map;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->type:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 195
    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    .line 197
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mode_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x61

    int-to-char v2, v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move v2, v4

    goto :goto_1

    .line 195
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 201
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->getItemModifierName()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 202
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 203
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getDisplayNameModel()Lcom/squareup/resources/TextModel;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 208
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->getAppliedConversationalModes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x4

    if-ge v0, v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 211
    iget-object v5, p0, Lcom/squareup/checkout/OrderModifier;->backingModifierOption:Lcom/squareup/api/items/ItemModifierOption;

    aput-object v5, v4, v2

    .line 212
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->getAppliedConversationalModes()Ljava/util/List;

    move-result-object v5

    aput-object v5, v4, v1

    const-string v1, "Can\'t handle more than 3 modes on %s, found %s "

    .line 209
    invoke-static {v3, v1, v4}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 215
    new-instance v1, Lcom/squareup/resources/PhraseModel;

    sget-object v3, Lcom/squareup/checkout/OrderModifier;->CONVERSATIONAL_MODIFIER_FORMATS:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-direct {v1, v0}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    .line 217
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->getAppliedConversationalModes()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 418
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    if-gez v2, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_1
    check-cast v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;

    .line 219
    sget-object v5, Lcom/squareup/checkout/OrderModifier;->CONVERSATIONAL_MODE_TO_RES_ID:Ljava/util/Map;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->type:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 218
    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    .line 221
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mode_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x61

    int-to-char v2, v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;I)Lcom/squareup/resources/PhraseModel;

    move v2, v4

    goto :goto_1

    .line 218
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 225
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->getItemModifierName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "name"

    invoke-virtual {v1, v2, v0}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object v0

    check-cast v0, Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public getDisplayQuantity()I
    .locals 3

    .line 179
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->getAppliedConversationalModes()Ljava/util/List;

    move-result-object v0

    .line 180
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->getOnByDefault()Z

    move-result v1

    const/4 v2, 0x0

    .line 179
    invoke-static {v0, v1, v2}, Lcom/squareup/checkout/util/ConversationalModesKt;->modifierQuantityForModes(Ljava/util/List;ZZ)I

    move-result v0

    return v0
.end method

.method public getHideFromCustomer()Z
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifier;->lineItem:Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->hide_from_customer:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifier;->lineItem:Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public getItemModifierName()Ljava/lang/String;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifier;->backingModifierOption:Lcom/squareup/api/items/ItemModifierOption;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierOption;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifier;->displayDetails:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->name:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    const-string v0, ""

    :goto_1
    return-object v0
.end method

.method public final getModifierId()Ljava/lang/String;
    .locals 2

    .line 74
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->getBackingDetails()Lcom/squareup/api/items/ItemModifierOption;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierOption;->id:Ljava/lang/String;

    const-string v1, "backingDetails.id"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getOnByDefault()Z
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifier;->backingModifierOption:Lcom/squareup/api/items/ItemModifierOption;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierOption;->on_by_default:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/api/items/ItemModifierOption;->DEFAULT_ON_BY_DEFAULT:Ljava/lang/Boolean;

    const-string v1, "DEFAULT_ON_BY_DEFAULT"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final hasBackingDetails()Z
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifier;->backingModifierOption:Lcom/squareup/api/items/ItemModifierOption;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 307
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifier;->lineItem:Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->hashCode()I

    move-result v0

    return v0
.end method

.method public id()Ljava/lang/String;
    .locals 1

    .line 138
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->getIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifier;->fallbackSharedCartCalculatorId:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public final isFreeModifier()Z
    .locals 7

    .line 98
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifier;->backingModifierOption:Lcom/squareup/api/items/ItemModifierOption;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    if-eqz v0, :cond_1

    iget-object v5, v0, Lcom/squareup/api/items/ItemModifierOption;->price:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v5, :cond_3

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierOption;->price:Lcom/squareup/protos/common/dinero/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/dinero/Money;->cents:Ljava/lang/Long;

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v0, v5, v2

    if-nez v0, :cond_4

    goto :goto_0

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifier;->basePrice:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v0, v5, v2

    if-nez v0, :cond_4

    :cond_3
    :goto_0
    const/4 v1, 0x1

    :cond_4
    :goto_1
    return v1
.end method

.method public price()J
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifier;->basePrice:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public quantity()I
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifier;->featureDetails:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->modifier_quantity:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public quantityTimesItemQuantity(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;
    .locals 2

    const-string v0, "itemQuantity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    new-instance v0, Ljava/math/BigDecimal;

    .line 161
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->quantity()I

    move-result v1

    invoke-static {v1, p1}, Lcom/squareup/quantity/SharedCalculationsKt;->modifierQuantityTimesItemizationQuantity(ILjava/math/BigDecimal;)Ljava/lang/String;

    move-result-object p1

    .line 160
    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final toModifierOptionLineItem(Ljava/math/BigDecimal;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem;
    .locals 4

    const-string v0, "itemizationQuantity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->hasBackingDetails()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 242
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifier;->featureDetails:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;-><init>()V

    .line 243
    :goto_0
    iget-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;

    move-result-object v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    new-instance v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;-><init>()V

    .line 246
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->quantity()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->modifier_quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;

    move-result-object v1

    .line 249
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->quantity()I

    move-result v2

    .line 248
    invoke-static {v2, p1}, Lcom/squareup/quantity/SharedCalculationsKt;->modifierQuantityTimesItemizationQuantity(ILjava/math/BigDecimal;)Ljava/lang/String;

    move-result-object v2

    .line 247
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->modifier_quantity_times_itemization_quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;

    move-result-object v1

    .line 252
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    move-result-object v1

    .line 244
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->quantities(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;

    .line 255
    iget-object v1, p0, Lcom/squareup/checkout/OrderModifier;->lineItem:Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;

    move-result-object v1

    if-eqz v1, :cond_2

    goto :goto_2

    :cond_2
    new-instance v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;-><init>()V

    .line 256
    :goto_2
    iget-object v2, p0, Lcom/squareup/checkout/OrderModifier;->basePrice:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_3
    const-wide/16 v2, 0x0

    invoke-static {v2, v3, p2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 259
    :goto_3
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->quantity()I

    move-result p2

    .line 258
    invoke-static {v2, p2, p1}, Lcom/squareup/quantity/SharedCalculationsKt;->modifierOptionTimesQuantityMoney(Lcom/squareup/protos/common/Money;ILjava/math/BigDecimal;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 262
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;->modifier_option_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;

    move-result-object p2

    .line 263
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;->modifier_option_times_quantity_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;

    .line 265
    iget-object p1, p0, Lcom/squareup/checkout/OrderModifier;->lineItem:Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object p1

    .line 266
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object p1

    .line 267
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->feature_details(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object p1

    .line 268
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    move-result-object p1

    const-string p2, "lineItem.newBuilder()\n  \u2026build())\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 240
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Shouldn\'t try and convert a read only OrderModifier to a proto"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 311
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OrderModifier(lineItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/OrderModifier;->lineItem:Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public totalPrice(Ljava/math/BigDecimal;)J
    .locals 3

    const-string v0, "itemQuantity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->price()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->quantity()I

    move-result v2

    invoke-static {v0, v1, v2, p1}, Lcom/squareup/quantity/SharedCalculationsKt;->modifierOptionTimesQuantityMoneyAsLong(JILjava/math/BigDecimal;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final withConversationalModes(Ljava/util/List;)Lcom/squareup/checkout/OrderModifier;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;",
            ">;)",
            "Lcom/squareup/checkout/OrderModifier;"
        }
    .end annotation

    const-string v0, "conversationalModes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 275
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderModifier;->getOnByDefault()Z

    move-result v0

    const/4 v1, 0x1

    .line 274
    invoke-static {p1, v0, v1}, Lcom/squareup/checkout/util/ConversationalModesKt;->modifierQuantityForModes(Ljava/util/List;ZZ)I

    move-result v0

    .line 277
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 278
    new-instance v1, Lcom/squareup/checkout/OrderModifier;

    .line 279
    iget-object v2, p0, Lcom/squareup/checkout/OrderModifier;->lineItem:Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object v2

    .line 280
    iget-object v3, p0, Lcom/squareup/checkout/OrderModifier;->featureDetails:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;

    move-result-object v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    new-instance v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;-><init>()V

    .line 281
    :goto_0
    invoke-virtual {v3, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->conversational_mode(Ljava/util/List;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;

    move-result-object p1

    .line 283
    new-instance v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;-><init>()V

    .line 284
    invoke-virtual {v3, v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->modifier_quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;

    move-result-object v0

    .line 285
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    move-result-object v0

    .line 282
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->quantities(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;

    move-result-object p1

    .line 287
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    move-result-object p1

    .line 279
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->feature_details(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object p1

    .line 288
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    move-result-object p1

    const-string v0, "lineItem.newBuilder().fe\u2026build()\n        ).build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 278
    invoke-direct {v1, p1}, Lcom/squareup/checkout/OrderModifier;-><init>(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)V

    return-object v1
.end method
