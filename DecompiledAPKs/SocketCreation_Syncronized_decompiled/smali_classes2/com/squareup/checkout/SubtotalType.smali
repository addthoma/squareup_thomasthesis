.class public final enum Lcom/squareup/checkout/SubtotalType;
.super Ljava/lang/Enum;
.source "SubtotalType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/checkout/SubtotalType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/checkout/SubtotalType;

.field public static final enum COUPON:Lcom/squareup/checkout/SubtotalType;

.field public static final enum DISCOUNT:Lcom/squareup/checkout/SubtotalType;

.field public static final enum INCLUSIVE_TAX:Lcom/squareup/checkout/SubtotalType;

.field public static final enum REWARD:Lcom/squareup/checkout/SubtotalType;

.field public static final enum SWEDISH_ROUNDING:Lcom/squareup/checkout/SubtotalType;

.field public static final enum TAX:Lcom/squareup/checkout/SubtotalType;

.field public static final enum UNKNOWN:Lcom/squareup/checkout/SubtotalType;

.field private static final mapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/SubtotalType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 9
    new-instance v0, Lcom/squareup/checkout/SubtotalType;

    const/4 v1, 0x0

    const-string v2, "DISCOUNT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/checkout/SubtotalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/checkout/SubtotalType;->DISCOUNT:Lcom/squareup/checkout/SubtotalType;

    new-instance v0, Lcom/squareup/checkout/SubtotalType;

    const/4 v2, 0x1

    const-string v3, "INCLUSIVE_TAX"

    invoke-direct {v0, v3, v2}, Lcom/squareup/checkout/SubtotalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/checkout/SubtotalType;->INCLUSIVE_TAX:Lcom/squareup/checkout/SubtotalType;

    new-instance v0, Lcom/squareup/checkout/SubtotalType;

    const/4 v3, 0x2

    const-string v4, "TAX"

    invoke-direct {v0, v4, v3}, Lcom/squareup/checkout/SubtotalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/checkout/SubtotalType;->TAX:Lcom/squareup/checkout/SubtotalType;

    new-instance v0, Lcom/squareup/checkout/SubtotalType;

    const/4 v4, 0x3

    const-string v5, "COUPON"

    invoke-direct {v0, v5, v4}, Lcom/squareup/checkout/SubtotalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/checkout/SubtotalType;->COUPON:Lcom/squareup/checkout/SubtotalType;

    new-instance v0, Lcom/squareup/checkout/SubtotalType;

    const/4 v5, 0x4

    const-string v6, "REWARD"

    invoke-direct {v0, v6, v5}, Lcom/squareup/checkout/SubtotalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/checkout/SubtotalType;->REWARD:Lcom/squareup/checkout/SubtotalType;

    new-instance v0, Lcom/squareup/checkout/SubtotalType;

    const/4 v6, 0x5

    const-string v7, "SWEDISH_ROUNDING"

    invoke-direct {v0, v7, v6}, Lcom/squareup/checkout/SubtotalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/checkout/SubtotalType;->SWEDISH_ROUNDING:Lcom/squareup/checkout/SubtotalType;

    new-instance v0, Lcom/squareup/checkout/SubtotalType;

    const/4 v7, 0x6

    const-string v8, "UNKNOWN"

    invoke-direct {v0, v8, v7}, Lcom/squareup/checkout/SubtotalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/checkout/SubtotalType;->UNKNOWN:Lcom/squareup/checkout/SubtotalType;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/checkout/SubtotalType;

    .line 8
    sget-object v8, Lcom/squareup/checkout/SubtotalType;->DISCOUNT:Lcom/squareup/checkout/SubtotalType;

    aput-object v8, v0, v1

    sget-object v8, Lcom/squareup/checkout/SubtotalType;->INCLUSIVE_TAX:Lcom/squareup/checkout/SubtotalType;

    aput-object v8, v0, v2

    sget-object v2, Lcom/squareup/checkout/SubtotalType;->TAX:Lcom/squareup/checkout/SubtotalType;

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/checkout/SubtotalType;->COUPON:Lcom/squareup/checkout/SubtotalType;

    aput-object v2, v0, v4

    sget-object v2, Lcom/squareup/checkout/SubtotalType;->REWARD:Lcom/squareup/checkout/SubtotalType;

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/checkout/SubtotalType;->SWEDISH_ROUNDING:Lcom/squareup/checkout/SubtotalType;

    aput-object v2, v0, v6

    sget-object v2, Lcom/squareup/checkout/SubtotalType;->UNKNOWN:Lcom/squareup/checkout/SubtotalType;

    aput-object v2, v0, v7

    sput-object v0, Lcom/squareup/checkout/SubtotalType;->$VALUES:[Lcom/squareup/checkout/SubtotalType;

    .line 11
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/squareup/checkout/SubtotalType;->mapping:Ljava/util/Map;

    .line 14
    invoke-static {}, Lcom/squareup/checkout/SubtotalType;->values()[Lcom/squareup/checkout/SubtotalType;

    move-result-object v0

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 15
    sget-object v4, Lcom/squareup/checkout/SubtotalType;->mapping:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/squareup/checkout/SubtotalType;->name()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/squareup/checkout/SubtotalType;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/checkout/SubtotalType;->mapping:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/checkout/SubtotalType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/checkout/SubtotalType;
    .locals 1

    .line 8
    const-class v0, Lcom/squareup/checkout/SubtotalType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/checkout/SubtotalType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/checkout/SubtotalType;
    .locals 1

    .line 8
    sget-object v0, Lcom/squareup/checkout/SubtotalType;->$VALUES:[Lcom/squareup/checkout/SubtotalType;

    invoke-virtual {v0}, [Lcom/squareup/checkout/SubtotalType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/checkout/SubtotalType;

    return-object v0
.end method
