.class public final Lcom/squareup/checkout/Discount;
.super Lcom/squareup/checkout/Adjustment;
.source "Discount.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkout/Discount$Scope;,
        Lcom/squareup/checkout/Discount$Matches;,
        Lcom/squareup/checkout/Discount$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDiscount.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Discount.kt\ncom/squareup/checkout/Discount\n*L\n1#1,558:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0017\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u0003>?@BW\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0008\u0010\r\u001a\u0004\u0018\u00010\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0002\u0010\u0013J\u0008\u0010\'\u001a\u00020\u0010H\u0016J\u0006\u0010(\u001a\u00020\u0012J \u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,2\u000e\u0010-\u001a\n\u0012\u0004\u0012\u00020/\u0018\u00010.H\u0016J$\u00100\u001a\u0002012\u0008\u00102\u001a\u0004\u0018\u00010,2\u0006\u00103\u001a\u00020\u00102\u0008\u0008\u0002\u00104\u001a\u00020\u0010H\u0007J\n\u00105\u001a\u0004\u0018\u000106H\u0002J\u0006\u00107\u001a\u00020\u0010J\u0006\u00108\u001a\u00020\u0010J\u000e\u00109\u001a\u00020\u00102\u0006\u0010:\u001a\u00020;J\u0006\u0010<\u001a\u00020\u0010J\u0008\u0010=\u001a\u00020\u0005H\u0016R\u0012\u0010\r\u001a\u0004\u0018\u00010\u000e8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0014\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0016R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0016R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u001b\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0011\u0010\u001d\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001cR\u0011\u0010\u001e\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001cR\u0011\u0010\u001f\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u001cR\u0011\u0010 \u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u001cR\u0011\u0010!\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\u001cR\u0011\u0010\"\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010\u001cR\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010$R\u0010\u0010\u000f\u001a\u00020\u00108\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010&\u00a8\u0006A"
    }
    d2 = {
        "Lcom/squareup/checkout/Discount;",
        "Lcom/squareup/checkout/Adjustment;",
        "builder",
        "Lcom/squareup/checkout/Discount$Builder;",
        "couponToken",
        "",
        "couponDefinitionToken",
        "couponReason",
        "Lcom/squareup/protos/client/coupons/Coupon$Reason;",
        "scope",
        "Lcom/squareup/checkout/Discount$Scope;",
        "matches",
        "Lcom/squareup/checkout/Discount$Matches;",
        "applicationMethod",
        "Lcom/squareup/api/items/Discount$ApplicationMethod;",
        "recalledFromTicket",
        "",
        "discountProto",
        "Lcom/squareup/api/items/Discount;",
        "(Lcom/squareup/checkout/Discount$Builder;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/coupons/Coupon$Reason;Lcom/squareup/checkout/Discount$Scope;Lcom/squareup/checkout/Discount$Matches;Lcom/squareup/api/items/Discount$ApplicationMethod;ZLcom/squareup/api/items/Discount;)V",
        "catalogId",
        "getCatalogId",
        "()Ljava/lang/String;",
        "getCouponDefinitionToken",
        "getCouponReason",
        "()Lcom/squareup/protos/client/coupons/Coupon$Reason;",
        "getCouponToken",
        "isAmountDiscount",
        "()Z",
        "isCartScopeAmountDiscount",
        "isCartScopeDiscount",
        "isComp",
        "isCoupon",
        "isFixedPercentage",
        "isVariable",
        "getMatches",
        "()Lcom/squareup/checkout/Discount$Matches;",
        "getScope",
        "()Lcom/squareup/checkout/Discount$Scope;",
        "amountAppliesPerItemQuantity",
        "asProto",
        "asRequestAdjustment",
        "Lcom/squareup/server/payment/value/Adjustment;",
        "collected",
        "Lcom/squareup/protos/common/Money;",
        "childAdjustments",
        "",
        "Lcom/squareup/server/payment/value/ItemizedAdjustment;",
        "buildDiscountLineItem",
        "Lcom/squareup/protos/client/bills/DiscountLineItem;",
        "appliedMoney",
        "discountApplicationIdEnabled",
        "appliedFromPricingEngine",
        "buildDiscountLineItemConfiguration",
        "Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;",
        "canBeAppliedPerItem",
        "canOnlyBeAppliedToOneItem",
        "isItemEligible",
        "cartItem",
        "Lcom/squareup/checkout/CartItem;",
        "passcodeRequired",
        "toString",
        "Builder",
        "Matches",
        "Scope",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final applicationMethod:Lcom/squareup/api/items/Discount$ApplicationMethod;

.field private final catalogId:Ljava/lang/String;

.field private final couponDefinitionToken:Ljava/lang/String;

.field private final couponReason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

.field private final couponToken:Ljava/lang/String;

.field private final discountProto:Lcom/squareup/api/items/Discount;

.field private final isAmountDiscount:Z

.field private final isCartScopeAmountDiscount:Z

.field private final isCartScopeDiscount:Z

.field private final isComp:Z

.field private final isCoupon:Z

.field private final isFixedPercentage:Z

.field private final isVariable:Z

.field private final matches:Lcom/squareup/checkout/Discount$Matches;

.field public final recalledFromTicket:Z

.field private final scope:Lcom/squareup/checkout/Discount$Scope;


# direct methods
.method private constructor <init>(Lcom/squareup/checkout/Discount$Builder;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/coupons/Coupon$Reason;Lcom/squareup/checkout/Discount$Scope;Lcom/squareup/checkout/Discount$Matches;Lcom/squareup/api/items/Discount$ApplicationMethod;ZLcom/squareup/api/items/Discount;)V
    .locals 0

    .line 80
    check-cast p1, Lcom/squareup/checkout/Adjustment$Builder;

    invoke-direct {p0, p1}, Lcom/squareup/checkout/Adjustment;-><init>(Lcom/squareup/checkout/Adjustment$Builder;)V

    iput-object p2, p0, Lcom/squareup/checkout/Discount;->couponToken:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/checkout/Discount;->couponDefinitionToken:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/checkout/Discount;->couponReason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    iput-object p5, p0, Lcom/squareup/checkout/Discount;->scope:Lcom/squareup/checkout/Discount$Scope;

    iput-object p6, p0, Lcom/squareup/checkout/Discount;->matches:Lcom/squareup/checkout/Discount$Matches;

    iput-object p7, p0, Lcom/squareup/checkout/Discount;->applicationMethod:Lcom/squareup/api/items/Discount$ApplicationMethod;

    iput-boolean p8, p0, Lcom/squareup/checkout/Discount;->recalledFromTicket:Z

    iput-object p9, p0, Lcom/squareup/checkout/Discount;->discountProto:Lcom/squareup/api/items/Discount;

    .line 87
    iget-object p1, p0, Lcom/squareup/checkout/Discount;->discountProto:Lcom/squareup/api/items/Discount;

    iget-object p1, p1, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    const-string p2, "discountProto.id"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkout/Discount;->catalogId:Ljava/lang/String;

    .line 90
    iget-object p1, p0, Lcom/squareup/checkout/Discount;->couponToken:Ljava/lang/String;

    const/4 p2, 0x1

    const/4 p3, 0x0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/checkout/Discount;->isCoupon:Z

    .line 96
    iget-object p1, p0, Lcom/squareup/checkout/Discount;->percentage:Lcom/squareup/util/Percentage;

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    iput-boolean p1, p0, Lcom/squareup/checkout/Discount;->isAmountDiscount:Z

    .line 98
    iget-object p1, p0, Lcom/squareup/checkout/Discount;->scope:Lcom/squareup/checkout/Discount$Scope;

    sget-object p4, Lcom/squareup/checkout/Discount$Scope;->CART:Lcom/squareup/checkout/Discount$Scope;

    if-ne p1, p4, :cond_2

    const/4 p1, 0x1

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    :goto_2
    iput-boolean p1, p0, Lcom/squareup/checkout/Discount;->isCartScopeDiscount:Z

    .line 100
    iget-boolean p1, p0, Lcom/squareup/checkout/Discount;->isAmountDiscount:Z

    if-eqz p1, :cond_3

    iget-boolean p1, p0, Lcom/squareup/checkout/Discount;->isCartScopeDiscount:Z

    if-eqz p1, :cond_3

    const/4 p1, 0x1

    goto :goto_3

    :cond_3
    const/4 p1, 0x0

    :goto_3
    iput-boolean p1, p0, Lcom/squareup/checkout/Discount;->isCartScopeAmountDiscount:Z

    .line 108
    iget-object p1, p0, Lcom/squareup/checkout/Discount;->discountProto:Lcom/squareup/api/items/Discount;

    iget-object p1, p1, Lcom/squareup/api/items/Discount;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    sget-object p4, Lcom/squareup/api/items/Discount$DiscountType;->VARIABLE_PERCENTAGE:Lcom/squareup/api/items/Discount$DiscountType;

    if-eq p1, p4, :cond_5

    iget-object p1, p0, Lcom/squareup/checkout/Discount;->discountProto:Lcom/squareup/api/items/Discount;

    iget-object p1, p1, Lcom/squareup/api/items/Discount;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    sget-object p4, Lcom/squareup/api/items/Discount$DiscountType;->VARIABLE_AMOUNT:Lcom/squareup/api/items/Discount$DiscountType;

    if-ne p1, p4, :cond_4

    goto :goto_4

    :cond_4
    const/4 p1, 0x0

    goto :goto_5

    :cond_5
    :goto_4
    const/4 p1, 0x1

    :goto_5
    iput-boolean p1, p0, Lcom/squareup/checkout/Discount;->isVariable:Z

    .line 111
    iget-object p1, p0, Lcom/squareup/checkout/Discount;->discountProto:Lcom/squareup/api/items/Discount;

    invoke-static {p1}, Lcom/squareup/checkout/Discounts;->isFixedPercentage(Lcom/squareup/api/items/Discount;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/checkout/Discount;->isFixedPercentage:Z

    .line 113
    iget-object p1, p0, Lcom/squareup/checkout/Discount;->applicationMethod:Lcom/squareup/api/items/Discount$ApplicationMethod;

    sget-object p4, Lcom/squareup/api/items/Discount$ApplicationMethod;->COMP:Lcom/squareup/api/items/Discount$ApplicationMethod;

    if-ne p1, p4, :cond_6

    goto :goto_6

    :cond_6
    const/4 p2, 0x0

    :goto_6
    iput-boolean p2, p0, Lcom/squareup/checkout/Discount;->isComp:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/checkout/Discount$Builder;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/coupons/Coupon$Reason;Lcom/squareup/checkout/Discount$Scope;Lcom/squareup/checkout/Discount$Matches;Lcom/squareup/api/items/Discount$ApplicationMethod;ZLcom/squareup/api/items/Discount;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 49
    invoke-direct/range {p0 .. p9}, Lcom/squareup/checkout/Discount;-><init>(Lcom/squareup/checkout/Discount$Builder;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/coupons/Coupon$Reason;Lcom/squareup/checkout/Discount$Scope;Lcom/squareup/checkout/Discount$Matches;Lcom/squareup/api/items/Discount$ApplicationMethod;ZLcom/squareup/api/items/Discount;)V

    return-void
.end method

.method public static final synthetic access$getDiscountProto$p(Lcom/squareup/checkout/Discount;)Lcom/squareup/api/items/Discount;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/checkout/Discount;->discountProto:Lcom/squareup/api/items/Discount;

    return-object p0
.end method

.method public static synthetic buildDiscountLineItem$default(Lcom/squareup/checkout/Discount;Lcom/squareup/protos/common/Money;ZZILjava/lang/Object;)Lcom/squareup/protos/client/bills/DiscountLineItem;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 242
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkout/Discount;->buildDiscountLineItem(Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object p0

    return-object p0
.end method

.method private final buildDiscountLineItemConfiguration()Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;
    .locals 2

    .line 280
    iget-object v0, p0, Lcom/squareup/checkout/Discount;->discountProto:Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/checkout/Discount$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/api/items/Discount$DiscountType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    :goto_0
    const/4 v0, 0x0

    goto :goto_1

    .line 284
    :cond_1
    new-instance v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;-><init>()V

    .line 285
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->percentage:Lcom/squareup/util/Percentage;

    invoke-virtual {v1}, Lcom/squareup/util/Percentage;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->variable_percentage(Ljava/lang/String;)Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;

    move-result-object v0

    .line 286
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    move-result-object v0

    goto :goto_1

    .line 281
    :cond_2
    new-instance v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;-><init>()V

    .line 282
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->variable_amount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;

    move-result-object v0

    .line 283
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    move-result-object v0

    :goto_1
    return-object v0
.end method


# virtual methods
.method public amountAppliesPerItemQuantity()Z
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/checkout/Discount;->scope:Lcom/squareup/checkout/Discount$Scope;

    iget-boolean v0, v0, Lcom/squareup/checkout/Discount$Scope;->applyPerQuantity:Z

    return v0
.end method

.method public final asProto()Lcom/squareup/api/items/Discount;
    .locals 1

    .line 212
    iget-object v0, p0, Lcom/squareup/checkout/Discount;->discountProto:Lcom/squareup/api/items/Discount;

    return-object v0
.end method

.method public asRequestAdjustment(Lcom/squareup/protos/common/Money;Ljava/util/List;)Lcom/squareup/server/payment/value/Adjustment;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/server/payment/value/ItemizedAdjustment;",
            ">;)",
            "Lcom/squareup/server/payment/value/Adjustment;"
        }
    .end annotation

    const-string p2, "collected"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 219
    new-instance p2, Lcom/squareup/server/payment/value/Adjustment;

    .line 220
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    .line 224
    iget-object v0, p0, Lcom/squareup/checkout/Discount;->phase:Lcom/squareup/api/items/CalculationPhase;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/api/items/CalculationPhase;->getValue()I

    move-result v0

    move v5, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v5, 0x0

    :goto_0
    const/4 v6, 0x0

    .line 226
    iget-object v7, p0, Lcom/squareup/checkout/Discount;->name:Ljava/lang/String;

    .line 227
    iget-object v8, p0, Lcom/squareup/checkout/Discount;->percentage:Lcom/squareup/util/Percentage;

    .line 228
    iget-object v9, p0, Lcom/squareup/checkout/Discount;->amount:Lcom/squareup/protos/common/Money;

    const/4 v10, 0x0

    .line 230
    iget-object v11, p0, Lcom/squareup/checkout/Discount;->couponToken:Ljava/lang/String;

    const-string v2, "discount"

    const/4 v3, 0x0

    move-object v0, p2

    move-object v4, p1

    .line 219
    invoke-direct/range {v0 .. v11}, Lcom/squareup/server/payment/value/Adjustment;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ILcom/squareup/api/items/Fee$InclusionType;Ljava/lang/String;Lcom/squareup/util/Percentage;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/lang/String;)V

    return-object p2
.end method

.method public final buildDiscountLineItem(Lcom/squareup/protos/common/Money;Z)Lcom/squareup/protos/client/bills/DiscountLineItem;
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-static/range {v0 .. v5}, Lcom/squareup/checkout/Discount;->buildDiscountLineItem$default(Lcom/squareup/checkout/Discount;Lcom/squareup/protos/common/Money;ZZILjava/lang/Object;)Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object p1

    return-object p1
.end method

.method public final buildDiscountLineItem(Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/protos/client/bills/DiscountLineItem;
    .locals 3

    .line 244
    new-instance v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;-><init>()V

    .line 245
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->idPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object v0

    .line 246
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->createdAt:Ljava/util/Date;

    invoke-static {v1}, Lcom/squareup/checkout/util/ISO8601Dates;->tryBuildISO8601Date(Ljava/util/Date;)Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object v0

    .line 247
    invoke-direct {p0}, Lcom/squareup/checkout/Discount;->buildDiscountLineItemConfiguration()Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->configuration(Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object v0

    .line 249
    new-instance v1, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;-><init>()V

    .line 250
    iget-object v2, p0, Lcom/squareup/checkout/Discount;->discountProto:Lcom/squareup/api/items/Discount;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->discount(Lcom/squareup/api/items/Discount;)Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;

    move-result-object v1

    if-eqz p2, :cond_2

    .line 252
    iget-object p2, p0, Lcom/squareup/checkout/Discount;->couponToken:Ljava/lang/String;

    check-cast p2, Ljava/lang/CharSequence;

    if-eqz p2, :cond_1

    invoke-static {p2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p2, 0x1

    :goto_1
    if-nez p2, :cond_2

    .line 253
    iget-object p2, p0, Lcom/squareup/checkout/Discount;->couponToken:Ljava/lang/String;

    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->coupon_ids(Ljava/util/List;)Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;

    move-result-object p2

    .line 254
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->couponToken:Ljava/lang/String;

    invoke-virtual {p2, v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->coupon_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;

    move-result-object p2

    goto :goto_2

    .line 257
    :cond_2
    iget-object p2, p0, Lcom/squareup/checkout/Discount;->couponToken:Ljava/lang/String;

    invoke-virtual {v1, p2}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->coupon_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;

    move-result-object p2

    .line 260
    :goto_2
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    move-result-object p2

    .line 248
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_backing_details(Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object p2

    const-string v0, "1"

    .line 262
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object p2

    .line 263
    iget-object v0, p0, Lcom/squareup/checkout/Discount;->scope:Lcom/squareup/checkout/Discount$Scope;

    iget-object v0, v0, Lcom/squareup/checkout/Discount$Scope;->protoScope:Lcom/squareup/protos/client/bills/ApplicationScope;

    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->application_scope(Lcom/squareup/protos/client/bills/ApplicationScope;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object p2

    if-eqz p1, :cond_3

    .line 267
    new-instance v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;-><init>()V

    .line 268
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;->applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;

    move-result-object p1

    .line 269
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    move-result-object p1

    .line 266
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    :cond_3
    if-eqz p3, :cond_4

    .line 273
    sget-object p1, Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;->PRICING_RULE:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->application_method(Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    .line 275
    :cond_4
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object p1

    const-string p2, "builder.build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final canBeAppliedPerItem()Z
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/squareup/checkout/Discount;->scope:Lcom/squareup/checkout/Discount$Scope;

    iget-boolean v0, v0, Lcom/squareup/checkout/Discount$Scope;->atItemScope:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/checkout/Discount;->percentage:Lcom/squareup/util/Percentage;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public final canOnlyBeAppliedToOneItem()Z
    .locals 1

    .line 173
    iget-object v0, p0, Lcom/squareup/checkout/Discount;->matches:Lcom/squareup/checkout/Discount$Matches;

    instance-of v0, v0, Lcom/squareup/checkout/Discount$Matches$AllItems;

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final getCatalogId()Ljava/lang/String;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/checkout/Discount;->catalogId:Ljava/lang/String;

    return-object v0
.end method

.method public final getCouponDefinitionToken()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/checkout/Discount;->couponDefinitionToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getCouponReason()Lcom/squareup/protos/client/coupons/Coupon$Reason;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/checkout/Discount;->couponReason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    return-object v0
.end method

.method public final getCouponToken()Ljava/lang/String;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/checkout/Discount;->couponToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getMatches()Lcom/squareup/checkout/Discount$Matches;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/checkout/Discount;->matches:Lcom/squareup/checkout/Discount$Matches;

    return-object v0
.end method

.method public final getScope()Lcom/squareup/checkout/Discount$Scope;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/checkout/Discount;->scope:Lcom/squareup/checkout/Discount$Scope;

    return-object v0
.end method

.method public final isAmountDiscount()Z
    .locals 1

    .line 96
    iget-boolean v0, p0, Lcom/squareup/checkout/Discount;->isAmountDiscount:Z

    return v0
.end method

.method public final isCartScopeAmountDiscount()Z
    .locals 1

    .line 100
    iget-boolean v0, p0, Lcom/squareup/checkout/Discount;->isCartScopeAmountDiscount:Z

    return v0
.end method

.method public final isCartScopeDiscount()Z
    .locals 1

    .line 98
    iget-boolean v0, p0, Lcom/squareup/checkout/Discount;->isCartScopeDiscount:Z

    return v0
.end method

.method public final isComp()Z
    .locals 1

    .line 113
    iget-boolean v0, p0, Lcom/squareup/checkout/Discount;->isComp:Z

    return v0
.end method

.method public final isCoupon()Z
    .locals 1

    .line 90
    iget-boolean v0, p0, Lcom/squareup/checkout/Discount;->isCoupon:Z

    return v0
.end method

.method public final isFixedPercentage()Z
    .locals 1

    .line 111
    iget-boolean v0, p0, Lcom/squareup/checkout/Discount;->isFixedPercentage:Z

    return v0
.end method

.method public final isItemEligible(Lcom/squareup/checkout/CartItem;)Z
    .locals 2

    const-string v0, "cartItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkout/Discount;->matches:Lcom/squareup/checkout/Discount$Matches;

    instance-of v1, v0, Lcom/squareup/checkout/Discount$Matches$Categories;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/squareup/checkout/Discount$Matches$Categories;

    invoke-virtual {v0}, Lcom/squareup/checkout/Discount$Matches$Categories;->getCategoryIds()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->categoryId()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->contains(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    move-result p1

    goto :goto_1

    .line 182
    :cond_1
    instance-of v1, v0, Lcom/squareup/checkout/Discount$Matches$Items;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/squareup/checkout/Discount$Matches$Items;

    invoke-virtual {v0}, Lcom/squareup/checkout/Discount$Matches$Items;->getItemVariationIds()Ljava/util/List;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v1, "cartItem.selectedVariation"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    goto :goto_1

    :cond_2
    const/4 p1, 0x1

    goto :goto_1

    :cond_3
    :goto_0
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method public final isVariable()Z
    .locals 1

    .line 107
    iget-boolean v0, p0, Lcom/squareup/checkout/Discount;->isVariable:Z

    return v0
.end method

.method public final passcodeRequired()Z
    .locals 2

    .line 191
    iget-object v0, p0, Lcom/squareup/checkout/Discount;->discountProto:Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->pin_required:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/api/items/Discount;->DEFAULT_PIN_REQUIRED:Ljava/lang/Boolean;

    const-string v1, "DEFAULT_PIN_REQUIRED"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 194
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OrderDiscount{id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", name=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    iget-object v2, p0, Lcom/squareup/checkout/Discount;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->percentage:Lcom/squareup/util/Percentage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", rate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->rate:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", maxAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->maxAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->phase:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", priority="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->priority:Lcom/squareup/calc/constants/CalculationPriority;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->scope:Lcom/squareup/checkout/Discount$Scope;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", taxBasis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->taxBasis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    iget-boolean v1, p0, Lcom/squareup/checkout/Discount;->enabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", couponToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->couponToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", couponReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    iget-object v1, p0, Lcom/squareup/checkout/Discount;->couponReason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
