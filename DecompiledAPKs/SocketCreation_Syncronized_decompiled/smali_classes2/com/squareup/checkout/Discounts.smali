.class public final Lcom/squareup/checkout/Discounts;
.super Ljava/lang/Object;
.source "Discounts.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDiscounts.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Discounts.kt\ncom/squareup/checkout/Discounts\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 4 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,198:1\n704#2:199\n777#2,2:200\n1642#2,2:202\n704#2:204\n777#2,2:205\n1642#2,2:207\n1360#2:209\n1429#2,3:210\n250#2,2:213\n1288#2:215\n1313#2,3:216\n1316#2,3:226\n1866#2,7:232\n1265#2,12:240\n1360#2:252\n1429#2,3:253\n1360#2:256\n1429#2,3:257\n347#3,7:219\n67#4:229\n92#4,2:230\n94#4:239\n*E\n*S KotlinDebug\n*F\n+ 1 Discounts.kt\ncom/squareup/checkout/Discounts\n*L\n16#1:199\n16#1,2:200\n17#1,2:202\n24#1:204\n24#1,2:205\n25#1,2:207\n75#1:209\n75#1,3:210\n83#1,2:213\n95#1:215\n95#1,3:216\n95#1,3:226\n96#1,7:232\n151#1,12:240\n152#1:252\n152#1,3:253\n183#1:256\n183#1,3:257\n95#1,7:219\n96#1:229\n96#1,2:230\n96#1:239\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u001a\u001a\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u001a\u001a\u0010\u0006\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u001a\"\u0010\u0007\u001a\u0010\u0012\u000c\u0012\n \t*\u0004\u0018\u00010\u00040\u00040\u00082\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00050\n\u001a\n\u0010\u000b\u001a\u00020\u0004*\u00020\u000c\u001a0\u0010\r\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00050\u000e0\u0008*\u0008\u0012\u0004\u0012\u00020\u000f0\u00082\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0011\u001a \u0010\u0012\u001a\u0004\u0018\u00010\u0005*\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00132\u0006\u0010\u0014\u001a\u00020\u0004\u001a\n\u0010\u0015\u001a\u00020\u0016*\u00020\u0017\u001a\n\u0010\u0015\u001a\u00020\u0016*\u00020\u0018\u001a\u0012\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0008*\u00020\u000fH\u0002\u001a\u0014\u0010\u001a\u001a\u00020\u000f*\u00020\u000f2\u0006\u0010\u001b\u001a\u00020\u000fH\u0002\u001a\u001a\u0010\u001c\u001a\u00020\u0005*\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0016\u001a\u001a\u0010\u001c\u001a\u00020\u0005*\u00020\u00182\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0016\u001a\u000c\u0010!\u001a\u00020\"*\u00020\u0004H\u0002\u001a\u0016\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0008*\u0008\u0012\u0004\u0012\u00020\u000f0\u0008\u00a8\u0006$"
    }
    d2 = {
        "removeCompDiscounts",
        "",
        "discounts",
        "",
        "",
        "Lcom/squareup/checkout/Discount;",
        "removeDiscountsThatCanBeAppliedToOnlyOneItem",
        "toIds",
        "",
        "kotlin.jvm.PlatformType",
        "",
        "couponDefinitionToken",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "deserializeToDiscounts",
        "Lkotlin/Pair;",
        "Lcom/squareup/protos/client/bills/DiscountLineItem;",
        "parentItemSelectedVariationId",
        "Lkotlin/Function0;",
        "getCatalogId",
        "",
        "catalogId",
        "isFixedPercentage",
        "",
        "Lcom/squareup/api/items/Discount;",
        "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
        "maybeExpand",
        "mergeWithOtherLineItem",
        "other",
        "toAppliedDiscount",
        "Lcom/squareup/checkout/Discount$Builder;",
        "scope",
        "Lcom/squareup/checkout/Discount$Scope;",
        "discountApplicationIdEnabled",
        "toIntOrOne",
        "",
        "toMergedDiscountLineItems",
        "checkout_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final couponDefinitionToken(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$couponDefinitionToken"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget-object p0, p0, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    iget-object p0, p0, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    const-string v0, "discount.id"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final deserializeToDiscounts(Ljava/util/List;Lkotlin/jvm/functions/Function0;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            "Lcom/squareup/checkout/Discount;",
            ">;>;"
        }
    .end annotation

    const-string v0, "$this$deserializeToDiscounts"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parentItemSelectedVariationId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    check-cast p0, Ljava/lang/Iterable;

    .line 240
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 247
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 248
    check-cast v1, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 151
    invoke-static {v1}, Lcom/squareup/checkout/Discounts;->maybeExpand(Lcom/squareup/protos/client/bills/DiscountLineItem;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 249
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 251
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 252
    new-instance p0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p0, Ljava/util/Collection;

    .line 253
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 254
    check-cast v1, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 153
    new-instance v2, Lkotlin/Pair;

    .line 155
    new-instance v3, Lcom/squareup/checkout/Discount$Builder;

    .line 157
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/4 v5, 0x1

    .line 155
    invoke-direct {v3, v1, v4, v5}, Lcom/squareup/checkout/Discount$Builder;-><init>(Lcom/squareup/protos/client/bills/DiscountLineItem;Ljava/lang/String;Z)V

    .line 159
    invoke-virtual {v3}, Lcom/squareup/checkout/Discount$Builder;->build()Lcom/squareup/checkout/Discount;

    move-result-object v3

    .line 153
    invoke-direct {v2, v1, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 160
    invoke-interface {p0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 255
    :cond_1
    check-cast p0, Ljava/util/List;

    return-object p0
.end method

.method public static final getCatalogId(Ljava/util/Map;Ljava/lang/String;)Lcom/squareup/checkout/Discount;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/checkout/Discount;"
        }
    .end annotation

    const-string v0, "$this$getCatalogId"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "catalogId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 213
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Discount;

    .line 83
    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->getCatalogId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 214
    :goto_0
    check-cast v0, Ljava/util/Map$Entry;

    if-eqz v0, :cond_2

    .line 84
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p0

    move-object v1, p0

    check-cast v1, Lcom/squareup/checkout/Discount;

    :cond_2
    return-object v1
.end method

.method public static final isFixedPercentage(Lcom/squareup/api/items/Discount;)Z
    .locals 2

    const-string v0, "$this$isFixedPercentage"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/api/items/Discount;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    sget-object v1, Lcom/squareup/api/items/Discount$DiscountType;->FIXED:Lcom/squareup/api/items/Discount$DiscountType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/api/items/Discount;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    if-nez v0, :cond_1

    iget-object p0, p0, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isFixedPercentage(Lcom/squareup/shared/catalog/models/CatalogDiscount;)Z
    .locals 1

    const-string v0, "$this$isFixedPercentage"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object p0

    const-string v0, "this.`object`()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lcom/squareup/api/items/Discount;

    invoke-static {p0}, Lcom/squareup/checkout/Discounts;->isFixedPercentage(Lcom/squareup/api/items/Discount;)Z

    move-result p0

    return p0
.end method

.method private static final maybeExpand(Lcom/squareup/protos/client/bills/DiscountLineItem;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;"
        }
    .end annotation

    .line 174
    iget-object v0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_deleted:Ljava/lang/Boolean;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p0

    goto :goto_2

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p0

    goto :goto_2

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_ids:Ljava/util/List;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-le v0, v1, :cond_4

    .line 182
    iget-object v0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_ids:Ljava/util/List;

    const-string/jumbo v1, "write_only_backing_details.coupon_ids"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 256
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 257
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 258
    check-cast v2, Ljava/lang/String;

    .line 184
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem;->newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object v3

    .line 186
    iget-object v4, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;

    move-result-object v4

    .line 187
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->coupon_ids(Ljava/util/List;)Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;

    move-result-object v4

    .line 188
    invoke-virtual {v4, v2}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->coupon_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;

    move-result-object v2

    .line 189
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    move-result-object v2

    .line 185
    invoke-virtual {v3, v2}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_backing_details(Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object v2

    .line 191
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 259
    :cond_3
    move-object p0, v1

    check-cast p0, Ljava/util/List;

    goto :goto_2

    .line 195
    :cond_4
    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    :goto_2
    return-object p0
.end method

.method private static final mergeWithOtherLineItem(Lcom/squareup/protos/client/bills/DiscountLineItem;Lcom/squareup/protos/client/bills/DiscountLineItem;)Lcom/squareup/protos/client/bills/DiscountLineItem;
    .locals 5

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem;->newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object v0

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;

    move-result-object v1

    .line 116
    iget-object v2, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_ids:Ljava/util/List;

    const-string/jumbo v3, "write_only_backing_details.coupon_ids"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/util/Collection;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_ids:Ljava/util/List;

    const-string v4, "other.write_only_backing_details.coupon_ids"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v2, v3}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    .line 115
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->coupon_ids(Ljava/util/List;)Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;

    move-result-object v1

    .line 118
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    move-result-object v1

    .line 113
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_backing_details(Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object v0

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;

    move-result-object v1

    .line 122
    iget-object v2, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-static {v2, v3}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;->applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;

    move-result-object v1

    .line 123
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    move-result-object v1

    .line 120
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object v0

    .line 126
    iget-object p0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_quantity:Ljava/lang/String;

    const-string v1, "discount_quantity"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/checkout/Discounts;->toIntOrOne(Ljava/lang/String;)I

    move-result p0

    iget-object p1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_quantity:Ljava/lang/String;

    const-string v1, "other.discount_quantity"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/checkout/Discounts;->toIntOrOne(Ljava/lang/String;)I

    move-result p1

    add-int/2addr p0, p1

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    .line 125
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object p0

    .line 128
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object p0

    const-string p1, "newBuilder()\n      .writ\u2026()\n      )\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final removeCompDiscounts(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;)V"
        }
    .end annotation

    const-string v0, "discounts"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 204
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 205
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/Discount;

    .line 24
    invoke-virtual {v3}, Lcom/squareup/checkout/Discount;->isComp()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 206
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 207
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 25
    invoke-interface {p0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    return-void
.end method

.method public static final removeDiscountsThatCanBeAppliedToOnlyOneItem(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;)V"
        }
    .end annotation

    const-string v0, "discounts"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 199
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 200
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/Discount;

    .line 16
    invoke-virtual {v3}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 201
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 202
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 17
    invoke-interface {p0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    return-void
.end method

.method public static final toAppliedDiscount(Lcom/squareup/checkout/Discount$Builder;Lcom/squareup/checkout/Discount$Scope;Z)Lcom/squareup/checkout/Discount;
    .locals 1

    const-string v0, "$this$toAppliedDiscount"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 40
    iget-object p2, p0, Lcom/squareup/checkout/Discount$Builder;->id:Ljava/lang/String;

    goto :goto_0

    .line 42
    :cond_0
    iget-object p2, p0, Lcom/squareup/checkout/Discount$Builder;->id:Ljava/lang/String;

    .line 45
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/checkout/Discount$Builder;->scope(Lcom/squareup/checkout/Discount$Scope;)Lcom/squareup/checkout/Discount$Builder;

    move-result-object p0

    .line 46
    invoke-virtual {p0, p2}, Lcom/squareup/checkout/Discount$Builder;->id(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object p0

    check-cast p0, Lcom/squareup/checkout/Discount$Builder;

    .line 47
    invoke-virtual {p0}, Lcom/squareup/checkout/Discount$Builder;->build()Lcom/squareup/checkout/Discount;

    move-result-object p0

    return-object p0
.end method

.method public static final toAppliedDiscount(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/Discount$Scope;Z)Lcom/squareup/checkout/Discount;
    .locals 1

    const-string v0, "$this$toAppliedDiscount"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance v0, Lcom/squareup/checkout/Discount$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/checkout/Discount$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;)V

    invoke-static {v0, p1, p2}, Lcom/squareup/checkout/Discounts;->toAppliedDiscount(Lcom/squareup/checkout/Discount$Builder;Lcom/squareup/checkout/Discount$Scope;Z)Lcom/squareup/checkout/Discount;

    move-result-object p0

    return-object p0
.end method

.method public static final toIds(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lcom/squareup/checkout/Discount;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "discounts"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 209
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 210
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 211
    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 75
    iget-object v1, v1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 212
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private static final toIntOrOne(Ljava/lang/String;)I
    .locals 0

    .line 135
    invoke-static {p0}, Lkotlin/text/StringsKt;->toIntOrNull(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x1

    :goto_0
    return p0
.end method

.method public static final toMergedDiscountLineItems(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$toMergedDiscountLineItems"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    check-cast p0, Ljava/lang/Iterable;

    .line 215
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 216
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 217
    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 95
    iget-object v2, v2, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->discount:Lcom/squareup/api/items/Discount;

    iget-object v2, v2, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    .line 219
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    .line 218
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 222
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    :cond_0
    check-cast v3, Ljava/util/List;

    .line 226
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 229
    :cond_1
    new-instance p0, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {p0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p0, Ljava/util/Collection;

    .line 230
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 231
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 97
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    const/4 v2, 0x0

    .line 99
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/DiscountLineItem;

    goto :goto_3

    .line 102
    :cond_2
    check-cast v1, Ljava/lang/Iterable;

    .line 232
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 233
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 234
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 235
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 236
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/DiscountLineItem;

    check-cast v2, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 102
    invoke-static {v2, v3}, Lcom/squareup/checkout/Discounts;->mergeWithOtherLineItem(Lcom/squareup/protos/client/bills/DiscountLineItem;Lcom/squareup/protos/client/bills/DiscountLineItem;)Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object v2

    goto :goto_2

    .line 238
    :cond_3
    move-object v1, v2

    check-cast v1, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 103
    :goto_3
    invoke-interface {p0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 233
    :cond_4
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Empty collection can\'t be reduced."

    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 239
    :cond_5
    check-cast p0, Ljava/util/List;

    return-object p0
.end method
