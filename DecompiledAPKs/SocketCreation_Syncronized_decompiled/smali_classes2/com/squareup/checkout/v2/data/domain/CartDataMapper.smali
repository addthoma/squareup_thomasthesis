.class public final Lcom/squareup/checkout/v2/data/domain/CartDataMapper;
.super Ljava/lang/Object;
.source "CartDataMapper.kt"

# interfaces
.implements Lcom/squareup/checkout/v2/data/domain/CheckoutDomainDataMapper;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/checkout/v2/data/domain/CheckoutDomainDataMapper<",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/checkout/CartItem;",
        ">;",
        "Lcom/squareup/checkout/v2/data/transaction/model/CartData;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCartDataMapper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CartDataMapper.kt\ncom/squareup/checkout/v2/data/domain/CartDataMapper\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,23:1\n1360#2:24\n1429#2,3:25\n*E\n*S KotlinDebug\n*F\n+ 1 CartDataMapper.kt\ncom/squareup/checkout/v2/data/domain/CartDataMapper\n*L\n14#1:24\n14#1,3:25\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0004\u0012\u00020\u00040\u0001B\u0005\u00a2\u0006\u0002\u0010\u0005J\u0016\u0010\u0006\u001a\u00020\u00042\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/checkout/v2/data/domain/CartDataMapper;",
        "Lcom/squareup/checkout/v2/data/domain/CheckoutDomainDataMapper;",
        "",
        "Lcom/squareup/checkout/CartItem;",
        "Lcom/squareup/checkout/v2/data/transaction/model/CartData;",
        "()V",
        "map",
        "source",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public map(Ljava/util/List;)Lcom/squareup/checkout/v2/data/transaction/model/CartData;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Lcom/squareup/checkout/v2/data/transaction/model/CartData;"
        }
    .end annotation

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    check-cast p1, Ljava/lang/Iterable;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 25
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 26
    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 15
    new-instance v2, Lcom/squareup/checkout/v2/data/transaction/model/ItemData;

    .line 16
    iget-object v3, v1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    const-string v4, "item.quantity"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/squareup/util/BigDecimals;->asIntOr1(Ljava/math/BigDecimal;)I

    move-result v3

    .line 17
    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    const-string v4, "item.itemName"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {v2, v3, v1}, Lcom/squareup/checkout/v2/data/transaction/model/ItemData;-><init>(ILjava/lang/String;)V

    .line 18
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 27
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 20
    invoke-static {v0}, Lcom/squareup/checkout/v2/data/transaction/model/CartDataKt;->toCartData(Ljava/util/List;)Lcom/squareup/checkout/v2/data/transaction/model/CartData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic map(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/v2/data/domain/CartDataMapper;->map(Ljava/util/List;)Lcom/squareup/checkout/v2/data/transaction/model/CartData;

    move-result-object p1

    return-object p1
.end method
