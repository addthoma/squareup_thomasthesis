.class final Lcom/squareup/checkout/v2/data/RealTransactionData$onEnterScope$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealTransactionData.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkout/v2/data/RealTransactionData;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/payment/OrderEntryEvents$CartChanged;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/payment/OrderEntryEvents$CartChanged;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkout/v2/data/RealTransactionData;


# direct methods
.method constructor <init>(Lcom/squareup/checkout/v2/data/RealTransactionData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkout/v2/data/RealTransactionData$onEnterScope$1;->this$0:Lcom/squareup/checkout/v2/data/RealTransactionData;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/v2/data/RealTransactionData$onEnterScope$1;->invoke(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 3

    .line 35
    iget-object p1, p0, Lcom/squareup/checkout/v2/data/RealTransactionData$onEnterScope$1;->this$0:Lcom/squareup/checkout/v2/data/RealTransactionData;

    invoke-static {p1}, Lcom/squareup/checkout/v2/data/RealTransactionData;->access$get_cartData$p(Lcom/squareup/checkout/v2/data/RealTransactionData;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/checkout/v2/data/RealTransactionData$onEnterScope$1;->this$0:Lcom/squareup/checkout/v2/data/RealTransactionData;

    invoke-static {v0}, Lcom/squareup/checkout/v2/data/RealTransactionData;->access$getCartDataMapper$p(Lcom/squareup/checkout/v2/data/RealTransactionData;)Lcom/squareup/checkout/v2/data/domain/CartDataMapper;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkout/v2/data/RealTransactionData$onEnterScope$1;->this$0:Lcom/squareup/checkout/v2/data/RealTransactionData;

    invoke-static {v1}, Lcom/squareup/checkout/v2/data/RealTransactionData;->access$getTransaction$p(Lcom/squareup/checkout/v2/data/RealTransactionData;)Lcom/squareup/payment/Transaction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object v1

    const-string v2, "transaction.items"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/v2/data/domain/CartDataMapper;->map(Ljava/util/List;)Lcom/squareup/checkout/v2/data/transaction/model/CartData;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
