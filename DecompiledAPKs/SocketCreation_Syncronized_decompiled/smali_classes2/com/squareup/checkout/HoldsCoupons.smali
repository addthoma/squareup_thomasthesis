.class public interface abstract Lcom/squareup/checkout/HoldsCoupons;
.super Ljava/lang/Object;
.source "HoldsCoupons.java"


# static fields
.field public static final DONT_QUEUE_RETURN_COUPON_TASK:Z = false

.field public static final QUEUE_RETURN_COUPON_TASK:Z = true


# virtual methods
.method public abstract apply(Lcom/squareup/protos/client/coupons/Coupon;)V
.end method

.method public abstract discountsChanged()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAllAddedCoupons(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation
.end method

.method public abstract hasQualifyingItem(Lcom/squareup/protos/client/coupons/Coupon;)Z
.end method

.method public abstract isCouponAddedToCart(Ljava/lang/String;)Z
.end method

.method public abstract remove(Lcom/squareup/protos/client/coupons/Coupon;)V
.end method

.method public abstract removeCoupon(Ljava/lang/String;Z)V
.end method
