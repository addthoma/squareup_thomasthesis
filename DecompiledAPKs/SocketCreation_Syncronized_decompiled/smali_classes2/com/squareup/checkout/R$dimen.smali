.class public final Lcom/squareup/checkout/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final buyer_action_container_helper_text:I = 0x7f070090

.field public static final buyer_action_container_margin:I = 0x7f070091

.field public static final buyer_facing_action_bar_medium_text_size:I = 0x7f07009a

.field public static final buyer_facing_bar_button_height:I = 0x7f07009b

.field public static final buyer_facing_bar_button_margin:I = 0x7f07009c

.field public static final buyer_facing_height:I = 0x7f07009d

.field public static final buyer_facing_input_margin:I = 0x7f07009e

.field public static final buyer_facing_input_padding:I = 0x7f07009f

.field public static final buyer_facing_margin:I = 0x7f0700a0

.field public static final buyer_facing_text_size_secondary:I = 0x7f0700a1

.field public static final buyer_facing_text_size_tile_button:I = 0x7f0700a2

.field public static final buyer_facing_tile_button_height:I = 0x7f0700a3

.field public static final buyer_facing_title_size:I = 0x7f0700a4

.field public static final buyer_noho_action_bar_condensed_height:I = 0x7f0700a5

.field public static final discount_tag_gap:I = 0x7f07013e

.field public static final gutter_sheet_horizontal:I = 0x7f070188

.field public static final multiline_spacing:I = 0x7f070354

.field public static final payment_flow_spacing_horizontal:I = 0x7f07040a

.field public static final payment_flow_spacing_vertical:I = 0x7f07040b

.field public static final payment_options_container_margin_horizontal:I = 0x7f07040d

.field public static final payment_options_container_margin_vertical:I = 0x7f07040e

.field public static final payment_options_divider_height:I = 0x7f07040f

.field public static final payment_options_row_height:I = 0x7f070410

.field public static final receipt_disclaimer_gap:I = 0x7f070452

.field public static final receipt_disclaimer_text_size:I = 0x7f070453


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
