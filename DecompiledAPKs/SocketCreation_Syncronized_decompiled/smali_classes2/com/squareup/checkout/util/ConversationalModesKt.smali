.class public final Lcom/squareup/checkout/util/ConversationalModesKt;
.super Ljava/lang/Object;
.source "ConversationalModes.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nConversationalModes.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ConversationalModes.kt\ncom/squareup/checkout/util/ConversationalModesKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,36:1\n1360#2:37\n1429#2,3:38\n*E\n*S KotlinDebug\n*F\n+ 1 ConversationalModes.kt\ncom/squareup/checkout/util/ConversationalModesKt\n*L\n26#1:37\n26#1,3:38\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0008\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u001a \u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0005\u00a8\u0006\u0007"
    }
    d2 = {
        "modifierQuantityForModes",
        "",
        "",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;",
        "isPreselectedModifier",
        "",
        "isQuantityForPricing",
        "checkout_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final modifierQuantityForModes(Ljava/util/List;ZZ)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;",
            ">;ZZ)I"
        }
    .end annotation

    const-string v0, "$this$modifierQuantityForModes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 26
    :cond_0
    move-object v0, p0

    check-cast v0, Ljava/lang/Iterable;

    .line 37
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 38
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 39
    check-cast v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;

    .line 26
    iget-object v3, v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->type:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 40
    :cond_1
    check-cast v2, Ljava/util/List;

    .line 28
    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;->EXTRA:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p1, :cond_2

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    if-ne p0, v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x2

    :goto_1
    return v1

    .line 30
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;->NO:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    invoke-interface {v2, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_5

    sget-object p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;->ALLERGY:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    invoke-interface {v2, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    goto :goto_2

    :cond_4
    return v1

    :cond_5
    :goto_2
    if-eqz p1, :cond_6

    if-eqz p2, :cond_6

    goto :goto_3

    :cond_6
    const/4 v1, 0x0

    :goto_3
    return v1
.end method
