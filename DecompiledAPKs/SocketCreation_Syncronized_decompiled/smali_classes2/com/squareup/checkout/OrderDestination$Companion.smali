.class public final Lcom/squareup/checkout/OrderDestination$Companion;
.super Ljava/lang/Object;
.source "OrderDestination.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/OrderDestination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderDestination.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderDestination.kt\ncom/squareup/checkout/OrderDestination$Companion\n*L\n1#1,74:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u0007J\u0018\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u000bH\u0007\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/checkout/OrderDestination$Companion;",
        "",
        "()V",
        "fromFeatureDetails",
        "Lcom/squareup/checkout/OrderDestination;",
        "featureDetails",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;",
        "seats",
        "",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
        "seatsFromFeatureDetails",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails;",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/checkout/OrderDestination$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromFeatureDetails(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;Ljava/util/List;)Lcom/squareup/checkout/OrderDestination;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Lcom/squareup/checkout/OrderDestination;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "seats"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 66
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    if-eqz p1, :cond_0

    new-instance v0, Lcom/squareup/checkout/OrderDestination;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    const-string v1, "it.destination"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1, p2}, Lcom/squareup/checkout/OrderDestination;-><init>(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;Ljava/util/List;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final seatsFromFeatureDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    if-eqz p1, :cond_0

    .line 71
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;->seat:Ljava/util/List;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_0
    return-object p1
.end method
