.class public Lcom/squareup/checkout/CartItem$Builder;
.super Ljava/lang/Object;
.source "CartItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/CartItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final appliedDiscounts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation
.end field

.field private appliedModifiers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;"
        }
    .end annotation
.end field

.field private final appliedTaxes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end field

.field private backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

.field private backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

.field private final blacklistedDiscounts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private category:Lcom/squareup/api/items/MenuCategory;

.field private color:Ljava/lang/String;

.field private courseId:Ljava/lang/String;

.field private createdAt:Ljava/util/Date;

.field private defaultTaxes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end field

.field private destination:Lcom/squareup/checkout/OrderDestination;

.field private final events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;"
        }
    .end annotation
.end field

.field private featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

.field private hasHiddenModifier:Z

.field private idPair:Lcom/squareup/protos/client/IdPair;

.field private isEcomAvailable:Z

.field private isTaxedInTransactionsHistory:Ljava/lang/Boolean;

.field private isVoided:Z

.field private itemAbbreviation:Ljava/lang/String;

.field private itemDescription:Ljava/lang/String;

.field private itemId:Ljava/lang/String;

.field private itemName:Ljava/lang/String;

.field private final itemOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation
.end field

.field private itemizedAdjustmentAmountsFromTransactionsHistoryById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private lockConfiguration:Z

.field private merchantCatalogObjectToken:Ljava/lang/String;

.field private modifierLists:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifierList;",
            ">;"
        }
    .end annotation
.end field

.field private notes:Ljava/lang/String;

.field private overridePrice:Lcom/squareup/protos/common/Money;

.field private photoToken:Ljava/lang/String;

.field private photoUrl:Ljava/lang/String;

.field private final pricingEngineAppliedDiscounts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private quantity:Ljava/math/BigDecimal;

.field private quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

.field private ruleBasedTaxes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end field

.field private selectedDiningOption:Lcom/squareup/checkout/DiningOption;

.field private selectedModifiers:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;>;"
        }
    .end annotation
.end field

.field private selectedVariation:Lcom/squareup/checkout/OrderVariation;

.field private showVariationNameOverride:Z

.field private skipModifierDetailScreen:Z

.field private final userEditedTaxIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private variablePrice:Lcom/squareup/protos/common/Money;

.field private final variations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/OrderVariation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedDiscounts:Ljava/util/Map;

    .line 1233
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->pricingEngineAppliedDiscounts:Ljava/util/Set;

    .line 1234
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->blacklistedDiscounts:Ljava/util/Set;

    .line 1235
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes:Ljava/util/Map;

    .line 1238
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->createdAt:Ljava/util/Date;

    const/4 v0, 0x0

    .line 1239
    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->ruleBasedTaxes:Ljava/util/Map;

    .line 1244
    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->defaultTaxes:Ljava/util/Map;

    .line 1245
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v1, p0, Lcom/squareup/checkout/CartItem$Builder;->userEditedTaxIds:Ljava/util/Set;

    .line 1247
    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_AMOUNT:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    iput-object v1, p0, Lcom/squareup/checkout/CartItem$Builder;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    const-string v1, ""

    .line 1250
    iput-object v1, p0, Lcom/squareup/checkout/CartItem$Builder;->itemAbbreviation:Ljava/lang/String;

    .line 1252
    iput-object v1, p0, Lcom/squareup/checkout/CartItem$Builder;->itemName:Ljava/lang/String;

    .line 1254
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/squareup/checkout/CartItem$Builder;->itemOptions:Ljava/util/List;

    .line 1255
    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->category:Lcom/squareup/api/items/MenuCategory;

    .line 1257
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->modifierLists:Ljava/util/SortedMap;

    .line 1261
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->quantity:Ljava/math/BigDecimal;

    .line 1262
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->MANUALLY_ENTERED:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    .line 1264
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedModifiers:Ljava/util/SortedMap;

    .line 1266
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedModifiers:Ljava/util/Map;

    .line 1267
    sget-object v0, Lcom/squareup/checkout/OrderVariation;->CUSTOM_ITEM_VARIATION:Lcom/squareup/checkout/OrderVariation;

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const/4 v0, 0x0

    .line 1268
    iput-boolean v0, p0, Lcom/squareup/checkout/CartItem$Builder;->showVariationNameOverride:Z

    .line 1269
    iput-boolean v0, p0, Lcom/squareup/checkout/CartItem$Builder;->skipModifierDetailScreen:Z

    .line 1270
    iput-boolean v0, p0, Lcom/squareup/checkout/CartItem$Builder;->hasHiddenModifier:Z

    .line 1273
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->variations:Ljava/util/List;

    .line 1274
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->events:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/checkout/CartItem;)V
    .locals 2

    .line 1299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedDiscounts:Ljava/util/Map;

    .line 1233
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->pricingEngineAppliedDiscounts:Ljava/util/Set;

    .line 1234
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->blacklistedDiscounts:Ljava/util/Set;

    .line 1235
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes:Ljava/util/Map;

    .line 1238
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->createdAt:Ljava/util/Date;

    const/4 v0, 0x0

    .line 1239
    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->ruleBasedTaxes:Ljava/util/Map;

    .line 1244
    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->defaultTaxes:Ljava/util/Map;

    .line 1245
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v1, p0, Lcom/squareup/checkout/CartItem$Builder;->userEditedTaxIds:Ljava/util/Set;

    .line 1247
    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_AMOUNT:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    iput-object v1, p0, Lcom/squareup/checkout/CartItem$Builder;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    const-string v1, ""

    .line 1250
    iput-object v1, p0, Lcom/squareup/checkout/CartItem$Builder;->itemAbbreviation:Ljava/lang/String;

    .line 1252
    iput-object v1, p0, Lcom/squareup/checkout/CartItem$Builder;->itemName:Ljava/lang/String;

    .line 1254
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/squareup/checkout/CartItem$Builder;->itemOptions:Ljava/util/List;

    .line 1255
    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->category:Lcom/squareup/api/items/MenuCategory;

    .line 1257
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->modifierLists:Ljava/util/SortedMap;

    .line 1261
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->quantity:Ljava/math/BigDecimal;

    .line 1262
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->MANUALLY_ENTERED:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    .line 1264
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedModifiers:Ljava/util/SortedMap;

    .line 1266
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedModifiers:Ljava/util/Map;

    .line 1267
    sget-object v0, Lcom/squareup/checkout/OrderVariation;->CUSTOM_ITEM_VARIATION:Lcom/squareup/checkout/OrderVariation;

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const/4 v0, 0x0

    .line 1268
    iput-boolean v0, p0, Lcom/squareup/checkout/CartItem$Builder;->showVariationNameOverride:Z

    .line 1269
    iput-boolean v0, p0, Lcom/squareup/checkout/CartItem$Builder;->skipModifierDetailScreen:Z

    .line 1270
    iput-boolean v0, p0, Lcom/squareup/checkout/CartItem$Builder;->hasHiddenModifier:Z

    .line 1273
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->variations:Ljava/util/List;

    .line 1274
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->events:Ljava/util/List;

    .line 1300
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->appliedDiscounts(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1301
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1302
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->backingDetails(Lcom/squareup/protos/client/bills/Itemization$BackingDetails;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1303
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->color:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->color(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1304
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->createdAt:Ljava/util/Date;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->createdAt(Ljava/util/Date;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1305
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->ruleBasedTaxes:Ljava/util/Map;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->ruleBasedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1306
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->defaultTaxes:Ljava/util/Map;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->defaultTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1307
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->userEditedTaxIds:Ljava/util/Set;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->userEditedTaxIds(Ljava/util/Set;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1308
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->idPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1309
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->backingType(Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1310
    invoke-static {p1}, Lcom/squareup/checkout/CartItem;->access$4600(Lcom/squareup/checkout/CartItem;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->isTaxedInTransactionsHistory(Ljava/lang/Boolean;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1311
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->itemizedAdjustmentAmountsFromTransactionsHistoryById:Ljava/util/Map;

    invoke-direct {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->itemizedAdjustmentAmountsFromTransactionsHistoryById(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1313
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->itemAbbreviation:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->itemAbbreviation(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1314
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->itemId(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1315
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->itemName(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1316
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->itemDescription:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->itemDescription(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1317
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->category:Lcom/squareup/api/items/MenuCategory;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->category(Lcom/squareup/api/items/MenuCategory;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1318
    iget-boolean v0, p1, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->lockConfiguration(Z)Lcom/squareup/checkout/CartItem$Builder;

    .line 1319
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->modifierLists:Ljava/util/SortedMap;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->modifierLists(Ljava/util/SortedMap;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1320
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->notes(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1321
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->photoToken:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->photoToken(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1322
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->photoUrl:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->photoUrl(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1323
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->pricingRuleAppliedDiscounts:Ljava/util/Set;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->pricingEngineAppliedDiscounts(Ljava/util/Set;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1324
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->blacklistedDiscounts:Ljava/util/Set;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->blacklistedDiscounts(Ljava/util/Set;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1325
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1326
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->quantityEntryType(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1327
    invoke-static {p1}, Lcom/squareup/checkout/CartItem;->access$4700(Lcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->selectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1328
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->selectedModifiers(Ljava/util/SortedMap;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1329
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation(Lcom/squareup/checkout/OrderVariation;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1330
    invoke-static {p1}, Lcom/squareup/checkout/CartItem;->access$4800(Lcom/squareup/checkout/CartItem;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->showVariationNameOverride(Z)Lcom/squareup/checkout/CartItem$Builder;

    .line 1331
    invoke-static {p1}, Lcom/squareup/checkout/CartItem;->access$4900(Lcom/squareup/checkout/CartItem;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->skipModifierDetailScreen(Z)Lcom/squareup/checkout/CartItem$Builder;

    .line 1332
    invoke-static {p1}, Lcom/squareup/checkout/CartItem;->access$5000(Lcom/squareup/checkout/CartItem;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->hasHiddenModifier(Z)Lcom/squareup/checkout/CartItem$Builder;

    .line 1333
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->variablePrice:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1334
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->overridePrice:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->overridePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1335
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->variations:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->variations(Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1336
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->events:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->events(Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1337
    invoke-static {p1}, Lcom/squareup/checkout/CartItem;->access$5100(Lcom/squareup/checkout/CartItem;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->isVoided(Z)Lcom/squareup/checkout/CartItem$Builder;

    .line 1338
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->destination:Lcom/squareup/checkout/OrderDestination;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->destination:Lcom/squareup/checkout/OrderDestination;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderDestination;->getSeats()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 1339
    :goto_0
    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    invoke-virtual {p0, v1, v0}, Lcom/squareup/checkout/CartItem$Builder;->featureDetails(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1340
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->destination:Lcom/squareup/checkout/OrderDestination;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->destination(Lcom/squareup/checkout/OrderDestination;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1341
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->courseId:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->courseId(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1342
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->itemOptions:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->itemOptions(Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1343
    iget-boolean v0, p1, Lcom/squareup/checkout/CartItem;->isEcomAvailable:Z

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->isEcomAvailable(Z)Lcom/squareup/checkout/CartItem$Builder;

    .line 1344
    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->merchantCatalogObjectToken:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->merchantCatalogObjectToken(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/checkout/CartItem;Lcom/squareup/checkout/CartItem$1;)V
    .locals 0

    .line 1231
    invoke-direct {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;-><init>(Lcom/squareup/checkout/CartItem;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/checkout/CartItem$Builder;Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 1231
    invoke-direct {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->itemizedAdjustmentAmountsFromTransactionsHistoryById(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Map;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedDiscounts:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Set;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->userEditedTaxIds:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/protos/client/IdPair;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->idPair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->isTaxedInTransactionsHistory:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Map;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->itemizedAdjustmentAmountsFromTransactionsHistoryById:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->itemAbbreviation:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->itemId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1700(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->itemName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1800(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->itemDescription:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1900(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/api/items/MenuCategory;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->category:Lcom/squareup/api/items/MenuCategory;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Set;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->pricingEngineAppliedDiscounts:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$2000(Lcom/squareup/checkout/CartItem$Builder;)Z
    .locals 0

    .line 1231
    iget-boolean p0, p0, Lcom/squareup/checkout/CartItem$Builder;->lockConfiguration:Z

    return p0
.end method

.method static synthetic access$2100(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/SortedMap;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->modifierLists:Ljava/util/SortedMap;

    return-object p0
.end method

.method static synthetic access$2200(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->notes:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2300(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->photoToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2400(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->photoUrl:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2500(Lcom/squareup/checkout/CartItem$Builder;)Ljava/math/BigDecimal;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->quantity:Ljava/math/BigDecimal;

    return-object p0
.end method

.method static synthetic access$2600(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    return-object p0
.end method

.method static synthetic access$2700(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/checkout/DiningOption;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    return-object p0
.end method

.method static synthetic access$2800(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/SortedMap;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedModifiers:Ljava/util/SortedMap;

    return-object p0
.end method

.method static synthetic access$2900(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Map;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedModifiers:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Set;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->blacklistedDiscounts:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$3000(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/checkout/OrderVariation;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    return-object p0
.end method

.method static synthetic access$3100(Lcom/squareup/checkout/CartItem$Builder;)Z
    .locals 0

    .line 1231
    iget-boolean p0, p0, Lcom/squareup/checkout/CartItem$Builder;->showVariationNameOverride:Z

    return p0
.end method

.method static synthetic access$3200(Lcom/squareup/checkout/CartItem$Builder;)Z
    .locals 0

    .line 1231
    iget-boolean p0, p0, Lcom/squareup/checkout/CartItem$Builder;->skipModifierDetailScreen:Z

    return p0
.end method

.method static synthetic access$3300(Lcom/squareup/checkout/CartItem$Builder;)Z
    .locals 0

    .line 1231
    iget-boolean p0, p0, Lcom/squareup/checkout/CartItem$Builder;->hasHiddenModifier:Z

    return p0
.end method

.method static synthetic access$3400(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->variablePrice:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method static synthetic access$3500(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->overridePrice:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method static synthetic access$3600(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    return-object p0
.end method

.method static synthetic access$3700(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/checkout/OrderDestination;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->destination:Lcom/squareup/checkout/OrderDestination;

    return-object p0
.end method

.method static synthetic access$3800(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->courseId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$3900(Lcom/squareup/checkout/CartItem$Builder;)Z
    .locals 0

    .line 1231
    iget-boolean p0, p0, Lcom/squareup/checkout/CartItem$Builder;->isEcomAvailable:Z

    return p0
.end method

.method static synthetic access$400(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Map;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$4000(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->merchantCatalogObjectToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$4100(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/List;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->variations:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$4200(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/List;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->itemOptions:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$4300(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/List;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->events:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$4400(Lcom/squareup/checkout/CartItem$Builder;)Z
    .locals 0

    .line 1231
    iget-boolean p0, p0, Lcom/squareup/checkout/CartItem$Builder;->isVoided:Z

    return p0
.end method

.method static synthetic access$500(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->color:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Date;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->createdAt:Ljava/util/Date;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Map;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->defaultTaxes:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Map;
    .locals 0

    .line 1231
    iget-object p0, p0, Lcom/squareup/checkout/CartItem$Builder;->ruleBasedTaxes:Ljava/util/Map;

    return-object p0
.end method

.method private courseId(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2222
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->courseId:Ljava/lang/String;

    return-object p0
.end method

.method private emptyAvailableOptions()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;
    .locals 3

    .line 1732
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method private hasItemVariationOrDisplayDetails(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 1722
    :cond_0
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    return v2

    .line 1725
    :cond_1
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    if-nez v1, :cond_2

    return v0

    .line 1728
    :cond_2
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;->item_variation:Lcom/squareup/api/items/ItemVariation;

    if-eqz p1, :cond_3

    const/4 v0, 0x1

    :cond_3
    return v0
.end method

.method private isTaxedInTransactionsHistory(Ljava/lang/Boolean;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 1996
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->isTaxedInTransactionsHistory:Ljava/lang/Boolean;

    return-object p0
.end method

.method private itemizedAdjustmentAmountsFromTransactionsHistoryById(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 2002
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->itemizedAdjustmentAmountsFromTransactionsHistoryById:Ljava/util/Map;

    return-object p0
.end method

.method private parseDiningOption(Lcom/squareup/protos/client/bills/Itemization;)V
    .locals 0

    .line 1871
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-nez p1, :cond_0

    return-void

    .line 1876
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    if-eqz p1, :cond_1

    .line 1878
    invoke-static {p1}, Lcom/squareup/checkout/DiningOption;->of(Lcom/squareup/protos/client/bills/DiningOptionLineItem;)Lcom/squareup/checkout/DiningOption;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    :cond_1
    return-void
.end method

.method private parseVariations(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;)V
    .locals 5

    .line 1739
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;->item_variation:Ljava/util/List;

    goto :goto_0

    .line 1741
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    .line 1743
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1744
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/items/ItemVariation;

    .line 1745
    invoke-interface {p3, p2, v2}, Lcom/squareup/checkout/OrderVariationNamer;->fromItemVariation(Lcom/squareup/util/Res;Lcom/squareup/api/items/ItemVariation;)Ljava/lang/String;

    move-result-object v3

    .line 1747
    invoke-static {p2, p1}, Lcom/squareup/quantity/UnitDisplayData;->fromItemization(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v4

    .line 1746
    invoke-static {v2, v3, v4}, Lcom/squareup/checkout/OrderVariation;->of(Lcom/squareup/api/items/ItemVariation;Ljava/lang/String;Lcom/squareup/quantity/UnitDisplayData;)Lcom/squareup/checkout/OrderVariation;

    move-result-object v2

    .line 1748
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1750
    :cond_1
    invoke-virtual {p0, v1}, Lcom/squareup/checkout/CartItem$Builder;->variations(Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1753
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_2
    const/4 v1, 0x0

    if-eqz v0, :cond_4

    .line 1759
    invoke-interface {p3, p2, v0}, Lcom/squareup/checkout/OrderVariationNamer;->fromItemVariationDetails(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;)Ljava/lang/String;

    move-result-object p3

    .line 1760
    invoke-static {p2, p1}, Lcom/squareup/quantity/UnitDisplayData;->fromItemization(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p2

    .line 1761
    invoke-static {v0, p3, p2}, Lcom/squareup/checkout/OrderVariation;->of(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;Ljava/lang/String;Lcom/squareup/quantity/UnitDisplayData;)Lcom/squareup/checkout/OrderVariation;

    move-result-object p2

    .line 1766
    iget-object p3, p1, Lcom/squareup/protos/client/bills/Itemization;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    invoke-virtual {p2, p3}, Lcom/squareup/checkout/OrderVariation;->setQuantityUnitOverride(Lcom/squareup/orders/model/Order$QuantityUnit;)V

    .line 1767
    invoke-virtual {p0, p2}, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation(Lcom/squareup/checkout/OrderVariation;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1769
    iget-object p2, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->display_variation_name:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    invoke-static {p2, p3}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/CartItem$Builder;->showVariationNameOverride(Z)Lcom/squareup/checkout/CartItem$Builder;

    goto :goto_3

    .line 1772
    :cond_4
    sget-object p2, Lcom/squareup/checkout/OrderVariation;->CUSTOM_ITEM_VARIATION:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation(Lcom/squareup/checkout/OrderVariation;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1773
    invoke-virtual {p0, v1}, Lcom/squareup/checkout/CartItem$Builder;->showVariationNameOverride(Z)Lcom/squareup/checkout/CartItem$Builder;

    .line 1776
    :goto_3
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    if-eqz p1, :cond_5

    .line 1777
    iget-object p2, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez p2, :cond_5

    .line 1778
    invoke-virtual {p1}, Lcom/squareup/protos/common/Money;->newBuilder()Lcom/squareup/protos/common/Money$Builder;

    move-result-object p1

    const-wide/16 p2, 0x0

    .line 1779
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/common/Money$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object p1

    .line 1780
    invoke-virtual {p1}, Lcom/squareup/protos/common/Money$Builder;->build()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 1784
    :cond_5
    iget-object p2, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {p2}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result p2

    if-eqz p2, :cond_6

    .line 1785
    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1790
    :cond_6
    iget-object p2, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {p2}, Lcom/squareup/checkout/OrderVariation;->isFixedPriced()Z

    move-result p2

    if-eqz p2, :cond_7

    iget-object p2, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 1791
    invoke-virtual {p2}, Lcom/squareup/checkout/OrderVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-static {p2, p1}, Lcom/squareup/money/MoneyMath;->isEqualNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p2

    if-nez p2, :cond_7

    .line 1792
    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->overridePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    :cond_7
    return-void
.end method


# virtual methods
.method public addAppliedDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 2

    .line 1895
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedDiscounts:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public addAppliedTax(Lcom/squareup/checkout/Tax;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 2

    .line 1935
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public addBlacklistedDiscount(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1

    .line 1924
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->blacklistedDiscounts:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1

    .line 2172
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->events:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPricingEngineAppliedDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 2

    .line 1900
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedDiscounts:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1901
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->pricingEngineAppliedDiscounts:Ljava/util/Set;

    iget-object p1, p1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addUserEditedTaxIds(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1

    .line 1981
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->userEditedTaxIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public appliedDiscounts(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 1883
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1884
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-object p0
.end method

.method public appliedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 1929
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1930
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-object p0
.end method

.method public backingDetails(Lcom/squareup/protos/client/bills/Itemization$BackingDetails;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 1945
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    return-object p0
.end method

.method public backingType(Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 1991
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    return-object p0
.end method

.method public blacklistedDiscounts(Ljava/util/Set;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 1918
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->blacklistedDiscounts:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1919
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->blacklistedDiscounts:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public blacklistedDiscountsFromFeatureDetails(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 2206
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->blacklisted_discount_ids:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2209
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    .line 2210
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->blacklisted_discount_ids:Ljava/util/List;

    .line 2211
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0, p1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->blacklistedDiscounts(Ljava/util/Set;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    return-object p1

    .line 2213
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->blacklistedDiscounts(Ljava/util/Set;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    return-object p1
.end method

.method public build()Lcom/squareup/checkout/CartItem;
    .locals 2

    .line 2234
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem$Builder;->ensureIdPair()Lcom/squareup/protos/client/IdPair;

    .line 2235
    new-instance v0, Lcom/squareup/checkout/CartItem;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/checkout/CartItem;-><init>(Lcom/squareup/checkout/CartItem$Builder;Lcom/squareup/checkout/CartItem$1;)V

    return-object v0
.end method

.method public category(Lcom/squareup/api/items/MenuCategory;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2039
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->category:Lcom/squareup/api/items/MenuCategory;

    return-object p0
.end method

.method public clearAppliedDiscounts()Lcom/squareup/checkout/CartItem$Builder;
    .locals 1

    .line 1912
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1913
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->pricingEngineAppliedDiscounts:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-object p0
.end method

.method public clearUserEditedTaxIds()Lcom/squareup/checkout/CartItem$Builder;
    .locals 1

    .line 1976
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->userEditedTaxIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-object p0
.end method

.method public color(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 1950
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->color:Ljava/lang/String;

    return-object p0
.end method

.method public createdAt(Ljava/util/Date;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 1955
    invoke-static {p1}, Lcom/squareup/util/Dates;->copy(Ljava/util/Date;)Ljava/util/Date;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->createdAt:Ljava/util/Date;

    return-object p0
.end method

.method public defaultTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 1965
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, p1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->defaultTaxes:Ljava/util/Map;

    return-object p0
.end method

.method public destination(Lcom/squareup/checkout/OrderDestination;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2217
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->destination:Lcom/squareup/checkout/OrderDestination;

    return-object p0
.end method

.method public ensureIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 3

    .line 2227
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->idPair:Lcom/squareup/protos/client/IdPair;

    if-nez v0, :cond_0

    .line 2228
    new-instance v0, Lcom/squareup/protos/client/IdPair;

    const/4 v1, 0x0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->idPair:Lcom/squareup/protos/client/IdPair;

    .line 2230
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->idPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public events(Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 2166
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->events:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2167
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->events:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public featureDetails(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 2193
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    if-eqz p1, :cond_1

    .line 2195
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    .line 2196
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->courseId(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 2198
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    if-eqz v0, :cond_1

    .line 2199
    invoke-static {p1, p2}, Lcom/squareup/checkout/OrderDestination;->fromFeatureDetails(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;Ljava/util/List;)Lcom/squareup/checkout/OrderDestination;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->destination(Lcom/squareup/checkout/OrderDestination;)Lcom/squareup/checkout/CartItem$Builder;

    :cond_1
    return-object p0
.end method

.method public fromCartItemization(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;ZLjava/util/List;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Itemization;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/checkout/OrderVariationNamer;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 1512
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/checkout/CartItem$Builder;->fromCartItemization(Lcom/squareup/protos/client/bills/Itemization;Ljava/util/Map;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;ZLjava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    return-object p1
.end method

.method public fromCartItemization(Lcom/squareup/protos/client/bills/Itemization;Ljava/util/Map;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;ZLjava/util/List;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Itemization;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/checkout/OrderVariationNamer;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 1526
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    if-nez v0, :cond_0

    .line 1528
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;-><init>()V

    .line 1529
    invoke-direct {p0}, Lcom/squareup/checkout/CartItem$Builder;->emptyAvailableOptions()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->available_options(Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    move-result-object v0

    .line 1530
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    move-result-object v0

    goto :goto_0

    .line 1531
    :cond_0
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    if-nez v1, :cond_1

    .line 1532
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    move-result-object v0

    .line 1533
    invoke-direct {p0}, Lcom/squareup/checkout/CartItem$Builder;->emptyAvailableOptions()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->available_options(Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    move-result-object v0

    .line 1534
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    move-result-object v0

    .line 1537
    :cond_1
    :goto_0
    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->backingDetails(Lcom/squareup/protos/client/bills/Itemization$BackingDetails;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1539
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->category:Lcom/squareup/api/items/MenuCategory;

    invoke-virtual {p0, v1}, Lcom/squareup/checkout/CartItem$Builder;->category(Lcom/squareup/api/items/MenuCategory;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1541
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    .line 1544
    iget-object v3, v1, Lcom/squareup/api/items/Item;->color:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/squareup/checkout/CartItem$Builder;->color(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1548
    iget-object v3, v1, Lcom/squareup/api/items/Item;->abbreviation:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/squareup/checkout/CartItem$Builder;->itemAbbreviation(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1549
    iget-object v3, v1, Lcom/squareup/api/items/Item;->name:Ljava/lang/String;

    .line 1550
    iget-object v4, v1, Lcom/squareup/api/items/Item;->type:Lcom/squareup/api/items/Item$Type;

    sget-object v5, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    if-ne v4, v5, :cond_3

    .line 1552
    invoke-static {v3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz p3, :cond_2

    .line 1553
    sget v3, Lcom/squareup/common/card/R$string;->gift_card:I

    invoke-interface {p3, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1556
    :cond_2
    iget-object v4, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->gift_card_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;->pan_suffix:Ljava/lang/String;

    const-string v5, " "

    invoke-static {v3, v5, v4}, Lcom/squareup/util/Strings;->ensureSuffix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1560
    :cond_3
    invoke-virtual {p0, v3}, Lcom/squareup/checkout/CartItem$Builder;->itemName(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1561
    iget-object v3, v1, Lcom/squareup/api/items/Item;->id:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/squareup/checkout/CartItem$Builder;->itemId(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1562
    iget-object v3, v1, Lcom/squareup/api/items/Item;->description:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/squareup/checkout/CartItem$Builder;->itemDescription(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1563
    iget-object v1, v1, Lcom/squareup/api/items/Item;->skips_modifier_screen:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/squareup/checkout/CartItem$Builder;->skipModifierDetailScreen(Z)Lcom/squareup/checkout/CartItem$Builder;

    goto :goto_1

    .line 1564
    :cond_4
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    if-eqz v1, :cond_5

    .line 1565
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->item:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->name:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/squareup/checkout/CartItem$Builder;->itemName(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1568
    :cond_5
    :goto_1
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item_image:Lcom/squareup/api/items/ItemImage;

    if-eqz v0, :cond_6

    .line 1570
    iget-object v0, v0, Lcom/squareup/api/items/ItemImage;->url:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->photoUrl(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1573
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->idPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1574
    new-instance v0, Ljava/math/BigDecimal;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1575
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->quantity_entry_type:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->MANUALLY_ENTERED:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->quantityEntryType(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1576
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->custom_note:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->notes(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1577
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-static {v0}, Lcom/squareup/util/ProtoDates;->tryParseIso8601Date(Lcom/squareup/protos/client/ISO8601Date;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->createdAt(Ljava/util/Date;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1579
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_AMOUNT:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    .line 1581
    invoke-direct {p0, p1, p3, p4}, Lcom/squareup/checkout/CartItem$Builder;->parseVariations(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;)V

    .line 1583
    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->parseModifiers(Lcom/squareup/protos/client/bills/Itemization;)V

    .line 1585
    invoke-direct {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->parseDiningOption(Lcom/squareup/protos/client/bills/Itemization;)V

    .line 1587
    iget-object p3, p1, Lcom/squareup/protos/client/bills/Itemization;->event:Ljava/util/List;

    invoke-virtual {p0, p3}, Lcom/squareup/checkout/CartItem$Builder;->events(Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1589
    iget-object p3, p1, Lcom/squareup/protos/client/bills/Itemization;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    invoke-virtual {p0, p3, p6}, Lcom/squareup/checkout/CartItem$Builder;->featureDetails(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1590
    iget-object p3, p1, Lcom/squareup/protos/client/bills/Itemization;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    invoke-virtual {p0, p3}, Lcom/squareup/checkout/CartItem$Builder;->blacklistedDiscountsFromFeatureDetails(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1593
    iget-object p3, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-nez p3, :cond_7

    return-object p0

    .line 1598
    :cond_7
    iget-object p3, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->fee:Ljava/util/List;

    .line 1599
    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result p4

    if-nez p4, :cond_c

    .line 1600
    new-instance p4, Ljava/util/HashMap;

    invoke-direct {p4}, Ljava/util/HashMap;-><init>()V

    .line 1601
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_2
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p6

    if-eqz p6, :cond_b

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p6

    check-cast p6, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 1604
    iget-object v0, p6, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    goto :goto_2

    .line 1606
    :cond_8
    iget-object v0, p6, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    if-nez v0, :cond_9

    goto :goto_2

    .line 1608
    :cond_9
    invoke-static {p6}, Lcom/squareup/checkout/Tax$Builder;->from(Lcom/squareup/protos/client/bills/FeeLineItem;)Lcom/squareup/checkout/Tax$Builder;

    move-result-object p6

    invoke-virtual {p6}, Lcom/squareup/checkout/Tax$Builder;->build()Lcom/squareup/checkout/Tax;

    move-result-object p6

    .line 1609
    iget-object v0, p6, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    .line 1611
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1612
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p6

    invoke-interface {p4, v0, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1614
    :cond_a
    invoke-interface {p4, v0, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1617
    :cond_b
    invoke-virtual {p0, p4}, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1622
    invoke-virtual {p0, p4}, Lcom/squareup/checkout/CartItem$Builder;->defaultTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1624
    invoke-virtual {p0, p4}, Lcom/squareup/checkout/CartItem$Builder;->ruleBasedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1628
    :cond_c
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    if-eqz p1, :cond_12

    .line 1630
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem$Builder;->clearAppliedDiscounts()Lcom/squareup/checkout/CartItem$Builder;

    .line 1631
    iget-object p2, p0, Lcom/squareup/checkout/CartItem$Builder;->pricingEngineAppliedDiscounts:Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Set;->clear()V

    if-eqz p5, :cond_e

    .line 1634
    new-instance p2, Lcom/squareup/checkout/-$$Lambda$CartItem$Builder$U6SEJSrX0HZhehtot8opx8SXQu4;

    invoke-direct {p2, p0}, Lcom/squareup/checkout/-$$Lambda$CartItem$Builder$U6SEJSrX0HZhehtot8opx8SXQu4;-><init>(Lcom/squareup/checkout/CartItem$Builder;)V

    invoke-static {p1, p2}, Lcom/squareup/checkout/Discounts;->deserializeToDiscounts(Ljava/util/List;Lkotlin/jvm/functions/Function0;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_12

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lkotlin/Pair;

    .line 1637
    invoke-virtual {p2}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/protos/client/bills/DiscountLineItem;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    sget-object p4, Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;->PRICING_RULE:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    if-ne p3, p4, :cond_d

    .line 1639
    invoke-virtual {p2}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/checkout/Discount;

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/CartItem$Builder;->addPricingEngineAppliedDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/checkout/CartItem$Builder;

    goto :goto_3

    .line 1641
    :cond_d
    invoke-virtual {p2}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/checkout/Discount;

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/CartItem$Builder;->addAppliedDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/checkout/CartItem$Builder;

    goto :goto_3

    .line 1645
    :cond_e
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_12

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 1648
    iget-object p3, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_deleted:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p4

    invoke-static {p3, p4}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    if-eqz p3, :cond_f

    goto :goto_4

    .line 1650
    :cond_f
    iget-object p3, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    if-nez p3, :cond_10

    goto :goto_4

    .line 1652
    :cond_10
    iget-object p3, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    sget-object p4, Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;->PRICING_RULE:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    if-ne p3, p4, :cond_11

    .line 1653
    iget-object p3, p0, Lcom/squareup/checkout/CartItem$Builder;->pricingEngineAppliedDiscounts:Ljava/util/Set;

    iget-object p4, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object p4, p4, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->discount:Lcom/squareup/api/items/Discount;

    iget-object p4, p4, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    invoke-interface {p3, p4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1656
    :cond_11
    new-instance p3, Lcom/squareup/checkout/Discount$Builder;

    iget-object p4, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 1658
    invoke-virtual {p4}, Lcom/squareup/checkout/OrderVariation;->getIdOrNull()Ljava/lang/String;

    move-result-object p4

    invoke-direct {p3, p2, p4, v2}, Lcom/squareup/checkout/Discount$Builder;-><init>(Lcom/squareup/protos/client/bills/DiscountLineItem;Ljava/lang/String;Z)V

    .line 1660
    invoke-virtual {p3}, Lcom/squareup/checkout/Discount$Builder;->build()Lcom/squareup/checkout/Discount;

    move-result-object p2

    .line 1661
    invoke-virtual {p0, p2}, Lcom/squareup/checkout/CartItem$Builder;->addAppliedDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/checkout/CartItem$Builder;

    goto :goto_4

    :cond_12
    return-object p0
.end method

.method public fromItemizationForReturn(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;ZLjava/util/List;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Itemization;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/checkout/OrderVariationNamer;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 1678
    invoke-virtual/range {p0 .. p5}, Lcom/squareup/checkout/CartItem$Builder;->fromCartItemization(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;ZLjava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1687
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1688
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-nez p1, :cond_0

    return-object p0

    .line 1693
    :cond_0
    iget-object p3, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    if-eqz p3, :cond_2

    .line 1695
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_1
    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_2

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 1696
    iget-object p5, p4, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    if-eqz p5, :cond_1

    .line 1697
    iget-object p5, p4, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object p5, p5, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    iget-object p4, p4, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object p4, p4, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object p4, p4, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-interface {p2, p5, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1703
    :cond_2
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->fee:Ljava/util/List;

    if-eqz p1, :cond_4

    .line 1705
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 1706
    iget-object p4, p3, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    if-eqz p4, :cond_3

    .line 1707
    iget-object p4, p3, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iget-object p4, p4, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object p3, p3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-interface {p2, p4, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1713
    :cond_4
    invoke-direct {p0, p2}, Lcom/squareup/checkout/CartItem$Builder;->itemizedAdjustmentAmountsFromTransactionsHistoryById(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    return-object p0
.end method

.method public fromItemizationHistory(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Itemization;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/checkout/OrderVariationNamer;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 1372
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->idPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1374
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_0

    .line 1375
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v0, v0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->createdAt(Ljava/util/Date;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1377
    :cond_0
    new-instance v0, Ljava/math/BigDecimal;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1378
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->quantity_entry_type:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->MANUALLY_ENTERED:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->quantityEntryType(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1379
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->custom_note:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->notes(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1380
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    if-eqz v0, :cond_1

    .line 1381
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->item:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->cogs_object_id:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->itemId(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1384
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Amounts;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1386
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    .line 1388
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->category:Lcom/squareup/api/items/MenuCategory;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->category(Lcom/squareup/api/items/MenuCategory;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1390
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    invoke-virtual {p0, v0, p4}, Lcom/squareup/checkout/CartItem$Builder;->featureDetails(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1391
    iget-object p4, p1, Lcom/squareup/protos/client/bills/Itemization;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    invoke-virtual {p0, p4}, Lcom/squareup/checkout/CartItem$Builder;->blacklistedDiscountsFromFeatureDetails(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1393
    iget-object p4, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object p4, p4, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-eqz p4, :cond_b

    .line 1395
    iget-object v0, p4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->modifier_option:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    .line 1397
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    .line 1398
    new-instance v3, Ljava/util/TreeMap;

    invoke-direct {v3}, Ljava/util/TreeMap;-><init>()V

    const/4 v4, 0x0

    .line 1400
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 1401
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    .line 1402
    invoke-static {v5}, Lcom/squareup/checkout/OrderModifier;->fromItemizationHistory(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)Lcom/squareup/checkout/OrderModifier;

    move-result-object v5

    .line 1403
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v6, v5}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1406
    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0, v3}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1407
    invoke-virtual {p0, v2}, Lcom/squareup/checkout/CartItem$Builder;->selectedModifiers(Ljava/util/SortedMap;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1409
    :cond_3
    iget-object v0, p4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->fee:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1410
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1411
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 1412
    iget-object v5, v4, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    invoke-static {v5}, Lcom/squareup/checkout/Tax$Builder;->from(Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;)Lcom/squareup/checkout/Tax$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/checkout/Tax$Builder;->build()Lcom/squareup/checkout/Tax;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/squareup/checkout/CartItem$Builder;->addAppliedTax(Lcom/squareup/checkout/Tax;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1413
    iget-object v5, v4, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iget-object v5, v5, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object v4, v4, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1416
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x1

    if-lez v0, :cond_5

    const/4 v1, 0x1

    :cond_5
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->isTaxedInTransactionsHistory(Ljava/lang/Boolean;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1422
    iget-object v0, p4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    .line 1423
    invoke-direct {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->hasItemVariationOrDisplayDetails(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1424
    invoke-interface {p3, p2, v0}, Lcom/squareup/checkout/OrderVariationNamer;->fromItemVariationDetails(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;)Ljava/lang/String;

    move-result-object p3

    .line 1425
    invoke-static {p2, p1}, Lcom/squareup/quantity/UnitDisplayData;->fromItemization(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v1

    .line 1426
    invoke-static {v0, p3, v1}, Lcom/squareup/checkout/OrderVariation;->of(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;Ljava/lang/String;Lcom/squareup/quantity/UnitDisplayData;)Lcom/squareup/checkout/OrderVariation;

    move-result-object p3

    .line 1431
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    invoke-virtual {p3, v1}, Lcom/squareup/checkout/OrderVariation;->setQuantityUnitOverride(Lcom/squareup/orders/model/Order$QuantityUnit;)V

    .line 1432
    invoke-virtual {p0, p3}, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation(Lcom/squareup/checkout/OrderVariation;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1436
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Amounts;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    .line 1437
    invoke-virtual {p3}, Lcom/squareup/checkout/OrderVariation;->isFixedPriced()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1438
    invoke-virtual {p3}, Lcom/squareup/checkout/OrderVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/money/MoneyMath;->isEqualNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p3

    if-nez p3, :cond_6

    .line 1439
    invoke-virtual {p0, v1}, Lcom/squareup/checkout/CartItem$Builder;->overridePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1442
    :cond_6
    iget-object p3, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->display_variation_name:Ljava/lang/Boolean;

    if-eqz p3, :cond_7

    .line 1443
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    if-eqz p3, :cond_7

    .line 1447
    invoke-virtual {p0, v3}, Lcom/squareup/checkout/CartItem$Builder;->showVariationNameOverride(Z)Lcom/squareup/checkout/CartItem$Builder;

    .line 1451
    :cond_7
    iget-object p3, p4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    if-eqz p3, :cond_8

    .line 1452
    iget-object p3, p4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    invoke-static {p3}, Lcom/squareup/checkout/DiningOption;->of(Lcom/squareup/protos/client/bills/DiningOptionLineItem;)Lcom/squareup/checkout/DiningOption;

    move-result-object p3

    .line 1453
    invoke-virtual {p0, p3}, Lcom/squareup/checkout/CartItem$Builder;->selectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1456
    :cond_8
    iget-object p3, p4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    if-eqz p3, :cond_a

    .line 1457
    iget-object p3, p4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    .line 1458
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_9
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_a

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 1459
    iget-object v0, p4, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    if-eqz v0, :cond_9

    iget-object v0, p4, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    sget-object v1, Lcom/squareup/api/items/Discount$ApplicationMethod;->COMP:Lcom/squareup/api/items/Discount$ApplicationMethod;

    if-ne v0, v1, :cond_9

    .line 1462
    new-instance p3, Lcom/squareup/checkout/Discount$Builder;

    iget-object v0, p4, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    invoke-direct {p3, v0}, Lcom/squareup/checkout/Discount$Builder;-><init>(Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;)V

    .line 1463
    invoke-virtual {p3}, Lcom/squareup/checkout/Discount$Builder;->build()Lcom/squareup/checkout/Discount;

    move-result-object p3

    .line 1462
    invoke-virtual {p0, p3}, Lcom/squareup/checkout/CartItem$Builder;->addAppliedDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1465
    iget-object p3, p4, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    iget-object p4, p4, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object p4, p4, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object p4, p4, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-interface {v2, p3, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1472
    :cond_a
    invoke-direct {p0, v2}, Lcom/squareup/checkout/CartItem$Builder;->itemizedAdjustmentAmountsFromTransactionsHistoryById(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1475
    :cond_b
    iget-object p3, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    if-eqz p3, :cond_e

    iget-object p3, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->item:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    if-eqz p3, :cond_e

    .line 1479
    iget-object p3, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->item:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->name:Ljava/lang/String;

    .line 1480
    iget-object p4, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    if-eqz p4, :cond_d

    invoke-virtual {p4}, Lcom/squareup/checkout/OrderVariation;->getGiftCardDetails()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    move-result-object p4

    if-eqz p4, :cond_d

    .line 1482
    invoke-static {p3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p4

    if-eqz p4, :cond_c

    if-eqz p2, :cond_c

    .line 1483
    sget p3, Lcom/squareup/common/card/R$string;->gift_card:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 1486
    :cond_c
    iget-object p2, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 1487
    invoke-virtual {p2}, Lcom/squareup/checkout/OrderVariation;->getGiftCardDetails()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;->pan_suffix:Ljava/lang/String;

    const-string p4, " "

    .line 1486
    invoke-static {p3, p4, p2}, Lcom/squareup/util/Strings;->ensureSuffix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 1490
    :cond_d
    invoke-virtual {p0, p3}, Lcom/squareup/checkout/CartItem$Builder;->itemName(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1491
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->item:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->description:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/CartItem$Builder;->itemDescription(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1492
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->item:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->color:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/CartItem$Builder;->color(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1493
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->item:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->image_url:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/CartItem$Builder;->photoUrl(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1496
    :cond_e
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->event:Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->events(Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    return-object p0
.end method

.method public hasHiddenModifier(Z)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2145
    iput-boolean p1, p0, Lcom/squareup/checkout/CartItem$Builder;->hasHiddenModifier:Z

    return-object p0
.end method

.method public idPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 1986
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->idPair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public isEcomAvailable(Z)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2087
    iput-boolean p1, p0, Lcom/squareup/checkout/CartItem$Builder;->isEcomAvailable:Z

    return-object p0
.end method

.method public isVoided(Z)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2188
    iput-boolean p1, p0, Lcom/squareup/checkout/CartItem$Builder;->isVoided:Z

    return-object p0
.end method

.method public itemAbbreviation(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2008
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->itemAbbreviation:Ljava/lang/String;

    return-object p0
.end method

.method public itemDescription(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2027
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->itemDescription:Ljava/lang/String;

    return-object p0
.end method

.method public itemId(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2013
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->itemId:Ljava/lang/String;

    .line 2014
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    sget-object p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->ITEM_VARIATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    :cond_0
    return-object p0
.end method

.method public itemName(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1

    .line 2019
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->itemName:Ljava/lang/String;

    .line 2020
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->itemAbbreviation:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2021
    invoke-static {p1}, Lcom/squareup/util/Strings;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->itemAbbreviation(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    :cond_0
    return-object p0
.end method

.method public itemOptions(Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 2033
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->itemOptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-object p0
.end method

.method public synthetic lambda$fromCartItemization$0$CartItem$Builder()Ljava/lang/String;
    .locals 1

    .line 1635
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getIdOrNull()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public lockConfiguration(Z)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2044
    iput-boolean p1, p0, Lcom/squareup/checkout/CartItem$Builder;->lockConfiguration:Z

    return-object p0
.end method

.method public merchantCatalogObjectToken(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2092
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->merchantCatalogObjectToken:Ljava/lang/String;

    return-object p0
.end method

.method public modifierLists(Ljava/util/SortedMap;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifierList;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    const-string v0, "modifierLists"

    .line 2049
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/SortedMap;

    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->modifierLists:Ljava/util/SortedMap;

    return-object p0
.end method

.method public notes(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2054
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->notes:Ljava/lang/String;

    return-object p0
.end method

.method public of(Lcom/squareup/server/payment/Itemization;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1

    .line 1286
    iget-object v0, p1, Lcom/squareup/server/payment/Itemization;->color:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->color(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1287
    iget-object v0, p1, Lcom/squareup/server/payment/Itemization;->item_id:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->itemId(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1288
    invoke-virtual {p1}, Lcom/squareup/server/payment/Itemization;->getItemName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->itemName(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1289
    invoke-virtual {p1}, Lcom/squareup/server/payment/Itemization;->getNotes()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->notes(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1290
    invoke-virtual {p1}, Lcom/squareup/server/payment/Itemization;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->photoUrl(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1291
    invoke-virtual {p1}, Lcom/squareup/server/payment/Itemization;->getItemQuantity()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1292
    invoke-virtual {p1}, Lcom/squareup/server/payment/Itemization;->getPriceMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1293
    iget-object v0, p1, Lcom/squareup/server/payment/Itemization;->item_variation_id:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1294
    invoke-static {p1}, Lcom/squareup/checkout/OrderVariation;->ofFixedForReadOnly(Lcom/squareup/server/payment/Itemization;)Lcom/squareup/checkout/OrderVariation;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation(Lcom/squareup/checkout/OrderVariation;)Lcom/squareup/checkout/CartItem$Builder;

    :cond_0
    return-object p0
.end method

.method public of(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 1349
    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->color(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1350
    invoke-virtual {p0, p2}, Lcom/squareup/checkout/CartItem$Builder;->itemAbbreviation(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1351
    invoke-virtual {p0, p3}, Lcom/squareup/checkout/CartItem$Builder;->itemId(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1352
    invoke-virtual {p0, p4}, Lcom/squareup/checkout/CartItem$Builder;->itemName(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1353
    invoke-virtual {p0, p5}, Lcom/squareup/checkout/CartItem$Builder;->itemDescription(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1354
    invoke-virtual {p0, p6}, Lcom/squareup/checkout/CartItem$Builder;->photoUrl(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    return-object p0
.end method

.method public overridePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2155
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->overridePrice:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public parseModifiers(Lcom/squareup/protos/client/bills/Itemization;)V
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 1799
    iget-object v2, v0, Lcom/squareup/checkout/CartItem$Builder;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    if-eqz v2, :cond_0

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/checkout/CartItem$Builder;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;->modifier_list:Ljava/util/List;

    goto :goto_0

    .line 1802
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 1805
    :goto_0
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    if-eqz v2, :cond_4

    .line 1808
    new-instance v6, Ljava/util/TreeMap;

    invoke-direct {v6}, Ljava/util/TreeMap;-><init>()V

    .line 1810
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/16 v17, 0x0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;

    .line 1811
    iget-object v15, v7, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    .line 1812
    new-instance v13, Ljava/util/TreeMap;

    invoke-direct {v13}, Ljava/util/TreeMap;-><init>()V

    .line 1814
    iget-object v7, v7, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;->modifier_option:Ljava/util/List;

    const/4 v8, 0x0

    .line 1815
    :goto_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    if-ge v8, v9, :cond_1

    .line 1816
    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/api/items/ItemModifierOption;

    .line 1817
    new-instance v10, Ljava/math/BigDecimal;

    iget-object v11, v1, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    invoke-direct {v10, v11}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 1818
    invoke-static {v9, v15, v10}, Lcom/squareup/checkout/OrderModifier;->fromTicketCart(Lcom/squareup/api/items/ItemModifierOption;Lcom/squareup/api/items/ItemModifierList;Ljava/math/BigDecimal;)Lcom/squareup/checkout/OrderModifier;

    move-result-object v9

    .line 1820
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v13, v10, v9}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 1823
    :cond_1
    iget-object v7, v15, Lcom/squareup/api/items/ItemModifierList;->selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    sget-object v8, Lcom/squareup/api/items/ItemModifierList$SelectionType;->MULTIPLE:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    if-ne v7, v8, :cond_2

    const/4 v12, 0x1

    goto :goto_3

    :cond_2
    const/4 v12, 0x0

    .line 1824
    :goto_3
    new-instance v14, Lcom/squareup/checkout/OrderModifierList;

    iget-object v8, v15, Lcom/squareup/api/items/ItemModifierList;->id:Ljava/lang/String;

    iget-object v9, v15, Lcom/squareup/api/items/ItemModifierList;->name:Ljava/lang/String;

    const/16 v16, 0x0

    sget-object v7, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_MIN_SELECTED_MODIFIERS:Ljava/lang/Integer;

    .line 1826
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v18

    sget-object v7, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_MAX_SELECTED_MODIFIERS:Ljava/lang/Integer;

    .line 1827
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v19

    move-object v7, v14

    move/from16 v10, v17

    move-object v11, v15

    move-object v5, v14

    move/from16 v14, v16

    move-object v4, v15

    move/from16 v15, v18

    move/from16 v16, v19

    invoke-direct/range {v7 .. v16}, Lcom/squareup/checkout/OrderModifierList;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/api/items/ItemModifierList;ZLjava/util/SortedMap;ZII)V

    .line 1828
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7, v5}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1830
    iget-object v4, v4, Lcom/squareup/api/items/ItemModifierList;->id:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v4, 0x1

    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    :cond_3
    const/4 v4, 0x1

    .line 1833
    invoke-virtual {v0, v6}, Lcom/squareup/checkout/CartItem$Builder;->modifierLists(Ljava/util/SortedMap;)Lcom/squareup/checkout/CartItem$Builder;

    goto :goto_4

    :cond_4
    const/4 v4, 0x1

    .line 1836
    :goto_4
    iget-object v2, v1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-eqz v2, :cond_5

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->modifier_option:Ljava/util/List;

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    if-eqz v1, :cond_9

    .line 1841
    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 1842
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 1845
    :goto_6
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v5, v7, :cond_8

    .line 1846
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    .line 1847
    invoke-static {v7}, Lcom/squareup/checkout/OrderModifier;->fromTicketCart(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)Lcom/squareup/checkout/OrderModifier;

    move-result-object v8

    .line 1850
    iget-object v7, v7, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    iget-object v7, v7, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    iget-object v7, v7, Lcom/squareup/api/items/ItemModifierOption;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    iget-object v7, v7, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    .line 1852
    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 1853
    invoke-interface {v2}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 1854
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    new-instance v10, Ljava/util/TreeMap;

    invoke-direct {v10}, Ljava/util/TreeMap;-><init>()V

    invoke-interface {v2, v9, v10}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1858
    :cond_6
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/SortedMap;

    .line 1859
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v7, v9, v8}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1861
    invoke-virtual {v8}, Lcom/squareup/checkout/OrderModifier;->getHideFromCustomer()Z

    move-result v7

    if-eqz v7, :cond_7

    const/4 v6, 0x1

    :cond_7
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 1865
    :cond_8
    invoke-virtual {v0, v2}, Lcom/squareup/checkout/CartItem$Builder;->selectedModifiers(Ljava/util/SortedMap;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1866
    invoke-virtual {v0, v6}, Lcom/squareup/checkout/CartItem$Builder;->hasHiddenModifier(Z)Lcom/squareup/checkout/CartItem$Builder;

    :cond_9
    return-void
.end method

.method public photoToken(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2059
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->photoToken:Ljava/lang/String;

    return-object p0
.end method

.method public photoUrl(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2064
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->photoUrl:Ljava/lang/String;

    return-object p0
.end method

.method public pricingEngineAppliedDiscounts(Ljava/util/Set;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 1889
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->pricingEngineAppliedDiscounts:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1890
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->pricingEngineAppliedDiscounts:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public quantity(Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2069
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->quantity:Ljava/math/BigDecimal;

    return-object p0
.end method

.method public quantityEntryType(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2074
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    return-object p0
.end method

.method public removeAppliedDiscount(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1

    .line 1906
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1907
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->pricingEngineAppliedDiscounts:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public removeAppliedTax(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1

    .line 1940
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public removeDiscountAddEvents(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 4

    const/4 v0, 0x0

    .line 2177
    :goto_0
    iget-object v1, p0, Lcom/squareup/checkout/CartItem$Builder;->events:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2178
    iget-object v1, p0, Lcom/squareup/checkout/CartItem$Builder;->events:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Itemization$Event;

    .line 2179
    iget-object v2, v1, Lcom/squareup/protos/client/bills/Itemization$Event;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    sget-object v3, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->DISCOUNT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    if-ne v2, v3, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Event;->reason:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2180
    iget-object v1, p0, Lcom/squareup/checkout/CartItem$Builder;->events:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object p0
.end method

.method public ruleBasedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 1960
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, p1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->ruleBasedTaxes:Ljava/util/Map;

    return-object p0
.end method

.method public selectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2082
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    return-object p0
.end method

.method public selectedModifiers(Ljava/util/SortedMap;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;>;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 2098
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    .line 2100
    invoke-interface {p1}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object p1

    .line 2102
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2103
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2104
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    if-eqz v2, :cond_0

    .line 2106
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2107
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    new-instance v3, Ljava/util/TreeMap;

    invoke-direct {v3, v2}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0, v1, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    const-string p1, "selectedModifiers"

    .line 2112
    invoke-static {v0, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/SortedMap;

    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedModifiers:Ljava/util/SortedMap;

    .line 2113
    iget-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedModifiers:Ljava/util/SortedMap;

    invoke-static {p1}, Lcom/squareup/checkout/ModifiersKt;->asAppliedModifiers(Ljava/util/SortedMap;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->appliedModifiers:Ljava/util/Map;

    return-object p0
.end method

.method public selectedVariation(Lcom/squareup/checkout/OrderVariation;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1

    const-string v0, "selectedVariation"

    .line 2118
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/OrderVariation;

    iput-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 2119
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 2120
    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    :cond_0
    return-object p0
.end method

.method public showVariationNameOverride(Z)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2135
    iput-boolean p1, p0, Lcom/squareup/checkout/CartItem$Builder;->showVariationNameOverride:Z

    return-object p0
.end method

.method public skipModifierDetailScreen(Z)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2140
    iput-boolean p1, p0, Lcom/squareup/checkout/CartItem$Builder;->skipModifierDetailScreen:Z

    return-object p0
.end method

.method public unsafeSelectedVariation(Lcom/squareup/checkout/OrderVariation;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    if-eqz p1, :cond_0

    .line 2128
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result p1

    if-nez p1, :cond_1

    :cond_0
    const/4 p1, 0x0

    .line 2129
    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    :cond_1
    return-object p0
.end method

.method public userEditedTaxIds(Ljava/util/Set;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 1970
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->userEditedTaxIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1971
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->userEditedTaxIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 0

    .line 2150
    iput-object p1, p0, Lcom/squareup/checkout/CartItem$Builder;->variablePrice:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public variations(Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/OrderVariation;",
            ">;)",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 2160
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2161
    iget-object v0, p0, Lcom/squareup/checkout/CartItem$Builder;->variations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method
