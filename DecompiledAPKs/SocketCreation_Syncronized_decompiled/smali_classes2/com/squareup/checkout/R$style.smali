.class public final Lcom/squareup/checkout/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final TextAppearance_BuyerFacing_LargeTitle:I = 0x7f13020c

.field public static final TextAppearance_BuyerFacing_SmallSubitle:I = 0x7f13020d

.field public static final TextAppearance_BuyerFacing_SmallTitle:I = 0x7f13020e

.field public static final Theme_BuyerFacing:I = 0x7f1302e7

.field public static final Theme_BuyerFacing_LargeTitle:I = 0x7f1302e8

.field public static final Theme_BuyerFacing_SmallTitle:I = 0x7f1302e9

.field public static final Widget_BuyerFacing_PrimaryTileButton:I = 0x7f1303c6

.field public static final Widget_BuyerFacing_PrimaryTileButton_LargeTitle:I = 0x7f1303c7

.field public static final Widget_BuyerFacing_PrimaryTileButton_SmallTitle:I = 0x7f1303c8

.field public static final Widget_BuyerFacing_SecondaryBarButton:I = 0x7f1303c9

.field public static final Widget_PreservedLayoutView:I = 0x7f1305b1


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
