.class public final Lcom/squareup/catalogfees/RealCatalogFeesPreloader_Factory;
.super Ljava/lang/Object;
.source "RealCatalogFeesPreloader_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/catalogfees/RealCatalogFeesPreloader;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final eventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader_Factory;->eventSinkProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/catalogfees/RealCatalogFeesPreloader_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)",
            "Lcom/squareup/catalogfees/RealCatalogFeesPreloader_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/catalogfees/RealCatalogFeesPreloader_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/catalogfees/RealCatalogFeesPreloader;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ")",
            "Lcom/squareup/catalogfees/RealCatalogFeesPreloader;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/catalogfees/RealCatalogFeesPreloader;-><init>(Ljavax/inject/Provider;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/AccountStatusSettings;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/catalogfees/RealCatalogFeesPreloader;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader_Factory;->cogsProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader_Factory;->eventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/badbus/BadEventSink;

    iget-object v2, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {v0, v1, v2}, Lcom/squareup/catalogfees/RealCatalogFeesPreloader_Factory;->newInstance(Ljavax/inject/Provider;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/catalogfees/RealCatalogFeesPreloader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/catalogfees/RealCatalogFeesPreloader_Factory;->get()Lcom/squareup/catalogfees/RealCatalogFeesPreloader;

    move-result-object v0

    return-object v0
.end method
