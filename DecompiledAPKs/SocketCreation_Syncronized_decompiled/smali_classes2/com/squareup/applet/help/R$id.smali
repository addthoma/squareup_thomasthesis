.class public final Lcom/squareup/applet/help/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/applet/help/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final activation_content:I = 0x7f0a014f

.field public static final announcements_list_animator:I = 0x7f0a01c3

.field public static final applet_sections_list:I = 0x7f0a01c7

.field public static final button1:I = 0x7f0a0273

.field public static final button2:I = 0x7f0a0274

.field public static final divider:I = 0x7f0a05e3

.field public static final email_support_ledger_row:I = 0x7f0a06ce

.field public static final empty:I = 0x7f0a06f2

.field public static final empty_view:I = 0x7f0a06f6

.field public static final fragment_container:I = 0x7f0a0771

.field public static final frequently_asked_questions_content:I = 0x7f0a0776

.field public static final frequently_asked_questions_header:I = 0x7f0a0777

.field public static final history_action_button:I = 0x7f0a07db

.field public static final image:I = 0x7f0a0821

.field public static final image_reader:I = 0x7f0a0824

.field public static final jedi_panel:I = 0x7f0a08f6

.field public static final learn_more_content:I = 0x7f0a091a

.field public static final learn_more_header:I = 0x7f0a091b

.field public static final libraries:I = 0x7f0a0928

.field public static final libraries_row:I = 0x7f0a0929

.field public static final license_group_header:I = 0x7f0a0935

.field public static final license_group_list:I = 0x7f0a0936

.field public static final licenses_text:I = 0x7f0a0937

.field public static final list:I = 0x7f0a0949

.field public static final message:I = 0x7f0a09d3

.field public static final messages_layout:I = 0x7f0a09d9

.field public static final notification_support_messages:I = 0x7f0a0a70

.field public static final ntep_certification_number:I = 0x7f0a0a72

.field public static final ntep_company:I = 0x7f0a0a73

.field public static final ntep_model:I = 0x7f0a0a74

.field public static final ntep_row:I = 0x7f0a0a75

.field public static final ntep_version:I = 0x7f0a0a76

.field public static final order_count:I = 0x7f0a0ac8

.field public static final order_date:I = 0x7f0a0ac9

.field public static final order_history_row:I = 0x7f0a0ad0

.field public static final order_legacy_reader_row:I = 0x7f0a0ad1

.field public static final order_list:I = 0x7f0a0ad2

.field public static final order_message:I = 0x7f0a0ad4

.field public static final order_more:I = 0x7f0a0ad7

.field public static final order_number:I = 0x7f0a0adc

.field public static final order_reader:I = 0x7f0a0add

.field public static final order_reader_all_hardware:I = 0x7f0a0ade

.field public static final order_reader_contactless:I = 0x7f0a0adf

.field public static final order_reader_magstripe:I = 0x7f0a0ae0

.field public static final order_reader_row:I = 0x7f0a0ae1

.field public static final order_status:I = 0x7f0a0aec

.field public static final order_total:I = 0x7f0a0af2

.field public static final order_track:I = 0x7f0a0af3

.field public static final privacy_policy_row:I = 0x7f0a0c78

.field public static final seller_agreement_row:I = 0x7f0a0e5c

.field public static final send_diagnostic_row:I = 0x7f0a0e65

.field public static final spoc_version_row:I = 0x7f0a0ef5

.field public static final title:I = 0x7f0a103f

.field public static final troubleshooting_content:I = 0x7f0a108c

.field public static final troubleshooting_header:I = 0x7f0a108d

.field public static final troubleshooting_progress_bar:I = 0x7f0a108e

.field public static final tutorials_content:I = 0x7f0a10a9

.field public static final upload_support_ledger_row:I = 0x7f0a10d3

.field public static final version_row:I = 0x7f0a10eb


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
