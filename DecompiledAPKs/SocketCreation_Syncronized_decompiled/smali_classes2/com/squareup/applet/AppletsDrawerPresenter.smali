.class public Lcom/squareup/applet/AppletsDrawerPresenter;
.super Lmortar/ViewPresenter;
.source "AppletsDrawerPresenter.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/applet/AppletsDrawerPresenter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/applet/AppletsDrawerLayout;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAppletsDrawerPresenter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AppletsDrawerPresenter.kt\ncom/squareup/applet/AppletsDrawerPresenter\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,254:1\n1103#2,7:255\n*E\n*S KotlinDebug\n*F\n+ 1 AppletsDrawerPresenter.kt\ncom/squareup/applet/AppletsDrawerPresenter\n*L\n82#1,7:255\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\u0008\u0017\u0018\u0000 /2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001/B=\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0006\u0010\u001a\u001a\u00020\u001bJ\u0010\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001d\u001a\u00020\u0012H\u0002J\u0006\u0010\u001e\u001a\u00020\u001bJ\u000e\u0010\u001f\u001a\u00020\u001b2\u0006\u0010 \u001a\u00020\u0002J\u0012\u0010!\u001a\u00020\u001b2\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\u000e\u0010$\u001a\u00020\u001b2\u0006\u0010%\u001a\u00020&J\u0008\u0010\'\u001a\u00020\u001bH\u0002J\u0010\u0010(\u001a\u00020\u001b2\u0006\u0010)\u001a\u00020\u0015H\u0016J\u0006\u0010*\u001a\u00020\u001bJ\u0010\u0010+\u001a\u00020\u001b2\u0006\u0010,\u001a\u00020\u0015H\u0002J\u0010\u0010-\u001a\u00020\u001b2\u0006\u0010.\u001a\u00020\u0015H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0013\u001a\u0010\u0012\u000c\u0012\n \u0016*\u0004\u0018\u00010\u00150\u00150\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0017\u001a\u0010\u0012\u000c\u0012\n \u0016*\u0004\u0018\u00010\u00150\u00150\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0018\u001a\u00020\u00158F\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0019R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/applet/AppletsDrawerPresenter;",
        "Lmortar/ViewPresenter;",
        "Lcom/squareup/applet/AppletsDrawerLayout;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "applets",
        "Lcom/squareup/applet/Applets;",
        "lazyMainContainer",
        "Ldagger/Lazy;",
        "Lcom/squareup/ui/main/PosContainer;",
        "appletSelection",
        "Lcom/squareup/applet/AppletSelection;",
        "appletsDrawerRunner",
        "Lcom/squareup/applet/AppletsDrawerRunner;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/analytics/Analytics;Lcom/squareup/applet/Applets;Ldagger/Lazy;Lcom/squareup/applet/AppletSelection;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/settings/server/Features;)V",
        "",
        "Lcom/squareup/applet/Applet;",
        "drawerIsReady",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "drawerLocked",
        "isDrawerOpen",
        "()Z",
        "closeDrawer",
        "",
        "itemSelected",
        "applet",
        "onDrawerClosed",
        "onDrawerOpened",
        "view",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onShowScreen",
        "screen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "openDrawer",
        "setHomeScreenIsEditing",
        "homeScreenIsEditing",
        "toggleDrawer",
        "updateDrawer",
        "locked",
        "updateDrawerLockState",
        "shouldLockDrawer",
        "Companion",
        "pos-container_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final APPLET_SWITCHER_CLICK_EVENT_NAME:Ljava/lang/String; = "Sales Navigation"

.field public static final Companion:Lcom/squareup/applet/AppletsDrawerPresenter$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final appletSelection:Lcom/squareup/applet/AppletSelection;

.field private final applets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/applet/Applet;",
            ">;"
        }
    .end annotation
.end field

.field private final appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

.field private final drawerIsReady:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final drawerLocked:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final lazyMainContainer:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/applet/AppletsDrawerPresenter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/applet/AppletsDrawerPresenter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/applet/AppletsDrawerPresenter;->Companion:Lcom/squareup/applet/AppletsDrawerPresenter$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/applet/Applets;Ldagger/Lazy;Lcom/squareup/applet/AppletSelection;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/applet/Applets;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/applet/AppletSelection;",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "applets"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyMainContainer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appletSelection"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appletsDrawerRunner"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p3, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->lazyMainContainer:Ldagger/Lazy;

    iput-object p4, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->appletSelection:Lcom/squareup/applet/AppletSelection;

    iput-object p5, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

    iput-object p6, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->features:Lcom/squareup/settings/server/Features;

    const/4 p1, 0x0

    .line 54
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p3

    const-string p4, "BehaviorRelay.createDefault(false)"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p3, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->drawerLocked:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 55
    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->drawerIsReady:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 61
    invoke-interface {p2}, Lcom/squareup/applet/Applets;->getApplets()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->applets:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$getAppletSelection$p(Lcom/squareup/applet/AppletsDrawerPresenter;)Lcom/squareup/applet/AppletSelection;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->appletSelection:Lcom/squareup/applet/AppletSelection;

    return-object p0
.end method

.method public static final synthetic access$getApplets$p(Lcom/squareup/applet/AppletsDrawerPresenter;)Ljava/util/List;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->applets:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getAppletsDrawerRunner$p(Lcom/squareup/applet/AppletsDrawerPresenter;)Lcom/squareup/applet/AppletsDrawerRunner;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

    return-object p0
.end method

.method public static final synthetic access$getDrawerIsReady$p(Lcom/squareup/applet/AppletsDrawerPresenter;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->drawerIsReady:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getDrawerLocked$p(Lcom/squareup/applet/AppletsDrawerPresenter;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->drawerLocked:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getFeatures$p(Lcom/squareup/applet/AppletsDrawerPresenter;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method public static final synthetic access$getLazyMainContainer$p(Lcom/squareup/applet/AppletsDrawerPresenter;)Ldagger/Lazy;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->lazyMainContainer:Ldagger/Lazy;

    return-object p0
.end method

.method public static final synthetic access$itemSelected(Lcom/squareup/applet/AppletsDrawerPresenter;Lcom/squareup/applet/Applet;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/applet/AppletsDrawerPresenter;->itemSelected(Lcom/squareup/applet/Applet;)V

    return-void
.end method

.method public static final synthetic access$updateDrawer(Lcom/squareup/applet/AppletsDrawerPresenter;Z)V
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/applet/AppletsDrawerPresenter;->updateDrawer(Z)V

    return-void
.end method

.method private final itemSelected(Lcom/squareup/applet/Applet;)V
    .locals 4

    .line 167
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletsDrawerLayout;

    .line 169
    iget-object v1, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->applets:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/applet/AppletsDrawerLayout;->setSelectedItem(I)V

    .line 171
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter;->isDrawerOpen()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter;->closeDrawer()V

    :cond_0
    const-string/jumbo v1, "view"

    .line 175
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/applet/AppletsDrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "view.resources"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/squareup/applet/Applet;->getText(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/applet/AppletsDrawerLayout;->announceAppletNameForAccessibility(Ljava/lang/String;)V

    .line 177
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/v1/DetailEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->APPLET_SWITCHER_SELECTED_APPLET:Lcom/squareup/analytics/RegisterActionName;

    invoke-virtual {p1}, Lcom/squareup/applet/Applet;->getAnalyticsName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/analytics/event/v1/DetailEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 178
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sales Navigation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/applet/Applet;->getAnalyticsName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method private final openDrawer()V
    .locals 4

    .line 239
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletsDrawerLayout;

    if-eqz v0, :cond_1

    const/4 v1, 0x3

    .line 244
    invoke-virtual {v0, v1}, Lcom/squareup/applet/AppletsDrawerLayout;->getDrawerLockMode(I)I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 245
    invoke-virtual {v0, v1}, Lcom/squareup/applet/AppletsDrawerLayout;->openDrawer(I)V

    :cond_0
    return-void

    .line 240
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "not attached to view"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final updateDrawer(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 219
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter;->closeDrawer()V

    .line 220
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/applet/AppletsDrawerLayout;

    invoke-virtual {p1}, Lcom/squareup/applet/AppletsDrawerLayout;->lockDrawer()V

    goto :goto_0

    .line 222
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/applet/AppletsDrawerLayout;

    invoke-virtual {p1}, Lcom/squareup/applet/AppletsDrawerLayout;->unlockDrawer()V

    :goto_0
    return-void
.end method

.method private final updateDrawerLockState(Z)V
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->drawerLocked:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final closeDrawer()V
    .locals 2

    .line 235
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletsDrawerLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/squareup/applet/AppletsDrawerLayout;->closeDrawer(I)V

    return-void
.end method

.method public final isDrawerOpen()Z
    .locals 2

    .line 58
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletsDrawerLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/squareup/applet/AppletsDrawerLayout;->isDrawerOpen(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final onDrawerClosed()V
    .locals 2

    .line 204
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->APPLET_SWITCHER_DID_CLOSE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 205
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

    sget-object v1, Lcom/squareup/applet/AppletsDrawerRunner$DrawerState;->CLOSED:Lcom/squareup/applet/AppletsDrawerRunner$DrawerState;

    invoke-virtual {v0, v1}, Lcom/squareup/applet/AppletsDrawerRunner;->drawerStateChanged(Lcom/squareup/applet/AppletsDrawerRunner$DrawerState;)V

    return-void
.end method

.method public final onDrawerOpened(Lcom/squareup/applet/AppletsDrawerLayout;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->APPLET_SWITCHER_DID_OPEN:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 199
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 200
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

    sget-object v0, Lcom/squareup/applet/AppletsDrawerRunner$DrawerState;->OPEN:Lcom/squareup/applet/AppletsDrawerRunner$DrawerState;

    invoke-virtual {p1, v0}, Lcom/squareup/applet/AppletsDrawerRunner;->drawerStateChanged(Lcom/squareup/applet/AppletsDrawerRunner$DrawerState;)V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 65
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/applet/AppletsDrawerLayout;

    .line 68
    invoke-virtual {p1}, Lcom/squareup/applet/AppletsDrawerLayout;->clearDrawer()V

    .line 72
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter;->applets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const-string/jumbo v2, "view"

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/applet/Applet;

    .line 73
    invoke-virtual {v1}, Lcom/squareup/applet/Applet;->isNotificationCenterApplet()Z

    move-result v3

    invoke-virtual {p1, v3}, Lcom/squareup/applet/AppletsDrawerLayout;->addItem(Z)Lcom/squareup/applet/AppletsDrawerItem;

    move-result-object v3

    const-string/jumbo v4, "view.addItem(applet.isNotificationCenterApplet)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/applet/AppletsDrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string/jumbo v4, "view.resources"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/squareup/applet/Applet;->getText(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v3, v2}, Lcom/squareup/applet/AppletsDrawerItem;->setTitle(Ljava/lang/CharSequence;)V

    .line 77
    invoke-virtual {v1}, Lcom/squareup/applet/Applet;->getAppletId()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 79
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/squareup/applet/AppletsDrawerItem;->setId(I)V

    .line 82
    :cond_0
    move-object v2, v3

    check-cast v2, Landroid/view/View;

    .line 255
    new-instance v4, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$$inlined$onClickDebounced$1;

    invoke-direct {v4, v3, p1, v1}, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/applet/AppletsDrawerItem;Lcom/squareup/applet/AppletsDrawerLayout;Lcom/squareup/applet/Applet;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    move-object v2, p1

    check-cast v2, Landroid/view/View;

    new-instance v4, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;

    invoke-direct {v4, p0, v1, v3}, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;-><init>(Lcom/squareup/applet/AppletsDrawerPresenter;Lcom/squareup/applet/Applet;Lcom/squareup/applet/AppletsDrawerItem;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    invoke-static {v2, v4}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 122
    new-instance v4, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$3;

    invoke-direct {v4, v1, v3}, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$3;-><init>(Lcom/squareup/applet/Applet;Lcom/squareup/applet/AppletsDrawerItem;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    invoke-static {v2, v4}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 134
    :cond_1
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$4;-><init>(Lcom/squareup/applet/AppletsDrawerPresenter;Lcom/squareup/applet/AppletsDrawerLayout;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 149
    new-instance p1, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$5;

    invoke-direct {p1, p0}, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$5;-><init>(Lcom/squareup/applet/AppletsDrawerPresenter;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 154
    new-instance p1, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$6;

    invoke-direct {p1, p0}, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$6;-><init>(Lcom/squareup/applet/AppletsDrawerPresenter;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 160
    new-instance p1, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$7;

    invoke-direct {p1, p0}, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$7;-><init>(Lcom/squareup/applet/AppletsDrawerPresenter;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final onShowScreen(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    const-class v0, Lcom/squareup/container/layer/CardScreen;

    invoke-static {p1, v0}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 192
    const-class v0, Lcom/squareup/container/layer/FullSheet;

    invoke-static {p1, v0}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 193
    const-class v0, Lcom/squareup/container/spot/ModalBodyScreen;

    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 194
    :goto_1
    invoke-direct {p0, p1}, Lcom/squareup/applet/AppletsDrawerPresenter;->updateDrawerLockState(Z)V

    return-void
.end method

.method public setHomeScreenIsEditing(Z)V
    .locals 0

    .line 186
    invoke-direct {p0, p1}, Lcom/squareup/applet/AppletsDrawerPresenter;->updateDrawerLockState(Z)V

    return-void
.end method

.method public final toggleDrawer()V
    .locals 2

    .line 227
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletsDrawerLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/squareup/applet/AppletsDrawerLayout;->isDrawerOpen(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter;->closeDrawer()V

    goto :goto_0

    .line 230
    :cond_0
    invoke-direct {p0}, Lcom/squareup/applet/AppletsDrawerPresenter;->openDrawer()V

    :goto_0
    return-void
.end method
