.class public final Lcom/squareup/applet/EmptyApplet;
.super Lcom/squareup/applet/Applet;
.source "EmptyApplet.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0010\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0008\u0010\u000c\u001a\u00020\u0008H\u0016R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/applet/EmptyApplet;",
        "Lcom/squareup/applet/Applet;",
        "()V",
        "analyticsName",
        "",
        "getAnalyticsName",
        "()Ljava/lang/String;",
        "activate",
        "",
        "getText",
        "resources",
        "Landroid/content/res/Resources;",
        "pushActivationScreen",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/applet/EmptyApplet;

# The value of this static final field might be set in the static constructor
.field private static final analyticsName:Ljava/lang/String; = ""


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/applet/EmptyApplet;

    invoke-direct {v0}, Lcom/squareup/applet/EmptyApplet;-><init>()V

    sput-object v0, Lcom/squareup/applet/EmptyApplet;->INSTANCE:Lcom/squareup/applet/EmptyApplet;

    const-string v0, ""

    .line 10
    sput-object v0, Lcom/squareup/applet/EmptyApplet;->analyticsName:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/applet/Applet;-><init>()V

    return-void
.end method


# virtual methods
.method public activate()V
    .locals 0

    return-void
.end method

.method public getAnalyticsName()Ljava/lang/String;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/applet/EmptyApplet;->analyticsName:Ljava/lang/String;

    return-object v0
.end method

.method public getText(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, ""

    return-object p1
.end method

.method public pushActivationScreen()V
    .locals 0

    return-void
.end method
