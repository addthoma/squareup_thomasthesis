.class Lcom/squareup/applet/AppletsDrawerLayout$1;
.super Landroidx/drawerlayout/widget/DrawerLayout$SimpleDrawerListener;
.source "AppletsDrawerLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/applet/AppletsDrawerLayout;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/applet/AppletsDrawerLayout;


# direct methods
.method constructor <init>(Lcom/squareup/applet/AppletsDrawerLayout;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerLayout$1;->this$0:Lcom/squareup/applet/AppletsDrawerLayout;

    invoke-direct {p0}, Landroidx/drawerlayout/widget/DrawerLayout$SimpleDrawerListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrawerClosed(Landroid/view/View;)V
    .locals 0

    .line 60
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerLayout$1;->this$0:Lcom/squareup/applet/AppletsDrawerLayout;

    iget-object p1, p1, Lcom/squareup/applet/AppletsDrawerLayout;->appletsDrawerPresenter:Lcom/squareup/applet/AppletsDrawerPresenter;

    invoke-virtual {p1}, Lcom/squareup/applet/AppletsDrawerPresenter;->onDrawerClosed()V

    return-void
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 1

    .line 56
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerLayout$1;->this$0:Lcom/squareup/applet/AppletsDrawerLayout;

    iget-object p1, p1, Lcom/squareup/applet/AppletsDrawerLayout;->appletsDrawerPresenter:Lcom/squareup/applet/AppletsDrawerPresenter;

    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerLayout$1;->this$0:Lcom/squareup/applet/AppletsDrawerLayout;

    invoke-virtual {p1, v0}, Lcom/squareup/applet/AppletsDrawerPresenter;->onDrawerOpened(Lcom/squareup/applet/AppletsDrawerLayout;)V

    return-void
.end method
