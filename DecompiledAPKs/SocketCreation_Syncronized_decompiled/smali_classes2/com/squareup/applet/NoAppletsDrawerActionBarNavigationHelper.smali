.class public final Lcom/squareup/applet/NoAppletsDrawerActionBarNavigationHelper;
.super Ljava/lang/Object;
.source "NoAppletsDrawerActionBarNavigationHelper.kt"

# interfaces
.implements Lcom/squareup/applet/ActionBarNavigationHelper;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J \u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/applet/NoAppletsDrawerActionBarNavigationHelper;",
        "Lcom/squareup/applet/ActionBarNavigationHelper;",
        "()V",
        "hasDrawerButton",
        "",
        "getHasDrawerButton",
        "()Z",
        "makeActionBarForAppletActivationScreen",
        "",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "name",
        "",
        "applet",
        "Lcom/squareup/applet/Applet;",
        "pos-container_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final hasDrawerButton:Z


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getHasDrawerButton()Z
    .locals 1

    .line 10
    iget-boolean v0, p0, Lcom/squareup/applet/NoAppletsDrawerActionBarNavigationHelper;->hasDrawerButton:Z

    return v0
.end method

.method public makeActionBarForAppletActivationScreen(Lcom/squareup/marin/widgets/MarinActionBar;Ljava/lang/String;Lcom/squareup/applet/Applet;)V
    .locals 1

    const-string v0, "actionBar"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "applet"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p3

    .line 23
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p3, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setTitleNoUpButton(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 24
    invoke-virtual {p3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
