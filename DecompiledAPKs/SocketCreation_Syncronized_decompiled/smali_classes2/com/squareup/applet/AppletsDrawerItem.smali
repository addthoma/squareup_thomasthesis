.class public Lcom/squareup/applet/AppletsDrawerItem;
.super Landroid/widget/LinearLayout;
.source "AppletsDrawerItem.java"


# instance fields
.field private final badge:Lcom/squareup/marketfont/MarketTextView;

.field private final title:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 22
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 23
    sget v0, Lcom/squareup/pos/container/R$layout;->applets_drawer_item:I

    invoke-static {p1, v0, p0}, Lcom/squareup/applet/AppletsDrawerItem;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 25
    sget p1, Lcom/squareup/pos/container/R$id;->applet_title:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerItem;->title:Lcom/squareup/marketfont/MarketTextView;

    .line 26
    sget p1, Lcom/squareup/pos/container/R$id;->applet_badge:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method private clearBadgeHeight()V
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    .line 82
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 83
    iget-object v1, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1, v0}, Lcom/squareup/marketfont/MarketTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private setBadgeHeight(I)V
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 73
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 74
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private static textAppearanceFor(Lcom/squareup/applet/Applet$Badge$Priority;)I
    .locals 1

    .line 87
    sget-object v0, Lcom/squareup/applet/AppletsDrawerItem$1;->$SwitchMap$com$squareup$applet$Applet$Badge$Priority:[I

    invoke-virtual {p0}, Lcom/squareup/applet/Applet$Badge$Priority;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    .line 90
    sget p0, Lcom/squareup/pos/container/R$style;->TextAppearance_Badge_Drawer:I

    return p0

    .line 93
    :cond_0
    sget p0, Lcom/squareup/pos/container/R$style;->TextAppearance_Badge_Drawer_High:I

    return p0
.end method


# virtual methods
.method hideBadge()V
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    return-void
.end method

.method public onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    .line 115
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 118
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result p1

    return p1
.end method

.method public sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .line 103
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    return-void

    .line 106
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerItem;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showBadge(Ljava/lang/CharSequence;Lcom/squareup/applet/Applet$Badge$Priority;)V
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 39
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 41
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerItem;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p2}, Lcom/squareup/applet/AppletsDrawerItem;->textAppearanceFor(Lcom/squareup/applet/Applet$Badge$Priority;)I

    move-result p2

    invoke-virtual {p1, v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 42
    invoke-direct {p0}, Lcom/squareup/applet/AppletsDrawerItem;->clearBadgeHeight()V

    return-void
.end method

.method showNotificationCenterBadge(Ljava/lang/CharSequence;)V
    .locals 3

    .line 46
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 47
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerItem;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/pos/container/R$style;->TextAppearance_Badge_Drawer_NotificationCenter:I

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 48
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_0

    const-string v0, ""

    .line 49
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 50
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    sget v0, Lcom/squareup/pos/container/R$drawable;->icon_blue_dot_24:I

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setBackgroundResource(I)V

    .line 51
    invoke-direct {p0}, Lcom/squareup/applet/AppletsDrawerItem;->clearBadgeHeight()V

    goto :goto_0

    .line 55
    :cond_0
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    sget v0, Lcom/squareup/pos/container/R$drawable;->icon_blue_dot_8:I

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setBackgroundResource(I)V

    .line 56
    sget p1, Lcom/squareup/pos/container/R$dimen;->applet_low_priority_badge_height:I

    invoke-direct {p0, p1}, Lcom/squareup/applet/AppletsDrawerItem;->setBadgeHeight(I)V

    :goto_0
    return-void
.end method

.method showNotificationCenterWarningBadge()V
    .locals 3

    .line 61
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 62
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/pos/container/R$drawable;->icon_triangle_warning_24:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setBackgroundResource(I)V

    .line 64
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerItem;->badge:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerItem;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/pos/container/R$style;->TextAppearance_Badge_Drawer_NotificationCenter:I

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 65
    invoke-direct {p0}, Lcom/squareup/applet/AppletsDrawerItem;->clearBadgeHeight()V

    return-void
.end method
