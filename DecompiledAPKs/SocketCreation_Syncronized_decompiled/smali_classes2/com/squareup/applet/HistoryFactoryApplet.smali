.class public abstract Lcom/squareup/applet/HistoryFactoryApplet;
.super Lcom/squareup/applet/Applet;
.source "HistoryFactoryApplet.java"

# interfaces
.implements Lcom/squareup/ui/main/HistoryFactory;


# instance fields
.field protected final appletEntryPoint:Lcom/squareup/applet/AppletEntryPoint;

.field protected final container:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ldagger/Lazy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Lcom/squareup/applet/Applet;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/applet/HistoryFactoryApplet;->container:Ldagger/Lazy;

    const/4 p1, 0x0

    .line 43
    iput-object p1, p0, Lcom/squareup/applet/HistoryFactoryApplet;->appletEntryPoint:Lcom/squareup/applet/AppletEntryPoint;

    return-void
.end method

.method protected constructor <init>(Ldagger/Lazy;Lcom/squareup/applet/AppletEntryPoint;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/applet/AppletEntryPoint;",
            ")V"
        }
    .end annotation

    .line 47
    invoke-direct {p0}, Lcom/squareup/applet/Applet;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/applet/HistoryFactoryApplet;->container:Ldagger/Lazy;

    .line 49
    iput-object p2, p0, Lcom/squareup/applet/HistoryFactoryApplet;->appletEntryPoint:Lcom/squareup/applet/AppletEntryPoint;

    return-void
.end method

.method private static reverse(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List<",
            "TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 126
    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public final activate()V
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/applet/HistoryFactoryApplet;->container:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-interface {v0, p0, v1}, Lcom/squareup/ui/main/PosContainer;->resetHistory(Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V

    return-void
.end method

.method public final createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 1

    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 116
    invoke-static {p1, v0}, Lcom/squareup/ui/main/HomeKt;->getHomeHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;

    move-result-object p1

    goto :goto_0

    .line 117
    :cond_0
    invoke-interface {p1, p2}, Lcom/squareup/ui/main/Home;->getHomeScreens(Lflow/History;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/applet/HistoryFactoryApplet;->reverse(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/squareup/container/Histories;->pushStack(Lflow/History;Ljava/util/List;)Lkotlin/Pair;

    move-result-object p1

    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/History;

    .line 121
    :goto_0
    invoke-virtual {p0, p2}, Lcom/squareup/applet/HistoryFactoryApplet;->getHomeScreens(Lflow/History;)Ljava/util/List;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/applet/HistoryFactoryApplet;->reverse(Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/container/Histories;->pushStack(Lflow/History;Ljava/util/List;)Lkotlin/Pair;

    move-result-object p1

    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/History;

    return-object p1
.end method

.method protected abstract getHomeScreens(Lflow/History;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation
.end method

.method public getIntentScreenExtra()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 131
    iget-object v0, p0, Lcom/squareup/applet/HistoryFactoryApplet;->appletEntryPoint:Lcom/squareup/applet/AppletEntryPoint;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 134
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 135
    iget-object v2, p0, Lcom/squareup/applet/HistoryFactoryApplet;->appletEntryPoint:Lcom/squareup/applet/AppletEntryPoint;

    invoke-virtual {v2}, Lcom/squareup/applet/AppletEntryPoint;->getAllSections()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/applet/AppletSection;

    .line 136
    invoke-virtual {v3}, Lcom/squareup/applet/AppletSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object v3

    .line 137
    invoke-virtual {v3}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 138
    invoke-virtual {v3}, Lcom/squareup/applet/SectionAccess;->getPermissions()Ljava/util/Set;

    move-result-object v3

    .line 139
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    return-object v1

    .line 144
    :cond_2
    invoke-interface {v0, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 147
    :cond_3
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    goto :goto_1

    :cond_4
    move-object v0, v1

    :goto_1
    return-object v0
.end method

.method public final pushActivationScreen()V
    .locals 3

    .line 84
    new-instance v0, Lcom/squareup/applet/HistoryFactoryApplet$1;

    invoke-direct {v0, p0}, Lcom/squareup/applet/HistoryFactoryApplet$1;-><init>(Lcom/squareup/applet/HistoryFactoryApplet;)V

    .line 102
    iget-object v1, p0, Lcom/squareup/applet/HistoryFactoryApplet;->container:Ldagger/Lazy;

    invoke-interface {v1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/PosContainer;

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-interface {v1, v0, v2}, Lcom/squareup/ui/main/PosContainer;->resetHistory(Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V

    return-void
.end method
