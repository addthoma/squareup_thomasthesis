.class public final Lcom/squareup/applet/AppletsDrawerPresenter_Factory;
.super Ljava/lang/Object;
.source "AppletsDrawerPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/applet/AppletsDrawerPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/applet/AppletsDrawerPresenter_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/applet/AppletsDrawerPresenter_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/applet/AppletsDrawerPresenter_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/applet/AppletsDrawerPresenter_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p6, p0, Lcom/squareup/applet/AppletsDrawerPresenter_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/applet/AppletsDrawerPresenter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/applet/AppletsDrawerPresenter_Factory;"
        }
    .end annotation

    .line 50
    new-instance v7, Lcom/squareup/applet/AppletsDrawerPresenter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/applet/AppletsDrawerPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/applet/Applets;Ldagger/Lazy;Lcom/squareup/applet/AppletSelection;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/settings/server/Features;)Lcom/squareup/applet/AppletsDrawerPresenter;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/applet/Applets;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/applet/AppletSelection;",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/applet/AppletsDrawerPresenter;"
        }
    .end annotation

    .line 55
    new-instance v7, Lcom/squareup/applet/AppletsDrawerPresenter;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/applet/AppletsDrawerPresenter;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/applet/Applets;Ldagger/Lazy;Lcom/squareup/applet/AppletSelection;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/settings/server/Features;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/applet/AppletsDrawerPresenter;
    .locals 7

    .line 43
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/applet/Applets;

    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/applet/AppletSelection;

    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/applet/AppletsDrawerRunner;

    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v6}, Lcom/squareup/applet/AppletsDrawerPresenter_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/applet/Applets;Ldagger/Lazy;Lcom/squareup/applet/AppletSelection;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/settings/server/Features;)Lcom/squareup/applet/AppletsDrawerPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter_Factory;->get()Lcom/squareup/applet/AppletsDrawerPresenter;

    move-result-object v0

    return-object v0
.end method
