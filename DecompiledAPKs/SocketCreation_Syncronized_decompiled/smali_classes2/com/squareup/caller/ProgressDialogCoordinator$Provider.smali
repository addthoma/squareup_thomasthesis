.class public Lcom/squareup/caller/ProgressDialogCoordinator$Provider;
.super Ljava/lang/Object;
.source "ProgressDialogCoordinator.java"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/caller/ProgressDialogCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Provider"
.end annotation


# instance fields
.field private final mainScheduler:Lio/reactivex/Scheduler;
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation
.end field

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 0
    .param p1    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p2    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/squareup/caller/ProgressDialogCoordinator$Provider;->mainScheduler:Lio/reactivex/Scheduler;

    .line 69
    iput-object p2, p0, Lcom/squareup/caller/ProgressDialogCoordinator$Provider;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method


# virtual methods
.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/caller/ProgressDialogCoordinator;
    .locals 4

    .line 73
    new-instance v0, Lcom/squareup/caller/ProgressDialogCoordinator;

    iget-object v1, p0, Lcom/squareup/caller/ProgressDialogCoordinator$Provider;->mainScheduler:Lio/reactivex/Scheduler;

    iget-object v2, p0, Lcom/squareup/caller/ProgressDialogCoordinator$Provider;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    const/4 v3, 0x0

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/squareup/caller/ProgressDialogCoordinator;-><init>(Landroid/view/View;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/caller/ProgressDialogCoordinator$1;)V

    return-object v0
.end method

.method public bridge synthetic provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 0

    .line 61
    invoke-virtual {p0, p1}, Lcom/squareup/caller/ProgressDialogCoordinator$Provider;->provideCoordinator(Landroid/view/View;)Lcom/squareup/caller/ProgressDialogCoordinator;

    move-result-object p1

    return-object p1
.end method
