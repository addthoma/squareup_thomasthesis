.class public Lcom/squareup/caller/BlockingPopup;
.super Ljava/lang/Object;
.source "BlockingPopup.java"

# interfaces
.implements Lcom/squareup/mortar/Popup;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/mortar/Popup<",
        "Lcom/squareup/ui/Showing;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private dialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/caller/BlockingPopup;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/caller/BlockingPopup;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/squareup/ui/DialogDestroyer;->get(Landroid/content/Context;)Lcom/squareup/ui/DialogDestroyer;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/caller/BlockingPopup;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/DialogDestroyer;->dismiss(Landroid/app/Dialog;)V

    const/4 v0, 0x0

    .line 46
    iput-object v0, p0, Lcom/squareup/caller/BlockingPopup;->dialog:Landroid/app/Dialog;

    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/caller/BlockingPopup;->context:Landroid/content/Context;

    return-object v0
.end method

.method public isShowing()Z
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/caller/BlockingPopup;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public bridge synthetic show(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/ui/Showing;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/caller/BlockingPopup;->show(Lcom/squareup/ui/Showing;ZLcom/squareup/mortar/PopupPresenter;)V

    return-void
.end method

.method public show(Lcom/squareup/ui/Showing;ZLcom/squareup/mortar/PopupPresenter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/Showing;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/Showing;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 29
    iget-object p1, p0, Lcom/squareup/caller/BlockingPopup;->dialog:Landroid/app/Dialog;

    if-nez p1, :cond_0

    .line 30
    new-instance p1, Lcom/squareup/dialog/GlassDialog;

    iget-object p2, p0, Lcom/squareup/caller/BlockingPopup;->context:Landroid/content/Context;

    sget p3, Lcom/squareup/noho/R$style;->Theme_Noho_Dialog_NoBackground_NoDim:I

    invoke-direct {p1, p2, p3}, Lcom/squareup/dialog/GlassDialog;-><init>(Landroid/content/Context;I)V

    iput-object p1, p0, Lcom/squareup/caller/BlockingPopup;->dialog:Landroid/app/Dialog;

    .line 32
    iget-object p1, p0, Lcom/squareup/caller/BlockingPopup;->dialog:Landroid/app/Dialog;

    invoke-static {}, Lcom/squareup/util/Dialogs;->ignoresSearchKeyListener()Landroid/content/DialogInterface$OnKeyListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 33
    iget-object p1, p0, Lcom/squareup/caller/BlockingPopup;->dialog:Landroid/app/Dialog;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 34
    iget-object p1, p0, Lcom/squareup/caller/BlockingPopup;->dialog:Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/4 p2, -0x1

    invoke-virtual {p1, p2, p2}, Landroid/view/Window;->setLayout(II)V

    .line 37
    :cond_0
    iget-object p1, p0, Lcom/squareup/caller/BlockingPopup;->dialog:Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->isShowing()Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/squareup/caller/BlockingPopup;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/squareup/ui/DialogDestroyer;->get(Landroid/content/Context;)Lcom/squareup/ui/DialogDestroyer;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/caller/BlockingPopup;->dialog:Landroid/app/Dialog;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/DialogDestroyer;->show(Landroid/app/Dialog;)V

    :cond_1
    return-void
.end method
