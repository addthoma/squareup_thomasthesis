.class public final Lcom/squareup/balance/applet/impl/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/applet/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final add_amount_to_account:I = 0x7f0d0049

.field public static final balance_applet_list_row:I = 0x7f0d007d

.field public static final balance_applet_master_view:I = 0x7f0d007e

.field public static final balance_header_button_layout:I = 0x7f0d007f

.field public static final balance_header_view:I = 0x7f0d0080

.field public static final balance_spinner:I = 0x7f0d0081

.field public static final balance_transaction_detail_view:I = 0x7f0d0082

.field public static final balance_transactions_view:I = 0x7f0d0083

.field public static final confirm_transfer_view:I = 0x7f0d0105

.field public static final date_row:I = 0x7f0d01b8

.field public static final empty_card_activity:I = 0x7f0d0250

.field public static final error_balance:I = 0x7f0d0261

.field public static final error_card_activity:I = 0x7f0d0262

.field public static final recent_activity_row:I = 0x7f0d0473

.field public static final square_card_instant_deposit_result_view:I = 0x7f0d04f6

.field public static final square_card_instant_deposit_view:I = 0x7f0d04f7

.field public static final transaction_load_more:I = 0x7f0d055f

.field public static final transaction_load_more_error:I = 0x7f0d0560

.field public static final transfer_result_view:I = 0x7f0d056f

.field public static final transfer_to_bank_view:I = 0x7f0d0570


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
