.class public final Lcom/squareup/balance/core/server/bizbank/BizbankServiceMainModule_ProvideRealBizbankServiceFactory;
.super Ljava/lang/Object;
.source "BizbankServiceMainModule_ProvideRealBizbankServiceFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        ">;"
    }
.end annotation


# instance fields
.field private final serviceCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/balance/core/server/bizbank/BizbankServiceMainModule_ProvideRealBizbankServiceFactory;->serviceCreatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/balance/core/server/bizbank/BizbankServiceMainModule_ProvideRealBizbankServiceFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;)",
            "Lcom/squareup/balance/core/server/bizbank/BizbankServiceMainModule_ProvideRealBizbankServiceFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/balance/core/server/bizbank/BizbankServiceMainModule_ProvideRealBizbankServiceFactory;

    invoke-direct {v0, p0}, Lcom/squareup/balance/core/server/bizbank/BizbankServiceMainModule_ProvideRealBizbankServiceFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideRealBizbankService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/balance/core/server/bizbank/BizbankService;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/balance/core/server/bizbank/BizbankServiceMainModule;->provideRealBizbankService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/balance/core/server/bizbank/BizbankService;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/balance/core/server/bizbank/BizbankService;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/balance/core/server/bizbank/BizbankService;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/balance/core/server/bizbank/BizbankServiceMainModule_ProvideRealBizbankServiceFactory;->serviceCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ServiceCreator;

    invoke-static {v0}, Lcom/squareup/balance/core/server/bizbank/BizbankServiceMainModule_ProvideRealBizbankServiceFactory;->provideRealBizbankService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/balance/core/server/bizbank/BizbankService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/balance/core/server/bizbank/BizbankServiceMainModule_ProvideRealBizbankServiceFactory;->get()Lcom/squareup/balance/core/server/bizbank/BizbankService;

    move-result-object v0

    return-object v0
.end method
