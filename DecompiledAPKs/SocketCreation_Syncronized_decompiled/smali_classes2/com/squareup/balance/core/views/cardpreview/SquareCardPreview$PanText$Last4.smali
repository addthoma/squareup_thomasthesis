.class public final Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;
.super Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText;
.source "SquareCardPreview.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Last4"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardPreview.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardPreview.kt\ncom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4\n*L\n1#1,448:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u000f\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0015\u0010\u0008\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\r\u0010\r\u001a\u00020\u0003H\u0010\u00a2\u0006\u0002\u0008\u000eJ\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\t\u0010\u0011\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;",
        "Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText;",
        "last4Pan",
        "",
        "(Ljava/lang/String;)V",
        "getLast4Pan",
        "()Ljava/lang/String;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "evaluatePan",
        "evaluatePan$public_release",
        "hashCode",
        "",
        "toString",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final last4Pan:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 415
    invoke-direct {p0, v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;->last4Pan:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;->last4Pan:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;->copy(Ljava/lang/String;)Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;->last4Pan:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;)Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;
    .locals 1

    new-instance v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;

    invoke-direct {v0, p1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;

    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;->last4Pan:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;->last4Pan:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public evaluatePan$public_release()Ljava/lang/String;
    .locals 3

    .line 417
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;->last4Pan:Ljava/lang/String;

    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_2

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_3

    goto :goto_3

    :cond_3
    invoke-static {}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreviewKt;->access$getOBFUSCATED_PAN_BLOCK$p()Ljava/lang/String;

    move-result-object v0

    .line 418
    :goto_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreviewKt;->access$getPAN_PREFIX$p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getLast4Pan()Ljava/lang/String;
    .locals 1

    .line 415
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;->last4Pan:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;->last4Pan:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Last4(last4Pan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;->last4Pan:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
