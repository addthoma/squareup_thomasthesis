.class final Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$singleNewCardData$1;
.super Ljava/lang/Object;
.source "RealSquareCardDataRequester.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->singleNewCardData(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "card",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $currentCardData:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;


# direct methods
.method constructor <init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$singleNewCardData$1;->$currentCardData:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final test(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Z
    .locals 1

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$singleNewCardData$1;->$currentCardData:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iget-object v0, v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-eq v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$singleNewCardData$1;->test(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Z

    move-result p1

    return p1
.end method
