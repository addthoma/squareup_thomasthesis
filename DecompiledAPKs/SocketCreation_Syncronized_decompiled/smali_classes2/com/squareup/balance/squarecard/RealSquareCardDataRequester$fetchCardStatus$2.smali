.class final Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$fetchCardStatus$2;
.super Ljava/lang/Object;
.source "RealSquareCardDataRequester.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->fetchCardStatus()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/balance/squarecard/CardStatus;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSquareCardDataRequester.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSquareCardDataRequester.kt\ncom/squareup/balance/squarecard/RealSquareCardDataRequester$fetchCardStatus$2\n*L\n1#1,132:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "status",
        "Lcom/squareup/balance/squarecard/CardStatus;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$fetchCardStatus$2;->this$0:Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/balance/squarecard/CardStatus;)V
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$fetchCardStatus$2;->this$0:Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;

    const-string v1, "status"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->access$cardData(Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;Lcom/squareup/balance/squarecard/CardStatus;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$fetchCardStatus$2;->this$0:Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;

    .line 89
    invoke-static {v0, p1}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->access$updateData(Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/balance/squarecard/CardStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$fetchCardStatus$2;->accept(Lcom/squareup/balance/squarecard/CardStatus;)V

    return-void
.end method
