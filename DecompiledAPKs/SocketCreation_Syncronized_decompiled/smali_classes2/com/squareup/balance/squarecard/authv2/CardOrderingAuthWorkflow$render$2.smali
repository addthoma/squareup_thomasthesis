.class final Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "CardOrderingAuthWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->render(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;",
        "+",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthOutput;",
        "output",
        "Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$2;->this$0:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$2;->$state:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;",
            "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    instance-of v0, p1, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoOutput$ContinueFromSsnInfo;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$2;->this$0:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->access$getAnalytics$p(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;->logSsnInfoContinueClicked()V

    .line 98
    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$2;->this$0:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;

    new-instance v3, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$2$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$2$1;-><init>(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$2;Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoOutput;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v2, v3, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 105
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoOutput$CancelSsn;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoOutput$CancelSsn;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$2;->this$0:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;

    sget-object v0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$2$2;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$2$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$2;->invoke(Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
