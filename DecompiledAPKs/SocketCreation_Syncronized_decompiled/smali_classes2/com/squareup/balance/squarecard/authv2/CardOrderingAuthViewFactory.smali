.class public final Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "CardOrderingAuthViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthViewFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthViewFactory;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthViewFactory;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthViewFactory;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthViewFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 10
    sget-object v1, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->Companion:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$Companion;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 11
    sget-object v1, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner;->Companion:Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoLayoutRunner$Companion;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 12
    sget-object v1, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogFactory;->Companion:Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogFactory$Companion;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 9
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
