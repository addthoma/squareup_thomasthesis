.class public final Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "CardOrderingAuthMissingFieldWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;",
        "Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCardOrderingAuthMissingFieldWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CardOrderingAuthMissingFieldWorkflow.kt\ncom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,44:1\n149#2,5:45\n*E\n*S KotlinDebug\n*F\n+ 1 CardOrderingAuthMissingFieldWorkflow.kt\ncom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow\n*L\n41#1,5:45\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000=\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000*\u0001\u000e\u0018\u00002*\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00060\u0005j\u0006\u0012\u0002\u0008\u0003`\u00070\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ:\u0010\u0010\u001a\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00060\u0005j\u0006\u0012\u0002\u0008\u0003`\u00072\n\u0010\u0011\u001a\u00060\u0002j\u0002`\u00032\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00040\u0013H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00040\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u000f\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;",
        "Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldProps;",
        "Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "analytics",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;",
        "(Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;)V",
        "finish",
        "Lcom/squareup/workflow/WorkflowAction;",
        "logAnalytics",
        "com/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow$logAnalytics$1",
        "Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow$logAnalytics$1;",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

.field private final finish:Lcom/squareup/workflow/WorkflowAction;

.field private final logAnalytics:Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow$logAnalytics$1;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow;->analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

    .line 25
    sget-object p1, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow$finish$1;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow$finish$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, p1, v1, v0}, Lcom/squareup/workflow/StatelessWorkflowKt;->action$default(Lcom/squareup/workflow/StatelessWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow;->finish:Lcom/squareup/workflow/WorkflowAction;

    .line 26
    new-instance p1, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow$logAnalytics$1;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow$logAnalytics$1;-><init>(Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow;->logAnalytics:Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow$logAnalytics$1;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow;->analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getFinish$p(Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow;->finish:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method


# virtual methods
.method public render(Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow;->logAnalytics:Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow$logAnalytics$1;

    check-cast v0, Lcom/squareup/workflow/Worker;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {p2, v0, v1, v2, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 38
    new-instance v0, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogRendering;

    .line 40
    new-instance v1, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow$render$1;

    invoke-direct {v1, p0, p2}, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow$render$1;-><init>(Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 38
    invoke-direct {v0, p1, v1}, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogRendering;-><init>(Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 46
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 47
    const-class p2, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogRendering;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 48
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 46
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow;->render(Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
