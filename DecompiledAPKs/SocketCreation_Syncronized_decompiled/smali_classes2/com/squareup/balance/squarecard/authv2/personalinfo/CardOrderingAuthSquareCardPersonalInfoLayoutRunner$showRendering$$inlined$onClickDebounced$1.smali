.class public final Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$showRendering$$inlined$onClickDebounced$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->showRendering(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;Lcom/squareup/workflow/ui/ContainerHints;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 CardOrderingAuthSquareCardPersonalInfoLayoutRunner.kt\ncom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner\n*L\n1#1,1322:1\n40#2,7:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $rendering$inlined:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$showRendering$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$showRendering$$inlined$onClickDebounced$1;->$rendering$inlined:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1323
    iget-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$showRendering$$inlined$onClickDebounced$1;->$rendering$inlined:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->getOnSubmitPersonalInfo()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 1324
    new-instance v0, Lcom/squareup/balance/squarecard/authv2/personalinfo/SubmitPersonalInfoData;

    .line 1325
    iget-object v1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$showRendering$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->access$getOwnerAddress$p(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;)Lcom/squareup/address/AddressLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/address/AddressLayout;->getAddress()Lcom/squareup/address/Address;

    move-result-object v1

    const-string v2, "ownerAddress.address"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1326
    iget-object v2, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$showRendering$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;

    invoke-static {v2}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->access$getOwnerBirthDate$p(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;)Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->getDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    .line 1324
    invoke-direct {v0, v1, v2}, Lcom/squareup/balance/squarecard/authv2/personalinfo/SubmitPersonalInfoData;-><init>(Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
