.class final Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CardOrderingAuthWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1;->invoke(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoOutput;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;",
        "-",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthOutput;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $output:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoOutput;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1;Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoOutput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1$1;->this$0:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1$1;->$output:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoOutput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;",
            "-",
            "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1$1;->this$0:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1;

    iget-object v0, v0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1;->this$0:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1$1;->this$0:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1;

    iget-object v1, v1, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1;->$state:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;

    .line 79
    iget-object v2, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1$1;->this$0:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1;

    iget-object v2, v2, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1;->$state:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v3

    .line 80
    iget-object v2, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1$1;->$output:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoOutput;

    check-cast v2, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoOutput$ContinueFromPersonalInfo;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoOutput$ContinueFromPersonalInfo;->getAddress()Lcom/squareup/address/Address;

    move-result-object v6

    .line 81
    iget-object v2, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1$1;->$output:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoOutput;

    check-cast v2, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoOutput$ContinueFromPersonalInfo;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoOutput$ContinueFromPersonalInfo;->getBirthDate()Lorg/threeten/bp/LocalDate;

    move-result-object v7

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x13

    const/4 v10, 0x0

    .line 79
    invoke-static/range {v3 .. v10}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->copy$default(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/balance/squarecard/order/ApprovalState;Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v2

    const/4 v3, 0x0

    .line 78
    invoke-static {v0, v1, v2, v3}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->access$tryStartIdv(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Z)Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
