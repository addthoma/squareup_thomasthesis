.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinatorKt;
.super Ljava/lang/Object;
.source "OrderSquareCardSignatureCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000*\u0016\u0010\u0002\"\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a8\u0006\u0005"
    }
    d2 = {
        "OUTLINE_TO_STROKE_WIDTH_RATIO",
        "",
        "Action",
        "Lkotlin/Function0;",
        "",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final OUTLINE_TO_STROKE_WIDTH_RATIO:F = 100.0f
