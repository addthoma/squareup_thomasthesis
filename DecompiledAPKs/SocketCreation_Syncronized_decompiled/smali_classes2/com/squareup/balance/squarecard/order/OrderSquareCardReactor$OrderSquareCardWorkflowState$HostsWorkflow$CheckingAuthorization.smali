.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;
.super Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow;
.source "OrderSquareCardReactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CheckingAuthorization"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow<",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\t\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001BQ\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012:\u0010\u0007\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r0\n0\t\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00020\u0008j\u0002`\u000f\u00a2\u0006\u0002\u0010\u0010R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014RH\u0010\u0015\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r0\n0\t\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00020\u0008j\u0002`\u000fX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
        "skippedInitialScreens",
        "",
        "squareCardCustomizationSettings",
        "Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;",
        "workflow",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflow;",
        "(ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Lcom/squareup/workflow/rx1/Workflow;)V",
        "getSkippedInitialScreens",
        "()Z",
        "getSquareCardCustomizationSettings",
        "()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;",
        "subWorkflow",
        "getSubWorkflow",
        "()Lcom/squareup/workflow/rx1/Workflow;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final skippedInitialScreens:Z

.field private final squareCardCustomizationSettings:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

.field private final subWorkflow:Lcom/squareup/workflow/rx1/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Lcom/squareup/workflow/rx1/Workflow;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+",
            "Lcom/squareup/workflow/ScreenState<",
            "+",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*+",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "squareCardCustomizationSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 206
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;->skippedInitialScreens:Z

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;->squareCardCustomizationSettings:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    .line 208
    new-instance p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization$subWorkflow$1;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization$subWorkflow$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p3, p1}, Lcom/squareup/workflow/rx1/WorkflowKt;->mapState(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;->subWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    return-void
.end method


# virtual methods
.method public final getSkippedInitialScreens()Z
    .locals 1

    .line 203
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;->skippedInitialScreens:Z

    return v0
.end method

.method public final getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;->squareCardCustomizationSettings:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    return-object v0
.end method

.method public getSubWorkflow()Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;"
        }
    .end annotation

    .line 207
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;->subWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    return-object v0
.end method
