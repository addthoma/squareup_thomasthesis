.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;
.super Ljava/lang/Object;
.source "OrderSquareCardSignatureCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->maybeAddStamp(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Landroid/graphics/Point;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "position",
        "Landroid/graphics/Point;",
        "kotlin.jvm.PlatformType",
        "accept",
        "com/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$1$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen$inlined:Lcom/squareup/workflow/legacy/Screen;

.field final synthetic $stampProto:Lcom/squareup/protos/client/bizbank/Stamp;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/protos/client/bizbank/Stamp;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;->$stampProto:Lcom/squareup/protos/client/bizbank/Stamp;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;->$screen$inlined:Lcom/squareup/workflow/legacy/Screen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Landroid/graphics/Point;)V
    .locals 7

    .line 334
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getStampView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/stampview/StampView;

    move-result-object v0

    .line 335
    new-instance v1, Lcom/squareup/cardcustomizations/stampview/Stamp;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;->$stampProto:Lcom/squareup/protos/client/bizbank/Stamp;

    iget-object v2, v2, Lcom/squareup/protos/client/bizbank/Stamp;->name:Ljava/lang/String;

    const-string v3, "stampProto.name"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;->$stampProto:Lcom/squareup/protos/client/bizbank/Stamp;

    iget-object v3, v3, Lcom/squareup/protos/client/bizbank/Stamp;->svg:Ljava/lang/String;

    const-string v4, "stampProto.svg"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardcustomizations/stampview/Stamp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    new-instance v2, Landroid/graphics/RectF;

    .line 337
    iget v3, p1, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    .line 338
    iget v4, p1, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    .line 339
    iget v5, p1, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v6}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getStampSize$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)F

    move-result v6

    add-float/2addr v5, v6

    .line 340
    iget p1, p1, Landroid/graphics/Point;->y:I

    int-to-float p1, p1

    iget-object v6, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v6}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getStampSize$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)F

    move-result v6

    add-float/2addr p1, v6

    .line 336
    invoke-direct {v2, v3, v4, v5, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 342
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;->$stampProto:Lcom/squareup/protos/client/bizbank/Stamp;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/Stamp;->min_scale:Ljava/lang/Integer;

    const-string v3, "stampProto.min_scale"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 334
    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/cardcustomizations/stampview/StampView;->addStamp(Lcom/squareup/cardcustomizations/stampview/Stamp;Landroid/graphics/RectF;I)V

    .line 344
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;->STAMP:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$setMode$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;)V

    .line 345
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getUndoStack$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Ljava/util/ArrayDeque;

    move-result-object p1

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 350
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$notifyState(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    .line 354
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;->$screen$inlined:Lcom/squareup/workflow/legacy/Screen;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$StampPopulated;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$StampPopulated;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 80
    check-cast p1, Landroid/graphics/Point;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;->accept(Landroid/graphics/Point;)V

    return-void
.end method
