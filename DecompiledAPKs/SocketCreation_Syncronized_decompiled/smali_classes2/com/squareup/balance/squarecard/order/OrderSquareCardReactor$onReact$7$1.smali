.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderSquareCardReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;",
        "stampStatus",
        "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;",
            ">;"
        }
    .end annotation

    const-string v0, "stampStatus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 401
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 402
    new-instance v9, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    .line 403
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;

    iget-object v1, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;->getSkippedInitialScreens()Z

    move-result v2

    .line 404
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;

    iget-object v1, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;->getAllowStampsCustomization()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    move-result-object v3

    .line 405
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;

    iget-object v1, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v4

    .line 406
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;

    iget-object v1, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v5

    .line 407
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;

    iget-object v1, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;->getStamp()Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object v6

    .line 409
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;

    iget-object v1, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;->getStampsToRestore()Ljava/util/List;

    move-result-object v7

    move-object v1, v9

    move-object v8, p1

    .line 402
    invoke-direct/range {v1 .. v8}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;-><init>(ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Lcom/squareup/protos/client/bizbank/Stamp;Ljava/util/List;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;)V

    .line 401
    invoke-direct {v0, v9}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 88
    check-cast p1, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7$1;->invoke(Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
