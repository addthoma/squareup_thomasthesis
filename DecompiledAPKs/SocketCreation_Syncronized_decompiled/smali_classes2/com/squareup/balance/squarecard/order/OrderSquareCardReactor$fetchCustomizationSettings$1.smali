.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$fetchCustomizationSettings$1;
.super Ljava/lang/Object;
.source "OrderSquareCardReactor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->fetchCustomizationSettings(Z)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $skippedInitialScreens:Z

.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$fetchCustomizationSettings$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;

    iput-boolean p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$fetchCustomizationSettings$1;->$skippedInitialScreens:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;",
            ">;)",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 477
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_1

    .line 478
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$fetchCustomizationSettings$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;

    invoke-static {v0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->access$toCustomizationSettings(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;)Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object p1

    .line 479
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->isValidForCardOrdering()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$fetchCustomizationSettings$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->access$getStateFactory$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;)Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$fetchCustomizationSettings$1;->$skippedInitialScreens:Z

    invoke-virtual {v0, v1, p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;->startAuthSquareCard(ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    move-result-object p1

    goto :goto_0

    .line 482
    :cond_0
    new-instance p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$fetchCustomizationSettings$1;->$skippedInitialScreens:Z

    sget-object v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError$ErrorType;->NO_OWNER_NAME:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError$ErrorType;

    invoke-direct {p1, v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;-><init>(ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError$ErrorType;)V

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    goto :goto_0

    .line 485
    :cond_1
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_2

    .line 486
    new-instance p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$fetchCustomizationSettings$1;->$skippedInitialScreens:Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {p1, v0, v2, v1, v2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;-><init>(ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError$ErrorType;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 88
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$fetchCustomizationSettings$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    move-result-object p1

    return-object p1
.end method
