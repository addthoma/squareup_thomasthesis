.class public final Lcom/squareup/balance/squarecard/utility/CardFormatterKt;
.super Ljava/lang/Object;
.source "CardFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCardFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CardFormatter.kt\ncom/squareup/balance/squarecard/utility/CardFormatterKt\n*L\n1#1,23:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0001H\u0002\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00058F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0006\u00a8\u0006\n"
    }
    d2 = {
        "formattedBrandAndUnmaskedDigits",
        "",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "getFormattedBrandAndUnmaskedDigits",
        "(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Ljava/lang/String;",
        "Lcom/squareup/protos/client/deposits/CardInfo;",
        "(Lcom/squareup/protos/client/deposits/CardInfo;)Ljava/lang/String;",
        "brand",
        "Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
        "lastFour",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final formattedBrandAndUnmaskedDigits(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->name()Ljava/lang/String;

    move-result-object p0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Locale.US"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_0

    invoke-virtual {p0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    const-string v1, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lkotlin/text/StringsKt;->capitalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p0, 0xa0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final getFormattedBrandAndUnmaskedDigits(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$formattedBrandAndUnmaskedDigits"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    iget-object v0, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const-string v1, "card_brand"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->pan_last_four:Ljava/lang/String;

    const-string v1, "pan_last_four"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p0}, Lcom/squareup/balance/squarecard/utility/CardFormatterKt;->formattedBrandAndUnmaskedDigits(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final getFormattedBrandAndUnmaskedDigits(Lcom/squareup/protos/client/deposits/CardInfo;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$formattedBrandAndUnmaskedDigits"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    iget-object v0, p0, Lcom/squareup/protos/client/deposits/CardInfo;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const-string v1, "brand"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lcom/squareup/protos/client/deposits/CardInfo;->pan_suffix:Ljava/lang/String;

    const-string v1, "pan_suffix"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p0}, Lcom/squareup/balance/squarecard/utility/CardFormatterKt;->formattedBrandAndUnmaskedDigits(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
