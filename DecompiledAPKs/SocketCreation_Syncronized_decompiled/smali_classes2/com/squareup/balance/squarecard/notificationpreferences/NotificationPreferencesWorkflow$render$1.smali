.class final Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "NotificationPreferencesWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;->render(Lkotlin/Unit;Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/notificationpreferences/fetching/FetchingResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;",
        "+",
        "Lkotlin/Unit;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0008\u0012\u00060\u0003j\u0002`\u00040\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;",
        "",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesFinished;",
        "result",
        "Lcom/squareup/balance/squarecard/notificationpreferences/fetching/FetchingResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$render$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$render$1;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$render$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$render$1;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$render$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/notificationpreferences/fetching/FetchingResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/notificationpreferences/fetching/FetchingResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    sget-object v0, Lcom/squareup/balance/squarecard/notificationpreferences/fetching/FetchingResult$OnBack;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/fetching/FetchingResult$OnBack;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$Exit;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$Exit;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_1

    .line 75
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/squarecard/notificationpreferences/fetching/FetchingResult$Finished;

    if-eqz v0, :cond_3

    .line 76
    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/fetching/FetchingResult$Finished;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/fetching/FetchingResult$Finished;->getData()Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult;

    move-result-object v0

    .line 77
    instance-of v1, v0, Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;

    if-eqz v1, :cond_1

    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$DisplayPreferences;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/fetching/FetchingResult$Finished;->getData()Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$DisplayPreferences;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;)V

    check-cast v0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action;

    goto :goto_0

    .line 78
    :cond_1
    sget-object p1, Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Error;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Error;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$DisplayError;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action$DisplayError;

    move-object v0, p1

    check-cast v0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action;

    .line 76
    :goto_0
    move-object p1, v0

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    :goto_1
    return-object p1

    .line 78
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 76
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/fetching/FetchingResult;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$render$1;->invoke(Lcom/squareup/balance/squarecard/notificationpreferences/fetching/FetchingResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
