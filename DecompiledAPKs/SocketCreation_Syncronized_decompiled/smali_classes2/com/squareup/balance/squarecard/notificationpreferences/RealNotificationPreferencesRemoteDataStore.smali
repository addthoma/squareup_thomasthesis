.class public final Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore;
.super Ljava/lang/Object;
.source "NotificationPreferencesRemoteDataStore.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cH\u0016J\u0016\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u001e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00130\u00120\u000cH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\u00088BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;",
        "bizbankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/settings/server/AccountStatusSettings;)V",
        "token",
        "",
        "getToken",
        "()Ljava/lang/String;",
        "getPreferences",
        "Lio/reactivex/Single;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult;",
        "updatePreferences",
        "preferences",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;",
        "mapResult",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bizbankService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method private final getToken()Ljava/lang/String;
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    const-string v1, "accountStatusSettings.userSettings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object v0
.end method

.method private final mapResult(Lio/reactivex/Single;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;",
            ">;>;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult;",
            ">;"
        }
    .end annotation

    .line 62
    sget-object v0, Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore$mapResult$1;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore$mapResult$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "map { result ->\n      wh\u2026esult.Error\n      }\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public getPreferences()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult;",
            ">;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest$Builder;-><init>()V

    .line 39
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest$Builder;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest$Builder;->build()Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v2, "request"

    .line 43
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->getNotificationPreferences(Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 45
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore;->mapResult(Lio/reactivex/Single;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public updatePreferences(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult;",
            ">;"
        }
    .end annotation

    const-string v0, "preferences"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v0, Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest$Builder;-><init>()V

    .line 50
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest$Builder;

    move-result-object v0

    .line 51
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;->getCardDeclines()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreference;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreference;->isEnabled()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest$Builder;->send_card_declined_notifications(Ljava/lang/Boolean;)Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest$Builder;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest$Builder;->build()Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest;

    move-result-object p1

    .line 54
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v1, "request"

    .line 55
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->setNotificationPreferences(Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 56
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore;->mapResult(Lio/reactivex/Single;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
