.class public final Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SquareCardConfirmationCodeCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardConfirmationCodeCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardConfirmationCodeCoordinator.kt\ncom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator\n*L\n1#1,134:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0000\u0018\u00002\u00020\u0001B)\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u001c\u0010\u0019\u001a\u00020\u00152\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u00150\u001bH\u0002J\u0008\u0010\u001d\u001a\u00020\u0015H\u0002J\u0018\u0010\u001e\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u001f\u001a\u00020\u0005H\u0002J\u0010\u0010 \u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020\u0005H\u0002J\u0008\u0010!\u001a\u00020\u0015H\u0002J\u0018\u0010\"\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020\u00052\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0010\u0010#\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020\u0005H\u0002J\u0016\u0010$\u001a\u00020\u0015*\u00020\u00102\u0008\u0008\u0001\u0010%\u001a\u00020&H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "codeConfirmationHelp",
        "Lcom/squareup/widgets/MessageView;",
        "codeConfirmationInput",
        "Landroid/widget/EditText;",
        "codeMessageBody",
        "Lcom/squareup/marketfont/MarketTextView;",
        "codeMessageTitle",
        "continueButton",
        "Landroid/widget/Button;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "handleBack",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$Event;",
        "initCodeConfirmationInput",
        "update",
        "screen",
        "updateActionBar",
        "updateContinueButton",
        "updateLayout",
        "updateTitleAndMessage",
        "setTextOrHide",
        "text",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private codeConfirmationHelp:Lcom/squareup/widgets/MessageView;

.field private codeConfirmationInput:Landroid/widget/EditText;

.field private codeMessageBody:Lcom/squareup/marketfont/MarketTextView;

.field private codeMessageTitle:Lcom/squareup/marketfont/MarketTextView;

.field private continueButton:Landroid/widget/Button;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getCodeConfirmationInput$p(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;)Landroid/widget/EditText;
    .locals 1

    .line 38
    iget-object p0, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->codeConfirmationInput:Landroid/widget/EditText;

    if-nez p0, :cond_0

    const-string v0, "codeConfirmationInput"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->handleBack(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final synthetic access$setCodeConfirmationInput$p(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;Landroid/widget/EditText;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->codeConfirmationInput:Landroid/widget/EditText;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;Landroid/view/View;Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->update(Landroid/view/View;Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;)V

    return-void
.end method

.method public static final synthetic access$updateContinueButton(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->updateContinueButton()V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 126
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 127
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_activation_code_confirmation_input:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->codeConfirmationInput:Landroid/widget/EditText;

    .line 128
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_activation_code_confirmation_help:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->codeConfirmationHelp:Lcom/squareup/widgets/MessageView;

    .line 129
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->card_activation_confirm_code_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->codeMessageTitle:Lcom/squareup/marketfont/MarketTextView;

    .line 130
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_activation_code_help_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->codeMessageBody:Lcom/squareup/marketfont/MarketTextView;

    .line 131
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_activation_code_confirmation_continue_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->continueButton:Landroid/widget/Button;

    return-void
.end method

.method private final handleBack(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$Event;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 68
    sget-object v0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$Event$GoBack;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final initCodeConfirmationInput()V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->codeConfirmationInput:Landroid/widget/EditText;

    if-nez v0, :cond_0

    const-string v1, "codeConfirmationInput"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$initCodeConfirmationInput$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$initCodeConfirmationInput$1;-><init>(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;)V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private final setTextOrHide(Lcom/squareup/marketfont/MarketTextView;I)V
    .locals 1

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    const/4 v0, 0x0

    .line 99
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 100
    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    goto :goto_0

    :cond_0
    const/16 p2, 0x8

    .line 102
    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;)V
    .locals 1

    .line 61
    new-instance v0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$update$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$update$1;-><init>(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 62
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->updateActionBar(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;)V

    .line 63
    invoke-direct {p0, p2, p1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->updateLayout(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;Landroid/view/View;)V

    .line 64
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->updateTitleAndMessage(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;)V

    return-void
.end method

.method private final updateActionBar(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;)V
    .locals 4

    .line 118
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 115
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 116
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;->getData()Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$ScreenData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$ScreenData;->getTitle()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 117
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$updateActionBar$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$updateActionBar$1;-><init>(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 118
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateContinueButton()V
    .locals 3

    .line 122
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->continueButton:Landroid/widget/Button;

    if-nez v0, :cond_0

    const-string v1, "continueButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->codeConfirmationInput:Landroid/widget/EditText;

    if-nez v1, :cond_1

    const-string v2, "codeConfirmationInput"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method private final updateLayout(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;Landroid/view/View;)V
    .locals 3

    .line 83
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->codeConfirmationHelp:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "codeConfirmationHelp"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 75
    :cond_0
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {v1, p2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 76
    sget p2, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirm_code_help:I

    const-string v2, "resend_email"

    invoke-virtual {v1, p2, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 77
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirm_code_help_resend_email:I

    invoke-virtual {p2, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 78
    new-instance v1, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$updateLayout$1;

    sget v2, Lcom/squareup/noho/R$color;->noho_text_help_link:I

    invoke-direct {v1, p1, v2}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$updateLayout$1;-><init>(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;I)V

    check-cast v1, Lcom/squareup/ui/LinkSpan;

    invoke-virtual {p2, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 83
    invoke-virtual {p2}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object p2, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->continueButton:Landroid/widget/Button;

    const-string v0, "continueButton"

    if-nez p2, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;->getData()Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$ScreenData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$ScreenData;->getButtonText()I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/widget/Button;->setText(I)V

    .line 86
    iget-object p2, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->continueButton:Landroid/widget/Button;

    if-nez p2, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 87
    :cond_2
    new-instance v0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$updateLayout$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$updateLayout$2;-><init>(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    check-cast p1, Landroid/view/View$OnClickListener;

    .line 86
    invoke-virtual {p2, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final updateTitleAndMessage(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;)V
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->codeMessageTitle:Lcom/squareup/marketfont/MarketTextView;

    if-nez v0, :cond_0

    const-string v1, "codeMessageTitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;->getData()Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$ScreenData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$ScreenData;->getMessageTitle()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->setTextOrHide(Lcom/squareup/marketfont/MarketTextView;I)V

    .line 94
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->codeMessageBody:Lcom/squareup/marketfont/MarketTextView;

    if-nez v0, :cond_1

    const-string v1, "codeMessageBody"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;->getData()Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$ScreenData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$ScreenData;->getMessage()I

    move-result p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->setTextOrHide(Lcom/squareup/marketfont/MarketTextView;I)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->bindViews(Landroid/view/View;)V

    .line 50
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->updateContinueButton()V

    .line 51
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->initCodeConfirmationInput()V

    .line 53
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$attach$1;-><init>(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { upda\u2026iew, it.unwrapV2Screen) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
