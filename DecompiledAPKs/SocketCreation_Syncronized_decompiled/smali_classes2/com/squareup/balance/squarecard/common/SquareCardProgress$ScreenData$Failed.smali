.class public final Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;
.super Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;
.source "SquareCardProgressScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Failed"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0006H\u00c6\u0003J1\u0010\u0013\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0006H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\n\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
        "title",
        "Lcom/squareup/util/ViewString;",
        "message",
        "primaryButtonText",
        "",
        "secondaryButtonText",
        "(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;II)V",
        "getMessage",
        "()Lcom/squareup/util/ViewString;",
        "getPrimaryButtonText",
        "()I",
        "getSecondaryButtonText",
        "getTitle",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final message:Lcom/squareup/util/ViewString;

.field private final primaryButtonText:I

.field private final secondaryButtonText:I

.field private final title:Lcom/squareup/util/ViewString;


# direct methods
.method public constructor <init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;II)V
    .locals 1

    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->title:Lcom/squareup/util/ViewString;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->message:Lcom/squareup/util/ViewString;

    iput p3, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->primaryButtonText:I

    iput p4, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->secondaryButtonText:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, -0x1

    .line 28
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;II)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILjava/lang/Object;)Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->title:Lcom/squareup/util/ViewString;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->message:Lcom/squareup/util/ViewString;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->primaryButtonText:I

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget p4, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->secondaryButtonText:I

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->copy(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;II)Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/util/ViewString;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->title:Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public final component2()Lcom/squareup/util/ViewString;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->message:Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->primaryButtonText:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->secondaryButtonText:I

    return v0
.end method

.method public final copy(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;II)Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;
    .locals 1

    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->title:Lcom/squareup/util/ViewString;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->title:Lcom/squareup/util/ViewString;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->message:Lcom/squareup/util/ViewString;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->message:Lcom/squareup/util/ViewString;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->primaryButtonText:I

    iget v1, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->primaryButtonText:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->secondaryButtonText:I

    iget p1, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->secondaryButtonText:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMessage()Lcom/squareup/util/ViewString;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->message:Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public final getPrimaryButtonText()I
    .locals 1

    .line 23
    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->primaryButtonText:I

    return v0
.end method

.method public final getSecondaryButtonText()I
    .locals 1

    .line 28
    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->secondaryButtonText:I

    return v0
.end method

.method public final getTitle()Lcom/squareup/util/ViewString;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->title:Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->title:Lcom/squareup/util/ViewString;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->message:Lcom/squareup/util/ViewString;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->primaryButtonText:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->secondaryButtonText:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failed(title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->title:Lcom/squareup/util/ViewString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->message:Lcom/squareup/util/ViewString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", primaryButtonText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->primaryButtonText:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", secondaryButtonText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->secondaryButtonText:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
