.class public final Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$updateLayout$1;
.super Lcom/squareup/ui/LinkSpan;
.source "SquareCardConfirmationCodeCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->updateLayout(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$updateLayout$1",
        "Lcom/squareup/ui/LinkSpan;",
        "onClick",
        "",
        "widget",
        "Landroid/view/View;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;I)V
    .locals 0

    .line 78
    iput-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$updateLayout$1;->$screen:Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;

    invoke-direct {p0, p2}, Lcom/squareup/ui/LinkSpan;-><init>(I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "widget"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$updateLayout$1;->$screen:Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object v0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$Event$ResendConfirmationCode;->INSTANCE:Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$Event$ResendConfirmationCode;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
