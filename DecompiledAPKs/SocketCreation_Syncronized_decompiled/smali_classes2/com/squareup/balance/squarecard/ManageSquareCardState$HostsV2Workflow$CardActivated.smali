.class public final Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;
.super Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow;
.source "ManageSquareCardState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CardActivated"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\u0008\u0010\u0014\u001a\u00020\u0015H\u0016J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow;",
        "subWorkflow",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;",
        "card",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V",
        "getCard",
        "()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "getSubWorkflow",
        "()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

.field private final subWorkflow:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V
    .locals 1

    const-string v0, "subWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "card"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 62
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->subWorkflow:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->getSubWorkflow()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->copy(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->getSubWorkflow()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    return-object v0
.end method

.method public final copy(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;
    .locals 1

    const-string v0, "subWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "card"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;

    invoke-direct {v0, p1, p2}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->getSubWorkflow()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->getSubWorkflow()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iget-object p1, p1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    return-object v0
.end method

.method public getSubWorkflow()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->subWorkflow:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;

    return-object v0
.end method

.method public bridge synthetic getSubWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 59
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->getSubWorkflow()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->getSubWorkflow()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 63
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated$toSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated$toSnapshot$1;-><init>(Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CardActivated(subWorkflow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->getSubWorkflow()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
