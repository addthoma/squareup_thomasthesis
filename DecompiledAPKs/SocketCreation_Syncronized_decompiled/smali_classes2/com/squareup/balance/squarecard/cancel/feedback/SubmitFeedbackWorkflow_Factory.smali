.class public final Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;
.super Ljava/lang/Object;
.source "SubmitFeedbackWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/success/SubmitFeedbackSuccessWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/success/SubmitFeedbackSuccessWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 31
    iput-object p4, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/success/SubmitFeedbackSuccessWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;",
            ">;)",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/success/SubmitFeedbackSuccessWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;",
            ">;)",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;"
        }
    .end annotation

    .line 50
    new-instance v0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;
    .locals 4

    .line 36
    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;->newInstance(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;->get()Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;

    move-result-object v0

    return-object v0
.end method
