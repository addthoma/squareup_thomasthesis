.class final Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$cancelSquareCard$1;
.super Ljava/lang/Object;
.source "CancelingSquareCardWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;->cancelSquareCard(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardOutput;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bizbank/DeactivateCardResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$cancelSquareCard$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$cancelSquareCard$1;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$cancelSquareCard$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$cancelSquareCard$1;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$cancelSquareCard$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardOutput;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bizbank/DeactivateCardResponse;",
            ">;)",
            "Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardOutput;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardOutput$CancelingSucceeded;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bizbank/DeactivateCardResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/DeactivateCardResponse;->bizbank_enabled:Ljava/lang/Boolean;

    const-string v1, "successOrFailure.response.bizbank_enabled"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardOutput$CancelingSucceeded;-><init>(Z)V

    check-cast v0, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardOutput;

    goto :goto_0

    .line 79
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardOutput$CancelingFailed;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardOutput$CancelingFailed;

    move-object v0, p1

    check-cast v0, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardOutput;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$cancelSquareCard$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardOutput;

    move-result-object p1

    return-object p1
.end method
