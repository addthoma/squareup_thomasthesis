.class final Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CancelSquareCardSuccessWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow;->render(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCancelSquareCardSuccessWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CancelSquareCardSuccessWorkflow.kt\ncom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow$render$1\n*L\n1#1,73:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "event",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow$render$1;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow$render$1;->invoke(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    sget-object v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event$GoBack;->INSTANCE:Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event$GoBack;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow$render$1;->$sink:Lcom/squareup/workflow/Sink;

    sget-object v0, Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessOutput$OnBack;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessOutput$OnBack;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    goto :goto_0

    .line 57
    :cond_0
    sget-object v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event$PrimaryAction;->INSTANCE:Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event$PrimaryAction;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow$render$1;->$sink:Lcom/squareup/workflow/Sink;

    sget-object v0, Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessOutput$ReOrderCard;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessOutput$ReOrderCard;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    goto :goto_0

    .line 58
    :cond_1
    sget-object v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event$SecondaryAction;->INSTANCE:Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event$SecondaryAction;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_0
    return-void

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " is not supported"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
