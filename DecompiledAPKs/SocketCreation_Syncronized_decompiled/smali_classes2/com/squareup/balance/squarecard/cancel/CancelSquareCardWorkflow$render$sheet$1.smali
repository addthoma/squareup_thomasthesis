.class final Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CancelSquareCardWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->render(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
        "+",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;",
        "output",
        "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$1;->this$0:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$1;->$state:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmOutput$OnBack;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmOutput$OnBack;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$BackToSelectReasons;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$BackToSelectReasons;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 110
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmOutput$DeactivateCard;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmOutput$DeactivateCard;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyToDeactivateCard;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$1;->this$0:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->access$getAnalytics$p(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;)Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$1;->$state:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;

    check-cast v1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ConfirmingCancelCard;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ConfirmingCancelCard;->getDeactivationReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyToDeactivateCard;-><init>(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;Lcom/squareup/balance/squarecard/cancel/DeactivationReason;)V

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$1;->invoke(Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
