.class final Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$1;
.super Ljava/lang/Object;
.source "SquareCardAddToGooglePayHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->addCardToGooglePay(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "isConnected",
        "",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Ljava/lang/Boolean;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$1;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$1;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Boolean;)V
    .locals 0

    .line 44
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    sget-object p1, Lcom/squareup/googlepay/GooglePayException$GooglePayUnavailableException;->INSTANCE:Lcom/squareup/googlepay/GooglePayException$GooglePayUnavailableException;

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 26
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$1;->accept(Ljava/lang/Boolean;)V

    return-void
.end method
