.class public final Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics_Factory;
.super Ljava/lang/Object;
.source "RealSquareCardActivatedAnalytics_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;",
            ">;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;",
            ">;)",
            "Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics_Factory;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;-><init>(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics_Factory;->newInstance(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics_Factory;->get()Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;

    move-result-object v0

    return-object v0
.end method
