.class public final Lcom/squareup/balance/squarecard/impl/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final bottom_sheet_indicator_height:I = 0x7f070088

.field public static final bottom_sheet_indicator_offset:I = 0x7f070089

.field public static final bottom_sheet_indicator_width:I = 0x7f07008a

.field public static final default_stamp_stroke_width:I = 0x7f0700f3

.field public static final deposits_info_phone_image_height:I = 0x7f070103

.field public static final deposits_info_phone_image_width:I = 0x7f070104

.field public static final order_square_card_image_width:I = 0x7f0703f7

.field public static final responsive_square_card_signature_button_margin:I = 0x7f070482

.field public static final responsive_square_card_signature_button_side:I = 0x7f070483

.field public static final responsive_square_card_signature_trash:I = 0x7f070484

.field public static final responsive_square_card_signature_trash_margin:I = 0x7f070485

.field public static final square_card_disabled_icon_size:I = 0x7f0704e1

.field public static final square_card_disabled_message_spacing:I = 0x7f0704e2

.field public static final square_card_max_width_for_signature_screen:I = 0x7f0704e3

.field public static final square_card_max_width_for_status_screens:I = 0x7f0704e4

.field public static final square_card_ordered_phone_image_height:I = 0x7f0704e5

.field public static final square_card_ordered_phone_image_width:I = 0x7f0704e6

.field public static final square_card_outline_corner_radius:I = 0x7f0704e7

.field public static final square_card_outline_dash_gap:I = 0x7f0704e8

.field public static final square_card_outline_dash_width:I = 0x7f0704e9

.field public static final square_card_outline_width:I = 0x7f0704ea

.field public static final square_card_signature_indicator_top_spacing:I = 0x7f0704f3

.field public static final square_card_signature_spacing:I = 0x7f0704f4

.field public static final square_card_stamp_preview_padding:I = 0x7f0704f5

.field public static final square_card_stamps_peek_height:I = 0x7f0704f6


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
