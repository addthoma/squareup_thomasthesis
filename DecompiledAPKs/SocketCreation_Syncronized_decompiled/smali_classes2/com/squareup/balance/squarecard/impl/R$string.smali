.class public final Lcom/squareup/balance/squarecard/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final add_to_google_pay:I = 0x7f120097

.field public static final add_to_google_pay_cancelled_message:I = 0x7f120098

.field public static final add_to_google_pay_cancelled_title:I = 0x7f120099

.field public static final add_to_google_pay_error_message:I = 0x7f12009a

.field public static final add_to_google_pay_error_title:I = 0x7f12009b

.field public static final add_to_google_pay_success_message:I = 0x7f12009c

.field public static final add_to_google_pay_success_title:I = 0x7f12009d

.field public static final auth_square_card_idv_cancel_leave:I = 0x7f1200ec

.field public static final auth_square_card_idv_cancel_message:I = 0x7f1200ed

.field public static final auth_square_card_idv_cancel_title:I = 0x7f1200ee

.field public static final auth_square_card_idv_cancel_try_again:I = 0x7f1200ef

.field public static final auth_square_card_idv_error_message:I = 0x7f1200f0

.field public static final auth_square_card_idv_error_title:I = 0x7f1200f1

.field public static final auth_square_card_idv_failure_close:I = 0x7f1200f2

.field public static final auth_square_card_idv_failure_message:I = 0x7f1200f3

.field public static final auth_square_card_idv_failure_title:I = 0x7f1200f4

.field public static final auth_square_card_idv_failure_try_again:I = 0x7f1200f5

.field public static final auth_square_card_idv_verifying:I = 0x7f1200f6

.field public static final auth_square_card_missing_address_message:I = 0x7f1200f7

.field public static final auth_square_card_missing_address_title:I = 0x7f1200f8

.field public static final auth_square_card_missing_birthdate_message:I = 0x7f1200f9

.field public static final auth_square_card_missing_birthdate_title:I = 0x7f1200fa

.field public static final auth_square_card_missing_ssn_message:I = 0x7f1200fb

.field public static final auth_square_card_missing_ssn_title:I = 0x7f1200fc

.field public static final auth_square_card_owner_address_label:I = 0x7f1200fd

.field public static final auth_square_card_owner_birthdate_label:I = 0x7f1200fe

.field public static final auth_square_card_owner_name_label:I = 0x7f1200ff

.field public static final auth_square_card_personal_info_header:I = 0x7f120100

.field public static final auth_square_card_ssn_info_caption:I = 0x7f120101

.field public static final auth_square_card_ssn_info_header:I = 0x7f120102

.field public static final auth_square_card_ssn_info_label:I = 0x7f120103

.field public static final auth_square_card_title:I = 0x7f120104

.field public static final cancel_bizbank_error_message:I = 0x7f120283

.field public static final cancel_bizbank_error_title:I = 0x7f120284

.field public static final cancel_card_action_bar:I = 0x7f120285

.field public static final cancel_card_button_text:I = 0x7f120286

.field public static final cancel_card_error_message:I = 0x7f120287

.field public static final cancel_card_error_title:I = 0x7f120288

.field public static final cancel_card_message:I = 0x7f120289

.field public static final cancel_card_message_title:I = 0x7f12028a

.field public static final cancel_card_never_received_message:I = 0x7f12028b

.field public static final cancel_card_only_message:I = 0x7f12028c

.field public static final cancel_card_reason_generic:I = 0x7f12028d

.field public static final cancel_card_reason_lost:I = 0x7f12028e

.field public static final cancel_card_reason_never_received:I = 0x7f12028f

.field public static final cancel_card_reason_stolen:I = 0x7f120290

.field public static final cancel_card_reasons_title:I = 0x7f120291

.field public static final cancel_lost_card_action_bar:I = 0x7f120295

.field public static final cancel_never_received_card_action_bar:I = 0x7f120296

.field public static final cancel_stolen_card_action_bar:I = 0x7f12029a

.field public static final canceled_bizbank_succeeded_action_bar:I = 0x7f1202a1

.field public static final canceled_bizbank_succeeded_help:I = 0x7f1202a2

.field public static final canceled_bizbank_succeeded_message:I = 0x7f1202a3

.field public static final canceled_bizbank_succeeded_message_title:I = 0x7f1202a4

.field public static final canceled_card_leave_feedback:I = 0x7f1202a5

.field public static final canceled_card_leave_feedback_button_text:I = 0x7f1202a6

.field public static final canceled_card_leave_feedback_hint:I = 0x7f1202a7

.field public static final canceled_card_leave_feedback_message:I = 0x7f1202a8

.field public static final canceled_card_success_bizbank_enabled_message:I = 0x7f1202a9

.field public static final canceled_card_success_disable_bizbank_button_text:I = 0x7f1202aa

.field public static final canceled_card_success_message:I = 0x7f1202ab

.field public static final canceled_card_success_order_replacement_button_text:I = 0x7f1202ac

.field public static final canceled_card_success_title:I = 0x7f1202ad

.field public static final canceling_bizbank_spinner_message:I = 0x7f1202af

.field public static final canceling_card_spinner_message:I = 0x7f1202b0

.field public static final card_activation_confirm_address_message:I = 0x7f1202d1

.field public static final card_activation_confirm_card_action_bar:I = 0x7f1202d2

.field public static final card_activation_confirm_card_incorrect_info_error_message:I = 0x7f1202d3

.field public static final card_activation_confirm_card_incorrect_info_error_title:I = 0x7f1202d4

.field public static final card_activation_confirm_card_message_no_reader:I = 0x7f1202d5

.field public static final card_activation_confirm_card_message_with_reader:I = 0x7f1202d6

.field public static final card_activation_confirm_card_title:I = 0x7f1202d7

.field public static final card_activation_confirm_code_action_bar:I = 0x7f1202d8

.field public static final card_activation_confirm_code_already_used_action_bar:I = 0x7f1202d9

.field public static final card_activation_confirm_code_already_used_message:I = 0x7f1202da

.field public static final card_activation_confirm_code_already_used_title:I = 0x7f1202db

.field public static final card_activation_confirm_code_expired_message:I = 0x7f1202dc

.field public static final card_activation_confirm_code_expired_title:I = 0x7f1202dd

.field public static final card_activation_confirm_code_expired_token_error_message:I = 0x7f1202de

.field public static final card_activation_confirm_code_expired_token_error_title:I = 0x7f1202df

.field public static final card_activation_confirm_code_help:I = 0x7f1202e0

.field public static final card_activation_confirm_code_help_resend_email:I = 0x7f1202e1

.field public static final card_activation_confirm_code_help_resend_email_failed_title:I = 0x7f1202e2

.field public static final card_activation_confirm_code_hint:I = 0x7f1202e3

.field public static final card_activation_confirm_code_invalid_token_error_message:I = 0x7f1202e4

.field public static final card_activation_confirm_code_invalid_token_error_title:I = 0x7f1202e5

.field public static final card_activation_confirm_code_message:I = 0x7f1202e6

.field public static final card_activation_confirm_code_title:I = 0x7f1202e7

.field public static final card_activation_confirming_card:I = 0x7f1202e8

.field public static final card_activation_confirming_code:I = 0x7f1202e9

.field public static final card_activation_creating_pin:I = 0x7f1202ea

.field public static final card_activation_creating_pin_button_label:I = 0x7f1202eb

.field public static final card_activation_generic_error_message:I = 0x7f1202ec

.field public static final card_activation_generic_error_title:I = 0x7f1202ed

.field public static final card_activation_sending_email:I = 0x7f1202ee

.field public static final card_disabled_message:I = 0x7f120309

.field public static final card_help:I = 0x7f12030e

.field public static final card_ordered_activate_button_text_pending_2fa:I = 0x7f12031d

.field public static final card_ordered_activate_button_text_shipped:I = 0x7f12031e

.field public static final card_ordered_activate_on_dashboard_text:I = 0x7f12031f

.field public static final card_ordered_activate_on_dashboard_title:I = 0x7f120320

.field public static final card_ordered_activation_complete_action_bar:I = 0x7f120321

.field public static final card_ordered_activation_complete_message:I = 0x7f120322

.field public static final card_ordered_activation_complete_title:I = 0x7f120323

.field public static final card_ordered_confirm_address_action_bar:I = 0x7f120324

.field public static final card_ordered_create_pin_action_bar:I = 0x7f120325

.field public static final card_ordered_create_pin_confirmation_hint:I = 0x7f120326

.field public static final card_ordered_create_pin_error_message:I = 0x7f120327

.field public static final card_ordered_create_pin_error_title:I = 0x7f120328

.field public static final card_ordered_create_pin_hint:I = 0x7f120329

.field public static final card_ordered_create_pin_message:I = 0x7f12032a

.field public static final card_ordered_create_pin_weak_error_button_text:I = 0x7f12032b

.field public static final card_ordered_create_pin_weak_error_message:I = 0x7f12032c

.field public static final card_ordered_create_pin_weak_error_title:I = 0x7f12032d

.field public static final card_ordered_deposits_info_action_bar:I = 0x7f12032e

.field public static final card_ordered_deposits_info_link:I = 0x7f12032f

.field public static final card_ordered_deposits_info_link_url:I = 0x7f120330

.field public static final card_ordered_deposits_info_message:I = 0x7f120331

.field public static final card_ordered_deposits_info_title:I = 0x7f120332

.field public static final card_ordered_message_issued:I = 0x7f120333

.field public static final card_ordered_message_pending_2fa:I = 0x7f120334

.field public static final card_ordered_message_shipped:I = 0x7f120335

.field public static final card_ordered_missing_address_message:I = 0x7f120336

.field public static final card_ordered_missing_address_title:I = 0x7f120337

.field public static final card_spend:I = 0x7f12033c

.field public static final card_spend_detail:I = 0x7f12033d

.field public static final card_suspended_dialog_message:I = 0x7f12033e

.field public static final card_suspended_dialog_title:I = 0x7f12033f

.field public static final card_suspended_message:I = 0x7f120340

.field public static final card_suspended_message_learn_more:I = 0x7f120341

.field public static final card_toggle_card_details_label:I = 0x7f120342

.field public static final card_toggle_label:I = 0x7f120343

.field public static final fetching_card_details_spinner_message:I = 0x7f120abb

.field public static final get_help_with_card:I = 0x7f120af0

.field public static final notification_preferences_action_bar_title:I = 0x7f1210b5

.field public static final notification_preferences_email_address_label:I = 0x7f1210b6

.field public static final notification_preferences_email_label:I = 0x7f1210b7

.field public static final notification_preferences_email_message:I = 0x7f1210b8

.field public static final notification_preferences_error_fetching_notifications_title:I = 0x7f1210b9

.field public static final notification_preferences_error_message:I = 0x7f1210ba

.field public static final notification_preferences_error_updating_notifications_title:I = 0x7f1210bb

.field public static final notification_preferences_inscreen_error_updating_notifications:I = 0x7f1210bc

.field public static final notification_preferences_preferences_card_declines:I = 0x7f1210bd

.field public static final notification_preferences_preferences_label:I = 0x7f1210be

.field public static final notification_preferences_top_message:I = 0x7f1210bf

.field public static final order_card_account_owner_row_title:I = 0x7f1211d9

.field public static final order_card_business_info_action_bar:I = 0x7f1211da

.field public static final order_card_business_info_continue_button:I = 0x7f1211db

.field public static final order_card_business_info_help:I = 0x7f1211dc

.field public static final order_card_business_info_help_location:I = 0x7f1211dd

.field public static final order_card_business_info_help_truncation:I = 0x7f1211de

.field public static final order_card_business_info_message:I = 0x7f1211df

.field public static final order_card_business_name_row_title:I = 0x7f1211e0

.field public static final order_card_button_text:I = 0x7f1211e1

.field public static final order_card_customization_error_message:I = 0x7f1211e2

.field public static final order_card_customization_error_title:I = 0x7f1211e3

.field public static final order_card_customization_idv_error_message:I = 0x7f1211e4

.field public static final order_card_customization_idv_error_title:I = 0x7f1211e5

.field public static final order_card_customization_settings_fetch_error_message:I = 0x7f1211e6

.field public static final order_card_customization_settings_fetch_error_title:I = 0x7f1211e7

.field public static final order_card_customization_settings_no_owner_name_error_message:I = 0x7f1211e8

.field public static final order_card_customization_settings_no_owner_name_error_title:I = 0x7f1211e9

.field public static final order_card_customization_too_little_ink_error_message:I = 0x7f1211ea

.field public static final order_card_customization_too_much_ink_error_message:I = 0x7f1211eb

.field public static final order_card_customizing_message:I = 0x7f1211ec

.field public static final order_card_fetching_info_error_message:I = 0x7f1211ed

.field public static final order_card_fetching_info_error_title:I = 0x7f1211ee

.field public static final order_card_generic_error_message:I = 0x7f1211ef

.field public static final order_card_generic_error_title:I = 0x7f1211f0

.field public static final order_card_label:I = 0x7f1211f1

.field public static final order_card_subtitle:I = 0x7f1211f2

.field public static final order_card_terms_of_service_hint:I = 0x7f1211f3

.field public static final order_card_terms_of_service_link:I = 0x7f1211f4

.field public static final order_card_terms_of_service_url:I = 0x7f1211f5

.field public static final order_card_title:I = 0x7f1211f6

.field public static final problem_with_card:I = 0x7f1214ef

.field public static final recent_activity_active_sales:I = 0x7f1215d4

.field public static final recent_activity_balance_title_uppercase:I = 0x7f1215d5

.field public static final recent_activity_card_activity_date_format:I = 0x7f1215d6

.field public static final recent_activity_deposits_title_uppercase:I = 0x7f1215d7

.field public static final recent_activity_empty_message:I = 0x7f1215d8

.field public static final recent_activity_empty_title:I = 0x7f1215d9

.field public static final recent_activity_error_message:I = 0x7f1215da

.field public static final recent_activity_error_title:I = 0x7f1215db

.field public static final recent_activity_pending_deposit:I = 0x7f1215dc

.field public static final recent_activity_sending_date:I = 0x7f1215dd

.field public static final reload_balance:I = 0x7f121660

.field public static final reset_card_pin:I = 0x7f121698

.field public static final reset_square_card_pin_create_code_message:I = 0x7f121699

.field public static final reset_square_card_pin_title:I = 0x7f12169a

.field public static final send_square_card_confirmation_subheading:I = 0x7f1217c2

.field public static final send_square_card_subheading:I = 0x7f1217c3

.field public static final sending_to_bank_uppercase:I = 0x7f1217c4

.field public static final show_private_data_two_factor_title:I = 0x7f1217f4

.field public static final signature_confirm_design_action_bar:I = 0x7f121800

.field public static final signature_hint:I = 0x7f121801

.field public static final signature_personalize_card_action_bar:I = 0x7f121807

.field public static final square_card:I = 0x7f121878

.field public static final square_card_enter_phone_number_help:I = 0x7f121879

.field public static final square_card_enter_phone_number_hint:I = 0x7f12187a

.field public static final square_card_enter_phone_number_message:I = 0x7f12187b

.field public static final square_card_enter_phone_number_submit_text:I = 0x7f12187c

.field public static final square_card_enter_phone_number_title:I = 0x7f12187d

.field public static final square_card_reset_pin_loading:I = 0x7f12187f

.field public static final square_card_reset_pin_success_message:I = 0x7f121880

.field public static final square_card_reset_pin_success_title:I = 0x7f121881

.field public static final square_card_submitting_phone_number:I = 0x7f121882

.field public static final square_card_two_factor_auth_enter_code_message:I = 0x7f121883

.field public static final square_card_two_factor_auth_sending_code:I = 0x7f121884

.field public static final square_card_two_factor_auth_verifying_code:I = 0x7f121885

.field public static final square_card_two_factor_error_expired_token_message:I = 0x7f121886

.field public static final square_card_two_factor_error_expired_token_title:I = 0x7f121887

.field public static final square_card_two_factor_error_generic_message:I = 0x7f121888

.field public static final square_card_two_factor_error_generic_title:I = 0x7f121889

.field public static final square_card_two_factor_error_invalid_token_message:I = 0x7f12188a

.field public static final square_card_two_factor_error_invalid_token_title:I = 0x7f12188b

.field public static final submit_feedback_error_message:I = 0x7f1218d0

.field public static final submit_feedback_error_title:I = 0x7f1218d1

.field public static final submitted_feedback_success_message:I = 0x7f1218d4

.field public static final submitted_feedback_success_message_title:I = 0x7f1218d5

.field public static final submitting_feedback_spinner_messsage:I = 0x7f1218d6

.field public static final view_billing_address_error_message:I = 0x7f121ba3

.field public static final view_billing_address_error_title:I = 0x7f121ba4

.field public static final view_billing_address_field_label:I = 0x7f121ba5

.field public static final view_billing_address_label:I = 0x7f121ba6

.field public static final view_billing_address_loading:I = 0x7f121ba7

.field public static final view_billing_address_message:I = 0x7f121ba8

.field public static final view_notifications_label:I = 0x7f121bad


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
