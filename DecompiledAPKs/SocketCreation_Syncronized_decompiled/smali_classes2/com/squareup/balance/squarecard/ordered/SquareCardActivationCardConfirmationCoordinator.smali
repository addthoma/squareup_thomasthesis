.class public final Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SquareCardActivationCardConfirmationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0001*B5\u0008\u0002\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0010H\u0016J\u0010\u0010\u001a\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0010H\u0002J\u0010\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0010H\u0016J \u0010\u001c\u001a\u00020\u00182\u0016\u0010\u001d\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J\u0010\u0010\u001e\u001a\u00020\u00182\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0010\u0010!\u001a\u00020\u00182\u0006\u0010\"\u001a\u00020#H\u0002J(\u0010$\u001a\u00020\u00182\u0006\u0010%\u001a\u00020&2\u0016\u0010\u001d\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J(\u0010\'\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u00102\u0016\u0010\u001d\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J \u0010(\u001a\u00020\u00182\u0016\u0010\u001d\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J \u0010)\u001a\u00020\u00182\u0016\u0010\u001d\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationScreen;",
        "hudToaster",
        "Lcom/squareup/hudtoaster/HudToaster;",
        "swipeBusWhenVisible",
        "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
        "(Lio/reactivex/Observable;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "continueButton",
        "Landroid/view/View;",
        "squareCardActivationMessage",
        "Lcom/squareup/widgets/MessageView;",
        "squareCardValidationEntryView",
        "Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;",
        "swipeSuccessSubscription",
        "Lio/reactivex/disposables/SerialDisposable;",
        "attach",
        "",
        "view",
        "bindViews",
        "detach",
        "handleBack",
        "screen",
        "onFailedSwipe",
        "ignored",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;",
        "onManualEntryValidityChange",
        "valid",
        "",
        "onSuccessfulSwipe",
        "swipe",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
        "update",
        "updateActionBar",
        "updateLayout",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private continueButton:Landroid/view/View;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
            ">;>;"
        }
    .end annotation
.end field

.field private squareCardActivationMessage:Lcom/squareup/widgets/MessageView;

.field private squareCardValidationEntryView:Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;

.field private final swipeBusWhenVisible:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

.field private final swipeSuccessSubscription:Lio/reactivex/disposables/SerialDisposable;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
            ">;>;",
            "Lcom/squareup/hudtoaster/HudToaster;",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ")V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->swipeBusWhenVisible:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    .line 40
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->swipeSuccessSubscription:Lio/reactivex/disposables/SerialDisposable;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;)V

    return-void
.end method

.method public static final synthetic access$getSquareCardValidationEntryView$p(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;)Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;
    .locals 1

    .line 34
    iget-object p0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->squareCardValidationEntryView:Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;

    if-nez p0, :cond_0

    const-string v0, "squareCardValidationEntryView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->handleBack(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method public static final synthetic access$onFailedSwipe(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->onFailedSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V

    return-void
.end method

.method public static final synthetic access$onManualEntryValidityChange(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;Z)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->onManualEntryValidityChange(Z)V

    return-void
.end method

.method public static final synthetic access$onSuccessfulSwipe(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->onSuccessfulSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method public static final synthetic access$setSquareCardValidationEntryView$p(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->squareCardValidationEntryView:Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 133
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 134
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_validation_entry_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->squareCardValidationEntryView:Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;

    .line 135
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_activation_complete_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->squareCardActivationMessage:Lcom/squareup/widgets/MessageView;

    .line 136
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_confirmation_card_confirmation_continue_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->continueButton:Landroid/view/View;

    return-void
.end method

.method private final handleBack(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
            ">;)V"
        }
    .end annotation

    .line 107
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$GoBack;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final onFailedSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 1

    .line 103
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v0, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_SQUARE_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    check-cast v0, Lcom/squareup/hudtoaster/HudToaster$ToastBundle;

    invoke-interface {p1, v0}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method private final onManualEntryValidityChange(Z)V
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->continueButton:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "continueButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    if-eqz p1, :cond_2

    .line 80
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->squareCardValidationEntryView:Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;

    if-nez p1, :cond_1

    const-string v0, "squareCardValidationEntryView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    :cond_2
    return-void
.end method

.method private final onSuccessfulSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
            ">;)V"
        }
    .end annotation

    .line 99
    iget-object p2, p2, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$CardConfirmationSwipe;

    iget-object p1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    const-string v1, "swipe.card"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$CardConfirmationSwipe;-><init>(Lcom/squareup/Card;)V

    invoke-interface {p2, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
            ">;)V"
        }
    .end annotation

    .line 88
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$update$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$update$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 89
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->updateActionBar(Lcom/squareup/workflow/legacy/Screen;)V

    .line 90
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->updateLayout(Lcom/squareup/workflow/legacy/Screen;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->swipeSuccessSubscription:Lio/reactivex/disposables/SerialDisposable;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->swipeBusWhenVisible:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->successfulSwipes()Lio/reactivex/Observable;

    move-result-object v0

    .line 92
    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$update$2;

    invoke-direct {v1, p0, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$update$2;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p2

    .line 91
    invoke-virtual {p1, p2}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private final updateActionBar(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
            ">;)V"
        }
    .end annotation

    .line 129
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 126
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 127
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirm_card_action_bar:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 128
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$updateActionBar$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$updateActionBar$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 129
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateLayout(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
            ">;)V"
        }
    .end annotation

    .line 111
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->squareCardValidationEntryView:Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;

    const-string v1, "squareCardValidationEntryView"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const-string v3, "screen.data.card.card_brand"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->setCardTenderBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->squareCardValidationEntryView:Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v2, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;->getCvv()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->setCvv(Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->squareCardValidationEntryView:Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;->getExpiration()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->setExpiration(Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->squareCardActivationMessage:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_3

    const-string v1, "squareCardActivationMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;->getCardMessage()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 115
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->continueButton:Landroid/view/View;

    if-nez v0, :cond_4

    const-string v1, "continueButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$updateLayout$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$updateLayout$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->bindViews(Landroid/view/View;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->continueButton:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "continueButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 62
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->squareCardValidationEntryView:Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;

    if-nez v0, :cond_1

    const-string v1, "squareCardValidationEntryView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->onInputsValidityChange()Lrx/Observable;

    move-result-object v0

    .line 63
    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$attach$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$attach$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$sam$rx_functions_Action1$0;

    invoke-direct {v3, v1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$sam$rx_functions_Action1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lrx/functions/Action1;

    invoke-virtual {v0, v3}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "squareCardValidationEntr\u2026anualEntryValidityChange)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->swipeBusWhenVisible:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->failedSwipes()Lio/reactivex/Observable;

    move-result-object v0

    .line 66
    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$attach$2;

    invoke-direct {v1, v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$attach$2;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "swipeBusWhenVisible.fail\u2026ubscribe(::onFailedSwipe)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$attach$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$attach$3;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->detach(Landroid/view/View;)V

    .line 74
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->swipeSuccessSubscription:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {p1}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    return-void
.end method
