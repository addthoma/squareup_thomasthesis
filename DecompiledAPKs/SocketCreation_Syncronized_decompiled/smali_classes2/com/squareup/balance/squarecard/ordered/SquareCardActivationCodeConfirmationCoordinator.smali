.class public final Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SquareCardActivationCodeConfirmationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardActivationCodeConfirmationCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardActivationCodeConfirmationCoordinator.kt\ncom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator\n*L\n1#1,101:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use SquareCardConfirmationCodeCoordinator"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\t\u0008\u0007\u0018\u00002\u00020\u0001B#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0010H\u0016J\u0010\u0010\u0014\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0010H\u0002J \u0010\u0015\u001a\u00020\u00122\u0016\u0010\u0016\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J(\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00102\u0016\u0010\u0016\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J \u0010\u0018\u001a\u00020\u00122\u0016\u0010\u0016\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J\u0008\u0010\u0019\u001a\u00020\u0012H\u0002J(\u0010\u001a\u001a\u00020\u00122\u0016\u0010\u0016\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00072\u0006\u0010\u0013\u001a\u00020\u0010H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$ScreenData;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationScreen;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "codeConfirmationHelp",
        "Lcom/squareup/widgets/MessageView;",
        "codeConfirmationInput",
        "Landroid/widget/EditText;",
        "continueButton",
        "Landroid/view/View;",
        "attach",
        "",
        "view",
        "bindViews",
        "handleBack",
        "screen",
        "update",
        "updateActionBar",
        "updateContinueButton",
        "updateLayout",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private codeConfirmationHelp:Lcom/squareup/widgets/MessageView;

.field private codeConfirmationInput:Landroid/widget/EditText;

.field private continueButton:Landroid/view/View;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getCodeConfirmationInput$p(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;)Landroid/widget/EditText;
    .locals 1

    .line 30
    iget-object p0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->codeConfirmationInput:Landroid/widget/EditText;

    if-nez p0, :cond_0

    const-string v0, "codeConfirmationInput"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->handleBack(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method public static final synthetic access$setCodeConfirmationInput$p(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;Landroid/widget/EditText;)V
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->codeConfirmationInput:Landroid/widget/EditText;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method public static final synthetic access$updateContinueButton(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->updateContinueButton()V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 95
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 96
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_activation_code_confirmation_input:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->codeConfirmationInput:Landroid/widget/EditText;

    .line 97
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_activation_code_confirmation_help:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->codeConfirmationHelp:Lcom/squareup/widgets/MessageView;

    .line 98
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_activation_code_confirmation_continue_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->continueButton:Landroid/view/View;

    return-void
.end method

.method private final handleBack(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event;",
            ">;)V"
        }
    .end annotation

    .line 60
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event$GoBack;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event;",
            ">;)V"
        }
    .end annotation

    .line 54
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$update$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$update$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 55
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->updateActionBar(Lcom/squareup/workflow/legacy/Screen;)V

    .line 56
    invoke-direct {p0, p2, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->updateLayout(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V

    return-void
.end method

.method private final updateActionBar(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event;",
            ">;)V"
        }
    .end annotation

    .line 87
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 84
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 85
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirm_code_action_bar:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 86
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$updateActionBar$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$updateActionBar$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 87
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateContinueButton()V
    .locals 3

    .line 91
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->continueButton:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "continueButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->codeConfirmationInput:Landroid/widget/EditText;

    if-nez v1, :cond_1

    const-string v2, "codeConfirmationInput"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method private final updateLayout(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->codeConfirmationHelp:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "codeConfirmationHelp"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 67
    :cond_0
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {v1, p2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 68
    sget p2, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirm_code_help:I

    const-string v2, "resend_email"

    invoke-virtual {v1, p2, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 69
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirm_code_help_resend_email:I

    invoke-virtual {p2, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 70
    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$updateLayout$1;

    sget v2, Lcom/squareup/noho/R$color;->noho_text_help_link:I

    invoke-direct {v1, p1, v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$updateLayout$1;-><init>(Lcom/squareup/workflow/legacy/Screen;I)V

    check-cast v1, Lcom/squareup/ui/LinkSpan;

    invoke-virtual {p2, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 75
    invoke-virtual {p2}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object p2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->continueButton:Landroid/view/View;

    if-nez p2, :cond_1

    const-string v0, "continueButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 78
    :cond_1
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$updateLayout$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$updateLayout$2;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    check-cast p1, Landroid/view/View$OnClickListener;

    .line 77
    invoke-virtual {p2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->bindViews(Landroid/view/View;)V

    .line 40
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->updateContinueButton()V

    .line 41
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->codeConfirmationInput:Landroid/widget/EditText;

    if-nez v0, :cond_0

    const-string v1, "codeConfirmationInput"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$attach$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;)V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$attach$2;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
