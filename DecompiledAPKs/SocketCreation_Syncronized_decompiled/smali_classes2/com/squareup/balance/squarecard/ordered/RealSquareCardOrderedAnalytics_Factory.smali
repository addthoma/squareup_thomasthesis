.class public final Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics_Factory;
.super Ljava/lang/Object;
.source "RealSquareCardOrderedAnalytics_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;",
            ">;)",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics_Factory;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;-><init>(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics_Factory;->newInstance(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics_Factory;->get()Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;

    move-result-object v0

    return-object v0
.end method
