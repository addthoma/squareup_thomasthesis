.class public interface abstract Lcom/squareup/balance/activity/data/service/BalanceActivityService;
.super Ljava/lang/Object;
.source "BalanceActivityService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\tH\'J\u0018\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u0008\u0008\u0001\u0010\u0005\u001a\u00020\rH\'\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/balance/activity/data/service/BalanceActivityService;",
        "",
        "getUnifiedActivities",
        "Lcom/squareup/balance/activity/data/service/BalanceActivityResponse;",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse;",
        "request",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;",
        "getUnifiedActivityDetails",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest;",
        "setTransactionCategory",
        "Lcom/squareup/server/StatusResponse;",
        "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;",
        "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getUnifiedActivities(Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;)Lcom/squareup/balance/activity/data/service/BalanceActivityResponse;
    .param p1    # Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;",
            ")",
            "Lcom/squareup/balance/activity/data/service/BalanceActivityResponse<",
            "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/get-unified-activities"
    .end annotation
.end method

.method public abstract getUnifiedActivityDetails(Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest;)Lcom/squareup/balance/activity/data/service/BalanceActivityResponse;
    .param p1    # Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest;",
            ")",
            "Lcom/squareup/balance/activity/data/service/BalanceActivityResponse<",
            "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/get-unified-activity-details"
    .end annotation
.end method

.method public abstract setTransactionCategory(Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/set-transaction-category"
    .end annotation
.end method
