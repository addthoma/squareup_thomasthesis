.class final Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;
.super Ljava/lang/Object;
.source "RealBalanceActivityRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->fetchMoreBalanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/balance/activity/data/BalanceActivity;",
        "kotlin.jvm.PlatformType",
        "it",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $type:Lcom/squareup/balance/activity/data/BalanceActivityType;

.field final synthetic this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;Lcom/squareup/balance/activity/data/BalanceActivityType;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;->this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;

    iput-object p2, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;->$type:Lcom/squareup/balance/activity/data/BalanceActivityType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/balance/activity/data/BalanceActivity;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivity;->getPendingResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/balance/activity/data/UnifiedActivityResult$Loading;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivity;->getCompletedResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/balance/activity/data/UnifiedActivityResult$Loading;

    if-eqz v0, :cond_0

    .line 55
    iget-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;->this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;

    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;->$type:Lcom/squareup/balance/activity/data/BalanceActivityType;

    invoke-virtual {p1, v0}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->balanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;)Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_0

    .line 57
    :cond_0
    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "Observable.just(it)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    :goto_0
    new-instance v0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1$1;-><init>(Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/balance/activity/data/BalanceActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;->apply(Lcom/squareup/balance/activity/data/BalanceActivity;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
