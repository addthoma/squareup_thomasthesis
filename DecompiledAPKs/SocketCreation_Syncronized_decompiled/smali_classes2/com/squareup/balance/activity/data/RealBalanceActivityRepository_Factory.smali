.class public final Lcom/squareup/balance/activity/data/RealBalanceActivityRepository_Factory;
.super Ljava/lang/Object;
.source "RealBalanceActivityRepository_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/balance/activity/data/RealBalanceActivityRepository_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;",
            ">;)",
            "Lcom/squareup/balance/activity/data/RealBalanceActivityRepository_Factory;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;)Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;

    invoke-direct {v0, p0}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;-><init>(Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;

    invoke-static {v0}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository_Factory;->newInstance(Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;)Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository_Factory;->get()Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;

    move-result-object v0

    return-object v0
.end method
