.class final Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$updateCategory$1;
.super Ljava/lang/Object;
.source "RealRemoteBalanceActivityDetailsDataStore.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore;->updateCategory(Ljava/lang/String;Z)Lio/reactivex/Completable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "received",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$updateCategory$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$updateCategory$1;

    invoke-direct {v0}, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$updateCategory$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$updateCategory$1;->INSTANCE:Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$updateCategory$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$updateCategory$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;",
            ">;)V"
        }
    .end annotation

    const-string v0, "received"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz p1, :cond_0

    return-void

    .line 53
    :cond_0
    sget-object p1, Lcom/squareup/balance/activity/data/FailureUpdatingCategory;->INSTANCE:Lcom/squareup/balance/activity/data/FailureUpdatingCategory;

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
