.class public final Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;
.super Ljava/lang/Object;
.source "DisplayActivitiesListChildRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$Factory;,
        Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisplayActivitiesListChildRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisplayActivitiesListChildRunner.kt\ncom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 BinderRowSpec.kt\ncom/squareup/cycler/BinderRowSpec\n+ 7 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 8 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,211:1\n49#2:212\n50#2,3:218\n53#2:285\n599#3,4:213\n601#3:217\n32#4,3:221\n36#4:234\n342#5,5:224\n344#5,2:229\n347#5:233\n310#5,3:235\n313#5,3:244\n310#5,3:247\n313#5,3:256\n310#5,3:259\n313#5,3:268\n310#5,3:271\n313#5,3:280\n24#6,2:231\n35#7,6:238\n35#7,6:250\n35#7,6:262\n35#7,6:274\n43#8,2:283\n*E\n*S KotlinDebug\n*F\n+ 1 DisplayActivitiesListChildRunner.kt\ncom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner\n*L\n62#1:212\n62#1,3:218\n62#1:285\n62#1,4:213\n62#1:217\n62#1,3:221\n62#1:234\n62#1,5:224\n62#1,2:229\n62#1:233\n62#1,3:235\n62#1,3:244\n62#1,3:247\n62#1,3:256\n62#1,3:259\n62#1,3:268\n62#1,3:271\n62#1,3:280\n62#1,2:231\n62#1,6:238\n62#1,6:250\n62#1,6:262\n62#1,6:274\n62#1,2:283\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0019\u001aB\u0017\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0002H\u0002J\u0008\u0010\u0015\u001a\u00020\u0011H\u0002J\u0018\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u0018H\u0016R\u0010\u0010\u0008\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n \n*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\n \n*\u0004\u0018\u00010\u000c0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;",
        "runnerView",
        "Landroid/view/View;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V",
        "lastScreen",
        "loading",
        "kotlin.jvm.PlatformType",
        "pullToRefresh",
        "Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;",
        "showData",
        "",
        "result",
        "Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Success;",
        "rendering",
        "showLoading",
        "showRendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Binding",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private lastScreen:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;

.field private final loading:Landroid/view/View;

.field private final pullToRefresh:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 3

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    sget v0, Lcom/squareup/balance/activity/impl/R$id;->balance_activities_loading:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->loading:Landroid/view/View;

    .line 57
    sget v0, Lcom/squareup/balance/activity/impl/R$id;->balance_activities_pull_to_refresh:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->pullToRefresh:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    .line 62
    sget v0, Lcom/squareup/balance/activity/impl/R$id;->balance_activities_list:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "runnerView.findViewById(\u2026.balance_activities_list)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    .line 212
    sget-object v0, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 213
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 214
    new-instance v0, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v0}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 218
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 219
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 222
    sget-object p2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$nohoRow$1;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$nohoRow$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 225
    new-instance v1, Lcom/squareup/cycler/BinderRowSpec;

    .line 229
    sget-object v2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$nohoRow$2;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$nohoRow$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 225
    invoke-direct {v1, v2, p2}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 231
    new-instance p2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1;

    invoke-direct {p2, p0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1;-><init>(Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;)V

    check-cast p2, Lkotlin/jvm/functions/Function3;

    invoke-virtual {v1, p2}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 228
    check-cast v1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 224
    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 236
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$row$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 95
    sget v1, Lcom/squareup/balance/activity/impl/R$layout;->balance_activities_view_item_title:I

    .line 238
    new-instance v2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$create$1;

    invoke-direct {v2, v1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$create$1;-><init>(I)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v2}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 236
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 235
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 248
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$row$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 110
    sget v1, Lcom/squareup/balance/activity/impl/R$layout;->balance_activities_load_more:I

    .line 250
    new-instance v2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$2;

    invoke-direct {v2, v1, p0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$2;-><init>(ILcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v2}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 248
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 247
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 260
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$row$3;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$row$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 116
    sget v1, Lcom/squareup/balance/activity/impl/R$layout;->balance_activities_error:I

    .line 262
    new-instance v2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$3;

    invoke-direct {v2, v1, p0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$3;-><init>(ILcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v2}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 260
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 259
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 272
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$row$4;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$row$4;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 127
    sget v1, Lcom/squareup/balance/activity/impl/R$layout;->balance_activities_empty:I

    .line 274
    new-instance v2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$4;

    invoke-direct {v2, v1, p0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$4;-><init>(ILcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v2}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 151
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$1$5$2;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$1$5$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 271
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 283
    new-instance p2, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {p2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/16 v1, 0x8

    .line 157
    invoke-virtual {p2, v1}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    .line 158
    check-cast p2, Lcom/squareup/cycler/ExtensionSpec;

    .line 283
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 216
    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->recycler:Lcom/squareup/cycler/Recycler;

    return-void

    .line 213
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public synthetic constructor <init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;-><init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V

    return-void
.end method

.method public static final synthetic access$getLastScreen$p(Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->lastScreen:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;

    return-object p0
.end method

.method public static final synthetic access$setLastScreen$p(Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->lastScreen:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;

    return-void
.end method

.method private final showData(Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Success;Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;)V
    .locals 2

    .line 181
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->loading:Landroid/view/View;

    const-string v1, "loading"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 183
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->pullToRefresh:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const-string v1, "pullToRefresh"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 184
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->pullToRefresh:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    new-instance v1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$showData$1;

    invoke-direct {v1, p2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$showData$1;-><init>(Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;)V

    check-cast v1, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$OnRefreshListener;

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$OnRefreshListener;)V

    .line 188
    iget-object p2, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->recycler:Lcom/squareup/cycler/Recycler;

    new-instance v0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$showData$2;

    invoke-direct {v0, p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$showData$2;-><init>(Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Success;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p2, v0}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final showLoading()V
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->pullToRefresh:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const-string v1, "pullToRefresh"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 197
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->pullToRefresh:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$OnRefreshListener;)V

    .line 199
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->loading:Landroid/view/View;

    const-string v1, "loading"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 200
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->recycler:Lcom/squareup/cycler/Recycler;

    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler;->clear()V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->lastScreen:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;

    .line 169
    iget-object p2, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->pullToRefresh:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const-string v0, "pullToRefresh"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 171
    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;->getBalanceActivityResult()Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;

    move-result-object p2

    .line 172
    sget-object v0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Loading;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Loading;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->showLoading()V

    goto :goto_0

    .line 173
    :cond_0
    instance-of v0, p2, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Success;

    if-eqz v0, :cond_1

    check-cast p2, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Success;

    invoke-direct {p0, p2, p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->showData(Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Success;Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->showRendering(Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
