.class final Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateDescriptions$$inlined$map$lambda$1;
.super Ljava/lang/Object;
.source "BalanceActivityDetailsSuccessLayoutRunner.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->populateDescriptions(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005\u00a8\u0006\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick",
        "com/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateDescriptions$1$1$1",
        "com/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$$special$$inlined$apply$lambda$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context$inlined:Landroid/content/Context;

.field final synthetic $description$inlined:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

.field final synthetic $onClick:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;

.field final synthetic $screen$inlined:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

.field final synthetic this$0:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;Landroid/content/Context;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateDescriptions$$inlined$map$lambda$1;->$onClick:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateDescriptions$$inlined$map$lambda$1;->$description$inlined:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    iput-object p3, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateDescriptions$$inlined$map$lambda$1;->$context$inlined:Landroid/content/Context;

    iput-object p4, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateDescriptions$$inlined$map$lambda$1;->this$0:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;

    iput-object p5, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateDescriptions$$inlined$map$lambda$1;->$screen$inlined:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    .line 204
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateDescriptions$$inlined$map$lambda$1;->$screen$inlined:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->getOnCommunityRewardTapped()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateDescriptions$$inlined$map$lambda$1;->$onClick:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;

    check-cast v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick$CommunityReward;

    invoke-virtual {v0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick$CommunityReward;->getModifier()Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
