.class public abstract Lcom/squareup/balance/activity/ui/common/Amount;
.super Ljava/lang/Object;
.source "Amount.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/common/Amount$CrossedOutAmount;,
        Lcom/squareup/balance/activity/ui/common/Amount$PositiveAmount;,
        Lcom/squareup/balance/activity/ui/common/Amount$UndecoratedAmount;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0018\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007\u0082\u0001\u0003\u000b\u000c\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/common/Amount;",
        "Landroid/os/Parcelable;",
        "()V",
        "text",
        "Lcom/squareup/resources/TextModel;",
        "",
        "getText",
        "()Lcom/squareup/resources/TextModel;",
        "CrossedOutAmount",
        "PositiveAmount",
        "UndecoratedAmount",
        "Lcom/squareup/balance/activity/ui/common/Amount$CrossedOutAmount;",
        "Lcom/squareup/balance/activity/ui/common/Amount$PositiveAmount;",
        "Lcom/squareup/balance/activity/ui/common/Amount$UndecoratedAmount;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/squareup/balance/activity/ui/common/Amount;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getText()Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end method
