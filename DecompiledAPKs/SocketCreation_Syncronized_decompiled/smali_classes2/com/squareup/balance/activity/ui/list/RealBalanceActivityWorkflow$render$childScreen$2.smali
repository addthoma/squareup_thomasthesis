.class final Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$render$childScreen$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealBalanceActivityWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;->render(Lkotlin/Unit;Lcom/squareup/balance/activity/ui/list/BalanceActivityState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromDetails;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromDetails;",
        "it",
        "",
        "invoke",
        "(Lkotlin/Unit;)Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromDetails;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/balance/activity/ui/list/BalanceActivityState;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/ui/list/BalanceActivityState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$render$childScreen$2;->$state:Lcom/squareup/balance/activity/ui/list/BalanceActivityState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlin/Unit;)Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromDetails;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    new-instance p1, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromDetails;

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$render$childScreen$2;->$state:Lcom/squareup/balance/activity/ui/list/BalanceActivityState;

    invoke-virtual {v0}, Lcom/squareup/balance/activity/ui/list/BalanceActivityState;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromDetails;-><init>(Lcom/squareup/balance/activity/data/BalanceActivityType;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$render$childScreen$2;->invoke(Lkotlin/Unit;)Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromDetails;

    move-result-object p1

    return-object p1
.end method
