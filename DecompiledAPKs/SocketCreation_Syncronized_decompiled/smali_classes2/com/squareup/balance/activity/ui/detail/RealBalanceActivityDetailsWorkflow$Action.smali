.class abstract Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action;
.super Ljava/lang/Object;
.source "RealBalanceActivityDetailsWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$DisplayDetails;,
        Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$DisplayFetchingError;,
        Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$GoBack;,
        Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$RefreshActivityDetails;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0004\u0008\t\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0019\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016\u00a2\u0006\u0002\u0010\u0007\u0082\u0001\u0004\u000c\r\u000e\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;",
        "",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;",
        "DisplayDetails",
        "DisplayFetchingError",
        "GoBack",
        "RefreshActivityDetails",
        "Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$DisplayDetails;",
        "Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$DisplayFetchingError;",
        "Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$GoBack;",
        "Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$RefreshActivityDetails;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 113
    invoke-direct {p0}, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 113
    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;",
            ">;)",
            "Lkotlin/Unit;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    instance-of v0, p0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$DisplayDetails;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 117
    new-instance v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState$DisplayingBalanceActivityDetails;

    move-object v2, p0

    check-cast v2, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$DisplayDetails;

    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$DisplayDetails;->getDetails()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState$DisplayingBalanceActivityDetails;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 120
    :cond_0
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$DisplayFetchingError;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$DisplayFetchingError;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState$DisplayingBalanceActivityFetchingError;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState$DisplayingBalanceActivityFetchingError;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 124
    :cond_1
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$RefreshActivityDetails;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$RefreshActivityDetails;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 125
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState$FetchingBalanceActivityDetails;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState$FetchingBalanceActivityDetails;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 128
    :cond_2
    sget-object p1, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$GoBack;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$GoBack;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    :goto_0
    return-object v1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
