.class public final Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "BalanceActivityDetailsSuccessWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBalanceActivityDetailsSuccessWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BalanceActivityDetailsSuccessWorkflow.kt\ncom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 4 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 5 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 6 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,204:1\n32#2,12:205\n149#3,5:217\n149#3,5:222\n149#3,5:227\n149#3,5:232\n85#4:237\n240#5:238\n276#6:239\n*E\n*S KotlinDebug\n*F\n+ 1 BalanceActivityDetailsSuccessWorkflow.kt\ncom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow\n*L\n48#1,12:205\n64#1,5:217\n87#1,5:222\n105#1,5:227\n106#1,5:232\n132#1:237\n132#1:238\n132#1:239\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u000026\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0002`\t0\u0001:\u0001*B\u001f\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J:\u0010\u0011\u001a\u00020\u00122\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u00142\u0008\u0008\u0002\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001bH\u0002J\u001a\u0010\u001d\u001a\u00020\u00032\u0006\u0010\u001e\u001a\u00020\u00022\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J\u0018\u0010!\u001a\u00020\u00192\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\"\u001a\u00020\u0014H\u0002JH\u0010#\u001a\u001e\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0002`\t2\u0006\u0010\u001e\u001a\u00020\u00022\u0006\u0010$\u001a\u00020\u00032\u0012\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040&H\u0016J\u0010\u0010\'\u001a\u00020 2\u0006\u0010$\u001a\u00020\u0003H\u0016J\u001e\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u00020)2\u0006\u0010\u0018\u001a\u00020\u00022\u0006\u0010\"\u001a\u00020\u0014H\u0002R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;",
        "",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityScreen;",
        "mapper",
        "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;",
        "detailsRepository",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;",
        "communityRewardWorkflow",
        "Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;",
        "(Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;)V",
        "balanceActivityDetailScreen",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;",
        "displayError",
        "",
        "isUpdatingCategories",
        "activity",
        "Lcom/squareup/protos/bizbank/UnifiedActivity;",
        "details",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails;",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "pendingExpenseTypeChangeDetails",
        "isPersonal",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "updateCategory",
        "Lcom/squareup/workflow/Worker;",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final communityRewardWorkflow:Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;

.field private final detailsRepository:Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;

.field private final mapper:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "mapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "detailsRepository"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "communityRewardWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->mapper:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->detailsRepository:Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;

    iput-object p3, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->communityRewardWorkflow:Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;

    return-void
.end method

.method private final balanceActivityDetailScreen(ZZLcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Lcom/squareup/workflow/Sink;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lcom/squareup/protos/bizbank/UnifiedActivity;",
            "Lcom/squareup/protos/bizbank/UnifiedActivityDetails;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action;",
            ">;)",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    .line 142
    new-instance v16, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    .line 143
    iget-object v4, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->mapper:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;

    invoke-virtual {v4, v1}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->actionBarTitle(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/util/ViewString;

    move-result-object v4

    .line 144
    iget-object v5, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->mapper:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;

    invoke-virtual {v5, v1}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->amount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/balance/activity/ui/common/Amount;

    move-result-object v5

    .line 145
    iget-object v6, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->mapper:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;

    invoke-virtual {v6, v1, v2}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->expenseType(Lcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;

    move-result-object v8

    .line 147
    iget-object v6, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->mapper:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;

    invoke-virtual {v6, v1}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->mainDescription(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    move-result-object v6

    .line 148
    iget-object v7, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->mapper:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;

    invoke-virtual {v7, v1, v2}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->itemizedDescriptions(Lcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)Ljava/util/List;

    move-result-object v7

    .line 149
    iget-object v9, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->mapper:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;

    invoke-virtual {v9, v1}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->merchantImage(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;

    move-result-object v9

    .line 151
    iget-object v10, v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->mapper:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;

    invoke-virtual {v10, v1}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->footnoteMessage(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;

    move-result-object v10

    .line 152
    new-instance v1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$balanceActivityDetailScreen$1;

    invoke-direct {v1, v3}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$balanceActivityDetailScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v14, v1

    check-cast v14, Lkotlin/jvm/functions/Function0;

    .line 153
    new-instance v1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$balanceActivityDetailScreen$2;

    invoke-direct {v1, v3, v2}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$balanceActivityDetailScreen$2;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)V

    move-object v11, v1

    check-cast v11, Lkotlin/jvm/functions/Function0;

    .line 156
    new-instance v1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$balanceActivityDetailScreen$3;

    invoke-direct {v1, v3, v2}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$balanceActivityDetailScreen$3;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)V

    move-object v13, v1

    check-cast v13, Lkotlin/jvm/functions/Function0;

    .line 159
    new-instance v1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$balanceActivityDetailScreen$4;

    invoke-direct {v1, v3, v2}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$balanceActivityDetailScreen$4;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)V

    move-object v12, v1

    check-cast v12, Lkotlin/jvm/functions/Function0;

    .line 162
    new-instance v1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$balanceActivityDetailScreen$5;

    invoke-direct {v1, v3, v2}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$balanceActivityDetailScreen$5;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)V

    move-object v15, v1

    check-cast v15, Lkotlin/jvm/functions/Function1;

    move-object/from16 v1, v16

    move-object v2, v4

    move-object v3, v9

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v10

    move/from16 v9, p1

    move/from16 v10, p2

    .line 142
    invoke-direct/range {v1 .. v15}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;Lcom/squareup/balance/activity/ui/common/Amount;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;Ljava/util/List;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    return-object v16
.end method

.method static synthetic balanceActivityDetailScreen$default(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;ZZLcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Lcom/squareup/workflow/Sink;ILjava/lang/Object;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;
    .locals 7

    and-int/lit8 p7, p6, 0x1

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 p1, p6, 0x2

    if-eqz p1, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    move v3, p2

    :goto_1
    move-object v1, p0

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 137
    invoke-direct/range {v1 .. v6}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->balanceActivityDetailScreen(ZZLcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Lcom/squareup/workflow/Sink;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    move-result-object p0

    return-object p0
.end method

.method private final pendingExpenseTypeChangeDetails(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Z)Lcom/squareup/protos/bizbank/UnifiedActivityDetails;
    .locals 0

    .line 119
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->newBuilder()Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;

    move-result-object p1

    .line 120
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->is_personal_expense(Ljava/lang/Boolean;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;

    move-result-object p1

    .line 121
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->build()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object p1

    const-string p2, "details.newBuilder()\n   \u2026ersonal)\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final updateCategory(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Z)Lcom/squareup/workflow/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
            "Z)",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
            ">;"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 129
    iget-object p2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->detailsRepository:Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;

    invoke-interface {p2, p1}, Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;->updateCategoryToPersonal(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 131
    :cond_0
    iget-object p2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->detailsRepository:Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;

    invoke-interface {p2, p1}, Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;->updateCategoryToBusiness(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)Lio/reactivex/Single;

    move-result-object p1

    .line 237
    :goto_0
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$updateCategory$$inlined$asWorker$1;

    const/4 v0, 0x0

    invoke-direct {p2, p1, v0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$updateCategory$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 238
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 239
    const-class p2, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;
    .locals 4

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz p2, :cond_4

    .line 205
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v0

    :goto_1
    if-eqz p2, :cond_3

    .line 210
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    const-string v3, "Parcel.obtain()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 212
    array-length v3, p2

    invoke-virtual {v2, p2, v1, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 213
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 214
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v2, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v3, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    goto :goto_2

    :cond_3
    move-object p2, v0

    .line 216
    :goto_2
    check-cast p2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;

    if-eqz p2, :cond_4

    goto :goto_3

    .line 49
    :cond_4
    new-instance p2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;->getUnifiedActivityDetails()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object p1

    const/4 v2, 0x2

    invoke-direct {p2, p1, v1, v2, v0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;

    :goto_3
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->initialState(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    check-cast p2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->render(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v8, p0

    move-object/from16 v9, p2

    const-string v0, "props"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    move-object/from16 v10, p3

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-interface/range {p3 .. p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v11

    .line 59
    instance-of v0, v9, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;

    const-string v12, ""

    if-eqz v0, :cond_0

    .line 60
    move-object v0, v9

    check-cast v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;

    invoke-virtual {v0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->getDisplayError()Z

    move-result v2

    const/4 v3, 0x0

    .line 61
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;->getUnifiedActivity()Lcom/squareup/protos/bizbank/UnifiedActivity;

    move-result-object v4

    .line 62
    invoke-virtual {v0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingBalanceActivityData;->getDetails()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object v5

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move v1, v2

    move v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v11

    .line 59
    invoke-static/range {v0 .. v7}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->balanceActivityDetailScreen$default(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;ZZLcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Lcom/squareup/workflow/Sink;ILjava/lang/Object;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 218
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 219
    const-class v2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v12}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 220
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 218
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 65
    sget-object v0, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    goto/16 :goto_0

    .line 66
    :cond_0
    instance-of v0, v9, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$UpdatingDetailsCategory;

    if-eqz v0, :cond_1

    .line 69
    new-instance v0, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    .line 70
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;->getUnifiedActivity()Lcom/squareup/protos/bizbank/UnifiedActivity;

    move-result-object v2

    .line 71
    move-object v13, v9

    check-cast v13, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$UpdatingDetailsCategory;

    invoke-virtual {v13}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$UpdatingDetailsCategory;->getDetails()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object v3

    .line 69
    invoke-direct {v0, v2, v3}, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)V

    .line 73
    invoke-virtual {v13}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$UpdatingDetailsCategory;->isPersonal()Z

    move-result v2

    .line 68
    invoke-direct {v8, v0, v2}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->updateCategory(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Z)Lcom/squareup/workflow/Worker;

    move-result-object v3

    const/4 v4, 0x0

    .line 75
    new-instance v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$render$1;

    invoke-direct {v0, v9}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$render$1;-><init>(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object/from16 v2, p3

    .line 67
    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 84
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;->getUnifiedActivity()Lcom/squareup/protos/bizbank/UnifiedActivity;

    move-result-object v4

    .line 85
    invoke-virtual {v13}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$UpdatingDetailsCategory;->getDetails()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object v0

    invoke-virtual {v13}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$UpdatingDetailsCategory;->isPersonal()Z

    move-result v1

    invoke-direct {v8, v0, v1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->pendingExpenseTypeChangeDetails(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Z)Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object v5

    const/4 v6, 0x1

    move-object/from16 v0, p0

    move v1, v2

    move v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v11

    .line 82
    invoke-static/range {v0 .. v7}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->balanceActivityDetailScreen$default(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;ZZLcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Lcom/squareup/workflow/Sink;ILjava/lang/Object;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 223
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 224
    const-class v2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v12}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 225
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 223
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 88
    sget-object v0, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    goto/16 :goto_0

    .line 90
    :cond_1
    instance-of v0, v9, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingCommunityReward;

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 92
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;->getUnifiedActivity()Lcom/squareup/protos/bizbank/UnifiedActivity;

    move-result-object v4

    .line 93
    move-object v13, v9

    check-cast v13, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingCommunityReward;

    invoke-virtual {v13}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingCommunityReward;->getDetails()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object v5

    const/4 v6, 0x3

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move v1, v2

    move v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v11

    .line 91
    invoke-static/range {v0 .. v7}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->balanceActivityDetailScreen$default(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;ZZLcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Lcom/squareup/workflow/Sink;ILjava/lang/Object;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    move-result-object v0

    .line 97
    iget-object v1, v8, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->communityRewardWorkflow:Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;

    move-object v2, v1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 98
    new-instance v3, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardProps;

    invoke-virtual {v13}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingCommunityReward;->getDetails()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object v1

    invoke-virtual {v13}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingCommunityReward;->getModifier()Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardProps;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;)V

    const/4 v4, 0x0

    .line 99
    new-instance v1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$render$dialog$1;

    invoke-direct {v1, v9}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$render$dialog$1;-><init>(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;)V

    move-object v5, v1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    move-object/from16 v1, p3

    .line 96
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardDialogScreen;

    .line 104
    sget-object v13, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 105
    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 228
    new-instance v14, Lcom/squareup/workflow/legacy/Screen;

    .line 229
    const-class v2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v12}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 230
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 228
    invoke-direct {v14, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 106
    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 233
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    .line 234
    const-class v2, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardDialogScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v12}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 235
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 233
    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/16 v18, 0x6

    const/16 v19, 0x0

    move-object/from16 v17, v0

    .line 104
    invoke-static/range {v13 .. v19}, Lcom/squareup/container/PosLayering$Companion;->dialogStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public snapshotState(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->snapshotState(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
