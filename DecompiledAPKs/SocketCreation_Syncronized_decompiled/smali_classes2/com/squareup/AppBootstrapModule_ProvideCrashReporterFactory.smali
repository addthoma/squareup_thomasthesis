.class public final Lcom/squareup/AppBootstrapModule_ProvideCrashReporterFactory;
.super Ljava/lang/Object;
.source "AppBootstrapModule_ProvideCrashReporterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/log/CrashReporter;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/AppBootstrapModule;


# direct methods
.method public constructor <init>(Lcom/squareup/AppBootstrapModule;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/AppBootstrapModule_ProvideCrashReporterFactory;->module:Lcom/squareup/AppBootstrapModule;

    return-void
.end method

.method public static create(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/AppBootstrapModule_ProvideCrashReporterFactory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/AppBootstrapModule_ProvideCrashReporterFactory;

    invoke-direct {v0, p0}, Lcom/squareup/AppBootstrapModule_ProvideCrashReporterFactory;-><init>(Lcom/squareup/AppBootstrapModule;)V

    return-object v0
.end method

.method public static provideCrashReporter(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/log/CrashReporter;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/AppBootstrapModule;->provideCrashReporter()Lcom/squareup/log/CrashReporter;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/log/CrashReporter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/log/CrashReporter;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/AppBootstrapModule_ProvideCrashReporterFactory;->module:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideCrashReporterFactory;->provideCrashReporter(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/log/CrashReporter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/AppBootstrapModule_ProvideCrashReporterFactory;->get()Lcom/squareup/log/CrashReporter;

    move-result-object v0

    return-object v0
.end method
