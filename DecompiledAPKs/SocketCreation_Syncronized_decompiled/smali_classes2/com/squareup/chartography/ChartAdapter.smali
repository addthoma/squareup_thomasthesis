.class public abstract Lcom/squareup/chartography/ChartAdapter;
.super Ljava/lang/Object;
.source "ChartAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DataT:",
        "Lcom/squareup/chartography/DataPoint<",
        "**>;>",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChartAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChartAdapter.kt\ncom/squareup/chartography/ChartAdapter\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,75:1\n1642#2,2:76\n*E\n*S KotlinDebug\n*F\n+ 1 ChartAdapter.kt\ncom/squareup/chartography/ChartAdapter\n*L\n35#1,2:76\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0006\n\u0002\u0008\t\n\u0002\u0010\u0011\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0008\u0008&\u0018\u0000*\u0010\u0008\u0000\u0010\u0001*\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00022\u00020\u0003B/\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0007H&J\u0010\u0010\u001f\u001a\u00020\u001d2\u0006\u0010 \u001a\u00020\u0010H&J\u0016\u0010!\u001a\u00020\u00102\u0006\u0010\"\u001a\u00020\u00072\u0006\u0010#\u001a\u00020\u0007J\u000e\u0010$\u001a\u00020\u00102\u0006\u0010\u001e\u001a\u00020\u0007R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u001a\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0013\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0015R\u0011\u0010\u0017\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0015R\u0016\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u001aX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u001b\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/chartography/ChartAdapter;",
        "DataT",
        "Lcom/squareup/chartography/DataPoint;",
        "",
        "data",
        "",
        "numDimensions",
        "",
        "targetRangeSteps",
        "allowFractionalSteps",
        "",
        "(Ljava/util/List;IIZ)V",
        "getData",
        "()Ljava/util/List;",
        "dataRangeExtent",
        "Lkotlin/ranges/ClosedRange;",
        "",
        "getDataRangeExtent",
        "()Lkotlin/ranges/ClosedRange;",
        "domainNumSteps",
        "getDomainNumSteps",
        "()I",
        "getNumDimensions",
        "rangeNumSteps",
        "getRangeNumSteps",
        "rangeSteps",
        "",
        "[Ljava/lang/Double;",
        "domainLabel",
        "",
        "step",
        "rangeLabel",
        "value",
        "rangePercentage",
        "domainStep",
        "dimension",
        "rangeStepValue",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TDataT;>;"
        }
    .end annotation
.end field

.field private final domainNumSteps:I

.field private final numDimensions:I

.field private final rangeNumSteps:I

.field private final rangeSteps:[Ljava/lang/Double;


# direct methods
.method public constructor <init>(Ljava/util/List;IIZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+TDataT;>;IIZ)V"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/chartography/ChartAdapter;->data:Ljava/util/List;

    iput p2, p0, Lcom/squareup/chartography/ChartAdapter;->numDimensions:I

    .line 28
    sget-object p1, Lcom/squareup/chartography/util/D3Util;->INSTANCE:Lcom/squareup/chartography/util/D3Util;

    invoke-direct {p0}, Lcom/squareup/chartography/ChartAdapter;->getDataRangeExtent()Lkotlin/ranges/ClosedRange;

    move-result-object p2

    invoke-virtual {p1, p2, p3, p4}, Lcom/squareup/chartography/util/D3Util;->steps(Lkotlin/ranges/ClosedRange;IZ)[Ljava/lang/Double;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/chartography/ChartAdapter;->rangeSteps:[Ljava/lang/Double;

    .line 45
    iget-object p1, p0, Lcom/squareup/chartography/ChartAdapter;->data:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    iput p1, p0, Lcom/squareup/chartography/ChartAdapter;->domainNumSteps:I

    .line 48
    iget-object p1, p0, Lcom/squareup/chartography/ChartAdapter;->rangeSteps:[Ljava/lang/Double;

    array-length p1, p1

    iput p1, p0, Lcom/squareup/chartography/ChartAdapter;->rangeNumSteps:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;IIZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_0

    const/4 p3, 0x5

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    const/4 p4, 0x1

    .line 24
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/chartography/ChartAdapter;-><init>(Ljava/util/List;IIZ)V

    return-void
.end method

.method private final getDataRangeExtent()Lkotlin/ranges/ClosedRange;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/ranges/ClosedRange<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/chartography/ChartAdapter;->data:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 76
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v1, 0x0

    move-wide v3, v1

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/chartography/DataPoint;

    .line 36
    invoke-virtual {v5}, Lcom/squareup/chartography/DataPoint;->getExtent()Lkotlin/ranges/ClosedRange;

    move-result-object v5

    .line 37
    invoke-interface {v5}, Lkotlin/ranges/ClosedRange;->getStart()Ljava/lang/Comparable;

    move-result-object v6

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v6

    invoke-static {v6, v7, v1, v2}, Ljava/lang/Math;->min(DD)D

    move-result-wide v1

    .line 38
    invoke-interface {v5}, Lkotlin/ranges/ClosedRange;->getEndInclusive()Ljava/lang/Comparable;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v5

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->max(DD)D

    move-result-wide v3

    goto :goto_0

    .line 41
    :cond_0
    invoke-static {v1, v2, v3, v4}, Lkotlin/ranges/RangesKt;->rangeTo(DD)Lkotlin/ranges/ClosedFloatingPointRange;

    move-result-object v0

    check-cast v0, Lkotlin/ranges/ClosedRange;

    return-object v0
.end method


# virtual methods
.method public abstract domainLabel(I)Ljava/lang/String;
.end method

.method public final getData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TDataT;>;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/chartography/ChartAdapter;->data:Ljava/util/List;

    return-object v0
.end method

.method public final getDomainNumSteps()I
    .locals 1

    .line 45
    iget v0, p0, Lcom/squareup/chartography/ChartAdapter;->domainNumSteps:I

    return v0
.end method

.method public final getNumDimensions()I
    .locals 1

    .line 22
    iget v0, p0, Lcom/squareup/chartography/ChartAdapter;->numDimensions:I

    return v0
.end method

.method public final getRangeNumSteps()I
    .locals 1

    .line 48
    iget v0, p0, Lcom/squareup/chartography/ChartAdapter;->rangeNumSteps:I

    return v0
.end method

.method public abstract rangeLabel(D)Ljava/lang/String;
.end method

.method public final rangePercentage(II)D
    .locals 4

    .line 55
    iget-object v0, p0, Lcom/squareup/chartography/ChartAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/chartography/DataPoint;

    invoke-virtual {p1}, Lcom/squareup/chartography/DataPoint;->getRanges()[Ljava/lang/Number;

    move-result-object p1

    aget-object p1, p1, p2

    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide p1

    .line 56
    iget-object v0, p0, Lcom/squareup/chartography/ChartAdapter;->rangeSteps:[Ljava/lang/Double;

    invoke-static {v0}, Lkotlin/collections/ArraysKt;->first([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    sub-double/2addr p1, v0

    iget-object v0, p0, Lcom/squareup/chartography/ChartAdapter;->rangeSteps:[Ljava/lang/Double;

    invoke-static {v0}, Lkotlin/collections/ArraysKt;->last([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/chartography/ChartAdapter;->rangeSteps:[Ljava/lang/Double;

    invoke-static {v2}, Lkotlin/collections/ArraysKt;->first([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    sub-double/2addr v0, v2

    div-double/2addr p1, v0

    return-wide p1
.end method

.method public final rangeStepValue(I)D
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/squareup/chartography/ChartAdapter;->rangeSteps:[Ljava/lang/Double;

    aget-object p1, v0, p1

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    return-wide v0
.end method
