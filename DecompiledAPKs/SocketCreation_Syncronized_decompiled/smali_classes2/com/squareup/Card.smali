.class public Lcom/squareup/Card;
.super Ljava/lang/Object;
.source "Card.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/Card$Builder;,
        Lcom/squareup/Card$PanWarning;,
        Lcom/squareup/Card$InputType;,
        Lcom/squareup/Card$Brand;
    }
.end annotation


# instance fields
.field private final brand:Lcom/squareup/Card$Brand;

.field private final countryCode:Ljava/lang/String;

.field private final discretionaryData:Ljava/lang/String;

.field private final expirationMonth:Ljava/lang/String;

.field private final expirationYear:Ljava/lang/String;

.field private final inputType:Lcom/squareup/Card$InputType;

.field private final name:Ljava/lang/String;

.field private final pan:Ljava/lang/String;

.field private final pinVerification:Ljava/lang/String;

.field private final postalCode:Ljava/lang/String;

.field private final serviceCode:Ljava/lang/String;

.field private final title:Ljava/lang/String;

.field private final trackData:Ljava/lang/String;

.field private final unmaskedDigits:Ljava/lang/String;

.field private final verification:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/Card$Brand;Lcom/squareup/Card$InputType;)V
    .locals 0

    .line 375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376
    iput-object p1, p0, Lcom/squareup/Card;->trackData:Ljava/lang/String;

    .line 377
    iput-object p14, p0, Lcom/squareup/Card;->inputType:Lcom/squareup/Card$InputType;

    .line 378
    iput-object p2, p0, Lcom/squareup/Card;->pan:Ljava/lang/String;

    .line 379
    iput-object p3, p0, Lcom/squareup/Card;->countryCode:Ljava/lang/String;

    .line 380
    iput-object p4, p0, Lcom/squareup/Card;->name:Ljava/lang/String;

    .line 381
    iput-object p5, p0, Lcom/squareup/Card;->title:Ljava/lang/String;

    .line 382
    iput-object p6, p0, Lcom/squareup/Card;->expirationYear:Ljava/lang/String;

    .line 383
    iput-object p7, p0, Lcom/squareup/Card;->expirationMonth:Ljava/lang/String;

    .line 384
    iput-object p8, p0, Lcom/squareup/Card;->serviceCode:Ljava/lang/String;

    .line 385
    iput-object p9, p0, Lcom/squareup/Card;->pinVerification:Ljava/lang/String;

    .line 386
    iput-object p10, p0, Lcom/squareup/Card;->discretionaryData:Ljava/lang/String;

    .line 387
    iput-object p11, p0, Lcom/squareup/Card;->verification:Ljava/lang/String;

    .line 388
    iput-object p12, p0, Lcom/squareup/Card;->postalCode:Ljava/lang/String;

    .line 389
    iput-object p13, p0, Lcom/squareup/Card;->brand:Lcom/squareup/Card$Brand;

    .line 392
    invoke-virtual {p13, p2}, Lcom/squareup/Card$Brand;->unmaskedDigitsFromPan(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/Card;->unmaskedDigits:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/Card;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/Card;->serviceCode:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/Card;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/Card;->pinVerification:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/Card;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/Card;->discretionaryData:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/Card;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/Card;->verification:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/Card;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/Card;->postalCode:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/squareup/Card;)Lcom/squareup/Card$InputType;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/Card;->inputType:Lcom/squareup/Card$InputType;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/squareup/Card;)Lcom/squareup/Card$Brand;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/Card;->brand:Lcom/squareup/Card$Brand;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/Card;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/Card;->trackData:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/Card;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/Card;->pan:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/Card;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/Card;->countryCode:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/Card;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/Card;->name:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/Card;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/Card;->title:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/Card;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/Card;->expirationMonth:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/Card;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/Card;->expirationYear:Ljava/lang/String;

    return-object p0
.end method

.method public static createFromPAN(Ljava/lang/String;)Lcom/squareup/Card;
    .locals 3

    .line 693
    invoke-static {p0}, Lcom/squareup/Luhn;->luhnSum(Ljava/lang/CharSequence;)I

    move-result v0

    const/4 v1, -0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    return-object v2

    :cond_0
    const/16 v0, 0x20

    .line 697
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-eq v0, v1, :cond_1

    const-string v0, " "

    const-string v1, ""

    .line 698
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 701
    :cond_1
    invoke-static {p0}, Lcom/squareup/Card;->guessBrand(Ljava/lang/CharSequence;)Lcom/squareup/Card$Brand;

    move-result-object v0

    .line 702
    invoke-virtual {v0, p0}, Lcom/squareup/Card$Brand;->validateLuhnIfRequired(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    return-object v2

    .line 704
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/Card$Brand;->isValidNumberLength(I)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lcom/squareup/Card$Builder;

    invoke-direct {v1}, Lcom/squareup/Card$Builder;-><init>()V

    .line 705
    invoke-virtual {v1, p0}, Lcom/squareup/Card$Builder;->pan(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p0

    invoke-virtual {p0, v0}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object p0

    sget-object v0, Lcom/squareup/Card$InputType;->MANUAL:Lcom/squareup/Card$InputType;

    invoke-virtual {p0, v0}, Lcom/squareup/Card$Builder;->inputType(Lcom/squareup/Card$InputType;)Lcom/squareup/Card$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object v2

    :cond_3
    return-object v2
.end method

.method private static equal(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    if-eq p0, p1, :cond_1

    if-eqz p0, :cond_0

    .line 733
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static guessBrand(Ljava/lang/CharSequence;)Lcom/squareup/Card$Brand;
    .locals 0

    .line 684
    invoke-static {p0}, Lcom/squareup/CardBrandGuesser;->guessBrand(Ljava/lang/CharSequence;)Lcom/squareup/Card$Brand;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 711
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_1

    .line 712
    :cond_1
    check-cast p1, Lcom/squareup/Card;

    .line 715
    iget-object v2, p0, Lcom/squareup/Card;->brand:Lcom/squareup/Card$Brand;

    iget-object v3, p1, Lcom/squareup/Card;->brand:Lcom/squareup/Card$Brand;

    invoke-static {v2, v3}, Lcom/squareup/Card;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/Card;->countryCode:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/Card;->countryCode:Ljava/lang/String;

    .line 716
    invoke-static {v2, v3}, Lcom/squareup/Card;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/Card;->discretionaryData:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/Card;->discretionaryData:Ljava/lang/String;

    .line 717
    invoke-static {v2, v3}, Lcom/squareup/Card;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/Card;->expirationMonth:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/Card;->expirationMonth:Ljava/lang/String;

    .line 718
    invoke-static {v2, v3}, Lcom/squareup/Card;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/Card;->expirationYear:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/Card;->expirationYear:Ljava/lang/String;

    .line 719
    invoke-static {v2, v3}, Lcom/squareup/Card;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/Card;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/Card;->name:Ljava/lang/String;

    .line 720
    invoke-static {v2, v3}, Lcom/squareup/Card;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/Card;->pan:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/Card;->pan:Ljava/lang/String;

    .line 721
    invoke-static {v2, v3}, Lcom/squareup/Card;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/Card;->pinVerification:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/Card;->pinVerification:Ljava/lang/String;

    .line 722
    invoke-static {v2, v3}, Lcom/squareup/Card;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/Card;->postalCode:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/Card;->postalCode:Ljava/lang/String;

    .line 723
    invoke-static {v2, v3}, Lcom/squareup/Card;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/Card;->serviceCode:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/Card;->serviceCode:Ljava/lang/String;

    .line 724
    invoke-static {v2, v3}, Lcom/squareup/Card;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/Card;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/Card;->title:Ljava/lang/String;

    .line 725
    invoke-static {v2, v3}, Lcom/squareup/Card;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/Card;->trackData:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/Card;->trackData:Ljava/lang/String;

    .line 726
    invoke-static {v2, v3}, Lcom/squareup/Card;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/Card;->inputType:Lcom/squareup/Card$InputType;

    iget-object v3, p1, Lcom/squareup/Card;->inputType:Lcom/squareup/Card$InputType;

    .line 727
    invoke-static {v2, v3}, Lcom/squareup/Card;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/Card;->unmaskedDigits:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/Card;->unmaskedDigits:Ljava/lang/String;

    .line 728
    invoke-static {v2, v3}, Lcom/squareup/Card;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/Card;->verification:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/Card;->verification:Ljava/lang/String;

    .line 729
    invoke-static {v2, p1}, Lcom/squareup/Card;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public getBrand()Lcom/squareup/Card$Brand;
    .locals 1

    .line 410
    iget-object v0, p0, Lcom/squareup/Card;->brand:Lcom/squareup/Card$Brand;

    return-object v0
.end method

.method public getCountryCode()Ljava/lang/String;
    .locals 1

    .line 427
    iget-object v0, p0, Lcom/squareup/Card;->countryCode:Ljava/lang/String;

    return-object v0
.end method

.method public getDiscretionaryData()Ljava/lang/String;
    .locals 1

    .line 476
    iget-object v0, p0, Lcom/squareup/Card;->discretionaryData:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiration()Ljava/lang/String;
    .locals 2

    .line 461
    iget-object v0, p0, Lcom/squareup/Card;->expirationYear:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/Card;->expirationMonth:Ljava/lang/String;

    if-nez v0, :cond_0

    goto :goto_0

    .line 464
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/Card;->expirationYear:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/Card;->expirationMonth:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpirationMonth()Ljava/lang/String;
    .locals 1

    .line 456
    iget-object v0, p0, Lcom/squareup/Card;->expirationMonth:Ljava/lang/String;

    return-object v0
.end method

.method public getExpirationYear()Ljava/lang/String;
    .locals 1

    .line 451
    iget-object v0, p0, Lcom/squareup/Card;->expirationYear:Ljava/lang/String;

    return-object v0
.end method

.method public getInputType()Lcom/squareup/Card$InputType;
    .locals 1

    .line 497
    iget-object v0, p0, Lcom/squareup/Card;->inputType:Lcom/squareup/Card$InputType;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 435
    iget-object v0, p0, Lcom/squareup/Card;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPan()Ljava/lang/String;
    .locals 1

    .line 423
    iget-object v0, p0, Lcom/squareup/Card;->pan:Ljava/lang/String;

    return-object v0
.end method

.method public getPinVerification()Ljava/lang/String;
    .locals 1

    .line 472
    iget-object v0, p0, Lcom/squareup/Card;->pinVerification:Ljava/lang/String;

    return-object v0
.end method

.method public getPostalCode()Ljava/lang/String;
    .locals 1

    .line 485
    iget-object v0, p0, Lcom/squareup/Card;->postalCode:Ljava/lang/String;

    return-object v0
.end method

.method public getServiceCode()Ljava/lang/String;
    .locals 1

    .line 468
    iget-object v0, p0, Lcom/squareup/Card;->serviceCode:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 439
    iget-object v0, p0, Lcom/squareup/Card;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackData()Ljava/lang/String;
    .locals 1

    .line 419
    iget-object v0, p0, Lcom/squareup/Card;->trackData:Ljava/lang/String;

    return-object v0
.end method

.method public getUnmaskedDigits()Ljava/lang/String;
    .locals 1

    .line 415
    iget-object v0, p0, Lcom/squareup/Card;->unmaskedDigits:Ljava/lang/String;

    return-object v0
.end method

.method public getVerification()Ljava/lang/String;
    .locals 1

    .line 481
    iget-object v0, p0, Lcom/squareup/Card;->verification:Ljava/lang/String;

    return-object v0
.end method

.method public handleInputType(Lcom/squareup/Card$InputType$InputTypeHandler;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/Card$InputType$InputTypeHandler<",
            "TE;>;)TE;"
        }
    .end annotation

    .line 489
    iget-object v0, p0, Lcom/squareup/Card;->inputType:Lcom/squareup/Card$InputType;

    invoke-virtual {v0, p1}, Lcom/squareup/Card$InputType;->accept(Lcom/squareup/Card$InputType$InputTypeHandler;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public hashCode()I
    .locals 3

    .line 739
    iget-object v0, p0, Lcom/squareup/Card;->brand:Lcom/squareup/Card$Brand;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/Card$Brand;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 740
    iget-object v2, p0, Lcom/squareup/Card;->countryCode:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 741
    iget-object v2, p0, Lcom/squareup/Card;->discretionaryData:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 742
    iget-object v2, p0, Lcom/squareup/Card;->expirationMonth:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 743
    iget-object v2, p0, Lcom/squareup/Card;->expirationYear:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 744
    iget-object v2, p0, Lcom/squareup/Card;->name:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 745
    iget-object v2, p0, Lcom/squareup/Card;->pan:Ljava/lang/String;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 746
    iget-object v2, p0, Lcom/squareup/Card;->pinVerification:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 747
    iget-object v2, p0, Lcom/squareup/Card;->postalCode:Ljava/lang/String;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_8
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 748
    iget-object v2, p0, Lcom/squareup/Card;->serviceCode:Ljava/lang/String;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_9
    const/4 v2, 0x0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 749
    iget-object v2, p0, Lcom/squareup/Card;->title:Ljava/lang/String;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_a

    :cond_a
    const/4 v2, 0x0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 750
    iget-object v2, p0, Lcom/squareup/Card;->trackData:Ljava/lang/String;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_b

    :cond_b
    const/4 v2, 0x0

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 751
    iget-object v2, p0, Lcom/squareup/Card;->inputType:Lcom/squareup/Card$InputType;

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Lcom/squareup/Card$InputType;->hashCode()I

    move-result v2

    goto :goto_c

    :cond_c
    const/4 v2, 0x0

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 752
    iget-object v2, p0, Lcom/squareup/Card;->unmaskedDigits:Ljava/lang/String;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_d

    :cond_d
    const/4 v2, 0x0

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 753
    iget-object v2, p0, Lcom/squareup/Card;->verification:Ljava/lang/String;

    if-eqz v2, :cond_e

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_e
    add-int/2addr v0, v1

    return v0
.end method

.method public includesExpiration()Z
    .locals 1

    .line 443
    iget-object v0, p0, Lcom/squareup/Card;->expirationMonth:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 444
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/Card;->expirationYear:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 446
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isManual()Z
    .locals 2

    .line 502
    iget-object v0, p0, Lcom/squareup/Card;->inputType:Lcom/squareup/Card$InputType;

    sget-object v1, Lcom/squareup/Card$InputType;->MANUAL:Lcom/squareup/Card$InputType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isValid()Z
    .locals 3

    .line 402
    iget-object v0, p0, Lcom/squareup/Card;->pan:Ljava/lang/String;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 403
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    goto :goto_0

    .line 405
    :cond_1
    iget-object v2, p0, Lcom/squareup/Card;->brand:Lcom/squareup/Card$Brand;

    invoke-virtual {v2, v0}, Lcom/squareup/Card$Brand;->isValidNumberLength(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/Card;->brand:Lcom/squareup/Card$Brand;

    iget-object v2, p0, Lcom/squareup/Card;->pan:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/squareup/Card$Brand;->validateLuhnIfRequired(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :cond_3
    :goto_0
    return v1
.end method

.method public toBuilder()Lcom/squareup/Card$Builder;
    .locals 2

    .line 515
    new-instance v0, Lcom/squareup/Card$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/Card$Builder;-><init>(Lcom/squareup/Card;Lcom/squareup/Card$1;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 511
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
