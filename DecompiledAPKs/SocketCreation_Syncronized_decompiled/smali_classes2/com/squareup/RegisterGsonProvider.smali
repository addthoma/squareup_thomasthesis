.class public Lcom/squareup/RegisterGsonProvider;
.super Ljava/lang/Object;
.source "RegisterGsonProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/RegisterGsonProvider$Holder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/google/gson/GsonBuilder;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/RegisterGsonProvider;->gsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v0

    return-object v0
.end method

.method public static gson()Lcom/google/gson/Gson;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/RegisterGsonProvider$Holder;->gson:Lcom/google/gson/Gson;

    return-object v0
.end method

.method private static gsonBuilder()Lcom/google/gson/GsonBuilder;
    .locals 3

    .line 27
    invoke-static {}, Lcom/squareup/gson/GsonProvider;->gsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Lcom/squareup/queue/Itemize;

    new-instance v2, Lcom/squareup/queue/Itemize$OopsAdapter;

    invoke-direct {v2}, Lcom/squareup/queue/Itemize$OopsAdapter;-><init>()V

    .line 28
    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Lcom/squareup/checkout/Adjustment;

    new-instance v2, Lcom/squareup/checkout/Adjustment$JsonAdapter;

    invoke-direct {v2}, Lcom/squareup/checkout/Adjustment$JsonAdapter;-><init>()V

    .line 29
    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Lcom/squareup/print/PrintablePayload;

    new-instance v2, Lcom/squareup/print/PrintablePayload$JsonAdapter;

    invoke-direct {v2}, Lcom/squareup/print/PrintablePayload$JsonAdapter;-><init>()V

    .line 30
    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Lcom/squareup/quantity/DecimalQuantity;

    new-instance v2, Lcom/squareup/quantity/DecimalQuantity$JsonAdapter;

    invoke-direct {v2}, Lcom/squareup/quantity/DecimalQuantity$JsonAdapter;-><init>()V

    .line 31
    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Lcom/squareup/wire/Message;

    new-instance v2, Lcom/squareup/queue/WireMessageJsonAdapter;

    invoke-direct {v2}, Lcom/squareup/queue/WireMessageJsonAdapter;-><init>()V

    .line 32
    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeHierarchyAdapter(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    new-instance v1, Lcom/squareup/log/HasPaymentTimings$PaymentTimingsTypeAdapterFactory;

    invoke-direct {v1}, Lcom/squareup/log/HasPaymentTimings$PaymentTimingsTypeAdapterFactory;-><init>()V

    .line 33
    invoke-virtual {v0, v1}, Lcom/google/gson/GsonBuilder;->registerTypeAdapterFactory(Lcom/google/gson/TypeAdapterFactory;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    return-object v0
.end method
