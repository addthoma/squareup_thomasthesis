.class final Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$1;
.super Ljava/lang/Object;
.source "RealCatalogAppliedLocationCountFetcher.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->doFetchAppliedLocationCount(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogTask<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "local",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "kotlin.jvm.PlatformType",
        "perform"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $objectCogsId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$1;->$objectCogsId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 0

    .line 26
    invoke-virtual {p0, p1}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$1;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    .line 90
    iget-object v1, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$1;->$objectCogsId:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Lkotlin/collections/SetsKt;->mutableSetOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    .line 91
    iget-object v0, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$1;->$objectCogsId:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogObject;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    return-object p1
.end method
