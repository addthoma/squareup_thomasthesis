.class public abstract Lcom/squareup/catalog/WireEnumAttributeEditor;
.super Ljava/lang/Object;
.source "WireEnumAttributeEditor.java"

# interfaces
.implements Lcom/squareup/catalog/AttributeEditor;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/wire/WireEnum;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/catalog/AttributeEditor<",
        "TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract fromValue(I)Lcom/squareup/wire/WireEnum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation
.end method

.method public final getValue(Lsquareup/items/merchant/Attribute$Builder;)Lcom/squareup/wire/WireEnum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/items/merchant/Attribute$Builder;",
            ")TT;"
        }
    .end annotation

    .line 10
    iget-object v0, p1, Lsquareup/items/merchant/Attribute$Builder;->int_value:Ljava/lang/Long;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    iget-object p1, p1, Lsquareup/items/merchant/Attribute$Builder;->int_value:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/catalog/WireEnumAttributeEditor;->fromValue(I)Lcom/squareup/wire/WireEnum;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic getValue(Lsquareup/items/merchant/Attribute$Builder;)Ljava/lang/Object;
    .locals 0

    .line 7
    invoke-virtual {p0, p1}, Lcom/squareup/catalog/WireEnumAttributeEditor;->getValue(Lsquareup/items/merchant/Attribute$Builder;)Lcom/squareup/wire/WireEnum;

    move-result-object p1

    return-object p1
.end method

.method public final setValue(Lsquareup/items/merchant/Attribute$Builder;Lcom/squareup/wire/WireEnum;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/items/merchant/Attribute$Builder;",
            "TT;)V"
        }
    .end annotation

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    .line 14
    :cond_0
    invoke-interface {p2}, Lcom/squareup/wire/WireEnum;->getValue()I

    move-result p2

    int-to-long v0, p2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    :goto_0
    invoke-virtual {p1, p2}, Lsquareup/items/merchant/Attribute$Builder;->int_value(Ljava/lang/Long;)Lsquareup/items/merchant/Attribute$Builder;

    return-void
.end method

.method public bridge synthetic setValue(Lsquareup/items/merchant/Attribute$Builder;Ljava/lang/Object;)V
    .locals 0

    .line 7
    check-cast p2, Lcom/squareup/wire/WireEnum;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/catalog/WireEnumAttributeEditor;->setValue(Lsquareup/items/merchant/Attribute$Builder;Lcom/squareup/wire/WireEnum;)V

    return-void
.end method
