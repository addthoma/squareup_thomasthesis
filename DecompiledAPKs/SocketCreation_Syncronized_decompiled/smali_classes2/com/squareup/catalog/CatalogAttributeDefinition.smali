.class public Lcom/squareup/catalog/CatalogAttributeDefinition;
.super Ljava/lang/Object;
.source "CatalogAttributeDefinition.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static COGS_ENABLED:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static CURRENCY:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static DURATION:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static ITEM_OPTION_0:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static ITEM_OPTION_1:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static ITEM_OPTION_2:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static ITEM_OPTION_3:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static ITEM_OPTION_4:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static ITEM_OPTION_5:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static ITEM_OPTION_VALUE_0:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static ITEM_OPTION_VALUE_1:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static ITEM_OPTION_VALUE_2:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static ITEM_OPTION_VALUE_3:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static ITEM_OPTION_VALUE_4:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static ITEM_OPTION_VALUE_5:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static ITEM_TYPE:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static MEASUREMENT_UNIT_TOKEN:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static NAME:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static NO_SHOW_FEE:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static NO_SHOW_FEE_CURRENCY:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static OBS_AVAILABILITY:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static ORDINAL:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static PRICE:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static PRICE_DESCRIPTION:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static PRICING_TYPE:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Lcom/squareup/api/items/PricingType;",
            ">;"
        }
    .end annotation
.end field

.field public static REFERENCED_ITEM_ID:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static SKU:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static TRANSITION_TIME:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final attributeEditor:Lcom/squareup/catalog/AttributeEditor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/AttributeEditor<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final definitionToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 13
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->LONG_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_1"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->PRICE:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 16
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_2"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->CURRENCY:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 19
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_3"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->NAME:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 22
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_14"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->SKU:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 25
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    new-instance v1, Lcom/squareup/catalog/CatalogAttributeDefinition$1;

    invoke-direct {v1}, Lcom/squareup/catalog/CatalogAttributeDefinition$1;-><init>()V

    const-string v2, "SQ_10"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_TYPE:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 32
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    new-instance v1, Lcom/squareup/catalog/CatalogAttributeDefinition$2;

    invoke-direct {v1}, Lcom/squareup/catalog/CatalogAttributeDefinition$2;-><init>()V

    const-string v2, "SQ_15"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->PRICING_TYPE:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 40
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->BOOLEAN_ATTRIBUTE_EDITOR_DEFAULT_FALSE:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_COGS_E"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->COGS_ENABLED:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 43
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_COGS_IID"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->REFERENCED_ITEM_ID:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 46
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->INTEGER_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_REG_2"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->ORDINAL:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 49
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_MU_ID"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->MEASUREMENT_UNIT_TOKEN:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 53
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->LONG_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_APPT_SD"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->DURATION:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 56
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->BOOLEAN_ATTRIBUTE_EDITOR_DEFAULT_FALSE:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_APPT_AV"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->OBS_AVAILABILITY:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 59
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_PD"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->PRICE_DESCRIPTION:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 62
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->LONG_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_APPT_TT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->TRANSITION_TIME:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 65
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->LONG_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_APPT_NSF"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->NO_SHOW_FEE:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 68
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_APPT_NSFCC"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->NO_SHOW_FEE_CURRENCY:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 72
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_IO_0"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_0:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 75
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_IO_1"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_1:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 78
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_IO_2"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_2:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 81
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_IO_3"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_3:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 84
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_IO_4"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_4:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 87
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_IO_5"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_5:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 90
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_IOV_0"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_VALUE_0:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 93
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_IOV_1"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_VALUE_1:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 96
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_IOV_2"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_VALUE_2:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 99
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_IOV_3"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_VALUE_3:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 102
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_IOV_4"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_VALUE_4:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 105
    new-instance v0, Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v1, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    const-string v2, "SQ_IOV_5"

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/CatalogAttributeDefinition;-><init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V

    sput-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_VALUE_5:Lcom/squareup/catalog/CatalogAttributeDefinition;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/squareup/catalog/AttributeEditor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/catalog/AttributeEditor<",
            "TT;>;)V"
        }
    .end annotation

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput-object p1, p0, Lcom/squareup/catalog/CatalogAttributeDefinition;->definitionToken:Ljava/lang/String;

    .line 113
    iput-object p2, p0, Lcom/squareup/catalog/CatalogAttributeDefinition;->attributeEditor:Lcom/squareup/catalog/AttributeEditor;

    return-void
.end method
