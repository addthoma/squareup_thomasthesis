.class public final Lcom/squareup/catalog/ItemConverter;
.super Ljava/lang/Object;
.source "ItemConverter.kt"

# interfaces
.implements Lcom/squareup/catalog/CatalogObjectConverter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;,
        Lcom/squareup/catalog/ItemConverter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/catalog/CatalogObjectConverter<",
        "Lcom/squareup/api/items/Item;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nItemConverter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ItemConverter.kt\ncom/squareup/catalog/ItemConverter\n*L\n1#1,106:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010!\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 \u00172\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0017\u0018B\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J2\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0006\u0010\u0008\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\n0\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\nH\u0016J2\u0010\u000e\u001a,\u0012(\u0012&\u0012\u000c\u0012\n \u0011*\u0004\u0018\u00010\n0\n \u0011*\u0012\u0012\u000c\u0012\n \u0011*\u0004\u0018\u00010\n0\n\u0018\u00010\u00100\u00100\u000fH\u0002J$\u0010\u0012\u001a\u0004\u0018\u00010\u00072\u0006\u0010\u0008\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\nH\u0016J\u0014\u0010\u0016\u001a\u00020\u0014*\u00020\u00142\u0006\u0010\u0008\u001a\u00020\u0002H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/catalog/ItemConverter;",
        "Lcom/squareup/catalog/CatalogObjectConverter;",
        "Lcom/squareup/api/items/Item;",
        "config",
        "Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;",
        "(Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;)V",
        "createCatalogObject",
        "Lsquareup/items/merchant/CatalogObject;",
        "item",
        "temporaryToken",
        "",
        "enabledUnitTokens",
        "",
        "parentId",
        "getOptionAttributeDefs",
        "",
        "Lcom/squareup/catalog/CatalogAttributeDefinition;",
        "kotlin.jvm.PlatformType",
        "updateCatalogObject",
        "builder",
        "Lcom/squareup/catalog/CatalogObjectBuilder;",
        "unitToken",
        "setItemOptions",
        "Companion",
        "ItemConverterConfig",
        "catalog_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/catalog/ItemConverter$Companion;


# instance fields
.field private final config:Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/catalog/ItemConverter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/catalog/ItemConverter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/catalog/ItemConverter;->Companion:Lcom/squareup/catalog/ItemConverter$Companion;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/catalog/ItemConverter;->config:Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/catalog/ItemConverter;-><init>(Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;)V

    return-void
.end method

.method public static final get()Lcom/squareup/catalog/ItemConverter;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/catalog/ItemConverter;->Companion:Lcom/squareup/catalog/ItemConverter$Companion;

    invoke-virtual {v0}, Lcom/squareup/catalog/ItemConverter$Companion;->get()Lcom/squareup/catalog/ItemConverter;

    move-result-object v0

    return-object v0
.end method

.method public static final get(Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;)Lcom/squareup/catalog/ItemConverter;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/catalog/ItemConverter;->Companion:Lcom/squareup/catalog/ItemConverter$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/catalog/ItemConverter$Companion;->get(Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;)Lcom/squareup/catalog/ItemConverter;

    move-result-object p0

    return-object p0
.end method

.method private final getOptionAttributeDefs()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 84
    sget-object v1, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_0:Lcom/squareup/catalog/CatalogAttributeDefinition;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 85
    sget-object v1, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_1:Lcom/squareup/catalog/CatalogAttributeDefinition;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 86
    sget-object v1, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_2:Lcom/squareup/catalog/CatalogAttributeDefinition;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 87
    sget-object v1, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_3:Lcom/squareup/catalog/CatalogAttributeDefinition;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 88
    sget-object v1, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_4:Lcom/squareup/catalog/CatalogAttributeDefinition;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 89
    sget-object v1, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_5:Lcom/squareup/catalog/CatalogAttributeDefinition;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 83
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private final setItemOptions(Lcom/squareup/catalog/CatalogObjectBuilder;Lcom/squareup/api/items/Item;)Lcom/squareup/catalog/CatalogObjectBuilder;
    .locals 3

    .line 67
    invoke-direct {p0}, Lcom/squareup/catalog/ItemConverter;->getOptionAttributeDefs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 68
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/catalog/CatalogAttributeDefinition;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    goto :goto_0

    .line 72
    :cond_0
    iget-object v0, p2, Lcom/squareup/api/items/Item;->item_options:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 73
    invoke-direct {p0}, Lcom/squareup/catalog/ItemConverter;->getOptionAttributeDefs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 74
    iget-object p2, p2, Lcom/squareup/api/items/Item;->item_options:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/ItemOptionForItem;

    .line 75
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 78
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object v1, v1, Lcom/squareup/api/items/ItemOptionForItem;->item_option_id:Ljava/lang/String;

    invoke-virtual {p1, v2, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    goto :goto_1

    .line 75
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The item has more item options than it should be allowed to have."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_2
    return-object p1
.end method


# virtual methods
.method public createCatalogObject(Lcom/squareup/api/items/Item;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/Item;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lsquareup/items/merchant/CatalogObject;"
        }
    .end annotation

    const-string p4, "item"

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "temporaryToken"

    invoke-static {p2, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "enabledUnitTokens"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p4, 0x0

    const-string v0, "#"

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 27
    invoke-static {p2, v0, p4, v1, v2}, Lkotlin/text/StringsKt;->startsWith$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 31
    new-instance v0, Lcom/squareup/catalog/CatalogObjectBuilder;

    .line 32
    new-instance v1, Lsquareup/items/merchant/CatalogObject$Builder;

    invoke-direct {v1}, Lsquareup/items/merchant/CatalogObject$Builder;-><init>()V

    .line 33
    sget-object v2, Lsquareup/items/merchant/CatalogObjectType;->ITEM:Lsquareup/items/merchant/CatalogObjectType;

    invoke-virtual {v1, v2}, Lsquareup/items/merchant/CatalogObject$Builder;->type(Lsquareup/items/merchant/CatalogObjectType;)Lsquareup/items/merchant/CatalogObject$Builder;

    move-result-object v1

    .line 34
    invoke-virtual {v1, p2}, Lsquareup/items/merchant/CatalogObject$Builder;->token(Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject$Builder;

    move-result-object p2

    .line 35
    invoke-virtual {p2}, Lsquareup/items/merchant/CatalogObject$Builder;->build()Lsquareup/items/merchant/CatalogObject;

    move-result-object p2

    .line 31
    invoke-direct {v0, p2}, Lcom/squareup/catalog/CatalogObjectBuilder;-><init>(Lsquareup/items/merchant/CatalogObject;)V

    .line 39
    sget-object p2, Lcom/squareup/catalog/CatalogAttributeDefinition;->COGS_ENABLED:Lcom/squareup/catalog/CatalogAttributeDefinition;

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p4

    invoke-virtual {v0, p2, p4}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    .line 40
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    .line 41
    sget-object p4, Lcom/squareup/catalog/CatalogAttributeDefinition;->COGS_ENABLED:Lcom/squareup/catalog/CatalogAttributeDefinition;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p4, p3, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setUnitOverride(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    goto :goto_0

    .line 44
    :cond_0
    sget-object p2, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_TYPE:Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object p3, p1, Lcom/squareup/api/items/Item;->type:Lcom/squareup/api/items/Item$Type;

    invoke-virtual {v0, p2, p3}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    .line 45
    sget-object p2, Lcom/squareup/catalog/CatalogAttributeDefinition;->NAME:Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object p3, p1, Lcom/squareup/api/items/Item;->name:Ljava/lang/String;

    if-nez p3, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {v0, p2, p3}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    .line 47
    iget-object p2, p0, Lcom/squareup/catalog/ItemConverter;->config:Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;

    invoke-interface {p2}, Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;->canApplyItemOptions()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 48
    invoke-direct {p0, v0, p1}, Lcom/squareup/catalog/ItemConverter;->setItemOptions(Lcom/squareup/catalog/CatalogObjectBuilder;Lcom/squareup/api/items/Item;)Lcom/squareup/catalog/CatalogObjectBuilder;

    .line 51
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/catalog/CatalogObjectBuilder;->build()Lsquareup/items/merchant/CatalogObject;

    move-result-object p1

    return-object p1

    .line 28
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The temporaryToken has to start with \'#\'."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic createCatalogObject(Ljava/lang/Object;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/api/items/Item;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/catalog/ItemConverter;->createCatalogObject(Lcom/squareup/api/items/Item;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;

    move-result-object p1

    return-object p1
.end method

.method public updateCatalogObject(Lcom/squareup/api/items/Item;Lcom/squareup/catalog/CatalogObjectBuilder;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;
    .locals 0

    const-string p3, "item"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "builder"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iget-object p3, p0, Lcom/squareup/catalog/ItemConverter;->config:Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;

    invoke-interface {p3}, Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;->canApplyItemOptions()Z

    move-result p3

    if-eqz p3, :cond_0

    .line 60
    invoke-direct {p0, p2, p1}, Lcom/squareup/catalog/ItemConverter;->setItemOptions(Lcom/squareup/catalog/CatalogObjectBuilder;Lcom/squareup/api/items/Item;)Lcom/squareup/catalog/CatalogObjectBuilder;

    .line 63
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/catalog/CatalogObjectBuilder;->build()Lsquareup/items/merchant/CatalogObject;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic updateCatalogObject(Ljava/lang/Object;Lcom/squareup/catalog/CatalogObjectBuilder;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/api/items/Item;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/catalog/ItemConverter;->updateCatalogObject(Lcom/squareup/api/items/Item;Lcom/squareup/catalog/CatalogObjectBuilder;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;

    move-result-object p1

    return-object p1
.end method
