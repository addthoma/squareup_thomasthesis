.class public final Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Factory;
.super Ljava/lang/Object;
.source "CheckPasswordLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Factory;",
        "",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "(Lcom/squareup/register/widgets/GlassSpinner;)V",
        "create",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;",
        "view",
        "Landroid/view/View;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;


# direct methods
.method public constructor <init>(Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "glassSpinner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Factory;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-void
.end method


# virtual methods
.method public final create(Landroid/view/View;)Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    new-instance v0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;

    iget-object v1, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Factory;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-direct {v0, p1, v1}, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/register/widgets/GlassSpinner;)V

    return-object v0
.end method
