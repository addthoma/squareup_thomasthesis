.class final Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealCheckBankAccountInfoWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->render(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;",
        "+",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;",
        "output",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;


# direct methods
.method constructor <init>(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$render$3;->$state:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    instance-of v0, p1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput$Success;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$LinkBank;

    .line 148
    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$render$3;->$state:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;

    check-cast v1, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$GettingDirectDebitInfo;

    invoke-virtual {v1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$GettingDirectDebitInfo;->getBankAccountDetails()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object v1

    check-cast p1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput$Success;

    invoke-virtual {p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput$Success;->getIdempotenceKey()Ljava/lang/String;

    move-result-object p1

    .line 147
    invoke-direct {v0, v1, p1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$LinkBank;-><init>(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 150
    :cond_0
    sget-object v0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput$Cancel;->INSTANCE:Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput$Cancel;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$CancelBankLinking;->INSTANCE:Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$CancelBankLinking;

    move-object v0, p1

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$render$3;->invoke(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
