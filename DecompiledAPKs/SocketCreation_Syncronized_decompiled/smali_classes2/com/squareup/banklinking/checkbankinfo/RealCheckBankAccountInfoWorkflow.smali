.class public final Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealCheckBankAccountInfoWorkflow.kt"

# interfaces
.implements Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action;,
        Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCheckBankAccountInfoWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCheckBankAccountInfoWorkflow.kt\ncom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,236:1\n32#2,12:237\n85#3:249\n240#4:250\n276#5:251\n149#6,5:252\n149#6,5:257\n149#6,5:262\n*E\n*S KotlinDebug\n*F\n+ 1 RealCheckBankAccountInfoWorkflow.kt\ncom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow\n*L\n109#1,12:237\n124#1:249\n124#1:250\n124#1:251\n135#1,5:252\n179#1,5:257\n189#1,5:262\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 /2\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0002./B\'\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0002\u0010\u0013J2\u0010\u0014\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t2\u0006\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018H\u0002J\u001a\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u00032\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J \u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H\u0002J\u0010\u0010#\u001a\u00020\u001e2\u0006\u0010$\u001a\u00020 H\u0002J\u001c\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050&2\u0006\u0010\u0016\u001a\u00020\'H\u0002JN\u0010(\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u00042\u0012\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050*H\u0016J\u0010\u0010+\u001a\u00020\u001c2\u0006\u0010\u0016\u001a\u00020\u0004H\u0016J*\u0010,\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t2\u0006\u0010\u0016\u001a\u00020-2\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "bankAccountSettings",
        "Lcom/squareup/banklinking/BankAccountSettings;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "getDirectDebitInfoWorkflow",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;",
        "(Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/CountryCode;Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;)V",
        "checkBankAccountInfoScreen",
        "props",
        "state",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action;",
        "initialState",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "logBankLinkingAttemptEvent",
        "",
        "isSuccessful",
        "",
        "routingNumber",
        "",
        "logBankLinkingCancelEvent",
        "requiresPassword",
        "onBankSettingsState",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/banklinking/BankAccountSettings$State;",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "warningDialogScreen",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$InvalidBankInfo;",
        "Action",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADD_ATTEMPT:Ljava/lang/String; = "Settings Bank Accounts: Add Bank Account Attempt"

.field public static final ADD_BANK_ACCOUNT_CANCEL:Ljava/lang/String; = "Settings Bank Account: Add Bank Account Cancel"

.field public static final Companion:Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Companion;

.field public static final EDIT_ATTEMPT:Ljava/lang/String; = "Settings Bank Accounts: Edit Bank Account Attempt"

.field public static final EDIT_BANK_ACCOUNT_CANCEL:Ljava/lang/String; = "Settings Bank Account: Edit Bank Account Cancel"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

.field private final countryCode:Lcom/squareup/CountryCode;

.field private final getDirectDebitInfoWorkflow:Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->Companion:Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/CountryCode;Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bankAccountSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getDirectDebitInfoWorkflow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    iput-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p3, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->countryCode:Lcom/squareup/CountryCode;

    iput-object p4, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->getDirectDebitInfoWorkflow:Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;

    return-void
.end method

.method public static final synthetic access$getCountryCode$p(Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;)Lcom/squareup/CountryCode;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->countryCode:Lcom/squareup/CountryCode;

    return-object p0
.end method

.method public static final synthetic access$logBankLinkingAttemptEvent(Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;ZLjava/lang/String;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->logBankLinkingAttemptEvent(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;ZLjava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$logBankLinkingCancelEvent(Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;Z)V
    .locals 0

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->logBankLinkingCancelEvent(Z)V

    return-void
.end method

.method public static final synthetic access$onBankSettingsState(Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->onBankSettingsState(Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final checkBankAccountInfoScreen(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 167
    new-instance v7, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;

    .line 168
    instance-of v1, p2, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$LinkingBank;

    .line 169
    iget-object v2, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->countryCode:Lcom/squareup/CountryCode;

    .line 170
    new-instance p2, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$1;

    invoke-direct {p2, p3}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v3, p2

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 171
    new-instance p2, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$2;

    invoke-direct {p2, p0, p1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$2;-><init>(Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;)V

    move-object v4, p2

    check-cast v4, Lkotlin/jvm/functions/Function2;

    .line 174
    new-instance p2, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$3;

    invoke-direct {p2, p0, p3}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$3;-><init>(Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;Lcom/squareup/workflow/Sink;)V

    move-object v5, p2

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 175
    new-instance p2, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$4;

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$4;-><init>(Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;Lcom/squareup/workflow/Sink;)V

    move-object v6, p2

    check-cast v6, Lkotlin/jvm/functions/Function0;

    move-object v0, v7

    .line 167
    invoke-direct/range {v0 .. v6}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;-><init>(ZLcom/squareup/CountryCode;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v7, Lcom/squareup/workflow/legacy/V2Screen;

    .line 258
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 259
    const-class p2, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string p3, ""

    invoke-static {p2, p3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 260
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 258
    invoke-direct {p1, p2, v7, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final logBankLinkingAttemptEvent(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;ZLjava/lang/String;)V
    .locals 2

    .line 210
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;->getRequiresPassword()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;->getPrimaryInstitutionNumber()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    .line 212
    new-instance v0, Lcom/squareup/log/deposits/BankLinkingAttemptEvent;

    .line 216
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;->getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->name()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const-string v1, "Settings Bank Accounts: Edit Bank Account Attempt"

    .line 212
    invoke-direct {v0, v1, p2, p3, p1}, Lcom/squareup/log/deposits/BankLinkingAttemptEvent;-><init>(Ljava/lang/String;ZZLjava/lang/String;)V

    .line 218
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    goto :goto_1

    .line 220
    :cond_1
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance p3, Lcom/squareup/log/deposits/BankLinkingAttemptEvent;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    const-string v0, "Settings Bank Accounts: Add Bank Account Attempt"

    invoke-direct {p3, v0, p2}, Lcom/squareup/log/deposits/BankLinkingAttemptEvent;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    check-cast p3, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p1, p3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    :goto_1
    return-void
.end method

.method private final logBankLinkingCancelEvent(Z)V
    .locals 2

    if-eqz p1, :cond_0

    const-string p1, "Settings Bank Account: Edit Bank Account Cancel"

    goto :goto_0

    :cond_0
    const-string p1, "Settings Bank Account: Add Bank Account Cancel"

    .line 226
    :goto_0
    iget-object v0, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    invoke-direct {v1, p1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method private final onBankSettingsState(Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;",
            ">;"
        }
    .end annotation

    .line 195
    iget-object v0, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    sget-object v1, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 199
    iget-object p1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 200
    :cond_0
    new-instance v0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$ShowWarning;

    new-instance v1, Lcom/squareup/widgets/warning/WarningStrings;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/widgets/warning/Warning;

    invoke-direct {v0, v1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$ShowWarning;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 202
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected linkBankAccountState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 197
    :cond_2
    new-instance v0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$CompleteBankLinking;

    iget-object v1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->verificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$CompleteBankLinking;-><init>(Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object v0
.end method

.method private final warningDialogScreen(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$InvalidBankInfo;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$InvalidBankInfo;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 186
    new-instance v0, Lcom/squareup/banklinking/WarningDialogScreen;

    .line 187
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$InvalidBankInfo;->getWarning()Lcom/squareup/widgets/warning/Warning;

    move-result-object p1

    .line 188
    new-instance v1, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$warningDialogScreen$1;

    invoke-direct {v1, p2}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$warningDialogScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 186
    invoke-direct {v0, p1, v1}, Lcom/squareup/banklinking/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 263
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 264
    const-class p2, Lcom/squareup/banklinking/WarningDialogScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 265
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 263
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 237
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 242
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 244
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 245
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 246
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 247
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 248
    :cond_3
    check-cast v1, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 109
    :cond_4
    sget-object p1, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$PrepareToLinkBank;->INSTANCE:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$PrepareToLinkBank;

    move-object v1, p1

    check-cast v1, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->initialState(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;

    check-cast p2, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->render(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;",
            "-",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    sget-object v0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$PrepareToLinkBank;->INSTANCE:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$PrepareToLinkBank;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p3

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->checkBankAccountInfoScreen(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 117
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 118
    :cond_0
    instance-of v0, p2, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$LinkingBank;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 120
    iget-object v0, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    .line 121
    move-object v2, p2

    check-cast v2, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$LinkingBank;

    invoke-virtual {v2}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$LinkingBank;->getBankAccountDetails()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object v3

    .line 123
    invoke-virtual {v2}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$LinkingBank;->getIdempotenceKey()Ljava/lang/String;

    move-result-object v4

    .line 120
    invoke-interface {v0, v3, v1, v4}, Lcom/squareup/banklinking/BankAccountSettings;->linkBankAccount(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;ZLjava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 249
    sget-object v1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$render$$inlined$asWorker$1;

    const/4 v3, 0x0

    invoke-direct {v1, v0, v3}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 250
    invoke-static {v1}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 251
    const-class v1, Lcom/squareup/banklinking/BankAccountSettings$State;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v5, v3

    check-cast v5, Lcom/squareup/workflow/Worker;

    const/4 v6, 0x0

    .line 125
    new-instance v0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$render$1;

    invoke-direct {v0, p0}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$render$1;-><init>(Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object v4, p3

    .line 119
    invoke-static/range {v4 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 127
    invoke-virtual {v2}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$LinkingBank;->getIdempotenceKey()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 128
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p3

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->checkBankAccountInfoScreen(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 130
    :cond_1
    new-instance p1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;

    .line 131
    new-instance p2, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Loading;

    .line 132
    new-instance v0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$render$2;

    invoke-direct {v0, p3}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$render$2;-><init>(Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 131
    invoke-direct {p2, v0}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Loading;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;

    .line 130
    invoke-direct {p1, p2}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;-><init>(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 253
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 254
    const-class p3, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    const-string v0, ""

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 255
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 253
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 136
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 139
    :cond_2
    instance-of v0, p2, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$GettingDirectDebitInfo;

    if-eqz v0, :cond_3

    .line 140
    new-instance v4, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoProps;

    .line 141
    move-object p1, p2

    check-cast p1, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$GettingDirectDebitInfo;

    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$GettingDirectDebitInfo;->getBankAccountDetails()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;->account_name:Ljava/lang/String;

    const-string v1, "state.bankAccountDetails.account_name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$GettingDirectDebitInfo;->getBankAccountDetails()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;->account_number:Ljava/lang/String;

    const-string v2, "state.bankAccountDetails.account_number"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$GettingDirectDebitInfo;->getBankAccountDetails()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;->primary_institution_number:Ljava/lang/String;

    const-string v2, "state.bankAccountDetails\u2026rimary_institution_number"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    invoke-direct {v4, v0, v1, p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoProps;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->getDirectDebitInfoWorkflow:Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;

    move-object v3, p1

    check-cast v3, Lcom/squareup/workflow/Workflow;

    const/4 v5, 0x0

    new-instance p1, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$render$3;

    invoke-direct {p1, p2}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$render$3;-><init>(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p3

    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 154
    :cond_3
    instance-of v0, p2, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$InvalidBankInfo;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 v2, 0x2

    new-array v2, v2, [Lkotlin/Pair;

    .line 155
    sget-object v3, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v4

    invoke-direct {p0, p1, p2, v4}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->checkBankAccountInfoScreen(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    invoke-static {v3, p1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    aput-object p1, v2, v1

    const/4 p1, 0x1

    .line 156
    sget-object v1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    check-cast p2, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$InvalidBankInfo;

    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p3

    invoke-direct {p0, p2, p3}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->warningDialogScreen(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$InvalidBankInfo;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    invoke-static {v1, p2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    aput-object p2, v2, p1

    .line 154
    invoke-virtual {v0, v2}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->snapshotState(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
