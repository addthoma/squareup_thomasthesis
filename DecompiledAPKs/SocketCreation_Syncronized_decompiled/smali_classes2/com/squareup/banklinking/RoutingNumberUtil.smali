.class public Lcom/squareup/banklinking/RoutingNumberUtil;
.super Ljava/lang/Object;
.source "RoutingNumberUtil.java"


# static fields
.field private static final ROUTING_TRANSIT_LEAD_RANGES:[[I

.field public static final ROUTING_TRANSIT_NUMBER_LENGTH:I = 0x9

.field private static noBankLogo:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [[I

    const/4 v1, 0x2

    new-array v2, v1, [I

    .line 14
    fill-array-data v2, :array_0

    const/4 v3, 0x0

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_1

    const/4 v3, 0x1

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    new-array v1, v1, [I

    fill-array-data v1, :array_3

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/banklinking/RoutingNumberUtil;->ROUTING_TRANSIT_LEAD_RANGES:[[I

    return-void

    :array_0
    .array-data 4
        0x0
        0xc
    .end array-data

    :array_1
    .array-data 4
        0x15
        0x20
    .end array-data

    :array_2
    .array-data 4
        0x3d
        0x48
    .end array-data

    :array_3
    .array-data 4
        0x50
        0x50
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bankNameForRoutingNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 57
    sget-object v0, Lcom/squareup/banklinking/RoutingNumberToBankName;->MAP:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method

.method private static hasGoodRoutingChecksum(Ljava/lang/String;)Z
    .locals 7

    const/4 v0, 0x0

    .line 74
    invoke-static {p0, v0}, Lcom/squareup/banklinking/RoutingNumberUtil;->intAt(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x3

    invoke-static {p0, v2}, Lcom/squareup/banklinking/RoutingNumberUtil;->intAt(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v1, v3

    const/4 v3, 0x6

    invoke-static {p0, v3}, Lcom/squareup/banklinking/RoutingNumberUtil;->intAt(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v1, v3

    const/4 v3, 0x7

    mul-int/lit8 v1, v1, 0x7

    const/4 v4, 0x1

    .line 75
    invoke-static {p0, v4}, Lcom/squareup/banklinking/RoutingNumberUtil;->intAt(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x4

    invoke-static {p0, v6}, Lcom/squareup/banklinking/RoutingNumberUtil;->intAt(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {p0, v3}, Lcom/squareup/banklinking/RoutingNumberUtil;->intAt(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v5, v3

    mul-int/lit8 v5, v5, 0x3

    add-int/2addr v1, v5

    const/4 v2, 0x2

    .line 76
    invoke-static {p0, v2}, Lcom/squareup/banklinking/RoutingNumberUtil;->intAt(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x5

    invoke-static {p0, v3}, Lcom/squareup/banklinking/RoutingNumberUtil;->intAt(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x9

    add-int/2addr v1, v2

    .line 77
    rem-int/lit8 v1, v1, 0xa

    const/16 v2, 0x8

    .line 79
    invoke-static {p0, v2}, Lcom/squareup/banklinking/RoutingNumberUtil;->intAt(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    if-ne v1, p0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private static hasGoodRoutingPrefix(Ljava/lang/String;)Z
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x2

    .line 83
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    .line 85
    sget-object v1, Lcom/squareup/banklinking/RoutingNumberUtil;->ROUTING_TRANSIT_LEAD_RANGES:[[I

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    .line 86
    aget v5, v4, v0

    if-lt p0, v5, :cond_0

    const/4 v5, 0x1

    aget v4, v4, v5

    if-gt p0, v4, :cond_0

    return v5

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method private static intAt(Ljava/lang/String;I)Ljava/lang/Integer;
    .locals 1

    add-int/lit8 v0, p1, 0x1

    .line 95
    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method public static isRoutingTransitNumber(Ljava/lang/CharSequence;)Z
    .locals 2

    .line 45
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    .line 47
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    .line 48
    invoke-static {p0}, Lcom/squareup/banklinking/RoutingNumberUtil;->hasGoodRoutingPrefix(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    invoke-static {p0}, Lcom/squareup/banklinking/RoutingNumberUtil;->hasGoodRoutingChecksum(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static noBankLogo(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 8

    .line 61
    sget-object v0, Lcom/squareup/banklinking/RoutingNumberUtil;->noBankLogo:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Lcom/squareup/register/widgets/StringDrawable;

    sget v1, Lcom/squareup/banklinking/R$string;->field_validation_error:I

    .line 63
    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v1, Lcom/squareup/banklinking/R$dimen;->field_error_text_size:I

    .line 64
    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    sget v1, Lcom/squareup/banklinking/R$color;->field_error_indicator:I

    .line 66
    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    sget v1, Lcom/squareup/banklinking/R$dimen;->bank_logo_error_shadow_radius:I

    .line 67
    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    sget v1, Lcom/squareup/banklinking/R$dimen;->bank_logo_error_shadow_fudge:I

    .line 68
    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/register/widgets/StringDrawable;-><init>(Ljava/lang/String;Ljava/lang/Float;Landroid/graphics/Typeface;IFF)V

    sput-object v0, Lcom/squareup/banklinking/RoutingNumberUtil;->noBankLogo:Landroid/graphics/drawable/Drawable;

    .line 70
    :cond_0
    sget-object p0, Lcom/squareup/banklinking/RoutingNumberUtil;->noBankLogo:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public static validateRoutingNumber(Ljava/lang/String;Landroid/widget/EditText;)V
    .locals 4

    .line 23
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 25
    :cond_0
    invoke-virtual {p1}, Landroid/widget/EditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 26
    invoke-static {p0}, Lcom/squareup/banklinking/RoutingNumberUtil;->isRoutingTransitNumber(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_1

    .line 27
    invoke-static {v0}, Lcom/squareup/banklinking/RoutingNumberUtil;->noBankLogo(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    goto :goto_0

    .line 29
    :cond_1
    invoke-static {p0}, Lcom/squareup/banklinking/RoutingNumberUtil;->bankNameForRoutingNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 31
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bank_logo_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 32
    invoke-virtual {p1}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "drawable"

    .line 31
    invoke-virtual {v0, p0, v3, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p0

    if-eqz p0, :cond_2

    .line 34
    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    goto :goto_0

    :cond_2
    move-object p0, v2

    .line 38
    :goto_0
    invoke-virtual {p1, v2, v2, p0, v2}, Landroid/widget/EditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
