.class final Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$observeValidation$1;
.super Ljava/lang/Object;
.source "BankFieldsCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->observeValidation(Lrx/Observable;[Landroid/widget/TextView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBankFieldsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BankFieldsCoordinator.kt\ncom/squareup/banklinking/widgets/BankFieldsCoordinator$observeValidation$1\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,142:1\n11416#2,2:143\n*E\n*S KotlinDebug\n*F\n+ 1 BankFieldsCoordinator.kt\ncom/squareup/banklinking/widgets/BankFieldsCoordinator$observeValidation$1\n*L\n106#1,2:143\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "result",
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $destinationViews:[Landroid/widget/TextView;

.field final synthetic this$0:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;[Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$observeValidation$1;->this$0:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    iput-object p2, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$observeValidation$1;->$destinationViews:[Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;)V
    .locals 5

    .line 105
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$observeValidation$1;->this$0:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    invoke-static {v0}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->access$getContext$p(Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$observeValidation$1;->this$0:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    const-string v3, "result"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v0, p1}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->access$getIcon(Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;Landroid/content/Context;Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v1

    .line 106
    :goto_0
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$observeValidation$1;->$destinationViews:[Landroid/widget/TextView;

    .line 143
    array-length v2, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_1

    aget-object v4, v0, v3

    .line 107
    invoke-virtual {v4, v1, v1, p1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$observeValidation$1;->call(Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;)V

    return-void
.end method
