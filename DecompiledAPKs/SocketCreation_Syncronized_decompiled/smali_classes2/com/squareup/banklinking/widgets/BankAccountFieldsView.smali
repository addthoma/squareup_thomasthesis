.class public final Lcom/squareup/banklinking/widgets/BankAccountFieldsView;
.super Landroid/widget/LinearLayout;
.source "BankAccountFieldsView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;,
        Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;,
        Lcom/squareup/banklinking/widgets/BankAccountFieldsView$ParentComponent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBankAccountFieldsView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BankAccountFieldsView.kt\ncom/squareup/banklinking/widgets/BankAccountFieldsView\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,93:1\n66#2:94\n1261#3:95\n*E\n*S KotlinDebug\n*F\n+ 1 BankAccountFieldsView.kt\ncom/squareup/banklinking/widgets/BankAccountFieldsView\n*L\n61#1:94\n29#1:95\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u0003#$%B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016J\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018J\u000e\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dJ\u0008\u0010\u001e\u001a\u00020\u001fH\u0014J\u0008\u0010 \u001a\u00020\u0010H\u0002J\u0016\u0010!\u001a\u00020\u001f2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\"\u001a\u00020\u001bR\u001e\u0010\t\u001a\u00020\n8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0011\u001a\u00020\n8FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0013\u0010\u0014\u001a\u0004\u0008\u0012\u0010\u000c\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "_countryCode",
        "Lcom/squareup/CountryCode;",
        "get_countryCode$public_release",
        "()Lcom/squareup/CountryCode;",
        "set_countryCode$public_release",
        "(Lcom/squareup/CountryCode;)V",
        "coordinator",
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;",
        "countryCode",
        "getCountryCode",
        "countryCode$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "emptyField",
        "Landroid/view/View;",
        "fieldChanged",
        "Lrx/Observable;",
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;",
        "getField",
        "",
        "fieldType",
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;",
        "onFinishInflate",
        "",
        "resolveBankNumbersCoordinator",
        "setField",
        "fieldText",
        "FieldChange",
        "FieldType",
        "ParentComponent",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field public _countryCode:Lcom/squareup/CountryCode;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private coordinator:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

.field private final countryCode$delegate:Lkotlin/properties/ReadWriteProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "countryCode"

    const-string v4, "getCountryCode()Lcom/squareup/CountryCode;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    sget-object v0, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-virtual {v0, p1}, Lcom/squareup/util/VectorFriendly;->ensureContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    new-instance p1, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$countryCode$2;

    move-object p2, p0

    check-cast p2, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;

    invoke-direct {p1, p2}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$countryCode$2;-><init>(Lcom/squareup/banklinking/widgets/BankAccountFieldsView;)V

    invoke-static {p1}, Lcom/squareup/util/DelegatesKt;->alias(Lkotlin/reflect/KMutableProperty0;)Lkotlin/properties/ReadWriteProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->countryCode$delegate:Lkotlin/properties/ReadWriteProperty;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 27
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 28
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getCoordinator$p(Lcom/squareup/banklinking/widgets/BankAccountFieldsView;)Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;
    .locals 1

    .line 24
    iget-object p0, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->coordinator:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    if-nez p0, :cond_0

    const-string v0, "coordinator"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$resolveBankNumbersCoordinator(Lcom/squareup/banklinking/widgets/BankAccountFieldsView;)Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->resolveBankNumbersCoordinator()Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setCoordinator$p(Lcom/squareup/banklinking/widgets/BankAccountFieldsView;Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->coordinator:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    return-void
.end method

.method private final resolveBankNumbersCoordinator()Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;
    .locals 4

    .line 87
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->_countryCode:Lcom/squareup/CountryCode;

    const-string v1, "_countryCode"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v2, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_3

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->_countryCode:Lcom/squareup/CountryCode;

    if-nez v3, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " does not support native bank linking"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 89
    :cond_2
    new-instance v0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;

    invoke-direct {v0}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;-><init>()V

    check-cast v0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    goto :goto_0

    .line 88
    :cond_3
    new-instance v0, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;

    invoke-direct {v0}, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;-><init>()V

    check-cast v0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    :goto_0
    return-object v0
.end method


# virtual methods
.method public final emptyField()Landroid/view/View;
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->coordinator:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    if-nez v0, :cond_0

    const-string v1, "coordinator"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->emptyField()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final fieldChanged()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;",
            ">;"
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->coordinator:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    if-nez v0, :cond_0

    const-string v1, "coordinator"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->fieldChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final getCountryCode()Lcom/squareup/CountryCode;
    .locals 3

    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->countryCode$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public final getField(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Ljava/lang/String;
    .locals 2

    const-string v0, "fieldType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->coordinator:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    if-nez v0, :cond_0

    const-string v1, "coordinator"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->getField(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final get_countryCode$public_release()Lcom/squareup/CountryCode;
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->_countryCode:Lcom/squareup/CountryCode;

    if-nez v0, :cond_0

    const-string v1, "_countryCode"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 58
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const/4 v0, 0x1

    .line 59
    invoke-virtual {p0, v0}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->setOrientation(I)V

    .line 61
    invoke-virtual {p0}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    const-class v1, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$ParentComponent;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->componentInParent(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$ParentComponent;

    .line 62
    invoke-interface {v0, p0}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$ParentComponent;->inject(Lcom/squareup/banklinking/widgets/BankAccountFieldsView;)V

    .line 64
    move-object v0, p0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$onFinishInflate$1;

    invoke-direct {v1, p0}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$onFinishInflate$1;-><init>(Lcom/squareup/banklinking/widgets/BankAccountFieldsView;)V

    check-cast v1, Lcom/squareup/coordinators/CoordinatorProvider;

    invoke-static {v0, v1}, Lcom/squareup/coordinators/Coordinators;->bind(Landroid/view/View;Lcom/squareup/coordinators/CoordinatorProvider;)V

    return-void
.end method

.method public final setField(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Ljava/lang/String;)V
    .locals 2

    const-string v0, "fieldType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fieldText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->coordinator:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    if-nez v0, :cond_0

    const-string v1, "coordinator"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->setField(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Ljava/lang/String;)V

    return-void
.end method

.method public final set_countryCode$public_release(Lcom/squareup/CountryCode;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->_countryCode:Lcom/squareup/CountryCode;

    return-void
.end method
