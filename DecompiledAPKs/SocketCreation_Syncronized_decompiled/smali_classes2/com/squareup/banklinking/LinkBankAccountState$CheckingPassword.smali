.class public final Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;
.super Lcom/squareup/banklinking/LinkBankAccountState;
.source "LinkBankAccountState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/LinkBankAccountState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CheckingPassword"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J+\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u00c6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\u0013\u0010\u0015\u001a\u00020\u00032\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u0005H\u00d6\u0001J\u0019\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0014H\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;",
        "Lcom/squareup/banklinking/LinkBankAccountState;",
        "requiresPassword",
        "",
        "primaryInstitutionNumber",
        "",
        "verificationState",
        "Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;",
        "(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V",
        "getPrimaryInstitutionNumber",
        "()Ljava/lang/String;",
        "getRequiresPassword",
        "()Z",
        "getVerificationState",
        "()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final primaryInstitutionNumber:Ljava/lang/String;

.field private final requiresPassword:Z

.field private final verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword$Creator;

    invoke-direct {v0}, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword$Creator;-><init>()V

    sput-object v0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V
    .locals 1

    const/4 v0, 0x0

    .line 16
    invoke-direct {p0, v0}, Lcom/squareup/banklinking/LinkBankAccountState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->requiresPassword:Z

    iput-object p2, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->primaryInstitutionNumber:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;ILjava/lang/Object;)Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-boolean p1, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->requiresPassword:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->primaryInstitutionNumber:Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->copy(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->requiresPassword:Z

    return v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->primaryInstitutionNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-object v0
.end method

.method public final copy(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;
    .locals 1

    new-instance v0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;-><init>(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;

    iget-boolean v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->requiresPassword:Z

    iget-boolean v1, p1, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->requiresPassword:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->primaryInstitutionNumber:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->primaryInstitutionNumber:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    iget-object p1, p1, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPrimaryInstitutionNumber()Ljava/lang/String;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->primaryInstitutionNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final getRequiresPassword()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->requiresPassword:Z

    return v0
.end method

.method public final getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->requiresPassword:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->primaryInstitutionNumber:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CheckingPassword(requiresPassword="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->requiresPassword:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", primaryInstitutionNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->primaryInstitutionNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", verificationState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean p2, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->requiresPassword:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object p2, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->primaryInstitutionNumber:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    :goto_0
    return-void
.end method
