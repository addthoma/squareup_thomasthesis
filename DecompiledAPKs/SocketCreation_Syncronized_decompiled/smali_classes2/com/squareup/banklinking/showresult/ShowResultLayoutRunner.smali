.class public final Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner;
.super Ljava/lang/Object;
.source "ShowResultLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/banklinking/showresult/ShowResultScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \r2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\rB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/banklinking/showresult/ShowResultScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "resultMessage",
        "Lcom/squareup/noho/NohoMessageView;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner$Companion;


# instance fields
.field private final resultMessage:Lcom/squareup/noho/NohoMessageView;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner;->Companion:Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner;->view:Landroid/view/View;

    .line 16
    iget-object p1, p0, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/banklinking/impl/R$id;->show_result_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner;->resultMessage:Lcom/squareup/noho/NohoMessageView;

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/banklinking/showresult/ShowResultScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 2

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object p2, p0, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner$showRendering$1;-><init>(Lcom/squareup/banklinking/showresult/ShowResultScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 23
    iget-object p2, p0, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner;->resultMessage:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p1}, Lcom/squareup/banklinking/showresult/ShowResultScreen;->getDrawable()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoMessageView;->setDrawable(I)V

    .line 24
    iget-object p2, p0, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner;->resultMessage:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p1}, Lcom/squareup/banklinking/showresult/ShowResultScreen;->getTitle()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 25
    iget-object p2, p0, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner;->resultMessage:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p1}, Lcom/squareup/banklinking/showresult/ShowResultScreen;->getMessage()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    .line 26
    iget-object p2, p0, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner;->resultMessage:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p1}, Lcom/squareup/banklinking/showresult/ShowResultScreen;->getPrimaryButtonText()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonText(I)V

    .line 27
    iget-object p2, p0, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner;->resultMessage:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p1}, Lcom/squareup/banklinking/showresult/ShowResultScreen;->getSecondaryButtonText()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonText(I)V

    .line 28
    iget-object p2, p0, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner;->resultMessage:Lcom/squareup/noho/NohoMessageView;

    .line 29
    new-instance v0, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner$showRendering$2;

    invoke-direct {v0, p1}, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner$showRendering$2;-><init>(Lcom/squareup/banklinking/showresult/ShowResultScreen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    const-string v1, "debounce { rendering.onPrimaryButtonClicked() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 31
    iget-object p2, p0, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner;->resultMessage:Lcom/squareup/noho/NohoMessageView;

    .line 32
    new-instance v0, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner$showRendering$3;

    invoke-direct {v0, p1}, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner$showRendering$3;-><init>(Lcom/squareup/banklinking/showresult/ShowResultScreen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    const-string v0, "debounce { rendering.onSecondaryButtonClicked() }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/banklinking/showresult/ShowResultScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner;->showRendering(Lcom/squareup/banklinking/showresult/ShowResultScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
