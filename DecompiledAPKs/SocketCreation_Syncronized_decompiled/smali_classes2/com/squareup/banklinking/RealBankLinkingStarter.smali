.class public final Lcom/squareup/banklinking/RealBankLinkingStarter;
.super Ljava/lang/Object;
.source "RealBankLinkingStarter.kt"

# interfaces
.implements Lcom/squareup/banklinking/BankLinkingStarter;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0008\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0014\u001a\u00020\u0012H\u0016J\u0010\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u000e\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0018H\u0016J\u0012\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u001a*\u00020\u0003H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/banklinking/RealBankLinkingStarter;",
        "Lcom/squareup/banklinking/BankLinkingStarter;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "onboardingType",
        "Lcom/squareup/onboarding/OnboardingType;",
        "onboardingStarter",
        "Lcom/squareup/onboarding/OnboardingStarter;",
        "bankAccountSettings",
        "Lcom/squareup/banklinking/BankAccountSettings;",
        "res",
        "Lcom/squareup/util/Res;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/util/Res;Lcom/squareup/util/BrowserLauncher;)V",
        "scope",
        "Lmortar/MortarScope;",
        "maybeStartBankLinking",
        "",
        "onEnterScope",
        "onExitScope",
        "startBankLinking",
        "variant",
        "Lcom/squareup/banklinking/BankLinkingStarter$Variant;",
        "Lrx/Observable;",
        "bankLinkingVariant",
        "Lio/reactivex/Observable;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final onboardingStarter:Lcom/squareup/onboarding/OnboardingStarter;

.field private final onboardingType:Lcom/squareup/onboarding/OnboardingType;

.field private final res:Lcom/squareup/util/Res;

.field private scope:Lmortar/MortarScope;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/util/Res;Lcom/squareup/util/BrowserLauncher;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingStarter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bankAccountSettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "browserLauncher"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/RealBankLinkingStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p2, p0, Lcom/squareup/banklinking/RealBankLinkingStarter;->onboardingType:Lcom/squareup/onboarding/OnboardingType;

    iput-object p3, p0, Lcom/squareup/banklinking/RealBankLinkingStarter;->onboardingStarter:Lcom/squareup/onboarding/OnboardingStarter;

    iput-object p4, p0, Lcom/squareup/banklinking/RealBankLinkingStarter;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    iput-object p5, p0, Lcom/squareup/banklinking/RealBankLinkingStarter;->res:Lcom/squareup/util/Res;

    iput-object p6, p0, Lcom/squareup/banklinking/RealBankLinkingStarter;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-void
.end method

.method public static final synthetic access$bankLinkingVariant(Lcom/squareup/banklinking/RealBankLinkingStarter;Lcom/squareup/settings/server/AccountStatusSettings;)Lio/reactivex/Observable;
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealBankLinkingStarter;->bankLinkingVariant(Lcom/squareup/settings/server/AccountStatusSettings;)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getOnboardingType$p(Lcom/squareup/banklinking/RealBankLinkingStarter;)Lcom/squareup/onboarding/OnboardingType;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/banklinking/RealBankLinkingStarter;->onboardingType:Lcom/squareup/onboarding/OnboardingType;

    return-object p0
.end method

.method public static final synthetic access$getSettings$p(Lcom/squareup/banklinking/RealBankLinkingStarter;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/banklinking/RealBankLinkingStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method public static final synthetic access$startBankLinking(Lcom/squareup/banklinking/RealBankLinkingStarter;Lcom/squareup/banklinking/BankLinkingStarter$Variant;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealBankLinkingStarter;->startBankLinking(Lcom/squareup/banklinking/BankLinkingStarter$Variant;)V

    return-void
.end method

.method private final bankLinkingVariant(Lcom/squareup/settings/server/AccountStatusSettings;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/banklinking/BankLinkingStarter$Variant;",
            ">;"
        }
    .end annotation

    .line 77
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/banklinking/RealBankLinkingStarter$bankLinkingVariant$1;

    invoke-direct {v1, p1}, Lcom/squareup/banklinking/RealBankLinkingStarter$bankLinkingVariant$1;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "settingsAvailable().map \u2026else -> WEB\n      }\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final startBankLinking(Lcom/squareup/banklinking/BankLinkingStarter$Variant;)V
    .locals 4

    .line 70
    sget-object v0, Lcom/squareup/banklinking/RealBankLinkingStarter$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/banklinking/BankLinkingStarter$Variant;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 73
    iget-object p1, p0, Lcom/squareup/banklinking/RealBankLinkingStarter;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/banklinking/RealBankLinkingStarter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/banklinking/R$string;->link_bank_account_url:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 72
    :cond_1
    iget-object p1, p0, Lcom/squareup/banklinking/RealBankLinkingStarter;->onboardingStarter:Lcom/squareup/onboarding/OnboardingStarter;

    new-instance v1, Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;

    sget-object v2, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;->RESTART:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0, v3}, Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;-><init>(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {p1, v1}, Lcom/squareup/onboarding/OnboardingStarter;->startActivation(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)V

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public maybeStartBankLinking()V
    .locals 4

    .line 62
    iget-object v0, p0, Lcom/squareup/banklinking/RealBankLinkingStarter;->scope:Lmortar/MortarScope;

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/squareup/banklinking/RealBankLinkingStarter;->variant()Lrx/Observable;

    move-result-object v1

    .line 65
    invoke-virtual {v1}, Lrx/Observable;->first()Lrx/Observable;

    move-result-object v1

    .line 66
    new-instance v2, Lcom/squareup/banklinking/RealBankLinkingStarter$maybeStartBankLinking$1;

    move-object v3, p0

    check-cast v3, Lcom/squareup/banklinking/RealBankLinkingStarter;

    invoke-direct {v2, v3}, Lcom/squareup/banklinking/RealBankLinkingStarter$maybeStartBankLinking$1;-><init>(Lcom/squareup/banklinking/RealBankLinkingStarter;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/banklinking/RealBankLinkingStarter$sam$rx_functions_Action1$0;

    invoke-direct {v3, v2}, Lcom/squareup/banklinking/RealBankLinkingStarter$sam$rx_functions_Action1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lrx/functions/Action1;

    invoke-virtual {v1, v3}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v1

    const-string/jumbo v2, "variant()\n        .first\u2026cribe(::startBankLinking)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-static {v1, v0}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnExit(Lrx/Subscription;Lmortar/MortarScope;)V

    :cond_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iput-object p1, p0, Lcom/squareup/banklinking/RealBankLinkingStarter;->scope:Lmortar/MortarScope;

    return-void
.end method

.method public onExitScope()V
    .locals 1

    const/4 v0, 0x0

    .line 41
    check-cast v0, Lmortar/MortarScope;

    iput-object v0, p0, Lcom/squareup/banklinking/RealBankLinkingStarter;->scope:Lmortar/MortarScope;

    return-void
.end method

.method public variant()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/banklinking/BankLinkingStarter$Variant;",
            ">;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/squareup/banklinking/RealBankLinkingStarter;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v0}, Lcom/squareup/banklinking/BankAccountSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    .line 46
    new-instance v1, Lcom/squareup/banklinking/RealBankLinkingStarter$variant$1;

    invoke-direct {v1, p0}, Lcom/squareup/banklinking/RealBankLinkingStarter$variant$1;-><init>(Lcom/squareup/banklinking/RealBankLinkingStarter;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "bankAccountSettings.stat\u2026              }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/ObservableSource;

    .line 58
    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method
