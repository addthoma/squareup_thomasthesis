.class public interface abstract Lcom/squareup/blescan/WirelessSearcher;
.super Ljava/lang/Object;
.source "WirelessSearcher.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u001c\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0008\u0010\u0008\u001a\u00020\tH&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/blescan/WirelessSearcher;",
        "",
        "startSearch",
        "Lcom/squareup/blescan/BleOperationResult;",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/blescan/DiscoveredDevices;",
        "timeoutMillis",
        "",
        "stopSearch",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract startSearch(I)Lcom/squareup/blescan/BleOperationResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/squareup/blescan/BleOperationResult<",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/blescan/DiscoveredDevices;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract stopSearch()V
.end method
