.class public final Lcom/squareup/blescan/BleScanner$Scan;
.super Landroid/bluetooth/le/ScanCallback;
.source "BleScanner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/blescan/BleScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "Scan"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBleScanner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BleScanner.kt\ncom/squareup/blescan/BleScanner$Scan\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,182:1\n1642#2,2:183\n*E\n*S KotlinDebug\n*F\n+ 1 BleScanner.kt\ncom/squareup/blescan/BleScanner$Scan\n*L\n102#1,2:183\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u000c\u001a\u00020\rH\u0002J\u0016\u0010\u000e\u001a\u00020\r2\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010H\u0016J\u0010\u0010\u0012\u001a\u00020\r2\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0018\u0010\u0015\u001a\u00020\r2\u0006\u0010\u0016\u001a\u00020\u00142\u0006\u0010\u0017\u001a\u00020\u0011H\u0016J\u0012\u0010\u0018\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u001a0\u0019J\u0006\u0010\u001b\u001a\u00020\rR\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\t0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/blescan/BleScanner$Scan;",
        "Landroid/bluetooth/le/ScanCallback;",
        "(Lcom/squareup/blescan/BleScanner;)V",
        "bleResults",
        "Lio/reactivex/subjects/PublishSubject;",
        "Lcom/squareup/blescan/DiscoveredDevices;",
        "cardreaders",
        "",
        "",
        "Lcom/squareup/blescan/DiscoveredDevice;",
        "error",
        "",
        "emitResults",
        "",
        "onBatchScanResults",
        "results",
        "",
        "Landroid/bluetooth/le/ScanResult;",
        "onScanFailed",
        "errorCode",
        "",
        "onScanResult",
        "callbackType",
        "result",
        "start",
        "Lcom/squareup/blescan/BleOperationResult;",
        "Lio/reactivex/Observable;",
        "stop",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bleResults:Lio/reactivex/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/PublishSubject<",
            "Lcom/squareup/blescan/DiscoveredDevices;",
            ">;"
        }
    .end annotation
.end field

.field private final cardreaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/blescan/DiscoveredDevice;",
            ">;"
        }
    .end annotation
.end field

.field private error:Z

.field final synthetic this$0:Lcom/squareup/blescan/BleScanner;


# direct methods
.method public constructor <init>(Lcom/squareup/blescan/BleScanner;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 34
    iput-object p1, p0, Lcom/squareup/blescan/BleScanner$Scan;->this$0:Lcom/squareup/blescan/BleScanner;

    invoke-direct {p0}, Landroid/bluetooth/le/ScanCallback;-><init>()V

    .line 35
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/blescan/BleScanner$Scan;->cardreaders:Ljava/util/Map;

    .line 36
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object p1

    const-string v0, "PublishSubject.create()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/blescan/BleScanner$Scan;->bleResults:Lio/reactivex/subjects/PublishSubject;

    return-void
.end method

.method public static final synthetic access$emitResults(Lcom/squareup/blescan/BleScanner$Scan;)V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/squareup/blescan/BleScanner$Scan;->emitResults()V

    return-void
.end method

.method public static final synthetic access$getCardreaders$p(Lcom/squareup/blescan/BleScanner$Scan;)Ljava/util/Map;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/blescan/BleScanner$Scan;->cardreaders:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic access$getError$p(Lcom/squareup/blescan/BleScanner$Scan;)Z
    .locals 0

    .line 34
    iget-boolean p0, p0, Lcom/squareup/blescan/BleScanner$Scan;->error:Z

    return p0
.end method

.method public static final synthetic access$setError$p(Lcom/squareup/blescan/BleScanner$Scan;Z)V
    .locals 0

    .line 34
    iput-boolean p1, p0, Lcom/squareup/blescan/BleScanner$Scan;->error:Z

    return-void
.end method

.method private final emitResults()V
    .locals 4

    .line 73
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner$Scan;->this$0:Lcom/squareup/blescan/BleScanner;

    invoke-static {v0}, Lcom/squareup/blescan/BleScanner;->access$getThreadEnforcer$p(Lcom/squareup/blescan/BleScanner;)Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 74
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner$Scan;->bleResults:Lio/reactivex/subjects/PublishSubject;

    .line 75
    new-instance v1, Lcom/squareup/blescan/DiscoveredDevices;

    .line 76
    iget-object v2, p0, Lcom/squareup/blescan/BleScanner$Scan;->cardreaders:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    .line 77
    iget-boolean v3, p0, Lcom/squareup/blescan/BleScanner$Scan;->error:Z

    .line 75
    invoke-direct {v1, v2, v3}, Lcom/squareup/blescan/DiscoveredDevices;-><init>(Ljava/util/List;Z)V

    .line 74
    invoke-virtual {v0, v1}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public onBatchScanResults(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/bluetooth/le/ScanResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "results"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    check-cast p1, Ljava/lang/Iterable;

    .line 183
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/le/ScanResult;

    const/4 v1, 0x1

    .line 103
    invoke-virtual {p0, v1, v0}, Lcom/squareup/blescan/BleScanner$Scan;->onScanResult(ILandroid/bluetooth/le/ScanResult;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onScanFailed(I)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 64
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "Failed to ble scan: %d"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 65
    iget-object p1, p0, Lcom/squareup/blescan/BleScanner$Scan;->this$0:Lcom/squareup/blescan/BleScanner;

    invoke-static {p1}, Lcom/squareup/blescan/BleScanner;->access$getMainThread$p(Lcom/squareup/blescan/BleScanner;)Lcom/squareup/thread/executor/MainThread;

    move-result-object p1

    new-instance v0, Lcom/squareup/blescan/BleScanner$Scan$onScanFailed$1;

    invoke-direct {v0, p0}, Lcom/squareup/blescan/BleScanner$Scan$onScanFailed$1;-><init>(Lcom/squareup/blescan/BleScanner$Scan;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {p1, v0}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onScanResult(ILandroid/bluetooth/le/ScanResult;)V
    .locals 1

    const-string p1, "result"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-static {p2}, Lcom/squareup/blescan/BleScannerKt;->access$buildBleScanResult(Landroid/bluetooth/le/ScanResult;)Lcom/squareup/blescan/BleScanResult;

    move-result-object p1

    .line 87
    iget-object p2, p0, Lcom/squareup/blescan/BleScanner$Scan;->this$0:Lcom/squareup/blescan/BleScanner;

    invoke-static {p2}, Lcom/squareup/blescan/BleScanner;->access$isSearching(Lcom/squareup/blescan/BleScanner;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 88
    iget-object p2, p0, Lcom/squareup/blescan/BleScanner$Scan;->this$0:Lcom/squareup/blescan/BleScanner;

    invoke-static {p2}, Lcom/squareup/blescan/BleScanner;->access$getMainThread$p(Lcom/squareup/blescan/BleScanner;)Lcom/squareup/thread/executor/MainThread;

    move-result-object p2

    new-instance v0, Lcom/squareup/blescan/BleScanner$Scan$onScanResult$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/blescan/BleScanner$Scan$onScanResult$1;-><init>(Lcom/squareup/blescan/BleScanner$Scan;Lcom/squareup/blescan/BleScanResult;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {p2, v0}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public final start()Lcom/squareup/blescan/BleOperationResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/blescan/BleOperationResult<",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/blescan/DiscoveredDevices;",
            ">;>;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner$Scan;->this$0:Lcom/squareup/blescan/BleScanner;

    invoke-static {v0}, Lcom/squareup/blescan/BleScanner;->access$getThreadEnforcer$p(Lcom/squareup/blescan/BleScanner;)Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 41
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner$Scan;->this$0:Lcom/squareup/blescan/BleScanner;

    invoke-static {v0}, Lcom/squareup/blescan/BleScanner;->access$getBluetoothLeScanner$p(Lcom/squareup/blescan/BleScanner;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/le/BluetoothLeScanner;

    if-eqz v0, :cond_0

    .line 43
    iget-object v1, p0, Lcom/squareup/blescan/BleScanner$Scan;->this$0:Lcom/squareup/blescan/BleScanner;

    invoke-static {v1}, Lcom/squareup/blescan/BleScanner;->access$getBleScanFilter$p(Lcom/squareup/blescan/BleScanner;)Lcom/squareup/blescan/BleScanFilter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/blescan/BleScanFilter;->createScanFiltersForSquarePos()Ljava/util/List;

    move-result-object v1

    .line 44
    new-instance v2, Landroid/bluetooth/le/ScanSettings$Builder;

    invoke-direct {v2}, Landroid/bluetooth/le/ScanSettings$Builder;-><init>()V

    const/4 v3, 0x2

    .line 45
    invoke-virtual {v2, v3}, Landroid/bluetooth/le/ScanSettings$Builder;->setScanMode(I)Landroid/bluetooth/le/ScanSettings$Builder;

    move-result-object v2

    .line 46
    invoke-virtual {v2}, Landroid/bluetooth/le/ScanSettings$Builder;->build()Landroid/bluetooth/le/ScanSettings;

    move-result-object v2

    .line 48
    move-object v3, p0

    check-cast v3, Landroid/bluetooth/le/ScanCallback;

    invoke-virtual {v0, v1, v2, v3}, Landroid/bluetooth/le/BluetoothLeScanner;->startScan(Ljava/util/List;Landroid/bluetooth/le/ScanSettings;Landroid/bluetooth/le/ScanCallback;)V

    .line 50
    new-instance v0, Lcom/squareup/blescan/BleOperationResult$Success;

    iget-object v1, p0, Lcom/squareup/blescan/BleScanner$Scan;->bleResults:Lio/reactivex/subjects/PublishSubject;

    invoke-direct {v0, v1}, Lcom/squareup/blescan/BleOperationResult$Success;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/blescan/BleOperationResult;

    return-object v0

    .line 41
    :cond_0
    sget-object v0, Lcom/squareup/blescan/BleOperationResult$Failed$BleDisabled;->INSTANCE:Lcom/squareup/blescan/BleOperationResult$Failed$BleDisabled;

    check-cast v0, Lcom/squareup/blescan/BleOperationResult;

    return-object v0
.end method

.method public final stop()V
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner$Scan;->this$0:Lcom/squareup/blescan/BleScanner;

    invoke-static {v0}, Lcom/squareup/blescan/BleScanner;->access$getThreadEnforcer$p(Lcom/squareup/blescan/BleScanner;)Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Stopping scanning of ble devices."

    .line 56
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner$Scan;->this$0:Lcom/squareup/blescan/BleScanner;

    invoke-static {v0}, Lcom/squareup/blescan/BleScanner;->access$getBluetoothLeScanner$p(Lcom/squareup/blescan/BleScanner;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/le/BluetoothLeScanner;

    if-eqz v0, :cond_0

    .line 58
    move-object v1, p0

    check-cast v1, Landroid/bluetooth/le/ScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/le/BluetoothLeScanner;->stopScan(Landroid/bluetooth/le/ScanCallback;)V

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner$Scan;->bleResults:Lio/reactivex/subjects/PublishSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/PublishSubject;->onComplete()V

    return-void
.end method
