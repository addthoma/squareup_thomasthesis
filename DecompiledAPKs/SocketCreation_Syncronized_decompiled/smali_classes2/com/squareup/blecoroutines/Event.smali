.class public final enum Lcom/squareup/blecoroutines/Event;
.super Ljava/lang/Enum;
.source "BleError.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/blecoroutines/Event;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0014\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/blecoroutines/Event;",
        "",
        "(Ljava/lang/String;I)V",
        "SERVICE_DISCOVERY_FAILED",
        "SERVICE_MISSING",
        "CHARACTERISTIC_MISSING",
        "CHARACTERISTIC_READ_FAILED",
        "CHARACTERISTIC_WRITE_FAILED",
        "CHARACTERISTIC_SUBSCRIBE_FAILED",
        "CONNECTION_FAILED",
        "TIMEOUT",
        "DESCRIPTOR_MISSING",
        "DESCRIPTOR_WRITE_FAILED",
        "ALREADY_DISCONNECTED",
        "CREATE_BOND_FAILED",
        "REMOVE_BOND_FAILED",
        "BLE_BUSY",
        "INTERNAL_BLE_ERROR",
        "ALREADY_SUBSCRIBED",
        "ALREADY_CONNECTING",
        "DISCONNECTED_DURING_OPERATION",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/blecoroutines/Event;

.field public static final enum ALREADY_CONNECTING:Lcom/squareup/blecoroutines/Event;

.field public static final enum ALREADY_DISCONNECTED:Lcom/squareup/blecoroutines/Event;

.field public static final enum ALREADY_SUBSCRIBED:Lcom/squareup/blecoroutines/Event;

.field public static final enum BLE_BUSY:Lcom/squareup/blecoroutines/Event;

.field public static final enum CHARACTERISTIC_MISSING:Lcom/squareup/blecoroutines/Event;

.field public static final enum CHARACTERISTIC_READ_FAILED:Lcom/squareup/blecoroutines/Event;

.field public static final enum CHARACTERISTIC_SUBSCRIBE_FAILED:Lcom/squareup/blecoroutines/Event;

.field public static final enum CHARACTERISTIC_WRITE_FAILED:Lcom/squareup/blecoroutines/Event;

.field public static final enum CONNECTION_FAILED:Lcom/squareup/blecoroutines/Event;

.field public static final enum CREATE_BOND_FAILED:Lcom/squareup/blecoroutines/Event;

.field public static final enum DESCRIPTOR_MISSING:Lcom/squareup/blecoroutines/Event;

.field public static final enum DESCRIPTOR_WRITE_FAILED:Lcom/squareup/blecoroutines/Event;

.field public static final enum DISCONNECTED_DURING_OPERATION:Lcom/squareup/blecoroutines/Event;

.field public static final enum INTERNAL_BLE_ERROR:Lcom/squareup/blecoroutines/Event;

.field public static final enum REMOVE_BOND_FAILED:Lcom/squareup/blecoroutines/Event;

.field public static final enum SERVICE_DISCOVERY_FAILED:Lcom/squareup/blecoroutines/Event;

.field public static final enum SERVICE_MISSING:Lcom/squareup/blecoroutines/Event;

.field public static final enum TIMEOUT:Lcom/squareup/blecoroutines/Event;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x12

    new-array v0, v0, [Lcom/squareup/blecoroutines/Event;

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const/4 v2, 0x0

    const-string v3, "SERVICE_DISCOVERY_FAILED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->SERVICE_DISCOVERY_FAILED:Lcom/squareup/blecoroutines/Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const/4 v2, 0x1

    const-string v3, "SERVICE_MISSING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->SERVICE_MISSING:Lcom/squareup/blecoroutines/Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const/4 v2, 0x2

    const-string v3, "CHARACTERISTIC_MISSING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->CHARACTERISTIC_MISSING:Lcom/squareup/blecoroutines/Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const/4 v2, 0x3

    const-string v3, "CHARACTERISTIC_READ_FAILED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->CHARACTERISTIC_READ_FAILED:Lcom/squareup/blecoroutines/Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const/4 v2, 0x4

    const-string v3, "CHARACTERISTIC_WRITE_FAILED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->CHARACTERISTIC_WRITE_FAILED:Lcom/squareup/blecoroutines/Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const/4 v2, 0x5

    const-string v3, "CHARACTERISTIC_SUBSCRIBE_FAILED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->CHARACTERISTIC_SUBSCRIBE_FAILED:Lcom/squareup/blecoroutines/Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const/4 v2, 0x6

    const-string v3, "CONNECTION_FAILED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->CONNECTION_FAILED:Lcom/squareup/blecoroutines/Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const/4 v2, 0x7

    const-string v3, "TIMEOUT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->TIMEOUT:Lcom/squareup/blecoroutines/Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const/16 v2, 0x8

    const-string v3, "DESCRIPTOR_MISSING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->DESCRIPTOR_MISSING:Lcom/squareup/blecoroutines/Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const/16 v2, 0x9

    const-string v3, "DESCRIPTOR_WRITE_FAILED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->DESCRIPTOR_WRITE_FAILED:Lcom/squareup/blecoroutines/Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const/16 v2, 0xa

    const-string v3, "ALREADY_DISCONNECTED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->ALREADY_DISCONNECTED:Lcom/squareup/blecoroutines/Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const/16 v2, 0xb

    const-string v3, "CREATE_BOND_FAILED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->CREATE_BOND_FAILED:Lcom/squareup/blecoroutines/Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const/16 v2, 0xc

    const-string v3, "REMOVE_BOND_FAILED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->REMOVE_BOND_FAILED:Lcom/squareup/blecoroutines/Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const/16 v2, 0xd

    const-string v3, "BLE_BUSY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->BLE_BUSY:Lcom/squareup/blecoroutines/Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const/16 v2, 0xe

    const-string v3, "INTERNAL_BLE_ERROR"

    invoke-direct {v1, v3, v2}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->INTERNAL_BLE_ERROR:Lcom/squareup/blecoroutines/Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const-string v2, "ALREADY_SUBSCRIBED"

    const/16 v3, 0xf

    invoke-direct {v1, v2, v3}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->ALREADY_SUBSCRIBED:Lcom/squareup/blecoroutines/Event;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const-string v2, "ALREADY_CONNECTING"

    const/16 v3, 0x10

    invoke-direct {v1, v2, v3}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->ALREADY_CONNECTING:Lcom/squareup/blecoroutines/Event;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blecoroutines/Event;

    const-string v2, "DISCONNECTED_DURING_OPERATION"

    const/16 v3, 0x11

    invoke-direct {v1, v2, v3}, Lcom/squareup/blecoroutines/Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/blecoroutines/Event;->DISCONNECTED_DURING_OPERATION:Lcom/squareup/blecoroutines/Event;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/blecoroutines/Event;->$VALUES:[Lcom/squareup/blecoroutines/Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/blecoroutines/Event;
    .locals 1

    const-class v0, Lcom/squareup/blecoroutines/Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/blecoroutines/Event;

    return-object p0
.end method

.method public static values()[Lcom/squareup/blecoroutines/Event;
    .locals 1

    sget-object v0, Lcom/squareup/blecoroutines/Event;->$VALUES:[Lcom/squareup/blecoroutines/Event;

    invoke-virtual {v0}, [Lcom/squareup/blecoroutines/Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/blecoroutines/Event;

    return-object v0
.end method
