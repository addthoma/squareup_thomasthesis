.class public final Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "RealCapitalOfferWorkflow.kt"

# interfaces
.implements Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;",
        "Lcom/squareup/capital/flexloan/offer/CapitalOfferOutput;",
        "Lcom/squareup/capital/flexloan/offer/CapitalOfferScreen;",
        ">;",
        "Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCapitalOfferWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCapitalOfferWorkflow.kt\ncom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,96:1\n41#2:97\n56#2,2:98\n276#3:100\n*E\n*S KotlinDebug\n*F\n+ 1 RealCapitalOfferWorkflow.kt\ncom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow\n*L\n41#1:97\n41#1,2:98\n41#1:100\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0018\u00002\u00020\u00012\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0002B7\u0008\u0007\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u000e\u0008\u0001\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0003H\u0002J\u0018\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0018H\u0002J\u000e\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001bH\u0002J$\u0010\u001d\u001a\u00020\u00052\u0006\u0010\u001e\u001a\u00020\u00032\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020!\u0012\u0004\u0012\u00020\u00040 H\u0016R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;",
        "Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;",
        "Lcom/squareup/capital/flexloan/offer/CapitalOfferOutput;",
        "Lcom/squareup/capital/flexloan/offer/CapitalOfferScreen;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "multipassOtkHelper",
        "Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;",
        "analytics",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "activityListener",
        "Lcom/squareup/ActivityListener;",
        "(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Lcom/squareup/text/Formatter;Lcom/squareup/ActivityListener;)V",
        "getOfferAmount",
        "",
        "offer",
        "handleOtkFetchFailure",
        "",
        "targetUrl",
        "",
        "offerId",
        "onForegroundedActivity",
        "Lio/reactivex/Observable;",
        "",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final activityListener:Lcom/squareup/ActivityListener;

.field private final analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final multipassOtkHelper:Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Lcom/squareup/text/Formatter;Lcom/squareup/ActivityListener;)V
    .locals 1
    .param p4    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/Shorter;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/BrowserLauncher;",
            "Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ActivityListener;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "browserLauncher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "multipassOtkHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityListener"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iput-object p2, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->multipassOtkHelper:Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;

    iput-object p3, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    iput-object p4, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p5, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->activityListener:Lcom/squareup/ActivityListener;

    return-void
.end method

.method public static final synthetic access$getActivityListener$p(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;)Lcom/squareup/ActivityListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->activityListener:Lcom/squareup/ActivityListener;

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;)Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getBrowserLauncher$p(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;)Lcom/squareup/util/BrowserLauncher;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-object p0
.end method

.method public static final synthetic access$getMultipassOtkHelper$p(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;)Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->multipassOtkHelper:Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;

    return-object p0
.end method

.method public static final synthetic access$handleOtkFetchFailure(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->handleOtkFetchFailure(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final getOfferAmount(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;)Ljava/lang/CharSequence;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;->getCustomer()Lcom/squareup/protos/capital/external/business/models/Customer;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->max_financed_amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "moneyFormatter.format(of\u2026ffer.max_financed_amount)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final handleOtkFetchFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .line 89
    iget-object v0, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v1, p2

    invoke-static/range {v0 .. v5}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics$DefaultImpls;->logViewCapitalSessionBridgeError$default(Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    .line 90
    iget-object p2, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {p2, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method

.method private final onForegroundedActivity()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 67
    new-instance v0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$onForegroundedActivity$1;

    invoke-direct {v0, p0}, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$onForegroundedActivity$1;-><init>(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;)V

    check-cast v0, Lio/reactivex/ObservableOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026istener(listener) }\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public render(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/capital/flexloan/offer/CapitalOfferScreen;
    .locals 9

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->onForegroundedActivity()Lio/reactivex/Observable;

    move-result-object v0

    .line 97
    sget-object v1, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    const-string v1, "this.toFlowable(BUFFER)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_0

    .line 99
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 100
    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 41
    new-instance v0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$render$1;

    invoke-direct {v0, p0}, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$render$1;-><init>(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p2

    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 45
    new-instance v0, Lcom/squareup/capital/flexloan/offer/CapitalOfferScreen;

    .line 46
    new-instance v1, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$render$2;

    invoke-direct {v1, p0, p2}, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$render$2;-><init>(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->getOfferAmount(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;)Ljava/lang/CharSequence;

    move-result-object p2

    .line 48
    new-instance v2, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$render$3;

    invoke-direct {v2, p0, p1}, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$render$3;-><init>(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 45
    invoke-direct {v0, p2, v1, v2}, Lcom/squareup/capital/flexloan/offer/CapitalOfferScreen;-><init>(Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-object v0

    .line 99
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->render(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/capital/flexloan/offer/CapitalOfferScreen;

    move-result-object p1

    return-object p1
.end method
