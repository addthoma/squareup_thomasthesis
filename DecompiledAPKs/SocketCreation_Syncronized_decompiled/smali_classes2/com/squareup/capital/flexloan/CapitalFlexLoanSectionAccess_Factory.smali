.class public final Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess_Factory;
.super Ljava/lang/Object;
.source "CapitalFlexLoanSectionAccess_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 30
    iput-object p4, p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;",
            ">;)",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;)Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v2, p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v3, p0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;)Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess_Factory;->get()Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;

    move-result-object v0

    return-object v0
.end method
