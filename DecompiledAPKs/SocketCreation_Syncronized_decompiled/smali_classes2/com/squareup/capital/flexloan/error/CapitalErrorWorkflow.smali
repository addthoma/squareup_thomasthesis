.class public interface abstract Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;
.super Ljava/lang/Object;
.source "CapitalErrorWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/Workflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/Workflow<",
        "Lcom/squareup/capital/flexloan/FailureData;",
        "Lkotlin/Unit;",
        "Lcom/squareup/capital/flexloan/error/CapitalErrorScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;",
        "Lcom/squareup/workflow/Workflow;",
        "Lcom/squareup/capital/flexloan/FailureData;",
        "",
        "Lcom/squareup/capital/flexloan/error/CapitalErrorScreen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
