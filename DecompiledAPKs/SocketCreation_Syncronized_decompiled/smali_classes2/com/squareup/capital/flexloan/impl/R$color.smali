.class public final Lcom/squareup/capital/flexloan/impl/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/capital/flexloan/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final capital_hero_background:I = 0x7f060049

.field public static final capital_hero_content_color:I = 0x7f06004a

.field public static final capital_no_offer_body_text_color:I = 0x7f06004b

.field public static final capital_no_offer_icon_color:I = 0x7f06004c

.field public static final capital_plan_minimum_due_bullet:I = 0x7f06004d

.field public static final capital_plan_outstanding_due_bullet:I = 0x7f06004e

.field public static final capital_plan_overdue_due_bullet:I = 0x7f06004f

.field public static final capital_plan_overdue_text:I = 0x7f060050

.field public static final capital_plan_standard_text:I = 0x7f060051

.field public static final capital_plan_total_paid_bullet:I = 0x7f060052


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
