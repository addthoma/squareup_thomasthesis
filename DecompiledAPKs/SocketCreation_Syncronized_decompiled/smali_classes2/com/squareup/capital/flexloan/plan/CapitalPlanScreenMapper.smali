.class public final Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;
.super Ljava/lang/Object;
.source "CapitalPlanScreenMapper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCapitalPlanScreenMapper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CapitalPlanScreenMapper.kt\ncom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper\n*L\n1#1,183:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\r\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u000e\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0010\u0007\n\u0002\u0008\r\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u0000 H2\u00020\u0001:\u0001HB)\u0008\u0007\u0012\u000e\u0008\u0001\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0010\u0010?\u001a\u00020@2\u0006\u0010A\u001a\u00020@H\u0002J\u0016\u0010B\u001a\u00020C2\u0006\u0010D\u001a\u00020\u000c2\u0006\u0010E\u001a\u00020\u0015J\u0010\u0010F\u001a\u0002022\u0006\u0010G\u001a\u000202H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\n\u001a\u00020\u000b*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u0018\u0010\u000f\u001a\u00020\u000b*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u000eR\u0018\u0010\u0011\u001a\u00020\u000b*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u000eR\u0018\u0010\u0013\u001a\u00020\u0014*\u00020\u00158BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0017R\u0018\u0010\u0018\u001a\u00020\u0019*\u00020\u00158BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u001bR\u0018\u0010\u001c\u001a\u00020\u0019*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001c\u0010\u001dR\u0018\u0010\u001e\u001a\u00020\u0014*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001f\u0010 R\u0018\u0010!\u001a\u00020\u0014*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\"\u0010 R\u0018\u0010#\u001a\u00020\u0014*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008$\u0010 R\u0018\u0010%\u001a\u00020\u0014*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008&\u0010 R\u0018\u0010\'\u001a\u00020(*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008)\u0010*R\u0018\u0010+\u001a\u00020(*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008,\u0010*R\u0018\u0010-\u001a\u00020\u0014*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008.\u0010 R\u0018\u0010/\u001a\u00020\u0014*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u00080\u0010 R\u0018\u00101\u001a\u000202*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u00083\u00104R\u0018\u00105\u001a\u000202*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u00086\u00104R\u0018\u00107\u001a\u000202*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u00088\u00104R\u0018\u00109\u001a\u00020\u0014*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008:\u0010 R\u0018\u0010;\u001a\u00020\u0014*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008<\u0010 R\u0018\u0010=\u001a\u000202*\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008>\u00104\u00a8\u0006I"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;",
        "",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "dateFormatter",
        "Ljava/text/DateFormat;",
        "res",
        "Landroid/content/res/Resources;",
        "(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Landroid/content/res/Resources;)V",
        "amountDue",
        "",
        "Lcom/squareup/protos/capital/servicing/plan/models/Plan;",
        "getAmountDue",
        "(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)J",
        "amountOutstanding",
        "getAmountOutstanding",
        "amountPaid",
        "getAmountPaid",
        "capitalButtonLinkText",
        "",
        "Lcom/squareup/capital/flexloan/CapitalFlexPlanType;",
        "getCapitalButtonLinkText",
        "(Lcom/squareup/capital/flexloan/CapitalFlexPlanType;)Ljava/lang/CharSequence;",
        "displayAmountDue",
        "",
        "getDisplayAmountDue",
        "(Lcom/squareup/capital/flexloan/CapitalFlexPlanType;)Z",
        "isPastDue",
        "(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Z",
        "minimumPaymentAmountText",
        "getMinimumPaymentAmountText",
        "(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;",
        "outstandingAmountText",
        "getOutstandingAmountText",
        "pastDueAmountText",
        "getPastDueAmountText",
        "paymentDueAmount",
        "getPaymentDueAmount",
        "paymentDueBulletColor",
        "",
        "getPaymentDueBulletColor",
        "(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)I",
        "paymentDueDateColor",
        "getPaymentDueDateColor",
        "paymentDueDateText",
        "getPaymentDueDateText",
        "paymentDueText",
        "getPaymentDueText",
        "percentDue",
        "",
        "getPercentDue",
        "(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)F",
        "percentOutstanding",
        "getPercentOutstanding",
        "percentPaid",
        "getPercentPaid",
        "percentPaidText",
        "getPercentPaidText",
        "totalPaid",
        "getTotalPaid",
        "totalPayment",
        "getTotalPayment",
        "getDate",
        "",
        "dateString",
        "mapPlanToScreenData",
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;",
        "plan",
        "flexPlanStatus",
        "roundPercentageDown",
        "fraction",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper$Companion;

.field private static final DUE_DATE:Ljava/lang/String; = "due_date"

.field private static final PERCENT_PAID:Ljava/lang/String; = "percent_paid"


# instance fields
.field private final dateFormatter:Ljava/text/DateFormat;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->Companion:Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Landroid/content/res/Resources;)V
    .locals 1
    .param p1    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/Shorter;
        .end annotation
    .end param
    .param p2    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumForm;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->dateFormatter:Ljava/text/DateFormat;

    iput-object p3, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->res:Landroid/content/res/Resources;

    return-void
.end method

.method private final getAmountDue(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)J
    .locals 2

    .line 154
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->isPastDue(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v0, "loan_portion_of_sales.past_due_money.amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    .line 157
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v0, "loan_portion_of_sales.pe\u2026od_remaining_money.amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method private final getAmountOutstanding(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)J
    .locals 2

    .line 160
    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v0, "loan_portion_of_sales.remaining_money.amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method private final getAmountPaid(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)J
    .locals 2

    .line 151
    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->paid_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v0, "loan_portion_of_sales.paid_money.amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method private final getCapitalButtonLinkText(Lcom/squareup/capital/flexloan/CapitalFlexPlanType;)Ljava/lang/CharSequence;
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/CapitalFlexPlanType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 57
    sget p1, Lcom/squareup/capital/flexloan/impl/R$string;->square_go_dashboard:I

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 56
    :cond_1
    sget p1, Lcom/squareup/capital/flexloan/impl/R$string;->capital_manage_plan:I

    .line 60
    :goto_0
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->res:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "res.getText(resourceString)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getDate(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 170
    invoke-static {p1}, Lcom/squareup/util/Times;->Iso8601ToNormalizedDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 172
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->dateFormatter:Ljava/text/DateFormat;

    invoke-virtual {p1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "dateFormatter.format(parseDate)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    return-object p1
.end method

.method private final getDisplayAmountDue(Lcom/squareup/capital/flexloan/CapitalFlexPlanType;)Z
    .locals 1

    .line 68
    sget-object v0, Lcom/squareup/capital/flexloan/CapitalFlexPlanType;->CURRENT_PLAN:Lcom/squareup/capital/flexloan/CapitalFlexPlanType;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final getMinimumPaymentAmountText(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "moneyFormatter.format(lo\u2026s.period_remaining_money)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getOutstandingAmountText(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "moneyFormatter.format(lo\u2026of_sales.remaining_money)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getPastDueAmountText(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "moneyFormatter.format(lo\u2026_of_sales.past_due_money)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getPaymentDueAmount(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;
    .locals 1

    .line 111
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->isPastDue(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getPastDueAmountText(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 114
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getMinimumPaymentAmountText(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final getPaymentDueBulletColor(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)I
    .locals 0

    .line 83
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->isPastDue(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 84
    sget p1, Lcom/squareup/capital/flexloan/impl/R$color;->capital_plan_overdue_due_bullet:I

    goto :goto_0

    .line 86
    :cond_0
    sget p1, Lcom/squareup/capital/flexloan/impl/R$color;->capital_plan_minimum_due_bullet:I

    :goto_0
    return p1
.end method

.method private final getPaymentDueDateColor(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)I
    .locals 0

    .line 91
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->isPastDue(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 92
    sget p1, Lcom/squareup/capital/flexloan/impl/R$color;->capital_plan_overdue_text:I

    goto :goto_0

    .line 94
    :cond_0
    sget p1, Lcom/squareup/capital/flexloan/impl/R$color;->capital_plan_standard_text:I

    :goto_0
    return p1
.end method

.method private final getPaymentDueDateText(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;
    .locals 3

    .line 99
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->isPastDue(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Z

    move-result v0

    const-string v1, "due_date"

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/capital/flexloan/impl/R$string;->square_capital_flex_plan_overdue_since:I

    invoke-static {v0, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 101
    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_started_at:Ljava/lang/String;

    const-string v2, "loan_portion_of_sales.past_due_started_at"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getDate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    goto :goto_0

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/capital/flexloan/impl/R$string;->square_capital_flex_plan_due_date:I

    invoke-static {v0, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 104
    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->payment_period_ended_at:Ljava/lang/String;

    const-string v2, "loan_portion_of_sales.payment_period_ended_at"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getDate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 107
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "resourceString.format()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getPaymentDueText(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;
    .locals 1

    .line 72
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->isPastDue(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 73
    sget p1, Lcom/squareup/capital/flexloan/impl/R$string;->capital_plan_screen_past_due:I

    goto :goto_0

    .line 75
    :cond_0
    sget p1, Lcom/squareup/capital/flexloan/impl/R$string;->capital_plan_screen_min_payment:I

    .line 78
    :goto_0
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->res:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "res.getString(resourceString)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method private final getPercentDue(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)F
    .locals 5

    .line 138
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getAmountOutstanding(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 141
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getAmountDue(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)J

    move-result-wide v0

    long-to-float v0, v0

    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getTotalPayment(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)F

    move-result p1

    div-float/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->roundPercentageDown(F)F

    move-result p1

    const/16 v0, 0x64

    int-to-float v0, v0

    mul-float p1, p1, v0

    :goto_0
    return p1
.end method

.method private final getPercentOutstanding(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)F
    .locals 5

    .line 147
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getAmountOutstanding(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)J

    move-result-wide v0

    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getAmountDue(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getAmountOutstanding(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)J

    move-result-wide v0

    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getAmountDue(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)J

    move-result-wide v2

    sub-long v2, v0, v2

    :goto_0
    long-to-float v0, v2

    .line 148
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getTotalPayment(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)F

    move-result p1

    div-float/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->roundPercentageDown(F)F

    move-result p1

    const/16 v0, 0x64

    int-to-float v0, v0

    mul-float p1, p1, v0

    return p1
.end method

.method private final getPercentPaid(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)F
    .locals 2

    .line 126
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getAmountPaid(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)J

    move-result-wide v0

    long-to-float v0, v0

    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getTotalPayment(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)F

    move-result p1

    div-float/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->roundPercentageDown(F)F

    move-result p1

    const/16 v0, 0x64

    int-to-float v0, v0

    mul-float p1, p1, v0

    return p1
.end method

.method private final getPercentPaidText(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/capital/flexloan/impl/R$string;->square_capital_flex_plan_percent_paid:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 131
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getPercentPaid(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)F

    move-result p1

    float-to-int p1, p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "percent_paid"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 133
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "phrase.format()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getTotalPaid(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->paid_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "moneyFormatter.format(lo\u2026tion_of_sales.paid_money)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getTotalPayment(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)F
    .locals 4

    .line 162
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getAmountOutstanding(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)J

    move-result-wide v0

    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getAmountPaid(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)J

    move-result-wide v2

    add-long/2addr v0, v2

    long-to-float p1, v0

    return p1
.end method

.method private final isPastDue(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Z
    .locals 4

    .line 66
    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final roundPercentageDown(F)F
    .locals 3

    const/16 v0, 0x3e8

    int-to-float v0, v0

    mul-float p1, p1, v0

    float-to-double v1, p1

    .line 166
    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-float p1, v1

    div-float/2addr p1, v0

    return p1
.end method


# virtual methods
.method public final mapPlanToScreenData(Lcom/squareup/protos/capital/servicing/plan/models/Plan;Lcom/squareup/capital/flexloan/CapitalFlexPlanType;)Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    const-string v2, "plan"

    move-object/from16 v3, p1

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "flexPlanStatus"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    new-instance v2, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;

    .line 36
    invoke-direct {v0, v1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getCapitalButtonLinkText(Lcom/squareup/capital/flexloan/CapitalFlexPlanType;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 37
    invoke-direct {v0, v1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getDisplayAmountDue(Lcom/squareup/capital/flexloan/CapitalFlexPlanType;)Z

    move-result v5

    .line 38
    invoke-direct/range {p0 .. p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->isPastDue(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Z

    move-result v6

    .line 39
    invoke-direct/range {p0 .. p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getOutstandingAmountText(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 40
    invoke-direct/range {p0 .. p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getPaymentDueAmount(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;

    move-result-object v8

    .line 41
    invoke-direct/range {p0 .. p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getPaymentDueBulletColor(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)I

    move-result v9

    .line 42
    invoke-direct/range {p0 .. p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getPaymentDueDateColor(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)I

    move-result v10

    .line 43
    invoke-direct/range {p0 .. p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getPaymentDueDateText(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;

    move-result-object v11

    .line 44
    invoke-direct/range {p0 .. p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getPaymentDueText(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;

    move-result-object v12

    .line 45
    invoke-direct/range {p0 .. p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getPercentDue(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)F

    move-result v13

    .line 46
    invoke-direct/range {p0 .. p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getPercentOutstanding(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)F

    move-result v14

    .line 47
    invoke-direct/range {p0 .. p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getPercentPaid(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)F

    move-result v15

    .line 48
    invoke-direct/range {p0 .. p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getPercentPaidText(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;

    move-result-object v16

    .line 49
    invoke-direct/range {p0 .. p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->getTotalPaid(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Ljava/lang/CharSequence;

    move-result-object v17

    move-object v3, v2

    .line 35
    invoke-direct/range {v3 .. v17}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;-><init>(Ljava/lang/CharSequence;ZZLjava/lang/CharSequence;Ljava/lang/CharSequence;IILjava/lang/CharSequence;Ljava/lang/CharSequence;FFFLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-object v2
.end method
