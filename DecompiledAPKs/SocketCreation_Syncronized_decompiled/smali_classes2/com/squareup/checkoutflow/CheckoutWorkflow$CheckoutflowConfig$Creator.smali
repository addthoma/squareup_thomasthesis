.class public final Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig$Creator;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Creator"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 4

    const-string v0, "in"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;

    const-class v1, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/squareup/tenderpayment/TenderPaymentConfig;

    const-class v2, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    const-class v3, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;-><init>(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/checkoutflow/services/NetworkRequestModifier;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 0

    new-array p1, p1, [Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;

    return-object p1
.end method
