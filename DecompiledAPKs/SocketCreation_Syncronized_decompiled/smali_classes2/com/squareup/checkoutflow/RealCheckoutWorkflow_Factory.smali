.class public final Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealCheckoutWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/RealCheckoutWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderPaymentV2Workflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderPaymentV2Workflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 45
    iput-object p7, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderPaymentV2Workflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;",
            ">;)",
            "Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;"
        }
    .end annotation

    .line 59
    new-instance v8, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Ldagger/Lazy;Lcom/squareup/ui/main/PosContainer;Ldagger/Lazy;Lcom/squareup/settings/server/Features;Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;)Lcom/squareup/checkoutflow/RealCheckoutWorkflow;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderPaymentV2Workflow;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;",
            "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;",
            "Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;",
            ")",
            "Lcom/squareup/checkoutflow/RealCheckoutWorkflow;"
        }
    .end annotation

    .line 66
    new-instance v8, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;-><init>(Ldagger/Lazy;Lcom/squareup/ui/main/PosContainer;Ldagger/Lazy;Lcom/squareup/settings/server/Features;Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/RealCheckoutWorkflow;
    .locals 8

    .line 50
    iget-object v0, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/main/PosContainer;

    iget-object v0, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;

    iget-object v0, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;

    iget-object v0, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    invoke-static/range {v1 .. v7}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;->newInstance(Ldagger/Lazy;Lcom/squareup/ui/main/PosContainer;Ldagger/Lazy;Lcom/squareup/settings/server/Features;Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;)Lcom/squareup/checkoutflow/RealCheckoutWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow_Factory;->get()Lcom/squareup/checkoutflow/RealCheckoutWorkflow;

    move-result-object v0

    return-object v0
.end method
