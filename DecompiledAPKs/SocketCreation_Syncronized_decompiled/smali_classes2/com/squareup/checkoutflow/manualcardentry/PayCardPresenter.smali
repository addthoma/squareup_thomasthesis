.class abstract Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;
.super Lcom/squareup/mortar/ContextPresenter;
.source "PayCardPresenter.java"

# interfaces
.implements Lcom/squareup/register/widgets/card/OnCardListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;",
        ">",
        "Lcom/squareup/mortar/ContextPresenter<",
        "TT;>;",
        "Lcom/squareup/register/widgets/card/OnCardListener;"
    }
.end annotation


# instance fields
.field protected final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field protected final features:Lcom/squareup/settings/server/Features;

.field protected final giftCards:Lcom/squareup/giftcard/GiftCards;

.field final invalidCardPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            ">;"
        }
    .end annotation
.end field

.field protected final res:Lcom/squareup/util/Res;

.field protected final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

.field private tender:Lcom/squareup/payment/tender/BaseTender$Builder;

.field protected final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field protected final tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

.field protected final thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;"
        }
    .end annotation
.end field

.field protected final transaction:Lcom/squareup/payment/Transaction;

.field protected final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/TenderScopeRunner;Lcom/squareup/payment/TenderInEdit;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/giftcard/GiftCards;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;)V"
        }
    .end annotation

    .line 69
    invoke-direct {p0}, Lcom/squareup/mortar/ContextPresenter;-><init>()V

    .line 56
    new-instance v0, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-direct {v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->invalidCardPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    const/4 v0, 0x0

    .line 63
    iput-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->tender:Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 70
    iput-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 71
    iput-object p2, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 72
    iput-object p3, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->res:Lcom/squareup/util/Res;

    .line 73
    iput-object p4, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    .line 74
    iput-object p9, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    .line 75
    iput-object p5, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 76
    iput-object p6, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->giftCards:Lcom/squareup/giftcard/GiftCards;

    .line 77
    iput-object p7, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 78
    iput-object p8, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->features:Lcom/squareup/settings/server/Features;

    .line 79
    iput-object p10, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 80
    iput-object p11, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private hidePanWarning()V
    .locals 1

    .line 183
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;->hidePanWarning()V

    :cond_0
    return-void
.end method

.method private showPanWarning(I)V
    .locals 1

    .line 189
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;

    invoke-interface {v0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;->showPanWarning(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected getAmountDue()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->tender:Lcom/squareup/payment/tender/BaseTender$Builder;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender$Builder;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method abstract getStrategy()Lcom/squareup/register/widgets/card/PanValidationStrategy;
.end method

.method public giftCardOnFileSelected()V
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->tender:Lcom/squareup/payment/tender/BaseTender$Builder;

    if-eqz v0, :cond_0

    .line 173
    iget-object v1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v1, v0}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-interface {v0}, Lcom/squareup/tenderpayment/TenderScopeRunner;->onGiftCardOnFileSelected()V

    return-void
.end method

.method abstract isGiftCardPayment()Z
.end method

.method public isThirdPartyGiftCardFeatureEnabled()Z
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v0}, Lcom/squareup/giftcard/GiftCards;->isThirdPartyGiftCardFeatureEnabled()Z

    move-result v0

    return v0
.end method

.method public onCardChanged(Lcom/squareup/register/widgets/card/PartialCard;)V
    .locals 0

    return-void
.end method

.method public onCardInvalid(Lcom/squareup/Card$PanWarning;)V
    .locals 3

    if-nez p1, :cond_0

    .line 125
    invoke-direct {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->hidePanWarning()V

    return-void

    .line 129
    :cond_0
    sget-object v0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$1;->$SwitchMap$com$squareup$Card$PanWarning:[I

    invoke-virtual {p1}, Lcom/squareup/Card$PanWarning;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 134
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized PanWarning: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 135
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;

    invoke-interface {p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;->hidePanWarning()V

    goto :goto_0

    .line 131
    :cond_1
    sget p1, Lcom/squareup/checkoutflow/manualcardentry/impl/R$string;->invalid_16_digit_pan:I

    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->showPanWarning(I)V

    :goto_0
    return-void
.end method

.method public onCardValid(Lcom/squareup/Card;)V
    .locals 0

    .line 141
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;

    invoke-interface {p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;->hidePanWarning()V

    :cond_0
    return-void
.end method

.method public onChargeCard(Lcom/squareup/Card;)V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->tenderKeyedCard(Lcom/squareup/protos/common/Money;Lcom/squareup/Card;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->setCard(Lcom/squareup/Card;)V

    .line 149
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-virtual {p1}, Lcom/squareup/ui/payment/SwipeHandler;->readyToAuthorize()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 150
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->cancelPaymentsOnNonActiveCardReaders()V

    .line 151
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-virtual {p1}, Lcom/squareup/ui/payment/SwipeHandler;->authorize()V

    :cond_1
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 84
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/UserSettings;->getCountryCodeOrNull()Lcom/squareup/CountryCode;

    move-result-object p1

    .line 85
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getCollectCnpPostalCode()Z

    move-result v0

    .line 86
    iget-object v1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v1}, Lcom/squareup/giftcard/GiftCards;->isThirdPartyGiftCardFeatureEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->isGiftCardPayment()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;

    iget-object v2, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/register/widgets/card/PanValidationStrategy;

    invoke-interface {v1, p1, v0, v2}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;->initCardEditor(Lcom/squareup/CountryCode;ZLcom/squareup/register/widgets/card/PanValidationStrategy;)V

    goto :goto_0

    .line 89
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;

    new-instance v2, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    invoke-direct {v2}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;-><init>()V

    invoke-interface {v1, p1, v0, v2}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;->initCardEditor(Lcom/squareup/CountryCode;ZLcom/squareup/register/widgets/card/PanValidationStrategy;)V

    :goto_0
    return-void
.end method

.method public onPanValid(Lcom/squareup/Card;Z)Z
    .locals 1

    .line 109
    iget-object p2, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->isGiftCardPayment()Z

    move-result v0

    invoke-virtual {p2, p1, v0}, Lcom/squareup/giftcard/GiftCards;->getCardTypeWarning(Lcom/squareup/Card;Z)Lcom/squareup/widgets/warning/Warning;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 111
    iget-object p2, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->invalidCardPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {p2, p1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 112
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;

    invoke-interface {p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter$PayCardView;->clearCard()V

    :cond_0
    const/4 p1, 0x1

    return p1

    .line 118
    :cond_1
    invoke-direct {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->hidePanWarning()V

    const/4 p1, 0x0

    return p1
.end method

.method public promptForPayment()V
    .locals 2

    .line 164
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->tender:Lcom/squareup/payment/tender/BaseTender$Builder;

    if-eqz v0, :cond_0

    .line 165
    iget-object v1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v1, v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->promptForPayment(Lcom/squareup/payment/tender/BaseTender$Builder;)Z

    goto :goto_0

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->promptForPayment()Z

    :goto_0
    return-void
.end method

.method protected x2ClearTender()V
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->badIsHodorCheck()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearBaseTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->tender:Lcom/squareup/payment/tender/BaseTender$Builder;

    :cond_0
    return-void
.end method
