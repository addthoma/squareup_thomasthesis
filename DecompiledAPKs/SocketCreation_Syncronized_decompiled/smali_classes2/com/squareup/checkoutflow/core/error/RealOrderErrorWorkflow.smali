.class public final Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealOrderErrorWorkflow.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;",
        "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState;",
        "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderErrorWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderErrorWorkflow.kt\ncom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,79:1\n32#2,12:80\n149#3,5:92\n149#3,5:97\n*E\n*S KotlinDebug\n*F\n+ 1 RealOrderErrorWorkflow.kt\ncom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow\n*L\n35#1,12:80\n49#1,5:92\n74#1,5:97\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002B\u000f\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ:\u0010\u000e\u001a\u0018\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u0008j\u0008\u0012\u0004\u0012\u00020\u000f`\u00112\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00132\u0006\u0010\u0014\u001a\u00020\u0003H\u0002J\u001a\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u00032\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0016JN\u0010\u0018\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0019\u001a\u00020\u00042\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0013H\u0016J\u0010\u0010\u001a\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u0004H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow;",
        "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;",
        "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState;",
        "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "permissionWorkflow",
        "Lcom/squareup/checkoutflow/core/error/OrderPermissionWorkflow;",
        "(Lcom/squareup/checkoutflow/core/error/OrderPermissionWorkflow;)V",
        "createErrorScreen",
        "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "props",
        "initialState",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final permissionWorkflow:Lcom/squareup/checkoutflow/core/error/OrderPermissionWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/core/error/OrderPermissionWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "permissionWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow;->permissionWorkflow:Lcom/squareup/checkoutflow/core/error/OrderPermissionWorkflow;

    return-void
.end method

.method private final createErrorScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;)Lcom/squareup/workflow/legacy/Screen;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState;",
            "-",
            "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowOutput;",
            ">;",
            "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 69
    new-instance v6, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowScreen;

    .line 70
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;->getAmount()Ljava/lang/String;

    move-result-object v1

    .line 71
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;->getAmountWithTip()Ljava/lang/String;

    move-result-object v2

    .line 72
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 73
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;->getMessage()Ljava/lang/String;

    move-result-object v4

    .line 74
    new-instance p2, Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow$createErrorScreen$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow$createErrorScreen$1;-><init>(Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow;Lcom/squareup/workflow/RenderContext;)V

    move-object v5, p2

    check-cast v5, Lkotlin/jvm/functions/Function0;

    move-object v0, v6

    .line 69
    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    check-cast v6, Lcom/squareup/workflow/legacy/V2Screen;

    .line 98
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 99
    const-class p2, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v0, ""

    invoke-static {p2, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 100
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 98
    invoke-direct {p1, p2, v6, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 80
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 85
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 87
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 88
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 89
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 91
    :cond_3
    check-cast v1, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 35
    :cond_4
    sget-object p1, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState$DisplayError;->INSTANCE:Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState$DisplayError;

    move-object v1, p1

    check-cast v1, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow;->initialState(Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;

    check-cast p2, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow;->render(Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;",
            "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState;",
            "-",
            "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    instance-of v0, p2, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState$DisplayError;

    if-eqz v0, :cond_0

    invoke-direct {p0, p3, p1}, Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow;->createErrorScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 44
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 45
    :cond_0
    instance-of p1, p2, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState$CancelDialog;

    if-eqz p1, :cond_1

    .line 46
    new-instance p1, Lcom/squareup/checkoutflow/core/error/CancelPaymentDialogScreen;

    .line 47
    new-instance p2, Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow$render$1;

    invoke-direct {p2, p0, p3}, Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow$render$1;-><init>(Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 48
    new-instance v0, Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow$render$2;

    invoke-direct {v0, p0, p3}, Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow$render$2;-><init>(Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 46
    invoke-direct {p1, p2, v0}, Lcom/squareup/checkoutflow/core/error/CancelPaymentDialogScreen;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 93
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 94
    const-class p3, Lcom/squareup/checkoutflow/core/error/CancelPaymentDialogScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    const-string v0, ""

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 95
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 93
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 50
    sget-object p1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 52
    :cond_1
    instance-of p1, p2, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState$GetPermission;

    if-eqz p1, :cond_2

    .line 54
    iget-object p1, p0, Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow;->permissionWorkflow:Lcom/squareup/checkoutflow/core/error/OrderPermissionWorkflow;

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    sget-object v2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    const/4 v3, 0x0

    .line 55
    new-instance p1, Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow$render$3;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow$render$3;-><init>(Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 53
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/core/error/RealOrderErrorWorkflow;->snapshotState(Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
