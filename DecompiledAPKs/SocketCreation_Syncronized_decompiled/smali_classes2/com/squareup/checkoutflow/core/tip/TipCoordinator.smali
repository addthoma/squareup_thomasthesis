.class public final Lcom/squareup/checkoutflow/core/tip/TipCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "TipCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/core/tip/TipCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTipCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TipCoordinator.kt\ncom/squareup/checkoutflow/core/tip/TipCoordinator\n*L\n1#1,194:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0086\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001*B;\u0008\u0002\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J>\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u00192\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u00140\u001c2\u000c\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u00192\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\"0!H\u0002J\u001e\u0010#\u001a\u0008\u0012\u0004\u0012\u00020$0\u00192\u0006\u0010\u0002\u001a\u00020\u00052\u0006\u0010%\u001a\u00020&H\u0002J\u0010\u0010\'\u001a\u00020(2\u0006\u0010\u0002\u001a\u00020\u0005H\u0003J \u0010)\u001a\u00020\u00142\u0006\u0010%\u001a\u00020&2\u0006\u0010\u0002\u001a\u00020\u00052\u0006\u0010\u0015\u001a\u00020\u0016H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/tip/TipCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screen",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/core/tip/TipScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;)V",
        "buyerActionBar",
        "Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;",
        "buyerActionContainer",
        "Lcom/squareup/ui/buyer/BuyerActionContainer;",
        "languageSelectionButton",
        "Lcom/squareup/marketfont/MarketTextView;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "createPrimaryActionOptions",
        "",
        "Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/checkoutflow/core/tip/TipScreen$Event;",
        "tipOptions",
        "Lcom/squareup/protos/common/tipping/TipOption;",
        "formattedTipOptions",
        "",
        "Lcom/squareup/tipping/FormattedTipOption;",
        "createSecondaryActionOptions",
        "Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;",
        "localeOverrideFactory",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "getCallToActionStringId",
        "",
        "update",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private buyerActionContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

.field private final screen:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ")V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->screen:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p3, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;)V

    return-void
.end method

.method public static final synthetic access$getBuyerActionBar$p(Lcom/squareup/checkoutflow/core/tip/TipCoordinator;)Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;
    .locals 1

    .line 38
    iget-object p0, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p0, :cond_0

    const-string v0, "buyerActionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getBuyerActionContainer$p(Lcom/squareup/checkoutflow/core/tip/TipCoordinator;)Lcom/squareup/ui/buyer/BuyerActionContainer;
    .locals 1

    .line 38
    iget-object p0, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->buyerActionContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

    if-nez p0, :cond_0

    const-string v0, "buyerActionContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setBuyerActionBar$p(Lcom/squareup/checkoutflow/core/tip/TipCoordinator;Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    return-void
.end method

.method public static final synthetic access$setBuyerActionContainer$p(Lcom/squareup/checkoutflow/core/tip/TipCoordinator;Lcom/squareup/ui/buyer/BuyerActionContainer;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->buyerActionContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/checkoutflow/core/tip/TipCoordinator;Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/core/tip/TipScreen;Landroid/view/View;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->update(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/core/tip/TipScreen;Landroid/view/View;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 189
    sget v0, Lcom/squareup/checkoutflow/core/tip/impl/R$id;->buyer_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.buyer_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 190
    sget v0, Lcom/squareup/checkoutflow/core/tip/impl/R$id;->noho_buyer_action_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.n\u2026o_buyer_action_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/buyer/BuyerActionContainer;

    iput-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->buyerActionContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

    .line 191
    sget v0, Lcom/squareup/checkoutflow/core/tip/impl/R$id;->switch_language_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.switch_language_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method private final createPrimaryActionOptions(Lkotlin/jvm/functions/Function1;Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/checkoutflow/core/tip/TipScreen$Event;",
            "Lkotlin/Unit;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/tipping/FormattedTipOption;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    .line 125
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 126
    move-object v3, v0

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_3

    .line 128
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/tipping/FormattedTipOption;

    iget-object v6, v6, Lcom/squareup/tipping/FormattedTipOption;->bottomLine:Ljava/lang/CharSequence;

    if-eqz v6, :cond_1

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-nez v6, :cond_0

    goto :goto_1

    :cond_0
    const/4 v6, 0x0

    goto :goto_2

    :cond_1
    :goto_1
    const/4 v6, 0x1

    :goto_2
    if-eqz v6, :cond_2

    const/4 v6, 0x0

    goto :goto_3

    :cond_2
    new-instance v6, Lcom/squareup/util/ViewString$TextString;

    .line 129
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/tipping/FormattedTipOption;

    iget-object v7, v7, Lcom/squareup/tipping/FormattedTipOption;->bottomLine:Ljava/lang/CharSequence;

    const-string v8, "formattedTipOptions[i].bottomLine"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    invoke-direct {v6, v7}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    .line 132
    :goto_3
    new-instance v14, Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;

    .line 133
    new-instance v7, Lcom/squareup/util/ViewString$TextString;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/tipping/FormattedTipOption;

    iget-object v8, v8, Lcom/squareup/tipping/FormattedTipOption;->topLine:Ljava/lang/CharSequence;

    const-string v9, "formattedTipOptions[i].topLine"

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v7, v8}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    move-object v8, v7

    check-cast v8, Lcom/squareup/util/ViewString;

    new-instance v7, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createPrimaryActionOptions$1;

    move-object/from16 v15, p0

    move-object/from16 v13, p1

    invoke-direct {v7, v15, v13, v0, v5}, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createPrimaryActionOptions$1;-><init>(Lcom/squareup/checkoutflow/core/tip/TipCoordinator;Lkotlin/jvm/functions/Function1;Ljava/util/List;I)V

    move-object v9, v7

    check-cast v9, Lkotlin/jvm/functions/Function0;

    .line 142
    move-object v10, v6

    check-cast v10, Lcom/squareup/util/ViewString;

    const/4 v11, 0x0

    const/16 v12, 0x8

    const/4 v6, 0x0

    move-object v7, v14

    move-object v13, v6

    .line 132
    invoke-direct/range {v7 .. v13}, Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;-><init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;Lcom/squareup/util/ViewString;Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 131
    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    move-object/from16 v15, p0

    .line 146
    check-cast v2, Ljava/util/List;

    return-object v2
.end method

.method private final createSecondaryActionOptions(Lcom/squareup/checkoutflow/core/tip/TipScreen;Lcom/squareup/locale/LocaleOverrideFactory;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/core/tip/TipScreen;",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;",
            ">;"
        }
    .end annotation

    .line 153
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 156
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/tip/TipScreen;->getHasAutoGratuity()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    sget v1, Lcom/squareup/checkout/R$string;->buyer_tip_additional_tip:I

    .line 158
    sget v2, Lcom/squareup/checkout/R$string;->buyer_tip_no_additional_tip:I

    goto :goto_0

    .line 160
    :cond_0
    sget v1, Lcom/squareup/checkout/R$string;->buyer_tip_custom:I

    .line 161
    sget v2, Lcom/squareup/checkout/R$string;->buyer_tip_no_tip:I

    .line 164
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isUsingCustomTip()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 166
    new-instance v3, Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;

    .line 167
    new-instance v4, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v5

    invoke-interface {v5, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v4, v1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v4, Lcom/squareup/util/ViewString;

    .line 168
    new-instance v1, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createSecondaryActionOptions$1;

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createSecondaryActionOptions$1;-><init>(Lcom/squareup/checkoutflow/core/tip/TipScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 166
    invoke-direct {v3, v4, v1}, Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;-><init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    .line 165
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    :cond_1
    new-instance v1, Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;

    .line 175
    new-instance v3, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p2

    invoke-interface {p2, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-direct {v3, p2}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/squareup/util/ViewString;

    .line 176
    new-instance p2, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createSecondaryActionOptions$2;

    invoke-direct {p2, p0, p1}, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$createSecondaryActionOptions$2;-><init>(Lcom/squareup/checkoutflow/core/tip/TipCoordinator;Lcom/squareup/checkoutflow/core/tip/TipScreen;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 174
    invoke-direct {v1, v3, p2}, Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;-><init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    .line 173
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final getCallToActionStringId(Lcom/squareup/checkoutflow/core/tip/TipScreen;)I
    .locals 0

    .line 116
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/tip/TipScreen;->getHasAutoGratuity()Z

    move-result p1

    if-eqz p1, :cond_0

    sget p1, Lcom/squareup/checkoutflow/core/tip/impl/R$string;->auto_gratuity_add_a_tip:I

    goto :goto_0

    .line 117
    :cond_0
    sget p1, Lcom/squareup/checkout/R$string;->buyer_add_a_tip:I

    :goto_0
    return p1
.end method

.method private final update(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/core/tip/TipScreen;Landroid/view/View;)V
    .locals 5

    .line 74
    new-instance v0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$update$1;

    invoke-direct {v0, p2}, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$update$1;-><init>(Lcom/squareup/checkoutflow/core/tip/TipScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p3, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 78
    iget-object p3, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    const-string v0, "buyerActionBar"

    if-nez p3, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 79
    :cond_0
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 80
    new-instance v2, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$update$2;

    invoke-direct {v2, p0, p2}, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$update$2;-><init>(Lcom/squareup/checkoutflow/core/tip/TipCoordinator;Lcom/squareup/checkoutflow/core/tip/TipScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 78
    invoke-virtual {p3, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 84
    iget-object p3, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p3, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 85
    :cond_1
    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    .line 86
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/tip/TipScreen;->getTotalAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "localeOverrideFactory.mo\u2026ormat(screen.totalAmount)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v0, Lcom/squareup/util/ViewString;

    .line 84
    invoke-virtual {p3, v0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    .line 90
    iget-object p3, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->buyerActionContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

    const-string v0, "buyerActionContainer"

    if-nez p3, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 91
    :cond_2
    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v2

    invoke-direct {p0, p2}, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->getCallToActionStringId(Lcom/squareup/checkoutflow/core/tip/TipScreen;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v1, Lcom/squareup/util/ViewString;

    .line 90
    invoke-virtual {p3, v1}, Lcom/squareup/ui/buyer/BuyerActionContainer;->setCallToAction(Lcom/squareup/util/ViewString;)V

    .line 95
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/tip/TipScreen;->getTipOptions()Ljava/util/List;

    move-result-object p3

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v1

    .line 96
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getPercentageFormatter()Lcom/squareup/text/Formatter;

    move-result-object v2

    .line 97
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getShortMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v3

    .line 94
    invoke-static {p3, v1, v2, v3}, Lcom/squareup/tipping/TipOptions;->buildFormattedTipOptions(Ljava/util/List;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)Ljava/util/List;

    move-result-object p3

    .line 99
    iget-object v1, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->buyerActionContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

    if-nez v1, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v0, Lcom/squareup/ui/buyer/BuyerActionContainer$Config;

    .line 100
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/tip/TipScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object v2

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/tip/TipScreen;->getTipOptions()Ljava/util/List;

    move-result-object v3

    const-string v4, "formattedTipOptions"

    invoke-static {p3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2, v3, p3}, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->createPrimaryActionOptions(Lkotlin/jvm/functions/Function1;Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object p3

    .line 101
    invoke-direct {p0, p2, p1}, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->createSecondaryActionOptions(Lcom/squareup/checkoutflow/core/tip/TipScreen;Lcom/squareup/locale/LocaleOverrideFactory;)Ljava/util/List;

    move-result-object v2

    .line 99
    invoke-direct {v0, p3, v2}, Lcom/squareup/ui/buyer/BuyerActionContainer$Config;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v1, v0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->setConfig(Lcom/squareup/ui/buyer/BuyerActionContainer$Config;)V

    .line 104
    iget-object p3, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    const-string v0, "languageSelectionButton"

    if-nez p3, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast p3, Landroid/view/View;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/tip/TipScreen;->getShowLanguageSelection()Z

    move-result v1

    invoke-static {p3, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 105
    iget-object p3, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    if-nez p3, :cond_5

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocaleFormatter()Lcom/squareup/locale/LocaleFormatter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/locale/LocaleFormatter;->displayLanguage()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p3, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object p3, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    if-nez p3, :cond_6

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 107
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p1

    sget v1, Lcom/squareup/activity/R$drawable;->buyer_language_icon:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    const/4 v1, 0x0

    .line 106
    invoke-virtual {p3, p1, v1, v1, v1}, Lcom/squareup/marketfont/MarketTextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 111
    iget-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_7

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 112
    :cond_7
    new-instance p3, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$update$3;

    invoke-direct {p3, p2}, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$update$3;-><init>(Lcom/squareup/checkoutflow/core/tip/TipScreen;)V

    check-cast p3, Landroid/view/View$OnClickListener;

    invoke-static {p3}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    check-cast p2, Landroid/view/View$OnClickListener;

    .line 111
    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 62
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->bindViews(Landroid/view/View;)V

    .line 64
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->screen:Lio/reactivex/Observable;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 65
    new-instance v1, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$attach$1;-><init>(Lcom/squareup/checkoutflow/core/tip/TipCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 66
    iget-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Shown TipScreen"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method
