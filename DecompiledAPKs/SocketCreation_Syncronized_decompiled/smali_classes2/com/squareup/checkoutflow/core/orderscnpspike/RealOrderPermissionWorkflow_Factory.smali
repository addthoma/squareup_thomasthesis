.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealOrderPermissionWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissionworkflow/PermissionWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissionworkflow/PermissionWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissionworkflow/PermissionWorkflow;",
            ">;)",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow_Factory;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/permissionworkflow/PermissionWorkflow;)Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow;-><init>(Lcom/squareup/permissionworkflow/PermissionWorkflow;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissionworkflow/PermissionWorkflow;

    invoke-static {v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow_Factory;->newInstance(Lcom/squareup/permissionworkflow/PermissionWorkflow;)Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow_Factory;->get()Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow;

    move-result-object v0

    return-object v0
.end method
