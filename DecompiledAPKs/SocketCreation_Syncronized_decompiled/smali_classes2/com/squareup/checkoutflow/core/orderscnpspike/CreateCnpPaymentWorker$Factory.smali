.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;
.super Ljava/lang/Object;
.source "CreateCnpPaymentWorker.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J(\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000c\u001a\u00020\rR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;",
        "",
        "service",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;",
        "(Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;)V",
        "create",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;",
        "card",
        "Lcom/squareup/Card;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "tip",
        "uuid",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final service:Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "service"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;->service:Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;

    return-void
.end method


# virtual methods
.method public final create(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;)Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;
    .locals 8

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "uuid"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;

    .line 36
    iget-object v2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;->service:Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;

    const/4 v7, 0x0

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    .line 35
    invoke-direct/range {v1 .. v7}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;-><init>(Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
