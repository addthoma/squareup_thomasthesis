.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;
.super Ljava/lang/Object;
.source "CreateCnpPaymentWorker.kt"

# interfaces
.implements Lcom/squareup/workflow/Worker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/Worker<",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0012B1\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\u0008\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0014\u0010\r\u001a\u00020\u000e2\n\u0010\u000f\u001a\u0006\u0012\u0002\u0008\u00030\u0001H\u0016J\u000e\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0011H\u0016R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult;",
        "service",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;",
        "card",
        "Lcom/squareup/Card;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "tip",
        "uuid",
        "",
        "(Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;)V",
        "doesSameWorkAs",
        "",
        "otherWorker",
        "run",
        "Lkotlinx/coroutines/flow/Flow;",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amount:Lcom/squareup/protos/common/Money;

.field private final card:Lcom/squareup/Card;

.field private final service:Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;

.field private final tip:Lcom/squareup/protos/common/Money;

.field private final uuid:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;->service:Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;

    iput-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;->card:Lcom/squareup/Card;

    iput-object p3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;->amount:Lcom/squareup/protos/common/Money;

    iput-object p4, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;->tip:Lcom/squareup/protos/common/Money;

    iput-object p5, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;->uuid:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 13
    invoke-direct/range {p0 .. p5}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;-><init>(Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public doesSameWorkAs(Lcom/squareup/workflow/Worker;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Worker<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "otherWorker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    instance-of v0, p1, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;

    if-eqz v0, :cond_0

    .line 59
    check-cast p1, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;

    iget-object p1, p1, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;->uuid:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;->uuid:Ljava/lang/String;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public run()Lkotlinx/coroutines/flow/Flow;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/flow/Flow<",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;->service:Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;

    .line 48
    iget-object v1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;->card:Lcom/squareup/Card;

    .line 49
    iget-object v2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;->amount:Lcom/squareup/protos/common/Money;

    .line 50
    iget-object v3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;->tip:Lcom/squareup/protos/common/Money;

    .line 51
    iget-object v4, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;->uuid:Ljava/lang/String;

    .line 47
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;->createCnpPayment(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lio/reactivex/Single;->toFlowable()Lio/reactivex/Flowable;

    move-result-object v0

    const-string v1, "service\n        .createC\u2026  )\n        .toFlowable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    .line 54
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    return-object v0
.end method
