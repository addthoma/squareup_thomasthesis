.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking;
.super Ljava/lang/Object;
.source "OrdersCnpAuthNetworking.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/core/auth/AuthNetworking;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrdersCnpAuthNetworking.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrdersCnpAuthNetworking.kt\ncom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking\n+ 2 Transform.kt\nkotlinx/coroutines/flow/FlowKt__TransformKt\n+ 3 Emitters.kt\nkotlinx/coroutines/flow/FlowKt__EmittersKt\n+ 4 SafeCollector.common.kt\nkotlinx/coroutines/flow/internal/SafeCollector_commonKt\n*L\n1#1,49:1\n47#2:50\n49#2:54\n51#3:51\n56#3:53\n106#4:52\n*E\n*S KotlinDebug\n*F\n+ 1 OrdersCnpAuthNetworking.kt\ncom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking\n*L\n30#1:50\n30#1:54\n30#1:51\n30#1:53\n30#1:52\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0002J\u000e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u000fH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking;",
        "Lcom/squareup/checkoutflow/core/auth/AuthNetworking;",
        "card",
        "Lcom/squareup/Card;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "tip",
        "createCnpPaymentWorkerFactory",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;",
        "(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;)V",
        "createRejectedResult",
        "Lcom/squareup/checkoutflow/core/auth/AuthResult;",
        "rejected",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult$Rejected;",
        "run",
        "Lkotlinx/coroutines/flow/Flow;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amount:Lcom/squareup/protos/common/Money;

.field private final card:Lcom/squareup/Card;

.field private final createCnpPaymentWorkerFactory:Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;

.field private final tip:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;)V
    .locals 1

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "createCnpPaymentWorkerFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking;->card:Lcom/squareup/Card;

    iput-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking;->amount:Lcom/squareup/protos/common/Money;

    iput-object p3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking;->tip:Lcom/squareup/protos/common/Money;

    iput-object p4, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking;->createCnpPaymentWorkerFactory:Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;

    return-void
.end method

.method public static final synthetic access$createRejectedResult(Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking;Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult$Rejected;)Lcom/squareup/checkoutflow/core/auth/AuthResult;
    .locals 0

    .line 14
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking;->createRejectedResult(Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult$Rejected;)Lcom/squareup/checkoutflow/core/auth/AuthResult;

    move-result-object p0

    return-object p0
.end method

.method private final createRejectedResult(Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult$Rejected;)Lcom/squareup/checkoutflow/core/auth/AuthResult;
    .locals 4

    .line 40
    new-instance v0, Lcom/squareup/checkoutflow/core/auth/AuthResult$AuthRejected;

    .line 44
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult$Rejected;->getResponse()Lcom/squareup/checkoutfe/CheckoutResponse;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/checkoutfe/CheckoutResponse;->errors:Ljava/util/List;

    const-string v2, "rejected.response.errors"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkoutfe/LocalizedError;

    iget-object v1, v1, Lcom/squareup/checkoutfe/LocalizedError;->error:Lcom/squareup/protos/connect/v2/resources/Error;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/resources/Error;->category:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Error$Category;->name()Ljava/lang/String;

    move-result-object v1

    .line 45
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult$Rejected;->getResponse()Lcom/squareup/checkoutfe/CheckoutResponse;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/checkoutfe/CheckoutResponse;->errors:Ljava/util/List;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutfe/LocalizedError;

    iget-object p1, p1, Lcom/squareup/checkoutfe/LocalizedError;->error:Lcom/squareup/protos/connect/v2/resources/Error;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/resources/Error;->code:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Error$Code;->name()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 40
    invoke-direct {v0, v2, v3, v1, p1}, Lcom/squareup/checkoutflow/core/auth/AuthResult$AuthRejected;-><init>(ZZLjava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/checkoutflow/core/auth/AuthResult;

    return-object v0
.end method


# virtual methods
.method public doesSameWorkAs(Lcom/squareup/workflow/Worker;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Worker<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "otherWorker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-static {p0, p1}, Lcom/squareup/checkoutflow/core/auth/AuthNetworking$DefaultImpls;->doesSameWorkAs(Lcom/squareup/checkoutflow/core/auth/AuthNetworking;Lcom/squareup/workflow/Worker;)Z

    move-result p1

    return p1
.end method

.method public run()Lkotlinx/coroutines/flow/Flow;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/flow/Flow<",
            "Lcom/squareup/checkoutflow/core/auth/AuthResult;",
            ">;"
        }
    .end annotation

    .line 21
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking;->createCnpPaymentWorkerFactory:Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;

    .line 24
    iget-object v2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking;->card:Lcom/squareup/Card;

    .line 25
    iget-object v3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking;->amount:Lcom/squareup/protos/common/Money;

    .line 26
    iget-object v4, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking;->tip:Lcom/squareup/protos/common/Money;

    .line 27
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v5, "generatedUUID.toString()"

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;->create(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;)Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker;->run()Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 52
    new-instance v1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking$run$$inlined$map$1;

    invoke-direct {v1, v0, p0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking$run$$inlined$map$1;-><init>(Lkotlinx/coroutines/flow/Flow;Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking;)V

    check-cast v1, Lkotlinx/coroutines/flow/Flow;

    return-object v1
.end method
