.class public final Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;
.super Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;
.source "EmoneyPaymentProcessingState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WaitingForRetap"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B!\u0012\u000e\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007J\u0011\u0010\u000c\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\r\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\'\u0010\u000e\u001a\u00020\u00002\u0010\u0008\u0002\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001R\u0019\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
        "authResponse",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bills/AddTendersResponse;",
        "errorMessage",
        "Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;",
        "(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;)V",
        "getAuthResponse",
        "()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "getErrorMessage",
        "()Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final authResponse:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final errorMessage:Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;


# direct methods
.method public constructor <init>(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 52
    invoke-direct {p0, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->authResponse:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->errorMessage:Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 51
    check-cast p2, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;-><init>(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;ILjava/lang/Object;)Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->authResponse:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->errorMessage:Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->copy(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;)Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->authResponse:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    return-object v0
.end method

.method public final component2()Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->errorMessage:Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    return-object v0
.end method

.method public final copy(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;)Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;",
            ")",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;"
        }
    .end annotation

    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;

    invoke-direct {v0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;-><init>(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->authResponse:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    iget-object v1, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->authResponse:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->errorMessage:Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    iget-object p1, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->errorMessage:Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAuthResponse()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;"
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->authResponse:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    return-object v0
.end method

.method public final getErrorMessage()Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->errorMessage:Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->authResponse:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->errorMessage:Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WaitingForRetap(authResponse="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->authResponse:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", errorMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->errorMessage:Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
