.class final Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEmoneyPaymentProcessingWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->handleDisplayRequestForProcessing(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
        "+",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
        "result",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

.field final synthetic this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;->$input:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;)Lcom/squareup/workflow/WorkflowAction;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 611
    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->getDisplayRequest()Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    move-result-object v0

    .line 613
    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_CANCEL:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    if-ne v0, v1, :cond_0

    sget-object p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$CancelRequested;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$CancelRequested;

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    goto/16 :goto_1

    .line 614
    :cond_0
    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_RETAP_WAITING:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    new-instance p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;

    .line 615
    new-instance v1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ReaderMessage;

    invoke-direct {v1, v0}, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ReaderMessage;-><init>(Lcom/squareup/cardreader/lcr/CrsTmnMessage;)V

    check-cast v1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    .line 614
    invoke-direct {p1, v2, v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;-><init>(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;)V

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    goto/16 :goto_1

    .line 618
    :cond_1
    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_PROCESSING_TRANSACTION:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    if-eq v0, v1, :cond_6

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_ONLINE_PROCESSING:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    if-ne v0, v1, :cond_2

    goto/16 :goto_0

    .line 619
    :cond_2
    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_COMPLETE_NORMAL_SETTLE:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    const-string v3, "input.money.currency_code"

    if-ne v0, v1, :cond_3

    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;

    .line 620
    iget-object v4, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->getBalance()Ljava/lang/String;

    move-result-object p1

    iget-object v5, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;->$input:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

    invoke-virtual {v5}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v5

    iget-object v5, v5, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v5, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4, p1, v5}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$parseBalance(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    const/4 v3, 0x0

    const/4 v4, 0x2

    .line 619
    invoke-direct {v1, p1, v3, v4, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;-><init>(Lcom/squareup/protos/common/Money;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    goto/16 :goto_1

    .line 622
    :cond_3
    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_DIFFERENT_CARD:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    if-ne v0, v1, :cond_4

    .line 623
    new-instance p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;

    new-instance v1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ReaderMessage;

    invoke-direct {v1, v0}, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ReaderMessage;-><init>(Lcom/squareup/cardreader/lcr/CrsTmnMessage;)V

    check-cast v1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    invoke-direct {p1, v2, v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;-><init>(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;)V

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    goto/16 :goto_1

    .line 624
    :cond_4
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    invoke-static {v1, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$isTMNDisplayMessageRetryableError(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/cardreader/lcr/CrsTmnMessage;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 625
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    invoke-static {v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$getEmoneyAddTenderProcessor$p(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;)Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;->$input:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

    invoke-virtual {v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v2

    iget-object v4, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;->$input:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

    invoke-virtual {v4}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;->dropAndResetTender(Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/common/Money;)V

    .line 626
    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$ReceivedRetryableErrorMessage;

    .line 627
    new-instance v2, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ReaderMessage;

    invoke-direct {v2, v0}, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ReaderMessage;-><init>(Lcom/squareup/cardreader/lcr/CrsTmnMessage;)V

    check-cast v2, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    .line 628
    iget-object v4, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->getBalance()Ljava/lang/String;

    move-result-object p1

    iget-object v5, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;->$input:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

    invoke-virtual {v5}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v5

    iget-object v5, v5, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v5, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4, p1, v5}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$parseBalance(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 626
    invoke-direct {v1, v2, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$ReceivedRetryableErrorMessage;-><init>(Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;Lcom/squareup/protos/common/Money;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    goto :goto_1

    .line 633
    :cond_5
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    invoke-static {v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$getEmoneyAddTenderProcessor$p(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;)Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;->$input:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

    invoke-virtual {v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;->dropTender(Lcom/squareup/payment/BillPayment;)V

    .line 634
    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;

    .line 635
    sget-object v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$FatalPaymentError;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$FatalPaymentError;

    move-object v5, v2

    check-cast v5, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;

    .line 636
    sget-object v2, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$GenericMessage;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$GenericMessage;

    move-object v6, v2

    check-cast v6, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    .line 637
    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->getBalance()Ljava/lang/String;

    move-result-object p1

    iget-object v4, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;->$input:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

    invoke-virtual {v4}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, p1, v4}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$parseBalance(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x8

    const/4 v10, 0x0

    move-object v4, v1

    .line 634
    invoke-direct/range {v4 .. v10}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;Lcom/squareup/protos/common/Money;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    goto :goto_1

    .line 618
    :cond_6
    :goto_0
    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$NoAuthOrReaderResponse;

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$NoAuthOrReaderResponse;-><init>(Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    .line 642
    :goto_1
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    invoke-static {v1, p1, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/cardreader/lcr/CrsTmnMessage;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 123
    check-cast p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;->invoke(Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
