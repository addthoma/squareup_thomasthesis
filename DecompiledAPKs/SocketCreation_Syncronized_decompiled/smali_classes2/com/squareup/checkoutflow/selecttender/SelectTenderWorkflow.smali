.class public interface abstract Lcom/squareup/checkoutflow/selecttender/SelectTenderWorkflow;
.super Ljava/lang/Object;
.source "SelectTenderWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/Workflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/Workflow<",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderInput;",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderResult;",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002 \u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u00050\u0001\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderWorkflow;",
        "Lcom/squareup/workflow/Workflow;",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderInput;",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderResult;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
