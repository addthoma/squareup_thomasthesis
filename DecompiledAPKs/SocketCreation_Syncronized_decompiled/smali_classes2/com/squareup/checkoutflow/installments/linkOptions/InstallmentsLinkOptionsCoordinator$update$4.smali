.class public final Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$update$4;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "InstallmentsLinkOptionsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->update(Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$update$4",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $data:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$update$4;->$data:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$update$4;->$data:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->getOnTextMeSelected()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
