.class public final Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "InstallmentsLinkOptionsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0001\u001aB+\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0018\u0010\u0019\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\r\u001a\u00020\u0005H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;",
        "",
        "Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Lcom/squareup/util/Res;)V",
        "buyerActionBar",
        "Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;",
        "data",
        "scanCodeButton",
        "Lcom/squareup/noho/NohoButton;",
        "subtitle",
        "Lcom/squareup/marketfont/MarketTextView;",
        "textMeButton",
        "title",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "update",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private final data:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private scanCodeButton:Lcom/squareup/noho/NohoButton;

.field private subtitle:Lcom/squareup/marketfont/MarketTextView;

.field private textMeButton:Lcom/squareup/noho/NohoButton;

.field private title:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->res:Lcom/squareup/util/Res;

    .line 32
    sget-object p2, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$data$1;->INSTANCE:Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$data$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "screens.map { it.data }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->data:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->update(Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 77
    sget v0, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026in.R.id.buyer_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 78
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_link_options_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.i\u2026ments_link_options_title)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    .line 79
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_link_options_subtitle:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.i\u2026ts_link_options_subtitle)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->subtitle:Lcom/squareup/marketfont/MarketTextView;

    .line 80
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_scan_code_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.i\u2026llments_scan_code_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->scanCodeButton:Lcom/squareup/noho/NohoButton;

    .line 81
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_text_me_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.i\u2026tallments_text_me_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->textMeButton:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;)V
    .locals 3

    .line 51
    new-instance v0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$update$1;

    invoke-direct {v0, p2}, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$update$1;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 54
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p1, :cond_0

    const-string v0, "buyerActionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v1, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$update$2;

    invoke-direct {v1, p2}, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$update$2;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 58
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_1

    const-string v0, "title"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_get_started:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->subtitle:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_2

    const-string v0, "subtitle"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_get_started_hint:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->scanCodeButton:Lcom/squareup/noho/NohoButton;

    const-string v0, "scanCodeButton"

    if-nez p1, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_scan_code:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->scanCodeButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    new-instance v0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$update$3;

    invoke-direct {v0, p2}, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$update$3;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->textMeButton:Lcom/squareup/noho/NohoButton;

    const-string v0, "textMeButton"

    if-nez p1, :cond_5

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_text_me:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->textMeButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_6

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    new-instance v0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$update$4;

    invoke-direct {v0, p2}, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$update$4;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->bindViews(Landroid/view/View;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;->data:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$attach$1;-><init>(Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "data.subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
