.class public abstract Lcom/squareup/checkoutflow/installments/InstallmentsServiceReleaseModule;
.super Ljava/lang/Object;
.source "InstallmentsServiceReleaseModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/checkoutflow/installments/InstallmentsServiceCommonModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindInstallmentsService(Lcom/squareup/checkoutflow/installments/InstallmentsService;)Lcom/squareup/checkoutflow/installments/InstallmentsService;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
