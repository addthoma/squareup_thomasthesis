.class public final Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "InstallmentsSmsSentCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0001\u0017B+\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0018\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\r\u001a\u00020\u0005H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsSentScreenData;",
        "",
        "Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Lcom/squareup/util/Res;)V",
        "buyerActionBar",
        "Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;",
        "data",
        "hint",
        "Landroid/widget/TextView;",
        "title",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "update",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private final data:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsSentScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private hint:Landroid/widget/TextView;

.field private final res:Lcom/squareup/util/Res;

.field private title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;->res:Lcom/squareup/util/Res;

    .line 30
    sget-object p2, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$data$1;->INSTANCE:Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$data$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "screens.map { it.data }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;->data:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsSentScreenData;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;->update(Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsSentScreenData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 59
    sget v0, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026in.R.id.buyer_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 60
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_sms_sent_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.i\u2026tallments_sms_sent_title)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;->title:Landroid/widget/TextView;

    .line 61
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_sms_sent_hint:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.installments_sms_sent_hint)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;->hint:Landroid/widget/TextView;

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsSentScreenData;)V
    .locals 3

    .line 47
    sget-object v0, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$update$1;->INSTANCE:Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$update$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 48
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p1, :cond_0

    const-string v0, "buyerActionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 49
    :cond_0
    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->new_sale:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v0, Lcom/squareup/util/ViewString;

    .line 50
    new-instance v1, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$update$2;

    invoke-direct {v1, p2}, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$update$2;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsSentScreenData;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 48
    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setUpLeftButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    .line 54
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;->title:Landroid/widget/TextView;

    if-nez p1, :cond_1

    const-string p2, "title"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object p2, p0, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_sent_title:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;->hint:Landroid/widget/TextView;

    if-nez p1, :cond_2

    const-string p2, "hint"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object p2, p0, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_sent_hint:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;->bindViews(Landroid/view/View;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;->data:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$attach$1;-><init>(Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "data.subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
