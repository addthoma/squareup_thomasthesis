.class public final Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask_MembersInjector;
.super Ljava/lang/Object;
.source "InstallmentsSmsTask_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final installmentsServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask_MembersInjector;->installmentsServiceProvider:Ljavax/inject/Provider;

    .line 22
    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;",
            ">;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAnalytics(Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;Lcom/squareup/analytics/Analytics;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static injectInstallmentsService(Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;Lcom/squareup/checkoutflow/installments/InstallmentsService;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->installmentsService:Lcom/squareup/checkoutflow/installments/InstallmentsService;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;)V
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask_MembersInjector;->installmentsServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsService;

    invoke-static {p1, v0}, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask_MembersInjector;->injectInstallmentsService(Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;Lcom/squareup/checkoutflow/installments/InstallmentsService;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask_MembersInjector;->injectAnalytics(Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask_MembersInjector;->injectMembers(Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;)V

    return-void
.end method
