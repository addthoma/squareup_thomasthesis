.class public final Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealInstallmentsWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p5, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p6, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;"
        }
    .end annotation

    .line 49
    new-instance v7, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;Lcom/squareup/util/Res;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/checkoutflow/installments/InstallmentsService;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;)Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;
    .locals 8

    .line 54
    new-instance v7, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;Lcom/squareup/util/Res;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/checkoutflow/installments/InstallmentsService;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;
    .locals 7

    .line 42
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/checkoutflow/installments/InstallmentsService;

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/analytics/Analytics;

    invoke-static/range {v1 .. v6}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;->newInstance(Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;Lcom/squareup/util/Res;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/checkoutflow/installments/InstallmentsService;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;)Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow_Factory;->get()Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;

    move-result-object v0

    return-object v0
.end method
