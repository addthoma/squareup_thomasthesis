.class public final Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;
.super Ljava/lang/Object;
.source "ReceiptCompleteScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;,
        Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\'\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u0000 G2\u00020\u0001:\u0002FGBu\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0008\u0010\u000c\u001a\u0004\u0018\u00010\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u001a0\u0019\u00a2\u0006\u0002\u0010\u001bJ\u000b\u00103\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u00104\u001a\u00020\u0015H\u00c6\u0003J\t\u00105\u001a\u00020\u0017H\u00c6\u0003J\u0015\u00106\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u001a0\u0019H\u00c6\u0003J\t\u00107\u001a\u00020\u0005H\u00c6\u0003J\t\u00108\u001a\u00020\u0007H\u00c6\u0003J\t\u00109\u001a\u00020\tH\u00c6\u0003J\t\u0010:\u001a\u00020\u000bH\u00c6\u0003J\u000b\u0010;\u001a\u0004\u0018\u00010\rH\u00c6\u0003J\t\u0010<\u001a\u00020\u000fH\u00c6\u0003J\t\u0010=\u001a\u00020\u0011H\u00c6\u0003J\t\u0010>\u001a\u00020\u0013H\u00c6\u0003J\u0091\u0001\u0010?\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\r2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u00112\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u00132\u0008\u0008\u0002\u0010\u0014\u001a\u00020\u00152\u0008\u0008\u0002\u0010\u0016\u001a\u00020\u00172\u0014\u0008\u0002\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u001a0\u0019H\u00c6\u0001J\u0013\u0010@\u001a\u00020\u000b2\u0008\u0010A\u001a\u0004\u0018\u00010BH\u00d6\u0003J\t\u0010C\u001a\u00020DH\u00d6\u0001J\t\u0010E\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u0011\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001fR\u0011\u0010\u0016\u001a\u00020\u0017\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010!R\u0011\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010#R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010%R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010&R\u001d\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u001a0\u0019\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010(R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008)\u0010*R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008+\u0010,R\u0013\u0010\u000c\u001a\u0004\u0018\u00010\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008-\u0010.R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008/\u00100R\u0011\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00081\u00102\u00a8\u0006H"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "displayName",
        "",
        "paymentTypeInfo",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
        "receiptSelection",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;",
        "smsMarketingResult",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;",
        "isPaymentComplete",
        "",
        "requestCreator",
        "Lcom/squareup/picasso/RequestCreator;",
        "curatedImage",
        "Lcom/squareup/merchantimages/CuratedImage;",
        "buyerLanguageState",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;",
        "updateCustomerState",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;",
        "addCardState",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;",
        "clickAnywhereState",
        "Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;",
        "onNewSaleClicked",
        "Lkotlin/Function1;",
        "",
        "(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;ZLcom/squareup/picasso/RequestCreator;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;Lkotlin/jvm/functions/Function1;)V",
        "getAddCardState",
        "()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;",
        "getBuyerLanguageState",
        "()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;",
        "getClickAnywhereState",
        "()Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;",
        "getCuratedImage",
        "()Lcom/squareup/merchantimages/CuratedImage;",
        "getDisplayName",
        "()Ljava/lang/String;",
        "()Z",
        "getOnNewSaleClicked",
        "()Lkotlin/jvm/functions/Function1;",
        "getPaymentTypeInfo",
        "()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
        "getReceiptSelection",
        "()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;",
        "getRequestCreator",
        "()Lcom/squareup/picasso/RequestCreator;",
        "getSmsMarketingResult",
        "()Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;",
        "getUpdateCustomerState",
        "()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;",
        "component1",
        "component10",
        "component11",
        "component12",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "ClickAnywhereState",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;

.field public static final SHOWN:Ljava/lang/String; = "Shown ReceiptCompleteScreen"


# instance fields
.field private final addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

.field private final buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

.field private final clickAnywhereState:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;

.field private final curatedImage:Lcom/squareup/merchantimages/CuratedImage;

.field private final displayName:Ljava/lang/String;

.field private final isPaymentComplete:Z

.field private final onNewSaleClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

.field private final receiptSelection:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

.field private final requestCreator:Lcom/squareup/picasso/RequestCreator;

.field private final smsMarketingResult:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

.field private final updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->Companion:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$Companion;

    .line 38
    const-class v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;ZLcom/squareup/picasso/RequestCreator;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;",
            "Z",
            "Lcom/squareup/picasso/RequestCreator;",
            "Lcom/squareup/merchantimages/CuratedImage;",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;",
            "Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "paymentTypeInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "receiptSelection"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "smsMarketingResult"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "curatedImage"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLanguageState"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "updateCustomerState"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "addCardState"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clickAnywhereState"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onNewSaleClicked"

    invoke-static {p12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->displayName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->receiptSelection:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    iput-object p4, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->smsMarketingResult:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    iput-boolean p5, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->isPaymentComplete:Z

    iput-object p6, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    iput-object p7, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    iput-object p8, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    iput-object p9, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    iput-object p10, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    iput-object p11, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->clickAnywhereState:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;

    iput-object p12, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->onNewSaleClicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;ZLcom/squareup/picasso/RequestCreator;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;
    .locals 13

    move-object v0, p0

    move/from16 v1, p13

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->displayName:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->receiptSelection:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->smsMarketingResult:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->isPaymentComplete:Z

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-object v11, v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget-object v12, v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->clickAnywhereState:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;

    goto :goto_a

    :cond_a
    move-object/from16 v12, p11

    :goto_a
    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_b

    iget-object v1, v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->onNewSaleClicked:Lkotlin/jvm/functions/Function1;

    goto :goto_b

    :cond_b
    move-object/from16 v1, p12

    :goto_b
    move-object p1, v2

    move-object p2, v3

    move-object/from16 p3, v4

    move-object/from16 p4, v5

    move/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v11

    move-object/from16 p11, v12

    move-object/from16 p12, v1

    invoke-virtual/range {p0 .. p12}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->copy(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;ZLcom/squareup/picasso/RequestCreator;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;Lkotlin/jvm/functions/Function1;)Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;

    move-result-object v0

    return-object v0
.end method

.method public static final getKEY()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->Companion:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static final isReceiptCompleteScreen(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->Companion:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$Companion;->isReceiptCompleteScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public final component10()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    return-object v0
.end method

.method public final component11()Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->clickAnywhereState:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;

    return-object v0
.end method

.method public final component12()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->onNewSaleClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component2()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    return-object v0
.end method

.method public final component3()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->receiptSelection:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    return-object v0
.end method

.method public final component4()Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->smsMarketingResult:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->isPaymentComplete:Z

    return v0
.end method

.method public final component6()Lcom/squareup/picasso/RequestCreator;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    return-object v0
.end method

.method public final component7()Lcom/squareup/merchantimages/CuratedImage;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    return-object v0
.end method

.method public final component8()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    return-object v0
.end method

.method public final component9()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;ZLcom/squareup/picasso/RequestCreator;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;Lkotlin/jvm/functions/Function1;)Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;",
            "Z",
            "Lcom/squareup/picasso/RequestCreator;",
            "Lcom/squareup/merchantimages/CuratedImage;",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;",
            "Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;"
        }
    .end annotation

    const-string v0, "paymentTypeInfo"

    move-object/from16 v3, p2

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "receiptSelection"

    move-object/from16 v4, p3

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "smsMarketingResult"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "curatedImage"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLanguageState"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "updateCustomerState"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "addCardState"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clickAnywhereState"

    move-object/from16 v12, p11

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onNewSaleClicked"

    move-object/from16 v13, p12

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;

    move-object v1, v0

    move-object v2, p1

    move/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v13}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;-><init>(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;ZLcom/squareup/picasso/RequestCreator;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->displayName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->displayName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->receiptSelection:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->receiptSelection:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->smsMarketingResult:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->smsMarketingResult:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->isPaymentComplete:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->isPaymentComplete:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->clickAnywhereState:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->clickAnywhereState:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->onNewSaleClicked:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->onNewSaleClicked:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAddCardState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    return-object v0
.end method

.method public final getBuyerLanguageState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    return-object v0
.end method

.method public final getClickAnywhereState()Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->clickAnywhereState:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;

    return-object v0
.end method

.method public final getCuratedImage()Lcom/squareup/merchantimages/CuratedImage;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    return-object v0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getOnNewSaleClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->onNewSaleClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getPaymentTypeInfo()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    return-object v0
.end method

.method public final getReceiptSelection()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->receiptSelection:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    return-object v0
.end method

.method public final getRequestCreator()Lcom/squareup/picasso/RequestCreator;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    return-object v0
.end method

.method public final getSmsMarketingResult()Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->smsMarketingResult:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    return-object v0
.end method

.method public final getUpdateCustomerState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->displayName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->receiptSelection:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->smsMarketingResult:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->isPaymentComplete:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_7
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_8
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_9
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->clickAnywhereState:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_a
    const/4 v2, 0x0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->onNewSaleClicked:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_b
    add-int/2addr v0, v1

    return v0
.end method

.method public final isPaymentComplete()Z
    .locals 1

    .line 21
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->isPaymentComplete:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReceiptCompleteScreen(displayName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentTypeInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", receiptSelection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->receiptSelection:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", smsMarketingResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->smsMarketingResult:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isPaymentComplete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->isPaymentComplete:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", requestCreator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", curatedImage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", buyerLanguageState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", updateCustomerState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", addCardState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", clickAnywhereState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->clickAnywhereState:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onNewSaleClicked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->onNewSaleClicked:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
