.class public final Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1$1;
.super Ljava/lang/Object;
.source "ReceiptCompleteCoordinator.kt"

# interfaces
.implements Lcom/squareup/picasso/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1;->onMeasured(Landroid/view/View;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016\u00a8\u0006\u0005\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$1$1$onMeasured$1",
        "Lcom/squareup/picasso/Callback;",
        "onError",
        "",
        "onSuccess",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1$1;->this$0:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1;

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Failed to load curated image"

    .line 129
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onSuccess()V
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1$1;->this$0:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1;

    iget-object v0, v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1;->this$0:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;

    invoke-static {v0}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->access$getReceiptContent$p(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;)Landroid/view/View;

    move-result-object v0

    .line 120
    sget v1, Lcom/squareup/checkoutflow/receipt/impl/R$color;->buyer_flow_merchant_background_translucent:I

    .line 119
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 122
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1$1;->this$0:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1;

    iget-object v0, v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1;->this$0:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;

    invoke-static {v0}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->access$getBuyerActionBar$p(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;)Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_transparent:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setButtonBackground(I)V

    .line 123
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1$1;->this$0:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1;

    iget-object v0, v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1;->this$0:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;

    invoke-static {v0}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->access$getLanguageSelectionButton$p(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;)Lcom/squareup/marketfont/MarketTextView;

    move-result-object v0

    .line 124
    sget v1, Lcom/squareup/marin/R$color;->marin_transparent:I

    .line 123
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setBackgroundResource(I)V

    return-void
.end method
