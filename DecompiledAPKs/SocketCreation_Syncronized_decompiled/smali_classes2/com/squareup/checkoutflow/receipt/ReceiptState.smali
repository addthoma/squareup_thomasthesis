.class public final Lcom/squareup/checkoutflow/receipt/ReceiptState;
.super Ljava/lang/Object;
.source "ReceiptState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/ReceiptState$Creator;,
        Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;,
        Lcom/squareup/checkoutflow/receipt/ReceiptState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0087\u0008\u0018\u0000  2\u00020\u0001:\u0002 !B\u0019\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0011\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\u0019\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0013H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\"\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008\n\u0010\u000b\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
        "Landroid/os/Parcelable;",
        "receiptScreenState",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;",
        "requestCreator",
        "Lcom/squareup/picasso/RequestCreator;",
        "(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;Lcom/squareup/picasso/RequestCreator;)V",
        "(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;)V",
        "getReceiptScreenState",
        "()Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;",
        "requestCreator$annotations",
        "()V",
        "getRequestCreator",
        "()Lcom/squareup/picasso/RequestCreator;",
        "setRequestCreator",
        "(Lcom/squareup/picasso/RequestCreator;)V",
        "component1",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "Companion",
        "ReceiptScreenState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final Companion:Lcom/squareup/checkoutflow/receipt/ReceiptState$Companion;


# instance fields
.field private final receiptScreenState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

.field private requestCreator:Lcom/squareup/picasso/RequestCreator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/receipt/ReceiptState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkoutflow/receipt/ReceiptState;->Companion:Lcom/squareup/checkoutflow/receipt/ReceiptState$Companion;

    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptState$Creator;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/receipt/ReceiptState$Creator;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/receipt/ReceiptState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;)V
    .locals 1

    const-string v0, "receiptScreenState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptState;->receiptScreenState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;Lcom/squareup/picasso/RequestCreator;)V
    .locals 1

    const-string v0, "receiptScreenState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/receipt/ReceiptState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;)V

    .line 26
    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptState;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/receipt/ReceiptState;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;ILjava/lang/Object;)Lcom/squareup/checkoutflow/receipt/ReceiptState;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptState;->receiptScreenState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/receipt/ReceiptState;->copy(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;)Lcom/squareup/checkoutflow/receipt/ReceiptState;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic requestCreator$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final component1()Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptState;->receiptScreenState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    return-object v0
.end method

.method public final copy(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;)Lcom/squareup/checkoutflow/receipt/ReceiptState;
    .locals 1

    const-string v0, "receiptScreenState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptState;

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/receipt/ReceiptState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/receipt/ReceiptState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/receipt/ReceiptState;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptState;->receiptScreenState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    iget-object p1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptState;->receiptScreenState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getReceiptScreenState()Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptState;->receiptScreenState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    return-object v0
.end method

.method public final getRequestCreator()Lcom/squareup/picasso/RequestCreator;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptState;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptState;->receiptScreenState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final setRequestCreator(Lcom/squareup/picasso/RequestCreator;)V
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptState;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReceiptState(receiptScreenState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptState;->receiptScreenState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptState;->receiptScreenState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
