.class public final Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;
.super Ljava/lang/Object;
.source "ReceiptErrorDialogScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \u00172\u00020\u0001:\u0001\u0017B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\u0015\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J)\u0010\u000e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0014\u0008\u0002\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001R\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "previousInputState",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;",
        "onOkayClicked",
        "Lkotlin/Function1;",
        "",
        "(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;Lkotlin/jvm/functions/Function1;)V",
        "getOnOkayClicked",
        "()Lkotlin/jvm/functions/Function1;",
        "getPreviousInputState",
        "()Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# instance fields
.field private final onOkayClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final previousInputState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->Companion:Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen$Companion;

    .line 13
    const-class v0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "previousInputState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onOkayClicked"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->previousInputState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->onOkayClicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 7
    sget-object v0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->previousInputState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->onOkayClicked:Lkotlin/jvm/functions/Function1;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->copy(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;Lkotlin/jvm/functions/Function1;)Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;

    move-result-object p0

    return-object p0
.end method

.method public static final getKEY()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    sget-object v0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->Companion:Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->previousInputState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;

    return-object v0
.end method

.method public final component2()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->onOkayClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;Lkotlin/jvm/functions/Function1;)Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;"
        }
    .end annotation

    const-string v0, "previousInputState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onOkayClicked"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;

    invoke-direct {v0, p1, p2}, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->previousInputState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->previousInputState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->onOkayClicked:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->onOkayClicked:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getOnOkayClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->onOkayClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getPreviousInputState()Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->previousInputState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->previousInputState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->onOkayClicked:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReceiptErrorDialogScreen(previousInputState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->previousInputState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onOkayClicked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->onOkayClicked:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
