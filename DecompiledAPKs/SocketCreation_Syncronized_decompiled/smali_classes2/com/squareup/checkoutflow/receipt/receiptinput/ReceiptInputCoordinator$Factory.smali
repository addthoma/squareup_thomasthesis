.class public final Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;
.super Ljava/lang/Object;
.source "ReceiptInputCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B)\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ*\u0010\u000b\u001a\u00020\u000c2\"\u0010\r\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000fj\u0008\u0012\u0004\u0012\u00020\u0010`\u00120\u000eR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;",
        "",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "phoneNumberScrubber",
        "Lcom/squareup/text/InsertingScrubber;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "receiptTextHelper",
        "Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;",
        "(Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;)V",
        "create",
        "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

.field private final receiptTextHelper:Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method public constructor <init>(Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;)V
    .locals 1
    .param p2    # Lcom/squareup/text/InsertingScrubber;
        .annotation runtime Lcom/squareup/text/InsertingScrubber$PhoneNumber;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "buyerLocaleOverride"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneNumberScrubber"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tutorialCore"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "receiptTextHelper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object p4, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;->receiptTextHelper:Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;

    .line 61
    iget-object v3, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iget-object v4, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    iget-object v5, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v6, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;->receiptTextHelper:Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p1

    .line 60
    invoke-direct/range {v1 .. v7}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
