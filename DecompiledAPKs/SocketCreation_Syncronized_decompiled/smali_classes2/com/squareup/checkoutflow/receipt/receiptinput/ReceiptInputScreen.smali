.class public final Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;
.super Ljava/lang/Object;
.source "ReceiptInputScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;,
        Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0015\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u0000 )2\u00020\u0001:\u0002)*B9\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\tH\u00c6\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003J\t\u0010 \u001a\u00020\rH\u00c6\u0003JI\u0010!\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\rH\u00c6\u0001J\u0013\u0010\"\u001a\u00020#2\u0008\u0010$\u001a\u0004\u0018\u00010%H\u00d6\u0003J\t\u0010&\u001a\u00020\'H\u00d6\u0001J\t\u0010(\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "displayName",
        "",
        "paymentTypeInfo",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
        "screenState",
        "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "requestCreator",
        "Lcom/squareup/picasso/RequestCreator;",
        "curatedImage",
        "Lcom/squareup/merchantimages/CuratedImage;",
        "(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;Lcom/squareup/CountryCode;Lcom/squareup/picasso/RequestCreator;Lcom/squareup/merchantimages/CuratedImage;)V",
        "getCountryCode",
        "()Lcom/squareup/CountryCode;",
        "getCuratedImage",
        "()Lcom/squareup/merchantimages/CuratedImage;",
        "getDisplayName",
        "()Ljava/lang/String;",
        "getPaymentTypeInfo",
        "()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
        "getRequestCreator",
        "()Lcom/squareup/picasso/RequestCreator;",
        "getScreenState",
        "()Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "Companion",
        "ScreenState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;

.field public static final SHOWN:Ljava/lang/String; = "Shown ReceiptInputScreen"


# instance fields
.field private final countryCode:Lcom/squareup/CountryCode;

.field private final curatedImage:Lcom/squareup/merchantimages/CuratedImage;

.field private final displayName:Ljava/lang/String;

.field private final paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

.field private final requestCreator:Lcom/squareup/picasso/RequestCreator;

.field private final screenState:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->Companion:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$Companion;

    .line 37
    const-class v0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;Lcom/squareup/CountryCode;Lcom/squareup/picasso/RequestCreator;Lcom/squareup/merchantimages/CuratedImage;)V
    .locals 1

    const-string v0, "paymentTypeInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenState"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "curatedImage"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->displayName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->screenState:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;

    iput-object p4, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->countryCode:Lcom/squareup/CountryCode;

    iput-object p5, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    iput-object p6, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;Lcom/squareup/CountryCode;Lcom/squareup/picasso/RequestCreator;Lcom/squareup/merchantimages/CuratedImage;ILjava/lang/Object;)Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->displayName:Ljava/lang/String;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->screenState:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->countryCode:Lcom/squareup/CountryCode;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->copy(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;Lcom/squareup/CountryCode;Lcom/squareup/picasso/RequestCreator;Lcom/squareup/merchantimages/CuratedImage;)Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    return-object v0
.end method

.method public final component3()Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->screenState:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;

    return-object v0
.end method

.method public final component4()Lcom/squareup/CountryCode;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->countryCode:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public final component5()Lcom/squareup/picasso/RequestCreator;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    return-object v0
.end method

.method public final component6()Lcom/squareup/merchantimages/CuratedImage;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;Lcom/squareup/CountryCode;Lcom/squareup/picasso/RequestCreator;Lcom/squareup/merchantimages/CuratedImage;)Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;
    .locals 8

    const-string v0, "paymentTypeInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenState"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "curatedImage"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;-><init>(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;Lcom/squareup/CountryCode;Lcom/squareup/picasso/RequestCreator;Lcom/squareup/merchantimages/CuratedImage;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->displayName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->displayName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->screenState:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->screenState:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->countryCode:Lcom/squareup/CountryCode;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->countryCode:Lcom/squareup/CountryCode;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    iget-object p1, p1, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCountryCode()Lcom/squareup/CountryCode;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->countryCode:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public final getCuratedImage()Lcom/squareup/merchantimages/CuratedImage;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    return-object v0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getPaymentTypeInfo()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    return-object v0
.end method

.method public final getRequestCreator()Lcom/squareup/picasso/RequestCreator;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    return-object v0
.end method

.method public final getScreenState()Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->screenState:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->displayName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->screenState:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->countryCode:Lcom/squareup/CountryCode;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReceiptInputScreen(displayName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentTypeInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", screenState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->screenState:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", countryCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->countryCode:Lcom/squareup/CountryCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", requestCreator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->requestCreator:Lcom/squareup/picasso/RequestCreator;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", curatedImage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
