.class public final Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$updateBackground$$inlined$let$lambda$1;
.super Ljava/lang/Object;
.source "ReceiptInputCoordinator.kt"

# interfaces
.implements Lcom/squareup/util/OnMeasuredCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->updateBackground(Lcom/squareup/picasso/RequestCreator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000#\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0007H\u0016\u00a8\u0006\t\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$updateBackground$1$1",
        "Lcom/squareup/util/OnMeasuredCallback;",
        "onMeasured",
        "",
        "view",
        "Landroid/view/View;",
        "width",
        "",
        "height",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $it:Lcom/squareup/picasso/RequestCreator;

.field final synthetic this$0:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/picasso/RequestCreator;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$updateBackground$$inlined$let$lambda$1;->$it:Lcom/squareup/picasso/RequestCreator;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$updateBackground$$inlined$let$lambda$1;->this$0:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMeasured(Landroid/view/View;II)V
    .locals 0

    const-string/jumbo p2, "view"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$updateBackground$$inlined$let$lambda$1;->$it:Lcom/squareup/picasso/RequestCreator;

    iget-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$updateBackground$$inlined$let$lambda$1;->this$0:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;

    invoke-static {p2}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->access$getMerchantImage$p(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;)Landroid/widget/ImageView;

    move-result-object p2

    new-instance p3, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$updateBackground$$inlined$let$lambda$1$1;

    invoke-direct {p3, p0}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$updateBackground$$inlined$let$lambda$1$1;-><init>(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$updateBackground$$inlined$let$lambda$1;)V

    check-cast p3, Lcom/squareup/picasso/Callback;

    invoke-virtual {p1, p2, p3}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    return-void
.end method
