.class public final Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;
.super Ljava/lang/Object;
.source "PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/shared/i18n/Localizer;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final discountPercentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final longDateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final mediumDateFormatNoYearProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final mediumDateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final percentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final realCentsMoneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final shortDateFormatNoYearProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final shortDateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final shortMoneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final taxPercentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final timeFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final wholeNumberPercentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->resProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p2, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->shortDateFormatProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p3, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->shortDateFormatNoYearProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p4, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->mediumDateFormatProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p5, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->mediumDateFormatNoYearProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p6, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->longDateFormatProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p7, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->timeFormatProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p8, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->percentageFormatterProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p9, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->discountPercentageFormatterProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p10, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->wholeNumberPercentageFormatterProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p11, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->taxPercentageFormatterProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p12, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p13, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->shortMoneyFormatterProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p14, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->realCentsMoneyFormatterProvider:Ljavax/inject/Provider;

    .line 82
    iput-object p15, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->applicationProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;"
        }
    .end annotation

    .line 104
    new-instance v16, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v16
.end method

.method public static provideCatalogLocalizerFactory(Lcom/squareup/util/Res;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Landroid/app/Application;)Lcom/squareup/shared/i18n/Localizer;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Landroid/app/Application;",
            ")",
            "Lcom/squareup/shared/i18n/Localizer;"
        }
    .end annotation

    .line 115
    invoke-static/range {p0 .. p14}, Lcom/squareup/PosLoggedInComponent$Module;->provideCatalogLocalizerFactory(Lcom/squareup/util/Res;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Landroid/app/Application;)Lcom/squareup/shared/i18n/Localizer;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/i18n/Localizer;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/shared/i18n/Localizer;
    .locals 17

    move-object/from16 v0, p0

    .line 87
    iget-object v1, v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->shortDateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Ljava/text/DateFormat;

    iget-object v1, v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->shortDateFormatNoYearProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Ljava/text/DateFormat;

    iget-object v1, v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->mediumDateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Ljava/text/DateFormat;

    iget-object v1, v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->mediumDateFormatNoYearProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Ljava/text/DateFormat;

    iget-object v1, v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->longDateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Ljava/text/DateFormat;

    iget-object v1, v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->timeFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Ljava/text/DateFormat;

    iget-object v1, v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->percentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->discountPercentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->wholeNumberPercentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->taxPercentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->shortMoneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->realCentsMoneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Landroid/app/Application;

    invoke-static/range {v2 .. v16}, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->provideCatalogLocalizerFactory(Lcom/squareup/util/Res;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Landroid/app/Application;)Lcom/squareup/shared/i18n/Localizer;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/PosLoggedInComponent_Module_ProvideCatalogLocalizerFactoryFactory;->get()Lcom/squareup/shared/i18n/Localizer;

    move-result-object v0

    return-object v0
.end method
