.class public Lcom/squareup/cashmanagement/RealTaskQueuer;
.super Ljava/lang/Object;
.source "RealTaskQueuer.java"

# interfaces
.implements Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;


# instance fields
.field private final retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;


# direct methods
.method public constructor <init>(Lcom/squareup/queue/retrofit/RetrofitQueue;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/cashmanagement/RealTaskQueuer;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-void
.end method


# virtual methods
.method public cashDrawerShiftClose(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealTaskQueuer;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose;

    invoke-direct {v1, p1}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose;-><init>(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    return-void
.end method

.method public cashDrawerShiftCreate(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealTaskQueuer;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;

    invoke-direct {v1, p1}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;-><init>(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    return-void
.end method

.method public cashDrawerShiftEmail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealTaskQueuer;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/queue/cashmanagement/CashDrawerShiftEmail;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftEmail;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    return-void
.end method

.method public cashDrawerShiftEnd(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealTaskQueuer;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/queue/cashmanagement/CashDrawerShiftEnd;

    invoke-direct {v1, p1}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftEnd;-><init>(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    return-void
.end method

.method public cashDrawerShiftUpdate(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealTaskQueuer;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;

    invoke-direct {v1, p1}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;-><init>(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    return-void
.end method
