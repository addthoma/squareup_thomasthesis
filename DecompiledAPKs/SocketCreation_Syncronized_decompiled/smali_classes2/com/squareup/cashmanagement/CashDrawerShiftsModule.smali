.class public abstract Lcom/squareup/cashmanagement/CashDrawerShiftsModule;
.super Ljava/lang/Object;
.source "CashDrawerShiftsModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCashDrawerShiftStore(Landroid/app/Application;Ljava/io/File;)Lcom/squareup/cashmanagement/CashDrawerShiftStore;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;-><init>(Landroid/content/Context;Ljava/io/File;)V

    return-object v0
.end method

.method static provideCashDrawerShifts(Lcom/squareup/cashmanagement/RealTaskQueuer;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cashmanagement/CashDrawerShiftStore;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/server/cashmanagement/CashManagementService;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;)Lcom/squareup/cashmanagement/CashDrawerShiftManager;
    .locals 15
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cashmanagement/RealTaskQueuer;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftStore;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/server/cashmanagement/CashManagementService;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;",
            ")",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;"
        }
    .end annotation

    .line 33
    new-instance v14, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;-><init>(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cashmanagement/CashDrawerShiftStore;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/server/cashmanagement/CashManagementService;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;)V

    return-object v14
.end method


# virtual methods
.method abstract bindsCashDrawerShiftManagerForPayments(Lcom/squareup/cashmanagement/CashDrawerShiftManager;)Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
