.class public final Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftStoreFactory;
.super Ljava/lang/Object;
.source "CashDrawerShiftsModule_ProvideCashDrawerShiftStoreFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cashmanagement/CashDrawerShiftStore;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final userDirProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftStoreFactory;->contextProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftStoreFactory;->userDirProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftStoreFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;)",
            "Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftStoreFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftStoreFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftStoreFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCashDrawerShiftStore(Landroid/app/Application;Ljava/io/File;)Lcom/squareup/cashmanagement/CashDrawerShiftStore;
    .locals 0

    .line 41
    invoke-static {p0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftsModule;->provideCashDrawerShiftStore(Landroid/app/Application;Ljava/io/File;)Lcom/squareup/cashmanagement/CashDrawerShiftStore;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cashmanagement/CashDrawerShiftStore;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cashmanagement/CashDrawerShiftStore;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftStoreFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftStoreFactory;->userDirProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-static {v0, v1}, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftStoreFactory;->provideCashDrawerShiftStore(Landroid/app/Application;Ljava/io/File;)Lcom/squareup/cashmanagement/CashDrawerShiftStore;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftStoreFactory;->get()Lcom/squareup/cashmanagement/CashDrawerShiftStore;

    move-result-object v0

    return-object v0
.end method
