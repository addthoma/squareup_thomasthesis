.class public Lcom/squareup/cashmanagement/CashReportPayload;
.super Lcom/squareup/print/PrintablePayload;
.source "CashReportPayload.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;
    }
.end annotation


# instance fields
.field public final headerSection:Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;

.field public final paidInOutSection:Lcom/squareup/cashmanagement/PaidInOutSection;

.field public final summarySection:Lcom/squareup/cashmanagement/DrawerSummarySection;


# direct methods
.method public constructor <init>(Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;Lcom/squareup/cashmanagement/DrawerSummarySection;Lcom/squareup/cashmanagement/PaidInOutSection;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/print/PrintablePayload;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/cashmanagement/CashReportPayload;->headerSection:Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;

    .line 20
    iput-object p2, p0, Lcom/squareup/cashmanagement/CashReportPayload;->summarySection:Lcom/squareup/cashmanagement/DrawerSummarySection;

    .line 21
    iput-object p3, p0, Lcom/squareup/cashmanagement/CashReportPayload;->paidInOutSection:Lcom/squareup/cashmanagement/PaidInOutSection;

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterActionName;
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_CASH_REPORT_PAPER:Lcom/squareup/analytics/RegisterActionName;

    return-object v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;Z)Landroid/graphics/Bitmap;
    .locals 0

    .line 36
    new-instance p2, Lcom/squareup/cashmanagement/CashReportRenderer;

    invoke-direct {p2, p1}, Lcom/squareup/cashmanagement/CashReportRenderer;-><init>(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 37
    invoke-virtual {p2, p0}, Lcom/squareup/cashmanagement/CashReportRenderer;->renderBitmap(Lcom/squareup/cashmanagement/CashReportPayload;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method
