.class public Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;
.super Ljava/lang/Object;
.source "RealCashDrawerShiftManager.java"

# interfaces
.implements Lcom/squareup/cashmanagement/CashDrawerShiftManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;
    }
.end annotation


# static fields
.field private static final LOADED:Ljava/lang/String; = "cash-drawer-shift-loaded"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            ">;"
        }
    .end annotation
.end field

.field private final cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final currentCashDrawerShiftRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            ">;"
        }
    .end annotation
.end field

.field private currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final fileExecutor:Ljava/util/concurrent/Executor;

.field private final hasCashDrawerData:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private loaded:Z

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final printingDispatcher:Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;

.field private final queuer:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;

.field private final service:Lcom/squareup/server/cashmanagement/CashManagementService;

.field private final store:Lcom/squareup/cashmanagement/CashDrawerShiftStore;

.field private final userToken:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cashmanagement/CashDrawerShiftStore;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/server/cashmanagement/CashManagementService;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftStore;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/server/cashmanagement/CashManagementService;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;",
            ")V"
        }
    .end annotation

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentCashDrawerShiftRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v0, 0x0

    .line 89
    iput-boolean v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->loaded:Z

    .line 97
    iput-object p1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->queuer:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;

    .line 98
    iput-object p2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->fileExecutor:Ljava/util/concurrent/Executor;

    .line 99
    iput-object p3, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 100
    iput-object p4, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->store:Lcom/squareup/cashmanagement/CashDrawerShiftStore;

    .line 101
    iput-object p5, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->hasCashDrawerData:Lcom/f2prateek/rx/preferences2/Preference;

    .line 102
    iput-object p6, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->service:Lcom/squareup/server/cashmanagement/CashManagementService;

    .line 103
    iput-object p7, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->userToken:Ljava/lang/String;

    .line 104
    iput-object p8, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 105
    iput-object p9, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->bundleKey:Lcom/squareup/BundleKey;

    .line 106
    iput-object p10, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 107
    iput-object p11, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    .line 108
    iput-object p12, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->analytics:Lcom/squareup/analytics/Analytics;

    .line 109
    iput-object p13, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->printingDispatcher:Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;)Z
    .locals 0

    .line 51
    iget-boolean p0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->loaded:Z

    return p0
.end method

.method static synthetic access$002(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;Z)Z
    .locals 0

    .line 51
    iput-boolean p1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->loaded:Z

    return p1
.end method

.method static synthetic access$100(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;)Lcom/squareup/BundleKey;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->bundleKey:Lcom/squareup/BundleKey;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    return-object p0
.end method

.method static synthetic access$202(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    return-object p1
.end method

.method private addEmployeeToShiftAuditIfUnique(Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 456
    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 457
    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private emailReportIfEnabled(Ljava/lang/String;)V
    .locals 1

    .line 449
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->isEmailReportEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->getEmailRecipient()Ljava/lang/String;

    move-result-object v0

    .line 451
    invoke-virtual {p0, p1, v0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->emailCashDrawerShiftReport(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private endCurrentShift(Lcom/squareup/protos/common/Money;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V
    .locals 5

    .line 316
    invoke-direct {p0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->enforceMainThreadAndLoaded()V

    .line 317
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    if-eqz v0, :cond_4

    .line 321
    invoke-static {}, Lcom/squareup/util/ISO8601Dates;->now()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v0

    .line 322
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 323
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ended_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->ENDED:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    .line 324
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->state(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    if-eqz p1, :cond_0

    .line 327
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object v1

    .line 328
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->CLOSED:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    .line 329
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->state(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v0

    .line 333
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 334
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    if-eqz p1, :cond_1

    .line 336
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 338
    :cond_1
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->employee_id:Ljava/util/List;

    invoke-direct {p0, v1, v0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->addEmployeeToShiftAuditIfUnique(Ljava/util/List;Ljava/lang/String;)V

    .line 341
    :cond_2
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    invoke-virtual {v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object v0

    .line 343
    iget-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->description:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    .line 344
    iget-object v2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v3, Lcom/squareup/ui/report/drawer/CashManagementEndDrawerEvent;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v4}, Lcom/squareup/ui/report/drawer/CashManagementEndDrawerEvent;-><init>(ZZ)V

    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 346
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->fileExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$JkjbIw9LmdQ1HwrzCCA90OxDyMo;

    invoke-direct {v2, p0, v0}, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$JkjbIw9LmdQ1HwrzCCA90OxDyMo;-><init>(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 347
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->queuer:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;

    invoke-interface {v1, v0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;->cashDrawerShiftEnd(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    if-eqz p1, :cond_3

    .line 349
    iget-object p1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_DRAWER_CLOSED_FROM_STARTED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 350
    iget-object p1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->queuer:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;

    invoke-interface {p1, v0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;->cashDrawerShiftClose(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    :cond_3
    const/4 p1, 0x0

    .line 352
    iput-object p1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 353
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentCashDrawerShiftRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 354
    invoke-direct {p0, v0, p2}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->printReportIfEnabled(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V

    .line 355
    iget-object p1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_cash_drawer_shift_id:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->emailReportIfEnabled(Ljava/lang/String;)V

    return-void

    .line 318
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No current shift to update"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private enforceMainThreadAndLoaded()V
    .locals 2

    .line 491
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->isCashManagementEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 494
    :cond_0
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 495
    iget-boolean v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->loaded:Z

    if-eqz v0, :cond_1

    return-void

    .line 496
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Current shift not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic lambda$enableCashManagement$9(Lcom/squareup/cashmanagement/CashDrawerShiftsResult;)V
    .locals 0

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;Lcom/squareup/cashmanagement/CashDrawerShiftCursor;)V
    .locals 0

    .line 120
    invoke-static {p1}, Lcom/squareup/cashmanagement/CashDrawerShiftsResults;->of(Ljava/lang/Object;)Lcom/squareup/cashmanagement/CashDrawerShiftsResult;

    move-result-object p1

    invoke-interface {p0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;->call(Lcom/squareup/cashmanagement/CashDrawerShiftsResult;)V

    return-void
.end method

.method static synthetic lambda$null$11(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;Lretrofit/RetrofitError;)V
    .locals 0

    .line 474
    invoke-static {p1}, Lcom/squareup/cashmanagement/CashDrawerShiftsResults;->failure(Ljava/lang/Throwable;)Lcom/squareup/cashmanagement/CashDrawerShiftsResult;

    move-result-object p1

    invoke-interface {p0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;->call(Lcom/squareup/cashmanagement/CashDrawerShiftsResult;)V

    return-void
.end method

.method static synthetic lambda$null$13()Ljava/lang/Void;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic lambda$null$2(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 0

    .line 129
    invoke-static {p1}, Lcom/squareup/cashmanagement/CashDrawerShiftsResults;->of(Ljava/lang/Object;)Lcom/squareup/cashmanagement/CashDrawerShiftsResult;

    move-result-object p1

    invoke-interface {p0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;->call(Lcom/squareup/cashmanagement/CashDrawerShiftsResult;)V

    return-void
.end method

.method static synthetic lambda$printReportIfEnabled$10(Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;)I
    .locals 2

    .line 441
    iget-object p0, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p0, p0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 442
    iget-object p0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p0, p0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide p0

    .line 443
    invoke-static {p0, p1, v0, v1}, Ljava/lang/Long;->compare(JJ)I

    move-result p0

    return p0
.end method

.method private loadCurrentShiftInBackground(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cashmanagement/CashDrawerShiftsCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 501
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->store:Lcom/squareup/cashmanagement/CashDrawerShiftStore;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftStore;->getOpenCashDrawerShift()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object v0

    .line 502
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v2, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$3By8n2pHr4Tct2r3kAsQkdr0U6c;

    invoke-direct {v2, p0, v0, p1}, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$3By8n2pHr4Tct2r3kAsQkdr0U6c;-><init>(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V

    invoke-interface {v1, v2}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private loadShiftsFromServer(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cashmanagement/CashDrawerShiftsCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 462
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->fileExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$XnlhxSkta4P1ogDDLXVFj_sdwB0;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$XnlhxSkta4P1ogDDLXVFj_sdwB0;-><init>(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private printReportIfEnabled(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V
    .locals 5

    .line 426
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->isPrintReportEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 430
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 433
    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->events:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;

    .line 434
    iget-object v3, v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v4, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_IN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-eq v3, v4, :cond_2

    iget-object v3, v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v4, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_OUT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-ne v3, v4, :cond_1

    .line 435
    :cond_2
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 440
    :cond_3
    sget-object v1, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$noSv9E7WMST7iXtyQnolqCjU9Jw;->INSTANCE:Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$noSv9E7WMST7iXtyQnolqCjU9Jw;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 445
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->printingDispatcher:Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;

    invoke-virtual {v1, p1, v0, p2}, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;->print(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;Ljava/util/List;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V

    return-void
.end method

.method private thirtyDaysAgo()Ljava/util/Date;
    .locals 3

    .line 522
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x6

    const/16 v2, -0x1e

    .line 523
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 524
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method private updateShift()V
    .locals 3

    .line 510
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    if-eqz v0, :cond_0

    .line 514
    invoke-virtual {v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object v0

    .line 516
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->fileExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$F20gfeOHqpyodbmBnlQ7uRVxBQ4;

    invoke-direct {v2, p0, v0}, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$F20gfeOHqpyodbmBnlQ7uRVxBQ4;-><init>(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 517
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->queuer:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;

    invoke-interface {v1, v0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;->cashDrawerShiftUpdate(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    .line 518
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentCashDrawerShiftRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void

    .line 511
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No current shift to update"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public addPaidInOutEvent(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 211
    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->addPaymentEventToCurrentCashDrawerShift(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method addPaymentEventToCurrentCashDrawerShift(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .line 227
    invoke-direct {p0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->enforceMainThreadAndLoaded()V

    .line 228
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    if-eqz v0, :cond_3

    .line 232
    invoke-static {}, Lcom/squareup/util/ISO8601Dates;->now()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v0

    .line 233
    new-instance v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;-><init>()V

    .line 234
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->client_event_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;

    move-result-object v1

    .line 235
    invoke-virtual {v1, p2}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->event_type(Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;

    move-result-object v1

    .line 236
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->event_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;

    move-result-object v1

    .line 237
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->occurred_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;

    move-result-object v0

    .line 239
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v1}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v1

    .line 240
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 241
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;

    .line 242
    iget-object v2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    iget-object v2, v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->employee_id:Ljava/util/List;

    invoke-direct {p0, v2, v1}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->addEmployeeToShiftAuditIfUnique(Ljava/util/List;Ljava/lang/String;)V

    .line 245
    :cond_0
    iget-object v2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v2}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 247
    new-instance v3, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    iget-object v4, v2, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    iget-object v2, v2, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    invoke-direct {v3, v1, v4, v2}, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->employee(Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;

    .line 254
    :cond_1
    sget-object v1, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$2;->$SwitchMap$com$squareup$protos$client$cashdrawers$CashDrawerShiftEvent$Type:[I

    invoke-virtual {p2}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->ordinal()I

    move-result p2

    aget p2, v1, p2

    packed-switch p2, :pswitch_data_0

    goto/16 :goto_0

    .line 291
    :pswitch_0
    iget-object p2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    iget-object p3, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    .line 292
    invoke-static {p3, p1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p3

    .line 291
    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_out_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 293
    iget-object p2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    iget-object p3, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    .line 294
    invoke-static {p3, p1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 293
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto :goto_0

    .line 285
    :pswitch_1
    iget-object p2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    iget-object p3, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    .line 286
    invoke-static {p3, p1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p3

    .line 285
    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_in_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 287
    iget-object p2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    iget-object p3, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    .line 288
    invoke-static {p3, p1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 287
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto :goto_0

    .line 282
    :pswitch_2
    invoke-virtual {v0, p3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->client_refund_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;

    goto :goto_0

    .line 275
    :pswitch_3
    iget-object p2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    .line 276
    invoke-static {v1, p1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 275
    invoke-virtual {p2, v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_refunds_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 277
    iget-object p2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    .line 278
    invoke-static {v1, p1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 277
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 279
    invoke-virtual {v0, p3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->client_refund_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;

    goto :goto_0

    .line 272
    :pswitch_4
    invoke-virtual {v0, p3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->client_payment_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;

    goto :goto_0

    .line 263
    :pswitch_5
    iget-object p2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_payment_money:Lcom/squareup/protos/common/Money;

    .line 264
    invoke-static {v1, p1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 263
    invoke-virtual {p2, v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_payment_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 265
    iget-object p2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    .line 266
    invoke-static {v1, p1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 265
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 267
    invoke-virtual {v0, p3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->client_payment_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;

    goto :goto_0

    .line 256
    :pswitch_6
    iget-object p2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_payment_money:Lcom/squareup/protos/common/Money;

    .line 257
    invoke-static {v1, p1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 256
    invoke-virtual {p2, v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_payment_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 258
    iget-object p2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    .line 259
    invoke-static {v1, p1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 258
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 260
    invoke-virtual {v0, p3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->client_payment_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;

    .line 298
    :goto_0
    invoke-static {p4}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 299
    invoke-virtual {v0, p4}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->description(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;

    .line 302
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;

    move-result-object p1

    .line 303
    iget-object p2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    iget-object p2, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->events:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 304
    invoke-direct {p0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->updateShift()V

    return-void

    .line 229
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No current shift to update"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public addTenderWithId(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 216
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->addPaymentEventToCurrentCashDrawerShift(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public cashManagementEnabledAndIsOpenShift()Z
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->isCashManagementEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public cashManagementEnabledAndNeedsLoading()Z
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->isCashManagementEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->loaded:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public closeCashDrawerShift(Ljava/lang/String;Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 373
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 374
    new-instance v0, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$mSxvZ0pdnV2Bi4Rcf2Gi1sAy0qU;

    invoke-direct {v0, p0, p2}, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$mSxvZ0pdnV2Bi4Rcf2Gi1sAy0qU;-><init>(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;Lcom/squareup/protos/common/Money;)V

    invoke-virtual {p0, p1, v0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->getCashDrawerShift(Ljava/lang/String;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V

    return-void
.end method

.method public createNewOpenCashDrawerShift(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
    .locals 5

    .line 166
    invoke-direct {p0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->enforceMainThreadAndLoaded()V

    .line 167
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    if-nez v0, :cond_1

    .line 171
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 172
    invoke-static {}, Lcom/squareup/util/ISO8601Dates;->now()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v1

    .line 174
    new-instance v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;-><init>()V

    .line 175
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->userToken:Ljava/lang/String;

    .line 176
    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object v0

    .line 177
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opened_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object v0

    .line 178
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->starting_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object v0

    .line 179
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/protos/common/Money;

    const-wide/16 v1, 0x0

    .line 180
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v0, v3, v4}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_payment_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/protos/common/Money;

    .line 181
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v0, v3, v4}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_refunds_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/protos/common/Money;

    .line 182
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v0, v3, v4}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_in_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/protos/common/Money;

    .line 183
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_out_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 184
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->events(Ljava/util/List;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->OPEN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    .line 185
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->state(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 187
    iget-object p1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {p1}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object p1

    .line 188
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 190
    invoke-direct {p0, v0, p1}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->addEmployeeToShiftAuditIfUnique(Ljava/util/List;Ljava/lang/String;)V

    .line 191
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->employee_id(Ljava/util/List;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 192
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 195
    :cond_0
    iget-object p1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object p1

    .line 197
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_DRAWER_OPENED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 199
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->fileExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$bDztqYLQvOSj1LH5a8Hm917yprk;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$bDztqYLQvOSj1LH5a8Hm917yprk;-><init>(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 204
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->queuer:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;

    invoke-interface {v0, p1}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;->cashDrawerShiftCreate(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    .line 205
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentCashDrawerShiftRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-object p1

    .line 168
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "There is already a current shift"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public currentCashDrawerShiftOrNull()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            ">;"
        }
    .end annotation

    .line 553
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentCashDrawerShiftRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public emailCashDrawerShiftReport(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 400
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 401
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 402
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->queuer:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;

    iget-object v2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->userToken:Ljava/lang/String;

    invoke-interface {v1, v0, p1, v2, p2}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;->cashDrawerShiftEmail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public enableCashManagement(Z)V
    .locals 1

    .line 406
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    if-nez p1, :cond_1

    .line 408
    iget-object p1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    if-nez p1, :cond_0

    goto :goto_0

    .line 409
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Current shift must be closed before disabling cash management"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    const/4 p1, 0x1

    .line 415
    iput-boolean p1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->loaded:Z

    .line 416
    iget-object p1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->hasCashDrawerData:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {p1}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_2

    .line 417
    sget-object p1, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$zZ5ZiVd6wcMAl2mfXKswGSqw2NU;->INSTANCE:Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$zZ5ZiVd6wcMAl2mfXKswGSqw2NU;

    invoke-direct {p0, p1}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->loadShiftsFromServer(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public endAndCloseCurrentCashDrawerShift(Lcom/squareup/protos/common/Money;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V
    .locals 0

    .line 369
    invoke-direct {p0, p1, p2}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->endCurrentShift(Lcom/squareup/protos/common/Money;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V

    return-void
.end method

.method public endCurrentCashDrawerShift()V
    .locals 2

    .line 359
    new-instance v0, Lcom/squareup/cashmanagement/NoExtraCashManagementPermissionsHolder;

    invoke-direct {v0}, Lcom/squareup/cashmanagement/NoExtraCashManagementPermissionsHolder;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->endCurrentShift(Lcom/squareup/protos/common/Money;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V

    return-void
.end method

.method public endCurrentCashDrawerShift(Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V
    .locals 1

    const/4 v0, 0x0

    .line 364
    invoke-direct {p0, v0, p1}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->endCurrentShift(Lcom/squareup/protos/common/Money;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V

    return-void
.end method

.method public getBundler()Lmortar/bundler/Bundler;
    .locals 2

    .line 528
    new-instance v0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$1;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$1;-><init>(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;Ljava/lang/String;)V

    return-object v0
.end method

.method public getCashDrawerShift(Ljava/lang/String;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftsCallback<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            ">;)V"
        }
    .end annotation

    .line 126
    invoke-direct {p0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->enforceMainThreadAndLoaded()V

    .line 127
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->fileExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$oiv_fCcEFbvuKrs5V_j0CkY-cEE;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$oiv_fCcEFbvuKrs5V_j0CkY-cEE;-><init>(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;Ljava/lang/String;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getCashDrawerShifts(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftsCallback<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftCursor;",
            ">;)V"
        }
    .end annotation

    .line 115
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 116
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->fileExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$fE5hcxdLXDRhTLyj9Qx6ilVmK0M;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$fE5hcxdLXDRhTLyj9Qx6ilVmK0M;-><init>(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getCurrentCashDrawerShift()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
    .locals 1

    .line 148
    invoke-direct {p0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->enforceMainThreadAndLoaded()V

    .line 149
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getCurrentCashDrawerShiftId()Ljava/lang/String;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public synthetic lambda$closeCashDrawerShift$8$RealCashDrawerShiftManager(Lcom/squareup/protos/common/Money;Lcom/squareup/cashmanagement/CashDrawerShiftsResult;)V
    .locals 1

    .line 376
    invoke-interface {p2}, Lcom/squareup/cashmanagement/CashDrawerShiftsResult;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    invoke-virtual {p2}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->newBuilder()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object p2

    .line 377
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object p1

    .line 378
    invoke-static {}, Lcom/squareup/util/ISO8601Dates;->now()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->CLOSED:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    .line 379
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->state(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object p1

    .line 381
    iget-object p2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {p2}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object p2

    .line 382
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 383
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 384
    invoke-direct {p0, v0, p2}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->addEmployeeToShiftAuditIfUnique(Ljava/util/List;Ljava/lang/String;)V

    .line 385
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->employee_id(Ljava/util/List;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 386
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 389
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object p1

    .line 391
    iget-object p2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_DRAWER_CLOSED_FROM_ENDED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 393
    iget-object p2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->fileExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$aZY9bXq00hwR_5tY9nTOKzUd4x4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$aZY9bXq00hwR_5tY9nTOKzUd4x4;-><init>(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    invoke-interface {p2, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 395
    iget-object p2, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->queuer:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;

    invoke-interface {p2, p1}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;->cashDrawerShiftClose(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    return-void
.end method

.method public synthetic lambda$createNewOpenCashDrawerShift$5$RealCashDrawerShiftManager(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 2

    .line 200
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->store:Lcom/squareup/cashmanagement/CashDrawerShiftStore;

    invoke-interface {v0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftStore;->saveCashDrawerShift(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    .line 201
    iget-object p1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->store:Lcom/squareup/cashmanagement/CashDrawerShiftStore;

    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->CLOSED:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    invoke-direct {p0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->thirtyDaysAgo()Ljava/util/Date;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/squareup/cashmanagement/CashDrawerShiftStore;->dropCashDrawerShifts(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;Ljava/util/Date;)V

    return-void
.end method

.method public synthetic lambda$endCurrentShift$6$RealCashDrawerShiftManager(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 1

    .line 346
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->store:Lcom/squareup/cashmanagement/CashDrawerShiftStore;

    invoke-interface {v0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftStore;->saveCashDrawerShift(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    return-void
.end method

.method public synthetic lambda$getCashDrawerShift$3$RealCashDrawerShiftManager(Ljava/lang/String;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->store:Lcom/squareup/cashmanagement/CashDrawerShiftStore;

    invoke-interface {v0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftStore;->getCashDrawerShift(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object p1

    .line 129
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$bYOe2P06R-qEpijy--ZdIXR4Y_Y;

    invoke-direct {v1, p2, p1}, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$bYOe2P06R-qEpijy--ZdIXR4Y_Y;-><init>(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$getCashDrawerShifts$1$RealCashDrawerShiftManager(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->store:Lcom/squareup/cashmanagement/CashDrawerShiftStore;

    .line 118
    invoke-interface {v0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftStore;->getCashDrawerShifts(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;)Lcom/squareup/util/ReadOnlyCursorList;

    move-result-object p1

    check-cast p1, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    .line 119
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$BeKhW5RfpvsWHCJJm1jwMQjW2XE;

    invoke-direct {v1, p2, p1}, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$BeKhW5RfpvsWHCJJm1jwMQjW2XE;-><init>(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;Lcom/squareup/cashmanagement/CashDrawerShiftCursor;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$loadCurrentCashDrawerShift$4$RealCashDrawerShiftManager(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V
    .locals 0

    .line 159
    invoke-direct {p0, p1}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->loadCurrentShiftInBackground(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V

    return-void
.end method

.method public synthetic lambda$loadCurrentShiftInBackground$14$RealCashDrawerShiftManager(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 503
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->newBuilder()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    const/4 p1, 0x1

    .line 504
    iput-boolean p1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->loaded:Z

    .line 505
    sget-object p1, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$9ATe6kgJBltUtYYQFqqgDmtmnbY;->INSTANCE:Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$9ATe6kgJBltUtYYQFqqgDmtmnbY;

    invoke-interface {p2, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;->call(Lcom/squareup/cashmanagement/CashDrawerShiftsResult;)V

    return-void
.end method

.method public synthetic lambda$loadShiftsFromServer$12$RealCashDrawerShiftManager(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V
    .locals 3

    .line 463
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->userToken:Ljava/lang/String;

    .line 465
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    move-result-object v0

    .line 466
    invoke-direct {p0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->thirtyDaysAgo()Ljava/util/Date;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/ISO8601Dates;->tryBuildISO8601Date(Ljava/util/Date;)Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->start_date(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    move-result-object v0

    .line 467
    invoke-static {}, Lcom/squareup/util/ISO8601Dates;->now()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->end_date(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    move-result-object v0

    .line 468
    invoke-virtual {v0}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;

    move-result-object v0

    .line 472
    :try_start_0
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->service:Lcom/squareup/server/cashmanagement/CashManagementService;

    invoke-interface {v1, v0}, Lcom/squareup/server/cashmanagement/CashManagementService;->getDrawerHistory(Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsResponse;

    move-result-object v0
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsResponse;->cash_drawer_shifts:Ljava/util/List;

    goto :goto_0

    .line 482
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 484
    :goto_0
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->store:Lcom/squareup/cashmanagement/CashDrawerShiftStore;

    invoke-interface {v1, v0}, Lcom/squareup/cashmanagement/CashDrawerShiftStore;->repopulateCashDrawerShifts(Ljava/util/List;)V

    .line 485
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->hasCashDrawerData:Lcom/f2prateek/rx/preferences2/Preference;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 486
    invoke-direct {p0, p1}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->loadCurrentShiftInBackground(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V

    return-void

    :catch_0
    move-exception v0

    .line 474
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v2, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$BRJIwao6_SthLusjPqZ3oErcT8U;

    invoke-direct {v2, p1, v0}, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$BRJIwao6_SthLusjPqZ3oErcT8U;-><init>(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;Lretrofit/RetrofitError;)V

    invoke-interface {v1, v2}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$null$7$RealCashDrawerShiftManager(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 1

    .line 393
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->store:Lcom/squareup/cashmanagement/CashDrawerShiftStore;

    invoke-interface {v0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftStore;->saveCashDrawerShift(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    return-void
.end method

.method public synthetic lambda$updateShift$15$RealCashDrawerShiftManager(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 1

    .line 516
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->store:Lcom/squareup/cashmanagement/CashDrawerShiftStore;

    invoke-interface {v0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftStore;->saveCashDrawerShift(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    return-void
.end method

.method public loadCurrentCashDrawerShift(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cashmanagement/CashDrawerShiftsCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 154
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 155
    iget-boolean v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->loaded:Z

    if-nez v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->hasCashDrawerData:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->fileExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$taTlKe6932MbyYD0AxuviOdZdf4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cashmanagement/-$$Lambda$RealCashDrawerShiftManager$taTlKe6932MbyYD0AxuviOdZdf4;-><init>(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 161
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->loadShiftsFromServer(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V

    :goto_0
    return-void

    .line 156
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Current Cash Drawer Shift already loaded."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public updateCurrentCashDrawerShiftDescription(Ljava/lang/String;)V
    .locals 1

    .line 308
    invoke-direct {p0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->enforceMainThreadAndLoaded()V

    .line 309
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->currentShiftBuilder:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->description(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    .line 310
    invoke-direct {p0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->updateShift()V

    return-void
.end method
