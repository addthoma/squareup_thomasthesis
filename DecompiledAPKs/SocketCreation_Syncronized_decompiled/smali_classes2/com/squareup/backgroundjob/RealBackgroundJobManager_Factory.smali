.class public final Lcom/squareup/backgroundjob/RealBackgroundJobManager_Factory;
.super Ljava/lang/Object;
.source "RealBackgroundJobManager_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/backgroundjob/RealBackgroundJobManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final jobNotificationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager_Factory;->jobNotificationManagerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/backgroundjob/RealBackgroundJobManager_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            ">;)",
            "Lcom/squareup/backgroundjob/RealBackgroundJobManager_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/backgroundjob/RealBackgroundJobManager_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/backgroundjob/RealBackgroundJobManager_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;)Lcom/squareup/backgroundjob/RealBackgroundJobManager;
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;

    invoke-direct {v0, p0, p1}, Lcom/squareup/backgroundjob/RealBackgroundJobManager;-><init>(Landroid/app/Application;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/backgroundjob/RealBackgroundJobManager;
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager_Factory;->jobNotificationManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    invoke-static {v0, v1}, Lcom/squareup/backgroundjob/RealBackgroundJobManager_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;)Lcom/squareup/backgroundjob/RealBackgroundJobManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/backgroundjob/RealBackgroundJobManager_Factory;->get()Lcom/squareup/backgroundjob/RealBackgroundJobManager;

    move-result-object v0

    return-object v0
.end method
