.class public Lcom/squareup/backgroundjob/notification/NoOpBackgroundJobNotificationManager;
.super Ljava/lang/Object;
.source "NoOpBackgroundJobNotificationManager.java"

# interfaces
.implements Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public enableNotifications(Z)V
    .locals 0

    return-void
.end method

.method public hideAllNotifications()V
    .locals 0

    return-void
.end method

.method public hideAllNotificationsForTag(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public hideNotificationFor(Lcom/evernote/android/job/JobRequest;)V
    .locals 0

    return-void
.end method

.method public hideNotificationFor(Lcom/squareup/backgroundjob/JobParams;)V
    .locals 0

    return-void
.end method

.method public showNotificationFor(Lcom/evernote/android/job/JobRequest;)V
    .locals 0

    return-void
.end method
