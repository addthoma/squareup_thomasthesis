.class public Lcom/squareup/pauses/PauseAndResumePresenter;
.super Lmortar/Presenter;
.source "PauseAndResumePresenter.java"

# interfaces
.implements Lcom/squareup/pauses/PauseAndResumeRegistrar;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/pauses/PauseAndResumePresenter$Registration;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/pauses/PauseAndResumeActivity;",
        ">;",
        "Lcom/squareup/pauses/PauseAndResumeRegistrar;"
    }
.end annotation


# instance fields
.field private isRunningState:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field final registrations:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/pauses/PauseAndResumePresenter$Registration;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    const/4 v0, 0x0

    .line 22
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/pauses/PauseAndResumePresenter;->isRunningState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 30
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/squareup/pauses/PauseAndResumePresenter;->registrations:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public activityPaused()V
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/pauses/PauseAndResumePresenter;->registrations:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/pauses/PauseAndResumePresenter$Registration;

    .line 63
    iget-object v1, v1, Lcom/squareup/pauses/PauseAndResumePresenter$Registration;->registrant:Lcom/squareup/pauses/PausesAndResumes;

    invoke-interface {v1}, Lcom/squareup/pauses/PausesAndResumes;->onPause()V

    goto :goto_0

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/squareup/pauses/PauseAndResumePresenter;->isRunningState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public activityResumed()V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/pauses/PauseAndResumePresenter;->registrations:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/pauses/PauseAndResumePresenter$Registration;

    .line 71
    iget-object v1, v1, Lcom/squareup/pauses/PauseAndResumePresenter$Registration;->registrant:Lcom/squareup/pauses/PausesAndResumes;

    invoke-interface {v1}, Lcom/squareup/pauses/PausesAndResumes;->onResume()V

    goto :goto_0

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/squareup/pauses/PauseAndResumePresenter;->isRunningState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method protected extractBundleService(Lcom/squareup/pauses/PauseAndResumeActivity;)Lmortar/bundler/BundleService;
    .locals 0

    .line 36
    invoke-interface {p1}, Lcom/squareup/pauses/PauseAndResumeActivity;->getBundleService()Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/pauses/PauseAndResumeActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/pauses/PauseAndResumePresenter;->extractBundleService(Lcom/squareup/pauses/PauseAndResumeActivity;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public isRunning()Z
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/pauses/PauseAndResumePresenter;->isRunningState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isRunningState()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/pauses/PauseAndResumePresenter;->isRunningState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public onExitScope()V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/pauses/PauseAndResumePresenter;->registrations:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method public register(Lmortar/MortarScope;Lcom/squareup/pauses/PausesAndResumes;)V
    .locals 2

    .line 44
    new-instance v0, Lcom/squareup/pauses/PauseAndResumePresenter$Registration;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p2, v1}, Lcom/squareup/pauses/PauseAndResumePresenter$Registration;-><init>(Lcom/squareup/pauses/PauseAndResumePresenter;Lcom/squareup/pauses/PausesAndResumes;Lcom/squareup/pauses/PauseAndResumePresenter$1;)V

    .line 45
    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 47
    iget-object p1, p0, Lcom/squareup/pauses/PauseAndResumePresenter;->registrations:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/squareup/pauses/PauseAndResumePresenter;->isRunning()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 49
    invoke-interface {p2}, Lcom/squareup/pauses/PausesAndResumes;->onResume()V

    :cond_0
    return-void
.end method
