.class public final Lcom/squareup/padlock/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/padlock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final backspaceColor:I = 0x7f04004d

.field public static final backspaceDisabledColor:I = 0x7f04004e

.field public static final backspaceSelector:I = 0x7f04004f

.field public static final buttonSelector:I = 0x7f040086

.field public static final buttonTextSize:I = 0x7f04008b

.field public static final clearTextColor:I = 0x7f0400c1

.field public static final clearTextDisabledColor:I = 0x7f0400c2

.field public static final deleteType:I = 0x7f04011c

.field public static final digitColor:I = 0x7f040124

.field public static final digitDisabledColor:I = 0x7f040126

.field public static final drawDividerLines:I = 0x7f040139

.field public static final drawLeftLine:I = 0x7f04013a

.field public static final drawRightLine:I = 0x7f04013b

.field public static final drawTopLine:I = 0x7f04013c

.field public static final horizontalDividerStyle:I = 0x7f0401c4

.field public static final lettersColor:I = 0x7f040278

.field public static final lettersDisabledColor:I = 0x7f040279

.field public static final lettersTextSize:I = 0x7f04027a

.field public static final lineColor:I = 0x7f04027d

.field public static final pinMode:I = 0x7f040313

.field public static final pinSubmitColor:I = 0x7f040314

.field public static final showDecimal:I = 0x7f040349

.field public static final sq_aspectRatio:I = 0x7f0403c2

.field public static final submitColor:I = 0x7f0403fe

.field public static final submitDisabledColor:I = 0x7f0403ff

.field public static final submitSelector:I = 0x7f040400

.field public static final submitType:I = 0x7f040401

.field public static final tabletMode:I = 0x7f040429


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
