.class Lcom/squareup/padlock/Padlock$ClearButtonInfo;
.super Lcom/squareup/padlock/Padlock$ButtonInfo;
.source "Padlock.java"

# interfaces
.implements Lcom/squareup/padlock/Padlock$UpdatableEnabledState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/padlock/Padlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClearButtonInfo"
.end annotation


# instance fields
.field private res:Lcom/squareup/util/Res;

.field final synthetic this$0:Lcom/squareup/padlock/Padlock;


# direct methods
.method constructor <init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V
    .locals 0

    .line 1757
    iput-object p1, p0, Lcom/squareup/padlock/Padlock$ClearButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    .line 1758
    invoke-direct/range {p0 .. p8}, Lcom/squareup/padlock/Padlock$ButtonInfo;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    .line 1759
    iput-object p8, p0, Lcom/squareup/padlock/Padlock$ClearButtonInfo;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method protected click(FF)V
    .locals 0

    .line 1763
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$ClearButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    if-eqz p1, :cond_0

    .line 1764
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$ClearButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    invoke-interface {p1}, Lcom/squareup/padlock/Padlock$OnKeyPressListener;->onClearClicked()V

    :cond_0
    return-void
.end method

.method public getDescription()Ljava/lang/String;
    .locals 2

    .line 1776
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ClearButtonInfo;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/padlock/R$string;->text_clear:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayValue()Ljava/lang/String;
    .locals 2

    .line 1780
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ClearButtonInfo;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/padlock/R$string;->button_clear:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected longClick(FF)Z
    .locals 0

    .line 1769
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$ClearButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    if-eqz p1, :cond_0

    .line 1770
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$ClearButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    invoke-interface {p1}, Lcom/squareup/padlock/Padlock$OnKeyPressListener;->onClearLongpressed()V

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public updateEnabledState()V
    .locals 2

    .line 1784
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ClearButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->pinPadLeftButtonState:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CLEAR:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ClearButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->pinPadLeftButtonState:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CANCEL:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/squareup/padlock/Padlock$ClearButtonInfo;->setEnabled(Z)V

    .line 1785
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ClearButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    return-void
.end method

.method public updateRes(Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V
    .locals 0

    .line 1789
    iput-object p2, p0, Lcom/squareup/padlock/Padlock$ClearButtonInfo;->res:Lcom/squareup/util/Res;

    return-void
.end method
