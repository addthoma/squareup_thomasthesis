.class Lcom/squareup/android/activity/RealToastFactory;
.super Ljava/lang/Object;
.source "RealToastFactory.java"

# interfaces
.implements Lcom/squareup/util/ToastFactory;


# instance fields
.field private final activityProvider:Lcom/squareup/util/ForegroundActivityProvider;


# direct methods
.method constructor <init>(Lcom/squareup/util/ForegroundActivityProvider;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/android/activity/RealToastFactory;->activityProvider:Lcom/squareup/util/ForegroundActivityProvider;

    return-void
.end method


# virtual methods
.method public makeToast()Landroid/widget/Toast;
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/android/activity/RealToastFactory;->activityProvider:Lcom/squareup/util/ForegroundActivityProvider;

    invoke-interface {v0}, Lcom/squareup/util/ForegroundActivityProvider;->getForegroundActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 29
    :cond_0
    new-instance v1, Landroid/widget/Toast;

    invoke-direct {v1, v0}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    return-object v1
.end method

.method public showText(II)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/android/activity/RealToastFactory;->activityProvider:Lcom/squareup/util/ForegroundActivityProvider;

    invoke-interface {v0}, Lcom/squareup/util/ForegroundActivityProvider;->getForegroundActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 45
    :cond_0
    invoke-static {v0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public showText(Ljava/lang/CharSequence;I)V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/android/activity/RealToastFactory;->activityProvider:Lcom/squareup/util/ForegroundActivityProvider;

    invoke-interface {v0}, Lcom/squareup/util/ForegroundActivityProvider;->getForegroundActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 37
    :cond_0
    invoke-static {v0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void
.end method
