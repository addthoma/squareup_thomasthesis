.class public final Lcom/squareup/android/util/ResModule;
.super Ljava/lang/Object;
.source "ResModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/android/util/ResModule;",
        "",
        "()V",
        "provideRes",
        "Lcom/squareup/util/Res;",
        "resources",
        "Landroid/content/res/Resources;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/android/util/ResModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    new-instance v0, Lcom/squareup/android/util/ResModule;

    invoke-direct {v0}, Lcom/squareup/android/util/ResModule;-><init>()V

    sput-object v0, Lcom/squareup/android/util/ResModule;->INSTANCE:Lcom/squareup/android/util/ResModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideRes(Landroid/content/res/Resources;)Lcom/squareup/util/Res;
    .locals 1
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    new-instance v0, Lcom/squareup/util/Res$RealRes;

    invoke-direct {v0, p1}, Lcom/squareup/util/Res$RealRes;-><init>(Landroid/content/res/Resources;)V

    check-cast v0, Lcom/squareup/util/Res;

    return-object v0
.end method
