.class public final Lcom/squareup/android/util/AndroidUtilModule_ProvideConnectivityManagerFactory;
.super Ljava/lang/Object;
.source "AndroidUtilModule_ProvideConnectivityManagerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Landroid/net/ConnectivityManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/android/util/AndroidUtilModule_ProvideConnectivityManagerFactory;->contextProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvideConnectivityManagerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/android/util/AndroidUtilModule_ProvideConnectivityManagerFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/android/util/AndroidUtilModule_ProvideConnectivityManagerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/android/util/AndroidUtilModule_ProvideConnectivityManagerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideConnectivityManager(Landroid/app/Application;)Landroid/net/ConnectivityManager;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/android/util/AndroidUtilModule;->INSTANCE:Lcom/squareup/android/util/AndroidUtilModule;

    invoke-virtual {v0, p0}, Lcom/squareup/android/util/AndroidUtilModule;->provideConnectivityManager(Landroid/app/Application;)Landroid/net/ConnectivityManager;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/net/ConnectivityManager;

    return-object p0
.end method


# virtual methods
.method public get()Landroid/net/ConnectivityManager;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/android/util/AndroidUtilModule_ProvideConnectivityManagerFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/squareup/android/util/AndroidUtilModule_ProvideConnectivityManagerFactory;->provideConnectivityManager(Landroid/app/Application;)Landroid/net/ConnectivityManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/android/util/AndroidUtilModule_ProvideConnectivityManagerFactory;->get()Landroid/net/ConnectivityManager;

    move-result-object v0

    return-object v0
.end method
