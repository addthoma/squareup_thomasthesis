.class public final Lcom/squareup/android/util/ClockModule_ProvideClockFactory;
.super Ljava/lang/Object;
.source "ClockModule_ProvideClockFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/android/util/ClockModule_ProvideClockFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/util/Clock;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory$InstanceHolder;->access$000()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideClock()Lcom/squareup/util/Clock;
    .locals 2

    .line 27
    sget-object v0, Lcom/squareup/android/util/ClockModule;->INSTANCE:Lcom/squareup/android/util/ClockModule;

    invoke-virtual {v0}, Lcom/squareup/android/util/ClockModule;->provideClock()Lcom/squareup/util/Clock;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Clock;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/util/Clock;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->provideClock()Lcom/squareup/util/Clock;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->get()Lcom/squareup/util/Clock;

    move-result-object v0

    return-object v0
.end method
