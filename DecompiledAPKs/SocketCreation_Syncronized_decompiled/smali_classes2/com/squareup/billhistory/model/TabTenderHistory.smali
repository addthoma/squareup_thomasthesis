.class public Lcom/squareup/billhistory/model/TabTenderHistory;
.super Lcom/squareup/billhistory/model/TenderHistory;
.source "TabTenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/billhistory/model/TabTenderHistory$Builder;
    }
.end annotation


# instance fields
.field public final customerName:Ljava/lang/String;

.field public final smallCustomerImageUrl:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/billhistory/model/TabTenderHistory$Builder;)V
    .locals 1

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory;-><init>(Lcom/squareup/billhistory/model/TenderHistory$Builder;)V

    .line 42
    invoke-static {p1}, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;->access$100(Lcom/squareup/billhistory/model/TabTenderHistory$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TabTenderHistory;->customerName:Ljava/lang/String;

    .line 43
    invoke-static {p1}, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;->access$200(Lcom/squareup/billhistory/model/TabTenderHistory$Builder;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Urls;->validateImageUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/billhistory/model/TabTenderHistory;->smallCustomerImageUrl:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/billhistory/model/TabTenderHistory$Builder;Lcom/squareup/billhistory/model/TabTenderHistory$1;)V
    .locals 0

    .line 5
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/TabTenderHistory;-><init>(Lcom/squareup/billhistory/model/TabTenderHistory$Builder;)V

    return-void
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/billhistory/model/TabTenderHistory$Builder;
    .locals 1

    .line 47
    new-instance v0, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;-><init>()V

    invoke-virtual {v0, p0}, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;->from(Lcom/squareup/billhistory/model/TabTenderHistory;)Lcom/squareup/billhistory/model/TabTenderHistory$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildUpon()Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 1

    .line 5
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TabTenderHistory;->buildUpon()Lcom/squareup/billhistory/model/TabTenderHistory$Builder;

    move-result-object v0

    return-object v0
.end method
