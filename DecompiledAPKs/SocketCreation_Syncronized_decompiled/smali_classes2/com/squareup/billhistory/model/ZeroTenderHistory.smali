.class public Lcom/squareup/billhistory/model/ZeroTenderHistory;
.super Lcom/squareup/billhistory/model/TenderHistory;
.source "ZeroTenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory;-><init>(Lcom/squareup/billhistory/model/TenderHistory$Builder;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;Lcom/squareup/billhistory/model/ZeroTenderHistory$1;)V
    .locals 0

    .line 7
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/ZeroTenderHistory;-><init>(Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildUpon()Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/ZeroTenderHistory;->buildUpon()Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;

    move-result-object v0

    return-object v0
.end method

.method public buildUpon()Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;-><init>()V

    invoke-virtual {v0, p0}, Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;->from(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;

    return-object v0
.end method

.method public getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method
