.class public Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;
.super Ljava/lang/Object;
.source "BillHistory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/model/BillHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ItemizationsNoteComponent"
.end annotation


# instance fields
.field public final isCustomAmount:Z

.field public final isUnitPriced:Z

.field public final itemName:Ljava/lang/String;

.field public final notes:Ljava/lang/String;

.field public final quantity:Ljava/math/BigDecimal;

.field public final quantityPrecision:I

.field public final unitAbbreviation:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;IZLjava/lang/String;Z)V
    .locals 0

    .line 1344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1345
    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->itemName:Ljava/lang/String;

    .line 1346
    iput-object p2, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->notes:Ljava/lang/String;

    .line 1347
    iput-object p3, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->quantity:Ljava/math/BigDecimal;

    .line 1348
    iput p4, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->quantityPrecision:I

    .line 1349
    iput-boolean p5, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->isUnitPriced:Z

    .line 1350
    iput-object p6, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->unitAbbreviation:Ljava/lang/String;

    .line 1351
    iput-boolean p7, p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;->isCustomAmount:Z

    return-void
.end method
