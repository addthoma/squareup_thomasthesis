.class public abstract Lcom/squareup/billhistory/model/TenderHistory$Builder;
.super Ljava/lang/Object;
.source "TenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/model/TenderHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/billhistory/model/TenderHistory;",
        "B:",
        "Lcom/squareup/billhistory/model/TenderHistory$Builder<",
        "TT;TB;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private amount:Lcom/squareup/protos/common/Money;

.field private autoGratuityAmount:Lcom/squareup/protos/common/Money;

.field private billId:Lcom/squareup/protos/client/IdPair;

.field private clientDetails:Lcom/squareup/protos/client/bills/ClientDetails;

.field private completeTenderDetails:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

.field private contactToken:Ljava/lang/String;

.field private employeeToken:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private lastRefundableAt:Ljava/util/Date;

.field private receiptNumber:Ljava/lang/String;

.field private refundCardPresenceRequirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

.field private sequentialTenderNumber:Ljava/lang/String;

.field private tenderState:Lcom/squareup/protos/client/bills/Tender$State;

.field private timestamp:Ljava/util/Date;

.field private final type:Lcom/squareup/billhistory/model/TenderHistory$Type;


# direct methods
.method protected constructor <init>(Lcom/squareup/billhistory/model/TenderHistory$Type;)V
    .locals 0

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Lcom/squareup/protos/client/IdPair;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->billId:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Ljava/util/Date;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->lastRefundableAt:Ljava/util/Date;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->refundCardPresenceRequirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Lcom/squareup/protos/client/bills/Tender$State;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->tenderState:Lcom/squareup/protos/client/bills/Tender$State;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->sequentialTenderNumber:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Lcom/squareup/protos/client/bills/ClientDetails;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->clientDetails:Lcom/squareup/protos/client/bills/ClientDetails;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->receiptNumber:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->employeeToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Lcom/squareup/billhistory/model/TenderHistory$Type;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->autoGratuityAmount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->completeTenderDetails:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Ljava/util/Date;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->timestamp:Ljava/util/Date;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->contactToken:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            ")TB;"
        }
    .end annotation

    .line 179
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public autoGratuityAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            ")TB;"
        }
    .end annotation

    .line 184
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->autoGratuityAmount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public billId(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            ")TB;"
        }
    .end annotation

    .line 164
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->billId:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public abstract build()Lcom/squareup/billhistory/model/TenderHistory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public clientDetails(Lcom/squareup/protos/client/bills/ClientDetails;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/ClientDetails;",
            ")TB;"
        }
    .end annotation

    .line 225
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->clientDetails:Lcom/squareup/protos/client/bills/ClientDetails;

    return-object p0
.end method

.method public completeTenderDetails(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;",
            ")TB;"
        }
    .end annotation

    .line 215
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->completeTenderDetails:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    return-object p0
.end method

.method public contactToken(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .line 194
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->contactToken:Ljava/lang/String;

    return-object p0
.end method

.method public employeeToken(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .line 174
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->employeeToken:Ljava/lang/String;

    return-object p0
.end method

.method public from(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TB;"
        }
    .end annotation

    .line 141
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->id:Ljava/lang/String;

    .line 142
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->billId:Lcom/squareup/protos/client/IdPair;

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->billId:Lcom/squareup/protos/client/IdPair;

    .line 143
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->receiptNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->receiptNumber:Ljava/lang/String;

    .line 144
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->employeeToken:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->employeeToken:Ljava/lang/String;

    .line 145
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->amount:Lcom/squareup/protos/common/Money;

    .line 146
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->autoGratuityAmount:Lcom/squareup/protos/common/Money;

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->autoGratuityAmount:Lcom/squareup/protos/common/Money;

    .line 147
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->completeTenderDetails:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->completeTenderDetails:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    .line 148
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->timestamp:Ljava/util/Date;

    invoke-static {v0}, Lcom/squareup/util/Dates;->copy(Ljava/util/Date;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->timestamp:Ljava/util/Date;

    .line 149
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->contactToken:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->contactToken:Ljava/lang/String;

    .line 150
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->lastRefundableAt:Ljava/util/Date;

    invoke-static {v0}, Lcom/squareup/util/Dates;->copy(Ljava/util/Date;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->lastRefundableAt:Ljava/util/Date;

    .line 151
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->refundCardPresenceRequirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->refundCardPresenceRequirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    .line 152
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->tenderState:Lcom/squareup/protos/client/bills/Tender$State;

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->tenderState:Lcom/squareup/protos/client/bills/Tender$State;

    .line 153
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->sequentialTenderNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->sequentialTenderNumber:Ljava/lang/String;

    .line 154
    iget-object p1, p1, Lcom/squareup/billhistory/model/TenderHistory;->clientDetails:Lcom/squareup/protos/client/bills/ClientDetails;

    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->clientDetails:Lcom/squareup/protos/client/bills/ClientDetails;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .line 159
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public lastRefundableAt(Ljava/util/Date;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            ")TB;"
        }
    .end annotation

    .line 199
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->lastRefundableAt:Ljava/util/Date;

    return-object p0
.end method

.method public receiptNumber(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .line 169
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->receiptNumber:Ljava/lang/String;

    return-object p0
.end method

.method public refundCardPresenceRequirement(Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;",
            ")TB;"
        }
    .end annotation

    .line 205
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->refundCardPresenceRequirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    return-object p0
.end method

.method public sequentialTenderNumber(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .line 220
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->sequentialTenderNumber:Ljava/lang/String;

    return-object p0
.end method

.method public tenderState(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Tender$State;",
            ")TB;"
        }
    .end annotation

    .line 210
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->tenderState:Lcom/squareup/protos/client/bills/Tender$State;

    return-object p0
.end method

.method public timestamp(Ljava/util/Date;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            ")TB;"
        }
    .end annotation

    .line 189
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory$Builder;->timestamp:Ljava/util/Date;

    return-object p0
.end method

.method public tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            ")TB;"
        }
    .end annotation

    return-object p0
.end method

.method public tipPercentage(Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Percentage;",
            ")TB;"
        }
    .end annotation

    return-object p0
.end method
