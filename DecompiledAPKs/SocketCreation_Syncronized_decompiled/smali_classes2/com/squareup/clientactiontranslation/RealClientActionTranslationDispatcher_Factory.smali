.class public final Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher_Factory;
.super Ljava/lang/Object;
.source "RealClientActionTranslationDispatcher_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/clientactiontranslation/ClientActionTranslator;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/clientactiontranslation/ClientActionTranslator;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/clientactiontranslation/ClientActionTranslator;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;",
            ">;)",
            "Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljava/util/Set;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;)Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/clientactiontranslation/ClientActionTranslator;",
            ">;",
            "Lcom/squareup/ui/main/DeepLinks;",
            "Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;",
            ")",
            "Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;-><init>(Ljava/util/Set;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iget-object v1, p0, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/DeepLinks;

    iget-object v2, p0, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;

    invoke-static {v0, v1, v2}, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher_Factory;->newInstance(Ljava/util/Set;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;)Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher_Factory;->get()Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;

    move-result-object v0

    return-object v0
.end method
