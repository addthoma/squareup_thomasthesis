.class Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$4;
.super Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;
.source "PaperSignatureBatchRetryingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;->tenderStatus(Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall<",
        "Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;",
        "Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;",
        "Ljava/util/List<",
        "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;


# direct methods
.method constructor <init>(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
    .locals 0

    .line 259
    iput-object p1, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$4;->this$0:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;-><init>(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Ljava/lang/Object;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic createResult(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 259
    check-cast p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$4;->createResult(Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method protected createResult(Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;",
            ">;"
        }
    .end annotation

    .line 275
    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->tender_status_list:Ljava/util/List;

    return-object p1
.end method

.method protected doRequest(Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;)V
    .locals 1

    .line 270
    iget-object v0, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$4;->this$0:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    invoke-static {v0}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;->access$000(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;)Lcom/squareup/server/papersignature/PaperSignatureBatchService;

    move-result-object v0

    invoke-interface {v0, p1, p0}, Lcom/squareup/server/papersignature/PaperSignatureBatchService;->tenderStatus(Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method protected bridge synthetic doRequest(Ljava/lang/Object;)V
    .locals 0

    .line 259
    check-cast p1, Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$4;->doRequest(Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;)V

    return-void
.end method

.method protected getErrorMessage(Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;)Ljava/lang/String;
    .locals 0

    .line 283
    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->error_message:Ljava/lang/String;

    return-object p1
.end method

.method protected bridge synthetic getErrorMessage(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 259
    check-cast p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$4;->getErrorMessage(Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected getErrorTitle(Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;)Ljava/lang/String;
    .locals 0

    .line 279
    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->error_title:Ljava/lang/String;

    return-object p1
.end method

.method protected bridge synthetic getErrorTitle(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 259
    check-cast p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$4;->getErrorTitle(Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getNextRequestBackoffSeconds(Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;)I
    .locals 1

    .line 266
    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1
.end method

.method public bridge synthetic getNextRequestBackoffSeconds(Ljava/lang/Object;)I
    .locals 0

    .line 259
    check-cast p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$4;->getNextRequestBackoffSeconds(Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;)I

    move-result p1

    return p1
.end method

.method public isSuccess(Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;)Z
    .locals 1

    .line 262
    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->success:Ljava/lang/Boolean;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1
.end method

.method public bridge synthetic isSuccess(Ljava/lang/Object;)Z
    .locals 0

    .line 259
    check-cast p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$4;->isSuccess(Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;)Z

    move-result p1

    return p1
.end method
