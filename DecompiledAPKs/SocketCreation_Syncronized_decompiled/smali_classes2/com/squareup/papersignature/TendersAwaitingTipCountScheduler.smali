.class public Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;
.super Ljava/lang/Object;
.source "TendersAwaitingTipCountScheduler.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$OnTenderCountUpdatedListener;
    }
.end annotation


# static fields
.field private static final NEVER_SEEN_TENDER_COUNT:I = -0x1

.field private static final POLLING_INTERVAL_MS:J = 0x493e0L


# instance fields
.field private final countTendersRequest:Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;

.field private final internetState:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/connectivity/InternetState;",
            ">;"
        }
    .end annotation
.end field

.field private volatile lastSeenTenderCount:I

.field private final lastSeenTenderCountLock:Ljava/lang/Object;

.field private volatile lastSeenTenderExpiringSoonCount:I

.field private final listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$OnTenderCountUpdatedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final paperSignatureService:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

.field private final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field private polling:Z

.field private final requestCount:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/connectivity/ConnectivityMonitor;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Lcom/squareup/papersignature/-$$Lambda$TendersAwaitingTipCountScheduler$zGd-303YABTmu_Y2fvaSJBIBKxQ;

    invoke-direct {v0, p0}, Lcom/squareup/papersignature/-$$Lambda$TendersAwaitingTipCountScheduler$zGd-303YABTmu_Y2fvaSJBIBKxQ;-><init>(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;)V

    iput-object v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->requestCount:Ljava/lang/Runnable;

    .line 61
    iput-object p2, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 62
    iput-object p1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->paperSignatureService:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    .line 63
    iput-object p3, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 64
    invoke-interface {p4}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->internetState:Lio/reactivex/Observable;

    .line 66
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->listeners:Ljava/util/Set;

    .line 68
    new-instance p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest$Builder;->build()Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->countTendersRequest:Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;

    const/4 p1, -0x1

    .line 69
    iput p1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->lastSeenTenderCount:I

    .line 70
    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->lastSeenTenderCountLock:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;ILjava/lang/Integer;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->setLastSeenTenderCount(ILjava/lang/Integer;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->schedulePollingUpdate()V

    return-void
.end method

.method private doRequestCount()V
    .locals 3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Requesting tender count..."

    .line 189
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 190
    iget-object v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->paperSignatureService:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    iget-object v1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->countTendersRequest:Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;

    new-instance v2, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$1;

    invoke-direct {v2, p0}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$1;-><init>(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;->countTendersAwaitingMerchantTip(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    return-void
.end method

.method private fireOnUpdate()V
    .locals 2

    .line 217
    iget-object v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->listeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$OnTenderCountUpdatedListener;

    .line 218
    invoke-interface {v1}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$OnTenderCountUpdatedListener;->onTenderCountUpdated()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/connectivity/InternetState;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 76
    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static synthetic lambda$zGd-303YABTmu_Y2fvaSJBIBKxQ(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->doRequestCount()V

    return-void
.end method

.method private requestUpdateOnFirstAccess()V
    .locals 4

    .line 175
    iget-object v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->lastSeenTenderCountLock:Ljava/lang/Object;

    monitor-enter v0

    .line 176
    :try_start_0
    iget v1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->lastSeenTenderCount:I

    const/4 v2, -0x1

    const/4 v3, 0x0

    if-ne v1, v2, :cond_0

    .line 178
    iput v3, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->lastSeenTenderCount:I

    const/4 v3, 0x1

    .line 181
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_1

    .line 184
    invoke-virtual {p0}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->requestImmediateUpdate()V

    :cond_1
    return-void

    :catchall_0
    move-exception v1

    .line 181
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private schedulePollingUpdate()V
    .locals 2

    .line 223
    iget-boolean v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->polling:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-wide/32 v0, 0x493e0

    .line 226
    invoke-direct {p0, v0, v1}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->scheduleUpdate(J)V

    return-void
.end method

.method private scheduleUpdate(J)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 230
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Scheduling next update to count of tenders awaiting tip for %d ms"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    iget-object v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->requestCount:Ljava/lang/Runnable;

    invoke-interface {v0, v1, p1, p2}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private setLastSeenTenderCount(ILjava/lang/Integer;)V
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->lastSeenTenderCountLock:Ljava/lang/Object;

    monitor-enter v0

    .line 210
    :try_start_0
    iput p1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->lastSeenTenderCount:I

    const/4 p1, 0x0

    .line 211
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->lastSeenTenderExpiringSoonCount:I

    .line 212
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    invoke-direct {p0}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->fireOnUpdate()V

    return-void

    :catchall_0
    move-exception p1

    .line 212
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method


# virtual methods
.method public addOnTenderCountUpdatedListener(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$OnTenderCountUpdatedListener;)V
    .locals 1

    const-string v0, "listener"

    .line 87
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 88
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 89
    iget-object v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addToCountUntilNextUpdate(II)V
    .locals 2

    .line 161
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 162
    invoke-direct {p0}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->requestUpdateOnFirstAccess()V

    .line 164
    iget-object v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->lastSeenTenderCountLock:Ljava/lang/Object;

    monitor-enter v0

    .line 165
    :try_start_0
    iget v1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->lastSeenTenderCount:I

    add-int/2addr v1, p1

    const/4 p1, 0x0

    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->lastSeenTenderCount:I

    .line 166
    iget v1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->lastSeenTenderExpiringSoonCount:I

    add-int/2addr v1, p2

    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->lastSeenTenderExpiringSoonCount:I

    .line 167
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    invoke-direct {p0}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->fireOnUpdate()V

    return-void

    :catchall_0
    move-exception p1

    .line 167
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public getLastSeenTenderCount()I
    .locals 1

    .line 131
    invoke-direct {p0}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->requestUpdateOnFirstAccess()V

    .line 132
    iget v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->lastSeenTenderCount:I

    return v0
.end method

.method public hasAtLeastOneExpiringTender()Z
    .locals 1

    .line 139
    invoke-direct {p0}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->requestUpdateOnFirstAccess()V

    .line 140
    iget v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->lastSeenTenderExpiringSoonCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$onEnterScope$1$TendersAwaitingTipCountScheduler(Lcom/squareup/connectivity/InternetState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 77
    invoke-virtual {p0}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->requestImmediateUpdate()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->internetState:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/papersignature/-$$Lambda$TendersAwaitingTipCountScheduler$m4ZLVi7GJp5vjCkuxlAtm5dokAE;->INSTANCE:Lcom/squareup/papersignature/-$$Lambda$TendersAwaitingTipCountScheduler$m4ZLVi7GJp5vjCkuxlAtm5dokAE;

    .line 76
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/papersignature/-$$Lambda$TendersAwaitingTipCountScheduler$X7b9_rhAjHqTDscD5KJmVAKKMgo;

    invoke-direct {v1, p0}, Lcom/squareup/papersignature/-$$Lambda$TendersAwaitingTipCountScheduler$X7b9_rhAjHqTDscD5KJmVAKKMgo;-><init>(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;)V

    .line 77
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 74
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 79
    invoke-virtual {p0}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->startPollingIfPaperSigEnabled()V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    .line 83
    invoke-virtual {p0}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->stopPolling()V

    return-void
.end method

.method public removeOnTenderCountUpdatedListener(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$OnTenderCountUpdatedListener;)V
    .locals 1

    const-string v0, "listener"

    .line 93
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 94
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 95
    iget-object v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public requestImmediateUpdate()V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/papersignature/-$$Lambda$TendersAwaitingTipCountScheduler$zGd-303YABTmu_Y2fvaSJBIBKxQ;

    invoke-direct {v1, p0}, Lcom/squareup/papersignature/-$$Lambda$TendersAwaitingTipCountScheduler$zGd-303YABTmu_Y2fvaSJBIBKxQ;-><init>(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public startPollingIfPaperSigEnabled()V
    .locals 1

    .line 99
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 101
    iget-object v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v0}, Lcom/squareup/papersignature/PaperSignatureSettings;->isSignOnPrintedReceiptEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 105
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->polling:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 106
    iput-boolean v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->polling:Z

    .line 107
    invoke-direct {p0}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->doRequestCount()V

    :cond_1
    return-void
.end method

.method public stopPolling()V
    .locals 2

    .line 120
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    const/4 v0, 0x0

    .line 121
    iput-boolean v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->polling:Z

    .line 122
    iget-object v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->requestCount:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method
