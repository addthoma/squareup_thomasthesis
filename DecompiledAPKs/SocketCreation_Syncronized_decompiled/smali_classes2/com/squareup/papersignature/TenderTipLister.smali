.class public Lcom/squareup/papersignature/TenderTipLister;
.super Ljava/lang/Object;
.source "TenderTipLister.java"


# static fields
.field private static final LIST_REQUEST:Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipRequest;


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final paperSignatureService:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

.field private final tenderStatusManager:Lcom/squareup/papersignature/TenderStatusManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipRequest$Builder;-><init>()V

    .line 28
    invoke-virtual {v0}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipRequest$Builder;->build()Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipRequest;

    move-result-object v0

    sput-object v0, Lcom/squareup/papersignature/TenderTipLister;->LIST_REQUEST:Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipRequest;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/papersignature/TenderStatusManager;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;",
            "Lcom/squareup/papersignature/TenderStatusManager;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/papersignature/TenderTipLister;->paperSignatureService:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    .line 37
    iput-object p2, p0, Lcom/squareup/papersignature/TenderTipLister;->tenderStatusManager:Lcom/squareup/papersignature/TenderStatusManager;

    .line 38
    iput-object p3, p0, Lcom/squareup/papersignature/TenderTipLister;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private buildTenderHistories(Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;"
        }
    .end annotation

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 67
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 70
    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_with_bill_id:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/paper_signature/TenderWithBillId;

    .line 71
    iget-object v4, v3, Lcom/squareup/protos/client/paper_signature/TenderWithBillId;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v5, v3, Lcom/squareup/protos/client/paper_signature/TenderWithBillId;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v6, v3, Lcom/squareup/protos/client/paper_signature/TenderWithBillId;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v6, v6, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v6, v6, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v3, v3, Lcom/squareup/protos/client/paper_signature/TenderWithBillId;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Tender$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    const/4 v7, 0x0

    .line 72
    invoke-static {v4, v5, v6, v3, v7}, Lcom/squareup/billhistory/model/TenderHistory;->fromTender(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v3

    .line 79
    invoke-virtual {v3}, Lcom/squareup/billhistory/model/TenderHistory;->buildUpon()Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v3

    sget-object v4, Lcom/squareup/protos/client/bills/Tender$State;->AWAITING_MERCHANT_TIP:Lcom/squareup/protos/client/bills/Tender$State;

    .line 80
    invoke-virtual {v3, v4}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->tenderState(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v3

    .line 81
    invoke-virtual {v3}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->build()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v3

    .line 82
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    iget-object v3, v3, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 87
    :cond_0
    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    .line 88
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_with_bill_id:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v2, v3, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_3

    .line 92
    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;

    .line 93
    iget-object v3, v2, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tender_id:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 94
    iget-object v3, p0, Lcom/squareup/papersignature/TenderTipLister;->localeProvider:Ljavax/inject/Provider;

    .line 95
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Locale;

    invoke-static {v2, v3}, Lcom/squareup/billhistory/model/TenderHistory;->fromLegacyPaperSignatureTender(Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;Ljava/util/Locale;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v2

    .line 98
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    return-object v0
.end method


# virtual methods
.method public getTendersAwaitingTip(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback<",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;>;)V"
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/squareup/papersignature/TenderTipLister;->paperSignatureService:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    sget-object v1, Lcom/squareup/papersignature/TenderTipLister;->LIST_REQUEST:Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipRequest;

    new-instance v2, Lcom/squareup/papersignature/TenderTipLister$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/papersignature/TenderTipLister$1;-><init>(Lcom/squareup/papersignature/TenderTipLister;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;->listTendersAwaitingMerchantTip(Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    return-void
.end method

.method handleListResult(Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;"
        }
    .end annotation

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/papersignature/TenderTipLister;->buildTenderHistories(Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;)Ljava/util/List;

    move-result-object p1

    .line 58
    iget-object v0, p0, Lcom/squareup/papersignature/TenderTipLister;->tenderStatusManager:Lcom/squareup/papersignature/TenderStatusManager;

    invoke-virtual {v0, p1}, Lcom/squareup/papersignature/TenderStatusManager;->processListTendersResult(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
