.class Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$3;
.super Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;
.source "PaperSignatureBatchRetryingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;->countTendersAwaitingMerchantTip(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall<",
        "Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;",
        "Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;",
        "Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;


# direct methods
.method constructor <init>(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
    .locals 0

    .line 226
    iput-object p1, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$3;->this$0:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;-><init>(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Ljava/lang/Object;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    return-void
.end method


# virtual methods
.method protected createResult(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;
    .locals 0

    return-object p1
.end method

.method protected bridge synthetic createResult(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 226
    check-cast p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$3;->createResult(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    move-result-object p1

    return-object p1
.end method

.method protected doRequest(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;)V
    .locals 1

    .line 238
    iget-object v0, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$3;->this$0:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    invoke-static {v0}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;->access$000(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;)Lcom/squareup/server/papersignature/PaperSignatureBatchService;

    move-result-object v0

    invoke-interface {v0, p1, p0}, Lcom/squareup/server/papersignature/PaperSignatureBatchService;->countTendersAwaitingMerchantTip(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method protected bridge synthetic doRequest(Ljava/lang/Object;)V
    .locals 0

    .line 226
    check-cast p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$3;->doRequest(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;)V

    return-void
.end method

.method protected getErrorMessage(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)Ljava/lang/String;
    .locals 0

    .line 251
    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    return-object p1
.end method

.method protected bridge synthetic getErrorMessage(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 226
    check-cast p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$3;->getErrorMessage(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected getErrorTitle(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)Ljava/lang/String;
    .locals 0

    .line 247
    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    return-object p1
.end method

.method protected bridge synthetic getErrorTitle(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 226
    check-cast p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$3;->getErrorTitle(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getNextRequestBackoffSeconds(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)I
    .locals 1

    .line 234
    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1
.end method

.method public bridge synthetic getNextRequestBackoffSeconds(Ljava/lang/Object;)I
    .locals 0

    .line 226
    check-cast p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$3;->getNextRequestBackoffSeconds(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)I

    move-result p1

    return p1
.end method

.method public isSuccess(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)Z
    .locals 0

    .line 229
    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1
.end method

.method public bridge synthetic isSuccess(Ljava/lang/Object;)Z
    .locals 0

    .line 226
    check-cast p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$3;->isSuccess(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)Z

    move-result p1

    return p1
.end method
