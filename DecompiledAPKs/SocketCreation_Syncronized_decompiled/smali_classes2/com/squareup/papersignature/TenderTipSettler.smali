.class public Lcom/squareup/papersignature/TenderTipSettler;
.super Ljava/lang/Object;
.source "TenderTipSettler.java"


# instance fields
.field private final paperSignatureService:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

.field private final tenderStatusManager:Lcom/squareup/papersignature/TenderStatusManager;

.field private final tippingCalculator:Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;


# direct methods
.method public constructor <init>(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/papersignature/TenderStatusManager;Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/papersignature/TenderTipSettler;->paperSignatureService:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    .line 33
    iput-object p2, p0, Lcom/squareup/papersignature/TenderTipSettler;->tenderStatusManager:Lcom/squareup/papersignature/TenderStatusManager;

    .line 34
    iput-object p3, p0, Lcom/squareup/papersignature/TenderTipSettler;->tippingCalculator:Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/papersignature/TenderTipSettler;Ljava/util/Collection;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/squareup/papersignature/TenderTipSettler;->onSubmitBatchSuccess(Ljava/util/Collection;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    return-void
.end method

.method private buildTipSettleList(Ljava/util/Collection;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest;",
            ">;"
        }
    .end annotation

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 67
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 68
    invoke-direct {p0, v1}, Lcom/squareup/papersignature/TenderTipSettler;->clampTipAmountToMax(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v1

    .line 70
    new-instance v2, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;-><init>()V

    iget-object v3, v1, Lcom/squareup/billhistory/model/TenderHistory;->billId:Lcom/squareup/protos/client/IdPair;

    .line 71
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;

    move-result-object v2

    iget-object v3, v1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    .line 72
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;->tender_id(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;

    move-result-object v2

    .line 73
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;

    move-result-object v2

    iget-object v1, v1, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    .line 74
    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;

    move-result-object v1

    .line 75
    invoke-virtual {v1}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;->build()Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest;

    move-result-object v1

    .line 70
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private clampTipAmountToMax(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/TenderHistory;
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/papersignature/TenderTipSettler;->tippingCalculator:Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;->getCustomTipMaxMoney(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 88
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 90
    invoke-static {v1, v0}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 91
    invoke-virtual {p1, v0, v1}, Lcom/squareup/billhistory/model/TenderHistory;->withTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private onSubmitBatchSuccess(Ljava/util/Collection;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/papersignature/TenderTipSettler;->tenderStatusManager:Lcom/squareup/papersignature/TenderStatusManager;

    invoke-virtual {v0, p1}, Lcom/squareup/papersignature/TenderStatusManager;->cacheAsSettledAndScheduleNextUpdateIfNecessary(Ljava/util/Collection;)V

    const/4 p1, 0x0

    .line 61
    invoke-interface {p2, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public settleTips(Ljava/util/Collection;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/papersignature/TenderTipSettler;->buildTipSettleList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    .line 42
    new-instance v2, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;-><init>()V

    .line 43
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;->client_request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;

    move-result-object v0

    .line 44
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;->add_tip_and_settle_request_list(Ljava/util/List;)Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;->build()Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lcom/squareup/papersignature/TenderTipSettler;->paperSignatureService:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    new-instance v2, Lcom/squareup/papersignature/TenderTipSettler$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/squareup/papersignature/TenderTipSettler$1;-><init>(Lcom/squareup/papersignature/TenderTipSettler;Ljava/util/Collection;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    invoke-virtual {v1, v0, v2}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;->submitTipAndSettleBatch(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    return-void
.end method
