.class Lcom/squareup/papersignature/TenderTipLister$1;
.super Ljava/lang/Object;
.source "TenderTipLister.java"

# interfaces
.implements Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/papersignature/TenderTipLister;->getTendersAwaitingTip(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback<",
        "Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/papersignature/TenderTipLister;

.field final synthetic val$callback:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;


# direct methods
.method constructor <init>(Lcom/squareup/papersignature/TenderTipLister;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/papersignature/TenderTipLister$1;->this$0:Lcom/squareup/papersignature/TenderTipLister;

    iput-object p2, p0, Lcom/squareup/papersignature/TenderTipLister$1;->val$callback:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/papersignature/TenderTipLister$1;->val$callback:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;

    invoke-interface {v0, p1, p2}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;->onFailure(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onSuccess(Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;)V
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/squareup/papersignature/TenderTipLister$1;->val$callback:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;

    iget-object v1, p0, Lcom/squareup/papersignature/TenderTipLister$1;->this$0:Lcom/squareup/papersignature/TenderTipLister;

    invoke-virtual {v1, p1}, Lcom/squareup/papersignature/TenderTipLister;->handleListResult(Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/TenderTipLister$1;->onSuccess(Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;)V

    return-void
.end method
