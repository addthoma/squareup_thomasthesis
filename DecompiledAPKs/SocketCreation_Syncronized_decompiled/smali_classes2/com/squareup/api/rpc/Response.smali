.class public final Lcom/squareup/api/rpc/Response;
.super Lcom/squareup/wire/Message;
.source "Response.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/rpc/Response$ProtoAdapter_Response;,
        Lcom/squareup/api/rpc/Response$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/rpc/Response;",
        "Lcom/squareup/api/rpc/Response$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/rpc/Response;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ID:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.CreateSessionResponse#ADAPTER"
        tag = 0x41a
    .end annotation
.end field

.field public final error:Lcom/squareup/api/rpc/Error;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.rpc.Error#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final get_response:Lcom/squareup/api/sync/GetResponse;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.GetResponse#ADAPTER"
        tag = 0x41b
    .end annotation
.end field

.field public final id:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field

.field public final inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.InventoryAdjustResponse#ADAPTER"
        tag = 0x578
    .end annotation
.end field

.field public final put_response:Lcom/squareup/api/sync/PutResponse;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.PutResponse#ADAPTER"
        tag = 0x41c
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/api/rpc/Response$ProtoAdapter_Response;

    invoke-direct {v0}, Lcom/squareup/api/rpc/Response$ProtoAdapter_Response;-><init>()V

    sput-object v0, Lcom/squareup/api/rpc/Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 29
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/rpc/Response;->DEFAULT_ID:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lcom/squareup/api/rpc/Error;Lcom/squareup/api/sync/CreateSessionResponse;Lcom/squareup/api/sync/GetResponse;Lcom/squareup/api/sync/PutResponse;Lcom/squareup/api/items/InventoryAdjustResponse;)V
    .locals 8

    .line 89
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/api/rpc/Response;-><init>(Ljava/lang/Long;Lcom/squareup/api/rpc/Error;Lcom/squareup/api/sync/CreateSessionResponse;Lcom/squareup/api/sync/GetResponse;Lcom/squareup/api/sync/PutResponse;Lcom/squareup/api/items/InventoryAdjustResponse;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lcom/squareup/api/rpc/Error;Lcom/squareup/api/sync/CreateSessionResponse;Lcom/squareup/api/sync/GetResponse;Lcom/squareup/api/sync/PutResponse;Lcom/squareup/api/items/InventoryAdjustResponse;Lokio/ByteString;)V
    .locals 1

    .line 95
    sget-object v0, Lcom/squareup/api/rpc/Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 96
    iput-object p1, p0, Lcom/squareup/api/rpc/Response;->id:Ljava/lang/Long;

    .line 97
    iput-object p2, p0, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    .line 98
    iput-object p3, p0, Lcom/squareup/api/rpc/Response;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    .line 99
    iput-object p4, p0, Lcom/squareup/api/rpc/Response;->get_response:Lcom/squareup/api/sync/GetResponse;

    .line 100
    iput-object p5, p0, Lcom/squareup/api/rpc/Response;->put_response:Lcom/squareup/api/sync/PutResponse;

    .line 101
    iput-object p6, p0, Lcom/squareup/api/rpc/Response;->inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 120
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/rpc/Response;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 121
    :cond_1
    check-cast p1, Lcom/squareup/api/rpc/Response;

    .line 122
    invoke-virtual {p0}, Lcom/squareup/api/rpc/Response;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/rpc/Response;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->id:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/api/rpc/Response;->id:Ljava/lang/Long;

    .line 123
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    iget-object v3, p1, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    .line 124
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    iget-object v3, p1, Lcom/squareup/api/rpc/Response;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    .line 125
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->get_response:Lcom/squareup/api/sync/GetResponse;

    iget-object v3, p1, Lcom/squareup/api/rpc/Response;->get_response:Lcom/squareup/api/sync/GetResponse;

    .line 126
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->put_response:Lcom/squareup/api/sync/PutResponse;

    iget-object v3, p1, Lcom/squareup/api/rpc/Response;->put_response:Lcom/squareup/api/sync/PutResponse;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;

    iget-object p1, p1, Lcom/squareup/api/rpc/Response;->inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;

    .line 128
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 133
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 135
    invoke-virtual {p0}, Lcom/squareup/api/rpc/Response;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->id:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 137
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/rpc/Error;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 138
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/api/sync/CreateSessionResponse;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 139
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->get_response:Lcom/squareup/api/sync/GetResponse;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/api/sync/GetResponse;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 140
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->put_response:Lcom/squareup/api/sync/PutResponse;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/api/sync/PutResponse;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/api/items/InventoryAdjustResponse;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 142
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/rpc/Response$Builder;
    .locals 2

    .line 106
    new-instance v0, Lcom/squareup/api/rpc/Response$Builder;

    invoke-direct {v0}, Lcom/squareup/api/rpc/Response$Builder;-><init>()V

    .line 107
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->id:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/api/rpc/Response$Builder;->id:Ljava/lang/Long;

    .line 108
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    iput-object v1, v0, Lcom/squareup/api/rpc/Response$Builder;->error:Lcom/squareup/api/rpc/Error;

    .line 109
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    iput-object v1, v0, Lcom/squareup/api/rpc/Response$Builder;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    .line 110
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->get_response:Lcom/squareup/api/sync/GetResponse;

    iput-object v1, v0, Lcom/squareup/api/rpc/Response$Builder;->get_response:Lcom/squareup/api/sync/GetResponse;

    .line 111
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->put_response:Lcom/squareup/api/sync/PutResponse;

    iput-object v1, v0, Lcom/squareup/api/rpc/Response$Builder;->put_response:Lcom/squareup/api/sync/PutResponse;

    .line 112
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;

    iput-object v1, v0, Lcom/squareup/api/rpc/Response$Builder;->inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;

    .line 113
    invoke-virtual {p0}, Lcom/squareup/api/rpc/Response;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/rpc/Response$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/api/rpc/Response;->newBuilder()Lcom/squareup/api/rpc/Response$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 150
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->id:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->id:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 151
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    if-eqz v1, :cond_1

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 152
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    if-eqz v1, :cond_2

    const-string v1, ", create_session_response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 153
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->get_response:Lcom/squareup/api/sync/GetResponse;

    if-eqz v1, :cond_3

    const-string v1, ", get_response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->get_response:Lcom/squareup/api/sync/GetResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 154
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->put_response:Lcom/squareup/api/sync/PutResponse;

    if-eqz v1, :cond_4

    const-string v1, ", put_response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->put_response:Lcom/squareup/api/sync/PutResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 155
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;

    if-eqz v1, :cond_5

    const-string v1, ", inventory_adjust_response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Response;->inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Response{"

    .line 156
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
