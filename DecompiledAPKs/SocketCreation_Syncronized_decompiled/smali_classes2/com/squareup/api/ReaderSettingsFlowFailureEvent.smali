.class public Lcom/squareup/api/ReaderSettingsFlowFailureEvent;
.super Lcom/squareup/api/RegisterApiEvent;
.source "ReaderSettingsFlowFailureEvent.java"


# instance fields
.field public final reason:Ljava/lang/String;

.field public final sequenceUuid:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "api_sequence_uuid"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/ApiErrorResult;J)V
    .locals 6

    .line 16
    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->API_READER_SETTINGS_FAILURE:Lcom/squareup/analytics/RegisterActionName;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-wide v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/RegisterApiEvent;-><init>(Lcom/squareup/util/Clock;Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;J)V

    .line 17
    invoke-virtual {p4}, Lcom/squareup/api/ApiErrorResult;->name()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/ReaderSettingsFlowFailureEvent;->reason:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Lcom/squareup/api/ReaderSettingsFlowFailureEvent;->sequenceUuid:Ljava/lang/String;

    return-void
.end method
