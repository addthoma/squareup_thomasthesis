.class Lcom/squareup/api/ApiTransactionController$1;
.super Lcom/squareup/mortar/BundlerAdapter;
.source "ApiTransactionController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/api/ApiTransactionController;->getBundler()Lmortar/bundler/Bundler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/api/ApiTransactionController;


# direct methods
.method constructor <init>(Lcom/squareup/api/ApiTransactionController;Ljava/lang/String;)V
    .locals 0

    .line 372
    iput-object p1, p0, Lcom/squareup/api/ApiTransactionController$1;->this$0:Lcom/squareup/api/ApiTransactionController;

    invoke-direct {p0, p2}, Lcom/squareup/mortar/BundlerAdapter;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$ApiTransactionController$1(Lkotlin/Unit;)V
    .locals 2

    .line 376
    iget-object p1, p0, Lcom/squareup/api/ApiTransactionController$1;->this$0:Lcom/squareup/api/ApiTransactionController;

    invoke-static {p1}, Lcom/squareup/api/ApiTransactionController;->access$500(Lcom/squareup/api/ApiTransactionController;)Lcom/squareup/api/ApiTransactionState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 377
    iget-object p1, p0, Lcom/squareup/api/ApiTransactionController$1;->this$0:Lcom/squareup/api/ApiTransactionController;

    const/4 v0, 0x0

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->STALE_TRANSACTION_REQUEST:Lcom/squareup/api/ApiErrorResult;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/api/ApiTransactionController;->handleApiTransactionCanceled(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/api/ApiErrorResult;)Z

    :cond_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 375
    iget-object p1, p0, Lcom/squareup/api/ApiTransactionController$1;->this$0:Lcom/squareup/api/ApiTransactionController;

    invoke-static {p1}, Lcom/squareup/api/ApiTransactionController;->access$000(Lcom/squareup/api/ApiTransactionController;)Lcom/squareup/api/ApiRequestController;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/ApiRequestController;->onStaleApiRequest()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/api/-$$Lambda$ApiTransactionController$1$k2Ge8CBe1b2VUwlTPU-Vnn3uILk;

    invoke-direct {v0, p0}, Lcom/squareup/api/-$$Lambda$ApiTransactionController$1$k2Ge8CBe1b2VUwlTPU-Vnn3uILk;-><init>(Lcom/squareup/api/ApiTransactionController$1;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 383
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController$1;->this$0:Lcom/squareup/api/ApiTransactionController;

    invoke-static {v0}, Lcom/squareup/api/ApiTransactionController;->access$100(Lcom/squareup/api/ApiTransactionController;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 386
    :cond_0
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController$1;->this$0:Lcom/squareup/api/ApiTransactionController;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/api/ApiTransactionController;->access$102(Lcom/squareup/api/ApiTransactionController;Z)Z

    if-eqz p1, :cond_1

    .line 388
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController$1;->this$0:Lcom/squareup/api/ApiTransactionController;

    const-string v1, "BROWSER_APPLICATION_ID"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/api/ApiTransactionController;->access$202(Lcom/squareup/api/ApiTransactionController;Ljava/lang/String;)Ljava/lang/String;

    .line 389
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController$1;->this$0:Lcom/squareup/api/ApiTransactionController;

    const-string v1, "WEB_CALLBACK_URI"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/api/ApiTransactionController;->access$302(Lcom/squareup/api/ApiTransactionController;Ljava/lang/String;)Ljava/lang/String;

    .line 390
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController$1;->this$0:Lcom/squareup/api/ApiTransactionController;

    const-string v1, "REQUEST_STATE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/api/ApiTransactionController;->access$402(Lcom/squareup/api/ApiTransactionController;Ljava/lang/String;)Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 395
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController$1;->this$0:Lcom/squareup/api/ApiTransactionController;

    invoke-static {v0}, Lcom/squareup/api/ApiTransactionController;->access$200(Lcom/squareup/api/ApiTransactionController;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController$1;->this$0:Lcom/squareup/api/ApiTransactionController;

    invoke-static {v0}, Lcom/squareup/api/ApiTransactionController;->access$200(Lcom/squareup/api/ApiTransactionController;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "BROWSER_APPLICATION_ID"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController$1;->this$0:Lcom/squareup/api/ApiTransactionController;

    invoke-static {v0}, Lcom/squareup/api/ApiTransactionController;->access$300(Lcom/squareup/api/ApiTransactionController;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 399
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController$1;->this$0:Lcom/squareup/api/ApiTransactionController;

    invoke-static {v0}, Lcom/squareup/api/ApiTransactionController;->access$300(Lcom/squareup/api/ApiTransactionController;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "WEB_CALLBACK_URI"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    :cond_1
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController$1;->this$0:Lcom/squareup/api/ApiTransactionController;

    invoke-static {v0}, Lcom/squareup/api/ApiTransactionController;->access$400(Lcom/squareup/api/ApiTransactionController;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 402
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController$1;->this$0:Lcom/squareup/api/ApiTransactionController;

    invoke-static {v0}, Lcom/squareup/api/ApiTransactionController;->access$400(Lcom/squareup/api/ApiTransactionController;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "REQUEST_STATE"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-void
.end method
