.class public final Lcom/squareup/api/ApiValidator_Factory;
.super Ljava/lang/Object;
.source "ApiValidator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/api/ApiValidator;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final appDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/AppDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final authenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private final clientSettingsValidatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ClientSettingsValidator;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final isReaderSdkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final resourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/AppDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ClientSettingsValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/api/ApiValidator_Factory;->appDelegateProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p2, p0, Lcom/squareup/api/ApiValidator_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p3, p0, Lcom/squareup/api/ApiValidator_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p4, p0, Lcom/squareup/api/ApiValidator_Factory;->resourcesProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p5, p0, Lcom/squareup/api/ApiValidator_Factory;->isReaderSdkProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p6, p0, Lcom/squareup/api/ApiValidator_Factory;->clientSettingsValidatorProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p7, p0, Lcom/squareup/api/ApiValidator_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/api/ApiValidator_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/AppDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ClientSettingsValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)",
            "Lcom/squareup/api/ApiValidator_Factory;"
        }
    .end annotation

    .line 59
    new-instance v8, Lcom/squareup/api/ApiValidator_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/api/ApiValidator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/AppDelegate;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/settings/server/Features;Landroid/content/res/Resources;ZLcom/squareup/api/ClientSettingsValidator;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/api/ApiValidator;
    .locals 9

    .line 66
    new-instance v8, Lcom/squareup/api/ApiValidator;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/api/ApiValidator;-><init>(Lcom/squareup/AppDelegate;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/settings/server/Features;Landroid/content/res/Resources;ZLcom/squareup/api/ClientSettingsValidator;Lcom/squareup/settings/server/AccountStatusSettings;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/api/ApiValidator;
    .locals 8

    .line 51
    iget-object v0, p0, Lcom/squareup/api/ApiValidator_Factory;->appDelegateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/AppDelegate;

    iget-object v0, p0, Lcom/squareup/api/ApiValidator_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/account/LegacyAuthenticator;

    iget-object v0, p0, Lcom/squareup/api/ApiValidator_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/api/ApiValidator_Factory;->resourcesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/squareup/api/ApiValidator_Factory;->isReaderSdkProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iget-object v0, p0, Lcom/squareup/api/ApiValidator_Factory;->clientSettingsValidatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/api/ClientSettingsValidator;

    iget-object v0, p0, Lcom/squareup/api/ApiValidator_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static/range {v1 .. v7}, Lcom/squareup/api/ApiValidator_Factory;->newInstance(Lcom/squareup/AppDelegate;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/settings/server/Features;Landroid/content/res/Resources;ZLcom/squareup/api/ClientSettingsValidator;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/api/ApiValidator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/api/ApiValidator_Factory;->get()Lcom/squareup/api/ApiValidator;

    move-result-object v0

    return-object v0
.end method
