.class public Lcom/squareup/api/ApiAddCardOnFileController;
.super Ljava/lang/Object;
.source "ApiAddCardOnFileController.java"


# static fields
.field private static final ADD_CUSTOMER_CARD_API_REQUEST_IN_FLIGHT:Ljava/lang/String; = "AddCustomerCardApiRequestInFlight"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final clock:Lcom/squareup/util/Clock;

.field private historyFactory:Lcom/squareup/ui/main/HistoryFactory;

.field private loaded:Z

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

.field private final requestController:Lcom/squareup/api/ApiRequestController;

.field private requestInFlight:Z

.field private final resources:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Lcom/squareup/util/Clock;Lcom/squareup/analytics/Analytics;Landroid/content/res/Resources;Lcom/squareup/api/ApiRequestController;Lcom/squareup/log/OhSnapLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/api/ApiAddCardOnFileController;->clock:Lcom/squareup/util/Clock;

    .line 45
    iput-object p2, p0, Lcom/squareup/api/ApiAddCardOnFileController;->analytics:Lcom/squareup/analytics/Analytics;

    .line 46
    iput-object p4, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestController:Lcom/squareup/api/ApiRequestController;

    .line 47
    iput-object p5, p0, Lcom/squareup/api/ApiAddCardOnFileController;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 48
    iput-object p3, p0, Lcom/squareup/api/ApiAddCardOnFileController;->resources:Landroid/content/res/Resources;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/api/ApiAddCardOnFileController;)Lcom/squareup/api/ApiRequestController;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestController:Lcom/squareup/api/ApiRequestController;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/api/ApiAddCardOnFileController;)Z
    .locals 0

    .line 27
    iget-boolean p0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->loaded:Z

    return p0
.end method

.method static synthetic access$102(Lcom/squareup/api/ApiAddCardOnFileController;Z)Z
    .locals 0

    .line 27
    iput-boolean p1, p0, Lcom/squareup/api/ApiAddCardOnFileController;->loaded:Z

    return p1
.end method

.method static synthetic access$200(Lcom/squareup/api/ApiAddCardOnFileController;)Z
    .locals 0

    .line 27
    iget-boolean p0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestInFlight:Z

    return p0
.end method

.method static synthetic access$202(Lcom/squareup/api/ApiAddCardOnFileController;Z)Z
    .locals 0

    .line 27
    iput-boolean p1, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestInFlight:Z

    return p1
.end method

.method static synthetic access$300(Lcom/squareup/api/ApiAddCardOnFileController;)Lcom/squareup/util/Clock;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->clock:Lcom/squareup/util/Clock;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/api/ApiAddCardOnFileController;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/api/ApiAddCardOnFileController;Lcom/squareup/api/ApiErrorResult;)Landroid/content/Intent;
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/api/ApiAddCardOnFileController;->makeFailureResponse(Lcom/squareup/api/ApiErrorResult;)Landroid/content/Intent;

    move-result-object p0

    return-object p0
.end method

.method private logError(Lcom/squareup/api/ApiErrorResult;)V
    .locals 8

    .line 132
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->sequenceUuid()Ljava/lang/String;

    move-result-object v3

    .line 133
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->requestStartTime()J

    move-result-wide v5

    .line 134
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->clientId()Ljava/lang/String;

    move-result-object v4

    .line 136
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->API:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Register API store card on file failed: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/api/ApiErrorResult;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 137
    iget-object p1, p0, Lcom/squareup/api/ApiAddCardOnFileController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/api/StoreCardCancelledEvent;

    iget-object v2, p0, Lcom/squareup/api/ApiAddCardOnFileController;->clock:Lcom/squareup/util/Clock;

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/api/StoreCardCancelledEvent;-><init>(Lcom/squareup/util/Clock;Ljava/lang/String;Ljava/lang/String;J)V

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private logSuccess()V
    .locals 8

    .line 122
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->sequenceUuid()Ljava/lang/String;

    move-result-object v3

    .line 123
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->requestStartTime()J

    move-result-wide v5

    .line 125
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->API:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v2, "Register API success: store card on file"

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->clientId()Ljava/lang/String;

    move-result-object v4

    .line 127
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v7, Lcom/squareup/api/StoreCardSuccessEvent;

    iget-object v2, p0, Lcom/squareup/api/ApiAddCardOnFileController;->clock:Lcom/squareup/util/Clock;

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/api/StoreCardSuccessEvent;-><init>(Lcom/squareup/util/Clock;Ljava/lang/String;Ljava/lang/String;J)V

    invoke-interface {v0, v7}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private makeFailureResponse(Lcom/squareup/api/ApiErrorResult;)Landroid/content/Intent;
    .locals 3

    .line 142
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 143
    invoke-virtual {p1}, Lcom/squareup/api/ApiErrorResult;->name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.squareup.pos.ERROR_CODE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    iget-object v1, p0, Lcom/squareup/api/ApiAddCardOnFileController;->resources:Landroid/content/res/Resources;

    iget p1, p1, Lcom/squareup/api/ApiErrorResult;->errorDescriptionResourceId:I

    .line 145
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "com.squareup.pos.ERROR_DESCRIPTION"

    .line 144
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public createHistory()Lflow/History;
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->historyFactory:Lcom/squareup/ui/main/HistoryFactory;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v1}, Lcom/squareup/ui/main/HistoryFactory;->createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;

    move-result-object v0

    return-object v0
.end method

.method public getBundler()Lmortar/bundler/Bundler;
    .locals 2

    .line 88
    new-instance v0, Lcom/squareup/api/ApiAddCardOnFileController$1;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/api/ApiAddCardOnFileController$1;-><init>(Lcom/squareup/api/ApiAddCardOnFileController;Ljava/lang/String;)V

    return-object v0
.end method

.method public handleAddCardOnFileError(Lcom/squareup/api/ApiErrorResult;)V
    .locals 2

    .line 81
    invoke-direct {p0, p1}, Lcom/squareup/api/ApiAddCardOnFileController;->logError(Lcom/squareup/api/ApiErrorResult;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 83
    invoke-direct {p0, p1}, Lcom/squareup/api/ApiAddCardOnFileController;->makeFailureResponse(Lcom/squareup/api/ApiErrorResult;)Landroid/content/Intent;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 84
    iget-object p1, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {p1}, Lcom/squareup/api/ApiRequestController;->finish()V

    return-void
.end method

.method public isApiAddCustomerCardRequest()Z
    .locals 1

    .line 64
    iget-boolean v0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestInFlight:Z

    return v0
.end method

.method public loadAddCustomerCardController(Lcom/squareup/ui/main/HistoryFactory;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1

    const/4 v0, 0x1

    .line 53
    iput-boolean v0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestInFlight:Z

    .line 54
    iput-object p1, p0, Lcom/squareup/api/ApiAddCardOnFileController;->historyFactory:Lcom/squareup/ui/main/HistoryFactory;

    .line 55
    iget-object p1, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {p1, p2, p3, p4, p5}, Lcom/squareup/api/ApiRequestController;->startApiSession(Ljava/lang/String;Ljava/lang/String;J)V

    .line 56
    iget-object p1, p0, Lcom/squareup/api/ApiAddCardOnFileController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance p3, Lcom/squareup/api/AddCustomerCardFlowStartedEvent;

    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->clock:Lcom/squareup/util/Clock;

    invoke-direct {p3, v0, p2, p4, p5}, Lcom/squareup/api/AddCustomerCardFlowStartedEvent;-><init>(Lcom/squareup/util/Clock;Ljava/lang/String;J)V

    invoke-interface {p1, p3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public onCancel()V
    .locals 1

    .line 77
    sget-object v0, Lcom/squareup/api/ApiErrorResult;->ADD_CARD_ON_FILE_CANCELED:Lcom/squareup/api/ApiErrorResult;

    invoke-virtual {p0, v0}, Lcom/squareup/api/ApiAddCardOnFileController;->handleAddCardOnFileError(Lcom/squareup/api/ApiErrorResult;)V

    return-void
.end method

.method public onSuccess(Lcom/squareup/sdk/reader/checkout/Card;)V
    .locals 2

    .line 68
    invoke-direct {p0}, Lcom/squareup/api/ApiAddCardOnFileController;->logSuccess()V

    .line 69
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 70
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 71
    invoke-static {p1, v1}, Lcom/squareup/sdk/reader/crm/StoreCardResultParcelable;->writeStoredCardToIntent(Lcom/squareup/sdk/reader/checkout/Card;Landroid/content/Intent;)V

    const/4 p1, -0x1

    .line 72
    invoke-virtual {v0, p1, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 73
    iget-object p1, p0, Lcom/squareup/api/ApiAddCardOnFileController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {p1}, Lcom/squareup/api/ApiRequestController;->finish()V

    return-void
.end method
