.class public final Lcom/squareup/api/ClientSettingsCache_Factory;
.super Ljava/lang/Object;
.source "ClientSettingsCache_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/api/ClientSettingsCache;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final fileSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/api/ClientSettingsCache_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/api/ClientSettingsCache_Factory;->gsonProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/api/ClientSettingsCache_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/api/ClientSettingsCache_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/api/ClientSettingsCache_Factory;->fileSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/api/ClientSettingsCache_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)",
            "Lcom/squareup/api/ClientSettingsCache_Factory;"
        }
    .end annotation

    .line 48
    new-instance v6, Lcom/squareup/api/ClientSettingsCache_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/ClientSettingsCache_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Landroid/app/Application;Lcom/google/gson/Gson;Lcom/squareup/util/Clock;Lrx/Scheduler;Lrx/Scheduler;)Lcom/squareup/api/ClientSettingsCache;
    .locals 7

    .line 53
    new-instance v6, Lcom/squareup/api/ClientSettingsCache;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/ClientSettingsCache;-><init>(Landroid/app/Application;Lcom/google/gson/Gson;Lcom/squareup/util/Clock;Lrx/Scheduler;Lrx/Scheduler;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/api/ClientSettingsCache;
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/squareup/api/ClientSettingsCache_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/api/ClientSettingsCache_Factory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/gson/Gson;

    iget-object v2, p0, Lcom/squareup/api/ClientSettingsCache_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Clock;

    iget-object v3, p0, Lcom/squareup/api/ClientSettingsCache_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lrx/Scheduler;

    iget-object v4, p0, Lcom/squareup/api/ClientSettingsCache_Factory;->fileSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lrx/Scheduler;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/api/ClientSettingsCache_Factory;->newInstance(Landroid/app/Application;Lcom/google/gson/Gson;Lcom/squareup/util/Clock;Lrx/Scheduler;Lrx/Scheduler;)Lcom/squareup/api/ClientSettingsCache;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/api/ClientSettingsCache_Factory;->get()Lcom/squareup/api/ClientSettingsCache;

    move-result-object v0

    return-object v0
.end method
