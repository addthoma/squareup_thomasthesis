.class public Lcom/squareup/api/ApiTransactionState;
.super Ljava/lang/Object;
.source "ApiTransactionState.java"


# static fields
.field private static final ALLOW_SPLIT_TENDER_KEY:Ljava/lang/String; = "ALLOW_SPLIT_TENDER"

.field private static final API_VERSION_KEY:Ljava/lang/String; = "API_VERSION"

.field private static final CHARGE_REQUEST_KEY:Ljava/lang/String; = "CHARGE_REQUEST"

.field private static final DELAY_CAPTURE_KEY:Ljava/lang/String; = "DELAY_CAPTURE"

.field private static final TENDER_TYPES_KEY:Ljava/lang/String; = "TENDER_TYPES"

.field private static final TIMEOUT_KEY:Ljava/lang/String; = "TIMEOUT"


# instance fields
.field private apiTransactionParams:Lcom/squareup/api/ApiTransactionParams;

.field private chargeRequestInFlight:Z

.field private isReaderSdk:Z

.field private loaded:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-boolean p1, p0, Lcom/squareup/api/ApiTransactionState;->isReaderSdk:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/api/ApiTransactionState;)Z
    .locals 0

    .line 27
    iget-boolean p0, p0, Lcom/squareup/api/ApiTransactionState;->loaded:Z

    return p0
.end method

.method static synthetic access$002(Lcom/squareup/api/ApiTransactionState;Z)Z
    .locals 0

    .line 27
    iput-boolean p1, p0, Lcom/squareup/api/ApiTransactionState;->loaded:Z

    return p1
.end method

.method static synthetic access$100(Lcom/squareup/api/ApiTransactionState;)Z
    .locals 0

    .line 27
    iget-boolean p0, p0, Lcom/squareup/api/ApiTransactionState;->chargeRequestInFlight:Z

    return p0
.end method

.method static synthetic access$102(Lcom/squareup/api/ApiTransactionState;Z)Z
    .locals 0

    .line 27
    iput-boolean p1, p0, Lcom/squareup/api/ApiTransactionState;->chargeRequestInFlight:Z

    return p1
.end method

.method static synthetic access$200(Lcom/squareup/api/ApiTransactionState;)Lcom/squareup/api/ApiTransactionParams;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/api/ApiTransactionState;->apiTransactionParams:Lcom/squareup/api/ApiTransactionParams;

    return-object p0
.end method

.method static synthetic access$202(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/api/ApiTransactionParams;)Lcom/squareup/api/ApiTransactionParams;
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/squareup/api/ApiTransactionState;->apiTransactionParams:Lcom/squareup/api/ApiTransactionParams;

    return-object p1
.end method


# virtual methods
.method public cardNotPresentSupported()Z
    .locals 2

    .line 110
    invoke-virtual {p0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState;->apiTransactionParams:Lcom/squareup/api/ApiTransactionParams;

    iget-object v0, v0, Lcom/squareup/api/ApiTransactionParams;->tenderTypes:Ljava/util/Set;

    sget-object v1, Lcom/squareup/api/ApiTenderType;->KEYED_IN_CARD:Lcom/squareup/api/ApiTenderType;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public cardSupported()Z
    .locals 2

    .line 71
    invoke-virtual {p0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/api/ApiTransactionState;->isReaderSdk:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState;->apiTransactionParams:Lcom/squareup/api/ApiTransactionParams;

    iget-object v0, v0, Lcom/squareup/api/ApiTransactionParams;->tenderTypes:Ljava/util/Set;

    sget-object v1, Lcom/squareup/api/ApiTenderType;->POS_API_CARD_PRESENT:Lcom/squareup/api/ApiTenderType;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0

    :cond_1
    return v1
.end method

.method public customersSupported()Z
    .locals 2

    .line 101
    iget-boolean v0, p0, Lcom/squareup/api/ApiTransactionState;->isReaderSdk:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 103
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState;->apiTransactionParams:Lcom/squareup/api/ApiTransactionParams;

    iget-object v0, v0, Lcom/squareup/api/ApiTransactionParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    sget-object v1, Lcom/squareup/api/ApiVersion;->V1_1:Lcom/squareup/api/ApiVersion;

    invoke-virtual {v0, v1}, Lcom/squareup/api/ApiVersion;->isAtLeast(Lcom/squareup/api/ApiVersion;)Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public getApiVersion()Lcom/squareup/api/ApiVersion;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState;->apiTransactionParams:Lcom/squareup/api/ApiTransactionParams;

    iget-object v0, v0, Lcom/squareup/api/ApiTransactionParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    return-object v0
.end method

.method public getBundler()Lmortar/bundler/Bundler;
    .locals 2

    .line 130
    new-instance v0, Lcom/squareup/api/ApiTransactionState$1;

    const-class v1, Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/api/ApiTransactionState$1;-><init>(Lcom/squareup/api/ApiTransactionState;Ljava/lang/String;)V

    return-object v0
.end method

.method public getReceiptScreenTimeout()J
    .locals 2

    .line 114
    invoke-virtual {p0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState;->apiTransactionParams:Lcom/squareup/api/ApiTransactionParams;

    iget-wide v0, v0, Lcom/squareup/api/ApiTransactionParams;->timeout:J

    return-wide v0

    .line 115
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not a register API transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public giftCardEntrySupported()Z
    .locals 3

    .line 91
    iget-boolean v0, p0, Lcom/squareup/api/ApiTransactionState;->isReaderSdk:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 93
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState;->apiTransactionParams:Lcom/squareup/api/ApiTransactionParams;

    iget-object v0, v0, Lcom/squareup/api/ApiTransactionParams;->tenderTypes:Ljava/util/Set;

    sget-object v2, Lcom/squareup/api/ApiTenderType;->POS_API_CARD_PRESENT:Lcom/squareup/api/ApiTenderType;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState;->apiTransactionParams:Lcom/squareup/api/ApiTransactionParams;

    iget-object v0, v0, Lcom/squareup/api/ApiTransactionParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    sget-object v2, Lcom/squareup/api/ApiVersion;->V1_1:Lcom/squareup/api/ApiVersion;

    .line 95
    invoke-virtual {v0, v2}, Lcom/squareup/api/ApiVersion;->isAtLeast(Lcom/squareup/api/ApiVersion;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public hasReceiptScreenTimeout()Z
    .locals 5

    .line 121
    invoke-virtual {p0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState;->apiTransactionParams:Lcom/squareup/api/ApiTransactionParams;

    iget-wide v0, v0, Lcom/squareup/api/ApiTransactionParams;->timeout:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isApiTransactionRequest()Z
    .locals 1

    .line 126
    iget-boolean v0, p0, Lcom/squareup/api/ApiTransactionState;->chargeRequestInFlight:Z

    return v0
.end method

.method public setApiTransactionParams(Lcom/squareup/api/ApiTransactionParams;)V
    .locals 2

    .line 57
    invoke-virtual {p0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 58
    iput-object p1, p0, Lcom/squareup/api/ApiTransactionState;->apiTransactionParams:Lcom/squareup/api/ApiTransactionParams;

    .line 59
    iput-boolean v1, p0, Lcom/squareup/api/ApiTransactionState;->chargeRequestInFlight:Z

    return-void
.end method

.method public splitTenderSupported()Z
    .locals 3

    .line 79
    invoke-virtual {p0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 82
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/api/ApiTransactionState;->isReaderSdk:Z

    if-eqz v0, :cond_1

    .line 83
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState;->apiTransactionParams:Lcom/squareup/api/ApiTransactionParams;

    iget-boolean v0, v0, Lcom/squareup/api/ApiTransactionParams;->allowSplitTender:Z

    return v0

    .line 85
    :cond_1
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState;->apiTransactionParams:Lcom/squareup/api/ApiTransactionParams;

    iget-object v0, v0, Lcom/squareup/api/ApiTransactionParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    sget-object v2, Lcom/squareup/api/ApiVersion;->V1_1:Lcom/squareup/api/ApiVersion;

    invoke-virtual {v0, v2}, Lcom/squareup/api/ApiVersion;->isAtLeast(Lcom/squareup/api/ApiVersion;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState;->apiTransactionParams:Lcom/squareup/api/ApiTransactionParams;

    iget-object v0, v0, Lcom/squareup/api/ApiTransactionParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    .line 86
    invoke-virtual {v0}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result v0

    sget-object v2, Lcom/squareup/api/ApiVersion;->V3_0:Lcom/squareup/api/ApiVersion;

    invoke-virtual {v2}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result v2

    if-ge v0, v2, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public tenderTypes()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/api/ApiTenderType;",
            ">;"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState;->apiTransactionParams:Lcom/squareup/api/ApiTransactionParams;

    iget-object v0, v0, Lcom/squareup/api/ApiTransactionParams;->tenderTypes:Ljava/util/Set;

    return-object v0
.end method
