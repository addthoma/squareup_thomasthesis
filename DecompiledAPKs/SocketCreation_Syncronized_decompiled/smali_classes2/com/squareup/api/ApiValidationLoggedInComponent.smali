.class public interface abstract Lcom/squareup/api/ApiValidationLoggedInComponent;
.super Ljava/lang/Object;
.source "ApiValidationLoggedInComponent.java"


# virtual methods
.method public abstract currencyCode()Lcom/squareup/protos/common/CurrencyCode;
.end method

.method public abstract jailKeeper()Lcom/squareup/jailkeeper/JailKeeper;
.end method

.method public abstract passcodeEmployeeManagement()Lcom/squareup/permissions/PasscodeEmployeeManagement;
.end method

.method public abstract rolodexServiceHelper()Lcom/squareup/crm/RolodexServiceHelper;
.end method

.method public abstract transaction()Lcom/squareup/payment/Transaction;
.end method

.method public abstract userToken()Ljava/lang/String;
.end method
