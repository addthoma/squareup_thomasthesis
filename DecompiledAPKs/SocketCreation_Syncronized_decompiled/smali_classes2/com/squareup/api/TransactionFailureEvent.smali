.class public Lcom/squareup/api/TransactionFailureEvent;
.super Lcom/squareup/api/RegisterApiEvent;
.source "TransactionFailureEvent.java"


# instance fields
.field public final reason:Ljava/lang/String;

.field public final screen_name:Ljava/lang/String;

.field public final sequenceUuid:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "api_sequence_uuid"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;Lcom/squareup/container/ContainerTreeKey;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/ApiErrorResult;J)V
    .locals 6

    .line 26
    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->API_TRANSACTION_FAILURE:Lcom/squareup/analytics/RegisterActionName;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p4

    move-wide v4, p6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/RegisterApiEvent;-><init>(Lcom/squareup/util/Clock;Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;J)V

    .line 27
    invoke-virtual {p5}, Lcom/squareup/api/ApiErrorResult;->name()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/TransactionFailureEvent;->reason:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/squareup/api/TransactionFailureEvent;->sequenceUuid:Ljava/lang/String;

    if-eqz p2, :cond_0

    .line 30
    invoke-virtual {p2}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/TransactionFailureEvent;->screen_name:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 32
    iput-object p1, p0, Lcom/squareup/api/TransactionFailureEvent;->screen_name:Ljava/lang/String;

    :goto_0
    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Clock;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/ApiErrorResult;J)V
    .locals 8

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    .line 21
    invoke-direct/range {v0 .. v7}, Lcom/squareup/api/TransactionFailureEvent;-><init>(Lcom/squareup/util/Clock;Lcom/squareup/container/ContainerTreeKey;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/ApiErrorResult;J)V

    return-void
.end method
