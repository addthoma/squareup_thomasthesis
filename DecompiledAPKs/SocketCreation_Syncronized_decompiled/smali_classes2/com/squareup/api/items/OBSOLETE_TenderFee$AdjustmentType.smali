.class public final enum Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;
.super Ljava/lang/Enum;
.source "OBSOLETE_TenderFee.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/OBSOLETE_TenderFee;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AdjustmentType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType$ProtoAdapter_AdjustmentType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum SURCHARGE:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 197
    new-instance v0, Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "SURCHARGE"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;->SURCHARGE:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    new-array v0, v1, [Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    .line 196
    sget-object v1, Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;->SURCHARGE:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;->$VALUES:[Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    .line 199
    new-instance v0, Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType$ProtoAdapter_AdjustmentType;

    invoke-direct {v0}, Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType$ProtoAdapter_AdjustmentType;-><init>()V

    sput-object v0, Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 203
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 204
    iput p3, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 212
    :cond_0
    sget-object p0, Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;->SURCHARGE:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;
    .locals 1

    .line 196
    const-class v0, Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;
    .locals 1

    .line 196
    sget-object v0, Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;->$VALUES:[Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    invoke-virtual {v0}, [Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 219
    iget v0, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;->value:I

    return v0
.end method
