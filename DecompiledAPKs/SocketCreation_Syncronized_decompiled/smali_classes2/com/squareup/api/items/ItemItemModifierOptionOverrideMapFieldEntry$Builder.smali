.class public final Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ItemItemModifierOptionOverrideMapFieldEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;",
        "Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public key:Lcom/squareup/api/sync/ObjectId;

.field public value:Lcom/squareup/api/items/ItemModifierOptionOverride;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 96
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;
    .locals 4

    .line 111
    new-instance v0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->key:Lcom/squareup/api/sync/ObjectId;

    iget-object v2, p0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;-><init>(Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/ItemModifierOptionOverride;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 91
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->build()Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;

    move-result-object v0

    return-object v0
.end method

.method public key(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->key:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public value(Lcom/squareup/api/items/ItemModifierOptionOverride;)Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    return-object p0
.end method
