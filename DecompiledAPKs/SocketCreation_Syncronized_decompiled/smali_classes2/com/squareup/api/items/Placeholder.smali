.class public final Lcom/squareup/api/items/Placeholder;
.super Lcom/squareup/wire/Message;
.source "Placeholder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Placeholder$ProtoAdapter_Placeholder;,
        Lcom/squareup/api/items/Placeholder$PlaceholderType;,
        Lcom/squareup/api/items/Placeholder$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/Placeholder;",
        "Lcom/squareup/api/items/Placeholder$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Placeholder;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PLACEHOLDER_TYPE:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field private static final serialVersionUID:J


# instance fields
.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final placeholder_type:Lcom/squareup/api/items/Placeholder$PlaceholderType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Placeholder$PlaceholderType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final tag_membership:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/api/items/Placeholder$ProtoAdapter_Placeholder;

    invoke-direct {v0}, Lcom/squareup/api/items/Placeholder$ProtoAdapter_Placeholder;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Placeholder;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 31
    sget-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->UNKNOWN:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    sput-object v0, Lcom/squareup/api/items/Placeholder;->DEFAULT_PLACEHOLDER_TYPE:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/Placeholder$PlaceholderType;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)V"
        }
    .end annotation

    .line 66
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/items/Placeholder;-><init>(Ljava/lang/String;Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/Placeholder$PlaceholderType;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 71
    sget-object v0, Lcom/squareup/api/items/Placeholder;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 72
    iput-object p1, p0, Lcom/squareup/api/items/Placeholder;->id:Ljava/lang/String;

    .line 73
    iput-object p2, p0, Lcom/squareup/api/items/Placeholder;->placeholder_type:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 74
    iput-object p3, p0, Lcom/squareup/api/items/Placeholder;->name:Ljava/lang/String;

    const-string p1, "tag_membership"

    .line 75
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/items/Placeholder;->tag_membership:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 92
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/Placeholder;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 93
    :cond_1
    check-cast p1, Lcom/squareup/api/items/Placeholder;

    .line 94
    invoke-virtual {p0}, Lcom/squareup/api/items/Placeholder;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/Placeholder;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Placeholder;->id:Ljava/lang/String;

    .line 95
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->placeholder_type:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    iget-object v3, p1, Lcom/squareup/api/items/Placeholder;->placeholder_type:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 96
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Placeholder;->name:Ljava/lang/String;

    .line 97
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->tag_membership:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/api/items/Placeholder;->tag_membership:Ljava/util/List;

    .line 98
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 103
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 105
    invoke-virtual {p0}, Lcom/squareup/api/items/Placeholder;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 106
    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 107
    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->placeholder_type:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/items/Placeholder$PlaceholderType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->tag_membership:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/Placeholder$Builder;
    .locals 2

    .line 80
    new-instance v0, Lcom/squareup/api/items/Placeholder$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Placeholder$Builder;-><init>()V

    .line 81
    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Placeholder$Builder;->id:Ljava/lang/String;

    .line 82
    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->placeholder_type:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    iput-object v1, v0, Lcom/squareup/api/items/Placeholder$Builder;->placeholder_type:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 83
    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Placeholder$Builder;->name:Ljava/lang/String;

    .line 84
    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->tag_membership:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/Placeholder$Builder;->tag_membership:Ljava/util/List;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/api/items/Placeholder;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Placeholder$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/api/items/Placeholder;->newBuilder()Lcom/squareup/api/items/Placeholder$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 118
    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->placeholder_type:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    if-eqz v1, :cond_1

    const-string v1, ", placeholder_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->placeholder_type:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 120
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->tag_membership:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", tag_membership="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Placeholder;->tag_membership:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Placeholder{"

    .line 122
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
