.class public final Lcom/squareup/api/items/OBSOLETE_TenderFee;
.super Lcom/squareup/wire/Message;
.source "OBSOLETE_TenderFee.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/OBSOLETE_TenderFee$ProtoAdapter_OBSOLETE_TenderFee;,
        Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;,
        Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/OBSOLETE_TenderFee;",
        "Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/OBSOLETE_TenderFee;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ADJUSTMENT_TYPE:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

.field public static final DEFAULT_CALCULATION_PHASE:Lcom/squareup/api/items/CalculationPhase;

.field public static final DEFAULT_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final adjustment_type:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.OBSOLETE_TenderFee$AdjustmentType#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final calculation_phase:Lcom/squareup/api/items/CalculationPhase;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.CalculationPhase#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final percentage:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/api/items/OBSOLETE_TenderFee$ProtoAdapter_OBSOLETE_TenderFee;

    invoke-direct {v0}, Lcom/squareup/api/items/OBSOLETE_TenderFee$ProtoAdapter_OBSOLETE_TenderFee;-><init>()V

    sput-object v0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 29
    sget-object v0, Lcom/squareup/api/items/CalculationPhase;->SURCHARGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

    sput-object v0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->DEFAULT_CALCULATION_PHASE:Lcom/squareup/api/items/CalculationPhase;

    .line 33
    sget-object v0, Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;->SURCHARGE:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    sput-object v0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->DEFAULT_ADJUSTMENT_TYPE:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    const/4 v0, 0x1

    .line 35
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->DEFAULT_ENABLED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/items/CalculationPhase;Ljava/lang/String;Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;Ljava/lang/Boolean;)V
    .locals 7

    .line 78
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/api/items/OBSOLETE_TenderFee;-><init>(Ljava/lang/String;Lcom/squareup/api/items/CalculationPhase;Ljava/lang/String;Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/items/CalculationPhase;Ljava/lang/String;Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 83
    sget-object v0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 84
    iput-object p1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->id:Ljava/lang/String;

    .line 85
    iput-object p2, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    .line 86
    iput-object p3, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->percentage:Ljava/lang/String;

    .line 87
    iput-object p4, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->adjustment_type:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    .line 88
    iput-object p5, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->enabled:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 106
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/OBSOLETE_TenderFee;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 107
    :cond_1
    check-cast p1, Lcom/squareup/api/items/OBSOLETE_TenderFee;

    .line 108
    invoke-virtual {p0}, Lcom/squareup/api/items/OBSOLETE_TenderFee;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/OBSOLETE_TenderFee;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/OBSOLETE_TenderFee;->id:Ljava/lang/String;

    .line 109
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    iget-object v3, p1, Lcom/squareup/api/items/OBSOLETE_TenderFee;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    .line 110
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->percentage:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/OBSOLETE_TenderFee;->percentage:Ljava/lang/String;

    .line 111
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->adjustment_type:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    iget-object v3, p1, Lcom/squareup/api/items/OBSOLETE_TenderFee;->adjustment_type:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    .line 112
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->enabled:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/api/items/OBSOLETE_TenderFee;->enabled:Ljava/lang/Boolean;

    .line 113
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 118
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 120
    invoke-virtual {p0}, Lcom/squareup/api/items/OBSOLETE_TenderFee;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 121
    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 122
    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/items/CalculationPhase;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->adjustment_type:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 126
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;
    .locals 2

    .line 93
    new-instance v0, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;-><init>()V

    .line 94
    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->id:Ljava/lang/String;

    .line 95
    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    iput-object v1, v0, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    .line 96
    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->percentage:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->percentage:Ljava/lang/String;

    .line 97
    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->adjustment_type:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    iput-object v1, v0, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->adjustment_type:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    .line 98
    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->enabled:Ljava/lang/Boolean;

    .line 99
    invoke-virtual {p0}, Lcom/squareup/api/items/OBSOLETE_TenderFee;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/api/items/OBSOLETE_TenderFee;->newBuilder()Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 134
    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    if-eqz v1, :cond_1

    const-string v1, ", calculation_phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 136
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->percentage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->adjustment_type:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    if-eqz v1, :cond_3

    const-string v1, ", adjustment_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->adjustment_type:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 138
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "OBSOLETE_TenderFee{"

    .line 139
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
