.class public final Lcom/squareup/api/items/Menu$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Menu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Menu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/Menu;",
        "Lcom/squareup/api/items/Menu$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public root_tag:Lcom/squareup/api/sync/ObjectId;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 130
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/Menu;
    .locals 7

    .line 159
    new-instance v6, Lcom/squareup/api/items/Menu;

    iget-object v1, p0, Lcom/squareup/api/items/Menu$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/Menu$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/Menu$Builder;->root_tag:Lcom/squareup/api/sync/ObjectId;

    iget-object v4, p0, Lcom/squareup/api/items/Menu$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/items/Menu;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/MerchantCatalogObjectReference;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/api/items/Menu$Builder;->build()Lcom/squareup/api/items/Menu;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_reference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/api/items/Menu$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/api/items/Menu$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/Menu$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/api/items/Menu$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/Menu$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/api/items/Menu$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public root_tag(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/Menu$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/api/items/Menu$Builder;->root_tag:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method
