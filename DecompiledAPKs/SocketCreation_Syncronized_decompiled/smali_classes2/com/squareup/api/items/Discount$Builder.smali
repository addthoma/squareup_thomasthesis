.class public final Lcom/squareup/api/items/Discount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Discount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Discount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/Discount;",
        "Lcom/squareup/api/items/Discount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount:Lcom/squareup/protos/common/dinero/Money;

.field public application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

.field public catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

.field public color:Ljava/lang/String;

.field public comp_ordinal:Ljava/lang/Integer;

.field public discount_type:Lcom/squareup/api/items/Discount$DiscountType;

.field public id:Ljava/lang/String;

.field public maximum_amount:Lcom/squareup/protos/common/dinero/Money;

.field public modify_tax_basis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

.field public name:Ljava/lang/String;

.field public percentage:Ljava/lang/String;

.field public pin_required:Ljava/lang/Boolean;

.field public v2_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 315
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/Discount$Builder;
    .locals 0

    .line 353
    iput-object p1, p0, Lcom/squareup/api/items/Discount$Builder;->amount:Lcom/squareup/protos/common/dinero/Money;

    return-object p0
.end method

.method public application_method(Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/api/items/Discount$Builder;
    .locals 0

    .line 378
    iput-object p1, p0, Lcom/squareup/api/items/Discount$Builder;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    return-object p0
.end method

.method public build()Lcom/squareup/api/items/Discount;
    .locals 17

    move-object/from16 v0, p0

    .line 426
    new-instance v16, Lcom/squareup/api/items/Discount;

    iget-object v2, v0, Lcom/squareup/api/items/Discount$Builder;->id:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/api/items/Discount$Builder;->color:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/api/items/Discount$Builder;->name:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/api/items/Discount$Builder;->percentage:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/api/items/Discount$Builder;->amount:Lcom/squareup/protos/common/dinero/Money;

    iget-object v7, v0, Lcom/squareup/api/items/Discount$Builder;->pin_required:Ljava/lang/Boolean;

    iget-object v8, v0, Lcom/squareup/api/items/Discount$Builder;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    iget-object v9, v0, Lcom/squareup/api/items/Discount$Builder;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    iget-object v10, v0, Lcom/squareup/api/items/Discount$Builder;->comp_ordinal:Ljava/lang/Integer;

    iget-object v11, v0, Lcom/squareup/api/items/Discount$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v12, v0, Lcom/squareup/api/items/Discount$Builder;->v2_id:Ljava/lang/String;

    iget-object v13, v0, Lcom/squareup/api/items/Discount$Builder;->modify_tax_basis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    iget-object v14, v0, Lcom/squareup/api/items/Discount$Builder;->maximum_amount:Lcom/squareup/protos/common/dinero/Money;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v15

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/api/items/Discount;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/dinero/Money;Ljava/lang/Boolean;Lcom/squareup/api/items/Discount$DiscountType;Lcom/squareup/api/items/Discount$ApplicationMethod;Ljava/lang/Integer;Lcom/squareup/api/items/MerchantCatalogObjectReference;Ljava/lang/String;Lcom/squareup/api/items/Discount$ModifyTaxBasis;Lcom/squareup/protos/common/dinero/Money;Lokio/ByteString;)V

    return-object v16
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 288
    invoke-virtual {p0}, Lcom/squareup/api/items/Discount$Builder;->build()Lcom/squareup/api/items/Discount;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_reference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/api/items/Discount$Builder;
    .locals 0

    .line 393
    iput-object p1, p0, Lcom/squareup/api/items/Discount$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    return-object p0
.end method

.method public color(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;
    .locals 0

    .line 324
    iput-object p1, p0, Lcom/squareup/api/items/Discount$Builder;->color:Ljava/lang/String;

    return-object p0
.end method

.method public comp_ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/Discount$Builder;
    .locals 0

    .line 387
    iput-object p1, p0, Lcom/squareup/api/items/Discount$Builder;->comp_ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public discount_type(Lcom/squareup/api/items/Discount$DiscountType;)Lcom/squareup/api/items/Discount$Builder;
    .locals 0

    .line 370
    iput-object p1, p0, Lcom/squareup/api/items/Discount$Builder;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;
    .locals 0

    .line 319
    iput-object p1, p0, Lcom/squareup/api/items/Discount$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public maximum_amount(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/Discount$Builder;
    .locals 0

    .line 420
    iput-object p1, p0, Lcom/squareup/api/items/Discount$Builder;->maximum_amount:Lcom/squareup/protos/common/dinero/Money;

    return-object p0
.end method

.method public modify_tax_basis(Lcom/squareup/api/items/Discount$ModifyTaxBasis;)Lcom/squareup/api/items/Discount$Builder;
    .locals 0

    .line 409
    iput-object p1, p0, Lcom/squareup/api/items/Discount$Builder;->modify_tax_basis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;
    .locals 0

    .line 329
    iput-object p1, p0, Lcom/squareup/api/items/Discount$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;
    .locals 0

    .line 341
    iput-object p1, p0, Lcom/squareup/api/items/Discount$Builder;->percentage:Ljava/lang/String;

    return-object p0
.end method

.method public pin_required(Ljava/lang/Boolean;)Lcom/squareup/api/items/Discount$Builder;
    .locals 0

    .line 361
    iput-object p1, p0, Lcom/squareup/api/items/Discount$Builder;->pin_required:Ljava/lang/Boolean;

    return-object p0
.end method

.method public v2_id(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;
    .locals 0

    .line 404
    iput-object p1, p0, Lcom/squareup/api/items/Discount$Builder;->v2_id:Ljava/lang/String;

    return-object p0
.end method
