.class final Lcom/squareup/api/items/TaxRule$Condition$ProtoAdapter_Condition;
.super Lcom/squareup/wire/ProtoAdapter;
.source "TaxRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/TaxRule$Condition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Condition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/api/items/TaxRule$Condition;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 472
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/api/items/TaxRule$Condition;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/items/TaxRule$Condition;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 495
    new-instance v0, Lcom/squareup/api/items/TaxRule$Condition$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/TaxRule$Condition$Builder;-><init>()V

    .line 496
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 497
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 504
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 502
    :cond_0
    sget-object v3, Lcom/squareup/protos/common/dinero/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/dinero/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->max_item_price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/TaxRule$Condition$Builder;

    goto :goto_0

    .line 501
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/dinero/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/dinero/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->min_item_price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/TaxRule$Condition$Builder;

    goto :goto_0

    .line 500
    :cond_2
    sget-object v3, Lcom/squareup/protos/common/dinero/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/dinero/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->max_total_amount(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/TaxRule$Condition$Builder;

    goto :goto_0

    .line 499
    :cond_3
    sget-object v3, Lcom/squareup/api/items/ObjectPredicate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ObjectPredicate;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->dining_option_predicate(Lcom/squareup/api/items/ObjectPredicate;)Lcom/squareup/api/items/TaxRule$Condition$Builder;

    goto :goto_0

    .line 508
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 509
    invoke-virtual {v0}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->build()Lcom/squareup/api/items/TaxRule$Condition;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 470
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/TaxRule$Condition$ProtoAdapter_Condition;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/items/TaxRule$Condition;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/items/TaxRule$Condition;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 486
    sget-object v0, Lcom/squareup/api/items/ObjectPredicate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/TaxRule$Condition;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 487
    sget-object v0, Lcom/squareup/protos/common/dinero/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/TaxRule$Condition;->max_total_amount:Lcom/squareup/protos/common/dinero/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 488
    sget-object v0, Lcom/squareup/protos/common/dinero/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/TaxRule$Condition;->min_item_price:Lcom/squareup/protos/common/dinero/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 489
    sget-object v0, Lcom/squareup/protos/common/dinero/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/TaxRule$Condition;->max_item_price:Lcom/squareup/protos/common/dinero/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 490
    invoke-virtual {p2}, Lcom/squareup/api/items/TaxRule$Condition;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 470
    check-cast p2, Lcom/squareup/api/items/TaxRule$Condition;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/items/TaxRule$Condition$ProtoAdapter_Condition;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/items/TaxRule$Condition;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/api/items/TaxRule$Condition;)I
    .locals 4

    .line 477
    sget-object v0, Lcom/squareup/api/items/ObjectPredicate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/items/TaxRule$Condition;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/dinero/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/TaxRule$Condition;->max_total_amount:Lcom/squareup/protos/common/dinero/Money;

    const/4 v3, 0x2

    .line 478
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/dinero/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/TaxRule$Condition;->min_item_price:Lcom/squareup/protos/common/dinero/Money;

    const/4 v3, 0x3

    .line 479
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/dinero/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/TaxRule$Condition;->max_item_price:Lcom/squareup/protos/common/dinero/Money;

    const/4 v3, 0x4

    .line 480
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 481
    invoke-virtual {p1}, Lcom/squareup/api/items/TaxRule$Condition;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 470
    check-cast p1, Lcom/squareup/api/items/TaxRule$Condition;

    invoke-virtual {p0, p1}, Lcom/squareup/api/items/TaxRule$Condition$ProtoAdapter_Condition;->encodedSize(Lcom/squareup/api/items/TaxRule$Condition;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/api/items/TaxRule$Condition;)Lcom/squareup/api/items/TaxRule$Condition;
    .locals 2

    .line 514
    invoke-virtual {p1}, Lcom/squareup/api/items/TaxRule$Condition;->newBuilder()Lcom/squareup/api/items/TaxRule$Condition$Builder;

    move-result-object p1

    .line 515
    iget-object v0, p1, Lcom/squareup/api/items/TaxRule$Condition$Builder;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/api/items/ObjectPredicate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/items/TaxRule$Condition$Builder;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ObjectPredicate;

    iput-object v0, p1, Lcom/squareup/api/items/TaxRule$Condition$Builder;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    .line 516
    :cond_0
    iget-object v0, p1, Lcom/squareup/api/items/TaxRule$Condition$Builder;->max_total_amount:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/dinero/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/items/TaxRule$Condition$Builder;->max_total_amount:Lcom/squareup/protos/common/dinero/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/dinero/Money;

    iput-object v0, p1, Lcom/squareup/api/items/TaxRule$Condition$Builder;->max_total_amount:Lcom/squareup/protos/common/dinero/Money;

    .line 517
    :cond_1
    iget-object v0, p1, Lcom/squareup/api/items/TaxRule$Condition$Builder;->min_item_price:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/dinero/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/items/TaxRule$Condition$Builder;->min_item_price:Lcom/squareup/protos/common/dinero/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/dinero/Money;

    iput-object v0, p1, Lcom/squareup/api/items/TaxRule$Condition$Builder;->min_item_price:Lcom/squareup/protos/common/dinero/Money;

    .line 518
    :cond_2
    iget-object v0, p1, Lcom/squareup/api/items/TaxRule$Condition$Builder;->max_item_price:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/common/dinero/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/items/TaxRule$Condition$Builder;->max_item_price:Lcom/squareup/protos/common/dinero/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/dinero/Money;

    iput-object v0, p1, Lcom/squareup/api/items/TaxRule$Condition$Builder;->max_item_price:Lcom/squareup/protos/common/dinero/Money;

    .line 519
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 520
    invoke-virtual {p1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->build()Lcom/squareup/api/items/TaxRule$Condition;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 470
    check-cast p1, Lcom/squareup/api/items/TaxRule$Condition;

    invoke-virtual {p0, p1}, Lcom/squareup/api/items/TaxRule$Condition$ProtoAdapter_Condition;->redact(Lcom/squareup/api/items/TaxRule$Condition;)Lcom/squareup/api/items/TaxRule$Condition;

    move-result-object p1

    return-object p1
.end method
