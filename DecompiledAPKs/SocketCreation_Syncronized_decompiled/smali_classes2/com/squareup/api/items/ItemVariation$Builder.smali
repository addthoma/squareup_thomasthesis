.class public final Lcom/squareup/api/items/ItemVariation$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ItemVariation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/ItemVariation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/ItemVariation;",
        "Lcom/squareup/api/items/ItemVariation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public available_on_online_booking_site:Ljava/lang/Boolean;

.field public buyer_facing_name:Ljava/lang/String;

.field public catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

.field public custom_attribute_values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;",
            ">;"
        }
    .end annotation
.end field

.field public employee_tokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public gtin:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public intermissions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;"
        }
    .end annotation
.end field

.field public inventory_alert_threshold:Ljava/lang/Long;

.field public inventory_alert_type:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

.field public item:Lcom/squareup/api/sync/ObjectId;

.field public item_option_values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemOptionValueForItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field public measurement_unit_token:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

.field public ordinal:Ljava/lang/Integer;

.field public price:Lcom/squareup/protos/common/dinero/Money;

.field public price_description:Ljava/lang/String;

.field public pricing_type:Lcom/squareup/api/items/PricingType;

.field public resource_tokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public service_duration:Ljava/lang/Long;

.field public sku:Ljava/lang/String;

.field public track_inventory:Ljava/lang/Boolean;

.field public transition_time:Ljava/lang/Long;

.field public user_data:Ljava/lang/String;

.field public v2_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 505
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 506
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/ItemVariation$Builder;->employee_tokens:Ljava/util/List;

    .line 507
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/ItemVariation$Builder;->item_option_values:Ljava/util/List;

    .line 508
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/ItemVariation$Builder;->intermissions:Ljava/util/List;

    .line 509
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/ItemVariation$Builder;->custom_attribute_values:Ljava/util/List;

    .line 510
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/ItemVariation$Builder;->resource_tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public available_on_online_booking_site(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 635
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->available_on_online_booking_site:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/api/items/ItemVariation;
    .locals 2

    .line 711
    new-instance v0, Lcom/squareup/api/items/ItemVariation;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/api/items/ItemVariation;-><init>(Lcom/squareup/api/items/ItemVariation$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 452
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemVariation$Builder;->build()Lcom/squareup/api/items/ItemVariation;

    move-result-object v0

    return-object v0
.end method

.method public buyer_facing_name(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 527
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->buyer_facing_name:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_object_reference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 650
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    return-object p0
.end method

.method public custom_attribute_values(Ljava/util/List;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;",
            ">;)",
            "Lcom/squareup/api/items/ItemVariation$Builder;"
        }
    .end annotation

    .line 695
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 696
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->custom_attribute_values:Ljava/util/List;

    return-object p0
.end method

.method public employee_tokens(Ljava/util/List;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/api/items/ItemVariation$Builder;"
        }
    .end annotation

    .line 643
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 644
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->employee_tokens:Ljava/util/List;

    return-object p0
.end method

.method public gtin(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 540
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->gtin:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 514
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public intermissions(Ljava/util/List;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;)",
            "Lcom/squareup/api/items/ItemVariation$Builder;"
        }
    .end annotation

    .line 685
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 686
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->intermissions:Ljava/util/List;

    return-object p0
.end method

.method public inventory_alert_threshold(Ljava/lang/Long;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 573
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->inventory_alert_threshold:Ljava/lang/Long;

    return-object p0
.end method

.method public inventory_alert_type(Lcom/squareup/api/items/ItemVariation$InventoryAlertType;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 568
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->inventory_alert_type:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

    return-object p0
.end method

.method public item(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 563
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public item_option_values(Ljava/util/List;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemOptionValueForItemVariation;",
            ">;)",
            "Lcom/squareup/api/items/ItemVariation$Builder;"
        }
    .end annotation

    .line 676
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 677
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->item_option_values:Ljava/util/List;

    return-object p0
.end method

.method public measurement_unit_token(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 658
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->measurement_unit_token:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 519
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public no_show_fee_money(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 625
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 558
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 545
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->price:Lcom/squareup/protos/common/dinero/Money;

    return-object p0
.end method

.method public price_description(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 607
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->price_description:Ljava/lang/String;

    return-object p0
.end method

.method public pricing_type(Lcom/squareup/api/items/PricingType;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 553
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->pricing_type:Lcom/squareup/api/items/PricingType;

    return-object p0
.end method

.method public resource_tokens(Ljava/util/List;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/api/items/ItemVariation$Builder;"
        }
    .end annotation

    .line 704
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 705
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->resource_tokens:Ljava/util/List;

    return-object p0
.end method

.method public service_duration(Ljava/lang/Long;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 598
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->service_duration:Ljava/lang/Long;

    return-object p0
.end method

.method public sku(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 532
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->sku:Ljava/lang/String;

    return-object p0
.end method

.method public track_inventory(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 581
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->track_inventory:Ljava/lang/Boolean;

    return-object p0
.end method

.method public transition_time(Ljava/lang/Long;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 616
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->transition_time:Ljava/lang/Long;

    return-object p0
.end method

.method public user_data(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 589
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->user_data:Ljava/lang/String;

    return-object p0
.end method

.method public v2_id(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 0

    .line 668
    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation$Builder;->v2_id:Ljava/lang/String;

    return-object p0
.end method
