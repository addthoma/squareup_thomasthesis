.class public final Lcom/squareup/api/items/ProductSet;
.super Lcom/squareup/wire/Message;
.source "ProductSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/ProductSet$ProtoAdapter_ProductSet;,
        Lcom/squareup/api/items/ProductSet$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/ProductSet;",
        "Lcom/squareup/api/items/ProductSet$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/ProductSet;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALL_PRODUCTS:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOM_AMOUNTS:Ljava/lang/Boolean;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_QUANTITY_EXACT:Ljava/lang/Integer;

.field public static final DEFAULT_QUANTITY_MAX:Ljava/lang/Integer;

.field public static final DEFAULT_QUANTITY_MIN:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final all_products:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final custom_amounts:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final products_all:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation
.end field

.field public final products_any:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation
.end field

.field public final quantity_exact:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x5
    .end annotation
.end field

.field public final quantity_max:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x7
    .end annotation
.end field

.field public final quantity_min:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 24
    new-instance v0, Lcom/squareup/api/items/ProductSet$ProtoAdapter_ProductSet;

    invoke-direct {v0}, Lcom/squareup/api/items/ProductSet$ProtoAdapter_ProductSet;-><init>()V

    sput-object v0, Lcom/squareup/api/items/ProductSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 32
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/ProductSet;->DEFAULT_QUANTITY_EXACT:Ljava/lang/Integer;

    .line 34
    sput-object v0, Lcom/squareup/api/items/ProductSet;->DEFAULT_QUANTITY_MIN:Ljava/lang/Integer;

    .line 36
    sput-object v0, Lcom/squareup/api/items/ProductSet;->DEFAULT_QUANTITY_MAX:Ljava/lang/Integer;

    .line 38
    sput-object v1, Lcom/squareup/api/items/ProductSet;->DEFAULT_ALL_PRODUCTS:Ljava/lang/Boolean;

    .line 40
    sput-object v1, Lcom/squareup/api/items/ProductSet;->DEFAULT_CUSTOM_AMOUNTS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 138
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/api/items/ProductSet;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 145
    sget-object v0, Lcom/squareup/api/items/ProductSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 146
    iput-object p1, p0, Lcom/squareup/api/items/ProductSet;->id:Ljava/lang/String;

    .line 147
    iput-object p2, p0, Lcom/squareup/api/items/ProductSet;->name:Ljava/lang/String;

    const-string p1, "products_any"

    .line 148
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/items/ProductSet;->products_any:Ljava/util/List;

    const-string p1, "products_all"

    .line 149
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/items/ProductSet;->products_all:Ljava/util/List;

    .line 150
    iput-object p5, p0, Lcom/squareup/api/items/ProductSet;->quantity_exact:Ljava/lang/Integer;

    .line 151
    iput-object p6, p0, Lcom/squareup/api/items/ProductSet;->quantity_min:Ljava/lang/Integer;

    .line 152
    iput-object p7, p0, Lcom/squareup/api/items/ProductSet;->quantity_max:Ljava/lang/Integer;

    .line 153
    iput-object p8, p0, Lcom/squareup/api/items/ProductSet;->all_products:Ljava/lang/Boolean;

    .line 154
    iput-object p9, p0, Lcom/squareup/api/items/ProductSet;->custom_amounts:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 176
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/ProductSet;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 177
    :cond_1
    check-cast p1, Lcom/squareup/api/items/ProductSet;

    .line 178
    invoke-virtual {p0}, Lcom/squareup/api/items/ProductSet;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/ProductSet;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/ProductSet;->id:Ljava/lang/String;

    .line 179
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/ProductSet;->name:Ljava/lang/String;

    .line 180
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->products_any:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/api/items/ProductSet;->products_any:Ljava/util/List;

    .line 181
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->products_all:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/api/items/ProductSet;->products_all:Ljava/util/List;

    .line 182
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->quantity_exact:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/ProductSet;->quantity_exact:Ljava/lang/Integer;

    .line 183
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->quantity_min:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/ProductSet;->quantity_min:Ljava/lang/Integer;

    .line 184
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->quantity_max:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/ProductSet;->quantity_max:Ljava/lang/Integer;

    .line 185
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->all_products:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/ProductSet;->all_products:Ljava/lang/Boolean;

    .line 186
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->custom_amounts:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/api/items/ProductSet;->custom_amounts:Ljava/lang/Boolean;

    .line 187
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 192
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 194
    invoke-virtual {p0}, Lcom/squareup/api/items/ProductSet;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 195
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 196
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 197
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->products_any:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 198
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->products_all:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 199
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->quantity_exact:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 200
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->quantity_min:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 201
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->quantity_max:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 202
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->all_products:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 203
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->custom_amounts:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 204
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/ProductSet$Builder;
    .locals 2

    .line 159
    new-instance v0, Lcom/squareup/api/items/ProductSet$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ProductSet$Builder;-><init>()V

    .line 160
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ProductSet$Builder;->id:Ljava/lang/String;

    .line 161
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ProductSet$Builder;->name:Ljava/lang/String;

    .line 162
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->products_any:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/ProductSet$Builder;->products_any:Ljava/util/List;

    .line 163
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->products_all:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/ProductSet$Builder;->products_all:Ljava/util/List;

    .line 164
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->quantity_exact:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/ProductSet$Builder;->quantity_exact:Ljava/lang/Integer;

    .line 165
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->quantity_min:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/ProductSet$Builder;->quantity_min:Ljava/lang/Integer;

    .line 166
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->quantity_max:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/ProductSet$Builder;->quantity_max:Ljava/lang/Integer;

    .line 167
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->all_products:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/ProductSet$Builder;->all_products:Ljava/lang/Boolean;

    .line 168
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->custom_amounts:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/ProductSet$Builder;->custom_amounts:Ljava/lang/Boolean;

    .line 169
    invoke-virtual {p0}, Lcom/squareup/api/items/ProductSet;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ProductSet$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/api/items/ProductSet;->newBuilder()Lcom/squareup/api/items/ProductSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 211
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 212
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->products_any:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", products_any="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->products_any:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 215
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->products_all:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", products_all="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->products_all:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 216
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->quantity_exact:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", quantity_exact="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->quantity_exact:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 217
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->quantity_min:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    const-string v1, ", quantity_min="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->quantity_min:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 218
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->quantity_max:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    const-string v1, ", quantity_max="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->quantity_max:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 219
    :cond_6
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->all_products:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", all_products="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->all_products:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 220
    :cond_7
    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->custom_amounts:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", custom_amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet;->custom_amounts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ProductSet{"

    .line 221
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
