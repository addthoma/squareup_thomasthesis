.class public final Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;
.super Lcom/squareup/wire/Message;
.source "ItemItemModifierOptionOverrideMapFieldEntry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$ProtoAdapter_ItemItemModifierOptionOverrideMapFieldEntry;,
        Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;",
        "Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final key:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final value:Lcom/squareup/api/items/ItemModifierOptionOverride;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemModifierOptionOverride#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$ProtoAdapter_ItemItemModifierOptionOverrideMapFieldEntry;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$ProtoAdapter_ItemItemModifierOptionOverrideMapFieldEntry;-><init>()V

    sput-object v0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/ItemModifierOptionOverride;)V
    .locals 1

    .line 42
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;-><init>(Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/ItemModifierOptionOverride;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/ItemModifierOptionOverride;Lokio/ByteString;)V
    .locals 1

    .line 47
    sget-object v0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 48
    iput-object p1, p0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->key:Lcom/squareup/api/sync/ObjectId;

    .line 49
    iput-object p2, p0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 64
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 65
    :cond_1
    check-cast p1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;

    .line 66
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->key:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->key:Lcom/squareup/api/sync/ObjectId;

    .line 67
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    iget-object p1, p1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    .line 68
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 73
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 75
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 76
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->key:Lcom/squareup/api/sync/ObjectId;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 77
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemModifierOptionOverride;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 78
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;
    .locals 2

    .line 54
    new-instance v0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;-><init>()V

    .line 55
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->key:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->key:Lcom/squareup/api/sync/ObjectId;

    .line 56
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    iput-object v1, v0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    .line 57
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->newBuilder()Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->key:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_0

    const-string v1, ", key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->key:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 87
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    if-eqz v1, :cond_1

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ItemItemModifierOptionOverrideMapFieldEntry{"

    .line 88
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
