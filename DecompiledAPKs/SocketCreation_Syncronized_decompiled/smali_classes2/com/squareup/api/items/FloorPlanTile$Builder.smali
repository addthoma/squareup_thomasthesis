.class public final Lcom/squareup/api/items/FloorPlanTile$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FloorPlanTile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/FloorPlanTile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/FloorPlanTile;",
        "Lcom/squareup/api/items/FloorPlanTile$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public color:Ljava/lang/String;

.field public floor_plan:Lcom/squareup/api/sync/ObjectId;

.field public height:Ljava/lang/Integer;

.field public id:Ljava/lang/String;

.field public object:Lcom/squareup/api/sync/ObjectId;

.field public position:Lcom/squareup/api/items/Point;

.field public rotation_angle:Ljava/lang/Integer;

.field public shape:Lcom/squareup/api/items/FloorPlanTile$Shape;

.field public width:Ljava/lang/Integer;

.field public z_order:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 250
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/FloorPlanTile;
    .locals 13

    .line 333
    new-instance v12, Lcom/squareup/api/items/FloorPlanTile;

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->object:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->floor_plan:Lcom/squareup/api/sync/ObjectId;

    iget-object v4, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->width:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->height:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->position:Lcom/squareup/api/items/Point;

    iget-object v7, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->rotation_angle:Ljava/lang/Integer;

    iget-object v8, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->shape:Lcom/squareup/api/items/FloorPlanTile$Shape;

    iget-object v9, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->z_order:Ljava/lang/Integer;

    iget-object v10, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->color:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/api/items/FloorPlanTile;-><init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/sync/ObjectId;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/api/items/Point;Ljava/lang/Integer;Lcom/squareup/api/items/FloorPlanTile$Shape;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 229
    invoke-virtual {p0}, Lcom/squareup/api/items/FloorPlanTile$Builder;->build()Lcom/squareup/api/items/FloorPlanTile;

    move-result-object v0

    return-object v0
.end method

.method public color(Ljava/lang/String;)Lcom/squareup/api/items/FloorPlanTile$Builder;
    .locals 0

    .line 327
    iput-object p1, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->color:Ljava/lang/String;

    return-object p0
.end method

.method public floor_plan(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/FloorPlanTile$Builder;
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->floor_plan:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public height(Ljava/lang/Integer;)Lcom/squareup/api/items/FloorPlanTile$Builder;
    .locals 0

    .line 285
    iput-object p1, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->height:Ljava/lang/Integer;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/FloorPlanTile$Builder;
    .locals 0

    .line 254
    iput-object p1, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public object(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/FloorPlanTile$Builder;
    .locals 0

    .line 263
    iput-object p1, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->object:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public position(Lcom/squareup/api/items/Point;)Lcom/squareup/api/items/FloorPlanTile$Builder;
    .locals 0

    .line 294
    iput-object p1, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->position:Lcom/squareup/api/items/Point;

    return-object p0
.end method

.method public rotation_angle(Ljava/lang/Integer;)Lcom/squareup/api/items/FloorPlanTile$Builder;
    .locals 0

    .line 304
    iput-object p1, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->rotation_angle:Ljava/lang/Integer;

    return-object p0
.end method

.method public shape(Lcom/squareup/api/items/FloorPlanTile$Shape;)Lcom/squareup/api/items/FloorPlanTile$Builder;
    .locals 0

    .line 309
    iput-object p1, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->shape:Lcom/squareup/api/items/FloorPlanTile$Shape;

    return-object p0
.end method

.method public width(Ljava/lang/Integer;)Lcom/squareup/api/items/FloorPlanTile$Builder;
    .locals 0

    .line 280
    iput-object p1, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->width:Ljava/lang/Integer;

    return-object p0
.end method

.method public z_order(Ljava/lang/Integer;)Lcom/squareup/api/items/FloorPlanTile$Builder;
    .locals 0

    .line 318
    iput-object p1, p0, Lcom/squareup/api/items/FloorPlanTile$Builder;->z_order:Ljava/lang/Integer;

    return-object p0
.end method
