.class public interface abstract Lcom/squareup/api/multipassauth/MultipassService;
.super Ljava/lang/Object;
.source "MultipassService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/multipassauth/MultipassService$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020\nH\'J\u000e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cH\'J\u0018\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0008\u0008\u0003\u0010\u000f\u001a\u00020\u0001H\'\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/api/multipassauth/MultipassService;",
        "",
        "checkPassword",
        "Lcom/squareup/server/StatusResponse;",
        "Lcom/squareup/protos/client/multipass/CheckPasswordResponse;",
        "request",
        "Lcom/squareup/protos/client/multipass/CheckPasswordRequest;",
        "createOtk",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/multipass/service/CreateOtkResponse;",
        "Lcom/squareup/protos/multipass/service/CreateOtkRequest;",
        "status",
        "Lcom/squareup/server/SimpleStandardResponse;",
        "Lcom/squareup/server/SimpleResponse;",
        "updateDeviceIdIfNeeded",
        "body",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract checkPassword(Lcom/squareup/protos/client/multipass/CheckPasswordRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/multipass/CheckPasswordRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/multipass/CheckPasswordRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/multipass/CheckPasswordResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/multipass/check-password"
    .end annotation
.end method

.method public abstract createOtk(Lcom/squareup/protos/multipass/service/CreateOtkRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/multipass/service/CreateOtkRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/multipass/service/CreateOtkRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/multipass/service/CreateOtkResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/multipass/create-otk"
    .end annotation
.end method

.method public abstract status()Lcom/squareup/server/SimpleStandardResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "/mp/status"
    .end annotation
.end method

.method public abstract updateDeviceIdIfNeeded(Ljava/lang/Object;)Lcom/squareup/server/SimpleStandardResponse;
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Content-Type: application/json",
            "Accept: application/json"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.multipass.external.MultipassAppService/AppUpdateDeviceId"
    .end annotation
.end method
