.class public Lcom/squareup/api/ApiCardOnFileBootstrapScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "ApiCardOnFileBootstrapScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/RegistersInScope;
.implements Lcom/squareup/container/MaybePersistent;


# instance fields
.field public final contact:Lcom/squareup/protos/client/rolodex/Contact;

.field public final crmScope:Lcom/squareup/ui/crm/flow/CrmScope;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/api/ApiCardOnFileBootstrapScreen;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 30
    sget-object v0, Lcom/squareup/ui/main/MainActivityScope;->INSTANCE:Lcom/squareup/ui/main/MainActivityScope;

    invoke-static {v0, p1}, Lcom/squareup/ui/crm/flow/CrmScope;->newApiCrmScope(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/ui/crm/flow/CrmScope;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/ApiCardOnFileBootstrapScreen;->crmScope:Lcom/squareup/ui/crm/flow/CrmScope;

    return-void
.end method


# virtual methods
.method public getParentKey()Lcom/squareup/ui/crm/flow/CrmScope;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/api/ApiCardOnFileBootstrapScreen;->crmScope:Lcom/squareup/ui/crm/flow/CrmScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/api/ApiCardOnFileBootstrapScreen;->getParentKey()Lcom/squareup/ui/crm/flow/CrmScope;

    move-result-object v0

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 53
    const-class v0, Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapComponent;

    .line 54
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapComponent;

    .line 56
    invoke-interface {p1}, Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapComponent;->getReaderSdkCrmRunner()Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;->startSaveCardWorkflow()V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 49
    sget v0, Lcom/squareup/container/R$layout;->assert_never_shown_view:I

    return v0
.end method
