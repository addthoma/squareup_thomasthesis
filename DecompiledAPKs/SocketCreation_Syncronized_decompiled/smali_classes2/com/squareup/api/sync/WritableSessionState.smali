.class public final Lcom/squareup/api/sync/WritableSessionState;
.super Lcom/squareup/wire/Message;
.source "WritableSessionState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/sync/WritableSessionState$ProtoAdapter_WritableSessionState;,
        Lcom/squareup/api/sync/WritableSessionState$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/sync/WritableSessionState;",
        "Lcom/squareup/api/sync/WritableSessionState$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/sync/WritableSessionState;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SEQ:Ljava/lang/Long;

.field public static final DEFAULT_SESSION_ID:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final seq:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field

.field public final session_id:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/api/sync/WritableSessionState$ProtoAdapter_WritableSessionState;

    invoke-direct {v0}, Lcom/squareup/api/sync/WritableSessionState$ProtoAdapter_WritableSessionState;-><init>()V

    sput-object v0, Lcom/squareup/api/sync/WritableSessionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 29
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/sync/WritableSessionState;->DEFAULT_SESSION_ID:Ljava/lang/Long;

    .line 31
    sput-object v0, Lcom/squareup/api/sync/WritableSessionState;->DEFAULT_SEQ:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 1

    .line 52
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/api/sync/WritableSessionState;-><init>(Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/api/sync/WritableSessionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 57
    iput-object p1, p0, Lcom/squareup/api/sync/WritableSessionState;->session_id:Ljava/lang/Long;

    .line 58
    iput-object p2, p0, Lcom/squareup/api/sync/WritableSessionState;->seq:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 73
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/sync/WritableSessionState;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 74
    :cond_1
    check-cast p1, Lcom/squareup/api/sync/WritableSessionState;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/api/sync/WritableSessionState;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/sync/WritableSessionState;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/WritableSessionState;->session_id:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/api/sync/WritableSessionState;->session_id:Ljava/lang/Long;

    .line 76
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/WritableSessionState;->seq:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/api/sync/WritableSessionState;->seq:Ljava/lang/Long;

    .line 77
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 82
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 84
    invoke-virtual {p0}, Lcom/squareup/api/sync/WritableSessionState;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lcom/squareup/api/sync/WritableSessionState;->session_id:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 86
    iget-object v1, p0, Lcom/squareup/api/sync/WritableSessionState;->seq:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 87
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/sync/WritableSessionState$Builder;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/api/sync/WritableSessionState$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/WritableSessionState$Builder;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/squareup/api/sync/WritableSessionState;->session_id:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/api/sync/WritableSessionState$Builder;->session_id:Ljava/lang/Long;

    .line 65
    iget-object v1, p0, Lcom/squareup/api/sync/WritableSessionState;->seq:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/api/sync/WritableSessionState$Builder;->seq:Ljava/lang/Long;

    .line 66
    invoke-virtual {p0}, Lcom/squareup/api/sync/WritableSessionState;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/WritableSessionState$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/api/sync/WritableSessionState;->newBuilder()Lcom/squareup/api/sync/WritableSessionState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    iget-object v1, p0, Lcom/squareup/api/sync/WritableSessionState;->session_id:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", session_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/WritableSessionState;->session_id:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 96
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/sync/WritableSessionState;->seq:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", seq="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/WritableSessionState;->seq:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "WritableSessionState{"

    .line 97
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
