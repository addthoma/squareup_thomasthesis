.class public final Lcom/squareup/api/sync/RequestScope;
.super Lcom/squareup/wire/Message;
.source "RequestScope.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/sync/RequestScope$ProtoAdapter_RequestScope;,
        Lcom/squareup/api/sync/RequestScope$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/sync/RequestScope;",
        "Lcom/squareup/api/sync/RequestScope$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/sync/RequestScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SUPPORTS_ADVANCED_AVAILABILITY:Ljava/lang/Boolean;

.field public static final DEFAULT_SUPPORTS_CANCELLED_APPOINTMENTS:Ljava/lang/Boolean;

.field public static final DEFAULT_SUPPORTS_CARTS_IN_APPOINTMENTS:Ljava/lang/Boolean;

.field public static final DEFAULT_SUPPORTS_CONFIRMATIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_SUPPORTS_PAGINATION:Ljava/lang/Boolean;

.field public static final DEFAULT_TICKETS_ROLE:Lcom/squareup/protos/tickets/TicketsRole;

.field public static final DEFAULT_USER_ID:Ljava/lang/Integer;

.field public static final DEFAULT_USER_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final supported_features:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.CatalogFeature#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->PACKED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;"
        }
    .end annotation
.end field

.field public final supports_advanced_availability:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3ee
    .end annotation
.end field

.field public final supports_cancelled_appointments:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3eb
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final supports_carts_in_appointments:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3ec
    .end annotation
.end field

.field public final supports_confirmations:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3ef
    .end annotation
.end field

.field public final supports_pagination:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3ed
    .end annotation
.end field

.field public final tickets_role:Lcom/squareup/protos/tickets/TicketsRole;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.tickets.TicketsRole#ADAPTER"
        tag = 0x3e8
    .end annotation
.end field

.field public final unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.agenda.UnitIdentifier#ADAPTER"
        tag = 0x3ea
    .end annotation
.end field

.field public final user_id:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final user_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.VisibleTypeSet#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 29
    new-instance v0, Lcom/squareup/api/sync/RequestScope$ProtoAdapter_RequestScope;

    invoke-direct {v0}, Lcom/squareup/api/sync/RequestScope$ProtoAdapter_RequestScope;-><init>()V

    sput-object v0, Lcom/squareup/api/sync/RequestScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 35
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/sync/RequestScope;->DEFAULT_USER_ID:Ljava/lang/Integer;

    .line 37
    sget-object v0, Lcom/squareup/protos/tickets/TicketsRole;->MERCHANT:Lcom/squareup/protos/tickets/TicketsRole;

    sput-object v0, Lcom/squareup/api/sync/RequestScope;->DEFAULT_TICKETS_ROLE:Lcom/squareup/protos/tickets/TicketsRole;

    .line 39
    sput-object v1, Lcom/squareup/api/sync/RequestScope;->DEFAULT_SUPPORTS_CANCELLED_APPOINTMENTS:Ljava/lang/Boolean;

    .line 41
    sput-object v1, Lcom/squareup/api/sync/RequestScope;->DEFAULT_SUPPORTS_CARTS_IN_APPOINTMENTS:Ljava/lang/Boolean;

    .line 43
    sput-object v1, Lcom/squareup/api/sync/RequestScope;->DEFAULT_SUPPORTS_PAGINATION:Ljava/lang/Boolean;

    .line 45
    sput-object v1, Lcom/squareup/api/sync/RequestScope;->DEFAULT_SUPPORTS_ADVANCED_AVAILABILITY:Ljava/lang/Boolean;

    .line 47
    sput-object v1, Lcom/squareup/api/sync/RequestScope;->DEFAULT_SUPPORTS_CONFIRMATIONS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/sync/VisibleTypeSet;Ljava/util/List;Lcom/squareup/protos/tickets/TicketsRole;Lcom/squareup/protos/agenda/UnitIdentifier;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/api/sync/VisibleTypeSet;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;",
            "Lcom/squareup/protos/tickets/TicketsRole;",
            "Lcom/squareup/protos/agenda/UnitIdentifier;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 167
    sget-object v12, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/api/sync/RequestScope;-><init>(Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/sync/VisibleTypeSet;Ljava/util/List;Lcom/squareup/protos/tickets/TicketsRole;Lcom/squareup/protos/agenda/UnitIdentifier;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/sync/VisibleTypeSet;Ljava/util/List;Lcom/squareup/protos/tickets/TicketsRole;Lcom/squareup/protos/agenda/UnitIdentifier;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/api/sync/VisibleTypeSet;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;",
            "Lcom/squareup/protos/tickets/TicketsRole;",
            "Lcom/squareup/protos/agenda/UnitIdentifier;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 176
    sget-object v0, Lcom/squareup/api/sync/RequestScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p12}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 177
    iput-object p1, p0, Lcom/squareup/api/sync/RequestScope;->user_token:Ljava/lang/String;

    .line 178
    iput-object p2, p0, Lcom/squareup/api/sync/RequestScope;->user_id:Ljava/lang/Integer;

    .line 179
    iput-object p3, p0, Lcom/squareup/api/sync/RequestScope;->visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;

    const-string p1, "supported_features"

    .line 180
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/sync/RequestScope;->supported_features:Ljava/util/List;

    .line 181
    iput-object p5, p0, Lcom/squareup/api/sync/RequestScope;->tickets_role:Lcom/squareup/protos/tickets/TicketsRole;

    .line 182
    iput-object p6, p0, Lcom/squareup/api/sync/RequestScope;->unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;

    .line 183
    iput-object p7, p0, Lcom/squareup/api/sync/RequestScope;->supports_cancelled_appointments:Ljava/lang/Boolean;

    .line 184
    iput-object p8, p0, Lcom/squareup/api/sync/RequestScope;->supports_carts_in_appointments:Ljava/lang/Boolean;

    .line 185
    iput-object p9, p0, Lcom/squareup/api/sync/RequestScope;->supports_pagination:Ljava/lang/Boolean;

    .line 186
    iput-object p10, p0, Lcom/squareup/api/sync/RequestScope;->supports_advanced_availability:Ljava/lang/Boolean;

    .line 187
    iput-object p11, p0, Lcom/squareup/api/sync/RequestScope;->supports_confirmations:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 211
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/sync/RequestScope;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 212
    :cond_1
    check-cast p1, Lcom/squareup/api/sync/RequestScope;

    .line 213
    invoke-virtual {p0}, Lcom/squareup/api/sync/RequestScope;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/sync/RequestScope;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->user_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/sync/RequestScope;->user_token:Ljava/lang/String;

    .line 214
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->user_id:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/sync/RequestScope;->user_id:Ljava/lang/Integer;

    .line 215
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;

    iget-object v3, p1, Lcom/squareup/api/sync/RequestScope;->visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;

    .line 216
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supported_features:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/api/sync/RequestScope;->supported_features:Ljava/util/List;

    .line 217
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->tickets_role:Lcom/squareup/protos/tickets/TicketsRole;

    iget-object v3, p1, Lcom/squareup/api/sync/RequestScope;->tickets_role:Lcom/squareup/protos/tickets/TicketsRole;

    .line 218
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;

    iget-object v3, p1, Lcom/squareup/api/sync/RequestScope;->unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;

    .line 219
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_cancelled_appointments:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/sync/RequestScope;->supports_cancelled_appointments:Ljava/lang/Boolean;

    .line 220
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_carts_in_appointments:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/sync/RequestScope;->supports_carts_in_appointments:Ljava/lang/Boolean;

    .line 221
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_pagination:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/sync/RequestScope;->supports_pagination:Ljava/lang/Boolean;

    .line 222
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_advanced_availability:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/sync/RequestScope;->supports_advanced_availability:Ljava/lang/Boolean;

    .line 223
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_confirmations:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/api/sync/RequestScope;->supports_confirmations:Ljava/lang/Boolean;

    .line 224
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 229
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 231
    invoke-virtual {p0}, Lcom/squareup/api/sync/RequestScope;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 232
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->user_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 233
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->user_id:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 234
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/api/sync/VisibleTypeSet;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 235
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supported_features:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 236
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->tickets_role:Lcom/squareup/protos/tickets/TicketsRole;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/tickets/TicketsRole;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 237
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/agenda/UnitIdentifier;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 238
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_cancelled_appointments:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 239
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_carts_in_appointments:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 240
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_pagination:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 241
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_advanced_availability:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 242
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_confirmations:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 243
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/sync/RequestScope$Builder;
    .locals 2

    .line 192
    new-instance v0, Lcom/squareup/api/sync/RequestScope$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/RequestScope$Builder;-><init>()V

    .line 193
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->user_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/sync/RequestScope$Builder;->user_token:Ljava/lang/String;

    .line 194
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->user_id:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/sync/RequestScope$Builder;->user_id:Ljava/lang/Integer;

    .line 195
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;

    iput-object v1, v0, Lcom/squareup/api/sync/RequestScope$Builder;->visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;

    .line 196
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supported_features:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/sync/RequestScope$Builder;->supported_features:Ljava/util/List;

    .line 197
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->tickets_role:Lcom/squareup/protos/tickets/TicketsRole;

    iput-object v1, v0, Lcom/squareup/api/sync/RequestScope$Builder;->tickets_role:Lcom/squareup/protos/tickets/TicketsRole;

    .line 198
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;

    iput-object v1, v0, Lcom/squareup/api/sync/RequestScope$Builder;->unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;

    .line 199
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_cancelled_appointments:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/sync/RequestScope$Builder;->supports_cancelled_appointments:Ljava/lang/Boolean;

    .line 200
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_carts_in_appointments:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/sync/RequestScope$Builder;->supports_carts_in_appointments:Ljava/lang/Boolean;

    .line 201
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_pagination:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/sync/RequestScope$Builder;->supports_pagination:Ljava/lang/Boolean;

    .line 202
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_advanced_availability:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/sync/RequestScope$Builder;->supports_advanced_availability:Ljava/lang/Boolean;

    .line 203
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_confirmations:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/sync/RequestScope$Builder;->supports_confirmations:Ljava/lang/Boolean;

    .line 204
    invoke-virtual {p0}, Lcom/squareup/api/sync/RequestScope;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/RequestScope$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/api/sync/RequestScope;->newBuilder()Lcom/squareup/api/sync/RequestScope$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 250
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 251
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->user_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", user_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->user_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->user_id:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", user_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->user_id:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 253
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;

    if-eqz v1, :cond_2

    const-string v1, ", visible_type_set="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 254
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supported_features:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", supported_features="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supported_features:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 255
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->tickets_role:Lcom/squareup/protos/tickets/TicketsRole;

    if-eqz v1, :cond_4

    const-string v1, ", tickets_role="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->tickets_role:Lcom/squareup/protos/tickets/TicketsRole;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 256
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;

    if-eqz v1, :cond_5

    const-string v1, ", unit_identifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 257
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_cancelled_appointments:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", supports_cancelled_appointments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_cancelled_appointments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 258
    :cond_6
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_carts_in_appointments:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", supports_carts_in_appointments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_carts_in_appointments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 259
    :cond_7
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_pagination:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", supports_pagination="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_pagination:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 260
    :cond_8
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_advanced_availability:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", supports_advanced_availability="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_advanced_availability:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 261
    :cond_9
    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_confirmations:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", supports_confirmations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope;->supports_confirmations:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RequestScope{"

    .line 262
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
