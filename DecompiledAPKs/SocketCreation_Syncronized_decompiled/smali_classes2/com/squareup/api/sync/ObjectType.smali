.class public final Lcom/squareup/api/sync/ObjectType;
.super Lcom/squareup/wire/Message;
.source "ObjectType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/sync/ObjectType$ProtoAdapter_ObjectType;,
        Lcom/squareup/api/sync/ObjectType$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/sync/ObjectType;",
        "Lcom/squareup/api/sync/ObjectType$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/sync/ObjectType;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AGENDA_TYPE:Lcom/squareup/protos/agenda/AgendaType;

.field public static final DEFAULT_EXTERNAL_TYPE:Lcom/squareup/api/items/ExternalType;

.field public static final DEFAULT_TYPE:Lcom/squareup/api/items/Type;

.field private static final serialVersionUID:J


# instance fields
.field public final agenda_type:Lcom/squareup/protos/agenda/AgendaType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.agenda.AgendaType#ADAPTER"
        tag = 0x6d
    .end annotation
.end field

.field public final external_type:Lcom/squareup/api/items/ExternalType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ExternalType#ADAPTER"
        tag = 0x67
    .end annotation
.end field

.field public final type:Lcom/squareup/api/items/Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Type#ADAPTER"
        tag = 0x65
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/api/sync/ObjectType$ProtoAdapter_ObjectType;

    invoke-direct {v0}, Lcom/squareup/api/sync/ObjectType$ProtoAdapter_ObjectType;-><init>()V

    sput-object v0, Lcom/squareup/api/sync/ObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/agenda/AgendaType;->SERVICE:Lcom/squareup/protos/agenda/AgendaType;

    sput-object v0, Lcom/squareup/api/sync/ObjectType;->DEFAULT_AGENDA_TYPE:Lcom/squareup/protos/agenda/AgendaType;

    .line 32
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    sput-object v0, Lcom/squareup/api/sync/ObjectType;->DEFAULT_TYPE:Lcom/squareup/api/items/Type;

    .line 34
    sget-object v0, Lcom/squareup/api/items/ExternalType;->ITEM_ARCHETYPE:Lcom/squareup/api/items/ExternalType;

    sput-object v0, Lcom/squareup/api/sync/ObjectType;->DEFAULT_EXTERNAL_TYPE:Lcom/squareup/api/items/ExternalType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/agenda/AgendaType;Lcom/squareup/api/items/Type;Lcom/squareup/api/items/ExternalType;)V
    .locals 1

    .line 64
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/api/sync/ObjectType;-><init>(Lcom/squareup/protos/agenda/AgendaType;Lcom/squareup/api/items/Type;Lcom/squareup/api/items/ExternalType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/agenda/AgendaType;Lcom/squareup/api/items/Type;Lcom/squareup/api/items/ExternalType;Lokio/ByteString;)V
    .locals 1

    .line 69
    sget-object v0, Lcom/squareup/api/sync/ObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 70
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectType;->agenda_type:Lcom/squareup/protos/agenda/AgendaType;

    .line 71
    iput-object p2, p0, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    .line 72
    iput-object p3, p0, Lcom/squareup/api/sync/ObjectType;->external_type:Lcom/squareup/api/items/ExternalType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 88
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/sync/ObjectType;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 89
    :cond_1
    check-cast p1, Lcom/squareup/api/sync/ObjectType;

    .line 90
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectType;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectType;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectType;->agenda_type:Lcom/squareup/protos/agenda/AgendaType;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectType;->agenda_type:Lcom/squareup/protos/agenda/AgendaType;

    .line 91
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    .line 92
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectType;->external_type:Lcom/squareup/api/items/ExternalType;

    iget-object p1, p1, Lcom/squareup/api/sync/ObjectType;->external_type:Lcom/squareup/api/items/ExternalType;

    .line 93
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 98
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 100
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectType;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectType;->agenda_type:Lcom/squareup/protos/agenda/AgendaType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/agenda/AgendaType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/items/Type;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 103
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectType;->external_type:Lcom/squareup/api/items/ExternalType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/api/items/ExternalType;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 104
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/sync/ObjectType$Builder;
    .locals 2

    .line 77
    new-instance v0, Lcom/squareup/api/sync/ObjectType$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/ObjectType$Builder;-><init>()V

    .line 78
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectType;->agenda_type:Lcom/squareup/protos/agenda/AgendaType;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectType$Builder;->agenda_type:Lcom/squareup/protos/agenda/AgendaType;

    .line 79
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectType$Builder;->type:Lcom/squareup/api/items/Type;

    .line 80
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectType;->external_type:Lcom/squareup/api/items/ExternalType;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectType$Builder;->external_type:Lcom/squareup/api/items/ExternalType;

    .line 81
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectType;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectType$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectType;->newBuilder()Lcom/squareup/api/sync/ObjectType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectType;->agenda_type:Lcom/squareup/protos/agenda/AgendaType;

    if-eqz v1, :cond_0

    const-string v1, ", agenda_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectType;->agenda_type:Lcom/squareup/protos/agenda/AgendaType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    if-eqz v1, :cond_1

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 114
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectType;->external_type:Lcom/squareup/api/items/ExternalType;

    if-eqz v1, :cond_2

    const-string v1, ", external_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectType;->external_type:Lcom/squareup/api/items/ExternalType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ObjectType{"

    .line 115
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
