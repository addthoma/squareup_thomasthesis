.class final Lcom/squareup/api/sync/VisibleTypeSet$ProtoAdapter_VisibleTypeSet;
.super Lcom/squareup/wire/ProtoAdapter;
.source "VisibleTypeSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/VisibleTypeSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_VisibleTypeSet"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/api/sync/VisibleTypeSet;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 131
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/api/sync/VisibleTypeSet;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/sync/VisibleTypeSet;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 150
    new-instance v0, Lcom/squareup/api/sync/VisibleTypeSet$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/VisibleTypeSet$Builder;-><init>()V

    .line 151
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 152
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/16 v4, 0xa

    if-eq v3, v4, :cond_0

    .line 164
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 157
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/api/items/ItemsVisibleTypeSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/ItemsVisibleTypeSet;

    invoke-virtual {v0, v4}, Lcom/squareup/api/sync/VisibleTypeSet$Builder;->items_visible_type_set(Lcom/squareup/api/items/ItemsVisibleTypeSet;)Lcom/squareup/api/sync/VisibleTypeSet$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 159
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/api/sync/VisibleTypeSet$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 154
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/VisibleTypeSet$Builder;->all_types(Ljava/lang/Boolean;)Lcom/squareup/api/sync/VisibleTypeSet$Builder;

    goto :goto_0

    .line 168
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/VisibleTypeSet$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 169
    invoke-virtual {v0}, Lcom/squareup/api/sync/VisibleTypeSet$Builder;->build()Lcom/squareup/api/sync/VisibleTypeSet;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 129
    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/VisibleTypeSet$ProtoAdapter_VisibleTypeSet;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/sync/VisibleTypeSet;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/sync/VisibleTypeSet;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 143
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/VisibleTypeSet;->all_types:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 144
    sget-object v0, Lcom/squareup/api/items/ItemsVisibleTypeSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/VisibleTypeSet;->items_visible_type_set:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 145
    invoke-virtual {p2}, Lcom/squareup/api/sync/VisibleTypeSet;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 129
    check-cast p2, Lcom/squareup/api/sync/VisibleTypeSet;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/sync/VisibleTypeSet$ProtoAdapter_VisibleTypeSet;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/sync/VisibleTypeSet;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/api/sync/VisibleTypeSet;)I
    .locals 4

    .line 136
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/VisibleTypeSet;->all_types:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/api/items/ItemsVisibleTypeSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/VisibleTypeSet;->items_visible_type_set:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    const/16 v3, 0xa

    .line 137
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 138
    invoke-virtual {p1}, Lcom/squareup/api/sync/VisibleTypeSet;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 129
    check-cast p1, Lcom/squareup/api/sync/VisibleTypeSet;

    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/VisibleTypeSet$ProtoAdapter_VisibleTypeSet;->encodedSize(Lcom/squareup/api/sync/VisibleTypeSet;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/api/sync/VisibleTypeSet;)Lcom/squareup/api/sync/VisibleTypeSet;
    .locals 0

    .line 174
    invoke-virtual {p1}, Lcom/squareup/api/sync/VisibleTypeSet;->newBuilder()Lcom/squareup/api/sync/VisibleTypeSet$Builder;

    move-result-object p1

    .line 175
    invoke-virtual {p1}, Lcom/squareup/api/sync/VisibleTypeSet$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 176
    invoke-virtual {p1}, Lcom/squareup/api/sync/VisibleTypeSet$Builder;->build()Lcom/squareup/api/sync/VisibleTypeSet;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 129
    check-cast p1, Lcom/squareup/api/sync/VisibleTypeSet;

    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/VisibleTypeSet$ProtoAdapter_VisibleTypeSet;->redact(Lcom/squareup/api/sync/VisibleTypeSet;)Lcom/squareup/api/sync/VisibleTypeSet;

    move-result-object p1

    return-object p1
.end method
