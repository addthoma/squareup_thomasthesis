.class public Lcom/squareup/api/ApiSessionLogger;
.super Ljava/lang/Object;
.source "ApiSessionLogger.java"


# static fields
.field private static final ES2_LOG_SEQUENCE_KEY:Ljava/lang/String; = "mobile_app_api_sequence_uuid"


# instance fields
.field private final eventStream:Lcom/squareup/eventstream/v1/EventStream;

.field private final eventstreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;


# direct methods
.method constructor <init>(Lcom/squareup/eventstream/v1/EventStream;Lcom/squareup/eventstream/v2/EventstreamV2;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/api/ApiSessionLogger;->eventStream:Lcom/squareup/eventstream/v1/EventStream;

    .line 18
    iput-object p2, p0, Lcom/squareup/api/ApiSessionLogger;->eventstreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;

    return-void
.end method


# virtual methods
.method public clearApiLogProperty()V
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/api/ApiSessionLogger;->eventStream:Lcom/squareup/eventstream/v1/EventStream;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v1/EventStream;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    const-string v1, "api_sequence_uuid"

    .line 30
    invoke-virtual {v0, v1}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->clearCommonProperty(Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/api/ApiSessionLogger;->eventstreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v2/EventstreamV2;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    const-string v1, "mobile_app_api_sequence_uuid"

    .line 32
    invoke-virtual {v0, v1}, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->clearCommonProperty(Ljava/lang/String;)V

    return-void
.end method

.method public setApiLogProperty(Ljava/lang/String;)V
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/squareup/api/ApiSessionLogger;->eventStream:Lcom/squareup/eventstream/v1/EventStream;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v1/EventStream;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    const-string v1, "api_sequence_uuid"

    .line 23
    invoke-virtual {v0, v1, p1}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/squareup/api/ApiSessionLogger;->eventstreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v2/EventstreamV2;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    const-string v1, "mobile_app_api_sequence_uuid"

    .line 25
    invoke-virtual {v0, v1, p1}, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
