.class public final Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;
.super Lcom/squareup/container/WorkflowV2Runner;
.source "RealBuyerLanguageSelectionWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/WorkflowV2Runner<",
        "Lcom/squareup/buyer/language/Exit;",
        ">;",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u0008\u0012\u0004\u0012\u00020\u00030\u0002B\u001f\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0014J\u0008\u0010\u0011\u001a\u00020\u000eH\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflowRunner;",
        "Lcom/squareup/container/WorkflowV2Runner;",
        "Lcom/squareup/buyer/language/Exit;",
        "viewFactory",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;",
        "workflow",
        "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "(Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;Lcom/squareup/ui/main/PosContainer;)V",
        "getWorkflow",
        "()Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "startWorkflow",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final workflow:Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;Lcom/squareup/ui/main/PosContainer;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "viewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    sget-object v0, Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflowRunner;->Companion:Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v2

    .line 17
    invoke-interface {p3}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 18
    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, p0

    .line 15
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/WorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;->workflow:Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;

    iput-object p3, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-object p0
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;->workflow:Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;->getWorkflow()Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-super {p0, p1}, Lcom/squareup/container/WorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 27
    invoke-virtual {p0}, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 29
    invoke-virtual {p0}, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public startWorkflow()V
    .locals 0

    .line 22
    invoke-virtual {p0}, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;->ensureWorkflow()V

    return-void
.end method
