.class final Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$ProtoAdapter_CheckoutPayer;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CheckoutClientDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CheckoutPayer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 218
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 239
    new-instance v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;

    invoke-direct {v0}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;-><init>()V

    .line 240
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 241
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 247
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 245
    :cond_0
    iget-object v3, v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->loyalty_status:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 244
    :cond_1
    iget-object v3, v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->coupon:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/coupons/Coupon;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 243
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->receipt_details(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;)Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;

    goto :goto_0

    .line 251
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 252
    invoke-virtual {v0}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->build()Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 216
    invoke-virtual {p0, p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$ProtoAdapter_CheckoutPayer;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 231
    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 232
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->coupon:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 233
    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->loyalty_status:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 234
    invoke-virtual {p2}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 216
    check-cast p2, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$ProtoAdapter_CheckoutPayer;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;)I
    .locals 4

    .line 223
    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/coupons/Coupon;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 224
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->coupon:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 225
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->loyalty_status:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 226
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 216
    check-cast p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$ProtoAdapter_CheckoutPayer;->encodedSize(Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;)Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;
    .locals 2

    .line 257
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->newBuilder()Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;

    move-result-object p1

    .line 258
    iget-object v0, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    iput-object v0, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    .line 259
    :cond_0
    iget-object v0, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->coupon:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/coupons/Coupon;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 260
    iget-object v0, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->loyalty_status:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 261
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 262
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->build()Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 216
    check-cast p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$ProtoAdapter_CheckoutPayer;->redact(Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;)Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    move-result-object p1

    return-object p1
.end method
