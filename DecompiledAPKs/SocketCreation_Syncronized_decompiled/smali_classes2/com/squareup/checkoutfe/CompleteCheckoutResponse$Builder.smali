.class public final Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CompleteCheckoutResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutfe/CompleteCheckoutResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/checkoutfe/CompleteCheckoutResponse;",
        "Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkoutfe/LocalizedError;",
            ">;"
        }
    .end annotation
.end field

.field public order:Lcom/squareup/orders/model/Order;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 95
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;->errors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/checkoutfe/CompleteCheckoutResponse;
    .locals 4

    .line 111
    new-instance v0, Lcom/squareup/checkoutfe/CompleteCheckoutResponse;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;->errors:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;->order:Lcom/squareup/orders/model/Order;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/checkoutfe/CompleteCheckoutResponse;-><init>(Ljava/util/List;Lcom/squareup/orders/model/Order;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;->build()Lcom/squareup/checkoutfe/CompleteCheckoutResponse;

    move-result-object v0

    return-object v0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkoutfe/LocalizedError;",
            ">;)",
            "Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;"
        }
    .end annotation

    .line 99
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 100
    iput-object p1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method

.method public order(Lcom/squareup/orders/model/Order;)Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;->order:Lcom/squareup/orders/model/Order;

    return-object p0
.end method
