.class public final Lcom/squareup/tutorialv2/RealTutorialCore_Factory;
.super Ljava/lang/Object;
.source "RealTutorialCore_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tutorialv2/RealTutorialCore;",
        ">;"
    }
.end annotation


# instance fields
.field private final creatorsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/tutorialv2/TutorialCreator;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/tutorialv2/TutorialCreator;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialContainer;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCore_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/tutorialv2/RealTutorialCore_Factory;->creatorsProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/tutorialv2/RealTutorialCore_Factory;->tutorialContainerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tutorialv2/RealTutorialCore_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/tutorialv2/TutorialCreator;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialContainer;",
            ">;)",
            "Lcom/squareup/tutorialv2/RealTutorialCore_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/tutorialv2/RealTutorialCore_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/tutorialv2/RealTutorialCore_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/thread/executor/MainThread;Ljava/util/List;Ldagger/Lazy;)Lcom/squareup/tutorialv2/RealTutorialCore;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Ljava/util/List<",
            "Lcom/squareup/tutorialv2/TutorialCreator;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/tutorialv2/TutorialContainer;",
            ">;)",
            "Lcom/squareup/tutorialv2/RealTutorialCore;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/tutorialv2/RealTutorialCore;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/tutorialv2/RealTutorialCore;-><init>(Lcom/squareup/thread/executor/MainThread;Ljava/util/List;Ldagger/Lazy;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tutorialv2/RealTutorialCore;
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/tutorialv2/RealTutorialCore_Factory;->creatorsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/tutorialv2/RealTutorialCore_Factory;->tutorialContainerProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/tutorialv2/RealTutorialCore_Factory;->newInstance(Lcom/squareup/thread/executor/MainThread;Ljava/util/List;Ldagger/Lazy;)Lcom/squareup/tutorialv2/RealTutorialCore;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/tutorialv2/RealTutorialCore_Factory;->get()Lcom/squareup/tutorialv2/RealTutorialCore;

    move-result-object v0

    return-object v0
.end method
