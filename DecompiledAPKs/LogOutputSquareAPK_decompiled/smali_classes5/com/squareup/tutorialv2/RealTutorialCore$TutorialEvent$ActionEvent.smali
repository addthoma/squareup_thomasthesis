.class public final Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent$ActionEvent;
.super Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;
.source "RealTutorialCore.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ActionEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent$ActionEvent;",
        "Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;",
        "name",
        "",
        "action",
        "Lcom/squareup/tutorialv2/TutorialCore$PendingAction;",
        "(Ljava/lang/String;Lcom/squareup/tutorialv2/TutorialCore$PendingAction;)V",
        "getAction",
        "()Lcom/squareup/tutorialv2/TutorialCore$PendingAction;",
        "sendToTutorial",
        "",
        "tutorial",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final action:Lcom/squareup/tutorialv2/TutorialCore$PendingAction;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/tutorialv2/TutorialCore$PendingAction;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 309
    invoke-direct {p0, p1, v0}, Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;-><init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent$ActionEvent;->action:Lcom/squareup/tutorialv2/TutorialCore$PendingAction;

    return-void
.end method


# virtual methods
.method public final getAction()Lcom/squareup/tutorialv2/TutorialCore$PendingAction;
    .locals 1

    .line 308
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent$ActionEvent;->action:Lcom/squareup/tutorialv2/TutorialCore$PendingAction;

    return-object v0
.end method

.method public sendToTutorial(Lcom/squareup/tutorialv2/Tutorial;)V
    .locals 2

    const-string/jumbo v0, "tutorial"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 311
    invoke-virtual {p0}, Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent$ActionEvent;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent$ActionEvent;->action:Lcom/squareup/tutorialv2/TutorialCore$PendingAction;

    invoke-interface {p1, v0, v1}, Lcom/squareup/tutorialv2/Tutorial;->onTutorialPendingActionEvent(Ljava/lang/String;Lcom/squareup/tutorialv2/TutorialCore$PendingAction;)V

    return-void
.end method
