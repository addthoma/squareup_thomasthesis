.class public Lcom/squareup/settings/LoggedInSettingsModule;
.super Ljava/lang/Object;
.source "LoggedInSettingsModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/settings/LoggedInSettingsPreferenceModule;,
        Lcom/squareup/invoices/InvoicesRxPreferencesModule;,
        Lcom/squareup/loyalty/impl/wiring/LoyaltyPreferencesModule;
    }
.end annotation


# static fields
.field private static final COMPLETED_PAYMENT:Ljava/lang/String; = "completed-payment"

.field private static final DEFAULT_TICKET_TEMPLATE_COUNT:Ljava/lang/String; = "default-ticket-template-count"

.field private static final EMAIL_COLLECTION_ENABLED:Ljava/lang/String; = "email-collection-enabled"

.field private static final IN_MIYRO:Ljava/lang/String; = "in-miryo"

.field private static final LAST_AUTHORIZATION_UNIQUE_KEY:Ljava/lang/String; = "last-authorization-unique-key"

.field private static final LAST_CAPTURE_PAYMENT_ID:Ljava/lang/String; = "last-capture-payment-id"

.field private static final LAST_LIBRARY_MODE:Ljava/lang/String; = "last-library-mode"

.field private static final LAST_LOCAL_PAYMENT_ID:Ljava/lang/String; = "last-local-payment-id"

.field private static final LOYALTY_SCREENS_ENABLED:Ljava/lang/String; = "loyalty-screens-enabled"

.field private static final LOYALTY_SCREEN_TIMEOUT:Ljava/lang/String; = "loyalty-screen-timeout"

.field private static final MAX_TICKET_GROUP_COUNT:Ljava/lang/String; = "max-ticket-group-count"

.field private static final MAX_TICKET_TEMPLATES_PER_GROUP:Ljava/lang/String; = "max-ticket-templates-per-group"

.field private static final OPEN_TICKETS_AS_HOME_SCREEN_ENABLED:Ljava/lang/String; = "open-tickets-as-home-screen-enabled"

.field private static final OPEN_TICKETS_ENABLED:Ljava/lang/String; = "open-tickets-enabled"

.field private static final PREDEFINED_TICKETS_ENABLED:Ljava/lang/String; = "predefined-tickets-enabled"

.field private static final SHOW_NON_QUALIFYING_LOYALTY_EVENTS:Ljava/lang/String; = "show-non-qualifying-loyalty-events"

.field private static final SKIP_RECEIPT_SCREEN:Ljava/lang/String; = "skip-receipt-screen"

.field private static final TIP_CACHE:Ljava/lang/String; = "tip-cache"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static canChangeOpenTicketSettingsDefaultsForRSTSubscriber(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)Z
    .locals 1

    .line 200
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_CHANGE_OT_DEFAULTS_RST:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p0, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 201
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getSubscriptions()Lcom/squareup/settings/server/Subscriptions;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/settings/server/Subscriptions;->hasRestaurantSubscription()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static provideAddTendersRequestServerIsForLocalBillTask(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;"
        }
    .end annotation

    .line 135
    const-class v0, Lcom/squareup/settings/AddTendersRequestServerIds;

    const-string v1, "addtendersrequest-serverid-localbilltask-cache.json"

    invoke-static {p0, v1, p1, v0}, Lcom/squareup/settings/GsonLocalSetting;->forClass(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/Class;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p0

    return-object p0
.end method

.method static provideAtomicBoolean(Lcom/google/gson/Gson;Ljava/io/File;)Lcom/squareup/persistent/AtomicSyncedValue;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            "Ljava/io/File;",
            ")",
            "Lcom/squareup/persistent/AtomicSyncedValue<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 106
    new-instance v0, Ljava/io/File;

    const-string v1, "in-miryo"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 107
    new-instance p1, Lcom/squareup/persistent/RealAtomicSyncedValue;

    const-class v1, Ljava/lang/Boolean;

    invoke-direct {p1, v0, p0, v1}, Lcom/squareup/persistent/RealAtomicSyncedValue;-><init>(Ljava/io/File;Lcom/google/gson/Gson;Ljava/lang/Class;)V

    return-object p1
.end method

.method static provideAtomicLastAuth(Lcom/google/gson/Gson;Ljava/io/File;)Lcom/squareup/persistent/AtomicSyncedValue;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            "Ljava/io/File;",
            ")",
            "Lcom/squareup/persistent/AtomicSyncedValue<",
            "Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;",
            ">;"
        }
    .end annotation

    .line 99
    new-instance v0, Ljava/io/File;

    const-string v1, "last-authorization-unique-key"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 100
    new-instance p1, Lcom/squareup/persistent/RealAtomicSyncedValue;

    const-class v1, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;

    invoke-direct {p1, v0, p0, v1}, Lcom/squareup/persistent/RealAtomicSyncedValue;-><init>(Ljava/io/File;Lcom/google/gson/Gson;Ljava/lang/Class;)V

    return-object p1
.end method

.method static provideAutoEmailReportEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 255
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "auto-email-report-enabled"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideAutoPrintReportEnabled(Landroid/content/SharedPreferences;)Lcom/squareup/settings/BooleanLocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 260
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "auto-print-report-enabled"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static provideCashManagementEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 224
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "cash-management-enabled"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideCashReportEmailRecipient(Landroid/content/SharedPreferences;)Lcom/squareup/settings/StringLocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 270
    new-instance v0, Lcom/squareup/settings/StringLocalSetting;

    const-string v1, "cash-report-email-recipient"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideCompletedPayment(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 149
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "completed-payment"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static provideCurrency(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 300
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p0

    return-object p0
.end method

.method static provideCustomerManagementInCartEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 230
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "customer-management-in-cart-enabled"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideCustomerManagementPostTransactionEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 237
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "customer-management-post-transaction-enabled"

    .line 236
    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideCustomerManagementSaveCardEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 243
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "customer-management-save-card-enabled"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideCustomerManagementSaveCardPostTransactionEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 249
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "customer-management-save-card-post-transaction-enabled"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideDefaultStartingCash(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LongLocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 265
    new-instance v0, Lcom/squareup/settings/LongLocalSetting;

    const-string v1, "default-starting-cash"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/LongLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideDefaultTicketTemplateCount(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 207
    new-instance v0, Lcom/squareup/settings/IntegerLocalSetting;

    const-string v1, "default-ticket-template-count"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/IntegerLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideEmailCollectionEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 312
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "email-collection-enabled"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideFirstPaymentTooltipStatus(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/FirstPaymentTooltipStatus;",
            ">;"
        }
    .end annotation

    .line 92
    new-instance v0, Lcom/squareup/settings/EnumLocalSetting;

    const-class v1, Lcom/squareup/settings/FirstPaymentTooltipStatus;

    const-string v2, "first-payment-tooltip-status"

    invoke-direct {v0, p0, v2, v1}, Lcom/squareup/settings/EnumLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Class;)V

    return-object v0
.end method

.method static provideHasCashDrawerData(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 280
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "has-cash-drawer-data"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideHasViewedReferral(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 275
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "has-tapped-referral"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static provideLastCaptureId(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 113
    new-instance v0, Lcom/squareup/settings/StringLocalSetting;

    const-string v1, "last-capture-payment-id"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideLastLibraryFilter(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/librarylist/CheckoutLibraryListState;",
            ">;"
        }
    .end annotation

    .line 143
    const-class v0, Lcom/squareup/librarylist/CheckoutLibraryListState;

    const-string v1, "last-library-mode"

    invoke-static {p0, v1, p1, v0}, Lcom/squareup/settings/GsonLocalSetting;->forClass(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/Class;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p0

    return-object p0
.end method

.method static provideLastLocalPaymentServerId(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Lcom/squareup/settings/LastLocalPaymentServerId;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 128
    new-instance v0, Lcom/squareup/settings/StringLocalSetting;

    const-string v1, "last-local-payment-id"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideLastTaskRequiresRetry(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 155
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "last-task-requires-retry"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideLoyaltyScreenTimeout(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    const-wide/16 v0, 0x3c

    .line 330
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "loyalty-screen-timeout"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getLong(Ljava/lang/String;Ljava/lang/Long;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideLoyaltyScreensEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 318
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "loyalty-screens-enabled"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideMaxTicketGroupCount(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 212
    new-instance v0, Lcom/squareup/settings/IntegerLocalSetting;

    const-string v1, "max-ticket-group-count"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/IntegerLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideMaxTicketTemplatesPerGroup(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 218
    new-instance v0, Lcom/squareup/settings/IntegerLocalSetting;

    const-string v1, "max-ticket-templates-per-group"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/IntegerLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideMessageCenterMessages(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;>;"
        }
    .end annotation

    .line 120
    new-instance v0, Lcom/squareup/settings/LoggedInSettingsModule$1;

    invoke-direct {v0}, Lcom/squareup/settings/LoggedInSettingsModule$1;-><init>()V

    .line 121
    invoke-virtual {v0}, Lcom/squareup/settings/LoggedInSettingsModule$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v1, "messages.json"

    .line 122
    invoke-static {p0, v1, p1, v0}, Lcom/squareup/settings/GsonLocalSetting;->forType(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/reflect/Type;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p0

    return-object p0
.end method

.method static provideNotificationsDismissed(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/user/DismissedNotifications;",
            ">;"
        }
    .end annotation

    .line 345
    const-class v0, Lcom/squareup/user/DismissedNotifications;

    const-string v1, "dismissed-notifications"

    invoke-static {p0, v1, p1, v0}, Lcom/squareup/settings/GsonLocalSetting;->forClass(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/Class;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p0

    return-object p0
.end method

.method static provideO1ReminderDay(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 291
    new-instance v0, Lcom/squareup/settings/LongLocalSetting;

    const-string v1, "o1-reminder-recent"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/LongLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideOpenTicketsAsHomeScreenEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/analytics/Analytics;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 180
    invoke-static {p1, p2}, Lcom/squareup/settings/LoggedInSettingsModule;->canChangeOpenTicketSettingsDefaultsForRSTSubscriber(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 182
    sget-object p2, Lcom/squareup/analytics/RegisterActionName;->RST_T2_DEFAULT_SETTING_OPEN_TICKETS_AS_HOME:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p3, p2}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 184
    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const-string p2, "open-tickets-as-home-screen-enabled"

    invoke-virtual {p0, p2, p1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideOpenTicketsEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/analytics/Analytics;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 168
    invoke-static {p1, p2}, Lcom/squareup/settings/LoggedInSettingsModule;->canChangeOpenTicketSettingsDefaultsForRSTSubscriber(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 170
    sget-object p2, Lcom/squareup/analytics/RegisterActionName;->RST_T2_DEFAULT_SETTING_OPEN_TICKETS:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p3, p2}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 172
    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const-string p2, "open-tickets-enabled"

    invoke-virtual {p0, p2, p1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static providePaymentNotificationRequired(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 161
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "payment-notification-required"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static providePredefinedTicketsEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/analytics/Analytics;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 191
    invoke-static {p1, p2}, Lcom/squareup/settings/LoggedInSettingsModule;->canChangeOpenTicketSettingsDefaultsForRSTSubscriber(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 193
    sget-object p2, Lcom/squareup/analytics/RegisterActionName;->RST_T2_DEFAULT_SETTING_PREDEFINED_TICKETS:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p3, p2}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 195
    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const-string p2, "predefined-tickets-enabled"

    invoke-virtual {p0, p2, p1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideSalesSummaryEmail(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Lcom/squareup/settings/SalesSummaryEmail;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 306
    new-instance v0, Lcom/squareup/settings/StringLocalSetting;

    const-string v1, "sales-summary-email"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideShowNonQualifyingLoyaltyEvents(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 324
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "show-non-qualifying-loyalty-events"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideSkipReceiptScreen(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreen;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 296
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "skip-receipt-screen"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static provideTenderTipCacheSetting(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;",
            ">;>;"
        }
    .end annotation

    .line 337
    new-instance v0, Lcom/squareup/settings/LoggedInSettingsModule$2;

    invoke-direct {v0}, Lcom/squareup/settings/LoggedInSettingsModule$2;-><init>()V

    .line 339
    invoke-virtual {v0}, Lcom/squareup/settings/LoggedInSettingsModule$2;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string/jumbo v1, "tip-cache"

    .line 337
    invoke-static {p0, v1, p1, v0}, Lcom/squareup/settings/GsonLocalSetting;->forType(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/reflect/Type;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p0

    return-object p0
.end method

.method static provideTicketAutoNumberingEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 286
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string/jumbo v1, "ticket-auto-numbering-enabled"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method
