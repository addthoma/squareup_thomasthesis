.class public Lcom/squareup/settings/server/EmployeeManagementSettings;
.super Ljava/lang/Object;
.source "EmployeeManagementSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;,
        Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;,
        Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;,
        Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;,
        Lcom/squareup/settings/server/EmployeeManagementSettings$TransactionLockModeEnabled;,
        Lcom/squareup/settings/server/EmployeeManagementSettings$TimecardEnabled;,
        Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeEmployeeManagementEnabled;
    }
.end annotation


# static fields
.field public static final TIMECARD_ENABLED_DEFAULT:Z

.field private static final TIMEOUT_DEFAULT:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;


# instance fields
.field private final accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private cachedGuestModeEnabledValue:Z

.field private cachedPasscodeEmployeeManagementEnabledValue:Z

.field private cachedTimeoutValue:Lcom/squareup/settings/server/PasscodeTimeout;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final guestModeDefault:Lcom/squareup/settings/server/passcode/GuestModeDefault;

.field private final onEmployeeManagementSettingChanged:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final onTimeTrackingSettingChanged:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementDefault:Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault;

.field private final passcodeEmployeeManagementEnabledSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final timecardEnabledSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLockModeDefault:Lcom/squareup/settings/server/passcode/TransactionLockModeDefault;

.field private final transactionLockModeEnabledSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 51
    sget-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->NEVER:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    sput-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings;->TIMEOUT_DEFAULT:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/passcode/TransactionLockModeDefault;Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault;Lcom/squareup/settings/server/passcode/GuestModeDefault;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/settings/server/passcode/TransactionLockModeDefault;",
            "Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault;",
            "Lcom/squareup/settings/server/passcode/GuestModeDefault;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->onTimeTrackingSettingChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 148
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->onEmployeeManagementSettingChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 163
    iput-object p1, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->features:Lcom/squareup/settings/server/Features;

    .line 164
    iput-object p2, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    .line 165
    iput-object p3, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 166
    iput-object p4, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->passcodeEmployeeManagementEnabledSetting:Lcom/squareup/settings/LocalSetting;

    .line 167
    iput-object p5, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->timecardEnabledSetting:Lcom/squareup/settings/LocalSetting;

    .line 168
    iput-object p6, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->transactionLockModeEnabledSetting:Lcom/squareup/settings/LocalSetting;

    .line 169
    iput-object p7, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->analytics:Lcom/squareup/analytics/Analytics;

    .line 170
    iput-object p8, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->transactionLockModeDefault:Lcom/squareup/settings/server/passcode/TransactionLockModeDefault;

    .line 171
    iput-object p9, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->passcodeEmployeeManagementDefault:Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault;

    .line 172
    iput-object p10, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->guestModeDefault:Lcom/squareup/settings/server/passcode/GuestModeDefault;

    .line 174
    invoke-direct {p0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->initPasscodeSettings()V

    .line 175
    invoke-direct {p0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->getTimeoutOrDefaultFromPrefs()Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->cachedTimeoutValue:Lcom/squareup/settings/server/PasscodeTimeout;

    return-void
.end method

.method private getTimeoutOrDefaultFromPrefs()Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;
    .locals 1

    .line 284
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 285
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getEmployeeSettings()Lcom/squareup/settings/server/EmployeeSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeSettings;->getInactivityTimeout()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->fromString(Ljava/lang/String;)Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 286
    :cond_0
    sget-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings;->TIMEOUT_DEFAULT:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    :goto_0
    return-object v0
.end method

.method private guestModeEnabledDefaultValue()Z
    .locals 2

    .line 388
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_CHANGE_EMPLOYEE_DEFAULTS_RST:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 389
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSubscriptions()Lcom/squareup/settings/server/Subscriptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/Subscriptions;->hasRestaurantSubscription()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->RST_T2_DEFAULT_SETTING_REQUIRE_EMPLOYEE_PASSCODE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    const/4 v0, 0x0

    return v0

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->guestModeDefault:Lcom/squareup/settings/server/passcode/GuestModeDefault;

    invoke-interface {v0}, Lcom/squareup/settings/server/passcode/GuestModeDefault;->enabled()Z

    move-result v0

    return v0
.end method

.method private initPasscodeSettings()V
    .locals 3

    .line 203
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->passcodeEmployeeManagementEnabledSetting:Lcom/squareup/settings/LocalSetting;

    iget-object v1, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->passcodeEmployeeManagementDefault:Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault;

    .line 204
    invoke-interface {v1}, Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault;->enabled()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 203
    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->cachedPasscodeEmployeeManagementEnabledValue:Z

    .line 206
    iget-boolean v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->cachedPasscodeEmployeeManagementEnabledValue:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 207
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 208
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getEmployeeSettings()Lcom/squareup/settings/server/EmployeeSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeSettings;->getTrackingLevel()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;->fromString(Ljava/lang/String;)Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 210
    sget-object v2, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;->GUEST_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->cachedGuestModeEnabledValue:Z

    goto :goto_0

    .line 212
    :cond_1
    invoke-direct {p0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->guestModeEnabledDefaultValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->cachedGuestModeEnabledValue:Z

    goto :goto_0

    .line 216
    :cond_2
    iput-boolean v1, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->cachedGuestModeEnabledValue:Z

    :goto_0
    return-void
.end method

.method private saveTransactionLockModeSetting(Z)V
    .locals 2

    .line 381
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    new-instance v1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;-><init>()V

    .line 382
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->requires_employee_logout_after_completed_transaction(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->build()Lcom/squareup/server/account/protos/PreferencesRequest;

    move-result-object p1

    .line 381
    invoke-interface {v0, p1}, Lcom/squareup/accountstatus/AccountStatusProvider;->maybeUpdatePreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)V

    return-void
.end method


# virtual methods
.method public canSeePayrollUpsell()Z
    .locals 2

    .line 246
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_PAYROLL_UPSELL:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public canUseTimecards()Z
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->canUseTimecards()Z

    move-result v0

    return v0
.end method

.method public getPasscodeAccess()Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;
    .locals 1

    .line 267
    invoke-virtual {p0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isPasscodeEmployeeManagementEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 268
    sget-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->NO_PASSCODE:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    return-object v0

    .line 271
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isGuestModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 272
    sget-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->PASSCODE_FOR_RESTRICTED_ACTIONS:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    return-object v0

    .line 276
    :cond_1
    sget-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->ALWAYS_REQUIRE_PASSCODE:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    return-object v0
.end method

.method public getTimeout()Lcom/squareup/settings/server/PasscodeTimeout;
    .locals 1

    .line 280
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->cachedTimeoutValue:Lcom/squareup/settings/server/PasscodeTimeout;

    return-object v0
.end method

.method public isEmployeeManagementAllowed()Z
    .locals 3

    .line 183
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 184
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getEmployeeSettings()Lcom/squareup/settings/server/EmployeeSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeSettings;->isEmployeeManagementEnabledForAccount()Ljava/lang/Boolean;

    move-result-object v0

    .line 185
    iget-object v1, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isGuestModeEnabled()Z
    .locals 1

    .line 260
    iget-boolean v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->cachedGuestModeEnabledValue:Z

    return v0
.end method

.method public isPaperSignatureFilteringAllowed()Z
    .locals 2

    .line 250
    invoke-virtual {p0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isEmployeeManagementAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PAPER_SIGNATURE_EMPLOYEE_FILTERING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isPasscodeEmployeeManagementEnabled()Z
    .locals 1

    .line 189
    iget-boolean v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->cachedPasscodeEmployeeManagementEnabledValue:Z

    return v0
.end method

.method public isTimecardEnabled()Z
    .locals 2

    .line 227
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getEmployeeSettings()Lcom/squareup/settings/server/EmployeeSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeSettings;->isTimeTrackingEnabled()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->timecardEnabledSetting:Lcom/squareup/settings/LocalSetting;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isTransactionLockModeEnabled()Z
    .locals 2

    .line 291
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->transactionLockModeEnabledSetting:Lcom/squareup/settings/LocalSetting;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 296
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->saveTransactionLockModeSetting(Z)V

    .line 297
    iget-object v1, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->transactionLockModeEnabledSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v1}, Lcom/squareup/settings/LocalSetting;->remove()V

    .line 298
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getEmployeeSettings()Lcom/squareup/settings/server/EmployeeSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeSettings;->isLockModeEnabled()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 301
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->transactionLockModeDefault:Lcom/squareup/settings/server/passcode/TransactionLockModeDefault;

    invoke-interface {v0}, Lcom/squareup/settings/server/passcode/TransactionLockModeDefault;->value()Z

    move-result v0

    :goto_0
    return v0
.end method

.method public onEmployeeManagementSettingChanged()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 242
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->onEmployeeManagementSettingChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onTimeTrackingSettingChanged()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 235
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->onTimeTrackingSettingChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public saveSettingsToServer()V
    .locals 4

    .line 370
    iget-boolean v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->cachedGuestModeEnabledValue:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;->GUEST_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;->COMPLETE_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;

    .line 371
    :goto_0
    iget-object v1, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    new-instance v2, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    invoke-direct {v2}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 373
    invoke-virtual {v3}, Lcom/squareup/settings/server/AccountStatusSettings;->isEmployeeManagementEnabledForAccount()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 372
    invoke-virtual {v2, v3}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_enabled_for_account(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v2

    const/4 v3, 0x1

    .line 374
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_enabled_for_device(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v2

    iget-object v0, v0, Lcom/squareup/settings/server/EmployeeManagementSettings$TrackingLevel;->serverName:Ljava/lang/String;

    .line 375
    invoke-virtual {v2, v0}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_tracking_level(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->cachedTimeoutValue:Lcom/squareup/settings/server/PasscodeTimeout;

    .line 376
    invoke-interface {v2}, Lcom/squareup/settings/server/PasscodeTimeout;->serverName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_inactivity_timeout(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->timecardEnabledSetting:Lcom/squareup/settings/LocalSetting;

    const/4 v3, 0x0

    .line 377
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->using_time_tracking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->build()Lcom/squareup/server/account/protos/PreferencesRequest;

    move-result-object v0

    .line 371
    invoke-interface {v1, v0}, Lcom/squareup/accountstatus/AccountStatusProvider;->maybeUpdatePreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)V

    return-void
.end method

.method public setBoolean(Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;Z)V
    .locals 2

    .line 329
    sget-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$FieldName:[I

    invoke-virtual {p1}, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 351
    invoke-direct {p0, p2}, Lcom/squareup/settings/server/EmployeeManagementSettings;->saveTransactionLockModeSetting(Z)V

    goto :goto_0

    .line 354
    :cond_0
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown EmployeeManagementSetting.FieldName + "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 355
    invoke-virtual {p1}, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 346
    :cond_1
    iput-boolean p2, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->cachedGuestModeEnabledValue:Z

    .line 347
    invoke-virtual {p0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->saveSettingsToServer()V

    .line 348
    iget-object p1, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p2}, Lcom/squareup/settings/server/EmployeeManagementSettingChanged;->guestModeEnabled(Z)Lcom/squareup/settings/server/EmployeeManagementSettingChanged;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 339
    :cond_2
    iget-object p1, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->timecardEnabledSetting:Lcom/squareup/settings/LocalSetting;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 340
    invoke-virtual {p0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->saveSettingsToServer()V

    .line 343
    iget-object p1, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->onTimeTrackingSettingChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 331
    :cond_3
    iput-boolean p2, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->cachedPasscodeEmployeeManagementEnabledValue:Z

    .line 332
    iget-object p1, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->passcodeEmployeeManagementEnabledSetting:Lcom/squareup/settings/LocalSetting;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 333
    invoke-virtual {p0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->saveSettingsToServer()V

    .line 336
    iget-object p1, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->onEmployeeManagementSettingChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public setPasscodeAccess(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V
    .locals 3

    .line 312
    sget-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$PasscodeAccess:[I

    invoke-virtual {p1}, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    const/4 v2, 0x2

    if-eq p1, v2, :cond_0

    .line 322
    sget-object p1, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-virtual {p0, p1, v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setBoolean(Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;Z)V

    .line 323
    sget-object p1, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->GUEST_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setBoolean(Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;Z)V

    goto :goto_0

    .line 318
    :cond_0
    sget-object p1, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-virtual {p0, p1, v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setBoolean(Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;Z)V

    .line 319
    sget-object p1, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->GUEST_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-virtual {p0, p1, v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setBoolean(Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;Z)V

    goto :goto_0

    .line 314
    :cond_1
    sget-object p1, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setBoolean(Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;Z)V

    .line 315
    sget-object p1, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->GUEST_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setBoolean(Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;Z)V

    :goto_0
    return-void
.end method

.method public setTimeout(Lcom/squareup/settings/server/PasscodeTimeout;)V
    .locals 0

    .line 360
    iput-object p1, p0, Lcom/squareup/settings/server/EmployeeManagementSettings;->cachedTimeoutValue:Lcom/squareup/settings/server/PasscodeTimeout;

    .line 361
    invoke-virtual {p0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->saveSettingsToServer()V

    return-void
.end method
