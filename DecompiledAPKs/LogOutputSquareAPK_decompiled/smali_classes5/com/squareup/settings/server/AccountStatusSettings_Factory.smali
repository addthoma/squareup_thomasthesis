.class public final Lcom/squareup/settings/server/AccountStatusSettings_Factory;
.super Ljava/lang/Object;
.source "AccountStatusSettings_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final statusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardEnabledSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/StoreAndForwardEnabledSetting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/StoreAndForwardEnabledSetting;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/settings/server/AccountStatusSettings_Factory;->accountStatusProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/settings/server/AccountStatusSettings_Factory;->statusProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/settings/server/AccountStatusSettings_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/settings/server/AccountStatusSettings_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/settings/server/AccountStatusSettings_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p6, p0, Lcom/squareup/settings/server/AccountStatusSettings_Factory;->storeAndForwardEnabledSettingProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/settings/server/AccountStatusSettings_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/StoreAndForwardEnabledSetting;",
            ">;)",
            "Lcom/squareup/settings/server/AccountStatusSettings_Factory;"
        }
    .end annotation

    .line 55
    new-instance v7, Lcom/squareup/settings/server/AccountStatusSettings_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/settings/server/AccountStatusSettings_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Ldagger/Lazy;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/payment/StoreAndForwardEnabledSetting;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            "Lcom/squareup/payment/StoreAndForwardEnabledSetting;",
            ")",
            "Lcom/squareup/settings/server/AccountStatusSettings;"
        }
    .end annotation

    .line 62
    new-instance v7, Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/settings/server/AccountStatusSettings;-><init>(Ldagger/Lazy;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/payment/StoreAndForwardEnabledSetting;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 7

    .line 46
    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings_Factory;->accountStatusProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/settings/server/AccountStatusSettings_Factory;->statusProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/settings/server/AccountStatusSettings_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/catalogapi/CatalogIntegrationController;

    iget-object v0, p0, Lcom/squareup/settings/server/AccountStatusSettings_Factory;->storeAndForwardEnabledSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/StoreAndForwardEnabledSetting;

    invoke-static/range {v1 .. v6}, Lcom/squareup/settings/server/AccountStatusSettings_Factory;->newInstance(Ldagger/Lazy;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/payment/StoreAndForwardEnabledSetting;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings_Factory;->get()Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v0

    return-object v0
.end method
