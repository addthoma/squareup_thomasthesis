.class public Lcom/squareup/settings/server/PaymentSettings;
.super Ljava/lang/Object;
.source "PaymentSettings.java"


# static fields
.field private static final EMPTY_LIMITS:Lcom/squareup/server/account/protos/Limits;


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final status:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/server/account/protos/Limits$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Limits$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/server/account/protos/Limits$Builder;->build()Lcom/squareup/server/account/protos/Limits;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/server/account/protos/Limits;->populateDefaults()Lcom/squareup/server/account/protos/Limits;

    move-result-object v0

    sput-object v0, Lcom/squareup/settings/server/PaymentSettings;->EMPTY_LIMITS:Lcom/squareup/server/account/protos/Limits;

    return-void
.end method

.method constructor <init>(Lcom/squareup/server/account/protos/AccountStatusResponse;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    .line 43
    iput-object p2, p0, Lcom/squareup/settings/server/PaymentSettings;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static getLimits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Limits;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->limits:Lcom/squareup/server/account/protos/Limits;

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->limits:Lcom/squareup/server/account/protos/Limits;

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/squareup/settings/server/PaymentSettings;->EMPTY_LIMITS:Lcom/squareup/server/account/protos/Limits;

    :goto_0
    return-object p0
.end method


# virtual methods
.method public canEnableTipping()Z
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_ENABLE_TIPPING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public canOptInToStoreAndForwardPayments()Z
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPT_IN_TO_STORE_AND_FORWARD_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public canSkipSignaturesForSmallPayments()Z
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SKIP_SIGNATURES_FOR_SMALL_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public canUseSplitTender()Z
    .locals 2

    .line 212
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SPLIT_TENDER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public canUseZeroAmountTender()Z
    .locals 2

    .line 216
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ZERO_TENDER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public eligibleForInstallments()Z
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_INSTALLMENTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    iget-object v0, v0, Lcom/squareup/server/account/protos/InstallmentsSettings;->should_show_installments_on_pos:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    iget-object v0, v0, Lcom/squareup/server/account/protos/InstallmentsSettings;->should_show_installments_on_pos:Ljava/lang/Boolean;

    .line 135
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public eligibleForSquareCardProcessing()Z
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ELIGIBLE_FOR_SQUARE_CARD_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public eligibleForThirdPartyCardProcessing()Z
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ELIGIBLE_FOR_THIRD_PARTY_CARD_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public getCnpFee()Lcom/squareup/server/account/protos/ProcessingFee;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    iget-object v0, v0, Lcom/squareup/server/account/protos/ProcessingFees;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    return-object v0
.end method

.method public getCofFee()Lcom/squareup/server/account/protos/ProcessingFee;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    iget-object v0, v0, Lcom/squareup/server/account/protos/ProcessingFees;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    return-object v0
.end method

.method public getCollectCnpPostalCode()Z
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->COLLECT_CNP_POSTAL_CODE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public getCpFee()Lcom/squareup/server/account/protos/ProcessingFee;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    iget-object v0, v0, Lcom/squareup/server/account/protos/ProcessingFees;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    return-object v0
.end method

.method public getEmoneyMin()J
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-static {v0}, Lcom/squareup/settings/server/PaymentSettings;->getLimits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Limits;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Limits;->emoney_transaction_min_cents:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getInteracFee()Lcom/squareup/server/account/protos/ProcessingFee;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    iget-object v0, v0, Lcom/squareup/server/account/protos/ProcessingFees;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    return-object v0
.end method

.method public getJCBFee()Lcom/squareup/server/account/protos/ProcessingFee;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    iget-object v0, v0, Lcom/squareup/server/account/protos/ProcessingFees;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    return-object v0
.end method

.method public getOtherTenderOptions()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/OtherTenderType;",
            ">;"
        }
    .end annotation

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 101
    iget-object v1, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v1, v1, Lcom/squareup/server/account/protos/AccountStatusResponse;->other_tenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/account/protos/OtherTenderType;

    .line 102
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 104
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getOtherTenderTypeNameMap()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 108
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 109
    invoke-virtual {p0}, Lcom/squareup/settings/server/PaymentSettings;->getOtherTenderOptions()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/account/protos/OtherTenderType;

    .line 110
    iget-object v3, v2, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    iget-object v2, v2, Lcom/squareup/server/account/protos/OtherTenderType;->tender_name:Ljava/lang/String;

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getRoundingType()Lcom/squareup/calc/constants/RoundingType;
    .locals 4

    .line 183
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    .line 184
    sget-object v1, Lcom/squareup/settings/server/PaymentSettings$1;->$SwitchMap$com$squareup$server$account$protos$FlagsAndPermissions$RoundingType:[I

    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 188
    sget-object v0, Lcom/squareup/calc/constants/RoundingType;->TRUNCATE:Lcom/squareup/calc/constants/RoundingType;

    return-object v0

    .line 190
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unrecognized rounding type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 186
    :cond_1
    sget-object v0, Lcom/squareup/calc/constants/RoundingType;->BANKERS:Lcom/squareup/calc/constants/RoundingType;

    return-object v0
.end method

.method public getSkipSignatureMaxCents()J
    .locals 2

    .line 200
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-static {v0}, Lcom/squareup/settings/server/PaymentSettings;->getLimits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Limits;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Limits;->skip_receipt_max_cents:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSuicaMax()J
    .locals 2

    .line 161
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-static {v0}, Lcom/squareup/settings/server/PaymentSettings;->getLimits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Limits;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Limits;->suica_transaction_max_cents:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getThirdPartyOtherTenderType()Lcom/squareup/server/account/protos/OtherTenderType;
    .locals 1

    .line 208
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    return-object v0
.end method

.method public getTransactionMaximum()J
    .locals 2

    .line 153
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-static {v0}, Lcom/squareup/settings/server/PaymentSettings;->getLimits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Limits;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Limits;->transaction_max_cents:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTransactionMinimum()J
    .locals 2

    .line 148
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-static {v0}, Lcom/squareup/settings/server/PaymentSettings;->getLimits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Limits;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Limits;->transaction_min_cents:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public hasCnpFee()Z
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    iget-object v0, v0, Lcom/squareup/server/account/protos/ProcessingFees;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasCofFee()Z
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    iget-object v0, v0, Lcom/squareup/server/account/protos/ProcessingFees;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasCpFee()Z
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    iget-object v0, v0, Lcom/squareup/server/account/protos/ProcessingFees;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasInteracFee()Z
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    iget-object v0, v0, Lcom/squareup/server/account/protos/ProcessingFees;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasJCBFee()Z
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    iget-object v0, v0, Lcom/squareup/server/account/protos/ProcessingFees;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isPaymentFlowAutoCaptureEnabled()Z
    .locals 2

    .line 204
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->AUTO_CAPTURE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public isSupportedCardBrandOffline(Ljava/lang/String;)Z
    .locals 2

    .line 175
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_offline:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 178
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 179
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public requiresTrack2OnNonAmexSwipe()Z
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REQUIRES_TRACK_2_IF_NOT_AMEX:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public supportsSms()Z
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/squareup/settings/server/PaymentSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SMS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method
