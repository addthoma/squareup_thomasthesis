.class public Lcom/squareup/settings/server/EmployeeSettings;
.super Ljava/lang/Object;
.source "EmployeeSettings.java"


# instance fields
.field private final statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method constructor <init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/settings/server/EmployeeSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    return-void
.end method


# virtual methods
.method public getEmployeeToken()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 28
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/EmployeesEntity;->token:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getEmployeesEntity()Lcom/squareup/server/account/protos/EmployeesEntity;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    return-object v0
.end method

.method public getInactivityTimeout()Ljava/lang/String;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-static {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->employee_management_inactivity_timeout:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackingLevel()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-static {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->employee_management_tracking_level:Ljava/lang/String;

    return-object v0
.end method

.method public isEmployee()Z
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, v0, Lcom/squareup/server/account/protos/EmployeesEntity;->token:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isEmployeeManagementEnabledForAccount()Ljava/lang/Boolean;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-static {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    return-object v0
.end method

.method public isLockModeEnabled()Ljava/lang/Boolean;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-static {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->employee_management_requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    return-object v0
.end method

.method public isTimeTrackingEnabled()Ljava/lang/Boolean;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->using_time_tracking:Ljava/lang/Boolean;

    return-object v0
.end method
