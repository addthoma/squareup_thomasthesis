.class public abstract Lcom/squareup/settings/server/passcode/PasscodeEnabledDefaultModule;
.super Ljava/lang/Object;
.source "PasscodeEnabledDefaultModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideGuestModeDefault(Lcom/squareup/settings/server/passcode/GuestModeDefault$GuestMode;)Lcom/squareup/settings/server/passcode/GuestModeDefault;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePasscodeEmployeeManagementDefault(Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault$PasscodeEmployeeManagementEnabled;)Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
