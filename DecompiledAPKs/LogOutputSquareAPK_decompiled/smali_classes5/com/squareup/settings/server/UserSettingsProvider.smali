.class public interface abstract Lcom/squareup/settings/server/UserSettingsProvider;
.super Ljava/lang/Object;
.source "UserSettingsProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/settings/server/UserSettingsProvider$NoUserSettingsProvider;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u0010J\u0008\u0010\u0002\u001a\u00020\u0003H&J\n\u0010\u0004\u001a\u0004\u0018\u00010\u0005H&J\n\u0010\u0006\u001a\u0004\u0018\u00010\u0003H&J\u0008\u0010\u0007\u001a\u00020\u0008H&J\u0008\u0010\t\u001a\u00020\u0003H&J\u0008\u0010\n\u001a\u00020\u000bH&J\u0008\u0010\u000c\u001a\u00020\u0003H&J\u0008\u0010\r\u001a\u00020\u0003H&J\u0008\u0010\u000e\u001a\u00020\u000fH&\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/settings/server/UserSettingsProvider;",
        "",
        "getBusinessName",
        "",
        "getCountryCode",
        "Lcom/squareup/CountryCode;",
        "getCuratedImageUrl",
        "getCurrencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "getEmail",
        "getMcc",
        "",
        "getName",
        "getToken",
        "hasUserSettings",
        "",
        "NoUserSettingsProvider",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getBusinessName()Ljava/lang/String;
.end method

.method public abstract getCountryCode()Lcom/squareup/CountryCode;
.end method

.method public abstract getCuratedImageUrl()Ljava/lang/String;
.end method

.method public abstract getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
.end method

.method public abstract getEmail()Ljava/lang/String;
.end method

.method public abstract getMcc()I
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getToken()Ljava/lang/String;
.end method

.method public abstract hasUserSettings()Z
.end method
