.class public final Lcom/squareup/settings/server/RealFeesEditor;
.super Ljava/lang/Object;
.source "RealFeesEditor.java"

# interfaces
.implements Lcom/squareup/settings/server/FeesEditor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/settings/server/RealFeesEditor$Fees;
    }
.end annotation


# static fields
.field static final MAX_OBJECT_COUNT_PER_WRITE_BATCH:I = 0x3e8


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final discountCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;"
        }
    .end annotation
.end field

.field private final eventSink:Lcom/squareup/badbus/BadEventSink;

.field private inForeground:Z

.field private pendingDeleteTaxId:Ljava/lang/String;

.field private final taxCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;"
        }
    .end annotation
.end field

.field private final taxRuleCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogTaxRule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljavax/inject/Provider;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/settings/server/RealFeesEditor;->taxCache:Ljava/util/Map;

    .line 40
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/settings/server/RealFeesEditor;->taxRuleCache:Ljava/util/Map;

    .line 41
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/settings/server/RealFeesEditor;->discountCache:Ljava/util/Map;

    .line 51
    iput-object p1, p0, Lcom/squareup/settings/server/RealFeesEditor;->cogsProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p2, p0, Lcom/squareup/settings/server/RealFeesEditor;->eventSink:Lcom/squareup/badbus/BadEventSink;

    .line 53
    iput-object p3, p0, Lcom/squareup/settings/server/RealFeesEditor;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/settings/server/RealFeesEditor;Lcom/squareup/shared/catalog/CatalogResult;Ljava/lang/Runnable;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/RealFeesEditor;->updateCacheAndAnnounce(Lcom/squareup/shared/catalog/CatalogResult;Ljava/lang/Runnable;)V

    return-void
.end method

.method private announce()V
    .locals 6

    .line 218
    iget-object v0, p0, Lcom/squareup/settings/server/RealFeesEditor;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance v1, Lcom/squareup/settings/server/FeesUpdate;

    invoke-virtual {p0}, Lcom/squareup/settings/server/RealFeesEditor;->getTaxes()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/settings/server/RealFeesEditor;->getTaxRules()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0}, Lcom/squareup/settings/server/RealFeesEditor;->getDiscounts()Ljava/util/List;

    move-result-object v4

    iget-boolean v5, p0, Lcom/squareup/settings/server/RealFeesEditor;->inForeground:Z

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/squareup/settings/server/FeesUpdate;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Z)V

    invoke-interface {v0, v1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    return-void
.end method

.method private enforceMainThread()V
    .locals 1

    const-string v0, "Cannot perform operation off the main thread."

    .line 222
    invoke-static {v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic lambda$-7VBHKwoSgB3t3AwksemGhutcAI(Lcom/squareup/settings/server/RealFeesEditor;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/settings/server/RealFeesEditor$Fees;
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/settings/server/RealFeesEditor;->loadInBackground(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/settings/server/RealFeesEditor$Fees;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$writeTax$1(ZLcom/squareup/shared/catalog/models/CatalogTax;ZLcom/squareup/settings/server/FeesEditor$TaxItemUpdater;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;
    .locals 7

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 105
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_0

    .line 108
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 p0, 0x0

    if-eqz p2, :cond_1

    .line 113
    invoke-interface {p3, p4, v0, v1}, Lcom/squareup/settings/server/FeesEditor$TaxItemUpdater;->findRelationsToUpdate(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/List;Ljava/util/List;)Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;

    move-result-object p0

    .line 120
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    .line 121
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p2

    add-int p3, p1, p2

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, p3, :cond_4

    add-int/lit16 v4, v3, 0x3e8

    if-lt v3, p1, :cond_2

    .line 126
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    goto :goto_1

    .line 127
    :cond_2
    invoke-static {v4, p1}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-interface {v0, v3, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    :goto_1
    sub-int/2addr v3, p1

    .line 129
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    sub-int v6, v4, p1

    .line 130
    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-static {v6, p2}, Ljava/lang/Math;->min(II)I

    move-result v6

    if-gtz v6, :cond_3

    .line 132
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    goto :goto_2

    .line 133
    :cond_3
    invoke-interface {v1, v3, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    .line 134
    :goto_2
    invoke-static {v5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {p4, v5, v3}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    move v3, v4

    goto :goto_0

    :cond_4
    return-object p0
.end method

.method private loadInBackground(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/settings/server/RealFeesEditor$Fees;
    .locals 3

    .line 166
    invoke-interface {p1}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllTaxes()Ljava/util/List;

    move-result-object v0

    .line 168
    iget-object v1, p0, Lcom/squareup/settings/server/RealFeesEditor;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldUseConditionalTaxes()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 169
    invoke-interface {p1}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllTaxRules()Ljava/util/List;

    move-result-object v1

    goto :goto_0

    .line 171
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 173
    :goto_0
    invoke-interface {p1}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllFixedDiscounts()Ljava/util/List;

    move-result-object p1

    .line 174
    new-instance v2, Lcom/squareup/settings/server/RealFeesEditor$Fees;

    invoke-direct {v2, v0, v1, p1}, Lcom/squareup/settings/server/RealFeesEditor$Fees;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v2
.end method

.method private updateCacheAndAnnounce(Lcom/squareup/shared/catalog/CatalogResult;Ljava/lang/Runnable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Lcom/squareup/settings/server/RealFeesEditor$Fees;",
            ">;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .line 178
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/settings/server/RealFeesEditor$Fees;

    .line 179
    iget-object v0, p1, Lcom/squareup/settings/server/RealFeesEditor$Fees;->taxes:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/squareup/settings/server/RealFeesEditor;->writeTaxCache(Ljava/util/List;)V

    .line 180
    invoke-virtual {p1}, Lcom/squareup/settings/server/RealFeesEditor$Fees;->hasTaxRules()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p1, Lcom/squareup/settings/server/RealFeesEditor$Fees;->taxRules:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/squareup/settings/server/RealFeesEditor;->writeTaxRuleCache(Ljava/util/List;)V

    .line 183
    :cond_0
    iget-object p1, p1, Lcom/squareup/settings/server/RealFeesEditor$Fees;->fixedPercentDiscounts:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/squareup/settings/server/RealFeesEditor;->writeDiscountCache(Ljava/util/List;)V

    .line 184
    invoke-direct {p0}, Lcom/squareup/settings/server/RealFeesEditor;->announce()V

    if-eqz p2, :cond_1

    .line 185
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    :cond_1
    return-void
.end method

.method private writeDiscountCache(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 210
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Writing %s entries to the discount cache."

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 211
    iget-object v0, p0, Lcom/squareup/settings/server/RealFeesEditor;->discountCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 212
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 213
    iget-object v1, p0, Lcom/squareup/settings/server/RealFeesEditor;->discountCache:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private writeTaxCache(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 189
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Writing %s entries to the tax cache."

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 190
    iget-object v0, p0, Lcom/squareup/settings/server/RealFeesEditor;->taxCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 191
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogTax;

    .line 195
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/settings/server/RealFeesEditor;->pendingDeleteTaxId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 196
    iget-object v1, p0, Lcom/squareup/settings/server/RealFeesEditor;->taxCache:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method private writeTaxRuleCache(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTaxRule;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 202
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Writing %s entries to the tax rule cache."

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 203
    iget-object v0, p0, Lcom/squareup/settings/server/RealFeesEditor;->taxRuleCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 204
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogTaxRule;

    .line 205
    iget-object v1, p0, Lcom/squareup/settings/server/RealFeesEditor;->taxRuleCache:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public deleteTax(Lcom/squareup/shared/catalog/models/CatalogTax;Ljava/lang/Runnable;)V
    .locals 2

    .line 79
    invoke-direct {p0}, Lcom/squareup/settings/server/RealFeesEditor;->enforceMainThread()V

    .line 80
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTax;->getId()Ljava/lang/String;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/squareup/settings/server/RealFeesEditor;->taxCache:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    iput-object v0, p0, Lcom/squareup/settings/server/RealFeesEditor;->pendingDeleteTaxId:Ljava/lang/String;

    .line 83
    iget-object v0, p0, Lcom/squareup/settings/server/RealFeesEditor;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    .line 84
    invoke-static {}, Lcom/squareup/cogs/CogsTasks;->write()Lcom/squareup/cogs/WriteBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/squareup/cogs/WriteBuilder;->delete(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/cogs/WriteBuilder;

    move-result-object p1

    new-instance v1, Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$XAUNK1wh65cYzAN8HaLjMAHn8KQ;

    invoke-direct {v1, p0, v0, p2}, Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$XAUNK1wh65cYzAN8HaLjMAHn8KQ;-><init>(Lcom/squareup/settings/server/RealFeesEditor;Lcom/squareup/cogs/Cogs;Ljava/lang/Runnable;)V

    invoke-interface {v0, p1, v1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public getDiscounts()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;"
        }
    .end annotation

    .line 73
    invoke-direct {p0}, Lcom/squareup/settings/server/RealFeesEditor;->enforceMainThread()V

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/settings/server/RealFeesEditor;->discountCache:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 75
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTaxRules()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTaxRule;",
            ">;"
        }
    .end annotation

    .line 67
    invoke-direct {p0}, Lcom/squareup/settings/server/RealFeesEditor;->enforceMainThread()V

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/settings/server/RealFeesEditor;->taxRuleCache:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 69
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTaxes()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;"
        }
    .end annotation

    .line 61
    invoke-direct {p0}, Lcom/squareup/settings/server/RealFeesEditor;->enforceMainThread()V

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/settings/server/RealFeesEditor;->taxCache:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 63
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$deleteTax$0$RealFeesEditor(Lcom/squareup/cogs/Cogs;Ljava/lang/Runnable;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 1

    const/4 v0, 0x0

    .line 85
    iput-object v0, p0, Lcom/squareup/settings/server/RealFeesEditor;->pendingDeleteTaxId:Ljava/lang/String;

    .line 86
    invoke-interface {p3}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    .line 87
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object p3

    const/4 v0, 0x0

    invoke-interface {p1, p3, v0}, Lcom/squareup/cogs/Cogs;->sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V

    .line 88
    invoke-direct {p0}, Lcom/squareup/settings/server/RealFeesEditor;->announce()V

    .line 89
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method public synthetic lambda$writeTax$2$RealFeesEditor(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 1

    .line 139
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 141
    iget-object v0, p0, Lcom/squareup/settings/server/RealFeesEditor;->eventSink:Lcom/squareup/badbus/BadEventSink;

    invoke-interface {v0, p1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public read(Ljava/lang/Runnable;)V
    .locals 2

    .line 152
    new-instance v0, Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$-7VBHKwoSgB3t3AwksemGhutcAI;

    invoke-direct {v0, p0}, Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$-7VBHKwoSgB3t3AwksemGhutcAI;-><init>(Lcom/squareup/settings/server/RealFeesEditor;)V

    .line 153
    new-instance v1, Lcom/squareup/settings/server/RealFeesEditor$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/settings/server/RealFeesEditor$1;-><init>(Lcom/squareup/settings/server/RealFeesEditor;Ljava/lang/Runnable;)V

    .line 162
    iget-object p1, p0, Lcom/squareup/settings/server/RealFeesEditor;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cogs/Cogs;

    invoke-interface {p1, v0, v1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public setInForeground()V
    .locals 1

    const/4 v0, 0x1

    .line 57
    iput-boolean v0, p0, Lcom/squareup/settings/server/RealFeesEditor;->inForeground:Z

    return-void
.end method

.method public writeTax(Lcom/squareup/shared/catalog/models/CatalogTax;Lcom/squareup/settings/server/FeesEditor$TaxItemUpdater;)V
    .locals 4

    .line 94
    invoke-direct {p0}, Lcom/squareup/settings/server/RealFeesEditor;->enforceMainThread()V

    .line 97
    iget-object v0, p0, Lcom/squareup/settings/server/RealFeesEditor;->taxCache:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTax;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogTax;

    .line 98
    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogTax;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v0, :cond_1

    if-eqz v1, :cond_2

    .line 102
    :cond_1
    iget-object v2, p0, Lcom/squareup/settings/server/RealFeesEditor;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cogs/Cogs;

    .line 103
    new-instance v3, Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$7EvqRiiPZo9B-6hTM0W8gXDSSHk;

    invoke-direct {v3, v0, p1, v1, p2}, Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$7EvqRiiPZo9B-6hTM0W8gXDSSHk;-><init>(ZLcom/squareup/shared/catalog/models/CatalogTax;ZLcom/squareup/settings/server/FeesEditor$TaxItemUpdater;)V

    new-instance p1, Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$zf0d1hv2XH0CjaUX--56kUfPr10;

    invoke-direct {p1, p0}, Lcom/squareup/settings/server/-$$Lambda$RealFeesEditor$zf0d1hv2XH0CjaUX--56kUfPr10;-><init>(Lcom/squareup/settings/server/RealFeesEditor;)V

    invoke-interface {v2, v3, p1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    :cond_2
    if-eqz v0, :cond_3

    .line 147
    invoke-direct {p0}, Lcom/squareup/settings/server/RealFeesEditor;->announce()V

    :cond_3
    return-void
.end method
