.class public Lcom/squareup/settings/server/SignatureSettings;
.super Ljava/lang/Object;
.source "SignatureSettings.java"


# instance fields
.field private final forPaperSignatureAlwaysPrintCustomerCopy:Z

.field private final forPaperSignaturePrintAdditionalAuthSlip:Z

.field private final forPaperSignatureQuickTipReceipt:Z

.field private final merchantAlwaysSkipSignature:Z

.field private final merchantIsAllowedToSkipSignaturesForSmallPayments:Z

.field private final merchantOptedInToSkipSignaturesForSmallPayments:Z

.field private final usePaperSignature:Z


# direct methods
.method public constructor <init>(ZZZZZZZ)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-boolean p1, p0, Lcom/squareup/settings/server/SignatureSettings;->merchantAlwaysSkipSignature:Z

    .line 22
    iput-boolean p2, p0, Lcom/squareup/settings/server/SignatureSettings;->merchantOptedInToSkipSignaturesForSmallPayments:Z

    .line 24
    iput-boolean p6, p0, Lcom/squareup/settings/server/SignatureSettings;->merchantIsAllowedToSkipSignaturesForSmallPayments:Z

    .line 26
    iput-boolean p3, p0, Lcom/squareup/settings/server/SignatureSettings;->usePaperSignature:Z

    .line 27
    iput-boolean p4, p0, Lcom/squareup/settings/server/SignatureSettings;->forPaperSignatureAlwaysPrintCustomerCopy:Z

    .line 28
    iput-boolean p5, p0, Lcom/squareup/settings/server/SignatureSettings;->forPaperSignatureQuickTipReceipt:Z

    .line 29
    iput-boolean p7, p0, Lcom/squareup/settings/server/SignatureSettings;->forPaperSignaturePrintAdditionalAuthSlip:Z

    return-void
.end method


# virtual methods
.method public forPaperSignatureAlwaysPrintCustomerCopy()Z
    .locals 1

    .line 49
    iget-boolean v0, p0, Lcom/squareup/settings/server/SignatureSettings;->forPaperSignatureAlwaysPrintCustomerCopy:Z

    return v0
.end method

.method public forPaperSignaturePrintAdditionalAuthSlip()Z
    .locals 1

    .line 53
    iget-boolean v0, p0, Lcom/squareup/settings/server/SignatureSettings;->forPaperSignaturePrintAdditionalAuthSlip:Z

    return v0
.end method

.method public forPaperSignatureQuickTipReceipt()Z
    .locals 1

    .line 57
    iget-boolean v0, p0, Lcom/squareup/settings/server/SignatureSettings;->forPaperSignatureQuickTipReceipt:Z

    return v0
.end method

.method public merchantAlwaysSkipSignature()Z
    .locals 1

    .line 33
    iget-boolean v0, p0, Lcom/squareup/settings/server/SignatureSettings;->merchantAlwaysSkipSignature:Z

    return v0
.end method

.method public merchantIsAllowedToSkipSignaturesForSmallPayments()Z
    .locals 1

    .line 41
    iget-boolean v0, p0, Lcom/squareup/settings/server/SignatureSettings;->merchantIsAllowedToSkipSignaturesForSmallPayments:Z

    return v0
.end method

.method public merchantOptedInToSkipSignaturesForSmallPayments()Z
    .locals 1

    .line 37
    iget-boolean v0, p0, Lcom/squareup/settings/server/SignatureSettings;->merchantOptedInToSkipSignaturesForSmallPayments:Z

    return v0
.end method

.method public usePaperSignature()Z
    .locals 1

    .line 45
    iget-boolean v0, p0, Lcom/squareup/settings/server/SignatureSettings;->usePaperSignature:Z

    return v0
.end method
