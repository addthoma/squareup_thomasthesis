.class public Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;
.super Ljava/lang/Object;
.source "FeatureFlagFeatures.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settings/server/FeatureFlagFeatures;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# instance fields
.field private final accountStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final device:Lcom/squareup/util/Device;

.field private final emailSupportLedgerEnabled:Z

.field private final lazyAccountStatus:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljavax/inject/Provider;Ldagger/Lazy;Lcom/squareup/util/Device;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;",
            "Lcom/squareup/util/Device;",
            "Z)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;->accountStatusProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p2, p0, Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;->lazyAccountStatus:Ldagger/Lazy;

    .line 63
    iput-object p3, p0, Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;->device:Lcom/squareup/util/Device;

    .line 64
    iput-boolean p4, p0, Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;->emailSupportLedgerEnabled:Z

    return-void
.end method


# virtual methods
.method public create(Lcom/squareup/settings/server/FeatureServiceVertical;)Lcom/squareup/settings/server/FeatureFlagFeatures;
    .locals 8

    .line 68
    new-instance v7, Lcom/squareup/settings/server/FeatureFlagFeatures;

    iget-object v1, p0, Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;->accountStatusProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;->lazyAccountStatus:Ldagger/Lazy;

    iget-object v3, p0, Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;->device:Lcom/squareup/util/Device;

    iget-boolean v4, p0, Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;->emailSupportLedgerEnabled:Z

    const/4 v6, 0x0

    move-object v0, v7

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/settings/server/FeatureFlagFeatures;-><init>(Ljavax/inject/Provider;Ldagger/Lazy;Lcom/squareup/util/Device;ZLcom/squareup/settings/server/FeatureServiceVertical;Lcom/squareup/settings/server/FeatureFlagFeatures$1;)V

    return-object v7
.end method
