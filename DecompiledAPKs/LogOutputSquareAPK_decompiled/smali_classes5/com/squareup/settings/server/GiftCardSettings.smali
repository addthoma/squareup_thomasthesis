.class public Lcom/squareup/settings/server/GiftCardSettings;
.super Ljava/lang/Object;
.source "GiftCardSettings.java"


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method public constructor <init>(Lcom/squareup/server/account/protos/AccountStatusResponse;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/settings/server/GiftCardSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    .line 14
    iput-object p2, p0, Lcom/squareup/settings/server/GiftCardSettings;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public canAcceptThirdPartyGiftCards()Z
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/squareup/settings/server/GiftCardSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ACCEPT_THIRD_PARTY_GIFT_CARDS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public canRefundToAnyGiftCard()Z
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/settings/server/GiftCardSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_REFUNDS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public canUseGiftCards()Z
    .locals 2

    .line 18
    iget-object v0, p0, Lcom/squareup/settings/server/GiftCardSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_GIFT_CARDS_V2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public disableGiftCardCreation()Z
    .locals 1

    .line 70
    invoke-virtual {p0}, Lcom/squareup/settings/server/GiftCardSettings;->canUseGiftCards()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/settings/server/GiftCardSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    iget-object v0, v0, Lcom/squareup/server/account/protos/GiftCards;->disable_gift_card_creation:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public getGiftCardActivationMaximum()J
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/settings/server/GiftCardSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-static {v0}, Lcom/squareup/settings/server/PaymentSettings;->getLimits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Limits;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Limits;->gift_card_max_activation_amount_cents:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getGiftCardActivationMinimum()J
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/settings/server/GiftCardSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-static {v0}, Lcom/squareup/settings/server/PaymentSettings;->getLimits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Limits;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Limits;->transaction_min_cents:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getGiftCardBins()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/settings/server/GiftCardSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    iget-object v0, v0, Lcom/squareup/server/account/protos/GiftCards;->bins:Ljava/util/List;

    return-object v0
.end method

.method public getGiftCardNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/settings/server/GiftCardSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    iget-object v0, v0, Lcom/squareup/server/account/protos/GiftCards;->track1_data:Ljava/util/List;

    return-object v0
.end method

.method public getGiftCardTotalPurchaseMaximum()Ljava/lang/Long;
    .locals 5

    .line 61
    iget-object v0, p0, Lcom/squareup/settings/server/GiftCardSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-static {v0}, Lcom/squareup/settings/server/PaymentSettings;->getLimits(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Limits;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Limits;->gift_card_max_total_purchase_amount_cents:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 62
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getGiftCardTransactionMinimum()J
    .locals 2

    const-wide/16 v0, 0x1

    return-wide v0
.end method
