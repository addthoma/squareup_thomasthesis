.class public final Lcom/squareup/settings/server/UserSettingsProvider$NoUserSettingsProvider;
.super Ljava/lang/Object;
.source "UserSettingsProvider.kt"

# interfaces
.implements Lcom/squareup/settings/server/UserSettingsProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settings/server/UserSettingsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoUserSettingsProvider"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016J\n\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016J\n\u0010\u0007\u001a\u0004\u0018\u00010\u0004H\u0016J\u0008\u0010\u0008\u001a\u00020\tH\u0016J\u0008\u0010\n\u001a\u00020\u0004H\u0016J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\u0004H\u0016J\u0008\u0010\u000e\u001a\u00020\u0004H\u0016J\u0008\u0010\u000f\u001a\u00020\u0010H\u0016\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/settings/server/UserSettingsProvider$NoUserSettingsProvider;",
        "Lcom/squareup/settings/server/UserSettingsProvider;",
        "()V",
        "getBusinessName",
        "",
        "getCountryCode",
        "Lcom/squareup/CountryCode;",
        "getCuratedImageUrl",
        "getCurrencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "getEmail",
        "getMcc",
        "",
        "getName",
        "getToken",
        "hasUserSettings",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/settings/server/UserSettingsProvider$NoUserSettingsProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/settings/server/UserSettingsProvider$NoUserSettingsProvider;

    invoke-direct {v0}, Lcom/squareup/settings/server/UserSettingsProvider$NoUserSettingsProvider;-><init>()V

    sput-object v0, Lcom/squareup/settings/server/UserSettingsProvider$NoUserSettingsProvider;->INSTANCE:Lcom/squareup/settings/server/UserSettingsProvider$NoUserSettingsProvider;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBusinessName()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public getCountryCode()Lcom/squareup/CountryCode;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getCuratedImageUrl()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/protos/common/CurrencyCode;->XXX:Lcom/squareup/protos/common/CurrencyCode;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public getMcc()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public hasUserSettings()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
