.class Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView$1;
.super Ljava/lang/Object;
.source "CashManagementSettingsBaseView.java"

# interfaces
.implements Landroid/animation/LayoutTransition$TransitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->scrollToBottom()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;


# direct methods
.method constructor <init>(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)V
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView$1;->this$0:Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public endTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 0

    .line 133
    iget-object p1, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView$1;->this$0:Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;

    invoke-static {p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->access$000(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)Landroid/view/ViewGroup;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/animation/LayoutTransition;->removeTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 134
    iget-object p1, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView$1;->this$0:Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;

    invoke-static {p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->access$100(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)Landroid/widget/ScrollView;

    move-result-object p1

    const/16 p2, 0x82

    invoke-virtual {p1, p2}, Landroid/widget/ScrollView;->fullScroll(I)Z

    .line 135
    iget-object p1, p0, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView$1;->this$0:Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;

    invoke-static {p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->access$200(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->requestFocus()Z

    return-void
.end method

.method public startTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 0

    return-void
.end method
