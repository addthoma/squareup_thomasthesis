.class public Lcom/squareup/settings/StringLocalSetting;
.super Lcom/squareup/settings/AbstractLocalSetting;
.source "StringLocalSetting.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/settings/AbstractLocalSetting<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final defaultValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 10
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/AbstractLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 15
    iput-object p3, p0, Lcom/squareup/settings/StringLocalSetting;->defaultValue:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doGet()Ljava/lang/Object;
    .locals 1

    .line 5
    invoke-virtual {p0}, Lcom/squareup/settings/StringLocalSetting;->doGet()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected doGet()Ljava/lang/String;
    .locals 3

    .line 20
    iget-object v0, p0, Lcom/squareup/settings/StringLocalSetting;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/squareup/settings/StringLocalSetting;->key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/settings/StringLocalSetting;->defaultValue:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/Object;)V
    .locals 0

    .line 5
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/settings/StringLocalSetting;->set(Ljava/lang/String;)V

    return-void
.end method

.method public set(Ljava/lang/String;)V
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/settings/StringLocalSetting;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/settings/StringLocalSetting;->key:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/settings/StringLocalSetting;->apply(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method
