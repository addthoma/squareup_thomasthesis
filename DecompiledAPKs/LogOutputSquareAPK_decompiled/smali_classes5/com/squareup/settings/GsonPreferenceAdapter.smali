.class public Lcom/squareup/settings/GsonPreferenceAdapter;
.super Ljava/lang/Object;
.source "GsonPreferenceAdapter.java"

# interfaces
.implements Lcom/f2prateek/rx/preferences2/Preference$Converter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/f2prateek/rx/preferences2/Preference$Converter<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final gson:Lcom/google/gson/Gson;

.field private final type:Ljava/lang/reflect/Type;


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;Ljava/lang/reflect/Type;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/settings/GsonPreferenceAdapter;->gson:Lcom/google/gson/Gson;

    .line 23
    iput-object p2, p0, Lcom/squareup/settings/GsonPreferenceAdapter;->type:Ljava/lang/reflect/Type;

    return-void
.end method


# virtual methods
.method public deserialize(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/settings/GsonPreferenceAdapter;->gson:Lcom/google/gson/Gson;

    iget-object v1, p0, Lcom/squareup/settings/GsonPreferenceAdapter;->type:Ljava/lang/reflect/Type;

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public serialize(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/settings/GsonPreferenceAdapter;->gson:Lcom/google/gson/Gson;

    invoke-virtual {v0, p1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
