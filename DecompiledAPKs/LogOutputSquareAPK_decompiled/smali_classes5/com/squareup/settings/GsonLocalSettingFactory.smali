.class public Lcom/squareup/settings/GsonLocalSettingFactory;
.super Ljava/lang/Object;
.source "GsonLocalSettingFactory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final clazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final gson:Lcom/google/gson/Gson;

.field private final preferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;Landroid/content/SharedPreferences;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            "Landroid/content/SharedPreferences;",
            "Ljava/lang/Class<",
            "TT;>;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/settings/GsonLocalSettingFactory;->gson:Lcom/google/gson/Gson;

    .line 17
    iput-object p2, p0, Lcom/squareup/settings/GsonLocalSettingFactory;->preferences:Landroid/content/SharedPreferences;

    .line 18
    iput-object p3, p0, Lcom/squareup/settings/GsonLocalSettingFactory;->clazz:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public generate(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/settings/GsonLocalSetting;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TT;)",
            "Lcom/squareup/settings/GsonLocalSetting<",
            "TT;>;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/squareup/settings/GsonLocalSettingFactory;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/squareup/settings/GsonLocalSettingFactory;->gson:Lcom/google/gson/Gson;

    iget-object v2, p0, Lcom/squareup/settings/GsonLocalSettingFactory;->clazz:Ljava/lang/Class;

    invoke-static {v0, p1, v1, v2}, Lcom/squareup/settings/GsonLocalSetting;->forClass(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/Class;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p1

    .line 23
    invoke-virtual {p1}, Lcom/squareup/settings/GsonLocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 24
    invoke-virtual {p1, p2}, Lcom/squareup/settings/GsonLocalSetting;->set(Ljava/lang/Object;)V

    :cond_0
    return-object p1
.end method
