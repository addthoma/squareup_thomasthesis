.class public final enum Lcom/squareup/settings/FirstPaymentTooltipStatus;
.super Ljava/lang/Enum;
.source "FirstPaymentTooltipStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/settings/FirstPaymentTooltipStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/settings/FirstPaymentTooltipStatus;

.field public static final enum DISMISSED:Lcom/squareup/settings/FirstPaymentTooltipStatus;

.field public static final enum NEVER_SHOWN:Lcom/squareup/settings/FirstPaymentTooltipStatus;

.field public static final enum SHOWING:Lcom/squareup/settings/FirstPaymentTooltipStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 6
    new-instance v0, Lcom/squareup/settings/FirstPaymentTooltipStatus;

    const/4 v1, 0x0

    const-string v2, "NEVER_SHOWN"

    invoke-direct {v0, v2, v1}, Lcom/squareup/settings/FirstPaymentTooltipStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/settings/FirstPaymentTooltipStatus;->NEVER_SHOWN:Lcom/squareup/settings/FirstPaymentTooltipStatus;

    new-instance v0, Lcom/squareup/settings/FirstPaymentTooltipStatus;

    const/4 v2, 0x1

    const-string v3, "SHOWING"

    invoke-direct {v0, v3, v2}, Lcom/squareup/settings/FirstPaymentTooltipStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/settings/FirstPaymentTooltipStatus;->SHOWING:Lcom/squareup/settings/FirstPaymentTooltipStatus;

    new-instance v0, Lcom/squareup/settings/FirstPaymentTooltipStatus;

    const/4 v3, 0x2

    const-string v4, "DISMISSED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/settings/FirstPaymentTooltipStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/settings/FirstPaymentTooltipStatus;->DISMISSED:Lcom/squareup/settings/FirstPaymentTooltipStatus;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/settings/FirstPaymentTooltipStatus;

    .line 4
    sget-object v4, Lcom/squareup/settings/FirstPaymentTooltipStatus;->NEVER_SHOWN:Lcom/squareup/settings/FirstPaymentTooltipStatus;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/settings/FirstPaymentTooltipStatus;->SHOWING:Lcom/squareup/settings/FirstPaymentTooltipStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/FirstPaymentTooltipStatus;->DISMISSED:Lcom/squareup/settings/FirstPaymentTooltipStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/settings/FirstPaymentTooltipStatus;->$VALUES:[Lcom/squareup/settings/FirstPaymentTooltipStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/settings/FirstPaymentTooltipStatus;
    .locals 1

    .line 4
    const-class v0, Lcom/squareup/settings/FirstPaymentTooltipStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/FirstPaymentTooltipStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/settings/FirstPaymentTooltipStatus;
    .locals 1

    .line 4
    sget-object v0, Lcom/squareup/settings/FirstPaymentTooltipStatus;->$VALUES:[Lcom/squareup/settings/FirstPaymentTooltipStatus;

    invoke-virtual {v0}, [Lcom/squareup/settings/FirstPaymentTooltipStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/settings/FirstPaymentTooltipStatus;

    return-object v0
.end method
