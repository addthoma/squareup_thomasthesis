.class public Lcom/squareup/settings/DeviceSettingsModule;
.super Ljava/lang/Object;
.source "DeviceSettingsModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# static fields
.field private static final DEVICE_ID:Ljava/lang/String; = "device-id"

.field private static final DEVICE_NAME:Ljava/lang/String; = "device-name"

.field private static final X2COMMS_BUS_TYPE:Ljava/lang/String; = "x2comms-bus-type"

.field private static final X2COMMS_REMOTE_HOST:Ljava/lang/String; = "x2comms-remote-host"

.field private static final X2COMMS_USE_HEALTH_CHECKER:Ljava/lang/String; = "x2comms-use-health-checker"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAdIdCache(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 169
    new-instance v0, Lcom/squareup/settings/StringLocalSetting;

    const-string v1, "adIdCache"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideDailyLocalSetting(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 145
    new-instance v0, Lcom/squareup/settings/LongLocalSetting;

    const-string v1, "r12-recent-blocking-update"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/LongLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideDevPrinterState(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 159
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "dev-hardware-printers-state"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static provideDeviceIdProvider(Landroid/app/Application;Landroid/telephony/TelephonyManager;Ljavax/inject/Provider;Landroid/content/SharedPreferences;Lcom/squareup/util/SystemProperties;)Lcom/squareup/settings/DeviceIdProvider;
    .locals 6
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Landroid/telephony/TelephonyManager;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/SharedPreferences;",
            "Lcom/squareup/util/SystemProperties;",
            ")",
            "Lcom/squareup/settings/DeviceIdProvider;"
        }
    .end annotation

    .line 51
    new-instance v4, Lcom/squareup/settings/StringLocalSetting;

    const-string v0, "device-id"

    invoke-direct {v4, p3, v0}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 52
    new-instance p3, Lcom/squareup/settings/RealDeviceIdProvider;

    move-object v0, p3

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/settings/RealDeviceIdProvider;-><init>(Landroid/app/Application;Landroid/telephony/TelephonyManager;Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/util/SystemProperties;)V

    return-object p3
.end method

.method static provideDeviceInitialized(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 82
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "device-settings-initialized"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static provideDeviceName(Lcom/squareup/settings/LocalSetting;)Ljava/lang/String;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 63
    invoke-interface {p0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method

.method static provideDeviceNameSetting(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 58
    new-instance v0, Lcom/squareup/settings/StringLocalSetting;

    const-string v1, "device-name"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideHasConnectedToR6(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 102
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "r6-has-connected"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static provideNewInvoiceFeaturesTutorialTipsDismissed(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashSet<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 151
    new-instance v0, Lcom/squareup/settings/DeviceSettingsModule$2;

    invoke-direct {v0}, Lcom/squareup/settings/DeviceSettingsModule$2;-><init>()V

    .line 152
    invoke-virtual {v0}, Lcom/squareup/settings/DeviceSettingsModule$2;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v1, "new-invoice-feature-tutorial-tips-dismissed"

    .line 153
    invoke-static {p0, v1, p1, v0}, Lcom/squareup/settings/GsonLocalSetting;->forType(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/reflect/Type;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p0

    return-object p0
.end method

.method static provideNfcReaderHasConnected(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 112
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "nfc-reader-has-connected"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static provideR12FirstTimeTutorialViewed(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 140
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "r12-ftu-tutorial-viewed"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static provideR12HasConnected(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 107
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "r12-has-connected"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static provideR6FirstTimeVideoViewed(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 134
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "r6-ftu-video-viewed"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static provideRequestedPermissions(Landroid/content/SharedPreferences;)Lcom/squareup/settings/EnumSetLocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/EnumSetLocalSetting<",
            "Lcom/squareup/systempermissions/SystemPermission;",
            ">;"
        }
    .end annotation

    .line 77
    new-instance v0, Lcom/squareup/settings/EnumSetLocalSetting;

    const-class v1, Lcom/squareup/systempermissions/SystemPermission;

    const-string v2, "requestedPermissions"

    invoke-direct {v0, p0, v2, v1}, Lcom/squareup/settings/EnumSetLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Class;)V

    return-object v0
.end method

.method static provideT2DeviceTourViewed(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 129
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "t2-device-tour-viewed"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideUiPrinter(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 164
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "dev-ui-printer-connected"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static provideX2CommsRemoteHostId(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 97
    new-instance v0, Lcom/squareup/settings/StringLocalSetting;

    const-string/jumbo v1, "x2comms-remote-host"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideX2CommsUseHealthCheckerId(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 87
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string/jumbo v1, "x2comms-use-health-checker"

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static provideX2CommsUseLocalBus(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/X2CommsBusType;",
            ">;"
        }
    .end annotation

    .line 92
    new-instance v0, Lcom/squareup/settings/EnumLocalSetting;

    const-class v1, Lcom/squareup/settings/X2CommsBusType;

    const-string/jumbo v2, "x2comms-bus-type"

    invoke-direct {v0, p0, v2, v1}, Lcom/squareup/settings/EnumLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Class;)V

    return-object v0
.end method

.method static provideX2DeviceTourViewed(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 118
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "education-tour-viewed"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static provideX2FeatureTourItemsSeen(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const-string/jumbo v0, "x2-feature-tour-items-seen"

    .line 124
    invoke-virtual {p0, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getStringSet(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static providesFirstInvoiceTutorialViewed(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 175
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "first-invoice-tutorial-viewed"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static providesLoyaltyAdjustPointsTutorialViewed(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 187
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "loyalty-adjust-points-tutorial-viewed"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static providesLoyaltyEnrollTutorialViewed(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 181
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "loyalty-enroll-tutorial-viewed"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static providesLoyaltyRedeemRewardsTutorialViewed(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 193
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "loyalty-redeem-rewards-tutorial-viewed"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method


# virtual methods
.method provideDeviceDetails(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
            ">;>;"
        }
    .end annotation

    .line 69
    new-instance v0, Lcom/squareup/settings/DeviceSettingsModule$1;

    invoke-direct {v0, p0}, Lcom/squareup/settings/DeviceSettingsModule$1;-><init>(Lcom/squareup/settings/DeviceSettingsModule;)V

    .line 70
    invoke-virtual {v0}, Lcom/squareup/settings/DeviceSettingsModule$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v1, "saved-device-details"

    .line 71
    invoke-static {p1, v1, p2, v0}, Lcom/squareup/settings/GsonLocalSetting;->forType(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/reflect/Type;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p1

    return-object p1
.end method
