.class public final Lcom/squareup/settings/DeviceSettingsSettingsInitializer_Factory;
.super Ljava/lang/Object;
.source "DeviceSettingsSettingsInitializer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/DeviceSettingsSettingsInitializer;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceInitializedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final executorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/settings/DeviceSettingsSettingsInitializer_Factory;->deviceInitializedProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/settings/DeviceSettingsSettingsInitializer_Factory;->executorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsSettingsInitializer_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;)",
            "Lcom/squareup/settings/DeviceSettingsSettingsInitializer_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/settings/DeviceSettingsSettingsInitializer_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/settings/DeviceSettingsSettingsInitializer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/LocalSetting;Ljava/util/concurrent/Executor;)Lcom/squareup/settings/DeviceSettingsSettingsInitializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/squareup/settings/DeviceSettingsSettingsInitializer;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/settings/DeviceSettingsSettingsInitializer;

    invoke-direct {v0, p0, p1}, Lcom/squareup/settings/DeviceSettingsSettingsInitializer;-><init>(Lcom/squareup/settings/LocalSetting;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/DeviceSettingsSettingsInitializer;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/settings/DeviceSettingsSettingsInitializer_Factory;->deviceInitializedProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    iget-object v1, p0, Lcom/squareup/settings/DeviceSettingsSettingsInitializer_Factory;->executorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {v0, v1}, Lcom/squareup/settings/DeviceSettingsSettingsInitializer_Factory;->newInstance(Lcom/squareup/settings/LocalSetting;Ljava/util/concurrent/Executor;)Lcom/squareup/settings/DeviceSettingsSettingsInitializer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/settings/DeviceSettingsSettingsInitializer_Factory;->get()Lcom/squareup/settings/DeviceSettingsSettingsInitializer;

    move-result-object v0

    return-object v0
.end method
