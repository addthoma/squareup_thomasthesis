.class public Lcom/squareup/settings/AddTendersRequestServerIds;
.super Ljava/lang/Object;
.source "AddTendersRequestServerIds.java"


# instance fields
.field private final billServerId:Lcom/squareup/protos/client/IdPair;

.field private final tenderServerId:Lcom/squareup/protos/client/IdPair;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V
    .locals 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "billServerId"

    .line 11
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/IdPair;

    iput-object p1, p0, Lcom/squareup/settings/AddTendersRequestServerIds;->billServerId:Lcom/squareup/protos/client/IdPair;

    const-string/jumbo p1, "tenderServerId"

    .line 12
    invoke-static {p2, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/IdPair;

    iput-object p1, p0, Lcom/squareup/settings/AddTendersRequestServerIds;->tenderServerId:Lcom/squareup/protos/client/IdPair;

    return-void
.end method


# virtual methods
.method public getBillServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/settings/AddTendersRequestServerIds;->billServerId:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 19
    :cond_0
    iget-object p1, p0, Lcom/squareup/settings/AddTendersRequestServerIds;->billServerId:Lcom/squareup/protos/client/IdPair;

    return-object p1
.end method

.method public getTenderServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/settings/AddTendersRequestServerIds;->tenderServerId:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 26
    :cond_0
    iget-object p1, p0, Lcom/squareup/settings/AddTendersRequestServerIds;->tenderServerId:Lcom/squareup/protos/client/IdPair;

    return-object p1
.end method
