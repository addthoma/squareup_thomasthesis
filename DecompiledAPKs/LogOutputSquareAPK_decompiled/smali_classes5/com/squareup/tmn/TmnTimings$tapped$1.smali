.class final Lcom/squareup/tmn/TmnTimings$tapped$1;
.super Ljava/lang/Object;
.source "TmnTimings.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/TmnTimings;->tapped()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $connectivity:Ljava/lang/String;

.field final synthetic $timeNanos:J

.field final synthetic this$0:Lcom/squareup/tmn/TmnTimings;


# direct methods
.method constructor <init>(Lcom/squareup/tmn/TmnTimings;JLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tmn/TmnTimings$tapped$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    iput-wide p2, p0, Lcom/squareup/tmn/TmnTimings$tapped$1;->$timeNanos:J

    iput-object p4, p0, Lcom/squareup/tmn/TmnTimings$tapped$1;->$connectivity:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 122
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings$tapped$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    sget-object v1, Lcom/squareup/tmn/Phase;->AFTER_TAP:Lcom/squareup/tmn/Phase;

    invoke-static {v0, v1}, Lcom/squareup/tmn/TmnTimings;->access$setCurrentPhase$p(Lcom/squareup/tmn/TmnTimings;Lcom/squareup/tmn/Phase;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings$tapped$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimings$tapped$1;->$timeNanos:J

    invoke-static {v0, v1, v2}, Lcom/squareup/tmn/TmnTimings;->access$setTapTimeNanos$p(Lcom/squareup/tmn/TmnTimings;J)V

    .line 124
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings$tapped$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    iget-object v1, p0, Lcom/squareup/tmn/TmnTimings$tapped$1;->$connectivity:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/tmn/TmnTimings;->access$setTapConnectivity$p(Lcom/squareup/tmn/TmnTimings;Ljava/lang/String;)V

    return-void
.end method
