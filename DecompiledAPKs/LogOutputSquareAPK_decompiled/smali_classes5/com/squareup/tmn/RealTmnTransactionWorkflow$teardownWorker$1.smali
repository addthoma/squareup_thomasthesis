.class public final Lcom/squareup/tmn/RealTmnTransactionWorkflow$teardownWorker$1;
.super Lcom/squareup/workflow/LifecycleWorker;
.source "RealTmnTransactionWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/RealTmnTransactionWorkflow;-><init>(Lcom/squareup/server/felica/FelicaService;Lcom/squareup/tmn/CardReaderHelper;Lcom/squareup/tmn/audio/TmnAudioPlayer;Lcom/squareup/tmn/TmnObservablesHelper;Lcom/squareup/tmn/EmoneyAnalyticsLogger;Lcom/squareup/tmn/MiryoWorkerDelayer;Lcom/squareup/tmn/TmnTimings;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0011\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"
    }
    d2 = {
        "com/squareup/tmn/RealTmnTransactionWorkflow$teardownWorker$1",
        "Lcom/squareup/workflow/LifecycleWorker;",
        "onStopped",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 124
    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$teardownWorker$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-direct {p0}, Lcom/squareup/workflow/LifecycleWorker;-><init>()V

    return-void
.end method


# virtual methods
.method public onStopped()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Teardown tmn transaction workflow"

    .line 126
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$teardownWorker$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-virtual {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->getMiryoWorkerDelayer()Lcom/squareup/tmn/MiryoWorkerDelayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/tmn/MiryoWorkerDelayer;->reset()V

    .line 128
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$teardownWorker$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getCardReaderHelper$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/CardReaderHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tmn/CardReaderHelper;->resetCardReader$impl_release()V

    .line 129
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$teardownWorker$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getAnalytics$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->sendEvents()V

    return-void
.end method
