.class final Lcom/squareup/tmn/TmnIndividualEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "TmnTimings.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0012\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0082\u0008\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\tH\u00c6\u0003J\t\u0010\u0019\u001a\u00020\tH\u00c6\u0003J;\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u00d6\u0003J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001J\t\u0010!\u001a\u00020\u0007H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\n\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/tmn/TmnIndividualEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "what",
        "Lcom/squareup/tmn/What;",
        "phase",
        "Lcom/squareup/tmn/Phase;",
        "transactionId",
        "",
        "timestampMillis",
        "",
        "timestampNanos",
        "(Lcom/squareup/tmn/What;Lcom/squareup/tmn/Phase;Ljava/lang/String;JJ)V",
        "getPhase",
        "()Lcom/squareup/tmn/Phase;",
        "getTimestampMillis",
        "()J",
        "getTimestampNanos",
        "getTransactionId",
        "()Ljava/lang/String;",
        "getWhat",
        "()Lcom/squareup/tmn/What;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "tmn-timings_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final phase:Lcom/squareup/tmn/Phase;

.field private final timestampMillis:J

.field private final timestampNanos:J

.field private final transactionId:Ljava/lang/String;

.field private final what:Lcom/squareup/tmn/What;


# direct methods
.method public constructor <init>(Lcom/squareup/tmn/What;Lcom/squareup/tmn/Phase;Ljava/lang/String;JJ)V
    .locals 2

    const-string/jumbo v0, "what"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phase"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionId"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 379
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->TIMING:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "EMoney: Timing Event"

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/tmn/TmnIndividualEvent;->what:Lcom/squareup/tmn/What;

    iput-object p2, p0, Lcom/squareup/tmn/TmnIndividualEvent;->phase:Lcom/squareup/tmn/Phase;

    iput-object p3, p0, Lcom/squareup/tmn/TmnIndividualEvent;->transactionId:Ljava/lang/String;

    iput-wide p4, p0, Lcom/squareup/tmn/TmnIndividualEvent;->timestampMillis:J

    iput-wide p6, p0, Lcom/squareup/tmn/TmnIndividualEvent;->timestampNanos:J

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tmn/TmnIndividualEvent;Lcom/squareup/tmn/What;Lcom/squareup/tmn/Phase;Ljava/lang/String;JJILjava/lang/Object;)Lcom/squareup/tmn/TmnIndividualEvent;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/tmn/TmnIndividualEvent;->what:Lcom/squareup/tmn/What;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/squareup/tmn/TmnIndividualEvent;->phase:Lcom/squareup/tmn/Phase;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/tmn/TmnIndividualEvent;->transactionId:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-wide p4, p0, Lcom/squareup/tmn/TmnIndividualEvent;->timestampMillis:J

    :cond_3
    move-wide v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-wide p6, p0, Lcom/squareup/tmn/TmnIndividualEvent;->timestampNanos:J

    :cond_4
    move-wide v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-object p5, v0

    move-wide p6, v1

    move-wide p8, v3

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/tmn/TmnIndividualEvent;->copy(Lcom/squareup/tmn/What;Lcom/squareup/tmn/Phase;Ljava/lang/String;JJ)Lcom/squareup/tmn/TmnIndividualEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/tmn/What;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/TmnIndividualEvent;->what:Lcom/squareup/tmn/What;

    return-object v0
.end method

.method public final component2()Lcom/squareup/tmn/Phase;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/TmnIndividualEvent;->phase:Lcom/squareup/tmn/Phase;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/TmnIndividualEvent;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/TmnIndividualEvent;->timestampMillis:J

    return-wide v0
.end method

.method public final component5()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/TmnIndividualEvent;->timestampNanos:J

    return-wide v0
.end method

.method public final copy(Lcom/squareup/tmn/What;Lcom/squareup/tmn/Phase;Ljava/lang/String;JJ)Lcom/squareup/tmn/TmnIndividualEvent;
    .locals 9

    const-string/jumbo v0, "what"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phase"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionId"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tmn/TmnIndividualEvent;

    move-object v1, v0

    move-wide v5, p4

    move-wide v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/squareup/tmn/TmnIndividualEvent;-><init>(Lcom/squareup/tmn/What;Lcom/squareup/tmn/Phase;Ljava/lang/String;JJ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tmn/TmnIndividualEvent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tmn/TmnIndividualEvent;

    iget-object v0, p0, Lcom/squareup/tmn/TmnIndividualEvent;->what:Lcom/squareup/tmn/What;

    iget-object v1, p1, Lcom/squareup/tmn/TmnIndividualEvent;->what:Lcom/squareup/tmn/What;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tmn/TmnIndividualEvent;->phase:Lcom/squareup/tmn/Phase;

    iget-object v1, p1, Lcom/squareup/tmn/TmnIndividualEvent;->phase:Lcom/squareup/tmn/Phase;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tmn/TmnIndividualEvent;->transactionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/tmn/TmnIndividualEvent;->transactionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/TmnIndividualEvent;->timestampMillis:J

    iget-wide v2, p1, Lcom/squareup/tmn/TmnIndividualEvent;->timestampMillis:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/TmnIndividualEvent;->timestampNanos:J

    iget-wide v2, p1, Lcom/squareup/tmn/TmnIndividualEvent;->timestampNanos:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPhase()Lcom/squareup/tmn/Phase;
    .locals 1

    .line 375
    iget-object v0, p0, Lcom/squareup/tmn/TmnIndividualEvent;->phase:Lcom/squareup/tmn/Phase;

    return-object v0
.end method

.method public final getTimestampMillis()J
    .locals 2

    .line 377
    iget-wide v0, p0, Lcom/squareup/tmn/TmnIndividualEvent;->timestampMillis:J

    return-wide v0
.end method

.method public final getTimestampNanos()J
    .locals 2

    .line 378
    iget-wide v0, p0, Lcom/squareup/tmn/TmnIndividualEvent;->timestampNanos:J

    return-wide v0
.end method

.method public final getTransactionId()Ljava/lang/String;
    .locals 1

    .line 376
    iget-object v0, p0, Lcom/squareup/tmn/TmnIndividualEvent;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public final getWhat()Lcom/squareup/tmn/What;
    .locals 1

    .line 374
    iget-object v0, p0, Lcom/squareup/tmn/TmnIndividualEvent;->what:Lcom/squareup/tmn/What;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/tmn/TmnIndividualEvent;->what:Lcom/squareup/tmn/What;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tmn/TmnIndividualEvent;->phase:Lcom/squareup/tmn/Phase;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tmn/TmnIndividualEvent;->transactionId:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/TmnIndividualEvent;->timestampMillis:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/TmnIndividualEvent;->timestampNanos:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TmnIndividualEvent(what="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/TmnIndividualEvent;->what:Lcom/squareup/tmn/What;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/TmnIndividualEvent;->phase:Lcom/squareup/tmn/Phase;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", transactionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/TmnIndividualEvent;->transactionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", timestampMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/TmnIndividualEvent;->timestampMillis:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", timestampNanos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/TmnIndividualEvent;->timestampNanos:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
