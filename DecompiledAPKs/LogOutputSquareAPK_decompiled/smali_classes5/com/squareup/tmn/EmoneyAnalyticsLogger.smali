.class public final Lcom/squareup/tmn/EmoneyAnalyticsLogger;
.super Ljava/lang/Object;
.source "EmoneyAnalyticsLogger.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEmoneyAnalyticsLogger.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EmoneyAnalyticsLogger.kt\ncom/squareup/tmn/EmoneyAnalyticsLogger\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,29:1\n1642#2,2:30\n*E\n*S KotlinDebug\n*F\n+ 1 EmoneyAnalyticsLogger.kt\ncom/squareup/tmn/EmoneyAnalyticsLogger\n*L\n25#1,2:30\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\tJ\u0006\u0010\r\u001a\u00020\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/tmn/EmoneyAnalyticsLogger;",
        "",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "dateTimeFactory",
        "Lcom/squareup/util/DateTimeFactory;",
        "events",
        "",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "addEvent",
        "",
        "event",
        "sendEvents",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final dateTimeFactory:Lcom/squareup/util/DateTimeFactory;

.field private events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/eventstream/v1/EventStreamEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->analytics:Lcom/squareup/analytics/Analytics;

    .line 16
    new-instance p1, Lcom/squareup/util/DateTimeFactory;

    invoke-direct {p1}, Lcom/squareup/util/DateTimeFactory;-><init>()V

    iput-object p1, p0, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->dateTimeFactory:Lcom/squareup/util/DateTimeFactory;

    .line 17
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->events:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final addEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iget-object v0, p0, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->dateTimeFactory:Lcom/squareup/util/DateTimeFactory;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/DateTimeFactory;->forMillis(J)Lcom/squareup/protos/common/time/DateTime;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/eventstream/v1/EventStreamEvent;->overrideTimestamp:Lcom/squareup/protos/common/time/DateTime;

    .line 21
    iget-object v0, p0, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->events:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final sendEvents()V
    .locals 3

    .line 25
    iget-object v0, p0, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->events:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 30
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 25
    iget-object v2, p0, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v2, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->events:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method
