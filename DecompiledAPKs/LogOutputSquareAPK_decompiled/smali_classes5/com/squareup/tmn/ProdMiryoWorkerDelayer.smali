.class public final Lcom/squareup/tmn/ProdMiryoWorkerDelayer;
.super Ljava/lang/Object;
.source "ProdMiryoWorkerDelayer.kt"

# interfaces
.implements Lcom/squareup/tmn/MiryoWorkerDelayer;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\t\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\u0008H\u0016J\u0008\u0010\n\u001a\u00020\u0008H\u0016J\u0008\u0010\u000b\u001a\u00020\u0008H\u0016J\u0008\u0010\u000c\u001a\u00020\u0008H\u0016J\u0010\u0010\r\u001a\u00020\u00082\u0006\u0010\u000e\u001a\u00020\u0004H\u0016J\u0010\u0010\u000f\u001a\u00020\u00082\u0006\u0010\u0010\u001a\u00020\u0006H\u0016\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/tmn/ProdMiryoWorkerDelayer;",
        "Lcom/squareup/tmn/MiryoWorkerDelayer;",
        "()V",
        "getDelay",
        "",
        "getMiryoType",
        "Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;",
        "maybeDelayAfterFirstRoundTripAfterMiryo",
        "",
        "maybeDelayAfterWriteNotify",
        "maybeDelayBeforeWriteNotify",
        "miryoStarted",
        "reset",
        "setDelay",
        "delayMs",
        "setMiryoType",
        "miryoType",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDelay()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getMiryoType()Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;->BeforeWriteNotify:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;

    return-object v0
.end method

.method public maybeDelayAfterFirstRoundTripAfterMiryo()V
    .locals 0

    return-void
.end method

.method public maybeDelayAfterWriteNotify()V
    .locals 0

    return-void
.end method

.method public maybeDelayBeforeWriteNotify()V
    .locals 0

    return-void
.end method

.method public miryoStarted()V
    .locals 0

    return-void
.end method

.method public reset()V
    .locals 0

    return-void
.end method

.method public setDelay(J)V
    .locals 0

    return-void
.end method

.method public setMiryoType(Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;)V
    .locals 1

    const-string v0, "miryoType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
