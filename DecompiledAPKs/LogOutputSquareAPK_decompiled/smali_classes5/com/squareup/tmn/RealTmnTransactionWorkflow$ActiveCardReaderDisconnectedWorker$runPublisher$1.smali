.class final Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker$runPublisher$1;
.super Ljava/lang/Object;
.source "RealTmnTransactionWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;->runPublisher()Lorg/reactivestreams/Publisher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/dipper/events/TmnEvent$TmnCardReaderConnectionEvent$OnActiveCardReaderDisconnected;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/dipper/events/TmnEvent$TmnCardReaderConnectionEvent$OnActiveCardReaderDisconnected;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;


# direct methods
.method constructor <init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker$runPublisher$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/dipper/events/TmnEvent$TmnCardReaderConnectionEvent$OnActiveCardReaderDisconnected;)V
    .locals 0

    .line 609
    iget-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker$runPublisher$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;

    iget-object p1, p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getTmnAudioPlayer$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/audio/TmnAudioPlayer;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/tmn/audio/TmnAudioPlayer;->stopAudio$impl_release()V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 599
    check-cast p1, Lcom/squareup/dipper/events/TmnEvent$TmnCardReaderConnectionEvent$OnActiveCardReaderDisconnected;

    invoke-virtual {p0, p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker$runPublisher$1;->accept(Lcom/squareup/dipper/events/TmnEvent$TmnCardReaderConnectionEvent$OnActiveCardReaderDisconnected;)V

    return-void
.end method
