.class public final Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen$Factory;
.super Ljava/lang/Object;
.source "OpenCashDrawerShiftDialogScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOpenCashDrawerShiftDialogScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OpenCashDrawerShiftDialogScreen.kt\ncom/squareup/signout/OpenCashDrawerShiftDialogScreen$Factory\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,39:1\n52#2:40\n*E\n*S KotlinDebug\n*F\n+ 1 OpenCashDrawerShiftDialogScreen.kt\ncom/squareup/signout/OpenCashDrawerShiftDialogScreen$Factory\n*L\n20#1:40\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen$Factory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "sign-out_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    const-class v0, Lcom/squareup/reports/applet/ReportsAppletGateway$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 20
    check-cast v0, Lcom/squareup/reports/applet/ReportsAppletGateway$ParentComponent;

    .line 21
    invoke-interface {v0}, Lcom/squareup/reports/applet/ReportsAppletGateway$ParentComponent;->reportsAppletGateway()Lcom/squareup/reports/applet/ReportsAppletGateway;

    move-result-object v0

    .line 24
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 25
    sget p1, Lcom/squareup/signout/R$string;->cannot_sign_out:I

    invoke-virtual {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 26
    sget v1, Lcom/squareup/signout/R$string;->cannot_sign_out_message_open_cash_drawer_shift:I

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 27
    sget v1, Lcom/squareup/signout/R$string;->end_cash_drawer_shift:I

    new-instance v2, Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen$Factory$create$1;

    invoke-direct {v2, v0}, Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen$Factory$create$1;-><init>(Lcom/squareup/reports/applet/ReportsAppletGateway;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 30
    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 31
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 23
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(\n          T\u2026        .create()\n      )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
