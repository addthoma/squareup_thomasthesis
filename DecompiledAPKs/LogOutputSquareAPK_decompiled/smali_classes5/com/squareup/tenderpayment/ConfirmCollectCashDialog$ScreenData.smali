.class public final Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;
.super Ljava/lang/Object;
.source "ConfirmCollectCashDialogScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/ConfirmCollectCashDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u000b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\t\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008R\u0011\u0010\u000b\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u000f\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0008\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;",
        "",
        "tenderedAmount",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/protos/common/Money;)V",
        "message",
        "",
        "getMessage",
        "()I",
        "negativeButton",
        "getNegativeButton",
        "positiveButton",
        "getPositiveButton",
        "getTenderedAmount",
        "()Lcom/squareup/protos/common/Money;",
        "title",
        "getTitle",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final message:I

.field private final negativeButton:I

.field private final positiveButton:I

.field private final tenderedAmount:Lcom/squareup/protos/common/Money;

.field private final title:I


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;)V
    .locals 1

    const-string/jumbo v0, "tenderedAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;->tenderedAmount:Lcom/squareup/protos/common/Money;

    .line 19
    sget p1, Lcom/squareup/sqwidgets/R$string;->dialog_okay:I

    iput p1, p0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;->positiveButton:I

    .line 20
    sget p1, Lcom/squareup/sqwidgets/R$string;->dialog_cancel:I

    iput p1, p0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;->negativeButton:I

    .line 21
    sget p1, Lcom/squareup/tenderworkflow/R$string;->confirm_collect_cash_title:I

    iput p1, p0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;->title:I

    .line 22
    sget p1, Lcom/squareup/tenderworkflow/R$string;->confirm_collect_cash_message:I

    iput p1, p0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;->message:I

    return-void
.end method


# virtual methods
.method public final getMessage()I
    .locals 1

    .line 22
    iget v0, p0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;->message:I

    return v0
.end method

.method public final getNegativeButton()I
    .locals 1

    .line 20
    iget v0, p0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;->negativeButton:I

    return v0
.end method

.method public final getPositiveButton()I
    .locals 1

    .line 19
    iget v0, p0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;->positiveButton:I

    return v0
.end method

.method public final getTenderedAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;->tenderedAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getTitle()I
    .locals 1

    .line 21
    iget v0, p0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;->title:I

    return v0
.end method
