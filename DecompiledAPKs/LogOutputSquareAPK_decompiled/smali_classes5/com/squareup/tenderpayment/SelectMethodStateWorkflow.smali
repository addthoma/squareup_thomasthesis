.class public final Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "SelectMethodStateWorkflow.kt"

# interfaces
.implements Lcom/squareup/tenderpayment/SelectMethodV2Workflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;,
        Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/tenderpayment/SelectMethodV2Workflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectMethodStateWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectMethodStateWorkflow.kt\ncom/squareup/tenderpayment/SelectMethodStateWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt\n*L\n1#1,1372:1\n1341#1:1426\n1366#1:1427\n1341#1:1428\n1366#1:1429\n1341#1:1430\n1366#1:1431\n1341#1:1432\n1366#1:1433\n1341#1:1434\n1366#1:1435\n1341#1:1436\n1366#1:1437\n1341#1:1438\n1366#1:1439\n1341#1:1440\n1366#1:1441\n1341#1:1442\n1366#1:1443\n1341#1:1444\n1366#1:1445\n1341#1:1446\n1366#1:1447\n1341#1:1455\n1366#1:1456\n1341#1:1457\n1366#1:1458\n1341#1:1459\n1366#1:1460\n1341#1:1461\n1366#1:1462\n1341#1:1463\n1366#1:1464\n1341#1:1465\n1366#1:1466\n1341#1:1467\n1366#1:1468\n1341#1:1469\n1366#1:1470\n1341#1:1471\n1366#1:1472\n1341#1:1473\n1366#1:1474\n1341#1:1475\n1366#1:1476\n1341#1:1477\n1366#1:1478\n1341#1:1479\n1366#1:1480\n1341#1:1481\n1366#1:1482\n41#2:1373\n56#2,2:1374\n41#2:1377\n56#2,2:1378\n41#2:1381\n56#2,2:1382\n41#2:1387\n56#2,2:1388\n41#2:1518\n56#2,2:1519\n276#3:1376\n276#3:1380\n276#3:1384\n276#3:1386\n276#3:1390\n276#3:1521\n240#4:1385\n179#5,3:1391\n199#5,4:1394\n179#5,3:1398\n199#5,4:1401\n179#5,3:1405\n199#5,4:1408\n179#5,3:1412\n199#5,4:1415\n179#5,3:1419\n199#5,4:1422\n179#5,3:1448\n199#5,4:1451\n179#5,3:1483\n199#5,4:1486\n179#5,3:1490\n199#5,4:1493\n179#5,3:1497\n199#5,4:1500\n179#5,3:1504\n199#5,4:1507\n179#5,3:1511\n199#5,4:1514\n*E\n*S KotlinDebug\n*F\n+ 1 SelectMethodStateWorkflow.kt\ncom/squareup/tenderpayment/SelectMethodStateWorkflow\n*L\n674#1:1426\n674#1:1427\n687#1:1428\n687#1:1429\n703#1:1430\n703#1:1431\n710#1:1432\n710#1:1433\n731#1:1434\n731#1:1435\n766#1:1436\n766#1:1437\n788#1:1438\n788#1:1439\n796#1:1440\n796#1:1441\n814#1:1442\n814#1:1443\n824#1:1444\n824#1:1445\n836#1:1446\n836#1:1447\n925#1:1455\n925#1:1456\n930#1:1457\n930#1:1458\n936#1:1459\n936#1:1460\n948#1:1461\n948#1:1462\n956#1:1463\n956#1:1464\n965#1:1465\n965#1:1466\n980#1:1467\n980#1:1468\n985#1:1469\n985#1:1470\n990#1:1471\n990#1:1472\n999#1:1473\n999#1:1474\n1009#1:1475\n1009#1:1476\n1013#1:1477\n1013#1:1478\n1017#1:1479\n1017#1:1480\n1023#1:1481\n1023#1:1482\n314#1:1373\n314#1,2:1374\n322#1:1377\n322#1,2:1378\n327#1:1381\n327#1,2:1382\n370#1:1387\n370#1,2:1388\n257#1:1518\n257#1,2:1519\n314#1:1376\n322#1:1380\n327#1:1384\n332#1:1386\n370#1:1390\n257#1:1521\n332#1:1385\n603#1,3:1391\n603#1,4:1394\n630#1,3:1398\n630#1,4:1401\n648#1,3:1405\n648#1,4:1408\n654#1,3:1412\n654#1,4:1415\n661#1,3:1419\n661#1,4:1422\n889#1,3:1448\n889#1,4:1451\n1036#1,3:1483\n1036#1,4:1486\n1067#1,3:1490\n1067#1,4:1493\n1073#1,3:1497\n1073#1,4:1500\n1080#1,3:1504\n1080#1,4:1507\n1085#1,3:1511\n1085#1,4:1514\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00b6\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000e\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u0010\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00c7\u00012\u00020\u00012@\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u0002:\u0004\u00c7\u0001\u00c8\u0001B\u00e7\u0001\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u0012\u0006\u0010\u001d\u001a\u00020\u001e\u0012\u0006\u0010\u001f\u001a\u00020 \u0012\u0006\u0010!\u001a\u00020\"\u0012\u0006\u0010#\u001a\u00020$\u0012\u0006\u0010%\u001a\u00020&\u0012\u0006\u0010\'\u001a\u00020(\u0012\u0006\u0010)\u001a\u00020*\u0012\u0006\u0010+\u001a\u00020,\u0012\u0006\u0010-\u001a\u00020.\u0012\u0006\u0010/\u001a\u000200\u0012\u0006\u00101\u001a\u000202\u0012\u0006\u00103\u001a\u000204\u0012\u0006\u00105\u001a\u000206\u0012\u0006\u00107\u001a\u000208\u0012\u0006\u00109\u001a\u00020:\u0012\u0006\u0010;\u001a\u00020<\u0012\u0006\u0010=\u001a\u00020>\u0012\u0006\u0010?\u001a\u00020@\u0012\u0006\u0010A\u001a\u00020B\u00a2\u0006\u0002\u0010CJN\u0010R\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050S\"\n\u0008\u0000\u0010T\u0018\u0001*\u00020\u00042+\u0008\u0004\u0010U\u001a%\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002HT\u0012\u0004\u0012\u00020\u00050W\u0012\u0004\u0012\u00020X0V\u00a2\u0006\u0002\u0008YH\u0082\u0008J\u0014\u0010Z\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u0014\u0010[\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u0014\u0010\\\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u0014\u0010]\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u0014\u0010^\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u0014\u0010_\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J&\u0010`\u001a\u00020a2\u0006\u0010b\u001a\u00020\u00032\u000c\u0010c\u001a\u0008\u0012\u0004\u0012\u00020e0d2\u0006\u0010f\u001a\u00020gH\u0002J\u0016\u0010h\u001a\u00020M2\u000c\u0010i\u001a\u0008\u0012\u0004\u0012\u00020M0jH\u0002J\u0010\u0010k\u001a\u00020l2\u0006\u0010b\u001a\u00020\u0003H\u0002J\u0010\u0010m\u001a\u00020g2\u0006\u0010b\u001a\u00020\u0003H\u0002J\u001a\u0010n\u001a\u00020\u00042\u0006\u0010o\u001a\u00020\u00032\u0008\u0010p\u001a\u0004\u0018\u00010qH\u0016J(\u0010r\u001a\u00020s2\u0006\u0010t\u001a\u00020M2\u0006\u0010u\u001a\u00020M2\u0006\u0010v\u001a\u00020M2\u0006\u0010w\u001a\u00020MH\u0002J\u0014\u0010x\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u0014\u0010y\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u0014\u0010z\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u0014\u0010{\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u0014\u0010|\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J/\u0010}\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050S2\u0006\u0010~\u001a\u00020l2\u0007\u0010\u007f\u001a\u00030\u0080\u00012\u0008\u0010\u0081\u0001\u001a\u00030\u0080\u0001H\u0002J.\u0010\u0082\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050S2\u0007\u0010\u0083\u0001\u001a\u00020g2\u0006\u0010b\u001a\u00020\u00032\u0006\u0010~\u001a\u00020lH\u0002J\u0015\u0010\u0084\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u0015\u0010\u0085\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u0015\u0010\u0086\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u0015\u0010\u0087\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J)\u0010\u0088\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050S2\u0008\u0010\u0089\u0001\u001a\u00030\u008a\u00012\u0008\u0010\u008b\u0001\u001a\u00030\u0080\u0001H\u0002J(\u0010\u008c\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050S2\u0008\u0010\u008d\u0001\u001a\u00030\u008e\u00012\u0007\u0010\u0083\u0001\u001a\u00020\u0004H\u0002J\u001f\u0010\u008f\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050S2\u0008\u0010\u008d\u0001\u001a\u00030\u0090\u0001H\u0002J\u001f\u0010\u0091\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050S2\u0008\u0010\u008d\u0001\u001a\u00030\u0092\u0001H\u0002J0\u0010\u0093\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050S2\u0008\u0010\u008d\u0001\u001a\u00030\u0094\u00012\u0007\u0010\u0083\u0001\u001a\u00020\u00042\u0006\u0010b\u001a\u00020\u0003H\u0002J\u001f\u0010\u0095\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050S2\u0008\u0010\u008d\u0001\u001a\u00030\u0096\u0001H\u0002J\u0015\u0010\u0097\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J%\u0010\u0098\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050S2\u0006\u0010b\u001a\u00020\u00032\u0006\u0010~\u001a\u00020lH\u0002J\u001e\u0010\u0099\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050S2\u0007\u0010\u009a\u0001\u001a\u00020lH\u0002J\u0015\u0010\u009b\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u0015\u0010\u009c\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u0015\u0010\u009d\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u001f\u0010\u009e\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050S2\u0008\u0010\u009f\u0001\u001a\u00030\u00a0\u0001H\u0002J\u0015\u0010\u00a1\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u0012\u0010\u00a2\u0001\u001a\u00020X2\u0007\u0010\u00a3\u0001\u001a\u00020\u0005H\u0002J\u001b\u0010\u00a4\u0001\u001a\u00020M2\u0007\u0010\u00a5\u0001\u001a\u00020M2\u0007\u0010\u00a6\u0001\u001a\u00020MH\u0002J\t\u0010\u00a7\u0001\u001a\u00020XH\u0002JR\u0010\u00a8\u0001\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010o\u001a\u00020\u00032\u0007\u0010\u0083\u0001\u001a\u00020\u00042\u0014\u0010\u00a9\u0001\u001a\u000f\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00aa\u0001H\u0016J5\u0010v\u001a\u00020M2\u0007\u0010\u00a5\u0001\u001a\u00020M2\u0007\u0010\u00a6\u0001\u001a\u00020M2\u0007\u0010\u00ab\u0001\u001a\u00020M2\u0007\u0010\u00ac\u0001\u001a\u00020M2\u0007\u0010\u00ad\u0001\u001a\u00020MH\u0002J,\u0010\u00ae\u0001\u001a\u00020M2\u0007\u0010\u00a5\u0001\u001a\u00020M2\u0007\u0010\u00a6\u0001\u001a\u00020M2\u0006\u0010u\u001a\u00020M2\u0007\u0010\u00ad\u0001\u001a\u00020MH\u0002J\u0012\u0010\u00af\u0001\u001a\u00020q2\u0007\u0010\u0083\u0001\u001a\u00020\u0004H\u0016J+\u0010\u00b0\u0001\u001a\u00030\u00b1\u00012\u0006\u0010b\u001a\u00020\u00032\u0007\u0010\u00b2\u0001\u001a\u00020M2\u000e\u0010\u00b3\u0001\u001a\t\u0012\u0004\u0012\u00020X0\u00b4\u0001H\u0002J$\u0010\u00b5\u0001\u001a\u00030\u00b6\u00012\u0006\u0010b\u001a\u00020\u00032\u0007\u0010\u00b7\u0001\u001a\u00020l2\u0007\u0010\u00b8\u0001\u001a\u00020MH\u0002J\u0015\u0010\u00b9\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u001e\u0010\u00ba\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050S2\u0007\u0010\u0083\u0001\u001a\u00020\u0004H\u0002JY\u0010\u00bb\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050S2\u0007\u0010\u0083\u0001\u001a\u00020\u00042\u0007\u0010\u00ab\u0001\u001a\u00020M2\u0007\u0010\u00ac\u0001\u001a\u00020M2\u0007\u0010\u00ad\u0001\u001a\u00020M2\u0007\u0010\u00bc\u0001\u001a\u00020M2\u0006\u0010b\u001a\u00020\u00032\r\u0010\u00bd\u0001\u001a\u0008\u0012\u0004\u0012\u00020O0NH\u0002J\u0015\u0010\u00be\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J\u0015\u0010\u00bf\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050SH\u0002J>\u0010\u00c0\u0001\u001a\t\u0012\u0005\u0012\u0003H\u00c1\u00010N\"\u0011\u0008\u0000\u0010\u00c1\u0001*\n\u0012\u0005\u0012\u0003H\u00c1\u00010\u00c2\u0001*\t\u0012\u0005\u0012\u0003H\u00c1\u00010N2\u0008\u0010\u00c3\u0001\u001a\u0003H\u00c1\u0001H\u0002\u00a2\u0006\u0003\u0010\u00c4\u0001J\u000c\u0010v\u001a\u00020M*\u00020gH\u0002J\u000e\u0010\u00c5\u0001\u001a\u00020\u0005*\u00030\u00c6\u0001H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010D\u001a\u0008\u0012\u0004\u0012\u00020F0EX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010G\u001a\u0008\u0012\u0004\u0012\u00020F0EX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020&X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020(X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020*X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020,X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020.X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010/\u001a\u000200X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00101\u001a\u000202X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00103\u001a\u000204X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010H\u001a\u0008\u0012\u0004\u0012\u00020F0EX\u0082\u0004\u00a2\u0006\u0002\n\u0000Rv\u0010I\u001aj\u0012f\u0012d\u0012\u000c\u0012\n L*\u0004\u0018\u00010K0K\u0012\u000c\u0012\n L*\u0004\u0018\u00010M0M\u0012(\u0012&\u0012\u000c\u0012\n L*\u0004\u0018\u00010O0O L*\u0012\u0012\u000c\u0012\n L*\u0004\u0018\u00010O0O\u0018\u00010N0N\u0012\u000c\u0012\n L*\u0004\u0018\u00010M0M\u0012\u000c\u0012\n L*\u0004\u0018\u00010M0M0J0EX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00105\u001a\u000206X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00107\u001a\u000208X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00109\u001a\u00020:X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010P\u001a\u0008\u0012\u0004\u0012\u00020F0EX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010;\u001a\u00020<X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010=\u001a\u00020>X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010?\u001a\u00020@X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010Q\u001a\u0008\u0012\u0004\u0012\u00020F0EX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010A\u001a\u00020BX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u00c9\u0001"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;",
        "Lcom/squareup/tenderpayment/SelectMethodV2Workflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "bus",
        "Lcom/squareup/badbus/BadBus;",
        "cardReaderHubUtils",
        "Lcom/squareup/cardreader/CardReaderHubUtils;",
        "cardReaderOracle",
        "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
        "changeHudToaster",
        "Lcom/squareup/tenderpayment/ChangeHudToaster;",
        "checkoutInformationEventLogger",
        "Lcom/squareup/log/CheckoutInformationEventLogger;",
        "completer",
        "Lcom/squareup/tenderpayment/TenderCompleter;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "device",
        "Lcom/squareup/util/Device;",
        "dippedCardTracker",
        "Lcom/squareup/cardreader/DippedCardTracker;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "manualCardEntryScreenDataHelper",
        "Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;",
        "nfcProcessor",
        "Lcom/squareup/ui/NfcProcessor;",
        "offlineModeMonitor",
        "Lcom/squareup/payment/OfflineModeMonitor;",
        "pauseAndResumeRegistrar",
        "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
        "paymentInputHandler",
        "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
        "permissionWorkflow",
        "Lcom/squareup/permissionworkflow/PermissionWorkflow;",
        "screenDataRenderer",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;",
        "selectMethodScreensFactory",
        "Lcom/squareup/tenderpayment/SelectMethodScreensFactory;",
        "services",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "swipeValidator",
        "Lcom/squareup/swipe/SwipeValidator;",
        "tenderFactory",
        "Lcom/squareup/payment/tender/TenderFactory;",
        "tenderInEdit",
        "Lcom/squareup/payment/TenderInEdit;",
        "touchEventMonitor",
        "Lcom/squareup/ui/TouchEventMonitor;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "x2SellerScreenRunner",
        "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
        "(Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/util/Device;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/permissionworkflow/PermissionWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;Lcom/squareup/tenderpayment/SelectMethodScreensFactory;Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/payment/Transaction;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)V",
        "checkoutLoggerShowScreenSideEffect",
        "Lcom/squareup/workflow/Worker;",
        "",
        "nonCardSelectedTutorialSideEffectWorker",
        "startCheckoutSideEffect",
        "stateChangedWorker",
        "Lcom/squareup/util/tuple/Quintuple;",
        "Lcom/squareup/connectivity/InternetState;",
        "kotlin.jvm.PlatformType",
        "",
        "Ljava/util/EnumSet;",
        "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
        "touchEventLoggerSideEffect",
        "x2InitialPromptForPaymentSideEffectWorker",
        "actionInState",
        "Lcom/squareup/workflow/WorkflowAction;",
        "ExpectedStateT",
        "apply",
        "Lkotlin/Function1;",
        "Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "cancellingBillCheckingPermissionsCancelled",
        "cancellingBillCheckingPermissionsSuccess",
        "cancellingBillConfirmAction",
        "cancellingBillDismissAction",
        "confirmChargeCardOnFileDialogChargeAction",
        "confirmChargeCardOnFileDialogDoNotChargeAction",
        "createSelectMethodScreenData",
        "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        "startArgs",
        "selectMethodInput",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/tenderpayment/SelectMethod$Event;",
        "selectMethodState",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;",
        "falseThenTrueFilter",
        "values",
        "",
        "getTenderAmount",
        "Lcom/squareup/protos/common/Money;",
        "initialSelectMethodState",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "nfcState",
        "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;",
        "isHodor",
        "areContactlessReadersReadyForPayments",
        "shouldEnableNfcField",
        "hasPaymentStartedOnContactlessReader",
        "onAddGiftCardSelectedAction",
        "onBackPressedAction",
        "onCardOnFileSelectedAction",
        "onCardSelectedAction",
        "onCashSelectedAction",
        "onConfirmChargeCardOnFileAction",
        "tenderedAmount",
        "instrumentToken",
        "",
        "cardNameAndNumber",
        "onConfirmCollectCashAction",
        "state",
        "onContactlessSelectedAction",
        "onDismissCollectCashAction",
        "onGiftCardSelectedAction",
        "onInvoiceSelectedAction",
        "onOtherTenderSelectedAction",
        "tender",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
        "tenderName",
        "onPaymentReceivedCardFailed",
        "paymentEvent",
        "Lcom/squareup/ui/main/errors/CardFailed;",
        "onPaymentReceivedReportReaderIssue",
        "Lcom/squareup/ui/main/errors/ReportReaderIssue;",
        "onPaymentReceivedTakeDipPayment",
        "Lcom/squareup/ui/main/errors/TakeDipPayment;",
        "onPaymentReceivedTakeSwipePayment",
        "Lcom/squareup/ui/main/errors/TakeSwipePayment;",
        "onPaymentReceivedTakeTapPayment",
        "Lcom/squareup/ui/main/errors/TakeTapPayment;",
        "onPerformAutoCaptureAction",
        "onQuickCashTenderReceivedAction",
        "onRecordFullyCompedPaymentAction",
        "amount",
        "onReenableContactlessClickedAction",
        "onSecondaryTendersSelectedAction",
        "onSplitTenderAction",
        "onTenderOptionSelectedAction",
        "tenderOption",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;",
        "onThirdPartyCardSelectedAction",
        "performOutputSideEffect",
        "result",
        "preAuthTipRequiredAndTipEnabled",
        "preAuthTipRequired",
        "tipEnabled",
        "promptForPayment",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "isConnectedToNetwork",
        "isInOfflineMode",
        "buyerCheckoutEnabled",
        "showContactlessRow",
        "snapshotState",
        "splitTenderLabel",
        "Lcom/squareup/tenderpayment/SelectMethod$TextData;",
        "hasSplitTenderBillPayment",
        "onSplitTender",
        "Lkotlin/Function0;",
        "splitTenderState",
        "Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;",
        "billAmount",
        "isTakingPaymentFromInvoice",
        "splitTenderWarningDismissed",
        "unsetToastDataAction",
        "updateSelectMethodStateAction",
        "isRunningState",
        "readerCapabilities",
        "x2BranConnectedAction",
        "x2PaymentMethodSelectedAction",
        "minus",
        "E",
        "",
        "value",
        "(Ljava/util/EnumSet;Ljava/lang/Enum;)Ljava/util/EnumSet;",
        "toTenderPaymentResult",
        "Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;",
        "Companion",
        "TypeSafeUpdater",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CUSTOM_CASH_VALUE:Lcom/squareup/protos/common/Money;

.field public static final Companion:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$Companion;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field private final cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

.field private final changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

.field private final checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

.field private final checkoutLoggerShowScreenSideEffect:Lcom/squareup/workflow/Worker;

.field private final completer:Lcom/squareup/tenderpayment/TenderCompleter;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final device:Lcom/squareup/util/Device;

.field private final dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final manualCardEntryScreenDataHelper:Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final nonCardSelectedTutorialSideEffectWorker:Lcom/squareup/workflow/Worker;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private final pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

.field private final paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

.field private final permissionWorkflow:Lcom/squareup/permissionworkflow/PermissionWorkflow;

.field private final screenDataRenderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;

.field private final selectMethodScreensFactory:Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

.field private final services:Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final startCheckoutSideEffect:Lcom/squareup/workflow/Worker;

.field private final stateChangedWorker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/util/tuple/Quintuple<",
            "Lcom/squareup/connectivity/InternetState;",
            "Ljava/lang/Boolean;",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final swipeValidator:Lcom/squareup/swipe/SwipeValidator;

.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final touchEventLoggerSideEffect:Lcom/squareup/workflow/Worker;

.field private final touchEventMonitor:Lcom/squareup/ui/TouchEventMonitor;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private final x2InitialPromptForPaymentSideEffectWorker:Lcom/squareup/workflow/Worker;

.field private final x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->Companion:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$Companion;

    .line 1369
    new-instance v0, Lcom/squareup/protos/common/Money;

    const-wide/16 v1, -0x1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/common/Money;->DEFAULT_CURRENCY_CODE:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    sput-object v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->CUSTOM_CASH_VALUE:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/util/Device;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/permissionworkflow/PermissionWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;Lcom/squareup/tenderpayment/SelectMethodScreensFactory;Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/payment/Transaction;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)V
    .locals 16
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v0, p16

    const-string v0, "analytics"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bus"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderHubUtils"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderOracle"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "changeHudToaster"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkoutInformationEventLogger"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "completer"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectivityMonitor"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dippedCardTracker"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "manualCardEntryScreenDataHelper"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nfcProcessor"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "offlineModeMonitor"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pauseAndResumeRegistrar"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentInputHandler"

    move-object/from16 v15, p16

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionWorkflow"

    move-object/from16 v15, p17

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenDataRenderer"

    move-object/from16 v15, p18

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectMethodScreensFactory"

    move-object/from16 v15, p19

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "services"

    move-object/from16 v15, p20

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    move-object/from16 v15, p21

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "swipeValidator"

    move-object/from16 v15, p22

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderFactory"

    move-object/from16 v15, p23

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderInEdit"

    move-object/from16 v15, p24

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "touchEventMonitor"

    move-object/from16 v15, p25

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    move-object/from16 v15, p26

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialCore"

    move-object/from16 v15, p27

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "x2SellerScreenRunner"

    move-object/from16 v15, p28

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    invoke-direct/range {p0 .. p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v15, p16

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object v2, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->bus:Lcom/squareup/badbus/BadBus;

    iput-object v3, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    iput-object v4, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    iput-object v5, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

    iput-object v6, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    iput-object v7, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->completer:Lcom/squareup/tenderpayment/TenderCompleter;

    iput-object v8, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    iput-object v9, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->device:Lcom/squareup/util/Device;

    iput-object v10, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    iput-object v11, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->features:Lcom/squareup/settings/server/Features;

    iput-object v12, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->manualCardEntryScreenDataHelper:Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;

    iput-object v13, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    iput-object v14, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    iput-object v15, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    move-object/from16 v1, p17

    move-object/from16 v2, p18

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->permissionWorkflow:Lcom/squareup/permissionworkflow/PermissionWorkflow;

    iput-object v2, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->screenDataRenderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;

    move-object/from16 v1, p19

    move-object/from16 v2, p20

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->selectMethodScreensFactory:Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    iput-object v2, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->services:Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;

    move-object/from16 v1, p21

    move-object/from16 v2, p22

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object v2, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->swipeValidator:Lcom/squareup/swipe/SwipeValidator;

    move-object/from16 v1, p23

    move-object/from16 v2, p24

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    iput-object v2, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    move-object/from16 v1, p25

    move-object/from16 v2, p26

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->touchEventMonitor:Lcom/squareup/ui/TouchEventMonitor;

    iput-object v2, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    move-object/from16 v1, p27

    move-object/from16 v2, p28

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object v2, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 221
    sget-object v1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v2, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$startCheckoutSideEffect$1;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$startCheckoutSideEffect$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v1, v2}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->startCheckoutSideEffect:Lcom/squareup/workflow/Worker;

    .line 227
    new-instance v1, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$touchEventLoggerSideEffect$1;

    invoke-direct {v1, v0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$touchEventLoggerSideEffect$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v1, Lio/reactivex/CompletableOnSubscribe;

    invoke-static {v1}, Lio/reactivex/Completable;->create(Lio/reactivex/CompletableOnSubscribe;)Lio/reactivex/Completable;

    move-result-object v1

    const-string v2, "Completable.create { emi\u2026tLogger.tap() }\n    )\n  }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    invoke-static {v1}, Lcom/squareup/workflow/rx2/RxWorkersKt;->asWorker(Lio/reactivex/Completable;)Lcom/squareup/workflow/Worker;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->touchEventLoggerSideEffect:Lcom/squareup/workflow/Worker;

    .line 234
    sget-object v1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v2, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$checkoutLoggerShowScreenSideEffect$1;

    invoke-direct {v2, v0, v3}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$checkoutLoggerShowScreenSideEffect$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v1, v2}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->checkoutLoggerShowScreenSideEffect:Lcom/squareup/workflow/Worker;

    .line 238
    sget-object v1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v2, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$x2InitialPromptForPaymentSideEffectWorker$1;

    invoke-direct {v2, v0, v3}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$x2InitialPromptForPaymentSideEffectWorker$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v1, v2}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->x2InitialPromptForPaymentSideEffectWorker:Lcom/squareup/workflow/Worker;

    .line 245
    sget-object v1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v2, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$nonCardSelectedTutorialSideEffectWorker$1;

    invoke-direct {v2, v0, v3}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$nonCardSelectedTutorialSideEffectWorker$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v1, v2}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->nonCardSelectedTutorialSideEffectWorker:Lcom/squareup/workflow/Worker;

    .line 249
    sget-object v1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 250
    iget-object v2, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v2}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "connectivityMonitor.internetState()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 251
    iget-object v3, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v3}, Lcom/squareup/payment/OfflineModeMonitor;->offlineMode()Lio/reactivex/Observable;

    move-result-object v3

    const-string v4, "offlineModeMonitor.offlineMode()"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    iget-object v4, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates()Lrx/Observable;

    move-result-object v4

    .line 253
    iget-object v5, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->features:Lcom/squareup/settings/server/Features;

    invoke-static {v5}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracleFilters;->asCapabilities(Lcom/squareup/settings/server/Features;)Lrx/Observable$Transformer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v4

    const-string v5, "cardReaderOracle.readerS\u2026asCapabilities(features))"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v4

    .line 254
    iget-object v5, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v6, Lcom/squareup/settings/server/Features$Feature;->BUYER_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v5, v6}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v5

    const-string v6, "features.featureEnabled(BUYER_CHECKOUT)"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 255
    iget-object v6, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    invoke-interface {v6}, Lcom/squareup/pauses/PauseAndResumeRegistrar;->isRunningState()Lio/reactivex/Observable;

    move-result-object v6

    const-string v7, "pauseAndResumeRegistrar.isRunningState"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 p1, v1

    move-object/from16 p2, v2

    move-object/from16 p3, v3

    move-object/from16 p4, v4

    move-object/from16 p5, v5

    move-object/from16 p6, v6

    .line 249
    invoke-virtual/range {p1 .. p6}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v1

    .line 1518
    sget-object v2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v1

    const-string/jumbo v2, "this.toFlowable(BUFFER)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lorg/reactivestreams/Publisher;

    if-eqz v1, :cond_0

    .line 1520
    invoke-static {v1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 1521
    const-class v2, Lcom/squareup/util/tuple/Quintuple;

    const/4 v3, 0x5

    new-array v3, v3, [Lkotlin/reflect/KTypeProjection;

    const/4 v4, 0x0

    sget-object v5, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v6, Lcom/squareup/connectivity/InternetState;

    invoke-static {v6}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v6

    invoke-virtual {v5, v6}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v6, Ljava/lang/Boolean;

    invoke-static {v6}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v6

    invoke-virtual {v5, v6}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    sget-object v5, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v6, Ljava/util/EnumSet;

    sget-object v7, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v8, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-static {v8}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v8

    invoke-virtual {v7, v8}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v7

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v6

    invoke-virtual {v5, v6}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    sget-object v5, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v6, Ljava/lang/Boolean;

    invoke-static {v6}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v6

    invoke-virtual {v5, v6}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    sget-object v5, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v6, Ljava/lang/Boolean;

    invoke-static {v6}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v6

    invoke-virtual {v5, v6}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;[Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v2, v1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v3, Lcom/squareup/workflow/Worker;

    .line 1518
    iput-object v3, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->stateChangedWorker:Lcom/squareup/workflow/Worker;

    return-void

    .line 1520
    :cond_0
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static final synthetic access$cancellingBillCheckingPermissionsCancelled(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->cancellingBillCheckingPermissionsCancelled()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$cancellingBillCheckingPermissionsSuccess(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->cancellingBillCheckingPermissionsSuccess()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$cancellingBillConfirmAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->cancellingBillConfirmAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$cancellingBillDismissAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->cancellingBillDismissAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$confirmChargeCardOnFileDialogChargeAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->confirmChargeCardOnFileDialogChargeAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$confirmChargeCardOnFileDialogDoNotChargeAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->confirmChargeCardOnFileDialogDoNotChargeAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$falseThenTrueFilter(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Ljava/util/List;)Z
    .locals 0

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->falseThenTrueFilter(Ljava/util/List;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 183
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getCUSTOM_CASH_VALUE$cp()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 183
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->CUSTOM_CASH_VALUE:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public static final synthetic access$getChangeHudToaster$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/tenderpayment/ChangeHudToaster;
    .locals 0

    .line 183
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

    return-object p0
.end method

.method public static final synthetic access$getCheckoutInformationEventLogger$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/log/CheckoutInformationEventLogger;
    .locals 0

    .line 183
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    return-object p0
.end method

.method public static final synthetic access$getCompleter$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/tenderpayment/TenderCompleter;
    .locals 0

    .line 183
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->completer:Lcom/squareup/tenderpayment/TenderCompleter;

    return-object p0
.end method

.method public static final synthetic access$getDippedCardTracker$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/cardreader/DippedCardTracker;
    .locals 0

    .line 183
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    return-object p0
.end method

.method public static final synthetic access$getManualCardEntryScreenDataHelper$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;
    .locals 0

    .line 183
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->manualCardEntryScreenDataHelper:Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;

    return-object p0
.end method

.method public static final synthetic access$getServices$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;
    .locals 0

    .line 183
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->services:Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;

    return-object p0
.end method

.method public static final synthetic access$getSettings$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 183
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method public static final synthetic access$getSwipeValidator$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/swipe/SwipeValidator;
    .locals 0

    .line 183
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->swipeValidator:Lcom/squareup/swipe/SwipeValidator;

    return-object p0
.end method

.method public static final synthetic access$getTenderAmount(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->getTenderAmount(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getTenderFactory$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/payment/tender/TenderFactory;
    .locals 0

    .line 183
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    return-object p0
.end method

.method public static final synthetic access$getTenderInEdit$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/payment/TenderInEdit;
    .locals 0

    .line 183
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    return-object p0
.end method

.method public static final synthetic access$getTouchEventMonitor$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/ui/TouchEventMonitor;
    .locals 0

    .line 183
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->touchEventMonitor:Lcom/squareup/ui/TouchEventMonitor;

    return-object p0
.end method

.method public static final synthetic access$getTransaction$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 183
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method public static final synthetic access$getTutorialCore$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/tutorialv2/TutorialCore;
    .locals 0

    .line 183
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-object p0
.end method

.method public static final synthetic access$getX2SellerScreenRunner$p(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/x2/MaybeX2SellerScreenRunner;
    .locals 0

    .line 183
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    return-object p0
.end method

.method public static final synthetic access$initialSelectMethodState(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;
    .locals 0

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->initialSelectMethodState(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$minus(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Ljava/util/EnumSet;Ljava/lang/Enum;)Ljava/util/EnumSet;
    .locals 0

    .line 183
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->minus(Ljava/util/EnumSet;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onAddGiftCardSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onAddGiftCardSelectedAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onBackPressedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onBackPressedAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onCardOnFileSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onCardOnFileSelectedAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onCardSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onCardSelectedAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onCashSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onCashSelectedAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onConfirmChargeCardOnFileAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onConfirmChargeCardOnFileAction(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onConfirmCollectCashAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/protos/common/Money;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onConfirmCollectCashAction(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/protos/common/Money;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onContactlessSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onContactlessSelectedAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onDismissCollectCashAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onDismissCollectCashAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onGiftCardSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onGiftCardSelectedAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onInvoiceSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onInvoiceSelectedAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onOtherTenderSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onOtherTenderSelectedAction(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onPaymentReceivedCardFailed(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/ui/main/errors/CardFailed;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onPaymentReceivedCardFailed(Lcom/squareup/ui/main/errors/CardFailed;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onPaymentReceivedReportReaderIssue(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/ui/main/errors/ReportReaderIssue;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onPaymentReceivedReportReaderIssue(Lcom/squareup/ui/main/errors/ReportReaderIssue;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onPaymentReceivedTakeDipPayment(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/ui/main/errors/TakeDipPayment;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onPaymentReceivedTakeDipPayment(Lcom/squareup/ui/main/errors/TakeDipPayment;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onPaymentReceivedTakeSwipePayment(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/ui/main/errors/TakeSwipePayment;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onPaymentReceivedTakeSwipePayment(Lcom/squareup/ui/main/errors/TakeSwipePayment;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onPaymentReceivedTakeTapPayment(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/ui/main/errors/TakeTapPayment;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onPaymentReceivedTakeTapPayment(Lcom/squareup/ui/main/errors/TakeTapPayment;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onPerformAutoCaptureAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onPerformAutoCaptureAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onQuickCashTenderReceivedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/protos/common/Money;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onQuickCashTenderReceivedAction(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/protos/common/Money;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onRecordFullyCompedPaymentAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/protos/common/Money;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onRecordFullyCompedPaymentAction(Lcom/squareup/protos/common/Money;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onReenableContactlessClickedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onReenableContactlessClickedAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onSecondaryTendersSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onSecondaryTendersSelectedAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onSplitTenderAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onSplitTenderAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onTenderOptionSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onTenderOptionSelectedAction(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onThirdPartyCardSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->onThirdPartyCardSelectedAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$performOutputSideEffect(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/TenderPaymentResult;)V
    .locals 0

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->performOutputSideEffect(Lcom/squareup/tenderpayment/TenderPaymentResult;)V

    return-void
.end method

.method public static final synthetic access$promptForPayment(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->promptForPayment()V

    return-void
.end method

.method public static final synthetic access$shouldEnableNfcField(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;ZZZZZ)Z
    .locals 0

    .line 183
    invoke-direct/range {p0 .. p5}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->shouldEnableNfcField(ZZZZZ)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$splitTenderWarningDismissed(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->splitTenderWarningDismissed()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toTenderPaymentResult(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)Lcom/squareup/tenderpayment/TenderPaymentResult;
    .locals 0

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->toTenderPaymentResult(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)Lcom/squareup/tenderpayment/TenderPaymentResult;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$unsetToastDataAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->unsetToastDataAction(Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateSelectMethodStateAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;ZZZZLcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Ljava/util/EnumSet;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct/range {p0 .. p7}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->updateSelectMethodStateAction(Lcom/squareup/tenderpayment/SelectMethodWorkflowState;ZZZZLcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Ljava/util/EnumSet;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$x2BranConnectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->x2BranConnectedAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$x2PaymentMethodSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->x2PaymentMethodSelectedAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final synthetic actionInState(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ExpectedStateT:",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$TypeSafeUpdater<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "TExpectedStateT;-",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1341
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$actionInState$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final cancellingBillCheckingPermissionsCancelled()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1475
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$cancellingBillCheckingPermissionsCancelled$$inlined$actionInState$1;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$cancellingBillCheckingPermissionsCancelled$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final cancellingBillCheckingPermissionsSuccess()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1473
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$cancellingBillCheckingPermissionsSuccess$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$cancellingBillCheckingPermissionsSuccess$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final cancellingBillConfirmAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1479
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$cancellingBillConfirmAction$$inlined$actionInState$1;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$cancellingBillConfirmAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final cancellingBillDismissAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1481
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$cancellingBillDismissAction$$inlined$actionInState$1;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$cancellingBillDismissAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final confirmChargeCardOnFileDialogChargeAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1471
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$confirmChargeCardOnFileDialogChargeAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$confirmChargeCardOnFileDialogChargeAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final confirmChargeCardOnFileDialogDoNotChargeAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1469
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$confirmChargeCardOnFileDialogDoNotChargeAction$$inlined$actionInState$1;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$confirmChargeCardOnFileDialogDoNotChargeAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final createSelectMethodScreenData(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;
    .locals 37
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;",
            ")",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    .line 1121
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getDisplayedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 1122
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isConnectedToNetwork()Z

    move-result v6

    .line 1123
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getToastData()Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    move-result-object v7

    .line 1124
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getReaderCapabilities()Ljava/util/EnumSet;

    move-result-object v8

    .line 1125
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getCompletedTenders()Ljava/util/List;

    move-result-object v1

    .line 1126
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getBuyerCheckoutEnabled()Z

    move-result v9

    .line 1128
    iget-object v3, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v10

    .line 1129
    iget-object v3, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v15

    .line 1131
    iget-object v3, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v3}, Lcom/squareup/ui/NfcProcessor;->areContactlessReadersReadyForPayments()Z

    move-result v3

    .line 1133
    iget-object v4, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-virtual {v4}, Lcom/squareup/cardreader/CardReaderHubUtils;->hasPaymentStartedOnContactlessReader()Z

    move-result v4

    .line 1135
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getPreAuthTipRequired()Z

    move-result v11

    .line 1136
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getTipEnabled()Z

    move-result v12

    .line 1139
    iget-object v13, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v13}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->badIsHodorCheck()Z

    move-result v13

    move-object/from16 v14, p3

    move-object/from16 v16, v8

    .line 1141
    invoke-direct {v0, v14}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->shouldEnableNfcField(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)Z

    move-result v8

    .line 1138
    invoke-direct {v0, v13, v3, v8, v4}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->nfcState(ZZZZ)Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    move-result-object v4

    .line 1144
    iget-object v8, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->screenDataRenderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;

    const-string v13, "billAmount"

    .line 1153
    invoke-static {v10, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1155
    iget-object v13, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v13}, Lcom/squareup/payment/Transaction;->getTransactionMaximum()J

    move-result-wide v13

    move-object/from16 v17, v7

    iget-object v7, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v7}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v7

    move/from16 v31, v6

    const-string v6, "settings.userSettings"

    invoke-static {v7, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v7

    move-object/from16 v32, v5

    const-string v5, "settings.userSettings.currency"

    invoke-static {v7, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1154
    invoke-static {v13, v14, v7}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v13

    .line 1158
    iget-object v7, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    move-object/from16 v33, v13

    invoke-virtual {v7}, Lcom/squareup/payment/Transaction;->getTransactionMinimum()J

    move-result-wide v13

    iget-object v7, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v7}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v7

    invoke-static {v7, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v7

    invoke-static {v7, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1157
    invoke-static {v13, v14, v7}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v13

    .line 1160
    invoke-static {v10}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result v14

    const/4 v7, 0x1

    if-eqz v15, :cond_0

    .line 1161
    iget-object v5, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v5}, Lcom/squareup/payment/Transaction;->getNumSplitsTotal()J

    move-result-wide v19

    const-wide/16 v21, 0x1

    cmp-long v5, v19, v21

    if-lez v5, :cond_0

    const/16 v34, 0x1

    goto :goto_0

    :cond_0
    const/16 v34, 0x0

    .line 1162
    :goto_0
    invoke-direct {v0, v11, v12, v3, v9}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->showContactlessRow(ZZZZ)Z

    move-result v35

    .line 1168
    iget-object v3, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getCustomerDisplayNameOrBlank()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v5, "transaction.customerDisplayNameOrBlank"

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_1

    const/16 v36, 0x1

    goto :goto_1

    :cond_1
    const/16 v36, 0x0

    .line 1170
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v1, v7

    int-to-long v11, v1

    move-wide/from16 v18, v11

    .line 1171
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getNumSplitsTotal()J

    move-result-wide v20

    .line 1175
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v1

    .line 1172
    invoke-direct {v0, v2, v10, v1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->splitTenderState(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/protos/common/Money;Z)Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;

    move-result-object v22

    .line 1180
    new-instance v1, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$createSelectMethodScreenData$1;

    move-object/from16 v3, p2

    invoke-direct {v1, v3}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$createSelectMethodScreenData$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 1177
    invoke-direct {v0, v2, v15, v1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->splitTenderLabel(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move-result-object v23

    .line 1182
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->cardOption()Lcom/squareup/payment/CardOptionEnabled;

    move-result-object v1

    move-object/from16 v24, v1

    const-string/jumbo v5, "transaction.cardOption()"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1183
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->badIsHodorCheck()Z

    move-result v25

    .line 1184
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v1

    move-object/from16 v26, v1

    const-string v5, "settings.userSettings.countryCode"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1185
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v27

    .line 1186
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getCustomerGiftCardInstrumentDetails()Ljava/util/List;

    move-result-object v28

    .line 1187
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getCustomerNonGiftCardInstrumentDetails()Ljava/util/List;

    move-result-object v29

    .line 1188
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isInOfflineMode()Z

    move-result v30

    move-object v1, v8

    move-object/from16 v2, p1

    move-object/from16 v5, v32

    move/from16 v6, v31

    move-object/from16 v7, v17

    move-object/from16 v8, v16

    move-object/from16 v11, v33

    move-object v12, v13

    move v13, v14

    move/from16 v14, v34

    move/from16 v17, v15

    move/from16 v15, v35

    move/from16 v16, v36

    .line 1144
    invoke-virtual/range {v1 .. v30}, Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;->createSelectMethodScreenData(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;ZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZZZZJJLcom/squareup/tenderpayment/SelectMethod$SplitTenderState;Lcom/squareup/tenderpayment/SelectMethod$TextData;Lcom/squareup/payment/CardOptionEnabled;ZLcom/squareup/CountryCode;ZLjava/util/List;Ljava/util/List;Z)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    move-result-object v1

    return-object v1
.end method

.method private final falseThenTrueFilter(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    .line 1302
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-ge v0, v3, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v2, 0x1

    :cond_1
    :goto_0
    return v2
.end method

.method private final getTenderAmount(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)Lcom/squareup/protos/common/Money;
    .locals 1

    .line 878
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 879
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    .line 881
    :cond_0
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p1

    const-string/jumbo v0, "transaction.amountDue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method private final initialSelectMethodState(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;
    .locals 17

    move-object/from16 v0, p0

    .line 273
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    const-string v1, "UUID.randomUUID()"

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 274
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getRestartSelectMethodData()Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;->getShowSecondaryMethods()Z

    move-result v3

    .line 275
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v4, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    move-object v4, v1

    .line 276
    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->DISABLED:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    .line 277
    invoke-direct/range {p0 .. p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->getTenderAmount(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 278
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v1}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v7

    .line 279
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v1}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v8

    .line 280
    new-instance v9, Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->NONE:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    invoke-direct {v9, v1}, Lcom/squareup/tenderpayment/SelectMethod$ToastData;-><init>(Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;)V

    .line 281
    const-class v1, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v10

    const-string v1, "EnumSet.noneOf(Connected\u2026Capabilities::class.java)"

    invoke-static {v10, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 282
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->services:Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->completedTenders()Ljava/util/List;

    move-result-object v11

    .line 283
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v12, Lcom/squareup/settings/server/Features$Feature;->BUYER_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v12}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v12

    .line 284
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v13, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_REQUIRED:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v13}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v13

    .line 285
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/TipSettings;->isEnabled()Z

    move-result v14

    .line 286
    iget-object v1, v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    invoke-interface {v1}, Lcom/squareup/pauses/PauseAndResumeRegistrar;->isRunning()Z

    move-result v15

    .line 272
    new-instance v16, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;-><init>(Ljava/util/UUID;ZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Ljava/util/List;ZZZZ)V

    return-object v16
.end method

.method private final minus(Ljava/util/EnumSet;Ljava/lang/Enum;)Ljava/util/EnumSet;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Enum<",
            "TE;>;>(",
            "Ljava/util/EnumSet<",
            "TE;>;TE;)",
            "Ljava/util/EnumSet<",
            "TE;>;"
        }
    .end annotation

    .line 1305
    invoke-virtual {p1, p2}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    return-object p1
.end method

.method private final nfcState(ZZZZ)Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;
    .locals 0

    if-eqz p1, :cond_0

    .line 1282
    sget-object p1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->AVAILABLE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    goto :goto_0

    :cond_0
    if-nez p2, :cond_1

    .line 1283
    sget-object p1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->DISABLED:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    goto :goto_0

    :cond_1
    if-nez p3, :cond_2

    .line 1284
    sget-object p1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->AVAILABLE_WITHOUT_TAP:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    goto :goto_0

    :cond_2
    if-eqz p4, :cond_3

    .line 1285
    sget-object p1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->AVAILABLE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    goto :goto_0

    .line 1286
    :cond_3
    sget-object p1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->TIMED_OUT:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    :goto_0
    return-object p1
.end method

.method private final onAddGiftCardSelectedAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1442
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onAddGiftCardSelectedAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onAddGiftCardSelectedAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onBackPressedAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1426
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onBackPressedAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onBackPressedAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onCardOnFileSelectedAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1440
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onCardOnFileSelectedAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onCardOnFileSelectedAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onCardSelectedAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1430
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onCardSelectedAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onCardSelectedAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onCashSelectedAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1428
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onCashSelectedAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onCashSelectedAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onConfirmChargeCardOnFileAction(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1444
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmChargeCardOnFileAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmChargeCardOnFileAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onConfirmCollectCashAction(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/protos/common/Money;)Lcom/squareup/workflow/WorkflowAction;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;",
            "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1451
    new-instance v6, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;

    const-string v1, ""

    move-object v0, v6

    move-object v2, p0

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onConfirmCollectCashAction$$inlined$action$1;-><init>(Ljava/lang/String;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    check-cast v6, Lcom/squareup/workflow/WorkflowAction;

    return-object v6
.end method

.method private final onContactlessSelectedAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1465
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onContactlessSelectedAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onContactlessSelectedAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onDismissCollectCashAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1455
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onDismissCollectCashAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onDismissCollectCashAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onGiftCardSelectedAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1432
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onGiftCardSelectedAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onGiftCardSelectedAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onInvoiceSelectedAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1438
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onInvoiceSelectedAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onInvoiceSelectedAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onOtherTenderSelectedAction(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1434
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onOtherTenderSelectedAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0, p2, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onOtherTenderSelectedAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Ljava/lang/String;Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onPaymentReceivedCardFailed(Lcom/squareup/ui/main/errors/CardFailed;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/errors/CardFailed;",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1422
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onPaymentReceivedCardFailed$$inlined$action$1;

    const-string v1, ""

    invoke-direct {v0, v1, p2, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onPaymentReceivedCardFailed$$inlined$action$1;-><init>(Ljava/lang/String;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/ui/main/errors/CardFailed;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onPaymentReceivedReportReaderIssue(Lcom/squareup/ui/main/errors/ReportReaderIssue;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/errors/ReportReaderIssue;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1415
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onPaymentReceivedReportReaderIssue$$inlined$action$1;

    const-string v1, ""

    invoke-direct {v0, v1, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onPaymentReceivedReportReaderIssue$$inlined$action$1;-><init>(Ljava/lang/String;Lcom/squareup/ui/main/errors/ReportReaderIssue;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onPaymentReceivedTakeDipPayment(Lcom/squareup/ui/main/errors/TakeDipPayment;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/errors/TakeDipPayment;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1401
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onPaymentReceivedTakeDipPayment$$inlined$action$1;

    const-string v1, ""

    invoke-direct {v0, v1, p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onPaymentReceivedTakeDipPayment$$inlined$action$1;-><init>(Ljava/lang/String;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/ui/main/errors/TakeDipPayment;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onPaymentReceivedTakeSwipePayment(Lcom/squareup/ui/main/errors/TakeSwipePayment;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)Lcom/squareup/workflow/WorkflowAction;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/errors/TakeSwipePayment;",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1394
    new-instance v6, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onPaymentReceivedTakeSwipePayment$$inlined$action$1;

    const-string v1, ""

    move-object v0, v6

    move-object v2, p0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onPaymentReceivedTakeSwipePayment$$inlined$action$1;-><init>(Ljava/lang/String;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/ui/main/errors/TakeSwipePayment;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)V

    check-cast v6, Lcom/squareup/workflow/WorkflowAction;

    return-object v6
.end method

.method private final onPaymentReceivedTakeTapPayment(Lcom/squareup/ui/main/errors/TakeTapPayment;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/errors/TakeTapPayment;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1408
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onPaymentReceivedTakeTapPayment$$inlined$action$1;

    const-string v1, ""

    invoke-direct {v0, v1, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onPaymentReceivedTakeTapPayment$$inlined$action$1;-><init>(Ljava/lang/String;Lcom/squareup/ui/main/errors/TakeTapPayment;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onPerformAutoCaptureAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1500
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onPerformAutoCaptureAction$$inlined$action$1;

    const-string v1, ""

    invoke-direct {v0, v1, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onPerformAutoCaptureAction$$inlined$action$1;-><init>(Ljava/lang/String;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onQuickCashTenderReceivedAction(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/protos/common/Money;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1446
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onQuickCashTenderReceivedAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0, p2, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onQuickCashTenderReceivedAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onRecordFullyCompedPaymentAction(Lcom/squareup/protos/common/Money;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1463
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onRecordFullyCompedPaymentAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onRecordFullyCompedPaymentAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/protos/common/Money;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onReenableContactlessClickedAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1461
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onReenableContactlessClickedAction$$inlined$actionInState$1;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onReenableContactlessClickedAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onSecondaryTendersSelectedAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1457
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onSecondaryTendersSelectedAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onSecondaryTendersSelectedAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onSplitTenderAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1459
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onSplitTenderAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onSplitTenderAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onTenderOptionSelectedAction(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1467
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onTenderOptionSelectedAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onTenderOptionSelectedAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final onThirdPartyCardSelectedAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1436
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onThirdPartyCardSelectedAction$$inlined$actionInState$1;

    invoke-direct {v0, p0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$onThirdPartyCardSelectedAction$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final performOutputSideEffect(Lcom/squareup/tenderpayment/TenderPaymentResult;)V
    .locals 2

    .line 1326
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->cardOptionShouldBeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Leaving SelectMethodScreen, card amount in range"

    goto :goto_0

    :cond_0
    const-string v0, "Leaving SelectMethodScreen"

    .line 1329
    :goto_0
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    invoke-interface {v1, v0, p1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1331
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

    invoke-interface {p1}, Lcom/squareup/tenderpayment/ChangeHudToaster;->toastIfAvailable()V

    return-void
.end method

.method private final preAuthTipRequiredAndTipEnabled(ZZ)Z
    .locals 0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final promptForPayment()V
    .locals 2

    .line 1292
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->badIsHodorCheck()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1293
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1294
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v1}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-interface {v0, v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->promptForPayment(Lcom/squareup/payment/tender/BaseTender$Builder;)Z

    goto :goto_0

    .line 1296
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->promptForPayment()Z

    :cond_1
    :goto_0
    return-void
.end method

.method private final shouldEnableNfcField(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)Z
    .locals 6

    .line 1264
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getPreAuthTipRequired()Z

    move-result v1

    .line 1265
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getTipEnabled()Z

    move-result v2

    .line 1266
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isConnectedToNetwork()Z

    move-result v3

    .line 1267
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isInOfflineMode()Z

    move-result v4

    .line 1268
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getBuyerCheckoutEnabled()Z

    move-result v5

    move-object v0, p0

    .line 1263
    invoke-direct/range {v0 .. v5}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->shouldEnableNfcField(ZZZZZ)Z

    move-result p1

    return p1
.end method

.method private final shouldEnableNfcField(ZZZZZ)Z
    .locals 0

    .line 1258
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->preAuthTipRequiredAndTipEnabled(ZZ)Z

    move-result p1

    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_0

    if-nez p5, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final showContactlessRow(ZZZZ)Z
    .locals 0

    .line 1237
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->preAuthTipRequiredAndTipEnabled(ZZ)Z

    move-result p1

    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final splitTenderLabel(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/tenderpayment/SelectMethod$TextData;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
            "Z",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/tenderpayment/SelectMethod$TextData;"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 1214
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getHasEditedSplit()Z

    move-result p1

    if-eqz p1, :cond_0

    sget p1, Lcom/squareup/tenderworkflow/R$string;->split_tender_edit_split:I

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 1215
    sget p1, Lcom/squareup/tenderworkflow/R$string;->split_tender_again:I

    goto :goto_0

    .line 1216
    :cond_1
    sget p1, Lcom/squareup/tenderworkflow/R$string;->split_tender:I

    :goto_0
    move v1, p1

    .line 1219
    new-instance p1, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1223
    new-instance p2, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$splitTenderLabel$1;

    invoke-direct {p2, p3}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$splitTenderLabel$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    move-object v4, p2

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    const/4 v5, 0x0

    move-object v0, p1

    .line 1219
    invoke-direct/range {v0 .. v5}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/fsm/SideEffect;Ljava/lang/String;)V

    return-object p1
.end method

.method private final splitTenderState(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/protos/common/Money;Z)Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;
    .locals 3

    .line 1197
    new-instance v0, Lcom/squareup/protos/common/Money;

    sget-object v1, Lcom/squareup/protos/common/Money;->DEFAULT_AMOUNT:Ljava/lang/Long;

    iget-object v2, p2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 1200
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getSplitTenderEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object p1, Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;->DISABLED:Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;

    goto :goto_0

    :cond_0
    if-eqz p3, :cond_1

    .line 1201
    sget-object p1, Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;->HIDDEN:Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;

    goto :goto_0

    .line 1202
    :cond_1
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    sget-object p1, Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;->HIDDEN:Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;

    goto :goto_0

    .line 1203
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getSplitTenderEnabled()Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;->NORMAL:Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;

    goto :goto_0

    .line 1204
    :cond_3
    sget-object p1, Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;->HIDDEN:Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;

    :goto_0
    return-object p1
.end method

.method private final splitTenderWarningDismissed()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1477
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$splitTenderWarningDismissed$$inlined$actionInState$1;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$splitTenderWarningDismissed$$inlined$actionInState$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final toTenderPaymentResult(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)Lcom/squareup/tenderpayment/TenderPaymentResult;
    .locals 1

    .line 589
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    .line 596
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_0

    .line 595
    :cond_0
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ShowPosApplet;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$ShowPosApplet;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_0

    .line 594
    :cond_1
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$DoNothing;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$DoNothing;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_0

    .line 593
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cash/Other tenders should not be authorized."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 591
    :cond_3
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PaidNeedToAuthorize;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$PaidNeedToAuthorize;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_0

    .line 590
    :cond_4
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    :goto_0
    return-object p1
.end method

.method private final unsetToastDataAction(Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1493
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$unsetToastDataAction$$inlined$action$1;

    const-string v1, ""

    invoke-direct {v0, v1, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$unsetToastDataAction$$inlined$action$1;-><init>(Ljava/lang/String;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final updateSelectMethodStateAction(Lcom/squareup/tenderpayment/SelectMethodWorkflowState;ZZZZLcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Ljava/util/EnumSet;)Lcom/squareup/workflow/WorkflowAction;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "ZZZZ",
            "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1486
    new-instance v10, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;

    const-string v1, ""

    move-object v0, v10

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$updateSelectMethodStateAction$$inlined$action$1;-><init>(Ljava/lang/String;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;ZZZZLcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Ljava/util/EnumSet;)V

    check-cast v10, Lcom/squareup/workflow/WorkflowAction;

    return-object v10
.end method

.method private final x2BranConnectedAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1507
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$x2BranConnectedAction$$inlined$action$1;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$x2BranConnectedAction$$inlined$action$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method private final x2PaymentMethodSelectedAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 1514
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$x2PaymentMethodSelectedAction$$inlined$action$1;

    const-string v1, ""

    invoke-direct {v0, v1, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$x2PaymentMethodSelectedAction$$inlined$action$1;-><init>(Ljava/lang/String;Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 265
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer;

    .line 267
    new-instance v1, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$initialState$$inlined$let$lambda$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$initialState$$inlined$let$lambda$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 265
    invoke-virtual {v0, p2, v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer;->fromSnapshot(Lcom/squareup/workflow/Snapshot;Lkotlin/jvm/functions/Function0;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    move-result-object p2

    if-eqz p2, :cond_0

    goto :goto_0

    .line 270
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->initialSelectMethodState(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object p1

    move-object p2, p1

    check-cast p2, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    :goto_0
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 183
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->initialState(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 183
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    check-cast p2, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->render(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "-",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 299
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->startCheckoutSideEffect:Lcom/squareup/workflow/Worker;

    const-string v1, "StartSideEffect"

    .line 298
    invoke-static {p3, v0, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 304
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->touchEventLoggerSideEffect:Lcom/squareup/workflow/Worker;

    const-string v1, "TouchMonitor"

    .line 303
    invoke-static {p3, v0, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 309
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->x2InitialPromptForPaymentSideEffectWorker:Lcom/squareup/workflow/Worker;

    const-string v1, "Start#X2PromptForPayment"

    .line 308
    invoke-static {p3, v0, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 314
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->onPaymentMethodSelected()Lio/reactivex/Observable;

    move-result-object v0

    const-string/jumbo v1, "x2SellerScreenRunner.onPaymentMethodSelected()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1373
    sget-object v1, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    const-string/jumbo v1, "this.toFlowable(BUFFER)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    const-string v2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    if-eqz v0, :cond_d

    .line 1375
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 1376
    const-class v3, Lkotlin/Unit;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v3, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v4, Lcom/squareup/workflow/Worker;

    .line 316
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$1;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const-string v3, "X2PaymentMethodSelected"

    .line 313
    invoke-interface {p3, v4, v3, v0}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 319
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->branReady()Lio/reactivex/Observable;

    move-result-object v0

    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 320
    invoke-virtual {v0, v3, v4}, Lio/reactivex/Observable;->buffer(II)Lio/reactivex/Observable;

    move-result-object v0

    .line 321
    new-instance v3, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$2;

    invoke-direct {v3, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$2;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v3, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string/jumbo v3, "x2SellerScreenRunner.bra\u2026falseThenTrueFilter(it) }"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1377
    sget-object v3, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v3}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_c

    .line 1379
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 1380
    const-class v3, Ljava/util/List;

    sget-object v4, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v5, Ljava/lang/Boolean;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v3

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v3, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v4, Lcom/squareup/workflow/Worker;

    .line 324
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$3;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$3;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const-string v3, "X2BranReady"

    .line 318
    invoke-interface {p3, v4, v3, v0}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 327
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->bus:Lcom/squareup/badbus/BadBus;

    const-class v3, Lcom/squareup/autocapture/PerformAutoCapture;

    invoke-virtual {v0, v3}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v3, "bus.events(PerformAutoCapture::class.java)"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1381
    sget-object v3, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v3}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_b

    .line 1383
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 1384
    const-class v3, Lcom/squareup/autocapture/PerformAutoCapture;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v3, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v4, Lcom/squareup/workflow/Worker;

    .line 329
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$4;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$4;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const-string v3, "PerformAutoCapture"

    .line 326
    invoke-interface {p3, v4, v3, v0}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 331
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;->getSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getToastData()Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/tenderpayment/SelectMethod$ToastData;->type:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    sget-object v3, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->NONE:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    const/4 v4, 0x0

    if-eq v0, v3, :cond_0

    .line 332
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$5;

    invoke-direct {v0, v4}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$5;-><init>(Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 1385
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 1386
    const-class v3, Lkotlin/Unit;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    new-instance v5, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v5, v3, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v7, v5

    check-cast v7, Lcom/squareup/workflow/Worker;

    const/4 v8, 0x0

    .line 332
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$6;

    invoke-direct {v0, p0, p2}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$6;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)V

    move-object v9, v0

    check-cast v9, Lkotlin/jvm/functions/Function1;

    const/4 v10, 0x2

    const/4 v11, 0x0

    move-object v6, p3

    invoke-static/range {v6 .. v11}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->stateChangedWorker:Lcom/squareup/workflow/Worker;

    .line 337
    new-instance v3, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$7;

    invoke-direct {v3, p0, p2, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$7;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const-string v5, "OnStateChanged"

    .line 335
    invoke-interface {p3, v0, v5, v3}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 354
    instance-of v0, p2, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    if-eqz v0, :cond_1

    .line 355
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v5, 0x23

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;->getSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getShowSecondaryMethods()Z

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 357
    :cond_1
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v5, "state::class.java.simpleName"

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 361
    :goto_0
    iget-object v5, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->checkoutLoggerShowScreenSideEffect:Lcom/squareup/workflow/Worker;

    .line 360
    invoke-static {p3, v5, v3}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 365
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;->getSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isRunningState()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 366
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;->getSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getReaderSessionId()Ljava/util/UUID;

    move-result-object v3

    .line 367
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;->getSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->shouldEnableNfcField(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)Z

    move-result v5

    .line 370
    iget-object v6, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-static {v6, v5}, Lcom/squareup/tenderpayment/PaymentInputHandlersKt;->awaitPaymentEvent(Lcom/squareup/ui/main/errors/PaymentInputHandler;Z)Lio/reactivex/Observable;

    move-result-object v6

    .line 1387
    sget-object v7, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v6, v7}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v6

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Lorg/reactivestreams/Publisher;

    if-eqz v6, :cond_2

    .line 1389
    invoke-static {v6}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 1390
    const-class v2, Lcom/squareup/tenderpayment/PaymentInputHandlerOutput;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v6, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v6, v2, v1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v6, Lcom/squareup/workflow/Worker;

    .line 371
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ReaderState:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v2, 0x3a

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 372
    new-instance v2, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$8;

    invoke-direct {v2, p0, p2, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$8;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 369
    invoke-interface {p3, v6, v1, v2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    goto :goto_1

    .line 1389
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    if-eqz v0, :cond_5

    .line 390
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$9;

    invoke-direct {v1, p0, v4}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$9;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object v0

    const-string v1, "SelectMethodState#X2PromptForPayment"

    invoke-static {p3, v0, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 395
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;

    invoke-direct {v0, p0, p3, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->WorkflowInput(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 433
    move-object v1, p2

    check-cast v1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    .line 430
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->createSelectMethodScreenData(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    move-result-object p1

    .line 436
    invoke-virtual {v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getShowSecondaryMethods()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 438
    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->nonCardSelectedTutorialSideEffectWorker:Lcom/squareup/workflow/Worker;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 437
    invoke-static {p3, v2, p2}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 441
    sget-object p2, Lcom/squareup/tenderpayment/SelectMethod;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod;

    .line 442
    iget-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->selectMethodScreensFactory:Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    .line 444
    invoke-virtual {v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getCompletedTenders()Ljava/util/List;

    move-result-object v1

    .line 442
    invoke-virtual {p3, p1, v1, v0}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;->createSecondarySelectMethodScreen(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Ljava/util/List;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 441
    invoke-virtual {p2, p1}, Lcom/squareup/tenderpayment/SelectMethod;->onlyCard(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_2

    .line 449
    :cond_4
    sget-object p2, Lcom/squareup/tenderpayment/SelectMethod;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod;

    .line 450
    iget-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->selectMethodScreensFactory:Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    .line 452
    invoke-virtual {v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getCompletedTenders()Ljava/util/List;

    move-result-object v1

    .line 450
    invoke-virtual {p3, p1, v1, v0}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;->createPrimarySelectMethodScreen(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Ljava/util/List;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 449
    invoke-virtual {p2, p1}, Lcom/squareup/tenderpayment/SelectMethod;->onlyCard(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_2

    .line 459
    :cond_5
    instance-of v0, p2, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;

    if-eqz v0, :cond_6

    .line 462
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 463
    check-cast p2, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;

    invoke-virtual {p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;->getPreviousSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v1

    .line 460
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->createSelectMethodScreenData(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    move-result-object v0

    .line 466
    new-instance v1, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;

    .line 467
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->getTenderAmount(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 468
    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getCustomerDisplayNameOrBlank()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "transaction.customerDisplayNameOrBlank"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 469
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;->getCardNameAndNumber()Ljava/lang/String;

    move-result-object v3

    .line 470
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;->getInstrumentToken()Ljava/lang/String;

    move-result-object v4

    .line 466
    invoke-direct {v1, p1, v2, v3, v4}, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->selectMethodScreensFactory:Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    .line 476
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ConfirmingChargeCardOnFileState;->getPreviousSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getShowSecondaryMethods()Z

    move-result p2

    .line 474
    invoke-virtual {p1, v0, p2}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;->createSelectMethodScreenForBackground(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Z)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 478
    iget-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->selectMethodScreensFactory:Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    .line 480
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$10;

    invoke-direct {v0, p0, p3}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$10;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 478
    invoke-virtual {p2, v1, v0}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;->createConfirmChargeCardOnFileDialogScreen(Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    .line 473
    invoke-static {p1, p2}, Lcom/squareup/tenderpayment/SelectMethod;->dialogStack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_2

    .line 495
    :cond_6
    instance-of v0, p2, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillState;

    if-eqz v0, :cond_7

    .line 498
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 499
    check-cast p2, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillState;

    invoke-virtual {p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillState;->getPreviousSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v1

    .line 496
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->createSelectMethodScreenData(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    move-result-object p1

    .line 503
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->selectMethodScreensFactory:Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    .line 505
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillState;->getPreviousSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getShowSecondaryMethods()Z

    move-result p2

    .line 503
    invoke-virtual {v0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;->createSelectMethodScreenForBackground(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Z)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    .line 507
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->selectMethodScreensFactory:Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    .line 508
    iget-object p1, p1, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->amountDue:Lcom/squareup/protos/common/Money;

    .line 509
    new-instance v1, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$11;

    invoke-direct {v1, p0, p3}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$11;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 507
    invoke-virtual {v0, p1, v1}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;->createCancelSplitTenderTransactionDialogScreen(Lcom/squareup/protos/common/Money;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 502
    invoke-static {p2, p1}, Lcom/squareup/tenderpayment/SelectMethod;->dialogStack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_2

    .line 523
    :cond_7
    instance-of v0, p2, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$CancellingBillCheckingPermissionsState;

    if-eqz v0, :cond_8

    .line 524
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->permissionWorkflow:Lcom/squareup/permissionworkflow/PermissionWorkflow;

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    new-instance v2, Lcom/squareup/permissionworkflow/PermissionRequestInput$ExplicitAccess;

    sget-object p1, Lcom/squareup/permissions/Permission;->CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

    invoke-direct {v2, p1}, Lcom/squareup/permissionworkflow/PermissionRequestInput$ExplicitAccess;-><init>(Lcom/squareup/permissions/Permission;)V

    const/4 v3, 0x0

    new-instance p1, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$12;

    invoke-direct {p1, p0}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$12;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    .line 532
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object p1

    goto :goto_2

    .line 535
    :cond_8
    instance-of v0, p2, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SplitTenderWarningState;

    if-eqz v0, :cond_9

    .line 538
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 539
    check-cast p2, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SplitTenderWarningState;

    invoke-virtual {p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SplitTenderWarningState;->getPreviousSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v1

    .line 536
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->createSelectMethodScreenData(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    move-result-object p1

    .line 543
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->selectMethodScreensFactory:Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    .line 545
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SplitTenderWarningState;->getPreviousSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getShowSecondaryMethods()Z

    move-result p2

    .line 543
    invoke-virtual {v0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;->createSelectMethodScreenForBackground(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Z)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 547
    iget-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->selectMethodScreensFactory:Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    .line 548
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$13;

    invoke-direct {v0, p0, p3}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$13;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 547
    invoke-virtual {p2, v0}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;->createSplitTenderWarningDialogScreen(Lkotlin/jvm/functions/Function0;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    .line 542
    invoke-static {p1, p2}, Lcom/squareup/tenderpayment/SelectMethod;->dialogStack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto :goto_2

    .line 553
    :cond_9
    instance-of v0, p2, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;

    if-eqz v0, :cond_a

    .line 556
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 557
    move-object v1, p2

    check-cast v1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;->getPreviousSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v2

    .line 554
    invoke-direct {p0, p1, v0, v2}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->createSelectMethodScreenData(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    move-result-object v0

    .line 561
    new-instance v2, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;-><init>(Lcom/squareup/protos/common/Money;)V

    .line 564
    iget-object v3, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->selectMethodScreensFactory:Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    .line 566
    invoke-virtual {v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$ShowingCashDialogState;->getPreviousSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->getShowSecondaryMethods()Z

    move-result v1

    .line 564
    invoke-virtual {v3, v0, v1}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;->createSelectMethodScreenForBackground(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Z)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v0

    .line 568
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->selectMethodScreensFactory:Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    .line 570
    new-instance v3, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$14;

    invoke-direct {v3, p0, p3, p2, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$14;-><init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 568
    invoke-virtual {v1, v2, v3}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;->createConfirmCollectCashDialogScreen(Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 563
    invoke-static {v0, p1}, Lcom/squareup/tenderpayment/SelectMethod;->dialogStack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    :goto_2
    return-object p1

    :cond_a
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 1383
    :cond_b
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1379
    :cond_c
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1375
    :cond_d
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 290
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer;

    invoke-virtual {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowStateSerializer;->snapshotState(Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 183
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->snapshotState(Lcom/squareup/tenderpayment/SelectMethodWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
