.class public final enum Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;
.super Ljava/lang/Enum;
.source "SelectMethodScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ToastDataType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;",
        "",
        "(Ljava/lang/String;I)V",
        "NONE",
        "CONTACTLESS_REENABLED",
        "REMOVE_CHIP_CARD",
        "PAYMENT_OUT_OF_RANGE",
        "PAYMENT_OUT_OF_RANGE_GIFT_CARD",
        "SWIPE_STRAIGHT",
        "TRY_AGAIN",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

.field public static final enum CONTACTLESS_REENABLED:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

.field public static final enum NONE:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

.field public static final enum PAYMENT_OUT_OF_RANGE:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

.field public static final enum PAYMENT_OUT_OF_RANGE_GIFT_CARD:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

.field public static final enum REMOVE_CHIP_CARD:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

.field public static final enum SWIPE_STRAIGHT:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

.field public static final enum TRY_AGAIN:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    new-instance v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    const/4 v2, 0x0

    const-string v3, "NONE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->NONE:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    const/4 v2, 0x1

    const-string v3, "CONTACTLESS_REENABLED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->CONTACTLESS_REENABLED:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    const/4 v2, 0x2

    const-string v3, "REMOVE_CHIP_CARD"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->REMOVE_CHIP_CARD:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    const/4 v2, 0x3

    const-string v3, "PAYMENT_OUT_OF_RANGE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->PAYMENT_OUT_OF_RANGE:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    const/4 v2, 0x4

    const-string v3, "PAYMENT_OUT_OF_RANGE_GIFT_CARD"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->PAYMENT_OUT_OF_RANGE_GIFT_CARD:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    const/4 v2, 0x5

    const-string v3, "SWIPE_STRAIGHT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->SWIPE_STRAIGHT:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    const/4 v2, 0x6

    const-string v3, "TRY_AGAIN"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->TRY_AGAIN:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->$VALUES:[Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 167
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;
    .locals 1

    const-class v0, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;
    .locals 1

    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->$VALUES:[Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    invoke-virtual {v0}, [Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    return-object v0
.end method
