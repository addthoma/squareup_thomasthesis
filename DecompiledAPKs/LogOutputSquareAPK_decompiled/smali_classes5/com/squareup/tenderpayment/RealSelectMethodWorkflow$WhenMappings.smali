.class public final synthetic Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I

.field public static final synthetic $EnumSwitchMapping$3:[I

.field public static final synthetic $EnumSwitchMapping$4:[I

.field public static final synthetic $EnumSwitchMapping$5:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->values()[Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    invoke-static {}, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->values()[Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ENDED_INVOICE_PAYMENT:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ENDED_API_TRANSACTION:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->values()[Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->THIRD_PARTY_CARD:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->DEBIT_OR_CREDIT:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->values()[Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CARD_ON_FILE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CASH:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->values()[Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW_AND_AUTHORIZE:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->SHOW_OFFLINE_MODE_TRANSACTION_LIMIT_REACHED_WARNING_DIALOG_SCREEN:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->DO_NOTHING:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v4, 0x4

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->SHOW_POS_APPLET:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v4, 0x5

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;->values()[Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v1, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;->CONFIRM_CANCEL_SPLIT_TENDER_TRANSACTION:Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;

    invoke-virtual {v1}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v1, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;->DISMISS_CANCEL_SPLIT_TENDER_TRANSACTION:Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;

    invoke-virtual {v1}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;->ordinal()I

    move-result v1

    aput v3, v0, v1

    return-void
.end method
