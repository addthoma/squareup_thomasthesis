.class public final Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnChargeCardOnFileEvent;
.super Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;
.source "SelectMethodWorkflowEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnChargeCardOnFileEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnChargeCardOnFileEvent;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;",
        "tenderedAmount",
        "Lcom/squareup/protos/common/Money;",
        "instrumentToken",
        "",
        "(Lcom/squareup/protos/common/Money;Ljava/lang/String;)V",
        "getInstrumentToken$tender_payment_release",
        "()Ljava/lang/String;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final instrumentToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "tenderedAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instrumentToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    sget-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager;->CARD_ON_FILE:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    const-string v1, "TenderSettingsManager.CARD_ON_FILE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/common/Money;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnChargeCardOnFileEvent;->instrumentToken:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getInstrumentToken$tender_payment_release()Ljava/lang/String;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnChargeCardOnFileEvent;->instrumentToken:Ljava/lang/String;

    return-object v0
.end method
