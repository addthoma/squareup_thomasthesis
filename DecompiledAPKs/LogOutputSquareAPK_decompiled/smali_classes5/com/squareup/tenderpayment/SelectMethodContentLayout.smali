.class public Lcom/squareup/tenderpayment/SelectMethodContentLayout;
.super Landroid/view/ViewGroup;
.source "SelectMethodContentLayout.java"


# instance fields
.field private currentPaymentTypeContainerMargin:I

.field private final paymentTypeContainerMarginWhenScrolling:I

.field private final paymentTypeOptionsScrollView:Landroid/view/View;

.field private final paymentTypeStatusHeader:Landroid/view/View;

.field private totalHeight:I

.field private totalWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x0

    .line 32
    iput p2, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->currentPaymentTypeContainerMargin:I

    .line 33
    iput p2, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->totalWidth:I

    .line 34
    iput p2, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->totalHeight:I

    .line 39
    sget p2, Lcom/squareup/tenderworkflow/R$layout;->select_method_content_layout:I

    invoke-static {p1, p2, p0}, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->getPaymentContainerMargin(Landroid/content/Context;)I

    move-result p1

    iput p1, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->paymentTypeContainerMarginWhenScrolling:I

    .line 43
    sget p1, Lcom/squareup/tenderworkflow/R$id;->payment_type_status_header:I

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->paymentTypeStatusHeader:Landroid/view/View;

    .line 44
    sget p1, Lcom/squareup/tenderworkflow/R$id;->payment_type_options_scroll_view:I

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->paymentTypeOptionsScrollView:Landroid/view/View;

    return-void
.end method

.method private getPaymentContainerMargin(Landroid/content/Context;)I
    .locals 1

    .line 135
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/tenderworkflow/R$dimen;->payment_options_container_margin_bottom:I

    .line 136
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    return p1
.end method

.method private layoutChildVertical(Landroid/view/View;II)I
    .locals 2

    .line 60
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 61
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, p2

    add-int/2addr v1, p3

    .line 63
    invoke-virtual {p1, p2, p3, v0, v1}, Landroid/view/View;->layout(IIII)V

    return v1
.end method

.method private measureAndCalculateTotalDimens(Landroid/view/View;II)V
    .locals 0

    .line 129
    invoke-virtual {p1, p2, p3}, Landroid/view/View;->measure(II)V

    .line 130
    iget p2, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->totalWidth:I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p3

    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->totalWidth:I

    .line 131
    iget p2, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->totalHeight:I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p1

    add-int/2addr p2, p1

    iput p2, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->totalHeight:I

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 0

    .line 52
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->paymentTypeStatusHeader:Landroid/view/View;

    const/4 p2, 0x0

    invoke-direct {p0, p1, p2, p2}, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->layoutChildVertical(Landroid/view/View;II)I

    move-result p1

    .line 55
    iget p3, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->currentPaymentTypeContainerMargin:I

    add-int/2addr p1, p3

    .line 56
    iget-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->paymentTypeOptionsScrollView:Landroid/view/View;

    invoke-direct {p0, p3, p2, p1}, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->layoutChildVertical(Landroid/view/View;II)I

    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .line 70
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    const/4 v1, 0x0

    .line 73
    iput v1, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->currentPaymentTypeContainerMargin:I

    .line 76
    iput v1, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->totalWidth:I

    .line 77
    iput v1, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->totalHeight:I

    const/high16 v1, -0x80000000

    .line 82
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 85
    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->paymentTypeStatusHeader:Landroid/view/View;

    invoke-direct {p0, v2, p1, v1}, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->measureAndCalculateTotalDimens(Landroid/view/View;II)V

    .line 89
    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->paymentTypeOptionsScrollView:Landroid/view/View;

    invoke-direct {p0, v2, p1, v1}, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->measureAndCalculateTotalDimens(Landroid/view/View;II)V

    .line 93
    iget v1, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->totalHeight:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->paymentTypeContainerMarginWhenScrolling:I

    sub-int/2addr v0, v1

    const/high16 v2, 0x40000000    # 2.0f

    if-le v0, v1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->paymentTypeStatusHeader:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v0

    .line 103
    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 105
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->paymentTypeStatusHeader:Landroid/view/View;

    invoke-virtual {v1, p1, v0}, Landroid/view/View;->measure(II)V

    goto :goto_0

    .line 111
    :cond_0
    iput v1, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->currentPaymentTypeContainerMargin:I

    .line 113
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->paymentTypeOptionsScrollView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v0

    .line 118
    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 120
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->paymentTypeOptionsScrollView:Landroid/view/View;

    invoke-virtual {v1, p1, v0}, Landroid/view/View;->measure(II)V

    .line 123
    :goto_0
    iget v0, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->totalWidth:I

    invoke-static {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->resolveSize(II)I

    move-result p1

    iget v0, p0, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->totalHeight:I

    .line 124
    invoke-static {v0, p2}, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->resolveSize(II)I

    move-result p2

    .line 123
    invoke-virtual {p0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodContentLayout;->setMeasuredDimension(II)V

    return-void
.end method
