.class final Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$3;
.super Ljava/lang/Object;
.source "RealSelectMethodWorkflow.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->createScreen(Lcom/squareup/workflow/legacy/Screen$Key;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00040\u00012\u000e\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "data",
        "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$3;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 229
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$3;->call(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 1319
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$3;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    .line 1320
    invoke-static {v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$getSelectMethodScreensFactory$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    move-result-object v1

    const-string v2, "data"

    .line 1321
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1322
    iget-object v2, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$3;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {v2}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$getCompletedTenders$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Ljava/util/List;

    move-result-object v2

    .line 1323
    iget-object v3, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$3;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {v3}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$getSelectMethodInput$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 1320
    invoke-virtual {v1, p1, v2, v3}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;->createSecondarySelectMethodScreen(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Ljava/util/List;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 1319
    invoke-static {v0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$onlyCard(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
