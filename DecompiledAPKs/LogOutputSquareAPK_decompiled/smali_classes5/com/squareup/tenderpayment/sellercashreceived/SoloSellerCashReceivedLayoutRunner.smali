.class public final Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;
.super Ljava/lang/Object;
.source "SoloSellerCashReceivedLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001bB#\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0018\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0010\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0002H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedScreen;",
        "view",
        "Landroid/view/View;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Landroid/view/View;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V",
        "continueButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "glyph",
        "Lcom/squareup/marin/widgets/MarinSpinnerGlyph;",
        "hasTransitioned",
        "",
        "subtitleTextView",
        "Lcom/squareup/widgets/MessageView;",
        "titleTextView",
        "Lcom/squareup/marketfont/MarketTextView;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateText",
        "Factory",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final continueButton:Lcom/squareup/marketfont/MarketButton;

.field private final glyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

.field private hasTransitioned:Z

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final subtitleTextView:Lcom/squareup/widgets/MessageView;

.field private final titleTextView:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->res:Lcom/squareup/util/Res;

    .line 22
    sget p2, Lcom/squareup/tenderworkflow/R$id;->change_glyph:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string/jumbo p3, "view.findViewById(R.id.change_glyph)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    iput-object p2, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->glyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    .line 24
    sget p2, Lcom/squareup/tenderworkflow/R$id;->seller_cash_received_title:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string/jumbo p3, "view.findViewById(R.id.seller_cash_received_title)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/marketfont/MarketTextView;

    iput-object p2, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->titleTextView:Lcom/squareup/marketfont/MarketTextView;

    .line 26
    sget p2, Lcom/squareup/tenderworkflow/R$id;->seller_cash_received_subtitle:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string/jumbo p3, "view.findViewById(R.id.s\u2026r_cash_received_subtitle)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/widgets/MessageView;

    iput-object p2, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->subtitleTextView:Lcom/squareup/widgets/MessageView;

    .line 28
    sget p2, Lcom/squareup/tenderworkflow/R$id;->seller_cash_received_continue:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.s\u2026r_cash_received_continue)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->continueButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method private final updateText(Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedScreen;)V
    .locals 3

    .line 63
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedScreen;->getHasChange()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/payment/R$string;->change_hud_amount_change:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedScreen;->getChangeAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "amount"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "res.phrase(com.squareup.\u2026unt))\n          .format()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    iget-object v1, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/payment/R$string;->change_hud_out_of_total:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 68
    iget-object v2, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedScreen;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string/jumbo v2, "total"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 69
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 71
    :cond_0
    iget-object p1, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/payment/R$string;->change_hud_no_change:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    const/4 p1, 0x0

    .line 72
    check-cast p1, Ljava/lang/CharSequence;

    .line 74
    :goto_0
    iget-object v1, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->titleTextView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->subtitleTextView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-boolean p2, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->hasTransitioned:Z

    if-nez p2, :cond_0

    .line 47
    iget-object p2, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->glyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    invoke-virtual {p2}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToSuccess()V

    const/4 p2, 0x1

    .line 48
    iput-boolean p2, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->hasTransitioned:Z

    .line 51
    :cond_0
    iget-object p2, p0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->continueButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$showRendering$1;-><init>(Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedScreen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->updateText(Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedScreen;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner;->showRendering(Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
