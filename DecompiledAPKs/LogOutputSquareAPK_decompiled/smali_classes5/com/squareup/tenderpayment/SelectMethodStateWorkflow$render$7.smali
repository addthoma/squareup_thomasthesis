.class final Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$7;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectMethodStateWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->render(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/util/tuple/Quintuple<",
        "Lcom/squareup/connectivity/InternetState;",
        "Ljava/lang/Boolean;",
        "Ljava/util/EnumSet<",
        "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
        ">;",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
        "+",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012h\u0010\u0004\u001ad\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00060\u0006\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00080\u0008\u0012(\u0012&\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\n0\n \u0007*\u0012\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\n0\n\u0018\u00010\t0\t\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00080\u0008\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00080\u00080\u0005H\n\u00a2\u0006\u0002\u0008\u000b"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/util/tuple/Quintuple;",
        "Lcom/squareup/connectivity/InternetState;",
        "kotlin.jvm.PlatformType",
        "",
        "Ljava/util/EnumSet;",
        "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

.field final synthetic $state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

.field final synthetic this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$7;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$7;->$state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$7;->$props:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/util/tuple/Quintuple;)Lcom/squareup/workflow/WorkflowAction;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/tuple/Quintuple<",
            "Lcom/squareup/connectivity/InternetState;",
            "Ljava/lang/Boolean;",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Quintuple;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/connectivity/InternetState;

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Quintuple;->component2()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Quintuple;->component3()Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Ljava/util/EnumSet;

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Quintuple;->component4()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Quintuple;->component5()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    .line 342
    iget-object v3, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$7;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    .line 343
    iget-object v4, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$7;->$state:Lcom/squareup/tenderpayment/SelectMethodWorkflowState;

    .line 344
    sget-object v5, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne v0, v5, :cond_0

    const/4 v0, 0x1

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v5, 0x0

    :goto_0
    const-string v0, "isInOfflineMode"

    .line 345
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    const-string v0, "buyerCheckoutEnabled"

    .line 346
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    const-string v0, "isRunningState"

    .line 347
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 348
    iget-object v9, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$7;->$props:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    const-string p1, "readerCapabilities"

    .line 349
    invoke-static {v10, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 342
    invoke-static/range {v3 .. v10}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$updateSelectMethodStateAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;ZZZZLcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Ljava/util/EnumSet;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 183
    check-cast p1, Lcom/squareup/util/tuple/Quintuple;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$7;->invoke(Lcom/squareup/util/tuple/Quintuple;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
