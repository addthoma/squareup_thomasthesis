.class public Lcom/squareup/tenderpayment/events/SplitTenderTappedEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "SplitTenderTappedEvent.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 10
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->TAP:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SPLIT_TENDER:Lcom/squareup/analytics/RegisterTapName;

    iget-object v1, v1, Lcom/squareup/analytics/RegisterTapName;->value:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method
