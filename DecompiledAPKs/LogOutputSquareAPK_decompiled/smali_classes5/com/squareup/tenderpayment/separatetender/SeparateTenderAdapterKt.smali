.class public final Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapterKt;
.super Ljava/lang/Object;
.source "SeparateTenderAdapter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "COMPLETED_PAYMENTS_HEADER",
        "",
        "COMPLETED_PAYMENT_ROW",
        "EDIT_TENDER_ROW",
        "EVEN_SPLITS_ROW",
        "HELP_BANNER",
        "STATIC_ROWS",
        "tender-payment_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final COMPLETED_PAYMENTS_HEADER:I = 0x3

.field private static final COMPLETED_PAYMENT_ROW:I = 0x4

.field private static final EDIT_TENDER_ROW:I = 0x1

.field private static final EVEN_SPLITS_ROW:I = 0x2

.field private static final HELP_BANNER:I = 0x0

.field private static final STATIC_ROWS:I = 0x3
