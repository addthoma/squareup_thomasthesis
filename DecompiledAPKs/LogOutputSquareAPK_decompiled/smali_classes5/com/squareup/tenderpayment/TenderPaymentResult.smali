.class public abstract Lcom/squareup/tenderpayment/TenderPaymentResult;
.super Ljava/lang/Object;
.source "TenderPaymentResult.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledApiPayment;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledBillPayment;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledInvoicePayment;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$CreateInvoice;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$EmoneyBrandSelected;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$PaidNeedToAuthorize;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$PayCashCustomAmount;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$PayCard;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$PayContactless;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$PayCardOnFileWithCurrentCustomer;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$SelectCustomerAndPayCardOnFile;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$PayGiftCard;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$RecordCardPayment;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$RecordCardPaymentDebitCredit;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$PayGiftCardOnFileWithCurrentCustomer;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$ShowBuyerPip;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$SplitTenderAutoCaptureVoid;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$SplitTenderRecordCardPayment;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$DoNothing;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSelectMethod;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$ShowPosApplet;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSingleTenderEmvDip;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSplitTenderEmvDip;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSplitTenderEmvDipWithTipApplied;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessContactless;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$ExitWithReaderIssue;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$ExitWithReaderNfcTimeout;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$SuccessfulSwipe;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$RestartSelectMethod;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$QuickCashReceived;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSeparateTenders;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$SeparateTenderPaymentResult;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$TenderOptionSelected;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$UnsuccessfullyComplete;,
        Lcom/squareup/tenderpayment/TenderPaymentResult$SuccessfullyComplete;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a2\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:%\u0003\u0004\u0005\u0006\u0007\u0008\t\n\u000b\u000c\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f !\"#$%&\'B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001%()*+,-./0123456789:;<=>?@ABCDEFGHIJKL\u00a8\u0006M"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "",
        "()V",
        "CanceledApiPayment",
        "CanceledBillPayment",
        "CanceledInvoicePayment",
        "CreateInvoice",
        "DoNothing",
        "EmoneyBrandSelected",
        "ExitWithReaderIssue",
        "ExitWithReaderNfcTimeout",
        "Paid",
        "PaidNeedToAuthorize",
        "PayCard",
        "PayCardOnFileWithCurrentCustomer",
        "PayCashCustomAmount",
        "PayContactless",
        "PayGiftCard",
        "PayGiftCardOnFileWithCurrentCustomer",
        "PayOther",
        "ProcessContactless",
        "ProcessSingleTenderEmvDip",
        "ProcessSplitTenderEmvDip",
        "ProcessSplitTenderEmvDipWithTipApplied",
        "QuickCashReceived",
        "RecordCardPayment",
        "RecordCardPaymentDebitCredit",
        "RestartSelectMethod",
        "SelectCustomerAndPayCardOnFile",
        "SeparateTenderPaymentResult",
        "ShowBuyerPip",
        "ShowPosApplet",
        "ShowSelectMethod",
        "ShowSeparateTenders",
        "SplitTenderAutoCaptureVoid",
        "SplitTenderRecordCardPayment",
        "SuccessfulSwipe",
        "SuccessfullyComplete",
        "TenderOptionSelected",
        "UnsuccessfullyComplete",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledApiPayment;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledBillPayment;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledInvoicePayment;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$CreateInvoice;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$EmoneyBrandSelected;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$PaidNeedToAuthorize;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$PayCashCustomAmount;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$PayCard;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$PayContactless;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$PayCardOnFileWithCurrentCustomer;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$SelectCustomerAndPayCardOnFile;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$PayGiftCard;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$RecordCardPayment;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$RecordCardPaymentDebitCredit;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$PayGiftCardOnFileWithCurrentCustomer;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$ShowBuyerPip;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$SplitTenderAutoCaptureVoid;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$SplitTenderRecordCardPayment;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$DoNothing;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSelectMethod;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$ShowPosApplet;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSingleTenderEmvDip;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSplitTenderEmvDip;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSplitTenderEmvDipWithTipApplied;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessContactless;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$ExitWithReaderIssue;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$ExitWithReaderNfcTimeout;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$SuccessfulSwipe;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$RestartSelectMethod;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$QuickCashReceived;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSeparateTenders;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$SeparateTenderPaymentResult;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$TenderOptionSelected;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$UnsuccessfullyComplete;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult$SuccessfullyComplete;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentResult;-><init>()V

    return-void
.end method
