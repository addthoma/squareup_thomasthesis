.class public interface abstract Lcom/squareup/tenderpayment/TenderCompleter;
.super Ljava/lang/Object;
.source "TenderCompleter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;,
        Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    }
.end annotation


# virtual methods
.method public abstract cancelPaymentFlow(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;
.end method

.method public abstract chargeCardOnFile(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
.end method

.method public abstract completeTenderAndAuthorize(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
.end method

.method public abstract onPrepareChangeHud()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/payment/CompletedPayment;",
            ">;"
        }
    .end annotation
.end method

.method public abstract payCardTender(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
.end method

.method public abstract payCardTender(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
.end method

.method public abstract payCashTender(Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
.end method

.method public abstract payOtherTender(Ljava/lang/String;Lcom/squareup/server/account/protos/OtherTenderType;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
.end method

.method public abstract payOtherTender(Ljava/lang/String;Lcom/squareup/server/account/protos/OtherTenderType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
.end method
