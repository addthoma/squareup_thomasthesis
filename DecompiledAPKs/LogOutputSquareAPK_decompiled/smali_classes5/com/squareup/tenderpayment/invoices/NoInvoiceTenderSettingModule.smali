.class public abstract Lcom/squareup/tenderpayment/invoices/NoInvoiceTenderSettingModule;
.super Ljava/lang/Object;
.source "NoInvoiceTenderSettingModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideInvoiceTenderSetting(Lcom/squareup/tenderpayment/invoices/NoInvoiceTenderSetting;)Lcom/squareup/tenderpayment/InvoiceTenderSetting;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
