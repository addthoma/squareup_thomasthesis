.class public interface abstract Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory$ParentComponent;
.super Ljava/lang/Object;
.source "ConfirmCollectCashDialogFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ParentComponent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003j\u0002`\u0006H&\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory$ParentComponent;",
        "",
        "dialogWorkflowScreen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;",
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;",
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialogScreen;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract dialogWorkflowScreen()Lcom/squareup/workflow/legacy/Screen;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;",
            "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;",
            ">;"
        }
    .end annotation
.end method
