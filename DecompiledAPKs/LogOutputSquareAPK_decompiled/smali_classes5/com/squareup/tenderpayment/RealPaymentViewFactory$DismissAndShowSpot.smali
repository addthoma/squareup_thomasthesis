.class final Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;
.super Lcom/squareup/container/spot/Spot;
.source "RealPaymentViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/RealPaymentViewFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DismissAndShowSpot"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\u0005\u0008\u0002\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J(\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0014J(\u0010\u000c\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0014J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\nJ(\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0014J(\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0014\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;",
        "Lcom/squareup/container/spot/Spot;",
        "()V",
        "backwardIncomingAnimation",
        "",
        "set",
        "Landroid/animation/AnimatorSet;",
        "hostContainer",
        "Landroid/view/ViewGroup;",
        "incomingView",
        "Landroid/view/View;",
        "outgoingView",
        "backwardOutgoingAnimation",
        "durationForView",
        "",
        "view",
        "forwardIncomingAnimation",
        "forwardOutgoingAnimation",
        "Companion",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot$Companion;

.field private static final INSTANCE:Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;->Companion:Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot$Companion;

    .line 166
    new-instance v0, Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;

    invoke-direct {v0}, Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;-><init>()V

    sput-object v0, Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;->INSTANCE:Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;

    .line 168
    sget-object v0, Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;->INSTANCE:Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;

    check-cast v0, Lcom/squareup/container/spot/Spot;

    invoke-static {v0}, Lcom/squareup/container/spot/Spot;->forSpotSingleton(Lcom/squareup/container/spot/Spot;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    const-string v1, "Spot.forSpotSingleton(INSTANCE)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    .line 121
    invoke-direct {p0, v0}, Lcom/squareup/container/spot/Spot;-><init>(Z)V

    return-void
.end method

.method public static final synthetic access$getINSTANCE$cp()Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;
    .locals 1

    .line 121
    sget-object v0, Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;->INSTANCE:Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;

    return-object v0
.end method


# virtual methods
.method protected backwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 1

    const-string v0, "set"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "hostContainer"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "incomingView"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "outgoingView"

    invoke-static {p4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method protected backwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 1

    const-string v0, "set"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "hostContainer"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "incomingView"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "outgoingView"

    invoke-static {p4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final durationForView(Landroid/view/View;)J
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 v0, 0x10e0000

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method protected forwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 2

    const-string v0, "set"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hostContainer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "incomingView"

    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "outgoingView"

    invoke-static {p4, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    sget-object p2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 p4, 0x2

    new-array p4, p4, [F

    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const/4 v1, 0x0

    aput v0, p4, v1

    const/4 v0, 0x1

    const/4 v1, 0x0

    aput v1, p4, v0

    invoke-static {p3, p2, p4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p2

    const-string p4, "anim"

    .line 129
    invoke-static {p2, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p3}, Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;->durationForView(Landroid/view/View;)J

    move-result-wide p3

    invoke-virtual {p2, p3, p4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 130
    check-cast p2, Landroid/animation/Animator;

    invoke-virtual {p1, p2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    return-void
.end method

.method protected forwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 2

    const-string v0, "set"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hostContainer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "incomingView"

    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "outgoingView"

    invoke-static {p4, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    sget-object p2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 p3, 0x2

    new-array p3, p3, [F

    const/4 v0, 0x0

    const/4 v1, 0x0

    aput v1, p3, v0

    invoke-virtual {p4}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const/4 v1, 0x1

    aput v0, p3, v1

    invoke-static {p4, p2, p3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p2

    const-string p3, "anim"

    .line 140
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p4}, Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;->durationForView(Landroid/view/View;)J

    move-result-wide p3

    invoke-virtual {p2, p3, p4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 141
    check-cast p2, Landroid/animation/Animator;

    invoke-virtual {p1, p2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    return-void
.end method
