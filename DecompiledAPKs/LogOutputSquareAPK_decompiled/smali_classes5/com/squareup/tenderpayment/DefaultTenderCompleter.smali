.class public Lcom/squareup/tenderpayment/DefaultTenderCompleter;
.super Ljava/lang/Object;
.source "DefaultTenderCompleter.java"

# interfaces
.implements Lcom/squareup/tenderpayment/TenderCompleter;


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final apiTransactionController:Lcom/squareup/api/ApiTransactionController;

.field private final cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

.field private final checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

.field private final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final instrumentOnFileSellerWorkflow:Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

.field private final paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

.field private final postReceiptOperations:Lcom/squareup/payment/PostReceiptOperations;

.field private final prepareChangeHud:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/payment/CompletedPayment;",
            ">;"
        }
    .end annotation
.end field

.field private final printingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final receiptSender:Lcom/squareup/receipt/ReceiptSender;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

.field private final synchronousLocalPaymentPresenter:Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;

.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/analytics/Analytics;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/payment/PostReceiptOperations;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->prepareChangeHud:Lcom/jakewharton/rxrelay2/PublishRelay;

    move-object v1, p1

    .line 108
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-object v1, p2

    .line 109
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p3

    .line 110
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    move-object v1, p4

    .line 111
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    move-object v1, p5

    .line 112
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    move-object v1, p6

    .line 113
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    move-object v1, p7

    .line 114
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    move-object v1, p8

    .line 115
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->instrumentOnFileSellerWorkflow:Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;

    move-object v1, p9

    .line 116
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    move-object v1, p10

    .line 117
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->postReceiptOperations:Lcom/squareup/payment/PostReceiptOperations;

    move-object v1, p11

    .line 118
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    move-object v1, p12

    .line 119
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p13

    .line 120
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    move-object/from16 v1, p14

    .line 121
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->synchronousLocalPaymentPresenter:Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;

    move-object/from16 v1, p15

    .line 122
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    move-object/from16 v1, p16

    .line 123
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    move-object/from16 v1, p17

    .line 124
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    move-object/from16 v1, p18

    .line 125
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    move-object/from16 v1, p19

    .line 126
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    move-object/from16 v1, p20

    .line 127
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->printingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    move-object/from16 v1, p21

    .line 128
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    move-object/from16 v1, p22

    .line 129
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

    move-object/from16 v1, p23

    .line 130
    iput-object v1, v0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    return-void
.end method

.method private advanceToBuyerFlow(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 0

    if-eqz p1, :cond_0

    .line 299
    sget-object p1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW_AND_AUTHORIZE:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    :goto_0
    return-object p1
.end method

.method private cancelPaymentShouldOpenCashDrawers()Z
    .locals 5

    .line 395
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getCapturedTenders()Ljava/util/List;

    move-result-object v0

    .line 396
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/tender/BaseTender;

    .line 397
    invoke-virtual {v1}, Lcom/squareup/payment/tender/BaseTender;->getTenderType()Lcom/squareup/protos/client/bills/Tender$Type;

    move-result-object v2

    .line 398
    sget-object v3, Lcom/squareup/protos/client/bills/Tender$Type;->CASH:Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v4, 0x1

    if-ne v2, v3, :cond_1

    return v4

    .line 400
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/bills/Tender$Type;->OTHER:Lcom/squareup/protos/client/bills/Tender$Type;

    if-ne v2, v3, :cond_0

    .line 401
    check-cast v1, Lcom/squareup/payment/tender/OtherTender;

    invoke-virtual {v1}, Lcom/squareup/payment/tender/OtherTender;->getOtherTenderType()Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object v1

    .line 402
    iget-object v1, v1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    return v4

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method private completeFastCheckout(Lcom/squareup/payment/Payment;Lcom/squareup/payment/PaymentReceipt;)V
    .locals 2

    .line 366
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-interface {v0, p2}, Lcom/squareup/receipt/ReceiptSender;->autoPrintValidatedReceipt(Lcom/squareup/payment/PaymentReceipt;)V

    .line 368
    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 370
    iget-object p2, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->printingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    .line 371
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getDisplayNameOrDefault()Ljava/lang/String;

    move-result-object v1

    .line 370
    invoke-virtual {p2, v0, v1}, Lcom/squareup/print/OrderPrintingDispatcher;->printStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    .line 373
    :cond_0
    iget-object p2, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->postReceiptOperations:Lcom/squareup/payment/PostReceiptOperations;

    invoke-virtual {p2}, Lcom/squareup/payment/PostReceiptOperations;->resetTransactionBeforeReceiptScreen()V

    .line 375
    iget-object p2, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {p2}, Lcom/squareup/crm/CustomerManagementSettings;->isAddCustomerToSaleEnabled()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 377
    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->enqueueAttachContactTask()V

    :cond_1
    return-void
.end method

.method private localPaymentFastCheckout(Lcom/squareup/payment/BillPayment;Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 2

    .line 310
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->isLocalPayment()Z

    move-result v0

    const-string v1, "A local payment is required."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    if-eqz p2, :cond_0

    .line 313
    invoke-direct {p0, p2}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->advanceToBuyerFlow(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1

    .line 316
    :cond_0
    iget-object p2, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->prepareChangeHud:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 318
    iget-object p2, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/payment/BillPayment;->isSingleTender()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 319
    invoke-direct {p0}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->singleTenderFastCheckout()Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p2, 0x1

    .line 323
    :try_start_0
    invoke-virtual {p1, p2}, Lcom/squareup/payment/BillPayment;->capture(Z)Z
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    iget-object p2, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->endCurrentTenderAndCreateReceipt()V

    .line 334
    iget-object p2, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->completeFastCheckout(Lcom/squareup/payment/Payment;Lcom/squareup/payment/PaymentReceipt;)V

    .line 336
    sget-object p1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_SPLIT_TENDER:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    return-object p1

    :catch_0
    move-exception p1

    .line 325
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BillPayment#capture may throw on authorizing tenders in foreground. But for a local tender that requires no auth, it won\'t throw.If it really throws here, something must be wrong."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    invoke-virtual {p1}, Ljava/security/InvalidKeyException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method private shouldUseZeroTender(Lcom/squareup/protos/common/Money;)Z
    .locals 2

    .line 382
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->canUseZeroAmountTender()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getRoundedRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    .line 388
    :cond_1
    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-nez p1, :cond_2

    const/4 p1, 0x1

    return p1

    .line 389
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot get change if cart amount is $0"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private singleTenderFastCheckout()Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 5

    .line 340
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getUniqueKey()Ljava/lang/String;

    move-result-object v0

    .line 341
    iget-object v1, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->captureLocalPaymentAndCreateReceipt()V

    .line 342
    iget-object v1, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    .line 343
    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v2

    .line 344
    invoke-virtual {v2}, Lcom/squareup/payment/Payment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 346
    invoke-direct {p0, v2, v1}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->completeFastCheckout(Lcom/squareup/payment/Payment;Lcom/squareup/payment/PaymentReceipt;)V

    .line 348
    iget-object v1, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    invoke-interface {v1, v0}, Lcom/squareup/ui/main/TransactionMetrics;->endTransaction(Ljava/lang/String;)V

    .line 350
    invoke-virtual {v2}, Lcom/squareup/payment/Payment;->getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    move-result-object v0

    .line 351
    iget-object v1, v3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 352
    iget-object v3, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    const/4 v4, 0x1

    invoke-interface {v3, v0, v1, v4}, Lcom/squareup/log/CheckoutInformationEventLogger;->finishCheckout(Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;Ljava/lang/String;Z)V

    .line 354
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    invoke-virtual {v0, v2}, Lcom/squareup/api/ApiTransactionController;->handleTenderFlowCompleted(Lcom/squareup/payment/Payment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    sget-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->DO_NOTHING:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    return-object v0

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isTransactionLockModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 359
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->clearCurrentEmployee()V

    .line 362
    :cond_1
    sget-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->SHOW_POS_APPLET:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    return-object v0
.end method


# virtual methods
.method public cancelPaymentFlow(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    invoke-direct {p0}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->cancelPaymentShouldOpenCashDrawers()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->openAllCashDrawers()V

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

    iget-object v1, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/PaymentAccuracyLogger;->logCancelBillAccuracyEvent(Lcom/squareup/payment/Payment;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->dropPayment(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setCanRenameCart(Z)V

    .line 148
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {v0}, Lcom/squareup/log/CheckoutInformationEventLogger;->cancelCheckout()V

    .line 149
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/NfcProcessor;->cancelPaymentOnAllContactlessReaders()V

    .line 151
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-interface {v0, p1}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 153
    iget-object p1, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    invoke-virtual {p1}, Lcom/squareup/api/ApiTransactionController;->handleTenderFlowCanceled()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 154
    sget-object p1, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ENDED_API_TRANSACTION:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    return-object p1

    .line 157
    :cond_2
    iget-object p1, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 158
    sget-object p1, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ENDED_INVOICE_PAYMENT:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    return-object p1

    .line 161
    :cond_3
    sget-object p1, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ENDED:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    return-object p1
.end method

.method public chargeCardOnFile(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->instrumentOnFileSellerWorkflow:Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;

    invoke-virtual {v0, p1, p0}, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->onConfirmChargeCardOnFile(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;Lcom/squareup/tenderpayment/TenderCompleter;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public completeTenderAndAuthorize(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 5

    .line 166
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setCanRenameCart(Z)V

    .line 168
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    .line 169
    iget-object v2, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    .line 170
    invoke-interface {v2}, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;->skipReceiptScreenForFastCheckout()Z

    move-result v2

    .line 172
    iget-object v3, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v3}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyProgramActive()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    .line 173
    invoke-interface {v3}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyScreensEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 177
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->isLocalPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    if-nez v3, :cond_1

    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    .line 180
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v1, 0x1

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->cancelPaymentsOnNonActiveCardReaders()V

    if-nez v1, :cond_2

    .line 185
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->attributeChargeIfPossible()V

    .line 186
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->advanceToBuyerFlow(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1

    .line 189
    :cond_2
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->localPaymentFastCheckout(Lcom/squareup/payment/BillPayment;Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public onPrepareChangeHud()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/payment/CompletedPayment;",
            ">;"
        }
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->prepareChangeHud:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public payCardTender(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 1

    const/4 v0, 0x0

    .line 271
    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->payCardTender(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public payCardTender(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 1

    .line 276
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/TenderFactory;->createCreditCard()Lcom/squareup/payment/tender/MagStripeTenderBuilder$CreditCardTenderBuilder;

    move-result-object v0

    .line 277
    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->setCard(Lcom/squareup/Card;)Lcom/squareup/payment/tender/MagStripeTenderBuilder;

    .line 278
    invoke-virtual {v0, p2}, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    if-eqz p3, :cond_0

    const/4 p1, 0x0

    .line 281
    invoke-virtual {v0, p3, p1}, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    goto :goto_1

    .line 283
    :cond_0
    iget-object p1, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    .line 284
    invoke-virtual {p1, v0}, Lcom/squareup/payment/TipDeterminerFactory;->createFor(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/TipDeterminer;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/TipDeterminer;->showPreAuthTipScreen()Z

    move-result p1

    const/4 p2, 0x0

    if-eqz p1, :cond_1

    .line 285
    invoke-virtual {v0}, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->askForTip()Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    .line 288
    iget-object p1, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p1, v0}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 289
    invoke-virtual {p0, p2}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->completeTenderAndAuthorize(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1

    .line 293
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/payment/BillPayment;->addTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object p1

    .line 294
    iget-object p2, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/payment/BillPayment;->shouldCallAuthOnTender(Lcom/squareup/payment/tender/BaseTender;)Z

    move-result p1

    .line 295
    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->completeTenderAndAuthorize(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public payCashTender(Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 3

    .line 199
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->openAllCashDrawers()V

    .line 200
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_FLOW_CASH_TENDER_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 202
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requiresSynchronousAuthorization()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->shouldUseZeroTender(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    iget-object p1, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->synchronousLocalPaymentPresenter:Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;->startSynchronousZeroAmountPayment()Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getRoundedRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 208
    iget-object v1, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->synchronousLocalPaymentPresenter:Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;

    invoke-virtual {v1, v0, p1}, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;->startSynchronousCashPayment(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 215
    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireCashTender()Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/CashTender$Builder;->setTendered(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object p1

    .line 216
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearCashTender()Lcom/squareup/payment/tender/CashTender$Builder;

    .line 217
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/BillPayment;->addTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object p1

    .line 218
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/BillPayment;->shouldCallAuthOnTender(Lcom/squareup/payment/tender/BaseTender;)Z

    move-result p1

    goto :goto_1

    .line 220
    :cond_2
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 221
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->shouldUseZeroTender(Lcom/squareup/protos/common/Money;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 222
    iget-object p1, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {p1}, Lcom/squareup/payment/tender/TenderFactory;->createZeroToReplaceCash()Lcom/squareup/payment/tender/ZeroTender$Builder;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/BillPayment;->addLocalTender(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)V

    goto :goto_0

    .line 224
    :cond_3
    iget-object v1, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getRoundedRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 225
    iget-object v2, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v2, v1, p1}, Lcom/squareup/payment/tender/TenderFactory;->createCash(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/BillPayment;->addLocalTender(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)V

    :goto_0
    const/4 p1, 0x0

    .line 229
    :goto_1
    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->completeTenderAndAuthorize(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public payOtherTender(Ljava/lang/String;Lcom/squareup/server/account/protos/OtherTenderType;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 1

    const/4 v0, 0x0

    .line 234
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->payOtherTender(Ljava/lang/String;Lcom/squareup/server/account/protos/OtherTenderType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public payOtherTender(Ljava/lang/String;Lcom/squareup/server/account/protos/OtherTenderType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 2

    .line 240
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_FLOW_OTHER_TENDER_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 242
    iget-object v0, p2, Lcom/squareup/server/account/protos/OtherTenderType;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/squareup/server/account/protos/OtherTenderType;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->openAllCashDrawers()V

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requiresSynchronousAuthorization()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 247
    iget-object p4, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->synchronousLocalPaymentPresenter:Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;

    invoke-virtual {p4, p2, p1, p3}, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;->startSynchronousOtherPayment(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1

    .line 250
    :cond_1
    iget-object v0, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v0, p2, p1, p3}, Lcom/squareup/payment/tender/TenderFactory;->createOther(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/OtherTender$Builder;

    move-result-object p1

    if-eqz p4, :cond_2

    const/4 p3, 0x0

    .line 252
    invoke-virtual {p1, p4, p3}, Lcom/squareup/payment/tender/OtherTender$Builder;->setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    :cond_2
    const/4 p3, 0x0

    .line 256
    iget-object p4, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p4}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result p4

    if-eqz p4, :cond_4

    .line 257
    iget-object p2, p2, Lcom/squareup/server/account/protos/OtherTenderType;->accepts_tips:Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_3

    .line 258
    sget-object p1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->SHOW_THIRD_PARTY:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    return-object p1

    .line 260
    :cond_3
    iget-object p2, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/payment/BillPayment;->addTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object p1

    .line 261
    iget-object p2, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/payment/BillPayment;->shouldCallAuthOnTender(Lcom/squareup/payment/tender/BaseTender;)Z

    move-result p3

    goto :goto_0

    .line 264
    :cond_4
    iget-object p2, p0, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p2

    .line 265
    invoke-virtual {p2, p1}, Lcom/squareup/payment/BillPayment;->addLocalTender(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)V

    .line 267
    :goto_0
    invoke-virtual {p0, p3}, Lcom/squareup/tenderpayment/DefaultTenderCompleter;->completeTenderAndAuthorize(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method
