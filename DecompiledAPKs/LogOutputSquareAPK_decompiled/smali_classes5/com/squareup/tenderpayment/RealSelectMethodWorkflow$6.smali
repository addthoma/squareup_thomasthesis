.class final Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$6;
.super Ljava/lang/Object;
.source "RealSelectMethodWorkflow.kt"

# interfaces
.implements Lcom/squareup/fsm/SideEffect;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;-><init>(Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/util/Device;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/badbus/BadBus;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/tutorialv2/TutorialCore;Lrx/Scheduler;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;Lcom/squareup/tenderpayment/SelectMethodScreensFactory;Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$6;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()V
    .locals 4

    .line 387
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$6;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$getUserToken(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Ljava/lang/String;

    move-result-object v0

    .line 388
    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$6;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {v1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$getShowModalList$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$6;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {v2}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$getShowModalList$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v2

    invoke-interface {v2}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v2

    const-string v3, "showModalList.get()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/util/Set;

    invoke-static {v2, v0}, Lkotlin/collections/SetsKt;->plus(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 389
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$6;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$getAnalytics$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->COLLECT_CASH_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 390
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$6;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$getCurrentScreenKey$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    sget-object v1, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
