.class public final Lcom/squareup/tenderpayment/TenderPaymentConfig;
.super Ljava/lang/Object;
.source "TenderPaymentConfig.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;,
        Lcom/squareup/tenderpayment/TenderPaymentConfig$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u0000 \"2\u00020\u0001:\u0002\"#B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0008\u0010\u0013\u001a\u00020\u0014H\u0016J\u0013\u0010\u0015\u001a\u00020\u00032\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0014H\u00d6\u0001J\u0006\u0010\u0019\u001a\u00020\u001aJ\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001J\u0018\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\u0014H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/TenderPaymentConfig;",
        "Landroid/os/Parcelable;",
        "defaultToBuyerCheckout",
        "",
        "tenderOptions",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;",
        "startAtStep",
        "Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;",
        "(ZLcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;)V",
        "getDefaultToBuyerCheckout",
        "()Z",
        "getStartAtStep",
        "()Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;",
        "getTenderOptions",
        "()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toSnapshot",
        "Lokio/Buffer;",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "Companion",
        "StartAtStep",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/tenderpayment/TenderPaymentConfig;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/tenderpayment/TenderPaymentConfig$Companion;


# instance fields
.field private final defaultToBuyerCheckout:Z

.field private final startAtStep:Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;

.field private final tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentConfig$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/TenderPaymentConfig$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->Companion:Lcom/squareup/tenderpayment/TenderPaymentConfig$Companion;

    .line 59
    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentConfig$Companion$CREATOR$1;

    invoke-direct {v0}, Lcom/squareup/tenderpayment/TenderPaymentConfig$Companion$CREATOR$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ZLcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;)V
    .locals 1

    const-string/jumbo v0, "tenderOptions"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "startAtStep"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->defaultToBuyerCheckout:Z

    iput-object p2, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    iput-object p3, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->startAtStep:Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;

    return-void
.end method

.method public synthetic constructor <init>(ZLcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 25
    sget-object p3, Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;->DEFAULT:Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/tenderpayment/TenderPaymentConfig;-><init>(ZLcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tenderpayment/TenderPaymentConfig;ZLcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;ILjava/lang/Object;)Lcom/squareup/tenderpayment/TenderPaymentConfig;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-boolean p1, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->defaultToBuyerCheckout:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->startAtStep:Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/tenderpayment/TenderPaymentConfig;->copy(ZLcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;)Lcom/squareup/tenderpayment/TenderPaymentConfig;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->defaultToBuyerCheckout:Z

    return v0
.end method

.method public final component2()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    return-object v0
.end method

.method public final component3()Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->startAtStep:Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;

    return-object v0
.end method

.method public final copy(ZLcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;)Lcom/squareup/tenderpayment/TenderPaymentConfig;
    .locals 1

    const-string/jumbo v0, "tenderOptions"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "startAtStep"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentConfig;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/tenderpayment/TenderPaymentConfig;-><init>(ZLcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentConfig;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentConfig;

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->defaultToBuyerCheckout:Z

    iget-boolean v1, p1, Lcom/squareup/tenderpayment/TenderPaymentConfig;->defaultToBuyerCheckout:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    iget-object v1, p1, Lcom/squareup/tenderpayment/TenderPaymentConfig;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->startAtStep:Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;

    iget-object p1, p1, Lcom/squareup/tenderpayment/TenderPaymentConfig;->startAtStep:Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDefaultToBuyerCheckout()Z
    .locals 1

    .line 23
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->defaultToBuyerCheckout:Z

    return v0
.end method

.method public final getStartAtStep()Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->startAtStep:Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;

    return-object v0
.end method

.method public final getTenderOptions()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->defaultToBuyerCheckout:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->startAtStep:Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    return v0
.end method

.method public final toSnapshot()Lokio/Buffer;
    .locals 3

    .line 35
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    .line 37
    move-object v1, v0

    check-cast v1, Lokio/BufferedSink;

    iget-boolean v2, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->defaultToBuyerCheckout:Z

    invoke-static {v1, v2}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 38
    iget-object v2, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->startAtStep:Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;

    check-cast v2, Ljava/lang/Enum;

    invoke-static {v1, v2}, Lcom/squareup/workflow/SnapshotKt;->writeEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    .line 40
    iget-object v2, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    check-cast v2, Landroid/os/Parcelable;

    invoke-static {v2}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object v2

    .line 41
    invoke-virtual {v2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/workflow/SnapshotKt;->writeByteStringWithLength(Lokio/BufferedSink;Lokio/ByteString;)Lokio/BufferedSink;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TenderPaymentConfig(defaultToBuyerCheckout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->defaultToBuyerCheckout:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", tenderOptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->tenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", startAtStep="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderPaymentConfig;->startAtStep:Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/TenderPaymentConfig;->toSnapshot()Lokio/Buffer;

    move-result-object p2

    .line 51
    invoke-virtual {p2}, Lokio/Buffer;->readByteArray()[B

    move-result-object p2

    .line 52
    array-length v0, p2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 53
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method
