.class public Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;
.super Ljava/lang/Object;
.source "SelectMethodWorkflowServices.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/SelectMethodWorkflowServices$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectMethodWorkflowServices.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectMethodWorkflowServices.kt\ncom/squareup/tenderpayment/SelectMethodWorkflowServices\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,162:1\n601#2,11:163\n*E\n*S KotlinDebug\n*F\n+ 1 SelectMethodWorkflowServices.kt\ncom/squareup/tenderpayment/SelectMethodWorkflowServices\n*L\n151#1,11:163\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0098\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0016\u0018\u0000 62\u00020\u0001:\u00016Bm\u0008\u0007\u0012\u0014\u0008\u0001\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u00a2\u0006\u0002\u0010\u001aJ\u001a\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!2\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0002J\u0018\u0010$\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!2\u0008\u0010\"\u001a\u0004\u0018\u00010#J\u000c\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\'0&J\u000e\u0010(\u001a\u00020)2\u0006\u0010\"\u001a\u00020#J\u000e\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-J\u0008\u0010.\u001a\u00020\u0005H\u0002J\u0010\u0010/\u001a\u00020)2\u0008\u0010\"\u001a\u0004\u0018\u00010#J\u0010\u00100\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!H\u0002J\u0018\u00101\u001a\u00020)2\u0006\u0010 \u001a\u0002022\u0008\u0010\"\u001a\u0004\u0018\u00010#J\u0006\u00103\u001a\u00020)J\u0006\u00104\u001a\u000205R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u001b\u001a\u00020\u00058F\u00a2\u0006\u0006\u001a\u0004\u0008\u001c\u0010\u001dR\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00067"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;",
        "",
        "showModalList",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "",
        "changeHudToaster",
        "Lcom/squareup/tenderpayment/ChangeHudToaster;",
        "completer",
        "Lcom/squareup/tenderpayment/TenderCompleter;",
        "tenderFactory",
        "Lcom/squareup/payment/tender/TenderFactory;",
        "tenderInEdit",
        "Lcom/squareup/payment/TenderInEdit;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "x2SellerScreenRunner",
        "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "device",
        "Lcom/squareup/util/Device;",
        "offlineModeMonitor",
        "Lcom/squareup/payment/OfflineModeMonitor;",
        "(Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;Lcom/squareup/payment/OfflineModeMonitor;)V",
        "orderIdentifier",
        "getOrderIdentifier",
        "()Ljava/lang/String;",
        "chargeCardOnFile",
        "Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;",
        "event",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;",
        "splitTenderAmount",
        "Lcom/squareup/protos/common/Money;",
        "completeTender",
        "completedTenders",
        "",
        "Lcom/squareup/payment/tender/BaseTender;",
        "createCardOnFileInstrumentTender",
        "",
        "getOtherTenderType",
        "Lcom/squareup/server/account/protos/OtherTenderType;",
        "tender",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
        "getUserToken",
        "payCashCustom",
        "payCashTender",
        "recordCardPayment",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnRecordPaymentEvent;",
        "setShouldShowAccidentalCashModal",
        "shouldShowAccidentalCashModal",
        "",
        "Companion",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/tenderpayment/SelectMethodWorkflowServices$Companion;

.field private static final NON_LOGGED_IN_EMPLOYEE_TOKEN:Ljava/lang/String; = "nonLoggedInEmployeeToken"


# instance fields
.field private final changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

.field private final completer:Lcom/squareup/tenderpayment/TenderCompleter;

.field private final device:Lcom/squareup/util/Device;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final showModalList:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->Companion:Lcom/squareup/tenderpayment/SelectMethodWorkflowServices$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;Lcom/squareup/payment/OfflineModeMonitor;)V
    .locals 1
    .param p1    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/tenderpayment/CollectCashModalShown;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            "Lcom/squareup/payment/tender/TenderFactory;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "showModalList"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "changeHudToaster"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "completer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderInEdit"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "x2SellerScreenRunner"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "offlineModeMonitor"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->showModalList:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->completer:Lcom/squareup/tenderpayment/TenderCompleter;

    iput-object p4, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    iput-object p5, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    iput-object p6, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p7, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->features:Lcom/squareup/settings/server/Features;

    iput-object p8, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iput-object p9, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p10, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->device:Lcom/squareup/util/Device;

    iput-object p11, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    return-void
.end method

.method private final chargeCardOnFile(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 1

    if-eqz p1, :cond_2

    .line 97
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnChargeCardOnFileEvent;

    .line 99
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnChargeCardOnFileEvent;->getInstrumentToken$tender_payment_release()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->getInstrumentDetails(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    move-result-object p1

    .line 101
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_0

    .line 102
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {p0, p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->createCardOnFileInstrumentTender(Lcom/squareup/protos/common/Money;)V

    .line 103
    iget-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p2}, Lcom/squareup/payment/TenderInEdit;->requireInstrumentTender()Lcom/squareup/payment/tender/InstrumentTender$Builder;

    move-result-object p2

    const-string v0, "tenderInEdit.requireInstrumentTender()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, p1}, Lcom/squareup/payment/tender/InstrumentTender$Builder;->setInstrument(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)V

    .line 107
    :cond_1
    iget-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->completer:Lcom/squareup/tenderpayment/TenderCompleter;

    invoke-interface {p2, p1}, Lcom/squareup/tenderpayment/TenderCompleter;->chargeCardOnFile(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    const-string p2, "completer.chargeCardOnFile(selectedInstrument)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 97
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.tenderpayment.SelectMethodWorkflowEvent.OnTenderReceivedEvent.OnChargeCardOnFileEvent"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final getUserToken()Ljava/lang/String;
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "nonLoggedInEmployeeToken"

    :goto_0
    return-object v0
.end method

.method private final payCashTender(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->completer:Lcom/squareup/tenderpayment/TenderCompleter;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/tenderpayment/TenderCompleter;->payCashTender(Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    const-string v0, "completer.payCashTender(event.tenderedAmount)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final completeTender(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 2

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

    invoke-interface {v0}, Lcom/squareup/tenderpayment/ChangeHudToaster;->toastIfAvailable()V

    .line 86
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;->getTender()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 p2, 0x2

    if-eq v0, p2, :cond_1

    .line 89
    :goto_0
    sget-object p1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->DO_NOTHING:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    goto :goto_1

    .line 88
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->payCashTender(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    goto :goto_1

    .line 87
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->chargeCardOnFile(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public final completedTenders()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    .line 138
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    const-string/jumbo v1, "transaction.requireBillPayment()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getCapturedTenders()Ljava/util/List;

    move-result-object v0

    const-string/jumbo v1, "transaction.requireBillP\u2026\n        .capturedTenders"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 142
    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public final createCardOnFileInstrumentTender(Lcom/squareup/protos/common/Money;)V
    .locals 2

    const-string v0, "splitTenderAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearBaseTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/TenderFactory;->createInstrument()Lcom/squareup/payment/tender/InstrumentTender$Builder;

    move-result-object v0

    const-string v1, "tender"

    .line 120
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/InstrumentTender$Builder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 121
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    check-cast v0, Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-interface {p1, v0}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 123
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 124
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/payment/TenderInEdit;->setContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    :cond_1
    return-void
.end method

.method public final getOrderIdentifier()Ljava/lang/String;
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->orderIdentifier()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "transaction.order.orderIdentifier()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getOtherTenderType(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;)Lcom/squareup/server/account/protos/OtherTenderType;
    .locals 9

    const-string v0, "tender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager;->LEGACY_OTHER:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Cannot return OtherTenderType for "

    if-eqz v0, :cond_6

    .line 150
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    const-string v4, "settings.paymentSettings"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getOtherTenderOptions()Ljava/util/List;

    move-result-object v0

    const-string v4, "settings.paymentSettings.otherTenderOptions"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 165
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v4, 0x0

    move-object v5, v4

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 166
    move-object v7, v6

    check-cast v7, Lcom/squareup/server/account/protos/OtherTenderType;

    .line 151
    iget-object v7, v7, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    iget-object v8, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz v3, :cond_2

    goto :goto_2

    :cond_2
    move-object v5, v6

    const/4 v3, 0x1

    goto :goto_1

    :cond_3
    if-nez v3, :cond_4

    goto :goto_2

    :cond_4
    move-object v4, v5

    .line 173
    :goto_2
    check-cast v4, Lcom/squareup/server/account/protos/OtherTenderType;

    if-eqz v4, :cond_5

    return-object v4

    .line 152
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 147
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 146
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final payCashCustom(Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 53
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    :cond_0
    if-nez p1, :cond_1

    .line 56
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-static {p1}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 57
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v1, p1, p1}, Lcom/squareup/payment/tender/TenderFactory;->createCash(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-interface {v0, p1}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    :cond_2
    return-void
.end method

.method public final recordCardPayment(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnRecordPaymentEvent;Lcom/squareup/protos/common/Money;)V
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    .line 72
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnRecordPaymentEvent;->getOtherTenderType()Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object p1

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_0

    .line 74
    :cond_1
    iget-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p2

    const-string/jumbo v2, "transaction.requireBillPayment()"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 71
    :goto_0
    invoke-virtual {v0, p1, v1, p2}, Lcom/squareup/payment/tender/TenderFactory;->createOther(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/OtherTender$Builder;

    move-result-object p1

    .line 76
    iget-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    check-cast p1, Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-interface {p2, p1}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    :cond_2
    return-void
.end method

.method public final setShouldShowAccidentalCashModal()V
    .locals 4

    .line 129
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->getUserToken()Ljava/lang/String;

    move-result-object v0

    .line 130
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->showModalList:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v1}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v2

    const-string v3, "showModalList.get()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/util/Set;

    invoke-static {v2, v0}, Lkotlin/collections/SetsKt;->plus(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public final shouldShowAccidentalCashModal()Z
    .locals 3

    .line 134
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->getUserToken()Ljava/lang/String;

    move-result-object v0

    .line 135
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->SHOW_ACCIDENTAL_CASH_MODAL:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowServices;->showModalList:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v1}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
