.class public final enum Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;
.super Ljava/lang/Enum;
.source "TenderSettingsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/TenderSettingsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TenderAvailability"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

.field public static final enum ALWAYS:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

.field public static final enum OFFLINE:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

.field public static final enum ONLINE_ONLY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 483
    new-instance v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    const/4 v1, 0x0

    const-string v2, "ALWAYS"

    invoke-direct {v0, v2, v1}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->ALWAYS:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    new-instance v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    const/4 v2, 0x1

    const-string v3, "OFFLINE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->OFFLINE:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    new-instance v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    const/4 v3, 0x2

    const-string v4, "ONLINE_ONLY"

    invoke-direct {v0, v4, v3}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->ONLINE_ONLY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    .line 482
    sget-object v4, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->ALWAYS:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->OFFLINE:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->ONLINE_ONLY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->$VALUES:[Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 482
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;
    .locals 1

    .line 482
    const-class v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    return-object p0
.end method

.method public static values()[Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;
    .locals 1

    .line 482
    sget-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->$VALUES:[Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    invoke-virtual {v0}, [Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    return-object v0
.end method
