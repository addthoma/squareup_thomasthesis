.class public final Lcom/squareup/splitticket/TicketSplitterModule_TicketSplitterBudleKeyModule_ProvideFosterStateKeyFactory;
.super Ljava/lang/Object;
.source "TicketSplitterModule_TicketSplitterBudleKeyModule_ProvideFosterStateKeyFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/BundleKey<",
        "Lcom/squareup/splitticket/FosterState;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule;


# direct methods
.method public constructor <init>(Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/splitticket/TicketSplitterModule_TicketSplitterBudleKeyModule_ProvideFosterStateKeyFactory;->module:Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule;

    .line 26
    iput-object p2, p0, Lcom/squareup/splitticket/TicketSplitterModule_TicketSplitterBudleKeyModule_ProvideFosterStateKeyFactory;->gsonProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule;Ljavax/inject/Provider;)Lcom/squareup/splitticket/TicketSplitterModule_TicketSplitterBudleKeyModule_ProvideFosterStateKeyFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)",
            "Lcom/squareup/splitticket/TicketSplitterModule_TicketSplitterBudleKeyModule_ProvideFosterStateKeyFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/splitticket/TicketSplitterModule_TicketSplitterBudleKeyModule_ProvideFosterStateKeyFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/splitticket/TicketSplitterModule_TicketSplitterBudleKeyModule_ProvideFosterStateKeyFactory;-><init>(Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideFosterStateKey(Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule;Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/splitticket/FosterState;",
            ">;"
        }
    .end annotation

    .line 41
    invoke-virtual {p0, p1}, Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule;->provideFosterStateKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/BundleKey;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/BundleKey;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/splitticket/FosterState;",
            ">;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/splitticket/TicketSplitterModule_TicketSplitterBudleKeyModule_ProvideFosterStateKeyFactory;->module:Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule;

    iget-object v1, p0, Lcom/squareup/splitticket/TicketSplitterModule_TicketSplitterBudleKeyModule_ProvideFosterStateKeyFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/gson/Gson;

    invoke-static {v0, v1}, Lcom/squareup/splitticket/TicketSplitterModule_TicketSplitterBudleKeyModule_ProvideFosterStateKeyFactory;->provideFosterStateKey(Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule;Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/splitticket/TicketSplitterModule_TicketSplitterBudleKeyModule_ProvideFosterStateKeyFactory;->get()Lcom/squareup/BundleKey;

    move-result-object v0

    return-object v0
.end method
