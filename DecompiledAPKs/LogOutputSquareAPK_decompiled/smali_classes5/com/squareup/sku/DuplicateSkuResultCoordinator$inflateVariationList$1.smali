.class final Lcom/squareup/sku/DuplicateSkuResultCoordinator$inflateVariationList$1;
.super Ljava/lang/Object;
.source "DuplicateSkuResultCoordinator.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/sku/DuplicateSkuResultCoordinator;->inflateVariationList(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $variation:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

.field final synthetic this$0:Lcom/squareup/sku/DuplicateSkuResultCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/sku/DuplicateSkuResultCoordinator;Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator$inflateVariationList$1;->this$0:Lcom/squareup/sku/DuplicateSkuResultCoordinator;

    iput-object p2, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator$inflateVariationList$1;->$variation:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .line 89
    iget-object p1, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator$inflateVariationList$1;->this$0:Lcom/squareup/sku/DuplicateSkuResultCoordinator;

    invoke-virtual {p1}, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->getController()Lcom/squareup/sku/DuplicateSkuResultController;

    move-result-object p1

    .line 90
    iget-object v0, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator$inflateVariationList$1;->$variation:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    iget-object v0, v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemId:Ljava/lang/String;

    const-string/jumbo v1, "variation.itemId"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator$inflateVariationList$1;->$variation:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    iget-object v1, v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->variationId:Ljava/lang/String;

    const-string/jumbo v2, "variation.variationId"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator$inflateVariationList$1;->$variation:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    iget-object v2, v2, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->type:Lcom/squareup/api/items/Item$Type;

    const-string/jumbo v3, "variation.type"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-interface {p1, v0, v1, v2}, Lcom/squareup/sku/DuplicateSkuResultController;->onVariationSelectedOnDuplicateSkuResultScreen(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;)V

    return-void
.end method
