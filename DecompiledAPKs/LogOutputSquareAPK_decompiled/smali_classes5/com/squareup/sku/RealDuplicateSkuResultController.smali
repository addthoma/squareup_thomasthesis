.class public final Lcom/squareup/sku/RealDuplicateSkuResultController;
.super Ljava/lang/Object;
.source "RealDuplicateSkuResultController.kt"

# interfaces
.implements Lcom/squareup/sku/DuplicateSkuResultController;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sku/RealDuplicateSkuResultController$State;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u0001\u001bB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\n\u001a\u00020\u000bH\u0002J\u0008\u0010\u000c\u001a\u00020\u000bH\u0016J \u0010\r\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0016\u0010\u0013\u001a\u00020\u000b2\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015H\u0016J\u0014\u0010\u0017\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001a0\u00190\u0018H\u0016J\u0014\u0010\u0014\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00150\u0018H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/sku/RealDuplicateSkuResultController;",
        "Lcom/squareup/sku/DuplicateSkuResultController;",
        "flow",
        "Lflow/Flow;",
        "(Lflow/Flow;)V",
        "getFlow",
        "()Lflow/Flow;",
        "state",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/sku/RealDuplicateSkuResultController$State;",
        "cleanUp",
        "",
        "onBackPressedOnDuplicateSkuResultScreen",
        "onVariationSelectedOnDuplicateSkuResultScreen",
        "itemId",
        "",
        "variationId",
        "type",
        "Lcom/squareup/api/items/Item$Type;",
        "showDuplicateSkuResult",
        "variationsWithDuplicateSku",
        "",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
        "variationSelection",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/sku/DuplicateSkuResultController$VariationSelection;",
        "State",
        "sku_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final flow:Lflow/Flow;

.field private final state:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/sku/RealDuplicateSkuResultController$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lflow/Flow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/sku/RealDuplicateSkuResultController;->flow:Lflow/Flow;

    .line 51
    sget-object p1, Lcom/squareup/sku/RealDuplicateSkuResultController$State$NotStarted;->INSTANCE:Lcom/squareup/sku/RealDuplicateSkuResultController$State$NotStarted;

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string v0, "BehaviorRelay.createDefault(NotStarted)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/sku/RealDuplicateSkuResultController;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method private final cleanUp()V
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/sku/RealDuplicateSkuResultController;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/sku/RealDuplicateSkuResultController$State$NotStarted;->INSTANCE:Lcom/squareup/sku/RealDuplicateSkuResultController$State$NotStarted;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getFlow()Lflow/Flow;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/sku/RealDuplicateSkuResultController;->flow:Lflow/Flow;

    return-object v0
.end method

.method public onBackPressedOnDuplicateSkuResultScreen()V
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/sku/RealDuplicateSkuResultController;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    .line 70
    iget-object v0, p0, Lcom/squareup/sku/RealDuplicateSkuResultController;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/sku/RealDuplicateSkuResultController$State$FinishedWithoutSelection;->INSTANCE:Lcom/squareup/sku/RealDuplicateSkuResultController$State$FinishedWithoutSelection;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 71
    invoke-direct {p0}, Lcom/squareup/sku/RealDuplicateSkuResultController;->cleanUp()V

    return-void
.end method

.method public onVariationSelectedOnDuplicateSkuResultScreen(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;)V
    .locals 2

    const-string v0, "itemId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "variationId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/sku/RealDuplicateSkuResultController;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    .line 64
    iget-object v0, p0, Lcom/squareup/sku/RealDuplicateSkuResultController;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/sku/RealDuplicateSkuResultController$State$VariationSelectedAndThenFinish;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/sku/RealDuplicateSkuResultController$State$VariationSelectedAndThenFinish;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 65
    invoke-direct {p0}, Lcom/squareup/sku/RealDuplicateSkuResultController;->cleanUp()V

    return-void
.end method

.method public showDuplicateSkuResult(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "variationsWithDuplicateSku"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/sku/RealDuplicateSkuResultController;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/sku/RealDuplicateSkuResultController$State$Started;

    invoke-direct {v1, p1}, Lcom/squareup/sku/RealDuplicateSkuResultController$State$Started;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 55
    iget-object p1, p0, Lcom/squareup/sku/RealDuplicateSkuResultController;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/sku/DuplicateSkuResultScreen;->INSTANCE:Lcom/squareup/sku/DuplicateSkuResultScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public variationSelection()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/sku/DuplicateSkuResultController$VariationSelection;",
            ">;>;"
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/squareup/sku/RealDuplicateSkuResultController;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 76
    sget-object v1, Lcom/squareup/sku/RealDuplicateSkuResultController$variationSelection$1;->INSTANCE:Lcom/squareup/sku/RealDuplicateSkuResultController$variationSelection$1;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 77
    sget-object v1, Lcom/squareup/sku/RealDuplicateSkuResultController$variationSelection$2;->INSTANCE:Lcom/squareup/sku/RealDuplicateSkuResultController$variationSelection$2;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "state\n        .filter { \u2026t\")\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public variationsWithDuplicateSku()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;>;"
        }
    .end annotation

    .line 89
    iget-object v0, p0, Lcom/squareup/sku/RealDuplicateSkuResultController;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 90
    sget-object v1, Lcom/squareup/sku/RealDuplicateSkuResultController$variationsWithDuplicateSku$1;->INSTANCE:Lcom/squareup/sku/RealDuplicateSkuResultController$variationsWithDuplicateSku$1;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 91
    sget-object v1, Lcom/squareup/sku/RealDuplicateSkuResultController$variationsWithDuplicateSku$2;->INSTANCE:Lcom/squareup/sku/RealDuplicateSkuResultController$variationsWithDuplicateSku$2;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "state\n        .filter { \u2026())\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
