.class public interface abstract Lcom/squareup/services/payment/ConnectV2PaymentService;
.super Ljava/lang/Object;
.source "ConnectV2PaymentService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/services/payment/ConnectV2PaymentService$CreatePaymentStandardResponse;,
        Lcom/squareup/services/payment/ConnectV2PaymentService$CompletePaymentStandardResponse;,
        Lcom/squareup/services/payment/ConnectV2PaymentService$EmvDataStandardResponse;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001:\u0003\u000f\u0010\u0011J\u001c\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007H\'J\u0012\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0001\u0010\n\u001a\u00020\u000bH\'J\u001c\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0001\u0010\n\u001a\u00020\u000eH\'\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/services/payment/ConnectV2PaymentService;",
        "",
        "completePayment",
        "Lcom/squareup/services/payment/ConnectV2PaymentService$CompletePaymentStandardResponse;",
        "payment_id",
        "",
        "message",
        "Lcom/squareup/protos/connect/v2/CompletePaymentRequest;",
        "createPayment",
        "Lcom/squareup/services/payment/ConnectV2PaymentService$CreatePaymentStandardResponse;",
        "request",
        "Lcom/squareup/protos/connect/v2/CreatePaymentRequest;",
        "setEmvData",
        "Lcom/squareup/services/payment/ConnectV2PaymentService$EmvDataStandardResponse;",
        "Lcom/squareup/protos/connect/v2/SetEmvDataRequest;",
        "CompletePaymentStandardResponse",
        "CreatePaymentStandardResponse",
        "EmvDataStandardResponse",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract completePayment(Ljava/lang/String;Lcom/squareup/protos/connect/v2/CompletePaymentRequest;)Lcom/squareup/services/payment/ConnectV2PaymentService$CompletePaymentStandardResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Path;
            value = "payment_id"
        .end annotation
    .end param
    .param p2    # Lcom/squareup/protos/connect/v2/CompletePaymentRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/v2/payments/{payment_id}/complete"
    .end annotation
.end method

.method public abstract createPayment(Lcom/squareup/protos/connect/v2/CreatePaymentRequest;)Lcom/squareup/services/payment/ConnectV2PaymentService$CreatePaymentStandardResponse;
    .param p1    # Lcom/squareup/protos/connect/v2/CreatePaymentRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/v2/payments"
    .end annotation
.end method

.method public abstract setEmvData(Ljava/lang/String;Lcom/squareup/protos/connect/v2/SetEmvDataRequest;)Lcom/squareup/services/payment/ConnectV2PaymentService$EmvDataStandardResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Path;
            value = "payment_id"
        .end annotation
    .end param
    .param p2    # Lcom/squareup/protos/connect/v2/SetEmvDataRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/v2/payments/{payment_id}/emv_data"
    .end annotation
.end method
