.class public final Lcom/squareup/skipreceiptscreen/RealSkipReceiptScreenSettings;
.super Ljava/lang/Object;
.source "RealSkipReceiptScreenSettings.kt"

# interfaces
.implements Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0006H\u0016J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/skipreceiptscreen/RealSkipReceiptScreenSettings;",
        "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
        "skipReceiptScreenDeviceOrLocalSetting",
        "Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;",
        "(Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;)V",
        "canSkipReceiptScreen",
        "",
        "skipReceiptScreenForFastCheckout",
        "updateSkipReceiptScreenSettings",
        "",
        "isChecked",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final skipReceiptScreenDeviceOrLocalSetting:Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;


# direct methods
.method public constructor <init>(Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "skipReceiptScreenDeviceOrLocalSetting"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/skipreceiptscreen/RealSkipReceiptScreenSettings;->skipReceiptScreenDeviceOrLocalSetting:Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;

    return-void
.end method


# virtual methods
.method public canSkipReceiptScreen()Z
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/skipreceiptscreen/RealSkipReceiptScreenSettings;->skipReceiptScreenDeviceOrLocalSetting:Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;

    invoke-virtual {v0}, Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;->isAllowed()Z

    move-result v0

    return v0
.end method

.method public skipReceiptScreenForFastCheckout()Z
    .locals 2

    .line 13
    iget-object v0, p0, Lcom/squareup/skipreceiptscreen/RealSkipReceiptScreenSettings;->skipReceiptScreenDeviceOrLocalSetting:Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;

    invoke-virtual {v0}, Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    const-string v1, "skipReceiptScreenDeviceOrLocalSetting.value"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public updateSkipReceiptScreenSettings(Z)V
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/skipreceiptscreen/RealSkipReceiptScreenSettings;->skipReceiptScreenDeviceOrLocalSetting:Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/skipreceiptscreen/SkipReceiptScreenDeviceOrLocalSetting;->setValueLocally(Ljava/lang/Object;)V

    return-void
.end method
