.class public final synthetic Lcom/squareup/tickets/-$$Lambda$RealTickets$vsvQBgS-ys6x8zM6GEZznwJFdjY;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/tickets/RegisterTicketTask;


# instance fields
.field private final synthetic f$0:Lcom/squareup/tickets/RealTickets;

.field private final synthetic f$1:Ljava/lang/String;

.field private final synthetic f$2:Lcom/squareup/tickets/TicketSort;

.field private final synthetic f$3:Ljava/lang/String;

.field private final synthetic f$4:Ljava/lang/String;

.field private final synthetic f$5:Lcom/squareup/tickets/TicketStore$EmployeeAccess;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/tickets/RealTickets;Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$vsvQBgS-ys6x8zM6GEZznwJFdjY;->f$0:Lcom/squareup/tickets/RealTickets;

    iput-object p2, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$vsvQBgS-ys6x8zM6GEZznwJFdjY;->f$1:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$vsvQBgS-ys6x8zM6GEZznwJFdjY;->f$2:Lcom/squareup/tickets/TicketSort;

    iput-object p4, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$vsvQBgS-ys6x8zM6GEZznwJFdjY;->f$3:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$vsvQBgS-ys6x8zM6GEZznwJFdjY;->f$4:Ljava/lang/String;

    iput-object p6, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$vsvQBgS-ys6x8zM6GEZznwJFdjY;->f$5:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    return-void
.end method


# virtual methods
.method public bridge synthetic perform(Lcom/squareup/tickets/TicketDatabase;)Ljava/lang/Object;
    .locals 0

    invoke-static {p0, p1}, Lcom/squareup/tickets/RegisterTicketTask$-CC;->$default$perform(Lcom/squareup/tickets/RegisterTicketTask;Lcom/squareup/tickets/TicketDatabase;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final perform(Lcom/squareup/tickets/TicketStore;)Ljava/lang/Object;
    .locals 7

    iget-object v0, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$vsvQBgS-ys6x8zM6GEZznwJFdjY;->f$0:Lcom/squareup/tickets/RealTickets;

    iget-object v1, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$vsvQBgS-ys6x8zM6GEZznwJFdjY;->f$1:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$vsvQBgS-ys6x8zM6GEZznwJFdjY;->f$2:Lcom/squareup/tickets/TicketSort;

    iget-object v3, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$vsvQBgS-ys6x8zM6GEZznwJFdjY;->f$3:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$vsvQBgS-ys6x8zM6GEZznwJFdjY;->f$4:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$vsvQBgS-ys6x8zM6GEZznwJFdjY;->f$5:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/tickets/RealTickets;->lambda$getGroupTicketList$3$RealTickets(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object p1

    return-object p1
.end method
