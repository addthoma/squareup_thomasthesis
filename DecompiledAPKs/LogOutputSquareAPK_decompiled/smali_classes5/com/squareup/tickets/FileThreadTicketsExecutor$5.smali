.class final Lcom/squareup/tickets/FileThreadTicketsExecutor$5;
.super Ljava/lang/Object;
.source "FileThreadTicketsExecutor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tickets/FileThreadTicketsExecutor;->fail(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/TicketsCallback;Ljava/lang/Throwable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$callback:Lcom/squareup/tickets/TicketsCallback;

.field final synthetic val$t:Ljava/lang/Throwable;


# direct methods
.method constructor <init>(Lcom/squareup/tickets/TicketsCallback;Ljava/lang/Throwable;)V
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$5;->val$callback:Lcom/squareup/tickets/TicketsCallback;

    iput-object p2, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$5;->val$t:Ljava/lang/Throwable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 85
    iget-object v0, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$5;->val$callback:Lcom/squareup/tickets/TicketsCallback;

    new-instance v1, Lcom/squareup/tickets/FileThreadTicketsExecutor$Failure;

    iget-object v2, p0, Lcom/squareup/tickets/FileThreadTicketsExecutor$5;->val$t:Ljava/lang/Throwable;

    invoke-direct {v1, v2}, Lcom/squareup/tickets/FileThreadTicketsExecutor$Failure;-><init>(Ljava/lang/Throwable;)V

    invoke-interface {v0, v1}, Lcom/squareup/tickets/TicketsCallback;->call(Lcom/squareup/tickets/TicketsResult;)V

    return-void
.end method
