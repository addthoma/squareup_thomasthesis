.class public Lcom/squareup/tickets/LocalTicketUpdateEvent;
.super Ljava/lang/Object;
.source "LocalTicketUpdateEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;
    }
.end annotation


# instance fields
.field private final currentTicket:Lcom/squareup/tickets/OpenTicket;

.field private final eventType:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

.field private final previousTicket:Lcom/squareup/tickets/OpenTicket;


# direct methods
.method private constructor <init>(Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p1, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->eventType:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    .line 86
    iput-object p2, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->previousTicket:Lcom/squareup/tickets/OpenTicket;

    .line 87
    iput-object p3, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    return-void
.end method

.method public static forCreate(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;
    .locals 3

    const-string v0, "Created ticket not present but required by CREATED event."

    .line 52
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    new-instance v0, Lcom/squareup/tickets/LocalTicketUpdateEvent;

    sget-object v1, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->CREATED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/tickets/LocalTicketUpdateEvent;-><init>(Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)V

    return-object v0
.end method

.method public static forDelete(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;
    .locals 3

    const-string v0, "Local ticket not present but required by DELETED event."

    .line 69
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 70
    new-instance v0, Lcom/squareup/tickets/LocalTicketUpdateEvent;

    sget-object v1, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->DELETED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/squareup/tickets/LocalTicketUpdateEvent;-><init>(Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)V

    return-object v0
.end method

.method public static forTransfer(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;
    .locals 2

    const-string v0, "Local ticket not present but required by UPDATED event."

    .line 63
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "Updated ticket not present but required by UPDATED event."

    .line 64
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 65
    new-instance v0, Lcom/squareup/tickets/LocalTicketUpdateEvent;

    sget-object v1, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->TRANSFERRED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    invoke-direct {v0, v1, p0, p1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;-><init>(Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)V

    return-object v0
.end method

.method public static forUnlock(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;
    .locals 2

    const-string v0, "Local ticket not present but required by UNLOCKED event."

    .line 75
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 76
    new-instance v0, Lcom/squareup/tickets/LocalTicketUpdateEvent;

    sget-object v1, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->UNLOCKED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    invoke-direct {v0, v1, p0, p1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;-><init>(Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)V

    return-object v0
.end method

.method public static forUpdate(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/LocalTicketUpdateEvent;
    .locals 2

    const-string v0, "Local ticket not present but required by UPDATED event."

    .line 57
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "Updated ticket not present but required by UPDATED event."

    .line 58
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 59
    new-instance v0, Lcom/squareup/tickets/LocalTicketUpdateEvent;

    sget-object v1, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->UPDATED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    invoke-direct {v0, v1, p0, p1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;-><init>(Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/tickets/OpenTicket;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_7

    .line 158
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_2

    .line 160
    :cond_1
    check-cast p1, Lcom/squareup/tickets/LocalTicketUpdateEvent;

    .line 162
    iget-object v2, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->eventType:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    iget-object v3, p1, Lcom/squareup/tickets/LocalTicketUpdateEvent;->eventType:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    if-eq v2, v3, :cond_2

    return v1

    .line 163
    :cond_2
    iget-object v2, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->previousTicket:Lcom/squareup/tickets/OpenTicket;

    if-eqz v2, :cond_3

    iget-object v3, p1, Lcom/squareup/tickets/LocalTicketUpdateEvent;->previousTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-virtual {v2, v3}, Lcom/squareup/tickets/OpenTicket;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    goto :goto_0

    :cond_3
    iget-object v2, p1, Lcom/squareup/tickets/LocalTicketUpdateEvent;->previousTicket:Lcom/squareup/tickets/OpenTicket;

    if-eqz v2, :cond_4

    :goto_0
    return v1

    .line 167
    :cond_4
    iget-object v2, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    iget-object p1, p1, Lcom/squareup/tickets/LocalTicketUpdateEvent;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    if-eqz v2, :cond_5

    invoke-virtual {v2, p1}, Lcom/squareup/tickets/OpenTicket;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_6

    goto :goto_1

    :cond_5
    if-eqz p1, :cond_6

    :goto_1
    return v1

    :cond_6
    return v0

    :cond_7
    :goto_2
    return v1
.end method

.method public getCurrentGroupId()Ljava/lang/String;
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->getGroupId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getPreviousGroupId()Ljava/lang/String;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->previousTicket:Lcom/squareup/tickets/OpenTicket;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->getGroupId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .line 176
    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->eventType:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    invoke-virtual {v0}, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 177
    iget-object v1, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->previousTicket:Lcom/squareup/tickets/OpenTicket;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/tickets/OpenTicket;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 178
    iget-object v1, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/tickets/OpenTicket;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    return v0
.end method

.method public isClosedByUpdate()Z
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->eventType:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    sget-object v1, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->UPDATED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->previousTicket:Lcom/squareup/tickets/OpenTicket;

    .line 97
    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    .line 98
    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isConvertedToCustomTicketByUpdate()Z
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->eventType:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    sget-object v1, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->UPDATED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->previousTicket:Lcom/squareup/tickets/OpenTicket;

    .line 103
    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->wasPredefinedTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    .line 104
    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->isPredefinedTicket()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isDeletedByUpdate()Z
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->eventType:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    sget-object v1, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->DELETED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isGroupChangedByUpdate()Z
    .locals 4

    .line 108
    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->eventType:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    sget-object v1, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->UPDATED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->previousTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_3

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->previousTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 116
    iget-object v0, v0, Lcom/squareup/api/items/TicketGroup;->id:Ljava/lang/String;

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 118
    :goto_1
    iget-object v3, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-virtual {v3}, Lcom/squareup/tickets/OpenTicket;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 119
    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    goto :goto_2

    :cond_3
    move-object v3, v1

    :goto_2
    if-eqz v3, :cond_4

    .line 122
    iget-object v1, v3, Lcom/squareup/api/items/TicketGroup;->id:Ljava/lang/String;

    :cond_4
    const/4 v3, 0x1

    if-eqz v0, :cond_5

    .line 125
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v3

    return v0

    :cond_5
    if-eqz v1, :cond_6

    const/4 v2, 0x1

    :cond_6
    :goto_3
    return v2
.end method

.method public isOpenedByUpdate()Z
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->eventType:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    sget-object v1, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->CREATED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    .line 92
    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isPredefinedTicket()Z
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->currentTicket:Lcom/squareup/tickets/OpenTicket;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->isPredefinedTicket()Z

    move-result v0

    :goto_0
    return v0
.end method

.method public isUnlockedByUpdate()Z
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->eventType:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    sget-object v1, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->UNLOCKED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public wasPredefinedTicket()Z
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->previousTicket:Lcom/squareup/tickets/OpenTicket;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->isPredefinedTicket()Z

    move-result v0

    :goto_0
    return v0
.end method

.method public wasTransferred()Z
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/tickets/LocalTicketUpdateEvent;->eventType:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    sget-object v1, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->TRANSFERRED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
