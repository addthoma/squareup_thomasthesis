.class public final Lcom/squareup/tickets/NoOpenTicketsSettings;
.super Ljava/lang/Object;
.source "NoOpenTicketsSettings.kt"

# interfaces
.implements Lcom/squareup/tickets/OpenTicketsSettings;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0015\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u000b\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u001eH\u0016J\u0010\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\u0004H\u0016J\u0010\u0010\"\u001a\u00020 2\u0006\u0010#\u001a\u00020\u0008H\u0016J\u0010\u0010$\u001a\u00020 2\u0006\u0010!\u001a\u00020\u0004H\u0016J\u0010\u0010%\u001a\u00020 2\u0006\u0010&\u001a\u00020\u0004H\u0016J\u0010\u0010\'\u001a\u00020 2\u0006\u0010(\u001a\u00020\u0008H\u0016J\u0010\u0010)\u001a\u00020 2\u0006\u0010(\u001a\u00020\u0008H\u0016J\u0008\u0010*\u001a\u00020\u0008H\u0016R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\tR\u0014\u0010\n\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\tR\u0014\u0010\u000b\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\tR\u0014\u0010\u000c\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\tR\u0014\u0010\r\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\tR\u0014\u0010\u000e\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\tR\u0014\u0010\u000f\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\tR\u0014\u0010\u0010\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\tR\u0014\u0010\u0011\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\tR\u0014\u0010\u0012\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\tR\u0014\u0010\u0013\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\tR\u0014\u0010\u0014\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\tR\u0014\u0010\u0015\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\tR\u0014\u0010\u0016\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\tR\u0014\u0010\u0017\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\tR\u0014\u0010\u0018\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\tR\u0014\u0010\u0019\u001a\u00020\u0004X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0006R\u0014\u0010\u001b\u001a\u00020\u0004X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u0006\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/tickets/NoOpenTicketsSettings;",
        "Lcom/squareup/tickets/OpenTicketsSettings;",
        "()V",
        "defaultTicketTemplateCount",
        "",
        "getDefaultTicketTemplateCount",
        "()I",
        "isBulkDeleteAllowed",
        "",
        "()Z",
        "isBulkVoidAllowed",
        "isDipTapToCreateTicketAllowed",
        "isMergeTicketsAllowed",
        "isMoveTicketsAllowed",
        "isOpenTicketsAllowed",
        "isOpenTicketsAsHomeScreenEnabled",
        "isOpenTicketsEmployeeFilteringEnabled",
        "isOpenTicketsEnabled",
        "isPredefinedTicketsAllowed",
        "isPredefinedTicketsEnabled",
        "isSavedCartsMode",
        "isSplitTicketsAllowed",
        "isSwipeToCreateTicketAllowed",
        "isTicketTransferAllowed",
        "isUsingDeviceProfiles",
        "maxTicketGroupCount",
        "getMaxTicketGroupCount",
        "maxTicketTemplatesPerGroup",
        "getMaxTicketTemplatesPerGroup",
        "onPredefinedTicketsEnabled",
        "Lrx/Observable;",
        "setDefaultTicketTemplateCount",
        "",
        "count",
        "setEnabled",
        "openTicketsEnabled",
        "setMaxTicketGroupCount",
        "setMaxTicketTemplatesPerGroup",
        "size",
        "setOpenTicketsAsHomeScreenEnabled",
        "enabled",
        "setPredefinedTicketsEnabled",
        "shouldShowOptInBetaWarning",
        "impl-noop_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/tickets/NoOpenTicketsSettings;

.field private static final defaultTicketTemplateCount:I

.field private static final isBulkDeleteAllowed:Z

.field private static final isBulkVoidAllowed:Z

.field private static final isDipTapToCreateTicketAllowed:Z

.field private static final isMergeTicketsAllowed:Z

.field private static final isMoveTicketsAllowed:Z

.field private static final isOpenTicketsAllowed:Z

.field private static final isOpenTicketsAsHomeScreenEnabled:Z

.field private static final isOpenTicketsEmployeeFilteringEnabled:Z

.field private static final isOpenTicketsEnabled:Z

.field private static final isPredefinedTicketsAllowed:Z

.field private static final isPredefinedTicketsEnabled:Z

.field private static final isSavedCartsMode:Z

.field private static final isSplitTicketsAllowed:Z

.field private static final isSwipeToCreateTicketAllowed:Z

.field private static final isTicketTransferAllowed:Z

.field private static final isUsingDeviceProfiles:Z

.field private static final maxTicketGroupCount:I

.field private static final maxTicketTemplatesPerGroup:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5
    new-instance v0, Lcom/squareup/tickets/NoOpenTicketsSettings;

    invoke-direct {v0}, Lcom/squareup/tickets/NoOpenTicketsSettings;-><init>()V

    sput-object v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->INSTANCE:Lcom/squareup/tickets/NoOpenTicketsSettings;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDefaultTicketTemplateCount()I
    .locals 1

    .line 46
    sget v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->defaultTicketTemplateCount:I

    return v0
.end method

.method public getMaxTicketGroupCount()I
    .locals 1

    .line 50
    sget v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->maxTicketGroupCount:I

    return v0
.end method

.method public getMaxTicketTemplatesPerGroup()I
    .locals 1

    .line 54
    sget v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->maxTicketTemplatesPerGroup:I

    return v0
.end method

.method public isBulkDeleteAllowed()Z
    .locals 1

    .line 18
    sget-boolean v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->isBulkDeleteAllowed:Z

    return v0
.end method

.method public isBulkVoidAllowed()Z
    .locals 1

    .line 20
    sget-boolean v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->isBulkVoidAllowed:Z

    return v0
.end method

.method public isDipTapToCreateTicketAllowed()Z
    .locals 1

    .line 14
    sget-boolean v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->isDipTapToCreateTicketAllowed:Z

    return v0
.end method

.method public isMergeTicketsAllowed()Z
    .locals 1

    .line 22
    sget-boolean v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->isMergeTicketsAllowed:Z

    return v0
.end method

.method public isMoveTicketsAllowed()Z
    .locals 1

    .line 24
    sget-boolean v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->isMoveTicketsAllowed:Z

    return v0
.end method

.method public isOpenTicketsAllowed()Z
    .locals 1

    .line 6
    sget-boolean v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->isOpenTicketsAllowed:Z

    return v0
.end method

.method public isOpenTicketsAsHomeScreenEnabled()Z
    .locals 1

    .line 42
    sget-boolean v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled:Z

    return v0
.end method

.method public isOpenTicketsEmployeeFilteringEnabled()Z
    .locals 1

    .line 10
    sget-boolean v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->isOpenTicketsEmployeeFilteringEnabled:Z

    return v0
.end method

.method public isOpenTicketsEnabled()Z
    .locals 1

    .line 8
    sget-boolean v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->isOpenTicketsEnabled:Z

    return v0
.end method

.method public isPredefinedTicketsAllowed()Z
    .locals 1

    .line 36
    sget-boolean v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->isPredefinedTicketsAllowed:Z

    return v0
.end method

.method public isPredefinedTicketsEnabled()Z
    .locals 1

    .line 38
    sget-boolean v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->isPredefinedTicketsEnabled:Z

    return v0
.end method

.method public isSavedCartsMode()Z
    .locals 1

    .line 32
    sget-boolean v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->isSavedCartsMode:Z

    return v0
.end method

.method public isSplitTicketsAllowed()Z
    .locals 1

    .line 16
    sget-boolean v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->isSplitTicketsAllowed:Z

    return v0
.end method

.method public isSwipeToCreateTicketAllowed()Z
    .locals 1

    .line 12
    sget-boolean v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->isSwipeToCreateTicketAllowed:Z

    return v0
.end method

.method public isTicketTransferAllowed()Z
    .locals 1

    .line 26
    sget-boolean v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->isTicketTransferAllowed:Z

    return v0
.end method

.method public isUsingDeviceProfiles()Z
    .locals 1

    .line 30
    sget-boolean v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->isUsingDeviceProfiles:Z

    return v0
.end method

.method public onPredefinedTicketsEnabled()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 58
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.just(false)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public setDefaultTicketTemplateCount(I)V
    .locals 0

    return-void
.end method

.method public setEnabled(Z)V
    .locals 0

    return-void
.end method

.method public setMaxTicketGroupCount(I)V
    .locals 0

    return-void
.end method

.method public setMaxTicketTemplatesPerGroup(I)V
    .locals 0

    return-void
.end method

.method public setOpenTicketsAsHomeScreenEnabled(Z)V
    .locals 0

    return-void
.end method

.method public setPredefinedTicketsEnabled(Z)V
    .locals 0

    return-void
.end method

.method public shouldShowOptInBetaWarning()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
