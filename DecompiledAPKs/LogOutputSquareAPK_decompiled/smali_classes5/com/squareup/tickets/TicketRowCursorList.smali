.class public Lcom/squareup/tickets/TicketRowCursorList;
.super Lcom/squareup/util/ReadOnlyCursorListWrapper;
.source "TicketRowCursorList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tickets/TicketRowCursorList$TicketRow;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/util/ReadOnlyCursorListWrapper<",
        "Lcom/squareup/tickets/TicketRowCursorList$TicketRow;",
        ">;"
    }
.end annotation


# static fields
.field public static final NO_EMPLOYEE:I = -0x1

.field public static final NO_SEARCH:Ljava/lang/String; = ""


# instance fields
.field private countForCurrentEmployee:I

.field private fixableTickets:J

.field private isOffline:Z

.field private final searchText:Ljava/lang/String;

.field private final sort:Lcom/squareup/tickets/TicketSort;

.field private unfixableTickets:J


# direct methods
.method public constructor <init>(Lcom/squareup/util/ReadOnlyCursorList;Lcom/squareup/tickets/TicketSort;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ReadOnlyCursorList<",
            "Lcom/squareup/tickets/TicketRowCursorList$TicketRow;",
            ">;",
            "Lcom/squareup/tickets/TicketSort;",
            ")V"
        }
    .end annotation

    const-string v0, ""

    .line 39
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/tickets/TicketRowCursorList;-><init>(Lcom/squareup/util/ReadOnlyCursorList;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/ReadOnlyCursorList;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ReadOnlyCursorList<",
            "Lcom/squareup/tickets/TicketRowCursorList$TicketRow;",
            ">;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v0, -0x1

    .line 44
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/tickets/TicketRowCursorList;-><init>(Lcom/squareup/util/ReadOnlyCursorList;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/ReadOnlyCursorList;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ReadOnlyCursorList<",
            "Lcom/squareup/tickets/TicketRowCursorList$TicketRow;",
            ">;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/util/ReadOnlyCursorListWrapper;-><init>(Lcom/squareup/util/ReadOnlyCursorList;)V

    .line 50
    iput-object p2, p0, Lcom/squareup/tickets/TicketRowCursorList;->sort:Lcom/squareup/tickets/TicketSort;

    .line 51
    iput-object p3, p0, Lcom/squareup/tickets/TicketRowCursorList;->searchText:Ljava/lang/String;

    .line 52
    iput p4, p0, Lcom/squareup/tickets/TicketRowCursorList;->countForCurrentEmployee:I

    return-void
.end method


# virtual methods
.method public countForCurrentEmployee()I
    .locals 2

    .line 68
    iget v0, p0, Lcom/squareup/tickets/TicketRowCursorList;->countForCurrentEmployee:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/squareup/tickets/TicketRowCursorList;->size()I

    move-result v0

    :cond_0
    return v0
.end method

.method public countForOtherEmployees()I
    .locals 2

    .line 75
    iget v0, p0, Lcom/squareup/tickets/TicketRowCursorList;->countForCurrentEmployee:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    .line 78
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/tickets/TicketRowCursorList;->size()I

    move-result v0

    iget v1, p0, Lcom/squareup/tickets/TicketRowCursorList;->countForCurrentEmployee:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getSearchText()Ljava/lang/String;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/tickets/TicketRowCursorList;->searchText:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSort()Lcom/squareup/tickets/TicketSort;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/tickets/TicketRowCursorList;->sort:Lcom/squareup/tickets/TicketSort;

    return-object v0
.end method

.method public getUnsyncableFixableTicketCount()J
    .locals 2

    .line 90
    iget-wide v0, p0, Lcom/squareup/tickets/TicketRowCursorList;->fixableTickets:J

    return-wide v0
.end method

.method public getUnsyncableUnfixableTicketCount()J
    .locals 2

    .line 102
    iget-wide v0, p0, Lcom/squareup/tickets/TicketRowCursorList;->unfixableTickets:J

    return-wide v0
.end method

.method public hasError()Z
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/tickets/TicketRowCursorList;->hasUnsyncedFixableTickets()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/tickets/TicketRowCursorList;->hasUnsyncedUnfixableTickets()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/tickets/TicketRowCursorList;->isOffline()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public hasSearch()Z
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/tickets/TicketRowCursorList;->searchText:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasUnsyncedFixableTickets()Z
    .locals 5

    .line 86
    iget-wide v0, p0, Lcom/squareup/tickets/TicketRowCursorList;->fixableTickets:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasUnsyncedUnfixableTickets()Z
    .locals 5

    .line 98
    iget-wide v0, p0, Lcom/squareup/tickets/TicketRowCursorList;->unfixableTickets:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOffline()Z
    .locals 1

    .line 110
    iget-boolean v0, p0, Lcom/squareup/tickets/TicketRowCursorList;->isOffline:Z

    return v0
.end method

.method public setOffline(Z)V
    .locals 0

    .line 114
    iput-boolean p1, p0, Lcom/squareup/tickets/TicketRowCursorList;->isOffline:Z

    return-void
.end method

.method public setUnsyncedFixableTicketCount(J)V
    .locals 0

    .line 94
    iput-wide p1, p0, Lcom/squareup/tickets/TicketRowCursorList;->fixableTickets:J

    return-void
.end method

.method public setUnsyncedUnfixableTicketCount(J)V
    .locals 0

    .line 106
    iput-wide p1, p0, Lcom/squareup/tickets/TicketRowCursorList;->unfixableTickets:J

    return-void
.end method
