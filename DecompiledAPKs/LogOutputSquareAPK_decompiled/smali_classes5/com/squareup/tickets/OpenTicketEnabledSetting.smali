.class public Lcom/squareup/tickets/OpenTicketEnabledSetting;
.super Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;
.source "OpenTicketEnabledSetting.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final openTicketsAsSavedCarts:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsEnabled:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/f2prateek/rx/preferences2/Preference;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-interface {p4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;-><init>(Lcom/squareup/settings/server/Features;Ljava/lang/Object;)V

    .line 28
    iput-object p1, p0, Lcom/squareup/tickets/OpenTicketEnabledSetting;->features:Lcom/squareup/settings/server/Features;

    .line 29
    iput-object p2, p0, Lcom/squareup/tickets/OpenTicketEnabledSetting;->accountStatusProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/tickets/OpenTicketEnabledSetting;->openTicketsEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    .line 31
    iput-object p4, p0, Lcom/squareup/tickets/OpenTicketEnabledSetting;->openTicketsAsSavedCarts:Ljavax/inject/Provider;

    return-void
.end method

.method private isAllowed()Ljava/lang/Boolean;
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicketEnabledSetting;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_TABLET:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/tickets/OpenTicketEnabledSetting;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    .line 56
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/tickets/OpenTicketEnabledSetting;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_V2:Lcom/squareup/settings/server/Features$Feature;

    .line 57
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 55
    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected getValueFromDeviceProfile()Ljava/lang/Boolean;
    .locals 1

    .line 43
    invoke-direct {p0}, Lcom/squareup/tickets/OpenTicketEnabledSetting;->isAllowed()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/OpenTicketEnabledSetting;->accountStatusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->open_tickets_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic getValueFromDeviceProfile()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicketEnabledSetting;->getValueFromDeviceProfile()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected getValueFromLocalSettings()Ljava/lang/Boolean;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicketEnabledSetting;->openTicketsEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method protected bridge synthetic getValueFromLocalSettings()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicketEnabledSetting;->getValueFromLocalSettings()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected isAllowedWhenUsingLocal()Z
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicketEnabledSetting;->openTicketsAsSavedCarts:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 39
    :cond_0
    invoke-direct {p0}, Lcom/squareup/tickets/OpenTicketEnabledSetting;->isAllowed()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method protected setValueLocallyInternal(Ljava/lang/Boolean;)V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicketEnabledSetting;->openTicketsEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0, p1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected bridge synthetic setValueLocallyInternal(Ljava/lang/Object;)V
    .locals 0

    .line 16
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/tickets/OpenTicketEnabledSetting;->setValueLocallyInternal(Ljava/lang/Boolean;)V

    return-void
.end method
