.class public abstract Lcom/squareup/tickets/voidcomp/CogsCache;
.super Ljava/lang/Object;
.source "CogsCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/cogs/Cogs;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/tickets/voidcomp/CogsCache;->cogs:Lcom/squareup/cogs/Cogs;

    .line 26
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/tickets/voidcomp/CogsCache;->data:Ljava/util/List;

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/cogs/Cogs;[Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            "[TT;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/tickets/voidcomp/CogsCache;->cogs:Lcom/squareup/cogs/Cogs;

    .line 31
    new-instance p1, Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/squareup/tickets/voidcomp/CogsCache;->data:Ljava/util/List;

    return-void
.end method

.method private replaceAll(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/squareup/tickets/voidcomp/CogsCache;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 68
    iget-object v0, p0, Lcom/squareup/tickets/voidcomp/CogsCache;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/tickets/voidcomp/CogsCache;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method getAll()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/squareup/tickets/voidcomp/CogsCache;->data:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public abstract getReasons()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public isEmpty()Z
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/tickets/voidcomp/CogsCache;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$loadAndPost$0$CogsCache(Ljava/lang/Runnable;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 51
    invoke-interface {p2}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/squareup/tickets/voidcomp/CogsCache;->replaceAll(Ljava/util/List;)V

    if-eqz p1, :cond_0

    .line 53
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method public loadAndPost()V
    .locals 1

    const/4 v0, 0x0

    .line 43
    invoke-virtual {p0, v0}, Lcom/squareup/tickets/voidcomp/CogsCache;->loadAndPost(Ljava/lang/Runnable;)V

    return-void
.end method

.method public loadAndPost(Ljava/lang/Runnable;)V
    .locals 3

    .line 48
    iget-object v0, p0, Lcom/squareup/tickets/voidcomp/CogsCache;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/tickets/voidcomp/-$$Lambda$SywCUhusgJDQU9WLj1w5UpwFNvU;

    invoke-direct {v1, p0}, Lcom/squareup/tickets/voidcomp/-$$Lambda$SywCUhusgJDQU9WLj1w5UpwFNvU;-><init>(Lcom/squareup/tickets/voidcomp/CogsCache;)V

    new-instance v2, Lcom/squareup/tickets/voidcomp/-$$Lambda$CogsCache$z0QutjQr1X_i1VqUoKy-Aa7YAm8;

    invoke-direct {v2, p0, p1}, Lcom/squareup/tickets/voidcomp/-$$Lambda$CogsCache$z0QutjQr1X_i1VqUoKy-Aa7YAm8;-><init>(Lcom/squareup/tickets/voidcomp/CogsCache;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method protected abstract readDataFromCogsLocal(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            ")",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end method
