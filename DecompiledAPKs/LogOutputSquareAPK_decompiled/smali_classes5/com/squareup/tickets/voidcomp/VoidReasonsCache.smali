.class public Lcom/squareup/tickets/voidcomp/VoidReasonsCache;
.super Lcom/squareup/tickets/voidcomp/CogsCache;
.source "VoidReasonsCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/tickets/voidcomp/CogsCache<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/cogs/Cogs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0, p1}, Lcom/squareup/tickets/voidcomp/CogsCache;-><init>(Lcom/squareup/cogs/Cogs;)V

    return-void
.end method

.method public varargs constructor <init>(Lcom/squareup/cogs/Cogs;[Ljava/lang/String;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/squareup/tickets/voidcomp/CogsCache;-><init>(Lcom/squareup/cogs/Cogs;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getReasons()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 24
    invoke-virtual {p0}, Lcom/squareup/tickets/voidcomp/VoidReasonsCache;->getAll()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected readDataFromCogsLocal(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 29
    invoke-interface {p1}, Lcom/squareup/shared/catalog/Catalog$Local;->readVoidReasons()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogVoidReason;

    .line 30
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogVoidReason;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method
