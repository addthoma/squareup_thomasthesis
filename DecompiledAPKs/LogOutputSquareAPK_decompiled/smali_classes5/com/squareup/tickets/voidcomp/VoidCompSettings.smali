.class public Lcom/squareup/tickets/voidcomp/VoidCompSettings;
.super Ljava/lang/Object;
.source "VoidCompSettings.java"


# instance fields
.field private final compDiscountsCache:Lcom/squareup/tickets/voidcomp/CompDiscountsCache;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final openTicketEnabledSetting:Lcom/squareup/tickets/OpenTicketEnabledSetting;

.field private final openTicketsAsSavedCarts:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final voidReasonsCache:Lcom/squareup/tickets/voidcomp/VoidReasonsCache;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/tickets/OpenTicketEnabledSetting;Ljavax/inject/Provider;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lcom/squareup/tickets/voidcomp/VoidReasonsCache;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/tickets/OpenTicketEnabledSetting;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            "Lcom/squareup/tickets/voidcomp/VoidReasonsCache;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->features:Lcom/squareup/settings/server/Features;

    .line 33
    iput-object p2, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->openTicketEnabledSetting:Lcom/squareup/tickets/OpenTicketEnabledSetting;

    .line 34
    iput-object p3, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->openTicketsAsSavedCarts:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->compDiscountsCache:Lcom/squareup/tickets/voidcomp/CompDiscountsCache;

    .line 36
    iput-object p5, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->voidReasonsCache:Lcom/squareup/tickets/voidcomp/VoidReasonsCache;

    return-void
.end method

.method private hasCompDiscounts()Z
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->compDiscountsCache:Lcom/squareup/tickets/voidcomp/CompDiscountsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/CompDiscountsCache;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private hasVoidReasons()Z
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->voidReasonsCache:Lcom/squareup/tickets/voidcomp/VoidReasonsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidReasonsCache;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private isOpenTicketsEnabled()Z
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->openTicketsAsSavedCarts:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->openTicketEnabledSetting:Lcom/squareup/tickets/OpenTicketEnabledSetting;

    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicketEnabledSetting;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    :cond_0
    invoke-direct {p0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isOpenTicketsFeatureFlagEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isOpenTicketsFeatureFlagEnabled()Z
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_TABLET:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    .line 83
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->OPEN_TICKETS_V2:Lcom/squareup/settings/server/Features$Feature;

    .line 84
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method public isCompEnabled()Z
    .locals 1

    .line 40
    invoke-virtual {p0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidCompAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->hasCompDiscounts()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isVoidCompAllowed()Z
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->VOID_COMP:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public isVoidEnabled()Z
    .locals 1

    .line 44
    invoke-virtual {p0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidCompAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    invoke-direct {p0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->hasVoidReasons()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-direct {p0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->openTicketsAsSavedCarts:Ljavax/inject/Provider;

    .line 47
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public shouldSyncCompDiscounts()Z
    .locals 1

    .line 51
    invoke-virtual {p0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidCompAllowed()Z

    move-result v0

    return v0
.end method

.method public shouldSyncVoidReasons()Z
    .locals 1

    .line 55
    invoke-virtual {p0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidCompAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
