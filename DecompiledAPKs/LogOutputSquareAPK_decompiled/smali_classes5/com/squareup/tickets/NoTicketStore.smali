.class public Lcom/squareup/tickets/NoTicketStore;
.super Ljava/lang/Object;
.source "NoTicketStore.java"

# interfaces
.implements Lcom/squareup/tickets/TicketStore;


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addTicket(Lcom/squareup/tickets/OpenTicket;)V
    .locals 0

    return-void
.end method

.method public close()V
    .locals 0

    return-void
.end method

.method public debugWipeStore()V
    .locals 0

    return-void
.end method

.method public deleteTickets(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public getAllDeletableTicketsOldestFirst()Lcom/squareup/util/ReadOnlyCursorList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/util/ReadOnlyCursorList<",
            "Lcom/squareup/tickets/TicketRowCursorList$TicketRow;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getCustomTicketCount(Ljava/lang/String;)I
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public getCustomTicketList(Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getGroupTicketCounts(Ljava/lang/String;)Ljava/util/Map;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public getGroupTicketList(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getNonClosedTicketsUnordered()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/v2/TicketInfo;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getNonZeroClientClockTickets()Lcom/squareup/tickets/TicketRowCursorList;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTicketCount(Ljava/lang/String;)I
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public getTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            ")",
            "Lcom/squareup/tickets/TicketRowCursorList;"
        }
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public updateTicket(Lcom/squareup/tickets/OpenTicket;)V
    .locals 0

    return-void
.end method
