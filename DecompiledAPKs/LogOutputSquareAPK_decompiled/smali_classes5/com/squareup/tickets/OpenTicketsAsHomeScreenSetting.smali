.class Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;
.super Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;
.source "OpenTicketsAsHomeScreenSetting.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketAsHomeScreenEnabled:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final predefinedTicketsEnabledSetting:Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const/4 v0, 0x0

    .line 23
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;-><init>(Lcom/squareup/settings/server/Features;Ljava/lang/Object;)V

    .line 24
    iput-object p2, p0, Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;->accountStatusProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;->openTicketAsHomeScreenEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    .line 26
    iput-object p4, p0, Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;->predefinedTicketsEnabledSetting:Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;

    return-void
.end method


# virtual methods
.method protected getValueFromDeviceProfile()Ljava/lang/Boolean;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;->accountStatusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    return-object v0
.end method

.method protected bridge synthetic getValueFromDeviceProfile()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;->getValueFromDeviceProfile()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected getValueFromLocalSettings()Ljava/lang/Boolean;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;->openTicketAsHomeScreenEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method protected bridge synthetic getValueFromLocalSettings()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;->getValueFromLocalSettings()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected isAllowedWhenUsingLocal()Z
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;->predefinedTicketsEnabledSetting:Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;

    invoke-virtual {v0}, Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;->isAllowedWhenUsingLocal()Z

    move-result v0

    return v0
.end method

.method protected setValueLocallyInternal(Ljava/lang/Boolean;)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;->openTicketAsHomeScreenEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0, p1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected bridge synthetic setValueLocallyInternal(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;->setValueLocallyInternal(Ljava/lang/Boolean;)V

    return-void
.end method
