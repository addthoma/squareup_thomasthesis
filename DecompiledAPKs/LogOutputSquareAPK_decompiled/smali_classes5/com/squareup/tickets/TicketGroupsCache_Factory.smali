.class public final Lcom/squareup/tickets/TicketGroupsCache_Factory;
.super Ljava/lang/Object;
.source "TicketGroupsCache_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tickets/TicketGroupsCache;",
        ">;"
    }
.end annotation


# instance fields
.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/tickets/TicketGroupsCache_Factory;->cogsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/tickets/TicketGroupsCache_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;)",
            "Lcom/squareup/tickets/TicketGroupsCache_Factory;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/tickets/TicketGroupsCache_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/tickets/TicketGroupsCache_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/cogs/Cogs;)Lcom/squareup/tickets/TicketGroupsCache;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/tickets/TicketGroupsCache;

    invoke-direct {v0, p0}, Lcom/squareup/tickets/TicketGroupsCache;-><init>(Lcom/squareup/cogs/Cogs;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tickets/TicketGroupsCache;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/tickets/TicketGroupsCache_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    invoke-static {v0}, Lcom/squareup/tickets/TicketGroupsCache_Factory;->newInstance(Lcom/squareup/cogs/Cogs;)Lcom/squareup/tickets/TicketGroupsCache;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/tickets/TicketGroupsCache_Factory;->get()Lcom/squareup/tickets/TicketGroupsCache;

    move-result-object v0

    return-object v0
.end method
