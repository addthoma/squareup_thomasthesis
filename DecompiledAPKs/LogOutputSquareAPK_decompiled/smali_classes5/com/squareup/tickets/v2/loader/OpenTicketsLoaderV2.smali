.class public abstract Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2;
.super Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;
.source "OpenTicketsLoaderV2.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\u0008&\u0018\u00002\u00020\u0001:\u0001\"B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0012\u0010\u000f\u001a\u00020\u00102\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u0012H&J\u001a\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\u00152\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u0012H&J\u0006\u0010\u0016\u001a\u00020\u0010J\u0006\u0010\u0017\u001a\u00020\u0010J(\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u00192\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u001b\u001a\u00020\u001c2\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u0012H&J6\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u00192\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010\u001b\u001a\u00020\u001c2\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u00122\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u00150!H&R\u0012\u0010\u000b\u001a\u00020\u000cX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2;",
        "Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;",
        "tickets",
        "Lcom/squareup/tickets/Tickets;",
        "ticketsListScheduler",
        "Lcom/squareup/opentickets/TicketsListScheduler;",
        "ticketsLogger",
        "Lcom/squareup/log/tickets/OpenTicketsLogger;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "(Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/connectivity/ConnectivityMonitor;)V",
        "ticketsLifecycleWorker",
        "Lcom/squareup/workflow/LifecycleWorker;",
        "getTicketsLifecycleWorker",
        "()Lcom/squareup/workflow/LifecycleWorker;",
        "loadAllTickets",
        "",
        "ticketSort",
        "Lcom/squareup/tickets/TicketSort;",
        "loadTicketsForSearch",
        "searchTerm",
        "",
        "onStart",
        "onStop",
        "searchTicketsWorker",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;",
        "permittedToViewAllTickets",
        "",
        "syncAndLoadTicketsWorker",
        "filterType",
        "Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;",
        "excludedIds",
        "",
        "TicketLoadResult",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/connectivity/ConnectivityMonitor;)V
    .locals 1

    const-string/jumbo v0, "tickets"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "ticketsListScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "ticketsLogger"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectivityMonitor"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;-><init>(Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/connectivity/ConnectivityMonitor;)V

    return-void
.end method

.method public static synthetic loadAllTickets$default(Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2;Lcom/squareup/tickets/TicketSort;ILjava/lang/Object;)V
    .locals 0

    if-nez p3, :cond_1

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 25
    sget-object p1, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2;->loadAllTickets(Lcom/squareup/tickets/TicketSort;)V

    return-void

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: loadAllTickets"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static synthetic loadTicketsForSearch$default(Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2;Ljava/lang/String;Lcom/squareup/tickets/TicketSort;ILjava/lang/Object;)V
    .locals 0

    if-nez p4, :cond_1

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 33
    sget-object p2, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2;->loadTicketsForSearch(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;)V

    return-void

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: loadTicketsForSearch"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static synthetic searchTicketsWorker$default(Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2;Ljava/lang/String;ZLcom/squareup/tickets/TicketSort;ILjava/lang/Object;)Lcom/squareup/workflow/Worker;
    .locals 0

    if-nez p5, :cond_1

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 65
    sget-object p3, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2;->searchTicketsWorker(Ljava/lang/String;ZLcom/squareup/tickets/TicketSort;)Lcom/squareup/workflow/Worker;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: searchTicketsWorker"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static synthetic syncAndLoadTicketsWorker$default(Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2;Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;ZLcom/squareup/tickets/TicketSort;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/workflow/Worker;
    .locals 0

    if-nez p6, :cond_1

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    .line 51
    sget-object p3, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2;->syncAndLoadTicketsWorker(Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;ZLcom/squareup/tickets/TicketSort;Ljava/util/List;)Lcom/squareup/workflow/Worker;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: syncAndLoadTicketsWorker"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public abstract getTicketsLifecycleWorker()Lcom/squareup/workflow/LifecycleWorker;
.end method

.method public abstract loadAllTickets(Lcom/squareup/tickets/TicketSort;)V
.end method

.method public abstract loadTicketsForSearch(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;)V
.end method

.method public final onStart()V
    .locals 0

    .line 73
    invoke-virtual {p0}, Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2;->start()V

    return-void
.end method

.method public final onStop()V
    .locals 0

    .line 74
    invoke-virtual {p0}, Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2;->stop()V

    return-void
.end method

.method public abstract searchTicketsWorker(Ljava/lang/String;ZLcom/squareup/tickets/TicketSort;)Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Lcom/squareup/tickets/TicketSort;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract syncAndLoadTicketsWorker(Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;ZLcom/squareup/tickets/TicketSort;Ljava/util/List;)Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;",
            "Z",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;",
            ">;"
        }
    .end annotation
.end method
