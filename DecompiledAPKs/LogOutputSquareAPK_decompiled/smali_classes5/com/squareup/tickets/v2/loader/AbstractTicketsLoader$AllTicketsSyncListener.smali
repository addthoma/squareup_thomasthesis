.class public Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$AllTicketsSyncListener;
.super Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;
.source "AbstractTicketsLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AllTicketsSyncListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;


# direct methods
.method public constructor <init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketSort;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/TicketSort;",
            ")V"
        }
    .end annotation

    .line 386
    iput-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$AllTicketsSyncListener;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    .line 387
    invoke-direct/range {p0 .. p5}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketSort;)V

    return-void
.end method


# virtual methods
.method protected fetchTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;",
            ")V"
        }
    .end annotation

    .line 393
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$AllTicketsSyncListener;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    invoke-static {v0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->access$900(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Lcom/squareup/tickets/Tickets;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-interface/range {v1 .. v7}, Lcom/squareup/tickets/Tickets;->getTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method
