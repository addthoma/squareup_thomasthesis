.class public final Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;
.super Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;
.source "TicketFilterType.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CustomTickets"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\t\u0010\u0007\u001a\u00020\u0008H\u00d6\u0001J\u0019\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0008H\u00d6\u0001R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;",
        "Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;",
        "()V",
        "id",
        "",
        "getId",
        "()Ljava/lang/String;",
        "describeContents",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final INSTANCE:Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;

# The value of this static final field might be set in the static constructor
.field private static final id:Ljava/lang/String; = "group-list-presenter-selected-section-custom-tickets"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;

    invoke-direct {v0}, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;-><init>()V

    sput-object v0, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;->INSTANCE:Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;

    const-string v0, "group-list-presenter-selected-section-custom-tickets"

    .line 27
    sput-object v0, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;->id:Ljava/lang/String;

    new-instance v0, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets$Creator;

    invoke-direct {v0}, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets$Creator;-><init>()V

    sput-object v0, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, v0}, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;->id:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
