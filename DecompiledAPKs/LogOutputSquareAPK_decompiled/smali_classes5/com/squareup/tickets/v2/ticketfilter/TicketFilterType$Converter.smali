.class public final Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$Converter;
.super Ljava/lang/Object;
.source "TicketFilterType.kt"

# interfaces
.implements Lcom/f2prateek/rx/preferences2/Preference$Converter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Converter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/f2prateek/rx/preferences2/Preference$Converter<",
        "Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\u0002H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$Converter;",
        "Lcom/f2prateek/rx/preferences2/Preference$Converter;",
        "Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;",
        "()V",
        "deserialize",
        "id",
        "",
        "serialize",
        "value",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$Converter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$Converter;

    invoke-direct {v0}, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$Converter;-><init>()V

    sput-object v0, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$Converter;->INSTANCE:Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$Converter;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public deserialize(Ljava/lang/String;)Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;
    .locals 1

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object v0, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$AllTickets;->INSTANCE:Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$AllTickets;

    invoke-virtual {v0}, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$AllTickets;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$AllTickets;->INSTANCE:Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$AllTickets;

    check-cast p1, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;

    goto :goto_0

    .line 38
    :cond_0
    sget-object v0, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$SearchByName;->INSTANCE:Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$SearchByName;

    invoke-virtual {v0}, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$SearchByName;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$SearchByName;->INSTANCE:Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$SearchByName;

    check-cast p1, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;

    goto :goto_0

    .line 39
    :cond_1
    sget-object v0, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;->INSTANCE:Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;

    invoke-virtual {v0}, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;->INSTANCE:Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$CustomTickets;

    check-cast p1, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;

    goto :goto_0

    .line 40
    :cond_2
    new-instance v0, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$TicketGroup;

    invoke-direct {v0, p1}, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$TicketGroup;-><init>(Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic deserialize(Ljava/lang/String;)Ljava/lang/Object;
    .locals 0

    .line 34
    invoke-virtual {p0, p1}, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$Converter;->deserialize(Ljava/lang/String;)Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;

    move-result-object p1

    return-object p1
.end method

.method public serialize(Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;)Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p1}, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;->getId()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;

    invoke-virtual {p0, p1}, Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType$Converter;->serialize(Lcom/squareup/tickets/v2/ticketfilter/TicketFilterType;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
