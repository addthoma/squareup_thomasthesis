.class final Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpSwipeRefresh$1;
.super Ljava/lang/Object;
.source "TransferReportsLayoutRunner.kt"

# interfaces
.implements Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$OnRefreshListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/transferreports/TransferReportsLayoutRunner;->setUpSwipeRefresh()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "onRefresh"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/transferreports/TransferReportsLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/transferreports/TransferReportsLayoutRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpSwipeRefresh$1;->this$0:Lcom/squareup/transferreports/TransferReportsLayoutRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onRefresh()V
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpSwipeRefresh$1;->this$0:Lcom/squareup/transferreports/TransferReportsLayoutRunner;

    invoke-static {v0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->access$getScreen$p(Lcom/squareup/transferreports/TransferReportsLayoutRunner;)Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->getOnReload()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 224
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpSwipeRefresh$1;->this$0:Lcom/squareup/transferreports/TransferReportsLayoutRunner;

    invoke-static {v0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->access$getSwipeRefresh$p(Lcom/squareup/transferreports/TransferReportsLayoutRunner;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    return-void
.end method
