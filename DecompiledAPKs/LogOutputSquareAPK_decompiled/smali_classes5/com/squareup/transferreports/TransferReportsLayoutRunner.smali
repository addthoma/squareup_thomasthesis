.class public final Lcom/squareup/transferreports/TransferReportsLayoutRunner;
.super Ljava/lang/Object;
.source "TransferReportsLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow;,
        Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;,
        Lcom/squareup/transferreports/TransferReportsLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/transferreports/TransferReportsScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransferReportsLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransferReportsLayoutRunner.kt\ncom/squareup/transferreports/TransferReportsLayoutRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 7 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n+ 8 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n*L\n1#1,507:1\n1370#2:508\n1401#2,4:509\n1360#2:513\n1429#2,3:514\n49#3:517\n50#3,3:523\n53#3:564\n599#4,4:518\n601#4:522\n310#5,3:526\n313#5,3:535\n310#5,3:538\n313#5,3:547\n355#5,3:550\n358#5,3:559\n35#6,6:529\n35#6,6:541\n35#6,6:553\n43#7,2:562\n17#8,2:565\n*E\n*S KotlinDebug\n*F\n+ 1 TransferReportsLayoutRunner.kt\ncom/squareup/transferreports/TransferReportsLayoutRunner\n*L\n169#1:508\n169#1,4:509\n180#1:513\n180#1,3:514\n250#1:517\n250#1,3:523\n250#1:564\n250#1,4:518\n250#1:522\n250#1,3:526\n250#1,3:535\n250#1,3:538\n250#1,3:547\n250#1,3:550\n250#1,3:559\n250#1,6:529\n250#1,6:541\n250#1,6:553\n250#1,2:562\n99#1,2:565\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00ce\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0003VWXB+\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ \u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020\u00072\u0006\u0010,\u001a\u00020-H\u0002J \u0010.\u001a\u00020(2\u0006\u0010/\u001a\u00020*2\u0006\u00100\u001a\u0002012\u0006\u00102\u001a\u00020-H\u0002J\u0018\u00103\u001a\u00020(2\u0006\u00104\u001a\u0002052\u0006\u00106\u001a\u000207H\u0002J\u0018\u00108\u001a\u00020(2\u0006\u00109\u001a\u00020:2\u0006\u0010\u0003\u001a\u00020-H\u0002J\u0018\u0010;\u001a\u0002052\u0006\u0010<\u001a\u00020\u00072\u0006\u0010=\u001a\u00020>H\u0002J\u0008\u0010?\u001a\u00020(H\u0002J\u0008\u0010@\u001a\u00020(H\u0002J\u0008\u0010A\u001a\u00020(H\u0002J\u0008\u0010B\u001a\u00020(H\u0002J\u0008\u0010C\u001a\u00020(H\u0002J\u0010\u0010D\u001a\u00020(2\u0006\u0010!\u001a\u00020\"H\u0002J\u0008\u0010E\u001a\u00020(H\u0002J \u0010F\u001a\u00020(2\u0006\u00102\u001a\u00020-2\u0006\u0010G\u001a\u00020H2\u0006\u0010I\u001a\u00020*H\u0002J \u0010J\u001a\u00020(2\u0006\u00102\u001a\u00020-2\u0006\u0010G\u001a\u00020H2\u0006\u0010K\u001a\u00020LH\u0002J\u0008\u0010M\u001a\u00020(H\u0002J\u0008\u0010N\u001a\u00020(H\u0002J\u0008\u0010O\u001a\u00020(H\u0002J\u0008\u0010P\u001a\u00020(H\u0002J\u0018\u0010Q\u001a\u00020(2\u0006\u0010R\u001a\u00020\u00022\u0006\u0010S\u001a\u00020TH\u0016J\u0008\u0010U\u001a\u00020(H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020&X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006Y"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/transferreports/TransferReportsScreen;",
        "view",
        "Landroid/view/View;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "shortDateFormatter",
        "Ljava/text/DateFormat;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "allowBack",
        "",
        "context",
        "Landroid/content/Context;",
        "dataRecyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "depositHistoryRecycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow;",
        "detailViewEnabled",
        "errorSection",
        "Lcom/squareup/marin/widgets/MarinGlyphMessage;",
        "layoutParams",
        "Landroid/view/ViewGroup$LayoutParams;",
        "mediumWeight",
        "Landroid/text/style/CharacterStyle;",
        "res",
        "Landroid/content/res/Resources;",
        "screen",
        "Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;",
        "spinner",
        "Landroid/widget/ProgressBar;",
        "swipeRefresh",
        "Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;",
        "bindActiveSales",
        "",
        "activeSalesReport",
        "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
        "activeSalesMoney",
        "activeSalesRow",
        "Lcom/squareup/ui/account/view/SmartLineRow;",
        "bindPendingSettlement",
        "pendingSettlementReportWrapper",
        "pendingSettlements",
        "Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;",
        "row",
        "bindTitle",
        "title",
        "",
        "titleView",
        "Lcom/squareup/marketfont/MarketTextView;",
        "bindsDeposits",
        "settlementReport",
        "Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;",
        "formatMoney",
        "money",
        "type",
        "Lcom/squareup/protos/client/settlements/SettlementType;",
        "hideError",
        "hideSections",
        "hideSpinner",
        "onFailure",
        "onLoading",
        "onScreen",
        "onSuccess",
        "setCurrentBalanceOnClickListener",
        "depositType",
        "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
        "settlementReportWrapper",
        "setHistoryOnClickListener",
        "token",
        "",
        "setUpActionBar",
        "setUpHistoryRecycler",
        "setUpSwipeRefresh",
        "showError",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "showSpinner",
        "Binding",
        "Factory",
        "ReportsRow",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final allowBack:Z

.field private final context:Landroid/content/Context;

.field private final dataRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private depositHistoryRecycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow;",
            ">;"
        }
    .end annotation
.end field

.field private detailViewEnabled:Z

.field private final errorSection:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private final layoutParams:Landroid/view/ViewGroup$LayoutParams;

.field private final mediumWeight:Landroid/text/style/CharacterStyle;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final res:Landroid/content/res/Resources;

.field private screen:Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;

.field private final shortDateFormatter:Ljava/text/DateFormat;

.field private final spinner:Landroid/widget/ProgressBar;

.field private final swipeRefresh:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "shortDateFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->shortDateFormatter:Ljava/text/DateFormat;

    iput-object p4, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 86
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->view:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    const-string p2, "ActionBarView.findIn(view)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 87
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/transferreports/R$id;->swipe_refresh:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.swipe_refresh)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->swipeRefresh:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    .line 88
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/transferreports/R$id;->data_recycler_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.data_recycler_view)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->dataRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 89
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/transferreports/R$id;->spinner:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.spinner)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->spinner:Landroid/widget/ProgressBar;

    .line 90
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/transferreports/R$id;->error_section:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.error_section)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->errorSection:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 91
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo p2, "view.context"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->context:Landroid/content/Context;

    .line 92
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string/jumbo p2, "view.resources"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->res:Landroid/content/res/Resources;

    .line 94
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getDevice(Landroid/content/Context;)Lcom/squareup/util/Device;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->allowBack:Z

    .line 95
    new-instance p1, Landroid/view/ViewGroup$LayoutParams;

    .line 97
    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->res:Landroid/content/res/Resources;

    sget p3, Lcom/squareup/marin/R$dimen;->marin_tall_list_row_height:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    const/4 p3, -0x1

    .line 95
    invoke-direct {p1, p3, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->layoutParams:Landroid/view/ViewGroup$LayoutParams;

    .line 99
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->context:Landroid/content/Context;

    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 566
    new-instance p3, Lcom/squareup/fonts/FontSpan;

    const/4 p4, 0x0

    invoke-static {p2, p4}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result p2

    invoke-direct {p3, p1, p2}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast p3, Landroid/text/style/CharacterStyle;

    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    .line 107
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->view:Landroid/view/View;

    sget-object p2, Lcom/squareup/transferreports/TransferReportsLayoutRunner$1;->INSTANCE:Lcom/squareup/transferreports/TransferReportsLayoutRunner$1;

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, p2}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 108
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->setUpHistoryRecycler()V

    return-void
.end method

.method public static final synthetic access$bindActiveSales(Lcom/squareup/transferreports/TransferReportsLayoutRunner;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/protos/common/Money;Lcom/squareup/ui/account/view/SmartLineRow;)V
    .locals 0

    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->bindActiveSales(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/protos/common/Money;Lcom/squareup/ui/account/view/SmartLineRow;)V

    return-void
.end method

.method public static final synthetic access$bindPendingSettlement(Lcom/squareup/transferreports/TransferReportsLayoutRunner;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;Lcom/squareup/ui/account/view/SmartLineRow;)V
    .locals 0

    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->bindPendingSettlement(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;Lcom/squareup/ui/account/view/SmartLineRow;)V

    return-void
.end method

.method public static final synthetic access$bindTitle(Lcom/squareup/transferreports/TransferReportsLayoutRunner;Ljava/lang/CharSequence;Lcom/squareup/marketfont/MarketTextView;)V
    .locals 0

    .line 79
    invoke-direct {p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->bindTitle(Ljava/lang/CharSequence;Lcom/squareup/marketfont/MarketTextView;)V

    return-void
.end method

.method public static final synthetic access$bindsDeposits(Lcom/squareup/transferreports/TransferReportsLayoutRunner;Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;Lcom/squareup/ui/account/view/SmartLineRow;)V
    .locals 0

    .line 79
    invoke-direct {p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->bindsDeposits(Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;Lcom/squareup/ui/account/view/SmartLineRow;)V

    return-void
.end method

.method public static final synthetic access$getDetailViewEnabled$p(Lcom/squareup/transferreports/TransferReportsLayoutRunner;)Z
    .locals 0

    .line 79
    iget-boolean p0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->detailViewEnabled:Z

    return p0
.end method

.method public static final synthetic access$getLayoutParams$p(Lcom/squareup/transferreports/TransferReportsLayoutRunner;)Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->layoutParams:Landroid/view/ViewGroup$LayoutParams;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/transferreports/TransferReportsLayoutRunner;)Landroid/content/res/Resources;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->res:Landroid/content/res/Resources;

    return-object p0
.end method

.method public static final synthetic access$getScreen$p(Lcom/squareup/transferreports/TransferReportsLayoutRunner;)Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;
    .locals 1

    .line 79
    iget-object p0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;

    if-nez p0, :cond_0

    const-string v0, "screen"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getSwipeRefresh$p(Lcom/squareup/transferreports/TransferReportsLayoutRunner;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->swipeRefresh:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    return-object p0
.end method

.method public static final synthetic access$setDetailViewEnabled$p(Lcom/squareup/transferreports/TransferReportsLayoutRunner;Z)V
    .locals 0

    .line 79
    iput-boolean p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->detailViewEnabled:Z

    return-void
.end method

.method public static final synthetic access$setScreen$p(Lcom/squareup/transferreports/TransferReportsLayoutRunner;Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;)V
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;

    return-void
.end method

.method private final bindActiveSales(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/protos/common/Money;Lcom/squareup/ui/account/view/SmartLineRow;)V
    .locals 1

    .line 324
    sget v0, Lcom/squareup/transferreports/R$id;->active_sales_row:I

    invoke-virtual {p3, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setId(I)V

    .line 325
    sget v0, Lcom/squareup/transferreports/R$string;->deposits_report_active_sales:I

    invoke-virtual {p3, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(I)V

    const/4 v0, 0x0

    .line 326
    invoke-virtual {p3, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 328
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 329
    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p3, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    const/4 v0, 0x1

    .line 330
    invoke-virtual {p3, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 332
    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->detailViewEnabled:Z

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 333
    sget-object p2, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p3, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 334
    sget-object p2, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ACTIVE_SALES:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-direct {p0, p3, p2, p1}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->setCurrentBalanceOnClickListener(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)V

    goto :goto_0

    .line 336
    :cond_0
    sget-object p1, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p3, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    const/4 p1, 0x0

    .line 337
    invoke-virtual {p3, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void
.end method

.method private final bindPendingSettlement(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;Lcom/squareup/ui/account/view/SmartLineRow;)V
    .locals 3

    .line 347
    invoke-virtual {p2}, Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;->component1()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;->component2()Ljava/util/Date;

    move-result-object p2

    .line 348
    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->isNegative(Lcom/squareup/protos/common/Money;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 349
    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->PENDING_WITHDRAWAL:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-direct {p0, p3, v1, p1}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->setCurrentBalanceOnClickListener(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)V

    .line 350
    sget p1, Lcom/squareup/transferreports/R$string;->deposits_report_pending_withdrawal:I

    invoke-virtual {p3, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(I)V

    .line 351
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/transferreports/R$string;->deposits_report_withdrawing_date:I

    invoke-static {p1, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v1, "Phrase.from(res, R.strin\u2026_report_withdrawing_date)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 353
    :cond_0
    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->PENDING_DEPOSIT:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-direct {p0, p3, v1, p1}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->setCurrentBalanceOnClickListener(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)V

    .line 354
    sget p1, Lcom/squareup/transferreports/R$string;->deposits_report_pending_deposit:I

    invoke-virtual {p3, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(I)V

    .line 355
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/transferreports/R$string;->deposits_report_sending_date:I

    invoke-static {p1, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v1, "Phrase.from(res, R.strin\u2026sits_report_sending_date)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    const/4 v1, 0x1

    if-nez p2, :cond_1

    const/4 p1, 0x0

    .line 358
    invoke-virtual {p3, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    const-string p1, ""

    .line 359
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 361
    :cond_1
    invoke-virtual {p3, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 362
    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->shortDateFormatter:Ljava/text/DateFormat;

    invoke-virtual {v2, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string v2, "date"

    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 363
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 365
    :goto_1
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 366
    sget p1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p3, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 367
    invoke-virtual {p3, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 368
    invoke-virtual {p3, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 370
    iget-boolean p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->detailViewEnabled:Z

    if-eqz p1, :cond_2

    .line 371
    sget-object p1, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p3, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    goto :goto_2

    .line 373
    :cond_2
    sget-object p1, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p3, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    :goto_2
    return-void
.end method

.method private final bindTitle(Ljava/lang/CharSequence;Lcom/squareup/marketfont/MarketTextView;)V
    .locals 0

    .line 316
    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final bindsDeposits(Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;Lcom/squareup/ui/account/view/SmartLineRow;)V
    .locals 7

    .line 381
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;->component1()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;->component2()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;->component3()Lcom/squareup/protos/client/settlements/SettlementType;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;->component4()Ljava/lang/String;

    move-result-object p1

    const/4 v3, 0x1

    .line 383
    invoke-virtual {p2, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 385
    iget-boolean v4, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->detailViewEnabled:Z

    if-eqz v4, :cond_0

    .line 386
    sget-object v4, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p2, v4}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    goto :goto_0

    .line 388
    :cond_0
    sget-object v4, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p2, v4}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 391
    :goto_0
    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->isNegative(Lcom/squareup/protos/common/Money;)Z

    move-result v4

    const/4 v5, 0x3

    if-eqz v4, :cond_1

    .line 392
    sget-object v4, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->WITHDRAWAL:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-direct {p0, p2, v4, p1}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->setHistoryOnClickListener(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Ljava/lang/String;)V

    .line 393
    sget p1, Lcom/squareup/transferreports/R$string;->deposits_report_withdrawal:I

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(I)V

    goto :goto_1

    .line 395
    :cond_1
    sget-object v4, Lcom/squareup/transferreports/TransferReportsLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v2}, Lcom/squareup/protos/client/settlements/SettlementType;->ordinal()I

    move-result v6

    aget v4, v4, v6

    if-eq v4, v3, :cond_6

    const/4 v6, 0x2

    if-eq v4, v6, :cond_5

    if-eq v4, v5, :cond_4

    const/4 v6, 0x4

    if-eq v4, v6, :cond_3

    const/4 v6, 0x5

    if-ne v4, v6, :cond_2

    .line 413
    sget-object v4, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->DEBIT_CARD_PAY_IN:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-direct {p0, p2, v4, p1}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->setHistoryOnClickListener(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Ljava/lang/String;)V

    .line 414
    sget p1, Lcom/squareup/transferreports/R$string;->deposits_report_pay_in:I

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(I)V

    goto :goto_1

    .line 416
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 409
    :cond_3
    sget-object v4, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->INSTANT_DEPOSIT:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-direct {p0, p2, v4, p1}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->setHistoryOnClickListener(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Ljava/lang/String;)V

    .line 410
    sget p1, Lcom/squareup/transferreports/R$string;->deposits_report_instant_deposit:I

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(I)V

    goto :goto_1

    .line 405
    :cond_4
    sget-object v4, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->SAME_DAY_DEPOSIT:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-direct {p0, p2, v4, p1}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->setHistoryOnClickListener(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Ljava/lang/String;)V

    .line 406
    sget p1, Lcom/squareup/transferreports/R$string;->deposits_report_same_day_deposit:I

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(I)V

    goto :goto_1

    .line 401
    :cond_5
    sget-object v4, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->NEXT_BUSINESS_DAY_DEPOSIT:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-direct {p0, p2, v4, p1}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->setHistoryOnClickListener(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Ljava/lang/String;)V

    .line 402
    sget p1, Lcom/squareup/transferreports/R$string;->deposits_report_next_business_day_deposit:I

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(I)V

    goto :goto_1

    .line 397
    :cond_6
    sget-object v4, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->NO_DEPOSIT:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-direct {p0, p2, v4, p1}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->setHistoryOnClickListener(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Ljava/lang/String;)V

    .line 398
    sget p1, Lcom/squareup/transferreports/R$string;->deposits_report_no_deposit:I

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(I)V

    .line 421
    :goto_1
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->res:Landroid/content/res/Resources;

    sget v4, Lcom/squareup/transferreports/R$string;->deposits_report_sent_date_time:I

    invoke-static {p1, v4}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 422
    invoke-static {v5, v5}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v4, "sent_date_time"

    invoke-virtual {p1, v4, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 423
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 424
    invoke-direct {p0, v0, v2}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->formatMoney(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/settlements/SettlementType;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 425
    sget p1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 426
    invoke-virtual {p2, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    return-void
.end method

.method private final formatMoney(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/settlements/SettlementType;)Ljava/lang/CharSequence;
    .locals 1

    .line 433
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 434
    sget-object v0, Lcom/squareup/protos/client/settlements/SettlementType;->DEBIT_CARD_PAY_IN:Lcom/squareup/protos/client/settlements/SettlementType;

    if-ne p2, v0, :cond_0

    .line 435
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v0, 0x2b

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    invoke-static {p1, p2}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1

    :cond_0
    const-string p2, "formattedMoney"

    .line 437
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final hideError()V
    .locals 2

    .line 245
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->errorSection:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    return-void
.end method

.method private final hideSections()V
    .locals 2

    .line 229
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->depositHistoryRecycler:Lcom/squareup/cycler/Recycler;

    if-nez v0, :cond_0

    const-string v1, "depositHistoryRecycler"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler;->clear()V

    return-void
.end method

.method private final hideSpinner()V
    .locals 2

    .line 237
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->spinner:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method private final onFailure()V
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->errorSection:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    new-instance v1, Lcom/squareup/transferreports/TransferReportsLayoutRunner$onFailure$1;

    invoke-direct {v1, p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$onFailure$1;-><init>(Lcom/squareup/transferreports/TransferReportsLayoutRunner;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->hideSpinner()V

    .line 141
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->hideSections()V

    .line 142
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->showError()V

    return-void
.end method

.method private final onLoading()V
    .locals 0

    .line 133
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->hideError()V

    .line 134
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->hideSections()V

    .line 135
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->showSpinner()V

    return-void
.end method

.method private final onScreen(Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;)V
    .locals 2

    .line 119
    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;

    .line 120
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->view:Landroid/view/View;

    new-instance v1, Lcom/squareup/transferreports/TransferReportsLayoutRunner$onScreen$1;

    invoke-direct {v1, p1}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$onScreen$1;-><init>(Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 121
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->getDetailViewEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->detailViewEnabled:Z

    .line 122
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->setUpActionBar()V

    .line 123
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->setUpSwipeRefresh()V

    .line 125
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->getReportSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getState()Lcom/squareup/transferreports/TransferReportsLoader$State;

    move-result-object p1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLoader$State;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 128
    :cond_0
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->onFailure()V

    goto :goto_0

    .line 127
    :cond_1
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->onSuccess()V

    goto :goto_0

    .line 126
    :cond_2
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->onLoading()V

    :goto_0
    return-void
.end method

.method private final onSuccess()V
    .locals 11

    .line 146
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->hideSpinner()V

    .line 147
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->hideError()V

    .line 149
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;

    const-string v1, "screen"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->getReportSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v0

    .line 151
    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->getShowCurrentBalanceAndActiveSales()Z

    move-result v1

    .line 152
    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->hasPendingSettlements()Z

    move-result v2

    .line 153
    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->isHistoryEmpty()Z

    move-result v3

    .line 156
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/List;

    if-eqz v1, :cond_2

    .line 158
    sget-object v1, Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$TitleRow$CurrentBalanceTitleRow;->INSTANCE:Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$TitleRow$CurrentBalanceTitleRow;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    new-instance v1, Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$DataRow$ActiveSalesRow;

    .line 161
    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->activeSalesReport()Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    move-result-object v5

    .line 162
    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->activeSalesMoney()Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 160
    invoke-direct {v1, v5, v6}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$DataRow$ActiveSalesRow;-><init>(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/protos/common/Money;)V

    .line 159
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    const/16 v1, 0xa

    if-eqz v2, :cond_5

    .line 167
    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getPendingSettlementsList()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 508
    new-instance v5, Ljava/util/ArrayList;

    invoke-static {v2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v5, Ljava/util/Collection;

    const/4 v6, 0x0

    .line 510
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    add-int/lit8 v8, v6, 0x1

    if-gez v6, :cond_3

    .line 511
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_3
    check-cast v7, Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;

    .line 170
    new-instance v9, Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$DataRow$PendingSettlementRow;

    .line 171
    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->pendingSettlementReport()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    .line 170
    invoke-direct {v9, v6, v7}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$DataRow$PendingSettlementRow;-><init>(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/transferreports/TransferReportsLoader$PendingSettlements;)V

    .line 172
    invoke-interface {v5, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v6, v8

    goto :goto_0

    .line 512
    :cond_4
    check-cast v5, Ljava/util/List;

    .line 174
    check-cast v5, Ljava/util/Collection;

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_5
    if-nez v3, :cond_7

    .line 178
    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getSettlementReportList()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 513
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {v2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 514
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 515
    check-cast v2, Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;

    new-instance v5, Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$DataRow$DepositHistoryRow;

    .line 180
    invoke-direct {v5, v2}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$DataRow$DepositHistoryRow;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$SettlementReport;)V

    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 516
    :cond_6
    check-cast v3, Ljava/util/List;

    .line 183
    sget-object v1, Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$TitleRow$DepositHistoryTitleRow;->INSTANCE:Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$TitleRow$DepositHistoryTitleRow;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    check-cast v3, Ljava/util/Collection;

    invoke-interface {v4, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 191
    :cond_7
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->depositHistoryRecycler:Lcom/squareup/cycler/Recycler;

    if-nez v1, :cond_8

    const-string v2, "depositHistoryRecycler"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 192
    :cond_8
    new-instance v2, Lcom/squareup/transferreports/TransferReportsLayoutRunner$onSuccess$5;

    invoke-direct {v2, v4, v0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$onSuccess$5;-><init>(Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final setCurrentBalanceOnClickListener(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)V
    .locals 1

    .line 445
    new-instance v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setCurrentBalanceOnClickListener$1;

    invoke-direct {v0, p0, p2, p3}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setCurrentBalanceOnClickListener$1;-><init>(Lcom/squareup/transferreports/TransferReportsLayoutRunner;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final setHistoryOnClickListener(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Ljava/lang/String;)V
    .locals 1

    .line 457
    new-instance v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setHistoryOnClickListener$1;

    invoke-direct {v0, p0, p2, p3}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setHistoryOnClickListener$1;-><init>(Lcom/squareup/transferreports/TransferReportsLayoutRunner;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final setUpActionBar()V
    .locals 9

    .line 199
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/transferreports/R$string;->deposits_report:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "res.getText(R.string.deposits_report)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 201
    iget-boolean v2, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->allowBack:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    .line 202
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 203
    invoke-virtual {v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 204
    new-instance v2, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpActionBar$1;

    invoke-direct {v2, p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpActionBar$1;-><init>(Lcom/squareup/transferreports/TransferReportsLayoutRunner;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 206
    invoke-virtual {v1, v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 207
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 209
    :goto_0
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 211
    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->res:Landroid/content/res/Resources;

    sget v4, Lcom/squareup/transferreports/R$string;->deposits_report_help_button:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v4, "res.getText(R.string.deposits_report_help_button)"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/text/style/CharacterStyle;

    const/4 v5, 0x0

    .line 213
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    iget-object v7, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->context:Landroid/content/Context;

    sget v8, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-static {v7, v8}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v7

    invoke-direct {v6, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    check-cast v6, Landroid/text/style/CharacterStyle;

    aput-object v6, v4, v5

    .line 214
    iget-object v5, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    aput-object v5, v4, v3

    .line 212
    invoke-static {v2, v4}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;[Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 210
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 217
    new-instance v2, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpActionBar$2;

    invoke-direct {v2, p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpActionBar$2;-><init>(Lcom/squareup/transferreports/TransferReportsLayoutRunner;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 218
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final setUpHistoryRecycler()V
    .locals 5

    .line 250
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->dataRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 517
    sget-object v2, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 518
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 519
    new-instance v2, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v2}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 523
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 524
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 251
    sget-object v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpHistoryRecycler$1$1;->INSTANCE:Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpHistoryRecycler$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 527
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/transferreports/TransferReportsLayoutRunner$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/transferreports/TransferReportsLayoutRunner$$special$$inlined$row$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 257
    sget v3, Lcom/squareup/transferreports/R$layout;->transfer_reports_title_section:I

    .line 529
    new-instance v4, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpHistoryRecycler$$inlined$adopt$lambda$1;

    invoke-direct {v4, v3, p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpHistoryRecycler$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/transferreports/TransferReportsLayoutRunner;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 527
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 526
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 539
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/transferreports/TransferReportsLayoutRunner$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/transferreports/TransferReportsLayoutRunner$$special$$inlined$row$2;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 274
    sget v3, Lcom/squareup/widgets/pos/R$layout;->smart_line_row:I

    .line 541
    new-instance v4, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpHistoryRecycler$$inlined$adopt$lambda$2;

    invoke-direct {v4, v3, p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpHistoryRecycler$$inlined$adopt$lambda$2;-><init>(ILcom/squareup/transferreports/TransferReportsLayoutRunner;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 293
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 538
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 551
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/transferreports/TransferReportsLayoutRunner$$special$$inlined$extraItem$1;->INSTANCE:Lcom/squareup/transferreports/TransferReportsLayoutRunner$$special$$inlined$extraItem$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 297
    sget v3, Lcom/squareup/transferreports/R$layout;->transfer_reports_load_more_layout:I

    .line 553
    new-instance v4, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpHistoryRecycler$$inlined$adopt$lambda$3;

    invoke-direct {v4, v3, p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpHistoryRecycler$$inlined$adopt$lambda$3;-><init>(ILcom/squareup/transferreports/TransferReportsLayoutRunner;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 551
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 550
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->extraItem(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 562
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/4 v3, 0x0

    .line 303
    invoke-virtual {v0, v3}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    .line 304
    sget v3, Lcom/squareup/transferreports/R$style;->DepositsRecyclerEdges:I

    invoke-virtual {v0, v3}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefStyleRes(I)V

    .line 305
    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    .line 562
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 521
    invoke-virtual {v2, v1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->depositHistoryRecycler:Lcom/squareup/cycler/Recycler;

    .line 309
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->depositHistoryRecycler:Lcom/squareup/cycler/Recycler;

    if-nez v0, :cond_0

    const-string v1, "depositHistoryRecycler"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    new-instance v1, Landroidx/recyclerview/widget/DefaultItemAnimator;

    invoke-direct {v1}, Landroidx/recyclerview/widget/DefaultItemAnimator;-><init>()V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    return-void

    .line 518
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final setUpSwipeRefresh()V
    .locals 2

    .line 222
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->swipeRefresh:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    new-instance v1, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpSwipeRefresh$1;

    invoke-direct {v1, p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpSwipeRefresh$1;-><init>(Lcom/squareup/transferreports/TransferReportsLayoutRunner;)V

    check-cast v1, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$OnRefreshListener;

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$OnRefreshListener;)V

    return-void
.end method

.method private final showError()V
    .locals 2

    .line 241
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->errorSection:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    return-void
.end method

.method private final showSpinner()V
    .locals 2

    .line 233
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->spinner:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/transferreports/TransferReportsScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsScreen;->getContent()Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;

    move-result-object p1

    if-eqz p1, :cond_0

    move-object p2, p0

    check-cast p2, Lcom/squareup/transferreports/TransferReportsLayoutRunner;

    invoke-direct {p2, p1}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->onScreen(Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 79
    check-cast p1, Lcom/squareup/transferreports/TransferReportsScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->showRendering(Lcom/squareup/transferreports/TransferReportsScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
