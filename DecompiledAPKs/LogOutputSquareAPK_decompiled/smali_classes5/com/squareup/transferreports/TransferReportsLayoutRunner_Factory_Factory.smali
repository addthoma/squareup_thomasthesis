.class public final Lcom/squareup/transferreports/TransferReportsLayoutRunner_Factory_Factory;
.super Ljava/lang/Object;
.source "TransferReportsLayoutRunner_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final recyclerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final shortDateFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner_Factory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner_Factory_Factory;->shortDateFormatterProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner_Factory_Factory;->recyclerFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/transferreports/TransferReportsLayoutRunner_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;)",
            "Lcom/squareup/transferreports/TransferReportsLayoutRunner_Factory_Factory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner_Factory_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsLayoutRunner_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/recycler/RecyclerFactory;)Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ")",
            "Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;-><init>(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/recycler/RecyclerFactory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner_Factory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner_Factory_Factory;->shortDateFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner_Factory_Factory;->recyclerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/recycler/RecyclerFactory;

    invoke-static {v0, v1, v2}, Lcom/squareup/transferreports/TransferReportsLayoutRunner_Factory_Factory;->newInstance(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/recycler/RecyclerFactory;)Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner_Factory_Factory;->get()Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;

    move-result-object v0

    return-object v0
.end method
