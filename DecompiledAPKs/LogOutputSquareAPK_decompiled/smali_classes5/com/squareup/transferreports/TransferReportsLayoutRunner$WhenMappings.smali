.class public final synthetic Lcom/squareup/transferreports/TransferReportsLayoutRunner$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/squareup/transferreports/TransferReportsLoader$State;->values()[Lcom/squareup/transferreports/TransferReportsLoader$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$State;->LOADING:Lcom/squareup/transferreports/TransferReportsLoader$State;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$State;->SUCCESS:Lcom/squareup/transferreports/TransferReportsLoader$State;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$State;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$State;->FAILURE:Lcom/squareup/transferreports/TransferReportsLoader$State;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$State;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/settlements/SettlementType;->values()[Lcom/squareup/protos/client/settlements/SettlementType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/settlements/SettlementType;->ZERO_AMOUNT_SETTLEMENT:Lcom/squareup/protos/client/settlements/SettlementType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/settlements/SettlementType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/settlements/SettlementType;->STANDARD:Lcom/squareup/protos/client/settlements/SettlementType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/settlements/SettlementType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/settlements/SettlementType;->SCHEDULED_INSTANT:Lcom/squareup/protos/client/settlements/SettlementType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/settlements/SettlementType;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/settlements/SettlementType;->MANUAL_INSTANT:Lcom/squareup/protos/client/settlements/SettlementType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/settlements/SettlementType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/settlements/SettlementType;->DEBIT_CARD_PAY_IN:Lcom/squareup/protos/client/settlements/SettlementType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/settlements/SettlementType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    return-void
.end method
