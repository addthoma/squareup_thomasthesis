.class public final Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;
.super Ljava/lang/Object;
.source "TransferReportsDetailLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RowData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0018\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000e\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\n\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eR\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R\u001a\u0010\u000c\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\"\u0004\u0008\u0019\u0010\u001aR\"\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\"\u0004\u0008\u001d\u0010\u001eR\u001a\u0010\u000b\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001f\u0010\u0010\"\u0004\u0008 \u0010\u0012R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008!\u0010\"\"\u0004\u0008#\u0010$\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;",
        "",
        "type",
        "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;",
        "createdAt",
        "Lcom/squareup/protos/common/time/DateTime;",
        "itemization",
        "",
        "Lcom/squareup/protos/client/bills/Itemization;",
        "collected",
        "Lcom/squareup/protos/common/Money;",
        "netTotal",
        "hasSplitTender",
        "",
        "(Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;Lcom/squareup/protos/common/time/DateTime;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Z)V",
        "getCollected",
        "()Lcom/squareup/protos/common/Money;",
        "setCollected",
        "(Lcom/squareup/protos/common/Money;)V",
        "getCreatedAt",
        "()Lcom/squareup/protos/common/time/DateTime;",
        "setCreatedAt",
        "(Lcom/squareup/protos/common/time/DateTime;)V",
        "getHasSplitTender",
        "()Z",
        "setHasSplitTender",
        "(Z)V",
        "getItemization",
        "()Ljava/util/List;",
        "setItemization",
        "(Ljava/util/List;)V",
        "getNetTotal",
        "setNetTotal",
        "getType",
        "()Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;",
        "setType",
        "(Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;)V",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private collected:Lcom/squareup/protos/common/Money;

.field private createdAt:Lcom/squareup/protos/common/time/DateTime;

.field private hasSplitTender:Z

.field private itemization:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;"
        }
    .end annotation
.end field

.field private netTotal:Lcom/squareup/protos/common/Money;

.field private type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;Lcom/squareup/protos/common/time/DateTime;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Z)V"
        }
    .end annotation

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "createdAt"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "collected"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "netTotal"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 961
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->createdAt:Lcom/squareup/protos/common/time/DateTime;

    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->itemization:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->collected:Lcom/squareup/protos/common/Money;

    iput-object p5, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->netTotal:Lcom/squareup/protos/common/Money;

    iput-boolean p6, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->hasSplitTender:Z

    return-void
.end method


# virtual methods
.method public final getCollected()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 965
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->collected:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getCreatedAt()Lcom/squareup/protos/common/time/DateTime;
    .locals 1

    .line 963
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->createdAt:Lcom/squareup/protos/common/time/DateTime;

    return-object v0
.end method

.method public final getHasSplitTender()Z
    .locals 1

    .line 967
    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->hasSplitTender:Z

    return v0
.end method

.method public final getItemization()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;"
        }
    .end annotation

    .line 964
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->itemization:Ljava/util/List;

    return-object v0
.end method

.method public final getNetTotal()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 966
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->netTotal:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getType()Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;
    .locals 1

    .line 962
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    return-object v0
.end method

.method public final setCollected(Lcom/squareup/protos/common/Money;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 965
    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->collected:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public final setCreatedAt(Lcom/squareup/protos/common/time/DateTime;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 963
    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->createdAt:Lcom/squareup/protos/common/time/DateTime;

    return-void
.end method

.method public final setHasSplitTender(Z)V
    .locals 0

    .line 967
    iput-boolean p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->hasSplitTender:Z

    return-void
.end method

.method public final setItemization(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;)V"
        }
    .end annotation

    .line 964
    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->itemization:Ljava/util/List;

    return-void
.end method

.method public final setNetTotal(Lcom/squareup/protos/common/Money;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 966
    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->netTotal:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public final setType(Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 962
    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    return-void
.end method
