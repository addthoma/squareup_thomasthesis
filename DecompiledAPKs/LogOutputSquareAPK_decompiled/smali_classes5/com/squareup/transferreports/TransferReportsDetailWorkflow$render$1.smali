.class final Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "TransferReportsDetailWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->render(Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;Lcom/squareup/transferreports/TransferReportsDetailState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/transferreports/TransferReportsDetailState;",
        "+",
        "Lkotlin/Unit;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/transferreports/TransferReportsDetailState;",
        "",
        "reportsSnapshot",
        "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$1;

    invoke-direct {v0}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$1;-><init>()V

    sput-object v0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$1;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/transferreports/TransferReportsDetailState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "reportsSnapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getDepositDetailsState()Lcom/squareup/transferreports/TransferReportsLoader$State;

    move-result-object v0

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$State;->LOADING:Lcom/squareup/transferreports/TransferReportsLoader$State;

    if-ne v0, v1, :cond_0

    .line 65
    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$LoadBillEntries;

    invoke-direct {v0, p1}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$LoadBillEntries;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 67
    :cond_0
    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$ShowTransferReportsDetail;

    invoke-direct {v0, p1}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$ShowTransferReportsDetail;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    invoke-virtual {p0, p1}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$1;->invoke(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
