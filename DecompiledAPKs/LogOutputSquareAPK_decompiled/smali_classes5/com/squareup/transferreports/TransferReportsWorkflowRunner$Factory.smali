.class public final Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Factory;
.super Ljava/lang/Object;
.source "TransferReportsWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/TransferReportsWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0014\u0010\t\u001a\u00020\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u000cR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Factory;",
        "",
        "viewFactoryFactory",
        "Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;",
        "workflow",
        "Lcom/squareup/transferreports/TransferReportsWorkflow;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "(Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;Lcom/squareup/transferreports/TransferReportsWorkflow;Lcom/squareup/ui/main/PosContainer;)V",
        "create",
        "Lcom/squareup/transferreports/TransferReportsWorkflowRunner;",
        "section",
        "Ljava/lang/Class;",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final viewFactoryFactory:Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;

.field private final workflow:Lcom/squareup/transferreports/TransferReportsWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;Lcom/squareup/transferreports/TransferReportsWorkflow;Lcom/squareup/ui/main/PosContainer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "viewFactoryFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Factory;->viewFactoryFactory:Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Factory;->workflow:Lcom/squareup/transferreports/TransferReportsWorkflow;

    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Factory;->container:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Class;)Lcom/squareup/transferreports/TransferReportsWorkflowRunner;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)",
            "Lcom/squareup/transferreports/TransferReportsWorkflowRunner;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;

    .line 36
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Factory;->viewFactoryFactory:Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;

    invoke-virtual {v1, p1}, Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;->create(Ljava/lang/Class;)Lcom/squareup/transferreports/TransferReportsViewFactory;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Factory;->workflow:Lcom/squareup/transferreports/TransferReportsWorkflow;

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Factory;->container:Lcom/squareup/ui/main/PosContainer;

    .line 35
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;-><init>(Lcom/squareup/transferreports/TransferReportsViewFactory;Lcom/squareup/transferreports/TransferReportsWorkflow;Lcom/squareup/ui/main/PosContainer;)V

    return-object v0
.end method
