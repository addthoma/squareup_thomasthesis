.class public final Lcom/squareup/securetouch/SecureKey$Companion;
.super Ljava/lang/Object;
.source "SecureTouchFeature.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/SecureKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSecureTouchFeature.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SecureTouchFeature.kt\ncom/squareup/securetouch/SecureKey$Companion\n*L\n1#1,180:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureKey$Companion;",
        "",
        "()V",
        "forKeypadIndex",
        "Lcom/squareup/securetouch/SecureKey;",
        "index",
        "",
        "secure-touch-feature_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/securetouch/SecureKey$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final forKeypadIndex(I)Lcom/squareup/securetouch/SecureKey;
    .locals 2

    packed-switch p1, :pswitch_data_0

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Keypad index "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " does not correspond to a secure touch key!"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 43
    :pswitch_0
    sget-object p1, Lcom/squareup/securetouch/SecureKey;->Delete:Lcom/squareup/securetouch/SecureKey;

    goto :goto_0

    .line 42
    :pswitch_1
    sget-object p1, Lcom/squareup/securetouch/SecureKey;->Zero:Lcom/squareup/securetouch/SecureKey;

    goto :goto_0

    .line 41
    :pswitch_2
    sget-object p1, Lcom/squareup/securetouch/SecureKey;->EmptySpace:Lcom/squareup/securetouch/SecureKey;

    goto :goto_0

    .line 40
    :pswitch_3
    sget-object p1, Lcom/squareup/securetouch/SecureKey;->Nine:Lcom/squareup/securetouch/SecureKey;

    goto :goto_0

    .line 39
    :pswitch_4
    sget-object p1, Lcom/squareup/securetouch/SecureKey;->Eight:Lcom/squareup/securetouch/SecureKey;

    goto :goto_0

    .line 38
    :pswitch_5
    sget-object p1, Lcom/squareup/securetouch/SecureKey;->Seven:Lcom/squareup/securetouch/SecureKey;

    goto :goto_0

    .line 37
    :pswitch_6
    sget-object p1, Lcom/squareup/securetouch/SecureKey;->Six:Lcom/squareup/securetouch/SecureKey;

    goto :goto_0

    .line 36
    :pswitch_7
    sget-object p1, Lcom/squareup/securetouch/SecureKey;->Five:Lcom/squareup/securetouch/SecureKey;

    goto :goto_0

    .line 35
    :pswitch_8
    sget-object p1, Lcom/squareup/securetouch/SecureKey;->Four:Lcom/squareup/securetouch/SecureKey;

    goto :goto_0

    .line 34
    :pswitch_9
    sget-object p1, Lcom/squareup/securetouch/SecureKey;->Three:Lcom/squareup/securetouch/SecureKey;

    goto :goto_0

    .line 33
    :pswitch_a
    sget-object p1, Lcom/squareup/securetouch/SecureKey;->Two:Lcom/squareup/securetouch/SecureKey;

    goto :goto_0

    .line 32
    :pswitch_b
    sget-object p1, Lcom/squareup/securetouch/SecureKey;->One:Lcom/squareup/securetouch/SecureKey;

    :goto_0
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
