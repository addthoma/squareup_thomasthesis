.class public final Lcom/squareup/securetouch/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final TextAppearance_SecureTouchModeSelection_Text:I = 0x7f13029b

.field public static final TextAppearance_SecureTouchModeSelection_Text_AmountDue:I = 0x7f13029c

.field public static final Widget_Noho_Squid_SecureKeypad:I = 0x7f13058f

.field public static final Widget_Noho_Squid_SecureKeypad_Button:I = 0x7f130590

.field public static final Widget_Noho_Squid_SecureKeypad_Button_Clear:I = 0x7f130591

.field public static final Widget_Noho_Squid_SecureKeypad_Button_Done:I = 0x7f130592

.field public static final Widget_Noho_Squid_SecureKeypad_Divider:I = 0x7f130593

.field public static final Widget_Noho_Squid_SecureKeypad_Divider_Horizontal:I = 0x7f130594

.field public static final Widget_Noho_Squid_SecureKeypad_Divider_Vertical:I = 0x7f130595

.field public static final Widget_Noho_Squid_SecureKeypad_Key:I = 0x7f130596

.field public static final Widget_Noho_Squid_SecureKeypad_Key_Icon:I = 0x7f130597

.field public static final Widget_Noho_Squid_SecureKeypad_Key_SoloText:I = 0x7f130598

.field public static final Widget_Noho_Squid_SecureKeypad_Key_Subtitle:I = 0x7f130599

.field public static final Widget_Noho_Squid_SecureKeypad_Key_Title:I = 0x7f13059a

.field public static final Widget_Noho_Squid_SecureKeypad_ModeSelection:I = 0x7f13059b

.field public static final Widget_Noho_Squid_SecureKeypad_ModeSelection_Button:I = 0x7f13059c

.field public static final Widget_Noho_Squid_SecureKeypad_ModeSelection_Button_Negative:I = 0x7f13059d

.field public static final Widget_Noho_Squid_SecureKeypad_ModeSelection_Button_Positive:I = 0x7f13059e

.field public static final Widget_Noho_Squid_SecureKeypad_ModeSelection_DisplayAmount:I = 0x7f13059f

.field public static final Widget_Noho_Squid_SecureKeypad_ModeSelection_Title:I = 0x7f1305a0

.field public static final Widget_Noho_Squid_SecureKeypad_StarGroup:I = 0x7f1305a1


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
