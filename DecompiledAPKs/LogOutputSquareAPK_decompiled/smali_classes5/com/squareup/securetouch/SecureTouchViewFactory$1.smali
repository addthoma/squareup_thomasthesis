.class public final Lcom/squareup/securetouch/SecureTouchViewFactory$1;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "SecureTouchViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/SecureTouchViewFactory;-><init>(Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;Lcom/squareup/securetouch/EnableAccessibleKeypadDialogFactory$Factory;Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$Factory;Lcom/squareup/workflow/CompoundWorkflowViewFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001\u00a8\u0006\u0002"
    }
    d2 = {
        "com/squareup/securetouch/SecureTouchViewFactory$1",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $enableAccessibleKeypadDialogFactory:Lcom/squareup/securetouch/EnableAccessibleKeypadDialogFactory$Factory;

.field final synthetic $secureTouchKeypadFactory:Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;

.field final synthetic $secureTouchModeSelectionCoordinatorFactory:Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$Factory;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$Factory;Lcom/squareup/securetouch/EnableAccessibleKeypadDialogFactory$Factory;[Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V
    .locals 0

    .line 16
    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchViewFactory$1;->$secureTouchKeypadFactory:Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;

    iput-object p2, p0, Lcom/squareup/securetouch/SecureTouchViewFactory$1;->$secureTouchModeSelectionCoordinatorFactory:Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$Factory;

    iput-object p3, p0, Lcom/squareup/securetouch/SecureTouchViewFactory$1;->$enableAccessibleKeypadDialogFactory:Lcom/squareup/securetouch/EnableAccessibleKeypadDialogFactory$Factory;

    invoke-direct {p0, p4}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
