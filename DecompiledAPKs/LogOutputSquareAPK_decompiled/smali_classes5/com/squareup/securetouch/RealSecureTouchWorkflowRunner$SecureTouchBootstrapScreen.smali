.class public final Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "RealSecureTouchWorkflowRunner.kt"


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SecureTouchBootstrapScreen"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSecureTouchWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSecureTouchWorkflowRunner.kt\ncom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchBootstrapScreen\n*L\n1#1,120:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "parentScope",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "secureTouchInput",
        "Lcom/squareup/securetouch/SecureTouchInput;",
        "(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/securetouch/SecureTouchInput;)V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope;",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final parentScope:Lcom/squareup/ui/main/RegisterTreeKey;

.field private final secureTouchInput:Lcom/squareup/securetouch/SecureTouchInput;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/securetouch/SecureTouchInput;)V
    .locals 1

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "secureTouchInput"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchBootstrapScreen;->parentScope:Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object p2, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchBootstrapScreen;->secureTouchInput:Lcom/squareup/securetouch/SecureTouchInput;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    sget-object v0, Lcom/squareup/securetouch/SecureTouchWorkflowRunner;->Companion:Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/securetouch/SecureTouchWorkflowRunner;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 112
    check-cast p1, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;

    .line 113
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchBootstrapScreen;->secureTouchInput:Lcom/squareup/securetouch/SecureTouchInput;

    invoke-static {p1, v0}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->access$onInput(Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;Lcom/squareup/securetouch/SecureTouchInput;)V

    .line 114
    invoke-static {p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->access$ensureWorkflow(Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;)V

    return-void

    .line 112
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.securetouch.RealSecureTouchWorkflowRunner"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getParentKey()Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope;
    .locals 2

    .line 107
    new-instance v0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope;

    iget-object v1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchBootstrapScreen;->parentScope:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v0, v1}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchBootstrapScreen;->getParentKey()Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope;

    move-result-object v0

    return-object v0
.end method
