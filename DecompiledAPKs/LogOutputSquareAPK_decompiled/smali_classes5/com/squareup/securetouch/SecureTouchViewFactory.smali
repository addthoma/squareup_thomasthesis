.class public final Lcom/squareup/securetouch/SecureTouchViewFactory;
.super Lcom/squareup/workflow/CompoundWorkflowViewFactory;
.source "SecureTouchViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B+\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\n\u0010\u0008\u001a\u00060\u0001j\u0002`\t\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchViewFactory;",
        "Lcom/squareup/workflow/CompoundWorkflowViewFactory;",
        "secureTouchKeypadFactory",
        "Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;",
        "enableAccessibleKeypadDialogFactory",
        "Lcom/squareup/securetouch/EnableAccessibleKeypadDialogFactory$Factory;",
        "secureTouchModeSelectionCoordinatorFactory",
        "Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$Factory;",
        "secureTouchAccessibilityViewFactory",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityViewFactory;",
        "(Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;Lcom/squareup/securetouch/EnableAccessibleKeypadDialogFactory$Factory;Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$Factory;Lcom/squareup/workflow/CompoundWorkflowViewFactory;)V",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;Lcom/squareup/securetouch/EnableAccessibleKeypadDialogFactory$Factory;Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$Factory;Lcom/squareup/workflow/CompoundWorkflowViewFactory;)V
    .locals 17
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    const-string v4, "secureTouchKeypadFactory"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "enableAccessibleKeypadDialogFactory"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "secureTouchModeSelectionCoordinatorFactory"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "secureTouchAccessibilityViewFactory"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x2

    new-array v5, v4, [Lcom/squareup/workflow/WorkflowViewFactory;

    .line 15
    check-cast v3, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    .line 16
    new-instance v3, Lcom/squareup/securetouch/SecureTouchViewFactory$1;

    const/4 v7, 0x3

    new-array v7, v7, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 17
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 18
    sget-object v9, Lcom/squareup/securetouch/SecureTouchScreen;->Companion:Lcom/squareup/securetouch/SecureTouchScreen$Companion;

    invoke-virtual {v9}, Lcom/squareup/securetouch/SecureTouchScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 19
    sget v10, Lcom/squareup/securetouch/R$layout;->keypad_view_container:I

    .line 20
    new-instance v11, Lcom/squareup/securetouch/SecureTouchViewFactory$2;

    invoke-direct {v11, v0}, Lcom/squareup/securetouch/SecureTouchViewFactory$2;-><init>(Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;)V

    move-object v13, v11

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v14, 0xc

    const/4 v15, 0x0

    .line 17
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v8

    aput-object v8, v7, v6

    .line 22
    sget-object v9, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 23
    sget-object v6, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->Companion:Lcom/squareup/securetouch/SecureTouchModeSelectionScreen$Companion;

    invoke-virtual {v6}, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v10

    .line 24
    sget v11, Lcom/squareup/securetouch/R$layout;->keypad_mode_selection:I

    .line 25
    new-instance v6, Lcom/squareup/securetouch/SecureTouchViewFactory$3;

    invoke-direct {v6, v2}, Lcom/squareup/securetouch/SecureTouchViewFactory$3;-><init>(Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$Factory;)V

    move-object v14, v6

    check-cast v14, Lkotlin/jvm/functions/Function1;

    const/4 v13, 0x0

    const/16 v15, 0xc

    const/16 v16, 0x0

    .line 22
    invoke-static/range {v9 .. v16}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v6

    const/4 v8, 0x1

    aput-object v6, v7, v8

    .line 27
    sget-object v6, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 28
    sget-object v9, Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen;->Companion:Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen$Companion;

    invoke-virtual {v9}, Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 29
    new-instance v10, Lcom/squareup/securetouch/SecureTouchViewFactory$4;

    invoke-direct {v10, v1}, Lcom/squareup/securetouch/SecureTouchViewFactory$4;-><init>(Lcom/squareup/securetouch/EnableAccessibleKeypadDialogFactory$Factory;)V

    check-cast v10, Lkotlin/jvm/functions/Function1;

    .line 27
    invoke-virtual {v6, v9, v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v6

    aput-object v6, v7, v4

    invoke-direct {v3, v0, v2, v1, v7}, Lcom/squareup/securetouch/SecureTouchViewFactory$1;-><init>(Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$Factory;Lcom/squareup/securetouch/EnableAccessibleKeypadDialogFactory$Factory;[Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    check-cast v3, Lcom/squareup/workflow/WorkflowViewFactory;

    aput-object v3, v5, v8

    move-object/from16 v0, p0

    .line 14
    invoke-direct {v0, v5}, Lcom/squareup/workflow/CompoundWorkflowViewFactory;-><init>([Lcom/squareup/workflow/WorkflowViewFactory;)V

    return-void
.end method
