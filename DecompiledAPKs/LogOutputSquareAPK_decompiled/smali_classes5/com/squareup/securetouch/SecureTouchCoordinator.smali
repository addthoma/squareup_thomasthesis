.class public final Lcom/squareup/securetouch/SecureTouchCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SecureTouchCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSecureTouchCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SecureTouchCoordinator.kt\ncom/squareup/securetouch/SecureTouchCoordinator\n*L\n1#1,275:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a2\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001<BA\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0010\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020\u0012H\u0016J\u0008\u0010*\u001a\u00020(H\u0002J\u0008\u0010+\u001a\u00020(H\u0002J\u000e\u0010,\u001a\u0008\u0012\u0004\u0012\u00020-0\u0003H\u0002J\u0018\u0010.\u001a\u00020(2\u0006\u0010/\u001a\u0002002\u0006\u00101\u001a\u000202H\u0002J\u0010\u00103\u001a\u0002042\u0006\u00105\u001a\u00020\u0005H\u0002J\u0018\u00106\u001a\u00020(2\u0006\u00105\u001a\u00020\u00052\u0006\u00107\u001a\u000202H\u0002J\u000c\u00108\u001a\u00020(*\u00020\u0012H\u0002J\u0016\u00109\u001a\u00020(*\u00020\u00122\u0008\u0008\u0002\u0010:\u001a\u00020;H\u0002R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R4\u0010\u0018\u001a(\u0012$\u0012\"\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u001c \u001d*\u0010\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u001c\u0018\u00010\u001a0\u001a0\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082.\u00a2\u0006\u0002\n\u0000R\u0018\u0010$\u001a\u00020\u001c*\u00020\u00128BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008%\u0010&\u00a8\u0006="
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/securetouch/SecureTouchScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/settings/server/Features;)V",
        "accessibilityButton",
        "Landroid/widget/ImageView;",
        "cancelButton",
        "Landroid/view/View;",
        "doneButton",
        "keypadKeys",
        "Lcom/squareup/securetouch/Keypad;",
        "keypadTitle",
        "Landroid/widget/TextView;",
        "onKeypadKeysLayoutChanged",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "Lcom/squareup/securetouch/SecureKey;",
        "Lcom/squareup/securetouch/SecureTouchRect;",
        "kotlin.jvm.PlatformType",
        "onRootLayoutChanged",
        "Lcom/squareup/securetouch/ButtonRects;",
        "rootLayout",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "starGroup",
        "Lcom/squareup/ui/StarGroup;",
        "locationOnScreen",
        "getLocationOnScreen",
        "(Landroid/view/View;)Lcom/squareup/securetouch/SecureTouchRect;",
        "attach",
        "",
        "view",
        "attachKeypadKeysLayoutListener",
        "attachRootLayoutListener",
        "onKeypadActivated",
        "Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;",
        "setTitle",
        "data",
        "Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;",
        "res",
        "Lcom/squareup/util/Res;",
        "spinnerState",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "screen",
        "updateView",
        "buyerRes",
        "bindChildViews",
        "reinflateContainerChild",
        "useHighContrastMode",
        "",
        "Factory",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private accessibilityButton:Landroid/widget/ImageView;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private cancelButton:Landroid/view/View;

.field private doneButton:Landroid/view/View;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private keypadKeys:Lcom/squareup/securetouch/Keypad;

.field private keypadTitle:Landroid/widget/TextView;

.field private final onKeypadKeysLayoutChanged:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/util/Map<",
            "Lcom/squareup/securetouch/SecureKey;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ">;>;"
        }
    .end annotation
.end field

.field private final onRootLayoutChanged:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/securetouch/ButtonRects;",
            ">;"
        }
    .end annotation
.end field

.field private rootLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private starGroup:Lcom/squareup/ui/StarGroup;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLocaleOverride"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassSpinner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p3, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iput-object p4, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->features:Lcom/squareup/settings/server/Features;

    .line 68
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<Map<\u2026eKey, SecureTouchRect>>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->onKeypadKeysLayoutChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 69
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->onRootLayoutChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$getAccessibilityButton$p(Lcom/squareup/securetouch/SecureTouchCoordinator;)Landroid/widget/ImageView;
    .locals 1

    .line 43
    iget-object p0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->accessibilityButton:Landroid/widget/ImageView;

    if-nez p0, :cond_0

    const-string v0, "accessibilityButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getCancelButton$p(Lcom/squareup/securetouch/SecureTouchCoordinator;)Landroid/view/View;
    .locals 1

    .line 43
    iget-object p0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->cancelButton:Landroid/view/View;

    if-nez p0, :cond_0

    const-string v0, "cancelButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getDoneButton$p(Lcom/squareup/securetouch/SecureTouchCoordinator;)Landroid/view/View;
    .locals 1

    .line 43
    iget-object p0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->doneButton:Landroid/view/View;

    if-nez p0, :cond_0

    const-string v0, "doneButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getKeypadKeys$p(Lcom/squareup/securetouch/SecureTouchCoordinator;)Lcom/squareup/securetouch/Keypad;
    .locals 1

    .line 43
    iget-object p0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->keypadKeys:Lcom/squareup/securetouch/Keypad;

    if-nez p0, :cond_0

    const-string v0, "keypadKeys"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getLocationOnScreen$p(Lcom/squareup/securetouch/SecureTouchCoordinator;Landroid/view/View;)Lcom/squareup/securetouch/SecureTouchRect;
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/securetouch/SecureTouchCoordinator;->getLocationOnScreen(Landroid/view/View;)Lcom/squareup/securetouch/SecureTouchRect;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getOnKeypadKeysLayoutChanged$p(Lcom/squareup/securetouch/SecureTouchCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->onKeypadKeysLayoutChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$getOnRootLayoutChanged$p(Lcom/squareup/securetouch/SecureTouchCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->onRootLayoutChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$reinflateContainerChild(Lcom/squareup/securetouch/SecureTouchCoordinator;Landroid/view/View;Z)V
    .locals 0

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/squareup/securetouch/SecureTouchCoordinator;->reinflateContainerChild(Landroid/view/View;Z)V

    return-void
.end method

.method public static final synthetic access$setAccessibilityButton$p(Lcom/squareup/securetouch/SecureTouchCoordinator;Landroid/widget/ImageView;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->accessibilityButton:Landroid/widget/ImageView;

    return-void
.end method

.method public static final synthetic access$setCancelButton$p(Lcom/squareup/securetouch/SecureTouchCoordinator;Landroid/view/View;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->cancelButton:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$setDoneButton$p(Lcom/squareup/securetouch/SecureTouchCoordinator;Landroid/view/View;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->doneButton:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$setKeypadKeys$p(Lcom/squareup/securetouch/SecureTouchCoordinator;Lcom/squareup/securetouch/Keypad;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->keypadKeys:Lcom/squareup/securetouch/Keypad;

    return-void
.end method

.method public static final synthetic access$spinnerState(Lcom/squareup/securetouch/SecureTouchCoordinator;Lcom/squareup/securetouch/SecureTouchScreen;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/securetouch/SecureTouchCoordinator;->spinnerState(Lcom/squareup/securetouch/SecureTouchScreen;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateView(Lcom/squareup/securetouch/SecureTouchCoordinator;Lcom/squareup/securetouch/SecureTouchScreen;Lcom/squareup/util/Res;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/squareup/securetouch/SecureTouchCoordinator;->updateView(Lcom/squareup/securetouch/SecureTouchScreen;Lcom/squareup/util/Res;)V

    return-void
.end method

.method private final attachKeypadKeysLayoutListener()V
    .locals 2

    .line 151
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->keypadKeys:Lcom/squareup/securetouch/Keypad;

    if-nez v0, :cond_0

    const-string v1, "keypadKeys"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/securetouch/SecureTouchCoordinator$attachKeypadKeysLayoutListener$1;

    invoke-direct {v1, p0}, Lcom/squareup/securetouch/SecureTouchCoordinator$attachKeypadKeysLayoutListener$1;-><init>(Lcom/squareup/securetouch/SecureTouchCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final attachRootLayoutListener()V
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->rootLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v0, :cond_0

    const-string v1, "rootLayout"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/securetouch/SecureTouchCoordinator$attachRootLayoutListener$1;

    invoke-direct {v1, p0}, Lcom/squareup/securetouch/SecureTouchCoordinator$attachRootLayoutListener$1;-><init>(Lcom/squareup/securetouch/SecureTouchCoordinator;)V

    check-cast v1, Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroidx/constraintlayout/widget/ConstraintLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    return-void
.end method

.method private final bindChildViews(Landroid/view/View;)V
    .locals 2

    .line 191
    sget v0, Lcom/squareup/securetouch/R$id;->keypad_screen_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.keypad_screen_view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    iput-object v0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->rootLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 192
    sget v0, Lcom/squareup/securetouch/R$id;->cancel_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->cancelButton:Landroid/view/View;

    .line 193
    sget v0, Lcom/squareup/securetouch/R$id;->accessibility_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->accessibilityButton:Landroid/widget/ImageView;

    .line 194
    sget v0, Lcom/squareup/securetouch/R$id;->keypad_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->keypadTitle:Landroid/widget/TextView;

    .line 195
    sget v0, Lcom/squareup/securetouch/R$id;->star_group:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/StarGroup;

    iput-object v0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    .line 196
    sget v0, Lcom/squareup/securetouch/R$id;->keypad_keys:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/securetouch/Keypad;

    iput-object v0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->keypadKeys:Lcom/squareup/securetouch/Keypad;

    .line 197
    sget v0, Lcom/squareup/securetouch/R$id;->done_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->doneButton:Landroid/view/View;

    return-void
.end method

.method private final getLocationOnScreen(Landroid/view/View;)Lcom/squareup/securetouch/SecureTouchRect;
    .locals 5

    .line 72
    new-instance v0, Lcom/squareup/securetouch/SecureTouchRect;

    .line 73
    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 74
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v2

    float-to-int v2, v2

    .line 75
    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    .line 76
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    add-int/2addr v4, p1

    .line 72
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/securetouch/SecureTouchRect;-><init>(IIII)V

    return-object v0
.end method

.method private final onKeypadActivated()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;",
            ">;"
        }
    .end annotation

    .line 158
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 159
    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->onKeypadKeysLayoutChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v1, Lio/reactivex/Observable;

    .line 160
    iget-object v2, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->onRootLayoutChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "onRootLayoutChanged.distinctUntilChanged()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 161
    sget-object v1, Lcom/squareup/securetouch/SecureTouchCoordinator$onKeypadActivated$1;->INSTANCE:Lcom/squareup/securetouch/SecureTouchCoordinator$onKeypadActivated$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "combineLatest(\n        o\u2026eButtonRect\n      )\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final reinflateContainerChild(Landroid/view/View;Z)V
    .locals 2

    const-string v0, "context"

    if-eqz p2, :cond_0

    .line 123
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/squareup/noho/NightModeUtilsKt;->nohoNightMode(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p2

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 124
    :goto_0
    sget v1, Lcom/squareup/securetouch/R$id;->keypad_view_container:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 125
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 126
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/securetouch/R$layout;->keypad_view:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->inflate(Landroid/content/Context;I)Landroid/view/View;

    move-result-object p2

    invoke-virtual {v1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 129
    invoke-direct {p0, p1}, Lcom/squareup/securetouch/SecureTouchCoordinator;->bindChildViews(Landroid/view/View;)V

    .line 130
    invoke-direct {p0}, Lcom/squareup/securetouch/SecureTouchCoordinator;->attachRootLayoutListener()V

    .line 131
    invoke-direct {p0}, Lcom/squareup/securetouch/SecureTouchCoordinator;->attachKeypadKeysLayoutListener()V

    return-void
.end method

.method static synthetic reinflateContainerChild$default(Lcom/squareup/securetouch/SecureTouchCoordinator;Landroid/view/View;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 122
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/securetouch/SecureTouchCoordinator;->reinflateContainerChild(Landroid/view/View;Z)V

    return-void
.end method

.method private final setTitle(Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;Lcom/squareup/util/Res;)V
    .locals 6

    .line 204
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->getDigitsEntered()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x4

    const-string v3, "keypadTitle"

    const-string v4, "starGroup"

    if-lez v0, :cond_4

    .line 205
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->getDigitsEntered()I

    move-result p1

    const/16 p2, 0xc

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 206
    iget-object p2, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    if-nez p2, :cond_0

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p2, p1}, Lcom/squareup/ui/StarGroup;->setExpectedStarCount(I)V

    .line 207
    iget-object p2, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    if-nez p2, :cond_1

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p2, p1}, Lcom/squareup/ui/StarGroup;->setStarCount(I)V

    .line 209
    iget-object p1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->keypadTitle:Landroid/widget/TextView;

    if-nez p1, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 210
    iget-object p1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    if-nez p1, :cond_3

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1, v1}, Lcom/squareup/ui/StarGroup;->setVisibility(I)V

    goto :goto_1

    .line 212
    :cond_4
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->keypadTitle:Landroid/widget/TextView;

    if-nez v0, :cond_5

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object p1

    .line 213
    sget-object v5, Lcom/squareup/securetouch/PinFirstTry;->INSTANCE:Lcom/squareup/securetouch/PinFirstTry;

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    sget p1, Lcom/squareup/securetouch/R$string;->keypad_enter_your_pin:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_0

    .line 214
    :cond_6
    sget-object v5, Lcom/squareup/securetouch/PinRetry;->INSTANCE:Lcom/squareup/securetouch/PinRetry;

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    sget p1, Lcom/squareup/securetouch/R$string;->keypad_incorrect_pin:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_0

    .line 215
    :cond_7
    sget-object v5, Lcom/squareup/securetouch/PinLastTry;->INSTANCE:Lcom/squareup/securetouch/PinLastTry;

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_a

    sget p1, Lcom/squareup/securetouch/R$string;->keypad_last_retry:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 212
    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object p1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    if-nez p1, :cond_8

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p1, v2}, Lcom/squareup/ui/StarGroup;->setVisibility(I)V

    .line 219
    iget-object p1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->keypadTitle:Landroid/widget/TextView;

    if-nez p1, :cond_9

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-void

    .line 215
    :cond_a
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final spinnerState(Lcom/squareup/securetouch/SecureTouchScreen;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 4

    .line 172
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchScreen;->getScreenData()Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->getWaitingForReader()Z

    move-result p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, p1, v1, v2, v3}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method private final updateView(Lcom/squareup/securetouch/SecureTouchScreen;Lcom/squareup/util/Res;)V
    .locals 3

    .line 179
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchScreen;->getScreenData()Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/squareup/securetouch/SecureTouchCoordinator;->setTitle(Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;Lcom/squareup/util/Res;)V

    .line 180
    iget-object p2, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->accessibilityButton:Landroid/widget/ImageView;

    const-string v0, "accessibilityButton"

    if-nez p2, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p2, Landroid/view/View;

    .line 181
    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->HARDWARE_SECURE_TOUCH_ACCESSIBILITY_MODE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    .line 180
    invoke-static {p2, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 184
    iget-object p2, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->cancelButton:Landroid/view/View;

    if-nez p2, :cond_1

    const-string v1, "cancelButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {p2, p1}, Lcom/squareup/securetouch/SecureTouchCoordinatorKt;->access$reportTouchEventsDevOnly(Landroid/view/View;Lcom/squareup/securetouch/SecureTouchScreen;)V

    .line 185
    iget-object p2, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->accessibilityButton:Landroid/widget/ImageView;

    if-nez p2, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p2, Landroid/view/View;

    invoke-static {p2, p1}, Lcom/squareup/securetouch/SecureTouchCoordinatorKt;->access$reportTouchEventsDevOnly(Landroid/view/View;Lcom/squareup/securetouch/SecureTouchScreen;)V

    .line 186
    iget-object p2, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->doneButton:Landroid/view/View;

    if-nez p2, :cond_3

    const-string v0, "doneButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-static {p2, p1}, Lcom/squareup/securetouch/SecureTouchCoordinatorKt;->access$reportTouchEventsDevOnly(Landroid/view/View;Lcom/squareup/securetouch/SecureTouchScreen;)V

    .line 187
    iget-object p2, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->rootLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez p2, :cond_4

    const-string v0, "rootLayout"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast p2, Landroid/view/View;

    invoke-static {p2, p1}, Lcom/squareup/securetouch/SecureTouchCoordinatorKt;->access$reportTouchEventsDevOnly(Landroid/view/View;Lcom/squareup/securetouch/SecureTouchScreen;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->screens:Lio/reactivex/Observable;

    .line 83
    sget-object v1, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$1;->INSTANCE:Lcom/squareup/securetouch/SecureTouchCoordinator$attach$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->distinctUntilChanged(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 84
    new-instance v1, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$2;-><init>(Lcom/squareup/securetouch/SecureTouchCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens\n        .distinc\u2026inflateContainerChild() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    new-instance v1, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$3;-><init>(Lcom/squareup/securetouch/SecureTouchCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 89
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "glassSpinner.showOrHideSpinner(view.context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 92
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 93
    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->screens:Lio/reactivex/Observable;

    .line 94
    iget-object v2, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v2}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v2

    .line 92
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    new-instance v2, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$4;

    invoke-direct {v2, p0}, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$4;-><init>(Lcom/squareup/securetouch/SecureTouchCoordinator;)V

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2(Lrx/functions/Func1;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "combineLatest(\n        s\u2026nnerState(screen.data) })"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    new-instance v1, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$5;

    invoke-direct {v1, p0}, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$5;-><init>(Lcom/squareup/securetouch/SecureTouchCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 112
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 113
    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator;->screens:Lio/reactivex/Observable;

    .line 114
    invoke-direct {p0}, Lcom/squareup/securetouch/SecureTouchCoordinator;->onKeypadActivated()Lio/reactivex/Observable;

    move-result-object v2

    .line 112
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 116
    sget-object v1, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$6;->INSTANCE:Lcom/squareup/securetouch/SecureTouchCoordinator$attach$6;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "combineLatest(\n        s\u2026.waitingForKeypadLayout }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    sget-object v1, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$7;->INSTANCE:Lcom/squareup/securetouch/SecureTouchCoordinator$attach$7;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
