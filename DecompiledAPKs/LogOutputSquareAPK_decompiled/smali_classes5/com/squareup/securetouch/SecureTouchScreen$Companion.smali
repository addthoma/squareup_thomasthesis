.class public final Lcom/squareup/securetouch/SecureTouchScreen$Companion;
.super Ljava/lang/Object;
.source "SecureTouchScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/SecureTouchScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\'\u0010\u0003\u001a\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u000e\u0010\n\u001a\u00020\u000bX\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchScreen$Companion;",
        "",
        "()V",
        "KEY",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/securetouch/SecureTouchScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapperKey;",
        "getKEY",
        "()Lcom/squareup/workflow/legacy/Screen$Key;",
        "MINIMUM_PIN_DIGITS",
        "",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/squareup/securetouch/SecureTouchScreen$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getKEY()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 49
    invoke-static {}, Lcom/squareup/securetouch/SecureTouchScreen;->access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    return-object v0
.end method
