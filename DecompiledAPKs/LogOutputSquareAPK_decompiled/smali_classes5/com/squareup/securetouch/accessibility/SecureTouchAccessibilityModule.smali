.class public abstract Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityModule;
.super Ljava/lang/Object;
.source "SecureTouchAccessibilityModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH!J\u0014\u0010\u000b\u001a\u00060\u000cj\u0002`\r2\u0006\u0010\u000e\u001a\u00020\u000fH!J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H!\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityModule;",
        "",
        "()V",
        "bindRestoreAccessibilitySettingsLifecycleWorker",
        "Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;",
        "lifecycleWorker",
        "Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker$Factory;",
        "bindSecureTouchAccessibilityPinEntryWorkflow",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;",
        "secureTouchAccessibilityPinEntryWorkflow",
        "Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;",
        "bindSecureTouchAccessibilityViewFactory",
        "Lcom/squareup/workflow/CompoundWorkflowViewFactory;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityViewFactory;",
        "viewFactory",
        "Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityViewFactory;",
        "bindSecureTouchAccessibilityWorkflow",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityWorkflow;",
        "secureTouchAccessibilityWorkflow",
        "Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindRestoreAccessibilitySettingsLifecycleWorker(Lcom/squareup/securetouch/accessibility/RealRestoreAccessibilitySettingsLifecycleWorker$Factory;)Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindSecureTouchAccessibilityPinEntryWorkflow(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindSecureTouchAccessibilityViewFactory(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityViewFactory;)Lcom/squareup/workflow/CompoundWorkflowViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindSecureTouchAccessibilityWorkflow(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
