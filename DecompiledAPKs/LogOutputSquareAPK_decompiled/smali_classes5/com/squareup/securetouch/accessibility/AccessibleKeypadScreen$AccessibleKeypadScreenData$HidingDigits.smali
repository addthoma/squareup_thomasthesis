.class public final Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;
.super Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;
.source "AccessibleKeypadScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HidingDigits"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B!\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;",
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;",
        "digitsEntered",
        "",
        "displayInputDigits",
        "Lcom/squareup/securetouch/accessibility/InputDigits;",
        "pinEntryState",
        "Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "(ILcom/squareup/securetouch/accessibility/InputDigits;Lcom/squareup/securetouch/SecureTouchPinEntryState;)V",
        "getDigitsEntered",
        "()I",
        "getDisplayInputDigits",
        "()Lcom/squareup/securetouch/accessibility/InputDigits;",
        "getPinEntryState",
        "()Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final digitsEntered:I

.field private final displayInputDigits:Lcom/squareup/securetouch/accessibility/InputDigits;

.field private final pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;


# direct methods
.method public constructor <init>(ILcom/squareup/securetouch/accessibility/InputDigits;Lcom/squareup/securetouch/SecureTouchPinEntryState;)V
    .locals 1

    const-string v0, "displayInputDigits"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pinEntryState"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 48
    invoke-direct {p0, v0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->digitsEntered:I

    iput-object p2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->displayInputDigits:Lcom/squareup/securetouch/accessibility/InputDigits;

    iput-object p3, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/squareup/securetouch/accessibility/InputDigits;Lcom/squareup/securetouch/SecureTouchPinEntryState;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    .line 46
    sget-object p2, Lcom/squareup/securetouch/accessibility/NoDigits;->INSTANCE:Lcom/squareup/securetouch/accessibility/NoDigits;

    check-cast p2, Lcom/squareup/securetouch/accessibility/InputDigits;

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;-><init>(ILcom/squareup/securetouch/accessibility/InputDigits;Lcom/squareup/securetouch/SecureTouchPinEntryState;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;ILcom/squareup/securetouch/accessibility/InputDigits;Lcom/squareup/securetouch/SecureTouchPinEntryState;ILjava/lang/Object;)Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getDigitsEntered()I

    move-result p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getDisplayInputDigits()Lcom/squareup/securetouch/accessibility/InputDigits;

    move-result-object p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object p3

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->copy(ILcom/squareup/securetouch/accessibility/InputDigits;Lcom/squareup/securetouch/SecureTouchPinEntryState;)Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getDigitsEntered()I

    move-result v0

    return v0
.end method

.method public final component2()Lcom/squareup/securetouch/accessibility/InputDigits;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getDisplayInputDigits()Lcom/squareup/securetouch/accessibility/InputDigits;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Lcom/squareup/securetouch/SecureTouchPinEntryState;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v0

    return-object v0
.end method

.method public final copy(ILcom/squareup/securetouch/accessibility/InputDigits;Lcom/squareup/securetouch/SecureTouchPinEntryState;)Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;
    .locals 1

    const-string v0, "displayInputDigits"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pinEntryState"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;-><init>(ILcom/squareup/securetouch/accessibility/InputDigits;Lcom/squareup/securetouch/SecureTouchPinEntryState;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getDigitsEntered()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getDigitsEntered()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getDisplayInputDigits()Lcom/squareup/securetouch/accessibility/InputDigits;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getDisplayInputDigits()Lcom/squareup/securetouch/accessibility/InputDigits;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getDigitsEntered()I
    .locals 1

    .line 45
    iget v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->digitsEntered:I

    return v0
.end method

.method public getDisplayInputDigits()Lcom/squareup/securetouch/accessibility/InputDigits;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->displayInputDigits:Lcom/squareup/securetouch/accessibility/InputDigits;

    return-object v0
.end method

.method public getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getDigitsEntered()I

    move-result v0

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getDisplayInputDigits()Lcom/squareup/securetouch/accessibility/InputDigits;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HidingDigits(digitsEntered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getDigitsEntered()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", displayInputDigits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getDisplayInputDigits()Lcom/squareup/securetouch/accessibility/InputDigits;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", pinEntryState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
