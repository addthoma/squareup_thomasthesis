.class public final Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData$Companion;
.super Ljava/lang/Object;
.source "SecureTouchScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData$Companion;",
        "",
        "()V",
        "fromSecureTouchState",
        "Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;",
        "state",
        "Lcom/squareup/securetouch/SecureTouchState;",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromSecureTouchState(Lcom/squareup/securetouch/SecureTouchState;)Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;
    .locals 23

    move-object/from16 v0, p1

    const-string v1, "state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    instance-of v1, v0, Lcom/squareup/securetouch/AwaitingKeypad;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 26
    check-cast v0, Lcom/squareup/securetouch/AwaitingKeypad;

    invoke-virtual {v0}, Lcom/squareup/securetouch/AwaitingKeypad;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 28
    invoke-virtual {v0}, Lcom/squareup/securetouch/AwaitingKeypad;->getUseHighContrastMode()Z

    move-result v10

    const/16 v11, 0x2f

    const/4 v12, 0x0

    move-object v2, v1

    .line 25
    invoke-direct/range {v2 .. v12}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;-><init>(IZZZLcom/squareup/securetouch/SecureTouchPinEntryState;ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    move-object v0, v1

    goto/16 :goto_5

    .line 30
    :cond_0
    instance-of v1, v0, Lcom/squareup/securetouch/UsingKeypad;

    if-eqz v1, :cond_4

    new-instance v1, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;

    .line 31
    check-cast v0, Lcom/squareup/securetouch/UsingKeypad;

    invoke-virtual {v0}, Lcom/squareup/securetouch/UsingKeypad;->getDotsShowing()I

    move-result v3

    .line 32
    invoke-virtual {v0}, Lcom/squareup/securetouch/UsingKeypad;->getDotsShowing()I

    move-result v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-lez v2, :cond_1

    const/4 v6, 0x1

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    .line 33
    :goto_1
    invoke-virtual {v0}, Lcom/squareup/securetouch/UsingKeypad;->getDotsShowing()I

    move-result v2

    if-lez v2, :cond_2

    const/4 v7, 0x1

    goto :goto_2

    :cond_2
    const/4 v7, 0x0

    .line 34
    :goto_2
    invoke-virtual {v0}, Lcom/squareup/securetouch/UsingKeypad;->getDotsShowing()I

    move-result v2

    const/4 v8, 0x4

    if-lt v2, v8, :cond_3

    const/4 v8, 0x1

    goto :goto_3

    :cond_3
    const/4 v8, 0x0

    .line 35
    :goto_3
    invoke-virtual {v0}, Lcom/squareup/securetouch/UsingKeypad;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 36
    invoke-virtual {v0}, Lcom/squareup/securetouch/UsingKeypad;->getUseHighContrastMode()Z

    move-result v0

    const/16 v12, 0x60

    const/4 v13, 0x0

    move-object v2, v1

    move v4, v6

    move v5, v7

    move v6, v8

    move-object v7, v9

    move v8, v10

    move v9, v11

    move v10, v0

    move v11, v12

    move-object v12, v13

    .line 30
    invoke-direct/range {v2 .. v12}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;-><init>(IZZZLcom/squareup/securetouch/SecureTouchPinEntryState;ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    .line 38
    :cond_4
    instance-of v1, v0, Lcom/squareup/securetouch/EnablingKeypad;

    if-eqz v1, :cond_5

    goto :goto_4

    .line 39
    :cond_5
    instance-of v0, v0, Lcom/squareup/securetouch/DisablingKeypad;

    if-eqz v0, :cond_6

    :goto_4
    new-instance v0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xdf

    const/4 v11, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v11}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;-><init>(IZZZLcom/squareup/securetouch/SecureTouchPinEntryState;ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_5

    .line 40
    :cond_6
    new-instance v0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0xff

    const/16 v22, 0x0

    move-object v12, v0

    invoke-direct/range {v12 .. v22}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;-><init>(IZZZLcom/squareup/securetouch/SecureTouchPinEntryState;ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_5
    return-object v0
.end method
