.class public final Lcom/squareup/securetouch/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accessibility_button_label:I = 0x7f12002c

.field public static final clear_uppercase:I = 0x7f120429

.field public static final digit_0:I = 0x7f12086b

.field public static final digit_1:I = 0x7f12086c

.field public static final digit_2:I = 0x7f12086d

.field public static final digit_3:I = 0x7f12086e

.field public static final digit_4:I = 0x7f12086f

.field public static final digit_5:I = 0x7f120870

.field public static final digit_6:I = 0x7f120871

.field public static final digit_7:I = 0x7f120872

.field public static final digit_8:I = 0x7f120873

.field public static final digit_9:I = 0x7f120874

.field public static final keypad_card_details:I = 0x7f120e7c

.field public static final keypad_enter_your_pin:I = 0x7f120e7d

.field public static final keypad_incorrect_pin:I = 0x7f120e7e

.field public static final keypad_last_retry:I = 0x7f120e7f

.field public static final keypad_mode_accessibility_button_text:I = 0x7f120e80

.field public static final keypad_mode_high_contrast_button_text:I = 0x7f120e81

.field public static final keypad_mode_standard_pin_button_text:I = 0x7f120e82

.field public static final keypad_select_mode_title:I = 0x7f120e83

.field public static final keypad_select_mode_title_content_description:I = 0x7f120e84

.field public static final keypad_try_again:I = 0x7f120e85

.field public static final letters_abc_uppercase:I = 0x7f120ebe

.field public static final letters_def_uppercase:I = 0x7f120ebf

.field public static final letters_empty:I = 0x7f120ec0

.field public static final letters_ghi_uppercase:I = 0x7f120ec1

.field public static final letters_jkl_uppercase:I = 0x7f120ec2

.field public static final letters_mno_uppercase:I = 0x7f120ec3

.field public static final letters_pqrs_uppercase:I = 0x7f120ec4

.field public static final letters_tuv_uppercase:I = 0x7f120ec5

.field public static final letters_wxyz_uppercase:I = 0x7f120ec6

.field public static final start_accessibility_dialog_enable:I = 0x7f1218b8

.field public static final start_accessibility_dialog_message:I = 0x7f1218b9

.field public static final start_accessibility_dialog_title:I = 0x7f1218ba


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
