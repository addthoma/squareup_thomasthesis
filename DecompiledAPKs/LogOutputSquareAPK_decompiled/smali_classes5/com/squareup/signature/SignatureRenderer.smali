.class public Lcom/squareup/signature/SignatureRenderer;
.super Ljava/lang/Object;
.source "SignatureRenderer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/signature/SignatureRenderer$BitmapProvider;,
        Lcom/squareup/signature/SignatureRenderer$SignatureState;
    }
.end annotation


# static fields
.field public static final DEFAULT_SIGNATURE_COLOR_INT:I = -0x1000000


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private final bitmapPaint:Landroid/graphics/Paint;

.field private canvas:Landroid/graphics/Canvas;

.field private currentGlyphPainter:Lcom/squareup/signature/BezierGlyphPainter;

.field private segmentCount:I

.field private final signature:Lcom/squareup/signature/Signature;

.field private final strokeWidth:F


# direct methods
.method public constructor <init>(Lcom/squareup/signature/Signature;F)V
    .locals 2

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->bitmapPaint:Landroid/graphics/Paint;

    .line 69
    iput-object p1, p0, Lcom/squareup/signature/SignatureRenderer;->signature:Lcom/squareup/signature/Signature;

    .line 70
    iput p2, p0, Lcom/squareup/signature/SignatureRenderer;->strokeWidth:F

    .line 72
    iget-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->bitmapPaint:Landroid/graphics/Paint;

    iget p1, p1, Lcom/squareup/signature/Signature;->color:I

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 73
    iget-object p1, p0, Lcom/squareup/signature/SignatureRenderer;->bitmapPaint:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 74
    iget-object p1, p0, Lcom/squareup/signature/SignatureRenderer;->bitmapPaint:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 75
    iget-object p1, p0, Lcom/squareup/signature/SignatureRenderer;->bitmapPaint:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/signature/SignatureRenderer;->bitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method

.method public static convertIfNecessary(Lcom/squareup/signature/SignatureRenderer;IIFILcom/squareup/signature/SignatureRenderer$BitmapProvider;)Lcom/squareup/signature/SignatureRenderer;
    .locals 9

    .line 143
    iget-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->signature:Lcom/squareup/signature/Signature;

    iget v0, v0, Lcom/squareup/signature/Signature;->width:I

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->signature:Lcom/squareup/signature/Signature;

    iget v0, v0, Lcom/squareup/signature/Signature;->height:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/squareup/signature/SignatureRenderer;->strokeWidth:F

    cmpl-float v0, v0, p3

    if-nez v0, :cond_0

    return-object p0

    .line 150
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/signature/SignatureRenderer;->clearBitmap()V

    .line 152
    new-instance v0, Lcom/squareup/signature/SignatureRenderer;

    new-instance v1, Lcom/squareup/signature/Signature;

    invoke-direct {v1, p1, p2, p4}, Lcom/squareup/signature/Signature;-><init>(III)V

    invoke-direct {v0, v1, p3}, Lcom/squareup/signature/SignatureRenderer;-><init>(Lcom/squareup/signature/Signature;F)V

    .line 155
    invoke-interface {p5, p1, p2}, Lcom/squareup/signature/SignatureRenderer$BitmapProvider;->createSignatureBitmap(II)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 156
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p2

    .line 157
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p3

    .line 158
    invoke-virtual {v0, p1}, Lcom/squareup/signature/SignatureRenderer;->setBitmap(Landroid/graphics/Bitmap;)V

    int-to-float p1, p2

    .line 160
    iget-object p2, p0, Lcom/squareup/signature/SignatureRenderer;->signature:Lcom/squareup/signature/Signature;

    iget p2, p2, Lcom/squareup/signature/Signature;->width:I

    int-to-float p2, p2

    div-float/2addr p1, p2

    int-to-float p2, p3

    .line 161
    iget-object p3, p0, Lcom/squareup/signature/SignatureRenderer;->signature:Lcom/squareup/signature/Signature;

    iget p3, p3, Lcom/squareup/signature/Signature;->height:I

    int-to-float p3, p3

    div-float/2addr p2, p3

    .line 164
    iget-object p3, p0, Lcom/squareup/signature/SignatureRenderer;->signature:Lcom/squareup/signature/Signature;

    iget-object p3, p3, Lcom/squareup/signature/Signature;->glyphs:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    const/4 p4, 0x0

    const/4 p5, 0x0

    :goto_0
    if-ge p5, p3, :cond_2

    .line 165
    invoke-virtual {v0}, Lcom/squareup/signature/SignatureRenderer;->startGlyph()V

    .line 166
    iget-object v1, p0, Lcom/squareup/signature/SignatureRenderer;->signature:Lcom/squareup/signature/Signature;

    iget-object v1, v1, Lcom/squareup/signature/Signature;->glyphs:Ljava/util/List;

    invoke-interface {v1, p5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/signature/Signature$Glyph;

    .line 167
    iget-object v1, v1, Lcom/squareup/signature/Signature$Glyph;->points:Ljava/util/List;

    .line 169
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_1

    .line 170
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/signature/Point$Timestamped;

    .line 171
    iget v5, v4, Lcom/squareup/signature/Point$Timestamped;->x:F

    mul-float v5, v5, p1

    iget v6, v4, Lcom/squareup/signature/Point$Timestamped;->y:F

    mul-float v6, v6, p2

    iget-wide v7, v4, Lcom/squareup/signature/Point$Timestamped;->time:J

    invoke-virtual {v0, v5, v6, v7, v8}, Lcom/squareup/signature/SignatureRenderer;->extendGlyph(FFJ)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 173
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/signature/SignatureRenderer;->finishGlyph()V

    add-int/lit8 p5, p5, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static decode(Ljava/lang/String;F)Lcom/squareup/signature/SignatureRenderer;
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/signature/-$$Lambda$SignatureRenderer$NqIQgU0mWQn2Ov_RMvqH7lyxFic;

    invoke-direct {v0, p1}, Lcom/squareup/signature/-$$Lambda$SignatureRenderer$NqIQgU0mWQn2Ov_RMvqH7lyxFic;-><init>(F)V

    invoke-static {p0, v0}, Lcom/squareup/signature/Signature;->decode(Ljava/lang/String;Lcom/squareup/signature/Signature$Rerenderer;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/signature/SignatureRenderer;

    return-object p0
.end method

.method private getCanvas()Landroid/graphics/Canvas;
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->canvas:Landroid/graphics/Canvas;

    if-nez v0, :cond_0

    .line 197
    new-instance v0, Landroid/graphics/Canvas;

    invoke-virtual {p0}, Lcom/squareup/signature/SignatureRenderer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->canvas:Landroid/graphics/Canvas;

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->canvas:Landroid/graphics/Canvas;

    return-object v0
.end method

.method static synthetic lambda$decode$0(FLcom/squareup/signature/Signature;[[[I)Lcom/squareup/signature/SignatureRenderer;
    .locals 10

    .line 22
    new-instance v0, Lcom/squareup/signature/SignatureRenderer;

    invoke-direct {v0, p1, p0}, Lcom/squareup/signature/SignatureRenderer;-><init>(Lcom/squareup/signature/Signature;F)V

    .line 24
    array-length p0, p2

    const/4 p1, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p0, :cond_1

    aget-object v2, p2, v1

    .line 25
    invoke-virtual {v0}, Lcom/squareup/signature/SignatureRenderer;->startGlyph()V

    .line 26
    array-length v3, v2

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v3, :cond_0

    aget-object v5, v2, v4

    .line 27
    aget v6, v5, p1

    int-to-float v6, v6

    const/4 v7, 0x1

    aget v7, v5, v7

    int-to-float v7, v7

    const/4 v8, 0x2

    aget v5, v5, v8

    int-to-long v8, v5

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/squareup/signature/SignatureRenderer;->extendGlyph(FFJ)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 29
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/signature/SignatureRenderer;->finishGlyph()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 2

    const/4 v0, 0x0

    .line 128
    iput v0, p0, Lcom/squareup/signature/SignatureRenderer;->segmentCount:I

    const/4 v0, 0x0

    .line 129
    iput-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->currentGlyphPainter:Lcom/squareup/signature/BezierGlyphPainter;

    .line 130
    iget-object v1, p0, Lcom/squareup/signature/SignatureRenderer;->signature:Lcom/squareup/signature/Signature;

    iget-object v1, v1, Lcom/squareup/signature/Signature;->glyphs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 131
    invoke-virtual {p0}, Lcom/squareup/signature/SignatureRenderer;->clearBitmap()V

    .line 132
    iput-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->canvas:Landroid/graphics/Canvas;

    return-void
.end method

.method public clearBitmap()V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->bitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/squareup/ui/Bitmaps;->safeRecycle(Landroid/graphics/Bitmap;)V

    const/4 v0, 0x0

    .line 137
    iput-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->bitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method public extendGlyph(FFJ)V
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->currentGlyphPainter:Lcom/squareup/signature/BezierGlyphPainter;

    if-nez v0, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/squareup/signature/SignatureRenderer;->startGlyph()V

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->currentGlyphPainter:Lcom/squareup/signature/BezierGlyphPainter;

    new-instance v1, Lcom/squareup/signature/Point$Timestamped;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/squareup/signature/Point$Timestamped;-><init>(FFJ)V

    invoke-virtual {v0, v1}, Lcom/squareup/signature/BezierGlyphPainter;->addPoint(Lcom/squareup/signature/Point$Timestamped;)V

    return-void
.end method

.method public finishGlyph()V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->currentGlyphPainter:Lcom/squareup/signature/BezierGlyphPainter;

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {v0}, Lcom/squareup/signature/BezierGlyphPainter;->finish()V

    .line 89
    iget v0, p0, Lcom/squareup/signature/SignatureRenderer;->segmentCount:I

    iget-object v1, p0, Lcom/squareup/signature/SignatureRenderer;->currentGlyphPainter:Lcom/squareup/signature/BezierGlyphPainter;

    invoke-virtual {v1}, Lcom/squareup/signature/BezierGlyphPainter;->getPointCount()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/squareup/signature/SignatureRenderer;->segmentCount:I

    :cond_0
    return-void
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 3

    .line 114
    iget-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->bitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->signature:Lcom/squareup/signature/Signature;

    iget v0, v0, Lcom/squareup/signature/Signature;->width:I

    iget-object v1, p0, Lcom/squareup/signature/SignatureRenderer;->signature:Lcom/squareup/signature/Signature;

    iget v1, v1, Lcom/squareup/signature/Signature;->height:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->bitmap:Landroid/graphics/Bitmap;

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->bitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getSignature()Lcom/squareup/signature/SignatureAsJson;
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->signature:Lcom/squareup/signature/Signature;

    return-object v0
.end method

.method public getStrokeWidth()F
    .locals 1

    .line 109
    iget v0, p0, Lcom/squareup/signature/SignatureRenderer;->strokeWidth:F

    return v0
.end method

.method public hasGlyphs()Z
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->signature:Lcom/squareup/signature/Signature;

    iget-object v0, v0, Lcom/squareup/signature/Signature;->glyphs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public invalidateCurrentGlyph(Landroid/view/View;)V
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/signature/SignatureRenderer;->currentGlyphPainter:Lcom/squareup/signature/BezierGlyphPainter;

    invoke-virtual {v0, p1}, Lcom/squareup/signature/BezierGlyphPainter;->invalidate(Landroid/view/View;)V

    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/signature/SignatureRenderer;->bitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method public startGlyph()V
    .locals 4

    .line 81
    new-instance v0, Lcom/squareup/signature/Signature$Glyph;

    invoke-direct {v0}, Lcom/squareup/signature/Signature$Glyph;-><init>()V

    .line 82
    new-instance v1, Lcom/squareup/signature/BezierGlyphPainter;

    invoke-direct {p0}, Lcom/squareup/signature/SignatureRenderer;->getCanvas()Landroid/graphics/Canvas;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/signature/SignatureRenderer;->bitmapPaint:Landroid/graphics/Paint;

    invoke-direct {v1, v2, v3, v0}, Lcom/squareup/signature/BezierGlyphPainter;-><init>(Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/squareup/signature/Signature$Glyph;)V

    iput-object v1, p0, Lcom/squareup/signature/SignatureRenderer;->currentGlyphPainter:Lcom/squareup/signature/BezierGlyphPainter;

    .line 83
    iget-object v1, p0, Lcom/squareup/signature/SignatureRenderer;->signature:Lcom/squareup/signature/Signature;

    iget-object v1, v1, Lcom/squareup/signature/Signature;->glyphs:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
