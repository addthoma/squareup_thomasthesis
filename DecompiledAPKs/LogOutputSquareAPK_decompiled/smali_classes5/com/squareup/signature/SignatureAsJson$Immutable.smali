.class public Lcom/squareup/signature/SignatureAsJson$Immutable;
.super Ljava/lang/Object;
.source "SignatureAsJson.java"

# interfaces
.implements Lcom/squareup/signature/SignatureAsJson;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/signature/SignatureAsJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Immutable"
.end annotation


# instance fields
.field private final jsonString:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/signature/SignatureAsJson$Immutable;->jsonString:Ljava/lang/String;

    return-void
.end method

.method public static fromJsonString(Ljava/lang/String;)Lcom/squareup/signature/SignatureAsJson;
    .locals 1

    .line 15
    new-instance v0, Lcom/squareup/signature/SignatureAsJson$Immutable;

    invoke-direct {v0, p0}, Lcom/squareup/signature/SignatureAsJson$Immutable;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public encode()Ljava/lang/String;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/signature/SignatureAsJson$Immutable;->jsonString:Ljava/lang/String;

    return-object v0
.end method
