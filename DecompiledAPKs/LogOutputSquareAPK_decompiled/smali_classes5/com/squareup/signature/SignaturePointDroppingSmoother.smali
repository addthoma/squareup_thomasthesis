.class public Lcom/squareup/signature/SignaturePointDroppingSmoother;
.super Ljava/lang/Object;
.source "SignaturePointDroppingSmoother.java"

# interfaces
.implements Lcom/squareup/signature/SignatureSmoothingStrategy;


# instance fields
.field private final n:I

.field private pointsSeen:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 10
    iput v0, p0, Lcom/squareup/signature/SignaturePointDroppingSmoother;->pointsSeen:I

    .line 13
    iput p1, p0, Lcom/squareup/signature/SignaturePointDroppingSmoother;->n:I

    return-void
.end method


# virtual methods
.method public includeNextPoint()Z
    .locals 3

    .line 17
    iget v0, p0, Lcom/squareup/signature/SignaturePointDroppingSmoother;->pointsSeen:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/signature/SignaturePointDroppingSmoother;->pointsSeen:I

    .line 18
    iget v0, p0, Lcom/squareup/signature/SignaturePointDroppingSmoother;->pointsSeen:I

    iget v2, p0, Lcom/squareup/signature/SignaturePointDroppingSmoother;->n:I

    rem-int/2addr v0, v2

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method
