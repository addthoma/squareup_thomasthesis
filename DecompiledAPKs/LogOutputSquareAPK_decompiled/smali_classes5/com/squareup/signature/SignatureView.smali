.class public Lcom/squareup/signature/SignatureView;
.super Landroid/view/View;
.source "SignatureView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/signature/SignatureView$SavedState;,
        Lcom/squareup/signature/SignatureView$SignatureStateListener;
    }
.end annotation


# instance fields
.field private accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private bitmapProvider:Lcom/squareup/signature/SignatureRenderer$BitmapProvider;

.field private clearAnimation:Landroid/view/animation/Animation;

.field private clearing:Z

.field private color:I

.field private listener:Lcom/squareup/signature/SignatureView$SignatureStateListener;

.field private signaturePaint:Landroid/graphics/Paint;

.field private signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

.field private signatureState:Lcom/squareup/signature/SignatureRenderer$SignatureState;

.field private smoothingStrategy:Lcom/squareup/signature/SignatureSmoothingStrategy;

.field private strokeWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 54
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    new-instance p2, Landroid/graphics/Paint;

    const/4 v0, 0x1

    invoke-direct {p2, v0}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p2, p0, Lcom/squareup/signature/SignatureView;->signaturePaint:Landroid/graphics/Paint;

    .line 34
    sget-object p2, Lcom/squareup/signature/SignatureRenderer$SignatureState;->CLEAR:Lcom/squareup/signature/SignatureRenderer$SignatureState;

    iput-object p2, p0, Lcom/squareup/signature/SignatureView;->signatureState:Lcom/squareup/signature/SignatureRenderer$SignatureState;

    .line 38
    sget-object p2, Lcom/squareup/signature/SignatureSmoothingStrategy;->NONE:Lcom/squareup/signature/SignatureSmoothingStrategy;

    iput-object p2, p0, Lcom/squareup/signature/SignatureView;->smoothingStrategy:Lcom/squareup/signature/SignatureSmoothingStrategy;

    const/high16 p2, -0x1000000

    .line 51
    iput p2, p0, Lcom/squareup/signature/SignatureView;->color:I

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/signature/R$dimen;->signature_stroke_width:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    iput p2, p0, Lcom/squareup/signature/SignatureView;->strokeWidth:F

    const-string p2, "accessibility"

    .line 56
    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/accessibility/AccessibilityManager;

    iput-object p1, p0, Lcom/squareup/signature/SignatureView;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    return-void
.end method

.method private setSignatureRenderer(Lcom/squareup/signature/SignatureRenderer;)V
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    if-eqz p1, :cond_0

    .line 124
    sget-object p1, Lcom/squareup/signature/SignatureRenderer$SignatureState;->STARTED_SIGNING:Lcom/squareup/signature/SignatureRenderer$SignatureState;

    invoke-direct {p0, p1}, Lcom/squareup/signature/SignatureView;->updateState(Lcom/squareup/signature/SignatureRenderer$SignatureState;)V

    .line 125
    sget-object p1, Lcom/squareup/signature/SignatureRenderer$SignatureState;->SIGNED:Lcom/squareup/signature/SignatureRenderer$SignatureState;

    invoke-direct {p0, p1}, Lcom/squareup/signature/SignatureView;->updateState(Lcom/squareup/signature/SignatureRenderer$SignatureState;)V

    goto :goto_0

    .line 127
    :cond_0
    sget-object p1, Lcom/squareup/signature/SignatureRenderer$SignatureState;->CLEAR:Lcom/squareup/signature/SignatureRenderer$SignatureState;

    invoke-direct {p0, p1}, Lcom/squareup/signature/SignatureView;->updateState(Lcom/squareup/signature/SignatureRenderer$SignatureState;)V

    .line 129
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/signature/SignatureView;->invalidate()V

    return-void
.end method

.method private updateState(Lcom/squareup/signature/SignatureRenderer$SignatureState;)V
    .locals 1

    .line 257
    iput-object p1, p0, Lcom/squareup/signature/SignatureView;->signatureState:Lcom/squareup/signature/SignatureRenderer$SignatureState;

    .line 258
    iget-object v0, p0, Lcom/squareup/signature/SignatureView;->listener:Lcom/squareup/signature/SignatureView$SignatureStateListener;

    if-eqz v0, :cond_3

    .line 259
    sget-object v0, Lcom/squareup/signature/SignatureView$1;->$SwitchMap$com$squareup$signature$SignatureRenderer$SignatureState:[I

    invoke-virtual {p1}, Lcom/squareup/signature/SignatureRenderer$SignatureState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 267
    :cond_0
    iget-object p1, p0, Lcom/squareup/signature/SignatureView;->listener:Lcom/squareup/signature/SignatureView$SignatureStateListener;

    invoke-interface {p1}, Lcom/squareup/signature/SignatureView$SignatureStateListener;->onSigned()V

    goto :goto_0

    .line 264
    :cond_1
    iget-object p1, p0, Lcom/squareup/signature/SignatureView;->listener:Lcom/squareup/signature/SignatureView$SignatureStateListener;

    invoke-interface {p1}, Lcom/squareup/signature/SignatureView$SignatureStateListener;->onStartedSigning()V

    goto :goto_0

    .line 261
    :cond_2
    iget-object p1, p0, Lcom/squareup/signature/SignatureView;->listener:Lcom/squareup/signature/SignatureView$SignatureStateListener;

    invoke-interface {p1}, Lcom/squareup/signature/SignatureView$SignatureStateListener;->onClearedSignature()V

    :cond_3
    :goto_0
    return-void
.end method


# virtual methods
.method public canBeCleared()Z
    .locals 2

    .line 249
    iget-object v0, p0, Lcom/squareup/signature/SignatureView;->signatureState:Lcom/squareup/signature/SignatureRenderer$SignatureState;

    sget-object v1, Lcom/squareup/signature/SignatureRenderer$SignatureState;->CLEAR:Lcom/squareup/signature/SignatureRenderer$SignatureState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public clear()V
    .locals 2

    .line 66
    iget-boolean v0, p0, Lcom/squareup/signature/SignatureView;->clearing:Z

    if-eqz v0, :cond_0

    return-void

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/squareup/signature/SignatureView;->clearAnimation:Landroid/view/animation/Animation;

    if-nez v0, :cond_1

    .line 69
    invoke-virtual {p0}, Lcom/squareup/signature/SignatureView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/signature/R$anim;->clear_signature:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/signature/SignatureView;->clearAnimation:Landroid/view/animation/Animation;

    :cond_1
    const/4 v0, 0x1

    .line 71
    iput-boolean v0, p0, Lcom/squareup/signature/SignatureView;->clearing:Z

    .line 73
    sget-object v0, Lcom/squareup/signature/SignatureRenderer$SignatureState;->CLEAR:Lcom/squareup/signature/SignatureRenderer$SignatureState;

    invoke-direct {p0, v0}, Lcom/squareup/signature/SignatureView;->updateState(Lcom/squareup/signature/SignatureRenderer$SignatureState;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/signature/SignatureView;->clearAnimation:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/squareup/signature/SignatureView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public clearBitmap()V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {v0}, Lcom/squareup/signature/SignatureRenderer;->clearBitmap()V

    :cond_0
    return-void
.end method

.method public getListener()Lcom/squareup/signature/SignatureView$SignatureStateListener;
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/squareup/signature/SignatureView;->listener:Lcom/squareup/signature/SignatureView$SignatureStateListener;

    return-object v0
.end method

.method public getSignatureRenderer()Lcom/squareup/signature/SignatureRenderer;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/signature/SignatureRenderer;->hasGlyphs()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasSigned()Z
    .locals 2

    .line 253
    iget-object v0, p0, Lcom/squareup/signature/SignatureView;->signatureState:Lcom/squareup/signature/SignatureRenderer$SignatureState;

    sget-object v1, Lcom/squareup/signature/SignatureRenderer$SignatureState;->SIGNED:Lcom/squareup/signature/SignatureRenderer$SignatureState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected onAnimationEnd()V
    .locals 1

    .line 85
    invoke-super {p0}, Landroid/view/View;->onAnimationEnd()V

    const/4 v0, 0x0

    .line 87
    iput-boolean v0, p0, Lcom/squareup/signature/SignatureView;->clearing:Z

    .line 88
    iget-object v0, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {v0}, Lcom/squareup/signature/SignatureRenderer;->clear()V

    .line 90
    invoke-virtual {p0}, Lcom/squareup/signature/SignatureView;->invalidate()V

    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .line 108
    iget-object v0, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {v0}, Lcom/squareup/signature/SignatureRenderer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/signature/SignatureView;->signaturePaint:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/signature/SignatureView;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {p0, p1}, Lcom/squareup/signature/SignatureView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1

    .line 145
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 224
    instance-of v0, p1, Lcom/squareup/signature/SignatureView$SavedState;

    if-eqz v0, :cond_0

    .line 225
    check-cast p1, Lcom/squareup/signature/SignatureView$SavedState;

    .line 226
    invoke-virtual {p1}, Lcom/squareup/signature/SignatureView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 227
    invoke-static {p1}, Lcom/squareup/signature/SignatureView$SavedState;->access$000(Lcom/squareup/signature/SignatureView$SavedState;)Lcom/squareup/signature/SignatureRenderer;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/signature/SignatureView;->setSignatureRenderer(Lcom/squareup/signature/SignatureRenderer;)V

    goto :goto_0

    .line 230
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 235
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 237
    iget-object v1, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    if-eqz v1, :cond_0

    .line 238
    new-instance v2, Lcom/squareup/signature/SignatureView$SavedState;

    invoke-direct {v2, v0, v1}, Lcom/squareup/signature/SignatureView$SavedState;-><init>(Landroid/os/Parcelable;Lcom/squareup/signature/SignatureRenderer;)V

    return-object v2

    :cond_0
    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 6

    .line 133
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 134
    iget-object v0, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    if-eqz v0, :cond_0

    .line 135
    iget v3, p0, Lcom/squareup/signature/SignatureView;->strokeWidth:F

    iget v4, p0, Lcom/squareup/signature/SignatureView;->color:I

    iget-object v5, p0, Lcom/squareup/signature/SignatureView;->bitmapProvider:Lcom/squareup/signature/SignatureRenderer$BitmapProvider;

    move v1, p1

    move v2, p2

    .line 136
    invoke-static/range {v0 .. v5}, Lcom/squareup/signature/SignatureRenderer;->convertIfNecessary(Lcom/squareup/signature/SignatureRenderer;IIFILcom/squareup/signature/SignatureRenderer$BitmapProvider;)Lcom/squareup/signature/SignatureRenderer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    .line 151
    iget-boolean v0, p0, Lcom/squareup/signature/SignatureView;->clearing:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    if-nez v0, :cond_1

    .line 157
    new-instance v0, Lcom/squareup/signature/SignatureRenderer;

    new-instance v2, Lcom/squareup/signature/Signature;

    .line 158
    invoke-virtual {p0}, Lcom/squareup/signature/SignatureView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/squareup/signature/SignatureView;->getHeight()I

    move-result v4

    iget v5, p0, Lcom/squareup/signature/SignatureView;->color:I

    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/signature/Signature;-><init>(III)V

    iget v3, p0, Lcom/squareup/signature/SignatureView;->strokeWidth:F

    invoke-direct {v0, v2, v3}, Lcom/squareup/signature/SignatureRenderer;-><init>(Lcom/squareup/signature/Signature;F)V

    iput-object v0, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    .line 159
    iget-object v0, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    iget-object v2, p0, Lcom/squareup/signature/SignatureView;->bitmapProvider:Lcom/squareup/signature/SignatureRenderer$BitmapProvider;

    invoke-virtual {p0}, Lcom/squareup/signature/SignatureView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/squareup/signature/SignatureView;->getHeight()I

    move-result v4

    invoke-interface {v2, v3, v4}, Lcom/squareup/signature/SignatureRenderer$BitmapProvider;->createSignatureBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/signature/SignatureRenderer;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 162
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 163
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 164
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    .line 166
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    const/4 v6, 0x1

    if-eqz v5, :cond_c

    const/16 v7, 0xa

    if-eq v5, v6, :cond_2

    const/4 v8, 0x2

    if-eq v5, v8, :cond_2

    const/4 v8, 0x7

    if-eq v5, v8, :cond_2

    const/16 v8, 0x9

    if-eq v5, v8, :cond_c

    if-eq v5, v7, :cond_2

    new-array v0, v6, [Ljava/lang/Object;

    .line 214
    invoke-virtual {p1}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "Ignored touch event: %s"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v1

    .line 182
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v5

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v5, :cond_4

    .line 184
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v9

    .line 185
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v10

    .line 186
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v11

    .line 188
    iget-object v13, p0, Lcom/squareup/signature/SignatureView;->smoothingStrategy:Lcom/squareup/signature/SignatureSmoothingStrategy;

    invoke-interface {v13}, Lcom/squareup/signature/SignatureSmoothingStrategy;->includeNextPoint()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 189
    iget-object v13, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    invoke-virtual {v13, v9, v10, v11, v12}, Lcom/squareup/signature/SignatureRenderer;->extendGlyph(FFJ)V

    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 193
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-eq v5, v6, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    if-ne p1, v7, :cond_6

    :cond_5
    const/4 v1, 0x1

    :cond_6
    if-eqz v1, :cond_7

    .line 196
    iget-object p1, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    invoke-virtual {p1}, Lcom/squareup/signature/SignatureRenderer;->finishGlyph()V

    :cond_7
    if-nez v1, :cond_8

    .line 199
    iget-object p1, p0, Lcom/squareup/signature/SignatureView;->smoothingStrategy:Lcom/squareup/signature/SignatureSmoothingStrategy;

    invoke-interface {p1}, Lcom/squareup/signature/SignatureSmoothingStrategy;->includeNextPoint()Z

    move-result p1

    if-eqz p1, :cond_9

    .line 201
    :cond_8
    iget-object p1, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    invoke-virtual {p1, v0, v2, v3, v4}, Lcom/squareup/signature/SignatureRenderer;->extendGlyph(FFJ)V

    .line 205
    :cond_9
    iget-object p1, p0, Lcom/squareup/signature/SignatureView;->signatureState:Lcom/squareup/signature/SignatureRenderer$SignatureState;

    sget-object v0, Lcom/squareup/signature/SignatureRenderer$SignatureState;->STARTED_SIGNING:Lcom/squareup/signature/SignatureRenderer$SignatureState;

    if-eq p1, v0, :cond_a

    .line 206
    sget-object p1, Lcom/squareup/signature/SignatureRenderer$SignatureState;->STARTED_SIGNING:Lcom/squareup/signature/SignatureRenderer$SignatureState;

    invoke-direct {p0, p1}, Lcom/squareup/signature/SignatureView;->updateState(Lcom/squareup/signature/SignatureRenderer$SignatureState;)V

    :cond_a
    if-eqz v1, :cond_b

    .line 209
    sget-object p1, Lcom/squareup/signature/SignatureRenderer$SignatureState;->SIGNED:Lcom/squareup/signature/SignatureRenderer$SignatureState;

    invoke-direct {p0, p1}, Lcom/squareup/signature/SignatureView;->updateState(Lcom/squareup/signature/SignatureRenderer$SignatureState;)V

    .line 218
    :cond_b
    iget-object p1, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    invoke-virtual {p1, p0}, Lcom/squareup/signature/SignatureRenderer;->invalidateCurrentGlyph(Landroid/view/View;)V

    return v6

    .line 169
    :cond_c
    iget-object p1, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    invoke-virtual {p1}, Lcom/squareup/signature/SignatureRenderer;->startGlyph()V

    .line 170
    iget-object p1, p0, Lcom/squareup/signature/SignatureView;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    invoke-virtual {p1, v0, v2, v3, v4}, Lcom/squareup/signature/SignatureRenderer;->extendGlyph(FFJ)V

    .line 171
    sget-object p1, Lcom/squareup/signature/SignatureRenderer$SignatureState;->STARTED_SIGNING:Lcom/squareup/signature/SignatureRenderer$SignatureState;

    invoke-direct {p0, p1}, Lcom/squareup/signature/SignatureView;->updateState(Lcom/squareup/signature/SignatureRenderer$SignatureState;)V

    return v6
.end method

.method public setBitmapProvider(Lcom/squareup/signature/SignatureRenderer$BitmapProvider;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/signature/SignatureView;->bitmapProvider:Lcom/squareup/signature/SignatureRenderer$BitmapProvider;

    return-void
.end method

.method public setListener(Lcom/squareup/signature/SignatureView$SignatureStateListener;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/signature/SignatureView;->listener:Lcom/squareup/signature/SignatureView$SignatureStateListener;

    return-void
.end method

.method public setSignatureColor(I)V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/signature/SignatureView;->signaturePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 61
    iput p1, p0, Lcom/squareup/signature/SignatureView;->color:I

    return-void
.end method

.method public setSmoothingStrategy(Lcom/squareup/signature/SignatureSmoothingStrategy;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/signature/SignatureView;->smoothingStrategy:Lcom/squareup/signature/SignatureSmoothingStrategy;

    return-void
.end method
