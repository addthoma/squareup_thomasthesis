.class public final Lcom/squareup/scales/UsbScaleInterpreter;
.super Ljava/lang/Object;
.source "UsbScaleInterpreter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/scales/UsbScaleInterpreter$Interpreter;,
        Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;,
        Lcom/squareup/scales/UsbScaleInterpreter$RangeType;,
        Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus;,
        Lcom/squareup/scales/UsbScaleInterpreter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUsbScaleInterpreter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UsbScaleInterpreter.kt\ncom/squareup/scales/UsbScaleInterpreter\n*L\n1#1,232:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0012\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0018\u0000 \u00132\u00020\u0001:\u0005\u0013\u0014\u0015\u0016\u0017B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0004H\u0002J\u0016\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u000eJ\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0002R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/scales/UsbScaleInterpreter;",
        "",
        "()V",
        "brecknellWeightDataRequest",
        "",
        "getBrecknellWeightDataRequest",
        "()[B",
        "decodeStatus",
        "Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus;",
        "message",
        "decodeWeightRequest",
        "Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;",
        "response",
        "manufacturer",
        "Lcom/squareup/scales/ScaleTracker$Manufacturer;",
        "stringToUnit",
        "Lcom/squareup/scales/UnitOfMeasurement;",
        "unit",
        "",
        "Companion",
        "Interpreter",
        "NetOrGross",
        "RangeType",
        "ScaleStatus",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CR:B = 0xdt

.field public static final Companion:Lcom/squareup/scales/UsbScaleInterpreter$Companion;

.field public static final ETX:B = 0x3t

.field public static final LF:B = 0xat

.field public static final SP:B = 0x20t

.field public static final STABLE_BIT_RAM_ERROR:I = 0x2

.field public static final STATUS:B = 0x53t

.field public static final STATUS2_BIT_ALWAYS_1:I = 0xd

.field public static final STATUS2_BIT_ALWAYS_1B:I = 0xc

.field public static final STATUS2_BIT_FAULTY_CALIBRATION:I = 0xb

.field public static final STATUS2_BIT_HAS_3RD_BYTE:I = 0xe

.field public static final STATUS2_BIT_OVER_CAPACITY:I = 0x9

.field public static final STATUS2_BIT_PARITY_IGNORE:I = 0xf

.field public static final STATUS2_BIT_ROM_ERROR:I = 0xa

.field public static final STATUS2_BIT_UNDER_CAPACITY:I = 0x8

.field public static final STATUS3_BIT_ALWAYS_1:I = 0x15

.field public static final STATUS3_BIT_ALWAYS_1B:I = 0x14

.field public static final STATUS3_BIT_HAS_4TH_BYTE:I = 0x16

.field public static final STATUS3_BIT_INITIAL_ZERO_ERROR:I = 0x13

.field public static final STATUS3_BIT_LOW_HIGH_RANGE_1:I = 0x11

.field public static final STATUS3_BIT_LOW_HIGH_RANGE_2:I = 0x10

.field public static final STATUS3_BIT_NET_GROSS_WEIGHT:I = 0x12

.field public static final STATUS3_BIT_PARITY_IGNORE:I = 0x17

.field public static final STATUS_BIT_ALWAYS_0:I = 0x6

.field public static final STATUS_BIT_ALWAYS_1:I = 0x5

.field public static final STATUS_BIT_ALWAYS_1B:I = 0x4

.field public static final STATUS_BIT_AT_ZERO:I = 0x1

.field public static final STATUS_BIT_EEPROM_ERROR:I = 0x3

.field public static final STATUS_BIT_IN_MOTION:I = 0x0

.field public static final STATUS_BIT_PARITY_IGNORE:I = 0x7


# instance fields
.field private final brecknellWeightDataRequest:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/scales/UsbScaleInterpreter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/scales/UsbScaleInterpreter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/scales/UsbScaleInterpreter;->Companion:Lcom/squareup/scales/UsbScaleInterpreter$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [B

    .line 46
    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/squareup/scales/UsbScaleInterpreter;->brecknellWeightDataRequest:[B

    return-void

    :array_0
    .array-data 1
        0x57t
        0xdt
    .end array-data
.end method

.method private final decodeStatus([B)Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus;
    .locals 20

    move-object/from16 v0, p1

    const/4 v1, 0x0

    .line 114
    aget-byte v2, v0, v1

    const/16 v3, 0x53

    if-eq v2, v3, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "Couldn\'t decode status: first byte not S!"

    .line 115
    invoke-static {v1, v0}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 116
    sget-object v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$DecodeError;->INSTANCE:Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$DecodeError;

    check-cast v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus;

    return-object v0

    :cond_0
    const/16 v2, 0xd

    .line 118
    invoke-static {v0, v2}, Lkotlin/collections/ArraysKt;->indexOf([BB)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "Couldn\'t decode status: no CR"

    .line 120
    invoke-static {v1, v0}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 121
    sget-object v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$DecodeError;->INSTANCE:Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$DecodeError;

    check-cast v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus;

    return-object v0

    .line 123
    :cond_1
    new-instance v3, Lkotlin/ranges/IntRange;

    const/4 v4, 0x1

    sub-int/2addr v2, v4

    invoke-direct {v3, v4, v2}, Lkotlin/ranges/IntRange;-><init>(II)V

    invoke-static {v0, v3}, Lkotlin/collections/ArraysKt;->sliceArray([BLkotlin/ranges/IntRange;)[B

    move-result-object v0

    .line 124
    invoke-static {v0}, Ljava/util/BitSet;->valueOf([B)Ljava/util/BitSet;

    move-result-object v2

    .line 125
    array-length v3, v0

    const-string v5, "Couldn\'t decode status: only "

    const/4 v6, 0x2

    if-ge v3, v6, :cond_2

    .line 126
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v0, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " bytes!"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 127
    sget-object v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$DecodeError;->INSTANCE:Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$DecodeError;

    check-cast v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus;

    return-object v0

    :cond_2
    const/16 v3, 0xe

    .line 129
    invoke-virtual {v2, v3}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    const/4 v7, 0x3

    if-eqz v3, :cond_3

    .line 130
    array-length v8, v0

    if-ge v8, v7, :cond_3

    .line 131
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v0, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " bytes and 3rd byte expected!"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    sget-object v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$DecodeError;->INSTANCE:Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$DecodeError;

    check-cast v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus;

    return-object v0

    :cond_3
    const/4 v0, 0x0

    if-nez v3, :cond_4

    move-object/from16 v18, v0

    goto :goto_1

    :cond_4
    const/16 v5, 0x12

    .line 137
    invoke-virtual {v2, v5}, Ljava/util/BitSet;->get(I)Z

    move-result v5

    if-eqz v5, :cond_5

    sget-object v5, Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;->NET:Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;

    goto :goto_0

    .line 138
    :cond_5
    sget-object v5, Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;->GROSS:Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;

    :goto_0
    move-object/from16 v18, v5

    :goto_1
    if-nez v3, :cond_7

    :cond_6
    move-object/from16 v19, v0

    goto :goto_3

    :cond_7
    const/16 v5, 0x11

    .line 143
    invoke-virtual {v2, v5}, Ljava/util/BitSet;->get(I)Z

    move-result v8

    const/16 v9, 0x10

    if-eqz v8, :cond_8

    invoke-virtual {v2, v9}, Ljava/util/BitSet;->get(I)Z

    move-result v8

    if-eqz v8, :cond_8

    sget-object v5, Lcom/squareup/scales/UsbScaleInterpreter$RangeType;->HIGH:Lcom/squareup/scales/UsbScaleInterpreter$RangeType;

    :goto_2
    move-object/from16 v19, v5

    goto :goto_3

    .line 144
    :cond_8
    invoke-virtual {v2, v5}, Ljava/util/BitSet;->get(I)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {v2, v9}, Ljava/util/BitSet;->get(I)Z

    move-result v5

    if-nez v5, :cond_6

    sget-object v5, Lcom/squareup/scales/UsbScaleInterpreter$RangeType;->LOW:Lcom/squareup/scales/UsbScaleInterpreter$RangeType;

    goto :goto_2

    .line 147
    :goto_3
    new-instance v5, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;

    .line 148
    invoke-virtual {v2, v7}, Ljava/util/BitSet;->get(I)Z

    move-result v9

    .line 149
    invoke-virtual {v2, v6}, Ljava/util/BitSet;->get(I)Z

    move-result v10

    .line 150
    invoke-virtual {v2, v4}, Ljava/util/BitSet;->get(I)Z

    move-result v11

    .line 151
    invoke-virtual {v2, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    xor-int/lit8 v12, v1, 0x1

    const/16 v1, 0xb

    .line 152
    invoke-virtual {v2, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v13

    const/16 v1, 0xa

    .line 153
    invoke-virtual {v2, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v14

    const/16 v1, 0x9

    .line 154
    invoke-virtual {v2, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v15

    const/16 v1, 0x8

    .line 155
    invoke-virtual {v2, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v16

    if-eqz v3, :cond_9

    const/16 v0, 0x13

    .line 156
    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :cond_9
    move-object/from16 v17, v0

    move-object v8, v5

    .line 147
    invoke-direct/range {v8 .. v19}, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;-><init>(ZZZZZZZZLjava/lang/Boolean;Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;Lcom/squareup/scales/UsbScaleInterpreter$RangeType;)V

    check-cast v5, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus;

    return-object v5
.end method

.method private final stringToUnit(Ljava/lang/String;)Lcom/squareup/scales/UnitOfMeasurement;
    .locals 2

    if-nez p1, :cond_0

    goto :goto_1

    .line 224
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/16 v1, 0x47

    if-eq v0, v1, :cond_5

    const/16 v1, 0x8e6

    if-eq v0, v1, :cond_4

    const/16 v1, 0x95c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x976

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9eb

    if-eq v0, v1, :cond_1

    goto :goto_1

    :cond_1
    const-string v0, "OZ"

    .line 228
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    sget-object p1, Lcom/squareup/scales/UnitOfMeasurement;->OZ:Lcom/squareup/scales/UnitOfMeasurement;

    goto :goto_2

    :cond_2
    const-string v0, "LB"

    .line 225
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    sget-object p1, Lcom/squareup/scales/UnitOfMeasurement;->LB:Lcom/squareup/scales/UnitOfMeasurement;

    goto :goto_2

    :cond_3
    const-string v0, "KG"

    .line 226
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    sget-object p1, Lcom/squareup/scales/UnitOfMeasurement;->KG:Lcom/squareup/scales/UnitOfMeasurement;

    goto :goto_2

    :cond_4
    const-string v0, "GM"

    .line 227
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    goto :goto_0

    :cond_5
    const-string v0, "G"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    :goto_0
    sget-object p1, Lcom/squareup/scales/UnitOfMeasurement;->G:Lcom/squareup/scales/UnitOfMeasurement;

    goto :goto_2

    :cond_6
    :goto_1
    const/4 p1, 0x0

    :goto_2
    return-object p1
.end method


# virtual methods
.method public final decodeWeightRequest([BLcom/squareup/scales/ScaleTracker$Manufacturer;)Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;
    .locals 13

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "manufacturer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    sget-object v0, Lcom/squareup/scales/ScaleTracker$Manufacturer;->BRECKNELL:Lcom/squareup/scales/ScaleTracker$Manufacturer;

    if-ne p2, v0, :cond_11

    const/4 p2, 0x3

    .line 167
    invoke-static {p1, p2}, Lkotlin/collections/ArraysKt;->indexOf([BB)I

    move-result p2

    const/4 v0, -0x1

    const/4 v1, 0x0

    if-ne p2, v0, :cond_0

    new-array p1, v1, [Ljava/lang/Object;

    const-string p2, "Couldn\'t decode weight response: no ETX!"

    .line 169
    invoke-static {p2, p1}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 170
    sget-object p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellDecodingResponseError;->INSTANCE:Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellDecodingResponseError;

    check-cast p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;

    return-object p1

    .line 172
    :cond_0
    new-instance v0, Lkotlin/ranges/IntRange;

    invoke-direct {v0, v1, p2}, Lkotlin/ranges/IntRange;-><init>(II)V

    invoke-static {p1, v0}, Lkotlin/collections/ArraysKt;->sliceArray([BLkotlin/ranges/IntRange;)[B

    move-result-object p2

    const/4 v0, 0x0

    .line 173
    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    .line 175
    move-object v3, v0

    check-cast v3, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus;

    .line 177
    aget-byte v4, p2, v1

    const/16 v5, 0xa

    if-eq v4, v5, :cond_1

    new-array p1, v1, [Ljava/lang/Object;

    const-string p2, "Couldn\'t decode weight response: no leading LF!"

    .line 178
    invoke-static {p2, p1}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 179
    sget-object p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellDecodingResponseError;->INSTANCE:Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellDecodingResponseError;

    check-cast p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;

    return-object p1

    .line 181
    :cond_1
    array-length v4, p2

    const-string v5, "Couldn\'t decode weight response: length = "

    const/4 v6, 0x2

    if-ge v4, v6, :cond_2

    .line 182
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p2, p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array p2, v1, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 183
    sget-object p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellDecodingResponseError;->INSTANCE:Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellDecodingResponseError;

    check-cast p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;

    return-object p1

    :cond_2
    const/4 v4, 0x1

    .line 185
    aget-byte v7, p2, v4

    const/16 v8, 0x53

    if-ne v7, v8, :cond_3

    .line 186
    new-instance p1, Lkotlin/ranges/IntRange;

    array-length v3, p2

    sub-int/2addr v3, v4

    invoke-direct {p1, v4, v3}, Lkotlin/ranges/IntRange;-><init>(II)V

    invoke-static {p2, p1}, Lkotlin/collections/ArraysKt;->sliceArray([BLkotlin/ranges/IntRange;)[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/scales/UsbScaleInterpreter;->decodeStatus([B)Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus;

    move-result-object v3

    move-object v5, v2

    goto :goto_0

    .line 188
    :cond_3
    array-length v2, p2

    const/16 v7, 0xc

    if-ge v2, v7, :cond_4

    .line 189
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p2, p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " and weight expected!"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array p2, v1, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 190
    sget-object p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellDecodingResponseError;->INSTANCE:Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellDecodingResponseError;

    check-cast p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;

    return-object p1

    .line 192
    :cond_4
    new-instance v2, Lkotlin/ranges/IntRange;

    const/4 v5, 0x6

    invoke-direct {v2, v4, v5}, Lkotlin/ranges/IntRange;-><init>(II)V

    invoke-static {p1, v2}, Lkotlin/collections/ArraysKt;->sliceArray([BLkotlin/ranges/IntRange;)[B

    move-result-object v2

    new-instance v5, Ljava/lang/String;

    sget-object v7, Lkotlin/text/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v5, v2, v7}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 193
    new-instance v2, Lkotlin/ranges/IntRange;

    const/4 v7, 0x7

    const/16 v9, 0x8

    invoke-direct {v2, v7, v9}, Lkotlin/ranges/IntRange;-><init>(II)V

    invoke-static {p1, v2}, Lkotlin/collections/ArraysKt;->sliceArray([BLkotlin/ranges/IntRange;)[B

    move-result-object p1

    new-instance v2, Ljava/lang/String;

    sget-object v7, Lkotlin/text/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v2, p1, v7}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    const/16 p1, 0xb

    .line 194
    aget-byte v7, p2, p1

    if-ne v7, v8, :cond_5

    .line 195
    new-instance v3, Lkotlin/ranges/IntRange;

    array-length v7, p2

    sub-int/2addr v7, v4

    invoke-direct {v3, p1, v7}, Lkotlin/ranges/IntRange;-><init>(II)V

    invoke-static {p2, v3}, Lkotlin/collections/ArraysKt;->sliceArray([BLkotlin/ranges/IntRange;)[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/scales/UsbScaleInterpreter;->decodeStatus([B)Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus;

    move-result-object v3

    .line 199
    :cond_5
    :goto_0
    invoke-direct {p0, v2}, Lcom/squareup/scales/UsbScaleInterpreter;->stringToUnit(Ljava/lang/String;)Lcom/squareup/scales/UnitOfMeasurement;

    move-result-object p1

    if-eqz v5, :cond_6

    .line 201
    :try_start_0
    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    :cond_6
    move-object p2, v0

    :goto_1
    if-eqz v5, :cond_8

    .line 206
    move-object v7, v5

    check-cast v7, Ljava/lang/CharSequence;

    const/16 v2, 0x2e

    invoke-static {v7, v2, v1, v6, v0}, Lkotlin/text/StringsKt;->contains$default(Ljava/lang/CharSequence;CZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v8, 0x2e

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x6

    const/4 v12, 0x0

    invoke-static/range {v7 .. v12}, Lkotlin/text/StringsKt;->indexOf$default(Ljava/lang/CharSequence;CIZILjava/lang/Object;)I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v1, v0, -0x1

    :cond_7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :cond_8
    if-eqz v3, :cond_10

    .line 210
    instance-of v1, v3, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;

    if-nez v1, :cond_9

    goto :goto_2

    .line 211
    :cond_9
    check-cast v3, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;

    invoke-virtual {v3}, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->getOverCapacity()Z

    move-result v1

    if-eqz v1, :cond_a

    sget-object p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellOverMaximumWeightResponse;->INSTANCE:Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellOverMaximumWeightResponse;

    check-cast p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;

    goto :goto_3

    .line 212
    :cond_a
    invoke-virtual {v3}, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->getUnderCapacity()Z

    move-result v1

    if-eqz v1, :cond_b

    sget-object p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellUnderMinimumWeightResponse;->INSTANCE:Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellUnderMinimumWeightResponse;

    check-cast p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;

    goto :goto_3

    .line 213
    :cond_b
    invoke-virtual {v3}, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->getStable()Z

    move-result v1

    if-nez v1, :cond_c

    sget-object p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellUnstableReadingResponse;->INSTANCE:Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellUnstableReadingResponse;

    check-cast p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;

    goto :goto_3

    :cond_c
    if-nez p1, :cond_d

    .line 214
    sget-object p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellUnitErrorResponse;->INSTANCE:Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellUnitErrorResponse;

    check-cast p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;

    goto :goto_3

    :cond_d
    if-nez v0, :cond_e

    .line 215
    sget-object p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellWeightReadingErrorResponse;->INSTANCE:Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellWeightReadingErrorResponse;

    check-cast p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;

    goto :goto_3

    :cond_e
    if-eqz p2, :cond_f

    .line 217
    new-instance v1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-direct {v1, v2, v3, p1, p2}, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;-><init>(DLcom/squareup/scales/UnitOfMeasurement;I)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;

    goto :goto_3

    .line 218
    :cond_f
    sget-object p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellWeightReadingErrorResponse;->INSTANCE:Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellWeightReadingErrorResponse;

    check-cast p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;

    goto :goto_3

    .line 210
    :cond_10
    :goto_2
    sget-object p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellDecodingResponseError;->INSTANCE:Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellDecodingResponseError;

    check-cast p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;

    :goto_3
    return-object p1

    .line 221
    :cond_11
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "no cas support yet"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final getBrecknellWeightDataRequest()[B
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/scales/UsbScaleInterpreter;->brecknellWeightDataRequest:[B

    return-object v0
.end method
