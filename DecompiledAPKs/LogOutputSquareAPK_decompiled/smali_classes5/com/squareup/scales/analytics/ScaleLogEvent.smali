.class abstract Lcom/squareup/scales/analytics/ScaleLogEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "ScalesHardwareAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleConnectionLogEvent;,
        Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleStableReadingLogEvent;,
        Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleErrorReadingLogEvent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nScalesHardwareAnalytics.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ScalesHardwareAnalytics.kt\ncom/squareup/scales/analytics/ScaleLogEvent\n*L\n1#1,118:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u00020\u0001:\u0003\u000f\u0010\u0011B\u0019\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\nR\u0011\u0010\r\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\n\u0082\u0001\u0003\u0012\u0013\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/scales/analytics/ScaleLogEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "eventValue",
        "Lcom/squareup/scales/analytics/ScaleEventValue;",
        "scale",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "(Lcom/squareup/scales/analytics/ScaleEventValue;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V",
        "connection_type",
        "",
        "getConnection_type",
        "()Ljava/lang/String;",
        "manufacturer",
        "getManufacturer",
        "model_name",
        "getModel_name",
        "ScaleConnectionLogEvent",
        "ScaleErrorReadingLogEvent",
        "ScaleStableReadingLogEvent",
        "Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleConnectionLogEvent;",
        "Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleStableReadingLogEvent;",
        "Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleErrorReadingLogEvent;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final connection_type:Ljava/lang/String;

.field private final manufacturer:Ljava/lang/String;

.field private final model_name:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/scales/analytics/ScaleEventValue;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V
    .locals 2

    .line 76
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->SCALE:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-virtual {p1}, Lcom/squareup/scales/analytics/ScaleEventValue;->getValue()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    const/4 p1, 0x0

    if-eqz p2, :cond_0

    .line 81
    invoke-virtual {p2}, Lcom/squareup/scales/ScaleTracker$HardwareScale;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, p1

    :goto_0
    const-string v1, ""

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/squareup/scales/analytics/ScaleLogEvent;->model_name:Ljava/lang/String;

    if-eqz p2, :cond_2

    .line 82
    invoke-virtual {p2}, Lcom/squareup/scales/ScaleTracker$HardwareScale;->getManufacturer()Lcom/squareup/scales/ScaleTracker$Manufacturer;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/squareup/scales/ScaleTracker$Manufacturer;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    move-object v0, p1

    :goto_2
    if-eqz v0, :cond_3

    goto :goto_3

    :cond_3
    move-object v0, v1

    :goto_3
    iput-object v0, p0, Lcom/squareup/scales/analytics/ScaleLogEvent;->manufacturer:Ljava/lang/String;

    if-eqz p2, :cond_4

    .line 83
    invoke-virtual {p2}, Lcom/squareup/scales/ScaleTracker$HardwareScale;->getConnectionType()Lcom/squareup/scales/ScaleTracker$ConnectionType;

    move-result-object p2

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/squareup/scales/ScaleTracker$ConnectionType;->name()Ljava/lang/String;

    move-result-object p1

    :cond_4
    if-eqz p1, :cond_5

    goto :goto_4

    :cond_5
    move-object p1, v1

    :goto_4
    iput-object p1, p0, Lcom/squareup/scales/analytics/ScaleLogEvent;->connection_type:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/scales/analytics/ScaleEventValue;Lcom/squareup/scales/ScaleTracker$HardwareScale;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 73
    invoke-direct {p0, p1, p2}, Lcom/squareup/scales/analytics/ScaleLogEvent;-><init>(Lcom/squareup/scales/analytics/ScaleEventValue;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    return-void
.end method


# virtual methods
.method public final getConnection_type()Ljava/lang/String;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/scales/analytics/ScaleLogEvent;->connection_type:Ljava/lang/String;

    return-object v0
.end method

.method public final getManufacturer()Ljava/lang/String;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/scales/analytics/ScaleLogEvent;->manufacturer:Ljava/lang/String;

    return-object v0
.end method

.method public final getModel_name()Ljava/lang/String;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/scales/analytics/ScaleLogEvent;->model_name:Ljava/lang/String;

    return-object v0
.end method
