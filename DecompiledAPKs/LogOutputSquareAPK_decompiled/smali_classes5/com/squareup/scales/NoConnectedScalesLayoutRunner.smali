.class public final Lcom/squareup/scales/NoConnectedScalesLayoutRunner;
.super Ljava/lang/Object;
.source "NoConnectedScalesLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Factory;,
        Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/scales/NoConnectedScalesScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNoConnectedScalesLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NoConnectedScalesLayoutRunner.kt\ncom/squareup/scales/NoConnectedScalesLayoutRunner\n+ 2 MosaicBlock.kt\ncom/squareup/blueprint/mosaic/MosaicBlockKt\n+ 3 ImageUiModel.kt\ncom/squareup/mosaic/components/ImageUiModelKt\n*L\n1#1,114:1\n15#2,8:115\n20#2,2:123\n23#2:127\n15#2,8:128\n20#2,4:136\n15#3,2:125\n*E\n*S KotlinDebug\n*F\n+ 1 NoConnectedScalesLayoutRunner.kt\ncom/squareup/scales/NoConnectedScalesLayoutRunner\n*L\n90#1,8:115\n90#1,2:123\n90#1:127\n98#1,8:128\n98#1,4:136\n90#1,2:125\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u001c\u001dB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0002H\u0002J\"\u0010\u0012\u001a\u00020\r\"\u0008\u0008\u0000\u0010\u0013*\u00020\u0014*\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u0002H\u00130\u0015H\u0002J\"\u0010\u0017\u001a\u00020\r\"\u0008\u0008\u0000\u0010\u0013*\u00020\u0014*\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u0002H\u00130\u0015H\u0002J,\u0010\u0018\u001a\u00020\r\"\u0008\u0008\u0000\u0010\u0019*\u00020\u001a\"\u0008\u0008\u0001\u0010\u0013*\u00020\u0014*\u000e\u0012\u0004\u0012\u0002H\u0019\u0012\u0004\u0012\u0002H\u00130\u001bH\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/scales/NoConnectedScalesLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/scales/NoConnectedScalesScreen;",
        "view",
        "Landroid/view/View;",
        "scalesActionBarConfig",
        "Lcom/squareup/scales/ScalesActionBarConfig;",
        "(Landroid/view/View;Lcom/squareup/scales/ScalesActionBarConfig;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "root",
        "Lcom/squareup/mosaic/core/Root;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "connectScaleLabelModel",
        "P",
        "",
        "Lcom/squareup/blueprint/BlueprintContext;",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
        "scaleImageModel",
        "smallSpacing",
        "C",
        "Lcom/squareup/blueprint/UpdateContext;",
        "Lcom/squareup/blueprint/LinearBlock;",
        "Binding",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final root:Lcom/squareup/mosaic/core/Root;

.field private final scalesActionBarConfig:Lcom/squareup/scales/ScalesActionBarConfig;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/scales/ScalesActionBarConfig;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scalesActionBarConfig"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->scalesActionBarConfig:Lcom/squareup/scales/ScalesActionBarConfig;

    .line 45
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object p2, p0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 46
    iget-object p1, p0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/connectedscales/impl/R$id;->no_connected_scales_container:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.n\u2026nnected_scales_container)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/mosaic/core/Root;

    iput-object p1, p0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->root:Lcom/squareup/mosaic/core/Root;

    .line 52
    iget-object p1, p0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 50
    new-instance p2, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 51
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/connectedscales/impl/R$string;->scales_action_bar_title:I

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v0, Lcom/squareup/resources/TextModel;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p2

    .line 52
    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method public static final synthetic access$connectScaleLabelModel(Lcom/squareup/scales/NoConnectedScalesLayoutRunner;Lcom/squareup/blueprint/BlueprintContext;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->connectScaleLabelModel(Lcom/squareup/blueprint/BlueprintContext;)V

    return-void
.end method

.method public static final synthetic access$scaleImageModel(Lcom/squareup/scales/NoConnectedScalesLayoutRunner;Lcom/squareup/blueprint/BlueprintContext;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->scaleImageModel(Lcom/squareup/blueprint/BlueprintContext;)V

    return-void
.end method

.method public static final synthetic access$smallSpacing(Lcom/squareup/scales/NoConnectedScalesLayoutRunner;Lcom/squareup/blueprint/LinearBlock;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->smallSpacing(Lcom/squareup/blueprint/LinearBlock;)V

    return-void
.end method

.method private final connectScaleLabelModel(Lcom/squareup/blueprint/BlueprintContext;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "TP;>;)V"
        }
    .end annotation

    .line 132
    new-instance v0, Lcom/squareup/blueprint/mosaic/MosaicBlock;

    .line 136
    invoke-interface {p1}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x1

    .line 132
    invoke-direct {v0, v1, v2, v2}, Lcom/squareup/blueprint/mosaic/MosaicBlock;-><init>(Ljava/lang/Object;ZZ)V

    .line 99
    move-object v1, v0

    check-cast v1, Lcom/squareup/mosaic/core/UiModelContext;

    sget-object v2, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$connectScaleLabelModel$1$1;->INSTANCE:Lcom/squareup/scales/NoConnectedScalesLayoutRunner$connectScaleLabelModel$1$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lcom/squareup/mosaic/components/LabelUiModelKt;->label(Lcom/squareup/mosaic/core/UiModelContext;Lkotlin/jvm/functions/Function1;)V

    .line 106
    check-cast v0, Lcom/squareup/blueprint/Block;

    .line 132
    invoke-interface {p1, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method private final scaleImageModel(Lcom/squareup/blueprint/BlueprintContext;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "TP;>;)V"
        }
    .end annotation

    .line 119
    new-instance v0, Lcom/squareup/blueprint/mosaic/MosaicBlock;

    .line 123
    invoke-interface {p1}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x1

    .line 119
    invoke-direct {v0, v1, v2, v2}, Lcom/squareup/blueprint/mosaic/MosaicBlock;-><init>(Ljava/lang/Object;ZZ)V

    .line 91
    move-object v1, v0

    check-cast v1, Lcom/squareup/mosaic/core/UiModelContext;

    .line 125
    new-instance v2, Lcom/squareup/mosaic/components/ImageUiModel;

    invoke-interface {v1}, Lcom/squareup/mosaic/core/UiModelContext;->createParams()Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/mosaic/components/ImageUiModel;-><init>(Ljava/lang/Object;)V

    .line 91
    move-object v3, v2

    check-cast v3, Lcom/squareup/mosaic/core/DrawableModelContext;

    sget v4, Lcom/squareup/connectedscales/impl/R$drawable;->ic_scale:I

    invoke-static {v3, v4}, Lcom/squareup/mosaic/drawables/SimpleDrawableModelKt;->resource(Lcom/squareup/mosaic/core/DrawableModelContext;I)V

    check-cast v2, Lcom/squareup/mosaic/core/UiModel;

    .line 125
    invoke-interface {v1, v2}, Lcom/squareup/mosaic/core/UiModelContext;->add(Lcom/squareup/mosaic/core/UiModel;)V

    .line 92
    check-cast v0, Lcom/squareup/blueprint/Block;

    .line 119
    invoke-interface {p1, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method private final smallSpacing(Lcom/squareup/blueprint/LinearBlock;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Lcom/squareup/blueprint/UpdateContext;",
            "P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/LinearBlock<",
            "TC;TP;>;)V"
        }
    .end annotation

    .line 95
    move-object v0, p1

    check-cast v0, Lcom/squareup/blueprint/Block;

    sget v1, Lcom/squareup/connectedscales/impl/R$dimen;->no_scales_container_spacing_between_icon_and_text:I

    invoke-static {v0, v1}, Lcom/squareup/blueprint/BlockKt;->dimen(Lcom/squareup/blueprint/Block;I)Lcom/squareup/resources/DimenModel;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/blueprint/LinearBlock;->spacing(Lcom/squareup/resources/DimenModel;)V

    return-void
.end method

.method private final updateActionBar(Lcom/squareup/scales/NoConnectedScalesScreen;)V
    .locals 4

    .line 86
    iget-object v0, p0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 78
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 80
    iget-object v2, p0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->scalesActionBarConfig:Lcom/squareup/scales/ScalesActionBarConfig;

    invoke-interface {v2}, Lcom/squareup/scales/ScalesActionBarConfig;->getShowBackButton()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$updateActionBar$$inlined$apply$lambda$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$updateActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/scales/NoConnectedScalesLayoutRunner;Lcom/squareup/scales/NoConnectedScalesScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 83
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 86
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/scales/NoConnectedScalesScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iget-object p2, p0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$showRendering$1;-><init>(Lcom/squareup/scales/NoConnectedScalesScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->updateActionBar(Lcom/squareup/scales/NoConnectedScalesScreen;)V

    .line 62
    iget-object p1, p0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->root:Lcom/squareup/mosaic/core/Root;

    new-instance p2, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$showRendering$2;

    invoke-direct {p2, p0}, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$showRendering$2;-><init>(Lcom/squareup/scales/NoConnectedScalesLayoutRunner;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2}, Lcom/squareup/mosaic/core/Root;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/scales/NoConnectedScalesScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->showRendering(Lcom/squareup/scales/NoConnectedScalesScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
