.class public final Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;
.super Ljava/lang/Object;
.source "SerialUsbScaleDiscoverer.kt"

# interfaces
.implements Lcom/squareup/usb/UsbDiscoverer$DeviceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "DiscovererListener"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSerialUsbScaleDiscoverer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SerialUsbScaleDiscoverer.kt\ncom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener\n*L\n1#1,82:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;",
        "Lcom/squareup/usb/UsbDiscoverer$DeviceListener;",
        "(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)V",
        "deviceAvailable",
        "",
        "usbDevice",
        "Landroid/hardware/usb/UsbDevice;",
        "deviceUnavailable",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;


# direct methods
.method public constructor <init>(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 50
    iput-object p1, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;->this$0:Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public deviceAvailable(Landroid/hardware/usb/UsbDevice;)V
    .locals 9

    const-string/jumbo v0, "usbDevice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    new-instance v0, Lcom/squareup/scales/UsbCable;

    .line 54
    iget-object v1, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;->this$0:Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;

    invoke-static {v1}, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->access$getRealScaleTracker$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Lcom/squareup/scales/RealScaleTracker;

    move-result-object v3

    .line 55
    iget-object v1, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;->this$0:Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;

    invoke-static {v1}, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->access$getBackgroundThreadExecutor$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Ljava/util/concurrent/Executor;

    move-result-object v4

    .line 56
    iget-object v1, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;->this$0:Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;

    invoke-static {v1}, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->access$getManager$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Lcom/squareup/hardware/usb/UsbManager;

    move-result-object v5

    .line 57
    iget-object v1, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;->this$0:Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;

    invoke-static {v1}, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->access$getMainThread$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Lcom/squareup/thread/executor/MainThread;

    move-result-object v6

    .line 58
    iget-object v1, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;->this$0:Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;

    invoke-static {v1}, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->access$getUsbScaleInterpreter$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Lcom/squareup/scales/UsbScaleInterpreter;

    move-result-object v7

    .line 59
    iget-object v1, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;->this$0:Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;

    invoke-static {v1}, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->access$getAnalytics$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;

    move-result-object v8

    move-object v1, v0

    move-object v2, p1

    .line 52
    invoke-direct/range {v1 .. v8}, Lcom/squareup/scales/UsbCable;-><init>(Landroid/hardware/usb/UsbDevice;Lcom/squareup/scales/RealScaleTracker;Ljava/util/concurrent/Executor;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/scales/UsbScaleInterpreter;Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;)V

    .line 62
    iget-object v1, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;->this$0:Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;

    invoke-static {v1}, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->access$getDeviceNameToCable$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/scales/UsbCable;

    if-eqz v1, :cond_0

    .line 63
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Connection event for a scale that we are already tracking! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    invoke-virtual {v1}, Lcom/squareup/scales/UsbCable;->onDisconnected()V

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;->this$0:Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;

    invoke-static {v1}, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->access$getDeviceNameToCable$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v2, "usbDevice.deviceName"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    invoke-virtual {v0}, Lcom/squareup/scales/UsbCable;->onPluggedIntoPhone()V

    return-void
.end method

.method public deviceUnavailable(Landroid/hardware/usb/UsbDevice;)V
    .locals 2

    const-string/jumbo v0, "usbDevice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;->this$0:Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;

    invoke-static {v0}, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->access$getDeviceNameToCable$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/scales/UsbCable;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Disconnection event for untracked cable! "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 77
    :cond_0
    iget-object v1, p0, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer$DiscovererListener;->this$0:Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;

    invoke-static {v1}, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;->access$getDeviceNameToCable$p(Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    invoke-virtual {v0}, Lcom/squareup/scales/UsbCable;->onDisconnected()V

    return-void
.end method
