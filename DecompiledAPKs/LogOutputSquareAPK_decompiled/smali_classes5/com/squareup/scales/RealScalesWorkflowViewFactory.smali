.class public final Lcom/squareup/scales/RealScalesWorkflowViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealScalesWorkflowViewFactory.kt"

# interfaces
.implements Lcom/squareup/scales/ScalesWorkflowViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0017\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/scales/RealScalesWorkflowViewFactory;",
        "Lcom/squareup/scales/ScalesWorkflowViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "noConnectedScalesLayoutRunnerFactory",
        "Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Factory;",
        "showingConnectedScalesLayoutRunnerFactory",
        "Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Factory;",
        "(Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Factory;Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Factory;Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Factory;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "noConnectedScalesLayoutRunnerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "showingConnectedScalesLayoutRunnerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 10
    new-instance v1, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Binding;

    invoke-direct {v1, p1}, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Binding;-><init>(Lcom/squareup/scales/NoConnectedScalesLayoutRunner$Factory;)V

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p1, 0x0

    aput-object v1, v0, p1

    .line 11
    new-instance p1, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Binding;

    invoke-direct {p1, p2}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Binding;-><init>(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Factory;)V

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p2, 0x1

    aput-object p1, v0, p2

    .line 9
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
