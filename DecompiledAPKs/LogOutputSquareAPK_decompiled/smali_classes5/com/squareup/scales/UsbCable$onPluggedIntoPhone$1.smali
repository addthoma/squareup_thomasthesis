.class final Lcom/squareup/scales/UsbCable$onPluggedIntoPhone$1;
.super Ljava/lang/Object;
.source "UsbCable.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/scales/UsbCable;->onPluggedIntoPhone()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUsbCable.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UsbCable.kt\ncom/squareup/scales/UsbCable$onPluggedIntoPhone$1\n*L\n1#1,82:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/scales/UsbCable;


# direct methods
.method constructor <init>(Lcom/squareup/scales/UsbCable;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/scales/UsbCable$onPluggedIntoPhone$1;->this$0:Lcom/squareup/scales/UsbCable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .line 33
    iget-object v0, p0, Lcom/squareup/scales/UsbCable$onPluggedIntoPhone$1;->this$0:Lcom/squareup/scales/UsbCable;

    invoke-static {v0}, Lcom/squareup/scales/UsbCable;->access$getManager$p(Lcom/squareup/scales/UsbCable;)Lcom/squareup/hardware/usb/UsbManager;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/scales/UsbCable$onPluggedIntoPhone$1;->this$0:Lcom/squareup/scales/UsbCable;

    invoke-static {v1}, Lcom/squareup/scales/UsbCable;->access$getUsbDevice$p(Lcom/squareup/scales/UsbCable;)Landroid/hardware/usb/UsbDevice;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/hardware/usb/UsbManager;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string/jumbo v1, "usbConnection is null!!"

    .line 35
    invoke-static {v1, v0}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 39
    :cond_0
    iget-object v2, p0, Lcom/squareup/scales/UsbCable$onPluggedIntoPhone$1;->this$0:Lcom/squareup/scales/UsbCable;

    invoke-static {v2}, Lcom/squareup/scales/UsbCable;->access$getUsbDevice$p(Lcom/squareup/scales/UsbCable;)Landroid/hardware/usb/UsbDevice;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/felhr/usbserial/UsbSerialDevice;->createUsbSerialDevice(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)Lcom/felhr/usbserial/UsbSerialDevice;

    move-result-object v0

    if-nez v0, :cond_1

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "serial is null!!"

    .line 41
    invoke-static {v1, v0}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 45
    :cond_1
    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSerialDevice;->syncOpen()Z

    const/16 v2, 0x2580

    .line 46
    invoke-virtual {v0, v2}, Lcom/felhr/usbserial/UsbSerialDevice;->setBaudRate(I)V

    const/4 v2, 0x7

    .line 47
    invoke-virtual {v0, v2}, Lcom/felhr/usbserial/UsbSerialDevice;->setDataBits(I)V

    const/4 v2, 0x2

    .line 48
    invoke-virtual {v0, v2}, Lcom/felhr/usbserial/UsbSerialDevice;->setParity(I)V

    const/4 v2, 0x1

    .line 49
    invoke-virtual {v0, v2}, Lcom/felhr/usbserial/UsbSerialDevice;->setStopBits(I)V

    .line 50
    invoke-virtual {v0, v1}, Lcom/felhr/usbserial/UsbSerialDevice;->setFlowControl(I)V

    .line 53
    :goto_0
    iget-object v1, p0, Lcom/squareup/scales/UsbCable$onPluggedIntoPhone$1;->this$0:Lcom/squareup/scales/UsbCable;

    invoke-static {v1}, Lcom/squareup/scales/UsbCable;->access$getActive$p(Lcom/squareup/scales/UsbCable;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 54
    iget-object v1, p0, Lcom/squareup/scales/UsbCable$onPluggedIntoPhone$1;->this$0:Lcom/squareup/scales/UsbCable;

    new-instance v2, Lcom/squareup/scales/UsbScale;

    .line 55
    invoke-static {v1}, Lcom/squareup/scales/UsbCable;->access$getUsbScaleInterpreter$p(Lcom/squareup/scales/UsbCable;)Lcom/squareup/scales/UsbScaleInterpreter;

    move-result-object v4

    iget-object v3, p0, Lcom/squareup/scales/UsbCable$onPluggedIntoPhone$1;->this$0:Lcom/squareup/scales/UsbCable;

    invoke-static {v3}, Lcom/squareup/scales/UsbCable;->access$getMainThread$p(Lcom/squareup/scales/UsbCable;)Lcom/squareup/thread/executor/MainThread;

    move-result-object v5

    iget-object v3, p0, Lcom/squareup/scales/UsbCable$onPluggedIntoPhone$1;->this$0:Lcom/squareup/scales/UsbCable;

    invoke-static {v3}, Lcom/squareup/scales/UsbCable;->access$getUsbDevice$p(Lcom/squareup/scales/UsbCable;)Landroid/hardware/usb/UsbDevice;

    move-result-object v6

    iget-object v3, p0, Lcom/squareup/scales/UsbCable$onPluggedIntoPhone$1;->this$0:Lcom/squareup/scales/UsbCable;

    invoke-static {v3}, Lcom/squareup/scales/UsbCable;->access$getRealScaleTracker$p(Lcom/squareup/scales/UsbCable;)Lcom/squareup/scales/RealScaleTracker;

    move-result-object v7

    iget-object v3, p0, Lcom/squareup/scales/UsbCable$onPluggedIntoPhone$1;->this$0:Lcom/squareup/scales/UsbCable;

    invoke-static {v3}, Lcom/squareup/scales/UsbCable;->access$getAnalytics$p(Lcom/squareup/scales/UsbCable;)Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;

    move-result-object v9

    move-object v3, v2

    move-object v8, v0

    .line 54
    invoke-direct/range {v3 .. v9}, Lcom/squareup/scales/UsbScale;-><init>(Lcom/squareup/scales/UsbScaleInterpreter;Lcom/squareup/thread/executor/MainThread;Landroid/hardware/usb/UsbDevice;Lcom/squareup/scales/RealScaleTracker;Lcom/felhr/usbserial/UsbSerialDevice;Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;)V

    invoke-static {v1, v2}, Lcom/squareup/scales/UsbCable;->access$setUsbScale$p(Lcom/squareup/scales/UsbCable;Lcom/squareup/scales/UsbScale;)V

    .line 61
    iget-object v1, p0, Lcom/squareup/scales/UsbCable$onPluggedIntoPhone$1;->this$0:Lcom/squareup/scales/UsbCable;

    invoke-static {v1}, Lcom/squareup/scales/UsbCable;->access$getUsbScale$p(Lcom/squareup/scales/UsbCable;)Lcom/squareup/scales/UsbScale;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/scales/UsbScale;->readScaleData()V

    .line 63
    :cond_2
    iget-object v1, p0, Lcom/squareup/scales/UsbCable$onPluggedIntoPhone$1;->this$0:Lcom/squareup/scales/UsbCable;

    invoke-static {v1}, Lcom/squareup/scales/UsbCable;->access$getActive$p(Lcom/squareup/scales/UsbCable;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_1

    :cond_3
    const-wide/16 v1, 0x3e8

    .line 64
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_0

    .line 67
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/squareup/scales/UsbCable$onPluggedIntoPhone$1;->this$0:Lcom/squareup/scales/UsbCable;

    invoke-static {v1}, Lcom/squareup/scales/UsbCable;->access$getUsbScale$p(Lcom/squareup/scales/UsbCable;)Lcom/squareup/scales/UsbScale;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/scales/UsbScale;->onDisconnected()V

    .line 69
    :cond_5
    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSerialDevice;->close()V

    return-void
.end method
