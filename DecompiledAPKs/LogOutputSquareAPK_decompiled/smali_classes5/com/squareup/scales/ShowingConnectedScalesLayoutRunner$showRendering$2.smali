.class final Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$showRendering$2;
.super Lkotlin/jvm/internal/Lambda;
.source "ShowingConnectedScalesLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->showRendering(Lcom/squareup/scales/ShowingConnectedScalesScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/mosaic/core/UiModelContext<",
        "Lkotlin/Unit;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nShowingConnectedScalesLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ShowingConnectedScalesLayoutRunner.kt\ncom/squareup/scales/ShowingConnectedScalesLayoutRunner$showRendering$2\n+ 2 BlueprintUiModel.kt\ncom/squareup/blueprint/mosaic/BlueprintUiModelKt\n+ 3 BlueprintDsl.kt\ncom/squareup/blueprint/BlueprintDslKt\n+ 4 MosaicBlock.kt\ncom/squareup/blueprint/mosaic/MosaicBlockKt\n+ 5 RecyclerUiModel.kt\ncom/squareup/cycler/mosaic/RecyclerUiModelKt\n*L\n1#1,222:1\n17#2,11:223\n26#2:234\n22#2,3:235\n28#2:256\n126#3,4:238\n130#3:255\n15#4,8:242\n20#4,2:250\n23#4:254\n13#5,2:252\n*E\n*S KotlinDebug\n*F\n+ 1 ShowingConnectedScalesLayoutRunner.kt\ncom/squareup/scales/ShowingConnectedScalesLayoutRunner$showRendering$2\n*L\n85#1,11:223\n85#1:234\n85#1,3:235\n85#1:256\n85#1,4:238\n85#1:255\n85#1,8:242\n85#1,2:250\n85#1:254\n85#1,2:252\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00010\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $rendering:Lcom/squareup/scales/ShowingConnectedScalesScreen;

.field final synthetic this$0:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;Lcom/squareup/scales/ShowingConnectedScalesScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$showRendering$2;->this$0:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;

    iput-object p2, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$showRendering$2;->$rendering:Lcom/squareup/scales/ShowingConnectedScalesScreen;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Lcom/squareup/mosaic/core/UiModelContext;

    invoke-virtual {p0, p1}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$showRendering$2;->invoke(Lcom/squareup/mosaic/core/UiModelContext;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/mosaic/core/UiModelContext;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    new-instance v0, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;

    .line 235
    invoke-interface {p1}, Lcom/squareup/mosaic/core/UiModelContext;->createParams()Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, v0

    .line 227
    invoke-direct/range {v1 .. v7}, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;-><init>(Ljava/lang/Object;ZZLcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 86
    move-object v1, v0

    check-cast v1, Lcom/squareup/blueprint/BlueprintContext;

    .line 87
    check-cast v0, Lcom/squareup/mosaic/core/UiModel;

    sget v2, Lcom/squareup/connectedscales/impl/R$dimen;->scales_container_horizontal_padding:I

    invoke-static {v0, v2}, Lcom/squareup/mosaic/core/UiModelKt;->dimen(Lcom/squareup/mosaic/core/UiModel;I)Lcom/squareup/resources/DimenModel;

    move-result-object v5

    const/4 v2, 0x0

    .line 238
    invoke-static {v2}, Lcom/squareup/resources/DimenModelsKt;->getDp(I)Lcom/squareup/resources/FixedDimen;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Lcom/squareup/resources/DimenModel;

    .line 241
    new-instance v2, Lcom/squareup/blueprint/InsetBlock;

    invoke-interface {v1}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v4

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xf8

    const/4 v13, 0x0

    move-object v3, v2

    invoke-direct/range {v3 .. v13}, Lcom/squareup/blueprint/InsetBlock;-><init>(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 89
    sget v3, Lcom/squareup/connectedscales/impl/R$dimen;->scales_container_top_padding:I

    invoke-static {v0, v3}, Lcom/squareup/mosaic/core/UiModelKt;->dimen(Lcom/squareup/mosaic/core/UiModel;I)Lcom/squareup/resources/DimenModel;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/blueprint/InsetBlock;->setTopInset(Lcom/squareup/resources/DimenModel;)V

    .line 90
    move-object v3, v2

    check-cast v3, Lcom/squareup/blueprint/BlueprintContext;

    .line 246
    new-instance v4, Lcom/squareup/blueprint/mosaic/MosaicBlock;

    .line 250
    invoke-interface {v3}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v5

    const/4 v6, 0x1

    .line 246
    invoke-direct {v4, v5, v6, v6}, Lcom/squareup/blueprint/mosaic/MosaicBlock;-><init>(Ljava/lang/Object;ZZ)V

    .line 91
    move-object v5, v4

    check-cast v5, Lcom/squareup/mosaic/core/UiModelContext;

    .line 252
    new-instance v13, Lcom/squareup/cycler/mosaic/RecyclerUiModel;

    invoke-interface {v5}, Lcom/squareup/mosaic/core/UiModelContext;->createParams()Ljava/lang/Object;

    move-result-object v7

    const/16 v11, 0xe

    const/4 v12, 0x0

    move-object v6, v13

    invoke-direct/range {v6 .. v12}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;-><init>(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lcom/squareup/cycler/DataSource;Ljava/lang/Object;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 92
    iget-object v6, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$showRendering$2;->this$0:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;

    invoke-static {v6}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->access$getRecyclerConfig$p(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;)Lkotlin/jvm/functions/Function1;

    move-result-object v6

    invoke-virtual {v13, v6}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->setConfig(Lkotlin/jvm/functions/Function1;)V

    .line 93
    iget-object v6, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$showRendering$2;->this$0:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;

    iget-object v7, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$showRendering$2;->$rendering:Lcom/squareup/scales/ShowingConnectedScalesScreen;

    invoke-virtual {v7}, Lcom/squareup/scales/ShowingConnectedScalesScreen;->getConnectedScales()Ljava/util/List;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->access$toRowsDataSource(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v6

    invoke-virtual {v13, v6}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->setData(Lcom/squareup/cycler/DataSource;)V

    .line 94
    check-cast v13, Lcom/squareup/mosaic/core/UiModel;

    .line 252
    invoke-interface {v5, v13}, Lcom/squareup/mosaic/core/UiModelContext;->add(Lcom/squareup/mosaic/core/UiModel;)V

    .line 95
    check-cast v4, Lcom/squareup/blueprint/Block;

    .line 246
    invoke-interface {v3, v4}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    .line 96
    check-cast v2, Lcom/squareup/blueprint/Block;

    .line 241
    invoke-interface {v1, v2}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    .line 233
    invoke-interface {p1, v0}, Lcom/squareup/mosaic/core/UiModelContext;->add(Lcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method
