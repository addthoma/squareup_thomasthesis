.class abstract Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;
.super Lcom/squareup/sdk/pos/transaction/Tender;
.source "$$AutoValue_Tender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;
    }
.end annotation


# instance fields
.field private final cardDetails:Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

.field private final cashDetails:Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

.field private final clientId:Ljava/lang/String;

.field private final createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

.field private final customerId:Ljava/lang/String;

.field private final serverId:Ljava/lang/String;

.field private final tipMoney:Lcom/squareup/sdk/pos/transaction/Money;

.field private final totalMoney:Lcom/squareup/sdk/pos/transaction/Money;

.field private final type:Lcom/squareup/sdk/pos/transaction/Tender$Type;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/DateTime;Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/Tender$Type;Lcom/squareup/sdk/pos/transaction/TenderCardDetails;Lcom/squareup/sdk/pos/transaction/TenderCashDetails;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/sdk/pos/transaction/Tender;-><init>()V

    if-eqz p1, :cond_4

    .line 31
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->clientId:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->serverId:Ljava/lang/String;

    if-eqz p3, :cond_3

    .line 36
    iput-object p3, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    if-eqz p4, :cond_2

    .line 40
    iput-object p4, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->totalMoney:Lcom/squareup/sdk/pos/transaction/Money;

    if-eqz p5, :cond_1

    .line 44
    iput-object p5, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->tipMoney:Lcom/squareup/sdk/pos/transaction/Money;

    .line 45
    iput-object p6, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->customerId:Ljava/lang/String;

    if-eqz p7, :cond_0

    .line 49
    iput-object p7, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->type:Lcom/squareup/sdk/pos/transaction/Tender$Type;

    .line 50
    iput-object p8, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->cardDetails:Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

    .line 51
    iput-object p9, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->cashDetails:Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

    return-void

    .line 47
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null type"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 42
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null tipMoney"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 38
    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null totalMoney"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 34
    :cond_3
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null createdAt"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 29
    :cond_4
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Null clientId"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public cardDetails()Lcom/squareup/sdk/pos/transaction/TenderCardDetails;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->cardDetails:Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

    return-object v0
.end method

.method public cashDetails()Lcom/squareup/sdk/pos/transaction/TenderCashDetails;
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->cashDetails:Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

    return-object v0
.end method

.method public clientId()Ljava/lang/String;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->clientId:Ljava/lang/String;

    return-object v0
.end method

.method public createdAt()Lcom/squareup/sdk/pos/transaction/DateTime;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    return-object v0
.end method

.method public customerId()Ljava/lang/String;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->customerId:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 124
    :cond_0
    instance-of v1, p1, Lcom/squareup/sdk/pos/transaction/Tender;

    const/4 v2, 0x0

    if-eqz v1, :cond_6

    .line 125
    check-cast p1, Lcom/squareup/sdk/pos/transaction/Tender;

    .line 126
    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->clientId:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Tender;->clientId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->serverId:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 127
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Tender;->serverId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Tender;->serverId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_0
    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    .line 128
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Tender;->createdAt()Lcom/squareup/sdk/pos/transaction/DateTime;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->totalMoney:Lcom/squareup/sdk/pos/transaction/Money;

    .line 129
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Tender;->totalMoney()Lcom/squareup/sdk/pos/transaction/Money;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->tipMoney:Lcom/squareup/sdk/pos/transaction/Money;

    .line 130
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Tender;->tipMoney()Lcom/squareup/sdk/pos/transaction/Money;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->customerId:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 131
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Tender;->customerId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Tender;->customerId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_1
    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->type:Lcom/squareup/sdk/pos/transaction/Tender$Type;

    .line 132
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Tender;->type()Lcom/squareup/sdk/pos/transaction/Tender$Type;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/sdk/pos/transaction/Tender$Type;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->cardDetails:Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

    if-nez v1, :cond_3

    .line 133
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Tender;->cardDetails()Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

    move-result-object v1

    if-nez v1, :cond_5

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Tender;->cardDetails()Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_2
    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->cashDetails:Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

    if-nez v1, :cond_4

    .line 134
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Tender;->cashDetails()Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

    move-result-object p1

    if-nez p1, :cond_5

    goto :goto_3

    :cond_4
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Tender;->cashDetails()Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    :goto_3
    return v0

    :cond_6
    return v2
.end method

.method public hashCode()I
    .locals 4

    .line 143
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->clientId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, 0xf4243

    xor-int/2addr v0, v1

    mul-int v0, v0, v1

    .line 145
    iget-object v2, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->serverId:Ljava/lang/String;

    const/4 v3, 0x0

    if-nez v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_0
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 147
    iget-object v2, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 149
    iget-object v2, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->totalMoney:Lcom/squareup/sdk/pos/transaction/Money;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 151
    iget-object v2, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->tipMoney:Lcom/squareup/sdk/pos/transaction/Money;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 153
    iget-object v2, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->customerId:Ljava/lang/String;

    if-nez v2, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 155
    iget-object v2, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->type:Lcom/squareup/sdk/pos/transaction/Tender$Type;

    invoke-virtual {v2}, Lcom/squareup/sdk/pos/transaction/Tender$Type;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 157
    iget-object v2, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->cardDetails:Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

    if-nez v2, :cond_2

    const/4 v2, 0x0

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_2
    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    .line 159
    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->cashDetails:Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

    if-nez v1, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_3
    xor-int/2addr v0, v3

    return v0
.end method

.method public serverId()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->serverId:Ljava/lang/String;

    return-object v0
.end method

.method public tipMoney()Lcom/squareup/sdk/pos/transaction/Money;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->tipMoney:Lcom/squareup/sdk/pos/transaction/Money;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Tender{clientId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->clientId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", serverId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->serverId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", totalMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->totalMoney:Lcom/squareup/sdk/pos/transaction/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tipMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->tipMoney:Lcom/squareup/sdk/pos/transaction/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", customerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->customerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->type:Lcom/squareup/sdk/pos/transaction/Tender$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cardDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->cardDetails:Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cashDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->cashDetails:Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public totalMoney()Lcom/squareup/sdk/pos/transaction/Money;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->totalMoney:Lcom/squareup/sdk/pos/transaction/Money;

    return-object v0
.end method

.method public type()Lcom/squareup/sdk/pos/transaction/Tender$Type;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender;->type:Lcom/squareup/sdk/pos/transaction/Tender$Type;

    return-object v0
.end method
