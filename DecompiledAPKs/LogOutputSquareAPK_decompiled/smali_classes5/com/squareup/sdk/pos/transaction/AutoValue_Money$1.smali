.class final Lcom/squareup/sdk/pos/transaction/AutoValue_Money$1;
.super Ljava/lang/Object;
.source "AutoValue_Money.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/AutoValue_Money;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/sdk/pos/transaction/AutoValue_Money;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/sdk/pos/transaction/AutoValue_Money;
    .locals 3

    .line 11
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Money;

    .line 12
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    .line 13
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/sdk/pos/transaction/Money$CurrencyCode;->valueOf(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Money$CurrencyCode;

    move-result-object p1

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/sdk/pos/transaction/AutoValue_Money;-><init>(JLcom/squareup/sdk/pos/transaction/Money$CurrencyCode;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 8
    invoke-virtual {p0, p1}, Lcom/squareup/sdk/pos/transaction/AutoValue_Money$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/sdk/pos/transaction/AutoValue_Money;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/sdk/pos/transaction/AutoValue_Money;
    .locals 0

    .line 18
    new-array p1, p1, [Lcom/squareup/sdk/pos/transaction/AutoValue_Money;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 8
    invoke-virtual {p0, p1}, Lcom/squareup/sdk/pos/transaction/AutoValue_Money$1;->newArray(I)[Lcom/squareup/sdk/pos/transaction/AutoValue_Money;

    move-result-object p1

    return-object p1
.end method
