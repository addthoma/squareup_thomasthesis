.class public abstract Lcom/squareup/sdk/pos/transaction/Tender;
.super Ljava/lang/Object;
.source "Tender.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/pos/transaction/Tender$Builder;,
        Lcom/squareup/sdk/pos/transaction/Tender$Type;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Lcom/squareup/sdk/pos/transaction/Tender$Builder;
    .locals 1

    .line 75
    new-instance v0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;

    invoke-direct {v0}, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Tender$Builder;-><init>()V

    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/squareup/sdk/pos/transaction/Tender;",
            ">;"
        }
    .end annotation

    .line 106
    new-instance v0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract cardDetails()Lcom/squareup/sdk/pos/transaction/TenderCardDetails;
.end method

.method public abstract cashDetails()Lcom/squareup/sdk/pos/transaction/TenderCashDetails;
.end method

.method public abstract clientId()Ljava/lang/String;
.end method

.method public abstract createdAt()Lcom/squareup/sdk/pos/transaction/DateTime;
.end method

.method public abstract customerId()Ljava/lang/String;
.end method

.method public abstract serverId()Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract tipMoney()Lcom/squareup/sdk/pos/transaction/Money;
.end method

.method public abstract totalMoney()Lcom/squareup/sdk/pos/transaction/Money;
.end method

.method public abstract type()Lcom/squareup/sdk/pos/transaction/Tender$Type;
.end method
