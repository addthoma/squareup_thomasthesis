.class public abstract Lcom/squareup/sdk/pos/transaction/Order$Builder;
.super Ljava/lang/Object;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract build()Lcom/squareup/sdk/pos/transaction/Order;
.end method

.method public abstract totalMoney(Lcom/squareup/sdk/pos/transaction/Money;)Lcom/squareup/sdk/pos/transaction/Order$Builder;
.end method

.method public abstract totalTaxMoney(Lcom/squareup/sdk/pos/transaction/Money;)Lcom/squareup/sdk/pos/transaction/Order$Builder;
.end method

.method public abstract totalTipMoney(Lcom/squareup/sdk/pos/transaction/Money;)Lcom/squareup/sdk/pos/transaction/Order$Builder;
.end method
