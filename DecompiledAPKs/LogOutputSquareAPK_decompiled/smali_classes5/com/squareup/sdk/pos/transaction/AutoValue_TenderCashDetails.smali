.class final Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCashDetails;
.super Lcom/squareup/sdk/pos/transaction/$AutoValue_TenderCashDetails;
.source "AutoValue_TenderCashDetails.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCashDetails;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCashDetails$1;

    invoke-direct {v0}, Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCashDetails$1;-><init>()V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCashDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Lcom/squareup/sdk/pos/transaction/$AutoValue_TenderCashDetails;-><init>(Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCashDetails;->buyerTenderedMoney()Lcom/squareup/sdk/pos/transaction/Money;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 29
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCashDetails;->changeBackMoney()Lcom/squareup/sdk/pos/transaction/Money;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
