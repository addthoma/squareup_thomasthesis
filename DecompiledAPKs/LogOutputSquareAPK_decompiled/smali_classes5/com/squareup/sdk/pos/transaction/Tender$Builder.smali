.class public abstract Lcom/squareup/sdk/pos/transaction/Tender$Builder;
.super Ljava/lang/Object;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract build()Lcom/squareup/sdk/pos/transaction/Tender;
.end method

.method public abstract cardDetails(Lcom/squareup/sdk/pos/transaction/TenderCardDetails;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
.end method

.method public abstract cashDetails(Lcom/squareup/sdk/pos/transaction/TenderCashDetails;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
.end method

.method public abstract clientId(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
.end method

.method public abstract createdAt(Lcom/squareup/sdk/pos/transaction/DateTime;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
.end method

.method public abstract customerId(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
.end method

.method public abstract serverId(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
.end method

.method public abstract tipMoney(Lcom/squareup/sdk/pos/transaction/Money;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
.end method

.method public abstract totalMoney(Lcom/squareup/sdk/pos/transaction/Money;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
.end method

.method public abstract type(Lcom/squareup/sdk/pos/transaction/Tender$Type;)Lcom/squareup/sdk/pos/transaction/Tender$Builder;
.end method
