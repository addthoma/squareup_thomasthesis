.class final Lcom/squareup/sdk/pos/transaction/AutoValue_Money;
.super Lcom/squareup/sdk/pos/transaction/$AutoValue_Money;
.source "AutoValue_Money.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/sdk/pos/transaction/AutoValue_Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Money$1;

    invoke-direct {v0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Money$1;-><init>()V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Money;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(JLcom/squareup/sdk/pos/transaction/Money$CurrencyCode;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/sdk/pos/transaction/$AutoValue_Money;-><init>(JLcom/squareup/sdk/pos/transaction/Money$CurrencyCode;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 28
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Money;->amount()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 29
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Money;->currencyCode()Lcom/squareup/sdk/pos/transaction/Money$CurrencyCode;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/sdk/pos/transaction/Money$CurrencyCode;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
