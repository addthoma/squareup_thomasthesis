.class final Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;
.super Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender;
.source "AutoValue_Tender.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender$1;

    invoke-direct {v0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender$1;-><init>()V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/DateTime;Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/Tender$Type;Lcom/squareup/sdk/pos/transaction/TenderCardDetails;Lcom/squareup/sdk/pos/transaction/TenderCashDetails;)V
    .locals 0

    .line 33
    invoke-direct/range {p0 .. p9}, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/DateTime;Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/Tender$Type;Lcom/squareup/sdk/pos/transaction/TenderCardDetails;Lcom/squareup/sdk/pos/transaction/TenderCashDetails;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .line 38
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;->clientId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;->serverId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 40
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 42
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;->serverId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 45
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;->createdAt()Lcom/squareup/sdk/pos/transaction/DateTime;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 46
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;->totalMoney()Lcom/squareup/sdk/pos/transaction/Money;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 47
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;->tipMoney()Lcom/squareup/sdk/pos/transaction/Money;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 48
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;->customerId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 49
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    .line 51
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 52
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;->customerId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 54
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;->type()Lcom/squareup/sdk/pos/transaction/Tender$Type;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/sdk/pos/transaction/Tender$Type;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;->cardDetails()Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 56
    invoke-virtual {p0}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;->cashDetails()Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
