.class public final Lcom/squareup/sdk/pos/TransactionRequest;
.super Ljava/lang/Object;
.source "TransactionRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/pos/TransactionRequest$TenderType;,
        Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;,
        Lcom/squareup/sdk/pos/TransactionRequest$Error;,
        Lcom/squareup/sdk/pos/TransactionRequest$Success;,
        Lcom/squareup/sdk/pos/TransactionRequest$Builder;
    }
.end annotation


# instance fields
.field public final allowSplitTender:Z

.field public final autoReturn:Z

.field public final currencyCode:Lcom/squareup/sdk/pos/CurrencyCode;

.field public final customerId:Ljava/lang/String;

.field public final locationId:Ljava/lang/String;

.field public final note:Ljava/lang/String;

.field public final skipReceipt:Z

.field public final state:Ljava/lang/String;

.field public final tenderTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/pos/TransactionRequest$TenderType;",
            ">;"
        }
    .end annotation
.end field

.field public final totalAmount:I


# direct methods
.method constructor <init>(Lcom/squareup/sdk/pos/TransactionRequest$Builder;)V
    .locals 1

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iget-object v0, p1, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->tenderTypes:Ljava/util/Set;

    .line 80
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/squareup/sdk/pos/TransactionRequest$TenderType;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->tenderTypes:Ljava/util/Set;

    .line 81
    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v0

    .line 79
    :goto_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/pos/TransactionRequest;->tenderTypes:Ljava/util/Set;

    .line 82
    iget v0, p1, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->totalAmount:I

    iput v0, p0, Lcom/squareup/sdk/pos/TransactionRequest;->totalAmount:I

    .line 83
    iget-object v0, p1, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->currencyCode:Lcom/squareup/sdk/pos/CurrencyCode;

    iput-object v0, p0, Lcom/squareup/sdk/pos/TransactionRequest;->currencyCode:Lcom/squareup/sdk/pos/CurrencyCode;

    .line 84
    iget-object v0, p1, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->note:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/sdk/pos/TransactionRequest;->note:Ljava/lang/String;

    .line 85
    iget-boolean v0, p1, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->autoReturn:Z

    iput-boolean v0, p0, Lcom/squareup/sdk/pos/TransactionRequest;->autoReturn:Z

    .line 86
    iget-object v0, p1, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->locationId:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/sdk/pos/TransactionRequest;->locationId:Ljava/lang/String;

    .line 87
    iget-object v0, p1, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->state:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/sdk/pos/TransactionRequest;->state:Ljava/lang/String;

    .line 88
    iget-object v0, p1, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->customerId:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/sdk/pos/TransactionRequest;->customerId:Ljava/lang/String;

    .line 89
    iget-boolean v0, p1, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->skipReceipt:Z

    iput-boolean v0, p0, Lcom/squareup/sdk/pos/TransactionRequest;->skipReceipt:Z

    .line 90
    iget-boolean p1, p1, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->allowSplitTender:Z

    iput-boolean p1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->allowSplitTender:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 117
    :cond_0
    instance-of v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 120
    :cond_1
    check-cast p1, Lcom/squareup/sdk/pos/TransactionRequest;

    .line 121
    iget v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->totalAmount:I

    iget v3, p1, Lcom/squareup/sdk/pos/TransactionRequest;->totalAmount:I

    if-eq v1, v3, :cond_2

    return v2

    .line 124
    :cond_2
    iget-boolean v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->autoReturn:Z

    iget-boolean v3, p1, Lcom/squareup/sdk/pos/TransactionRequest;->autoReturn:Z

    if-eq v1, v3, :cond_3

    return v2

    .line 127
    :cond_3
    iget-object v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->tenderTypes:Ljava/util/Set;

    iget-object v3, p1, Lcom/squareup/sdk/pos/TransactionRequest;->tenderTypes:Ljava/util/Set;

    invoke-interface {v1, v3}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    return v2

    .line 130
    :cond_4
    iget-object v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->currencyCode:Lcom/squareup/sdk/pos/CurrencyCode;

    iget-object v3, p1, Lcom/squareup/sdk/pos/TransactionRequest;->currencyCode:Lcom/squareup/sdk/pos/CurrencyCode;

    if-eq v1, v3, :cond_5

    return v2

    .line 133
    :cond_5
    iget-object v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->note:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v3, p1, Lcom/squareup/sdk/pos/TransactionRequest;->note:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto :goto_0

    :cond_6
    iget-object v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->note:Ljava/lang/String;

    if-eqz v1, :cond_7

    :goto_0
    return v2

    .line 136
    :cond_7
    iget-object v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->locationId:Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v3, p1, Lcom/squareup/sdk/pos/TransactionRequest;->locationId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto :goto_1

    :cond_8
    iget-object v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->locationId:Ljava/lang/String;

    if-eqz v1, :cond_9

    :goto_1
    return v2

    .line 139
    :cond_9
    iget-object v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->state:Ljava/lang/String;

    if-eqz v1, :cond_a

    iget-object v3, p1, Lcom/squareup/sdk/pos/TransactionRequest;->state:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto :goto_2

    :cond_a
    iget-object v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->state:Ljava/lang/String;

    if-eqz v1, :cond_b

    :goto_2
    return v2

    .line 142
    :cond_b
    iget-object v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->customerId:Ljava/lang/String;

    if-eqz v1, :cond_c

    iget-object v3, p1, Lcom/squareup/sdk/pos/TransactionRequest;->customerId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    goto :goto_3

    :cond_c
    iget-object v1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->customerId:Ljava/lang/String;

    if-eqz v1, :cond_d

    :goto_3
    return v2

    .line 145
    :cond_d
    iget-boolean v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->skipReceipt:Z

    iget-boolean v3, p1, Lcom/squareup/sdk/pos/TransactionRequest;->skipReceipt:Z

    if-eq v1, v3, :cond_e

    return v2

    .line 148
    :cond_e
    iget-boolean v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->allowSplitTender:Z

    iget-boolean p1, p1, Lcom/squareup/sdk/pos/TransactionRequest;->allowSplitTender:Z

    if-eq v1, p1, :cond_f

    return v2

    :cond_f
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 155
    iget-object v0, p0, Lcom/squareup/sdk/pos/TransactionRequest;->tenderTypes:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 156
    iget v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->totalAmount:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 157
    iget-object v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->currencyCode:Lcom/squareup/sdk/pos/CurrencyCode;

    invoke-virtual {v1}, Lcom/squareup/sdk/pos/CurrencyCode;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 158
    iget-object v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->note:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 159
    iget-boolean v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->autoReturn:Z

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 160
    iget-object v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->locationId:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 161
    iget-object v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->state:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 162
    iget-object v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->customerId:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 163
    iget-boolean v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->skipReceipt:Z

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 164
    iget-boolean v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->allowSplitTender:Z

    add-int/2addr v0, v1

    return v0
.end method

.method public newBuilder()Lcom/squareup/sdk/pos/TransactionRequest$Builder;
    .locals 2

    .line 95
    iget v0, p0, Lcom/squareup/sdk/pos/TransactionRequest;->totalAmount:I

    iget-object v1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->currencyCode:Lcom/squareup/sdk/pos/CurrencyCode;

    invoke-virtual {p0, v0, v1}, Lcom/squareup/sdk/pos/TransactionRequest;->newBuilder(ILcom/squareup/sdk/pos/CurrencyCode;)Lcom/squareup/sdk/pos/TransactionRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder(ILcom/squareup/sdk/pos/CurrencyCode;)Lcom/squareup/sdk/pos/TransactionRequest$Builder;
    .locals 1

    .line 102
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$Builder;

    invoke-direct {v0, p1, p2}, Lcom/squareup/sdk/pos/TransactionRequest$Builder;-><init>(ILcom/squareup/sdk/pos/CurrencyCode;)V

    iget-object p1, p0, Lcom/squareup/sdk/pos/TransactionRequest;->tenderTypes:Ljava/util/Set;

    .line 103
    invoke-virtual {v0, p1}, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->restrictTendersTo(Ljava/util/Collection;)Lcom/squareup/sdk/pos/TransactionRequest$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/sdk/pos/TransactionRequest;->note:Ljava/lang/String;

    .line 104
    invoke-virtual {p1, p2}, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->note(Ljava/lang/String;)Lcom/squareup/sdk/pos/TransactionRequest$Builder;

    move-result-object p1

    iget-boolean p2, p0, Lcom/squareup/sdk/pos/TransactionRequest;->autoReturn:Z

    .line 105
    invoke-virtual {p1, p2}, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->autoReturn(Z)Lcom/squareup/sdk/pos/TransactionRequest$Builder;

    move-result-object p1

    iget-boolean p2, p0, Lcom/squareup/sdk/pos/TransactionRequest;->skipReceipt:Z

    .line 106
    invoke-virtual {p1, p2}, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->skipReceipt(Z)Lcom/squareup/sdk/pos/TransactionRequest$Builder;

    move-result-object p1

    iget-boolean p2, p0, Lcom/squareup/sdk/pos/TransactionRequest;->allowSplitTender:Z

    .line 107
    invoke-virtual {p1, p2}, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->allowSplitTender(Z)Lcom/squareup/sdk/pos/TransactionRequest$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/sdk/pos/TransactionRequest;->locationId:Ljava/lang/String;

    .line 108
    invoke-virtual {p1, p2}, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->enforceBusinessLocation(Ljava/lang/String;)Lcom/squareup/sdk/pos/TransactionRequest$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/sdk/pos/TransactionRequest;->state:Ljava/lang/String;

    .line 109
    invoke-virtual {p1, p2}, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->state(Ljava/lang/String;)Lcom/squareup/sdk/pos/TransactionRequest$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/sdk/pos/TransactionRequest;->customerId:Ljava/lang/String;

    .line 110
    invoke-virtual {p1, p2}, Lcom/squareup/sdk/pos/TransactionRequest$Builder;->customerId(Ljava/lang/String;)Lcom/squareup/sdk/pos/TransactionRequest$Builder;

    move-result-object p1

    return-object p1
.end method
