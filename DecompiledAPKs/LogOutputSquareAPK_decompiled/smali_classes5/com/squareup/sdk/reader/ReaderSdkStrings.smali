.class public final Lcom/squareup/sdk/reader/ReaderSdkStrings;
.super Ljava/lang/Object;
.source "ReaderSdkStrings.java"


# static fields
.field public static final ENTRY_ACTIVITY:Ljava/lang/String; = "com.squareup.ui.ApiActivity"

.field public static final EXTRA_ALLOW_SPLIT_TENDER:Ljava/lang/String; = "com.squareup.pos.ALLOW_SPLIT_TENDER"

.field public static final EXTRA_API_VERSION:Ljava/lang/String; = "com.squareup.pos.API_VERSION"

.field public static final EXTRA_APPLICATION_ID:Ljava/lang/String; = "com.squareup.pos.CLIENT_ID"

.field public static final EXTRA_AUTO_RETURN_TIMEOUT_MS:Ljava/lang/String; = "com.squareup.pos.AUTO_RETURN_TIMEOUT_MS"

.field public static final EXTRA_CURRENCY_CODE:Ljava/lang/String; = "com.squareup.pos.CURRENCY_CODE"

.field public static final EXTRA_CUSTOMER_ID:Ljava/lang/String; = "com.squareup.pos.CUSTOMER_ID"

.field public static final EXTRA_CUSTOM_PERCENTAGES:Ljava/lang/String; = "com.squareup.pos.CUSTOM_TIP_PERCENTAGES"

.field public static final EXTRA_DELAY_CAPTURE:Ljava/lang/String; = "com.squareup.pos.DELAY_CAPTURE"

.field public static final EXTRA_ENABLE_TIPPING:Ljava/lang/String; = "com.squareup.pos.ENABLE_TIPPING"

.field public static final EXTRA_NOTE:Ljava/lang/String; = "com.squareup.pos.NOTE"

.field public static final EXTRA_SDK_VERSION:Ljava/lang/String; = "com.squareup.pos.SDK_VERSION"

.field public static final EXTRA_SEPARATE_TIP_SCREEN:Ljava/lang/String; = "com.squareup.pos.SEPARATE_TIP_SCREEN"

.field public static final EXTRA_SHOW_CUSTOM_TIP:Ljava/lang/String; = "com.squareup.pos.SHOW_CUSTOM_TIP"

.field public static final EXTRA_SKIP_RECEIPT:Ljava/lang/String; = "com.squareup.pos.SKIP_RECEIPT"

.field public static final EXTRA_SKIP_SIGNATURE:Ljava/lang/String; = "com.squareup.pos.SKIP_SIGNATURE"

.field public static final EXTRA_TENDER_CASH:Ljava/lang/String; = "com.squareup.pos.TENDER_CASH"

.field public static final EXTRA_TENDER_KEYED_IN_CARD:Ljava/lang/String; = "com.squareup.pos.TENDER_KEYED_IN_CARD"

.field public static final EXTRA_TENDER_OTHER:Ljava/lang/String; = "com.squareup.pos.TENDER_OTHER"

.field public static final EXTRA_TENDER_TYPES:Ljava/lang/String; = "com.squareup.pos.TENDER_TYPES"

.field public static final EXTRA_TOTAL_AMOUNT_LONG:Ljava/lang/String; = "com.squareup.pos.TOTAL_AMOUNT_LONG"

.field public static final EXTRA_USED_DEPRECATED_PARAMETERS:Ljava/lang/String; = "com.squareup.pos.USED_DEPRECATED_PARAMETERS"

.field public static final INTENT_ACTION_CHARGE:Ljava/lang/String; = "com.squareup.pos.action.CHARGE"

.field public static final INTENT_ACTION_CONNECT_READER:Ljava/lang/String; = "com.squareup.pos.action.CONNECT_READER"

.field public static final INTENT_ACTION_STORE_CARD:Ljava/lang/String; = "com.squareup.pos.action.STORE_CARD"

.field private static final NAMESPACE:Ljava/lang/String; = "com.squareup.pos."

.field public static final RESULT_ERROR_CODE:Ljava/lang/String; = "com.squareup.pos.ERROR_CODE"

.field public static final RESULT_ERROR_DESCRIPTION:Ljava/lang/String; = "com.squareup.pos.ERROR_DESCRIPTION"

.field public static final RESULT_LOCATION_ID:Ljava/lang/String; = "com.squareup.pos.CLIENT_LOCATION_ID"


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method
