.class public final Lcom/squareup/sdk/reader/authorization/AuthorizationState;
.super Ljava/lang/Object;
.source "AuthorizationState.java"


# instance fields
.field private final authorizationInProgress:Z

.field private final canDeauthorize:Z

.field private final location:Lcom/squareup/sdk/reader/authorization/Location;


# direct methods
.method private constructor <init>(Lcom/squareup/sdk/reader/authorization/Location;ZZ)V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->location:Lcom/squareup/sdk/reader/authorization/Location;

    .line 49
    iput-boolean p2, p0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->canDeauthorize:Z

    .line 50
    iput-boolean p3, p0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->authorizationInProgress:Z

    return-void
.end method

.method public static newAuthorizedState(Lcom/squareup/sdk/reader/authorization/Location;Z)Lcom/squareup/sdk/reader/authorization/AuthorizationState;
    .locals 2

    const-string v0, "location"

    .line 20
    invoke-static {p0, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 21
    new-instance v0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/sdk/reader/authorization/AuthorizationState;-><init>(Lcom/squareup/sdk/reader/authorization/Location;ZZ)V

    return-object v0
.end method

.method public static newInProgressState()Lcom/squareup/sdk/reader/authorization/AuthorizationState;
    .locals 3

    .line 39
    new-instance v0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/sdk/reader/authorization/AuthorizationState;-><init>(Lcom/squareup/sdk/reader/authorization/Location;ZZ)V

    return-object v0
.end method

.method public static newUnauthorizedState()Lcom/squareup/sdk/reader/authorization/AuthorizationState;
    .locals 4

    .line 30
    new-instance v0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/sdk/reader/authorization/AuthorizationState;-><init>(Lcom/squareup/sdk/reader/authorization/Location;ZZ)V

    return-object v0
.end method


# virtual methods
.method public canDeauthorize()Z
    .locals 1

    .line 77
    iget-boolean v0, p0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->canDeauthorize:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_6

    .line 94
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 96
    :cond_1
    check-cast p1, Lcom/squareup/sdk/reader/authorization/AuthorizationState;

    .line 98
    iget-boolean v2, p0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->canDeauthorize:Z

    iget-boolean v3, p1, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->canDeauthorize:Z

    if-eq v2, v3, :cond_2

    return v1

    .line 99
    :cond_2
    iget-boolean v2, p0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->authorizationInProgress:Z

    iget-boolean v3, p1, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->authorizationInProgress:Z

    if-eq v2, v3, :cond_3

    return v1

    .line 100
    :cond_3
    iget-object v2, p0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->location:Lcom/squareup/sdk/reader/authorization/Location;

    iget-object p1, p1, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->location:Lcom/squareup/sdk/reader/authorization/Location;

    if-eqz v2, :cond_4

    invoke-virtual {v2, p1}, Lcom/squareup/sdk/reader/authorization/Location;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    goto :goto_0

    :cond_4
    if-eqz p1, :cond_5

    :goto_0
    return v1

    :cond_5
    return v0

    :cond_6
    :goto_1
    return v1
.end method

.method public getAuthorizedLocation()Lcom/squareup/sdk/reader/authorization/Location;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 85
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->isAuthorized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->location:Lcom/squareup/sdk/reader/authorization/Location;

    return-object v0

    .line 86
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call getLocation() when isAuthorized() returns false"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hashCode()I
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->location:Lcom/squareup/sdk/reader/authorization/Location;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/sdk/reader/authorization/Location;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 107
    iget-boolean v1, p0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->canDeauthorize:Z

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 108
    iget-boolean v1, p0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->authorizationInProgress:Z

    add-int/2addr v0, v1

    return v0
.end method

.method public isAuthorizationInProgress()Z
    .locals 1

    .line 65
    iget-boolean v0, p0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->authorizationInProgress:Z

    return v0
.end method

.method public isAuthorized()Z
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->location:Lcom/squareup/sdk/reader/authorization/Location;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AuthorizationState{location="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->location:Lcom/squareup/sdk/reader/authorization/Location;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", canDeauthorize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->canDeauthorize:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", authorizationInProgress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/sdk/reader/authorization/AuthorizationState;->authorizationInProgress:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
