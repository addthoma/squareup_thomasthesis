.class public final Lcom/squareup/sdk/reader/checkout/CheckoutParameters;
.super Ljava/lang/Object;
.source "CheckoutParameters.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
    }
.end annotation


# instance fields
.field private final additionalPaymentTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/reader/checkout/AdditionalPaymentType;",
            ">;"
        }
    .end annotation
.end field

.field private final allowSplitTender:Z

.field private final amountMoney:Lcom/squareup/sdk/reader/checkout/Money;

.field private final collectSignature:Z

.field private final delayCapture:Z

.field private final note:Ljava/lang/String;

.field private final skipReceipt:Z

.field private final tipSettings:Lcom/squareup/sdk/reader/checkout/TipSettings;

.field private final usedDeprecatedAlwaysRequireSignature:Z


# direct methods
.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)V
    .locals 1

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->access$100(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/squareup/sdk/reader/checkout/AdditionalPaymentType;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    goto :goto_0

    .line 48
    :cond_0
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->access$100(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v0

    .line 46
    :goto_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->additionalPaymentTypes:Ljava/util/Set;

    .line 49
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->access$200(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->amountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    .line 50
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->access$300(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->note:Ljava/lang/String;

    .line 51
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->access$400(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Lcom/squareup/sdk/reader/checkout/TipSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->tipSettings:Lcom/squareup/sdk/reader/checkout/TipSettings;

    .line 52
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->access$500(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->skipReceipt:Z

    .line 53
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->access$600(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->allowSplitTender:Z

    .line 54
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->access$700(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->collectSignature:Z

    .line 55
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->access$800(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->usedDeprecatedAlwaysRequireSignature:Z

    .line 56
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->access$900(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->delayCapture:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;Lcom/squareup/sdk/reader/checkout/CheckoutParameters$1;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;-><init>(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)V

    return-void
.end method

.method public static newBuilder(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
    .locals 2

    .line 24
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/CheckoutParameters$1;)V

    return-object v0
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
    .locals 3

    .line 125
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->amountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/CheckoutParameters$1;)V

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->additionalPaymentTypes:Ljava/util/Set;

    .line 126
    invoke-virtual {v0, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->additionalPaymentTypes(Ljava/util/Set;)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->skipReceipt:Z

    .line 127
    invoke-virtual {v0, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->skipReceipt(Z)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->allowSplitTender:Z

    .line 128
    invoke-virtual {v0, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->allowSplitTender(Z)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->collectSignature:Z

    .line 129
    invoke-virtual {v0, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->collectSignature(Z)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->delayCapture:Z

    .line 130
    invoke-virtual {v0, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->delayCapture(Z)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;

    move-result-object v0

    .line 131
    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->note:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 132
    invoke-virtual {v0, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->note(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;

    .line 134
    :cond_0
    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->tipSettings:Lcom/squareup/sdk/reader/checkout/TipSettings;

    if-eqz v1, :cond_1

    .line 135
    invoke-virtual {v0, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->tipSettings(Lcom/squareup/sdk/reader/checkout/TipSettings;)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;

    :cond_1
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 142
    :cond_0
    instance-of v1, p1, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 143
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    .line 145
    :cond_2
    check-cast p1, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;

    .line 147
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getAllowSplitTender()Z

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getAllowSplitTender()Z

    move-result v3

    if-eq v1, v3, :cond_3

    return v2

    .line 148
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getCollectSignature()Z

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getCollectSignature()Z

    move-result v3

    if-eq v1, v3, :cond_4

    return v2

    .line 149
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getSkipReceipt()Z

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getSkipReceipt()Z

    move-result v3

    if-eq v1, v3, :cond_5

    return v2

    .line 150
    :cond_5
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getUsedDeprecatedAlwaysRequireSignature()Z

    move-result v1

    .line 151
    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getUsedDeprecatedAlwaysRequireSignature()Z

    move-result v3

    if-eq v1, v3, :cond_6

    return v2

    .line 154
    :cond_6
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getDelayCapture()Z

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getDelayCapture()Z

    move-result v3

    if-eq v1, v3, :cond_7

    return v2

    .line 155
    :cond_7
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getAmountMoney()Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getAmountMoney()Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/sdk/reader/checkout/Money;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    return v2

    .line 156
    :cond_8
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getNote()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getNote()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getNote()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto :goto_0

    :cond_9
    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getNote()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_a

    :goto_0
    return v2

    .line 159
    :cond_a
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getAdditionalPaymentTypes()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getAdditionalPaymentTypes()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    return v2

    .line 160
    :cond_b
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getTipSettings()Lcom/squareup/sdk/reader/checkout/TipSettings;

    move-result-object v1

    if-eqz v1, :cond_c

    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getTipSettings()Lcom/squareup/sdk/reader/checkout/TipSettings;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getTipSettings()Lcom/squareup/sdk/reader/checkout/TipSettings;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/sdk/reader/checkout/TipSettings;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_d

    goto :goto_1

    .line 161
    :cond_c
    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getTipSettings()Lcom/squareup/sdk/reader/checkout/TipSettings;

    move-result-object p1

    if-eqz p1, :cond_d

    :goto_1
    return v2

    :cond_d
    return v0
.end method

.method public getAdditionalPaymentTypes()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/reader/checkout/AdditionalPaymentType;",
            ">;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->additionalPaymentTypes:Ljava/util/Set;

    return-object v0
.end method

.method public getAllowSplitTender()Z
    .locals 1

    .line 76
    iget-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->allowSplitTender:Z

    return v0
.end method

.method public getAlwaysRequireSignature()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 105
    iget-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->collectSignature:Z

    return v0
.end method

.method public getAmountMoney()Lcom/squareup/sdk/reader/checkout/Money;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->amountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object v0
.end method

.method public getCollectSignature()Z
    .locals 1

    .line 91
    iget-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->collectSignature:Z

    return v0
.end method

.method public getDelayCapture()Z
    .locals 1

    .line 118
    iget-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->delayCapture:Z

    return v0
.end method

.method public getNote()Ljava/lang/String;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->note:Ljava/lang/String;

    return-object v0
.end method

.method public getSkipReceipt()Z
    .locals 1

    .line 110
    iget-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->skipReceipt:Z

    return v0
.end method

.method public getTipSettings()Lcom/squareup/sdk/reader/checkout/TipSettings;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->tipSettings:Lcom/squareup/sdk/reader/checkout/TipSettings;

    return-object v0
.end method

.method public getUsedDeprecatedAlwaysRequireSignature()Z
    .locals 1

    .line 114
    iget-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->usedDeprecatedAlwaysRequireSignature:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 169
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 170
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getAmountMoney()Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/Money;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 171
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getNote()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getNote()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 172
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getAdditionalPaymentTypes()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 173
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getAllowSplitTender()Z

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 174
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getCollectSignature()Z

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 175
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getTipSettings()Lcom/squareup/sdk/reader/checkout/TipSettings;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getTipSettings()Lcom/squareup/sdk/reader/checkout/TipSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/TipSettings;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 176
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getSkipReceipt()Z

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 177
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getUsedDeprecatedAlwaysRequireSignature()Z

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 178
    invoke-virtual {p0}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->getDelayCapture()Z

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CheckoutParameters{amountMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->amountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", note=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", additionalPaymentTypes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->additionalPaymentTypes:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", allowSplitTender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->allowSplitTender:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", collectSignature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->collectSignature:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", tipSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->tipSettings:Lcom/squareup/sdk/reader/checkout/TipSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", skipReceipt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->skipReceipt:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", usedDeprecatedAlwaysRequireSignature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->usedDeprecatedAlwaysRequireSignature:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", delayCapture="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;->delayCapture:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
