.class public final Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;
.super Ljava/lang/Object;
.source "TenderCardDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/reader/checkout/TenderCardDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private card:Lcom/squareup/sdk/reader/checkout/Card;

.field private cardReceiptDetails:Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

.field private entryMethod:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;


# direct methods
.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/Card;Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;)V
    .locals 0

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;->card:Lcom/squareup/sdk/reader/checkout/Card;

    .line 127
    iput-object p2, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;->entryMethod:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    .line 128
    iput-object p3, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;->cardReceiptDetails:Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/Card;Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;Lcom/squareup/sdk/reader/checkout/TenderCardDetails$1;)V
    .locals 0

    .line 120
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Card;Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;)V

    return-void
.end method

.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/TenderCardDetails;)V
    .locals 2

    .line 132
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->access$500(Lcom/squareup/sdk/reader/checkout/TenderCardDetails;)Lcom/squareup/sdk/reader/checkout/Card;

    move-result-object v0

    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->access$600(Lcom/squareup/sdk/reader/checkout/TenderCardDetails;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    move-result-object v1

    .line 133
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->access$700(Lcom/squareup/sdk/reader/checkout/TenderCardDetails;)Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    move-result-object p1

    .line 132
    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Card;Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/TenderCardDetails;Lcom/squareup/sdk/reader/checkout/TenderCardDetails$1;)V
    .locals 0

    .line 120
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/TenderCardDetails;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;)Lcom/squareup/sdk/reader/checkout/Card;
    .locals 0

    .line 120
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;->card:Lcom/squareup/sdk/reader/checkout/Card;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;
    .locals 0

    .line 120
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;->entryMethod:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;)Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;
    .locals 0

    .line 120
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;->cardReceiptDetails:Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/sdk/reader/checkout/TenderCardDetails;
    .locals 2

    .line 159
    new-instance v0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;-><init>(Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;Lcom/squareup/sdk/reader/checkout/TenderCardDetails$1;)V

    return-object v0
.end method

.method public card(Lcom/squareup/sdk/reader/checkout/Card;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;
    .locals 1

    const-string v0, "card"

    .line 138
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/Card;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;->card:Lcom/squareup/sdk/reader/checkout/Card;

    return-object p0
.end method

.method public cardReceiptDetails(Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;
    .locals 1

    const-string v0, "cardReceiptDetails"

    .line 150
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;->cardReceiptDetails:Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    return-object p0
.end method

.method public entryMethod(Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;
    .locals 1

    const-string v0, "entryMethod"

    .line 144
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;->entryMethod:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    return-object p0
.end method
