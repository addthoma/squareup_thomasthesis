.class public interface abstract Lcom/squareup/sdk/reader/checkout/CheckoutManager;
.super Ljava/lang/Object;
.source "CheckoutManager.java"


# virtual methods
.method public abstract addCheckoutActivityCallback(Lcom/squareup/sdk/reader/checkout/CheckoutActivityCallback;)Lcom/squareup/sdk/reader/core/CallbackReference;
.end method

.method public abstract startCheckoutActivity(Landroid/content/Context;Lcom/squareup/sdk/reader/checkout/CheckoutParameters;)V
.end method
