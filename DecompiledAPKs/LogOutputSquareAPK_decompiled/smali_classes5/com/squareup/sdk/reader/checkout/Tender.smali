.class public final Lcom/squareup/sdk/reader/checkout/Tender;
.super Ljava/lang/Object;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/reader/checkout/Tender$Builder;,
        Lcom/squareup/sdk/reader/checkout/Tender$Type;
    }
.end annotation


# instance fields
.field private final cardDetails:Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

.field private final cashDetails:Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

.field private final createdAt:Ljava/util/Date;

.field private final tenderId:Ljava/lang/String;

.field private final tipMoney:Lcom/squareup/sdk/reader/checkout/Money;

.field private final totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

.field private final type:Lcom/squareup/sdk/reader/checkout/Tender$Type;


# direct methods
.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)V
    .locals 3

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->access$100(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->tenderId:Ljava/lang/String;

    .line 77
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->access$200(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->createdAt:Ljava/util/Date;

    .line 78
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->access$300(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object v0

    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->access$400(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 85
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->access$400(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    .line 86
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->access$300(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->tipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    .line 87
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->access$500(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Lcom/squareup/sdk/reader/checkout/Tender$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    .line 88
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->access$600(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->cardDetails:Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    .line 89
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->access$700(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Tender;->cashDetails:Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    return-void

    .line 79
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "All moneys in a tender must have the same currency code. totalMoney currency is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->access$400(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " but tipMoney currency is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->access$300(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/Tender$Builder;Lcom/squareup/sdk/reader/checkout/Tender$1;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/Tender;-><init>(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/sdk/reader/checkout/Tender;)Lcom/squareup/sdk/reader/checkout/Tender$Type;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/sdk/reader/checkout/Tender;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->tenderId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/sdk/reader/checkout/Tender;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->cardDetails:Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/sdk/reader/checkout/Tender;)Lcom/squareup/sdk/reader/checkout/TenderCashDetails;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->cashDetails:Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/sdk/reader/checkout/Tender;)Ljava/util/Date;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->createdAt:Ljava/util/Date;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/squareup/sdk/reader/checkout/Tender;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->tipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/sdk/reader/checkout/Tender;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method public static newCardTenderBuilder(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/TenderCardDetails;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;
    .locals 8

    const-string v0, "tenderId"

    .line 36
    invoke-static {p0, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string/jumbo v0, "totalMoney"

    .line 37
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "cardDetails"

    .line 38
    invoke-static {p2, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 39
    new-instance v0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;

    sget-object v3, Lcom/squareup/sdk/reader/checkout/Tender$Type;->CARD:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p1

    move-object v4, p0

    move-object v5, p2

    invoke-direct/range {v1 .. v7}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/Tender$Type;Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/TenderCardDetails;Lcom/squareup/sdk/reader/checkout/TenderCashDetails;Lcom/squareup/sdk/reader/checkout/Tender$1;)V

    return-object v0
.end method

.method public static newCashTenderBuilder(Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/TenderCashDetails;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;
    .locals 8

    const-string/jumbo v0, "totalMoney"

    .line 51
    invoke-static {p0, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "cashDetails"

    .line 52
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 53
    new-instance v0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;

    sget-object v3, Lcom/squareup/sdk/reader/checkout/Tender$Type;->CASH:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/Tender$Type;Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/TenderCardDetails;Lcom/squareup/sdk/reader/checkout/TenderCashDetails;Lcom/squareup/sdk/reader/checkout/Tender$1;)V

    return-object v0
.end method

.method public static newOtherTenderBuilder(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;
    .locals 8

    .line 64
    new-instance v7, Lcom/squareup/sdk/reader/checkout/Tender$Builder;

    sget-object v2, Lcom/squareup/sdk/reader/checkout/Tender$Type;->OTHER:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/Tender$Type;Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/TenderCardDetails;Lcom/squareup/sdk/reader/checkout/TenderCashDetails;Lcom/squareup/sdk/reader/checkout/Tender$1;)V

    return-object v7
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/sdk/reader/checkout/Tender$Builder;
    .locals 2

    .line 168
    new-instance v0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Tender;Lcom/squareup/sdk/reader/checkout/Tender$1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_c

    .line 173
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_3

    .line 175
    :cond_1
    check-cast p1, Lcom/squareup/sdk/reader/checkout/Tender;

    .line 177
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Tender;->tenderId:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/Tender;->tenderId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/squareup/sdk/reader/checkout/Tender;->tenderId:Ljava/lang/String;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 180
    :cond_3
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Tender;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/Tender;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v2, v3}, Lcom/squareup/sdk/reader/checkout/Money;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    return v1

    .line 181
    :cond_4
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Tender;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/Tender;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    if-eq v2, v3, :cond_5

    return v1

    .line 182
    :cond_5
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Tender;->cardDetails:Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    if-eqz v2, :cond_6

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/Tender;->cardDetails:Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    invoke-virtual {v2, v3}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    goto :goto_1

    :cond_6
    iget-object v2, p1, Lcom/squareup/sdk/reader/checkout/Tender;->cardDetails:Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    if-eqz v2, :cond_7

    :goto_1
    return v1

    .line 186
    :cond_7
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Tender;->cashDetails:Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    if-eqz v2, :cond_8

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/Tender;->cashDetails:Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    invoke-virtual {v2, v3}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    goto :goto_2

    :cond_8
    iget-object v2, p1, Lcom/squareup/sdk/reader/checkout/Tender;->cashDetails:Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    if-eqz v2, :cond_9

    :goto_2
    return v1

    .line 190
    :cond_9
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Tender;->createdAt:Ljava/util/Date;

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/Tender;->createdAt:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    return v1

    .line 191
    :cond_a
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Tender;->tipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    iget-object p1, p1, Lcom/squareup/sdk/reader/checkout/Tender;->tipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v2, p1}, Lcom/squareup/sdk/reader/checkout/Money;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_b

    return v1

    :cond_b
    return v0

    :cond_c
    :goto_3
    return v1
.end method

.method public getCardDetails()Lcom/squareup/sdk/reader/checkout/TenderCardDetails;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 127
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    sget-object v1, Lcom/squareup/sdk/reader/checkout/Tender$Type;->CARD:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    if-ne v0, v1, :cond_0

    .line 131
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->cardDetails:Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    return-object v0

    .line 128
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot call getCardDetails() when getType() returns "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Tender;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCashDetails()Lcom/squareup/sdk/reader/checkout/TenderCashDetails;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 140
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    sget-object v1, Lcom/squareup/sdk/reader/checkout/Tender$Type;->CASH:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    if-ne v0, v1, :cond_0

    .line 144
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->cashDetails:Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    return-object v0

    .line 141
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot call getCashDetails() when getType() returns "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Tender;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCreatedAt()Ljava/util/Date;
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->createdAt:Ljava/util/Date;

    return-object v0
.end method

.method public getTenderId()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 100
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    sget-object v1, Lcom/squareup/sdk/reader/checkout/Tender$Type;->CARD:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    if-ne v0, v1, :cond_0

    .line 104
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->tenderId:Ljava/lang/String;

    return-object v0

    .line 101
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot call getTenderId() when getType() returns "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Tender;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getTipMoney()Lcom/squareup/sdk/reader/checkout/Money;
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->tipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object v0
.end method

.method public getTotalMoney()Lcom/squareup/sdk/reader/checkout/Money;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object v0
.end method

.method public getType()Lcom/squareup/sdk/reader/checkout/Tender$Type;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .line 197
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender;->tenderId:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 198
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Tender;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v2}, Lcom/squareup/sdk/reader/checkout/Money;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 199
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Tender;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    invoke-virtual {v2}, Lcom/squareup/sdk/reader/checkout/Tender$Type;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 200
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Tender;->cardDetails:Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 201
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Tender;->cashDetails:Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 202
    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/Tender;->createdAt:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 203
    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/Tender;->tipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/Money;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Tender{tenderId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/Tender;->tenderId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", totalMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/Tender;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/Tender;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cardDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/Tender;->cardDetails:Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cashDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/Tender;->cashDetails:Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/Tender;->createdAt:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tipMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/Tender;->tipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
