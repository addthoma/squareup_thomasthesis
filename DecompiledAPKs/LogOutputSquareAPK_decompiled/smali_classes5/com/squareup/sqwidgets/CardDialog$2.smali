.class Lcom/squareup/sqwidgets/CardDialog$2;
.super Ljava/lang/Object;
.source "CardDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/sqwidgets/CardDialog;->setAction(ZLjava/lang/Runnable;)Lcom/squareup/sqwidgets/CardDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/sqwidgets/CardDialog;

.field final synthetic val$onAction:Ljava/lang/Runnable;

.field final synthetic val$shouldDismiss:Z


# direct methods
.method constructor <init>(Lcom/squareup/sqwidgets/CardDialog;Ljava/lang/Runnable;Z)V
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/sqwidgets/CardDialog$2;->this$0:Lcom/squareup/sqwidgets/CardDialog;

    iput-object p2, p0, Lcom/squareup/sqwidgets/CardDialog$2;->val$onAction:Ljava/lang/Runnable;

    iput-boolean p3, p0, Lcom/squareup/sqwidgets/CardDialog$2;->val$shouldDismiss:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0

    .line 122
    iget-object p1, p0, Lcom/squareup/sqwidgets/CardDialog$2;->val$onAction:Ljava/lang/Runnable;

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 123
    iget-boolean p1, p0, Lcom/squareup/sqwidgets/CardDialog$2;->val$shouldDismiss:Z

    if-eqz p1, :cond_0

    .line 124
    iget-object p1, p0, Lcom/squareup/sqwidgets/CardDialog$2;->this$0:Lcom/squareup/sqwidgets/CardDialog;

    invoke-virtual {p1}, Lcom/squareup/sqwidgets/CardDialog;->dismiss()V

    :cond_0
    return-void
.end method
