.class public final Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy;
.super Lcom/squareup/sqwidgets/time/PickerStrategy;
.source "PickerStrategy.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPickerStrategy.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PickerStrategy.kt\ncom/squareup/sqwidgets/time/TwelveHourPickerStrategy\n*L\n1#1,111:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0000\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bBz\u0012U\u0010\u0002\u001aQ\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\u0007\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\n\u0012\u0013\u0012\u00110\u000b\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\u000c\u0012\u0004\u0012\u00020\r0\u0003j\u0002`\u000e\u0012\u0008\u0008\u0001\u0010\u000f\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0010\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0011\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0012J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0015H\u0016R\u000e\u0010\u0013\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u00020\u00158VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy;",
        "Lcom/squareup/sqwidgets/time/PickerStrategy;",
        "pickerFactory",
        "Lkotlin/Function3;",
        "",
        "Lkotlin/ParameterName;",
        "name",
        "id",
        "",
        "",
        "values",
        "",
        "isInfiniteScroll",
        "Lcom/squareup/sqwidgets/time/Picker;",
        "Lcom/squareup/sqwidgets/time/PickerFactory;",
        "hourPickerId",
        "minutePickerId",
        "amPmPickerId",
        "(Lkotlin/jvm/functions/Function3;III)V",
        "amPmPicker",
        "time",
        "Ljava/util/Calendar;",
        "getTime",
        "()Ljava/util/Calendar;",
        "setDisplayedTime",
        "",
        "currentTime",
        "Companion",
        "x2widgets_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy$Companion;

.field private static final HOUR_PICKER_END:I = 0xc

.field private static final HOUR_PICKER_START:I = 0x1


# instance fields
.field private final amPmPicker:Lcom/squareup/sqwidgets/time/Picker;

.field private final amPmPickerId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy;->Companion:Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy$Companion;

    return-void
.end method

.method public constructor <init>(Lkotlin/jvm/functions/Function3;III)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;-",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/sqwidgets/time/Picker;",
            ">;III)V"
        }
    .end annotation

    const-string v0, "pickerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x1

    const/16 v4, 0xc

    move-object v1, p0

    move-object v2, p1

    move v5, p2

    move v6, p3

    .line 42
    invoke-direct/range {v1 .. v6}, Lcom/squareup/sqwidgets/time/PickerStrategy;-><init>(Lkotlin/jvm/functions/Function3;IIII)V

    iput p4, p0, Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy;->amPmPickerId:I

    .line 63
    iget p2, p0, Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy;->amPmPickerId:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-static {}, Lcom/squareup/sqwidgets/time/AmPm;->allDisplayNames()Ljava/util/List;

    move-result-object p3

    const-string p4, "AmPm.allDisplayNames()"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p4, 0x0

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p4

    invoke-interface {p1, p2, p3, p4}, Lkotlin/jvm/functions/Function3;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sqwidgets/time/Picker;

    iput-object p1, p0, Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy;->amPmPicker:Lcom/squareup/sqwidgets/time/Picker;

    return-void
.end method


# virtual methods
.method public getTime()Ljava/util/Calendar;
    .locals 4

    .line 55
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 57
    invoke-virtual {p0}, Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy;->getHourAndMinutePickers()Lcom/squareup/sqwidgets/time/HourAndMinutePickers;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getHour()I

    move-result v1

    const/16 v2, 0xc

    rem-int/2addr v1, v2

    const/16 v3, 0xa

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->set(II)V

    .line 58
    invoke-virtual {p0}, Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy;->getHourAndMinutePickers()Lcom/squareup/sqwidgets/time/HourAndMinutePickers;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getMinute()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    .line 59
    iget-object v1, p0, Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy;->amPmPicker:Lcom/squareup/sqwidgets/time/Picker;

    invoke-virtual {v1}, Lcom/squareup/sqwidgets/time/Picker;->getSelectedValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/sqwidgets/time/AmPm;->forDisplayName(Ljava/lang/String;)Lcom/squareup/sqwidgets/time/AmPm;

    move-result-object v1

    const-string v2, "AmPm.forDisplayName(amPmPicker.selectedValue)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/sqwidgets/time/AmPm;->getId()I

    move-result v1

    const/16 v2, 0x9

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    const-string v1, "Calendar.getInstance()\n \u2026tedValue).id)\n          }"

    .line 56
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public setDisplayedTime(Ljava/util/Calendar;)V
    .locals 3

    const-string v0, "currentTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy;->getHourAndMinutePickers()Lcom/squareup/sqwidgets/time/HourAndMinutePickers;

    move-result-object v0

    const/16 v1, 0xa

    .line 67
    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/16 v2, 0xc

    .line 68
    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 66
    invoke-virtual {v0, v1, v2}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->selectValues(II)V

    .line 70
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy;->amPmPicker:Lcom/squareup/sqwidgets/time/Picker;

    const/16 v1, 0x9

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result p1

    invoke-static {p1}, Lcom/squareup/sqwidgets/time/AmPm;->forId(I)Lcom/squareup/sqwidgets/time/AmPm;

    move-result-object p1

    const-string v1, "AmPm.forId(currentTime.get(Calendar.AM_PM))"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/sqwidgets/time/AmPm;->getDisplayName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/sqwidgets/time/Picker;->selectValue(Ljava/lang/String;)V

    return-void
.end method
