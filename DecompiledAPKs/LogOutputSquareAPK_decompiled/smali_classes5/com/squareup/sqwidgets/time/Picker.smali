.class final Lcom/squareup/sqwidgets/time/Picker;
.super Ljava/lang/Object;
.source "Picker.java"


# instance fields
.field private final data:Lcom/squareup/sqwidgets/time/DataAdapter;

.field private final layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final scrollListener:Lcom/squareup/sqwidgets/time/SnappingScrollListener;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/squareup/sqwidgets/time/DataAdapter;Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 2

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p2, p0, Lcom/squareup/sqwidgets/time/Picker;->data:Lcom/squareup/sqwidgets/time/DataAdapter;

    .line 29
    iput-object p3, p0, Lcom/squareup/sqwidgets/time/Picker;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 30
    iget-object p3, p0, Lcom/squareup/sqwidgets/time/Picker;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p3, p2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 32
    new-instance p3, Landroidx/recyclerview/widget/LinearLayoutManager;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p3, p1, v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    iput-object p3, p0, Lcom/squareup/sqwidgets/time/Picker;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 33
    iget-object p1, p0, Lcom/squareup/sqwidgets/time/Picker;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object p3, p0, Lcom/squareup/sqwidgets/time/Picker;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p1, p3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 35
    iget-object p1, p0, Lcom/squareup/sqwidgets/time/Picker;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance p3, Landroid/widget/LinearLayout$LayoutParams;

    .line 36
    invoke-virtual {p2}, Lcom/squareup/sqwidgets/time/DataAdapter;->getViewWidth()I

    move-result v1

    .line 37
    invoke-virtual {p2}, Lcom/squareup/sqwidgets/time/DataAdapter;->getViewHeight()I

    move-result p2

    mul-int/lit8 p2, p2, 0x3

    invoke-direct {p3, v1, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 35
    invoke-virtual {p1, p3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 39
    new-instance p1, Lcom/squareup/sqwidgets/time/SnappingScrollListener;

    iget-object p2, p0, Lcom/squareup/sqwidgets/time/Picker;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {p1, p2}, Lcom/squareup/sqwidgets/time/SnappingScrollListener;-><init>(Landroidx/recyclerview/widget/LinearLayoutManager;)V

    iput-object p1, p0, Lcom/squareup/sqwidgets/time/Picker;->scrollListener:Lcom/squareup/sqwidgets/time/SnappingScrollListener;

    .line 40
    iget-object p1, p0, Lcom/squareup/sqwidgets/time/Picker;->scrollListener:Lcom/squareup/sqwidgets/time/SnappingScrollListener;

    invoke-virtual {p0, p1}, Lcom/squareup/sqwidgets/time/Picker;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    .line 41
    iget-object p1, p0, Lcom/squareup/sqwidgets/time/Picker;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    return-void
.end method


# virtual methods
.method public addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/Picker;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    return-void
.end method

.method public getHeight()I
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/Picker;->data:Lcom/squareup/sqwidgets/time/DataAdapter;

    invoke-virtual {v0}, Lcom/squareup/sqwidgets/time/DataAdapter;->getViewHeight()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    return v0
.end method

.method getSelectedValue()Ljava/lang/String;
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/Picker;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    move-result v0

    .line 52
    iget-object v1, p0, Lcom/squareup/sqwidgets/time/Picker;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v0

    .line 53
    iget-object v1, p0, Lcom/squareup/sqwidgets/time/Picker;->data:Lcom/squareup/sqwidgets/time/DataAdapter;

    invoke-virtual {v1, v0}, Lcom/squareup/sqwidgets/time/DataAdapter;->getData(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/Picker;->data:Lcom/squareup/sqwidgets/time/DataAdapter;

    invoke-virtual {v0}, Lcom/squareup/sqwidgets/time/DataAdapter;->getViewWidth()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    return v0
.end method

.method selectValue(Ljava/lang/String;)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/Picker;->data:Lcom/squareup/sqwidgets/time/DataAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/sqwidgets/time/DataAdapter;->positionOf(Ljava/lang/String;)I

    move-result p1

    .line 64
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/Picker;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    add-int/lit8 p1, p1, -0x1

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    return-void
.end method
