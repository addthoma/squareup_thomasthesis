.class public final Lcom/squareup/singlesignon/SingleSignOn$NoSingleSignOn;
.super Ljava/lang/Object;
.source "SingleSignOn.kt"

# interfaces
.implements Lcom/squareup/singlesignon/SingleSignOn;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/singlesignon/SingleSignOn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoSingleSignOn"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u000c\u001a\u00020\u0008H\u0016J\u0010\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u0008H\u0016\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/singlesignon/SingleSignOn$NoSingleSignOn;",
        "Lcom/squareup/singlesignon/SingleSignOn;",
        "()V",
        "clearSessionToken",
        "",
        "reason",
        "Lcom/squareup/singlesignon/ClearSessionReason;",
        "getSessionToken",
        "",
        "hasSessionToken",
        "",
        "logEvent",
        "message",
        "setSessionToken",
        "sessionToken",
        "singlesignon_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/singlesignon/SingleSignOn$NoSingleSignOn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/singlesignon/SingleSignOn$NoSingleSignOn;

    invoke-direct {v0}, Lcom/squareup/singlesignon/SingleSignOn$NoSingleSignOn;-><init>()V

    sput-object v0, Lcom/squareup/singlesignon/SingleSignOn$NoSingleSignOn;->INSTANCE:Lcom/squareup/singlesignon/SingleSignOn$NoSingleSignOn;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clearSessionToken()V
    .locals 0

    .line 43
    invoke-static {p0}, Lcom/squareup/singlesignon/SingleSignOn$DefaultImpls;->clearSessionToken(Lcom/squareup/singlesignon/SingleSignOn;)V

    return-void
.end method

.method public clearSessionToken(Lcom/squareup/singlesignon/ClearSessionReason;)V
    .locals 1

    const-string v0, "reason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public getSessionToken()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public hasSessionToken()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public logEvent(Ljava/lang/String;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public setSessionToken(Ljava/lang/String;)V
    .locals 1

    const-string v0, "sessionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
