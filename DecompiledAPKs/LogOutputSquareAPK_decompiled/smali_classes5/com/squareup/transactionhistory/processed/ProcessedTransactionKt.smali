.class public final Lcom/squareup/transactionhistory/processed/ProcessedTransactionKt;
.super Ljava/lang/Object;
.source "ProcessedTransaction.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u001a\u0010\u0000\u001a\u0002H\u0001\"\u0006\u0008\u0000\u0010\u0001\u0018\u0001*\u00020\u0002H\u0086\u0008\u00a2\u0006\u0002\u0010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "asBillHistory",
        "T",
        "Lcom/squareup/transactionhistory/processed/ProcessedTransaction;",
        "(Lcom/squareup/transactionhistory/processed/ProcessedTransaction;)Ljava/lang/Object;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic asBillHistory(Lcom/squareup/transactionhistory/processed/ProcessedTransaction;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/transactionhistory/processed/ProcessedTransaction;",
            ")TT;"
        }
    .end annotation

    const-string v0, "$this$asBillHistory"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransaction;->getBillHistory()Ljava/lang/Object;

    move-result-object p0

    const/4 v0, 0x1

    const-string v1, "T"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    check-cast p0, Ljava/lang/Object;

    return-object p0
.end method
