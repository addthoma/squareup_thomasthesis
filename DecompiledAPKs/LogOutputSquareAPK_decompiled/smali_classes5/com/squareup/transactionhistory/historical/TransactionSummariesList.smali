.class public final Lcom/squareup/transactionhistory/historical/TransactionSummariesList;
.super Ljava/lang/Object;
.source "TransactionSummariesList.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransactionSummariesList.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransactionSummariesList.kt\ncom/squareup/transactionhistory/historical/TransactionSummariesList\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,576:1\n1642#2,2:577\n*E\n*S KotlinDebug\n*F\n+ 1 TransactionSummariesList.kt\ncom/squareup/transactionhistory/historical/TransactionSummariesList\n*L\n438#1,2:577\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000~\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0008\u0003\n\u0002\u0010\u001e\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010$\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B\u001b\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0015\u001a\u00020\u000c2\u0006\u0010\u0016\u001a\u00020\tJ\u0018\u0010\u0015\u001a\u00020\u000c2\u0006\u0010\u0016\u001a\u00020\t2\u0006\u0010\u0017\u001a\u00020\u000cH\u0002J\u0014\u0010\u0015\u001a\u00020\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0018J\u001e\u0010\u0015\u001a\u00020\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00182\u0006\u0010\u0019\u001a\u00020\u000cH\u0002J\u000e\u0010\u001a\u001a\u00020\u000c2\u0006\u0010\u0016\u001a\u00020\tJ\u0018\u0010\u001a\u001a\u00020\u000c2\u0006\u0010\u0016\u001a\u00020\t2\u0006\u0010\u0017\u001a\u00020\u000cH\u0002J\u0014\u0010\u001a\u001a\u00020\u001b2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0018J\u001e\u0010\u001a\u001a\u00020\u001b2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00182\u0006\u0010\u0019\u001a\u00020\u000cH\u0002J\u0018\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u0016\u001a\u00020\t2\u0006\u0010\u0017\u001a\u00020\u000cH\u0002J\u0010\u0010\u001d\u001a\u00020\u000c2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0006\u0010 \u001a\u00020\u001bJ\u0016\u0010!\u001a\u00020\u001b2\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u001b0#H\u0002J\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0014J\u0010\u0010%\u001a\u0004\u0018\u00010\t2\u0006\u0010\u001e\u001a\u00020\u001fJ$\u0010&\u001a\u00020\u000c2\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\t0\'2\u0006\u0010\u0016\u001a\u00020\tH\u0002J\u0010\u0010(\u001a\u00020)2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0006\u0010*\u001a\u00020\u000cJ\u0008\u0010+\u001a\u00020\u001bH\u0002J\u0010\u0010,\u001a\u00020\u001b2\u0006\u0010\u0016\u001a\u00020\tH\u0002J\u0018\u0010-\u001a\u00020\u001b2\u0006\u0010\u0016\u001a\u00020\t2\u0006\u0010.\u001a\u00020\u000cH\u0002J\u000e\u0010/\u001a\u00020\u000c2\u0006\u0010\u0016\u001a\u00020\tJ\u0018\u0010/\u001a\u00020\u000c2\u0006\u0010\u0016\u001a\u00020\t2\u0006\u0010.\u001a\u00020\u000cH\u0002J\u0014\u0010/\u001a\u00020\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0014J\u0014\u00100\u001a\u00020\u001b2\u000c\u00101\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0018J\u001e\u00102\u001a\u00020\u001b2\u0016\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t0\u0008j\u0008\u0012\u0004\u0012\u00020\t`\nJ\u0008\u00103\u001a\u00020\u001bH\u0002J\u0012\u0010\r\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u001404J\u0016\u00105\u001a\u00020\u000c2\u0006\u00106\u001a\u00020\u001f2\u0006\u00107\u001a\u00020\u001fJ\u0018\u00108\u001a\u00020\u001b2\u0006\u00109\u001a\u00020\t2\u0006\u0010:\u001a\u00020\tH\u0002J\u000c\u0010;\u001a\u00020\u0011*\u00020\u001fH\u0002J\u000c\u0010;\u001a\u00020\u0011*\u00020\tH\u0002R\u001e\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t0\u0008j\u0008\u0012\u0004\u0012\u00020\t`\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\t0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\t0\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0012\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00140\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006<"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/historical/TransactionSummariesList;",
        "",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "mainThread",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V",
        "comparator",
        "Ljava/util/Comparator;",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
        "Lkotlin/Comparator;",
        "silenceAllNotifications",
        "",
        "summaries",
        "",
        "summariesMap",
        "",
        "",
        "summariesRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "addIfNotPresent",
        "summary",
        "sortAfterAdding",
        "",
        "sortAfterAddingEachSummary",
        "addOrSafelyReplace",
        "",
        "addSummary",
        "alreadyPresent",
        "ids",
        "Lcom/squareup/transactionhistory/TransactionIds;",
        "clearAllSummaries",
        "doSilently",
        "block",
        "Lkotlin/Function0;",
        "getSummaries",
        "getSummary",
        "hasForwardedVersionOf",
        "",
        "indexOfTransaction",
        "",
        "isEmpty",
        "notifySummariesChanged",
        "removeSummarySilently",
        "replaceSummary",
        "sortAfterReplacing",
        "safelyReplaceIfPresent",
        "safelyReplacePendingSummaries",
        "pendingSummaries",
        "setSortOrder",
        "sortSummaries",
        "Lio/reactivex/Observable;",
        "updateSummaryIdsIfPresent",
        "oldIds",
        "newIds",
        "verifyStoreAndForward",
        "existingSummary",
        "newSummary",
        "asKey",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private silenceAllNotifications:Z

.field private final summaries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;"
        }
    .end annotation
.end field

.field private final summariesMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;"
        }
    .end annotation
.end field

.field private final summariesRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1
    .param p1    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p2    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "mainScheduler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p2, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    .line 54
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summaries:Ljava/util/List;

    .line 58
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summariesMap:Ljava/util/Map;

    .line 60
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(emptyList())"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summariesRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 68
    sget-object p1, Lcom/squareup/transactionhistory/historical/HistoricalTransactionComparator;->INSTANCE:Lcom/squareup/transactionhistory/historical/HistoricalTransactionComparator;

    check-cast p1, Ljava/util/Comparator;

    iput-object p1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->comparator:Ljava/util/Comparator;

    return-void
.end method

.method public static final synthetic access$addIfNotPresent(Lcom/squareup/transactionhistory/historical/TransactionSummariesList;Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)Z
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->addIfNotPresent(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$addIfNotPresent(Lcom/squareup/transactionhistory/historical/TransactionSummariesList;Ljava/util/Collection;Z)Z
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->addIfNotPresent(Ljava/util/Collection;Z)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$safelyReplaceIfPresent(Lcom/squareup/transactionhistory/historical/TransactionSummariesList;Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)Z
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->safelyReplaceIfPresent(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)Z

    move-result p0

    return p0
.end method

.method private final addIfNotPresent(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)Z
    .locals 1

    .line 447
    invoke-static {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->getTransactionIds(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Lcom/squareup/transactionhistory/TransactionIds;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->alreadyPresent(Lcom/squareup/transactionhistory/TransactionIds;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 450
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->addSummary(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)V

    const/4 p1, 0x1

    return p1
.end method

.method private final addIfNotPresent(Ljava/util/Collection;Z)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;Z)Z"
        }
    .end annotation

    .line 458
    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 459
    new-instance v0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$addIfNotPresent$2;

    invoke-direct {v0, p0, p2}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$addIfNotPresent$2;-><init>(Lcom/squareup/transactionhistory/historical/TransactionSummariesList;Z)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lkotlin/sequences/SequencesKt;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 460
    invoke-static {p1}, Lkotlin/sequences/SequencesKt;->count(Lkotlin/sequences/Sequence;)I

    move-result p1

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final addOrSafelyReplace(Ljava/util/Collection;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;Z)V"
        }
    .end annotation

    .line 438
    check-cast p1, Ljava/lang/Iterable;

    .line 577
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    .line 439
    invoke-direct {p0, v0, p2}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->addOrSafelyReplace(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method private final addOrSafelyReplace(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)Z
    .locals 1

    .line 427
    invoke-direct {p0, p1, p2}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->addIfNotPresent(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 431
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->safelyReplaceIfPresent(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)Z

    move-result p1

    return p1
.end method

.method private final addSummary(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)V
    .locals 2

    .line 466
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summaries:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 467
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summariesMap:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->asKey(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    .line 469
    invoke-direct {p0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->sortSummaries()V

    goto :goto_0

    .line 471
    :cond_0
    invoke-direct {p0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->notifySummariesChanged()V

    :goto_0
    return-void
.end method

.method private final alreadyPresent(Lcom/squareup/transactionhistory/TransactionIds;)Z
    .locals 1

    .line 406
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summariesMap:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->asKey(Lcom/squareup/transactionhistory/TransactionIds;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private final asKey(Lcom/squareup/transactionhistory/TransactionIds;)Ljava/lang/String;
    .locals 2

    .line 562
    invoke-virtual {p1}, Lcom/squareup/transactionhistory/TransactionIds;->getClientId()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/transactionhistory/TransactionIds;->getClientId()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/transactionhistory/TransactionIds;->getServerId()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final asKey(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Ljava/lang/String;
    .locals 2

    .line 560
    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getClientId()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getClientId()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getServerId()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final doSilently(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    .line 548
    iput-boolean v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->silenceAllNotifications:Z

    .line 549
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    const/4 p1, 0x0

    .line 550
    iput-boolean p1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->silenceAllNotifications:Z

    return-void
.end method

.method private final hasForwardedVersionOf(Ljava/util/Map;Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ")Z"
        }
    .end annotation

    .line 415
    invoke-direct {p0, p2}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->asKey(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    sget-object p2, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->FORWARDED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    if-ne p1, p2, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method private final indexOfTransaction(Lcom/squareup/transactionhistory/TransactionIds;)I
    .locals 2

    .line 419
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summariesMap:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->asKey(Lcom/squareup/transactionhistory/TransactionIds;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    if-eqz v0, :cond_0

    .line 420
    iget-object p1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summaries:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    return p1

    .line 419
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Transaction summary with ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " not in list"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final notifySummariesChanged()V
    .locals 2

    .line 534
    iget-boolean v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->silenceAllNotifications:Z

    if-eqz v0, :cond_0

    return-void

    .line 537
    :cond_0
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summariesRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summaries:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final removeSummarySilently(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)V
    .locals 2

    .line 524
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summaries:Ljava/util/List;

    invoke-static {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->getTransactionIds(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Lcom/squareup/transactionhistory/TransactionIds;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->indexOfTransaction(Lcom/squareup/transactionhistory/TransactionIds;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 525
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summariesMap:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->asKey(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final replaceSummary(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)V
    .locals 2

    .line 510
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summaries:Ljava/util/List;

    invoke-static {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->getTransactionIds(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Lcom/squareup/transactionhistory/TransactionIds;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->indexOfTransaction(Lcom/squareup/transactionhistory/TransactionIds;)I

    move-result v1

    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 511
    invoke-direct {p0, p1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->asKey(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Ljava/lang/String;

    move-result-object v0

    .line 512
    iget-object v1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summariesMap:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    .line 514
    invoke-direct {p0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->sortSummaries()V

    goto :goto_0

    .line 516
    :cond_0
    invoke-direct {p0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->notifySummariesChanged()V

    :goto_0
    return-void
.end method

.method private final safelyReplaceIfPresent(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)Z
    .locals 5

    .line 479
    invoke-static {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->getTransactionIds(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Lcom/squareup/transactionhistory/TransactionIds;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->alreadyPresent(Lcom/squareup/transactionhistory/TransactionIds;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 484
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object v0

    sget-object v2, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->FORWARDED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    const/4 v3, 0x1

    if-ne v0, v2, :cond_1

    .line 485
    invoke-direct {p0, p1, p2}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->replaceSummary(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)V

    return v3

    .line 489
    :cond_1
    invoke-static {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->getTransactionIds(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Lcom/squareup/transactionhistory/TransactionIds;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->getSummary(Lcom/squareup/transactionhistory/TransactionIds;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 493
    invoke-virtual {v0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object v2

    sget-object v4, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->FORWARDED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    if-ne v2, v4, :cond_2

    return v1

    .line 498
    :cond_2
    invoke-static {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->isPending(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->isPending(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z

    move-result v0

    if-nez v0, :cond_3

    return v1

    .line 502
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->replaceSummary(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)V

    return v3

    .line 490
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Should never throw. See alreadyPresent() invoked above"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final sortSummaries()V
    .locals 2

    .line 529
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summaries:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->comparator:Ljava/util/Comparator;

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->sortWith(Ljava/util/List;Ljava/util/Comparator;)V

    .line 530
    invoke-direct {p0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->notifySummariesChanged()V

    return-void
.end method

.method private final verifyStoreAndForward(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)V
    .locals 2

    .line 568
    invoke-static {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->isStoreAndForward(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->isStoreAndForward(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    return-void

    .line 569
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Two pending transaction summaries share an id but somehow are not both store-and-forward "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "transactions. Existing summary = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 570
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " (store-and-forward state = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 571
    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "). New summary = "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 p1, 0x20

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string p1, "(store-and-forward state = "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 572
    invoke-virtual {p2}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ")."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 568
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method


# virtual methods
.method public final addIfNotPresent(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z
    .locals 1

    const-string v0, "summary"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 366
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    const/4 v0, 0x1

    .line 367
    invoke-direct {p0, p1, v0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->addIfNotPresent(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)Z

    move-result p1

    return p1
.end method

.method public final addIfNotPresent(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "summaries"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 377
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 379
    new-instance v0, Lkotlin/jvm/internal/Ref$BooleanRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$BooleanRef;-><init>()V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    .line 380
    new-instance v1, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$addIfNotPresent$1;

    invoke-direct {v1, p0, v0, p1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$addIfNotPresent$1;-><init>(Lcom/squareup/transactionhistory/historical/TransactionSummariesList;Lkotlin/jvm/internal/Ref$BooleanRef;Ljava/util/Collection;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-direct {p0, v1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->doSilently(Lkotlin/jvm/functions/Function0;)V

    .line 383
    invoke-direct {p0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->sortSummaries()V

    .line 384
    iget-boolean p1, v0, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    return p1
.end method

.method public final addOrSafelyReplace(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;)V"
        }
    .end annotation

    const-string v0, "summaries"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    const/4 v0, 0x0

    .line 141
    invoke-direct {p0, p1, v0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->addOrSafelyReplace(Ljava/util/Collection;Z)V

    .line 142
    invoke-direct {p0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->sortSummaries()V

    return-void
.end method

.method public final addOrSafelyReplace(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z
    .locals 1

    const-string v0, "summary"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    const/4 v0, 0x1

    .line 133
    invoke-direct {p0, p1, v0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->addOrSafelyReplace(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)Z

    move-result p1

    return p1
.end method

.method public final clearAllSummaries()V
    .locals 1

    .line 355
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 356
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summaries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 357
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summariesMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 358
    invoke-direct {p0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->notifySummariesChanged()V

    return-void
.end method

.method public final getSummaries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;"
        }
    .end annotation

    .line 394
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 395
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summaries:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getSummary(Lcom/squareup/transactionhistory/TransactionIds;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;
    .locals 1

    const-string v0, "ids"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 98
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summariesMap:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->asKey(Lcom/squareup/transactionhistory/TransactionIds;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    return-object p1
.end method

.method public final isEmpty()Z
    .locals 1

    .line 402
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 403
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summaries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final safelyReplaceIfPresent(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z
    .locals 1

    const-string v0, "summary"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    const/4 v0, 0x1

    .line 169
    invoke-direct {p0, p1, v0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->safelyReplaceIfPresent(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)Z

    move-result p1

    return p1
.end method

.method public final safelyReplaceIfPresent(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "summaries"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 193
    new-instance v0, Lkotlin/jvm/internal/Ref$BooleanRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$BooleanRef;-><init>()V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    .line 194
    new-instance v1, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$safelyReplaceIfPresent$1;

    invoke-direct {v1, p0, p1, v0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$safelyReplaceIfPresent$1;-><init>(Lcom/squareup/transactionhistory/historical/TransactionSummariesList;Ljava/util/List;Lkotlin/jvm/internal/Ref$BooleanRef;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-direct {p0, v1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->doSilently(Lkotlin/jvm/functions/Function0;)V

    .line 202
    invoke-direct {p0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->sortSummaries()V

    .line 203
    iget-boolean p1, v0, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    return p1
.end method

.method public final safelyReplacePendingSummaries(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;)V"
        }
    .end annotation

    const-string v0, "pendingSummaries"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 280
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 284
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 285
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    .line 286
    invoke-direct {p0, v1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->asKey(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    if-eqz v2, :cond_0

    .line 288
    invoke-direct {p0, v2, v1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->verifyStoreAndForward(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)V

    .line 289
    invoke-virtual {v2}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object v2

    sget-object v3, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->FORWARDED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    if-ne v2, v3, :cond_0

    goto :goto_0

    .line 293
    :cond_0
    move-object v2, v0

    check-cast v2, Ljava/util/Map;

    invoke-direct {p0, v1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->asKey(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 299
    :cond_1
    iget-object p1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summaries:Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    .line 302
    invoke-static {v1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->isProcessed(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    .line 307
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object v2

    sget-object v3, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->STORED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    if-ne v2, v3, :cond_4

    .line 308
    move-object v2, v0

    check-cast v2, Ljava/util/Map;

    invoke-direct {p0, v2, v1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->hasForwardedVersionOf(Ljava/util/Map;Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 309
    invoke-direct {p0, v1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->removeSummarySilently(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)V

    goto :goto_1

    .line 316
    :cond_4
    move-object v2, v0

    check-cast v2, Ljava/util/Map;

    invoke-direct {p0, v1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->asKey(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 317
    invoke-direct {p0, v1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->removeSummarySilently(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)V

    goto :goto_1

    .line 325
    :cond_5
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object p1

    const-string v0, "newSummariesMap.values"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->addOrSafelyReplace(Ljava/util/Collection;Z)V

    .line 327
    invoke-direct {p0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->sortSummaries()V

    return-void
.end method

.method public final setSortOrder(Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;)V"
        }
    .end annotation

    const-string v0, "comparator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 109
    iput-object p1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->comparator:Ljava/util/Comparator;

    return-void
.end method

.method public final summaries()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;>;"
        }
    .end annotation

    .line 91
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->summariesRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "summariesRelay.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final updateSummaryIdsIfPresent(Lcom/squareup/transactionhistory/TransactionIds;Lcom/squareup/transactionhistory/TransactionIds;)Z
    .locals 23

    move-object/from16 v0, p0

    const-string v1, "oldIds"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "newIds"

    move-object/from16 v3, p2

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 341
    iget-object v1, v0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 342
    invoke-virtual/range {p0 .. p1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->getSummary(Lcom/squareup/transactionhistory/TransactionIds;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 343
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/transactionhistory/TransactionIds;->getClientId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/transactionhistory/TransactionIds;->getServerId()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const v21, 0xfffc

    const/16 v22, 0x0

    move-object v4, v1

    invoke-static/range {v4 .. v22}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->copy$default(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/TransactionType;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZZZZZLjava/util/List;ILjava/lang/Object;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object v2

    .line 346
    invoke-direct {v0, v1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->removeSummarySilently(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)V

    const/4 v1, 0x1

    .line 347
    invoke-direct {v0, v2, v1}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->addSummary(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)V

    return v1

    :cond_0
    const/4 v1, 0x0

    return v1
.end method
