.class public interface abstract Lcom/squareup/transactionhistory/TransactionsLoader;
.super Ljava/lang/Object;
.source "TransactionsLoader.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0002J\u0014\u0010\u0003\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\u00050\u0004H&J\u0014\u0010\u0006\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00070\u0004H&J\u0008\u0010\t\u001a\u00020\nH&J\u000e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0004H&J\u000e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0004H&J\u0008\u0010\u000f\u001a\u00020\nH&J \u0010\u0010\u001a\u00020\n2\u0016\u0010\u0011\u001a\u0012\u0012\u0004\u0012\u00028\u00000\u0012j\u0008\u0012\u0004\u0012\u00028\u0000`\u0013H&\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/TransactionsLoader;",
        "T",
        "",
        "cumulativeSummaries",
        "Lio/reactivex/Observable;",
        "",
        "error",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/receiving/FailureMessage;",
        "loadNext",
        "",
        "loadingSequenceState",
        "Lcom/squareup/transactionhistory/LoadingSequenceState;",
        "loadingState",
        "Lcom/squareup/transactionhistory/TransactionsLoaderState;",
        "restart",
        "setSortOrder",
        "comparator",
        "Ljava/util/Comparator;",
        "Lkotlin/Comparator;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract cumulativeSummaries()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "TT;>;>;"
        }
    .end annotation
.end method

.method public abstract error()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/receiving/FailureMessage;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract loadNext()V
.end method

.method public abstract loadingSequenceState()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/transactionhistory/LoadingSequenceState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract loadingState()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/transactionhistory/TransactionsLoaderState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract restart()V
.end method

.method public abstract setSortOrder(Ljava/util/Comparator;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator<",
            "TT;>;)V"
        }
    .end annotation
.end method
