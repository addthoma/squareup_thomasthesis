.class public interface abstract Lcom/squareup/transactionhistory/ConfigurableTransactionsLoader;
.super Ljava/lang/Object;
.source "ConfigurableTransactionsLoader.kt"

# interfaces
.implements Lcom/squareup/transactionhistory/TransactionsLoader;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/transactionhistory/TransactionsLoader<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0008f\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0014\u0010\u0008\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\n0\tH&J\u0014\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\r0\u000c0\tH&J\u0008\u0010\u000e\u001a\u00020\u000fH&\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/ConfigurableTransactionsLoader;",
        "T",
        "",
        "Lcom/squareup/transactionhistory/TransactionsLoader;",
        "configureWith",
        "",
        "configuration",
        "Lcom/squareup/transactionhistory/Configuration;",
        "cumulativeSummaries",
        "Lio/reactivex/Observable;",
        "",
        "error",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/receiving/FailureMessage;",
        "restart",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract configureWith(Lcom/squareup/transactionhistory/Configuration;)Z
.end method

.method public abstract cumulativeSummaries()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "TT;>;>;"
        }
    .end annotation
.end method

.method public abstract error()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/receiving/FailureMessage;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract restart()V
.end method
