.class public final Lcom/squareup/transactionhistory/mappings/TenderTypeMappingKt;
.super Ljava/lang/Object;
.source "TenderTypeMapping.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTenderTypeMapping.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TenderTypeMapping.kt\ncom/squareup/transactionhistory/mappings/TenderTypeMappingKt\n*L\n1#1,87:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "toTenderType",
        "Lcom/squareup/transactionhistory/TenderType;",
        "Lcom/squareup/billhistory/model/TenderHistory$Type;",
        "Lcom/squareup/protos/client/bills/Tender$Type;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toTenderType(Lcom/squareup/billhistory/model/TenderHistory$Type;)Lcom/squareup/transactionhistory/TenderType;
    .locals 6

    const-string v0, "$this$toTenderType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-static {}, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->values()[Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    move-result-object v0

    .line 84
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    invoke-virtual {v4}, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->getBillsTenderType$public_release()Lcom/squareup/billhistory/model/TenderHistory$Type;

    move-result-object v5

    if-ne v5, p0, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_3

    .line 85
    invoke-virtual {v4}, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->getTransactionHistoryTenderType$public_release()Lcom/squareup/transactionhistory/TenderType;

    move-result-object p0

    if-eqz p0, :cond_3

    goto :goto_3

    :cond_3
    sget-object p0, Lcom/squareup/transactionhistory/TenderType;->UNKNOWN:Lcom/squareup/transactionhistory/TenderType;

    :goto_3
    return-object p0
.end method

.method public static final toTenderType(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/transactionhistory/TenderType;
    .locals 6

    const-string v0, "$this$toTenderType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-static {}, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->values()[Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;

    move-result-object v0

    .line 78
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    invoke-virtual {v4}, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->getProtoTenderType$public_release()Lcom/squareup/protos/client/bills/Tender$Type;

    move-result-object v5

    if-ne v5, p0, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_3

    .line 79
    invoke-virtual {v4}, Lcom/squareup/transactionhistory/mappings/TenderTypeMapping;->getTransactionHistoryTenderType$public_release()Lcom/squareup/transactionhistory/TenderType;

    move-result-object p0

    if-eqz p0, :cond_3

    goto :goto_3

    :cond_3
    sget-object p0, Lcom/squareup/transactionhistory/TenderType;->UNKNOWN:Lcom/squareup/transactionhistory/TenderType;

    :goto_3
    return-object p0
.end method
