.class public final enum Lcom/squareup/transactionhistory/mappings/CardBrandMapping;
.super Ljava/lang/Enum;
.source "CardBrandMapping.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/transactionhistory/mappings/CardBrandMapping;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0018\u0008\u0080\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u001f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0006\u001a\u00020\u0007X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018j\u0002\u0008\u0019j\u0002\u0008\u001aj\u0002\u0008\u001bj\u0002\u0008\u001cj\u0002\u0008\u001dj\u0002\u0008\u001e\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/mappings/CardBrandMapping;",
        "",
        "protoCardBrand",
        "Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
        "billsCardBrand",
        "Lcom/squareup/Card$Brand;",
        "transactionHistoryCardBrand",
        "Lcom/squareup/transactionhistory/CardBrand;",
        "(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V",
        "getBillsCardBrand$public_release",
        "()Lcom/squareup/Card$Brand;",
        "getProtoCardBrand$public_release",
        "()Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
        "getTransactionHistoryCardBrand$public_release",
        "()Lcom/squareup/transactionhistory/CardBrand;",
        "UNKNOWN",
        "VISA",
        "MASTERCARD",
        "AMERICAN_EXPRESS",
        "DISCOVER",
        "DISCOVER_DINERS",
        "JCB",
        "CHINA_UNION_PAY",
        "SQUARE_GIFT_CARD_V2",
        "INTERAC",
        "SQUARE_CAPITAL_CARD",
        "EFTPOS",
        "FELICA",
        "ALIPAY",
        "CASH_APP",
        "EBT",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

.field public static final enum ALIPAY:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

.field public static final enum AMERICAN_EXPRESS:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

.field public static final enum CASH_APP:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

.field public static final enum CHINA_UNION_PAY:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

.field public static final enum DISCOVER:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

.field public static final enum DISCOVER_DINERS:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

.field public static final enum EBT:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

.field public static final enum EFTPOS:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

.field public static final enum FELICA:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

.field public static final enum INTERAC:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

.field public static final enum JCB:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

.field public static final enum MASTERCARD:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

.field public static final enum SQUARE_CAPITAL_CARD:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

.field public static final enum SQUARE_GIFT_CARD_V2:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

.field public static final enum UNKNOWN:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

.field public static final enum VISA:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;


# instance fields
.field private final billsCardBrand:Lcom/squareup/Card$Brand;

.field private final protoCardBrand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field private final transactionHistoryCardBrand:Lcom/squareup/transactionhistory/CardBrand;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    new-instance v7, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    .line 23
    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 24
    sget-object v5, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    .line 25
    sget-object v6, Lcom/squareup/transactionhistory/CardBrand;->UNKNOWN:Lcom/squareup/transactionhistory/CardBrand;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x0

    move-object v1, v7

    .line 22
    invoke-direct/range {v1 .. v6}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V

    sput-object v7, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->UNKNOWN:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    const/4 v1, 0x0

    aput-object v7, v0, v1

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    .line 28
    sget-object v11, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->VISA:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 29
    sget-object v12, Lcom/squareup/Card$Brand;->VISA:Lcom/squareup/Card$Brand;

    .line 30
    sget-object v13, Lcom/squareup/transactionhistory/CardBrand;->VISA:Lcom/squareup/transactionhistory/CardBrand;

    const-string v9, "VISA"

    const/4 v10, 0x1

    move-object v8, v1

    .line 27
    invoke-direct/range {v8 .. v13}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->VISA:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    .line 33
    sget-object v6, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->MASTERCARD:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 34
    sget-object v7, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    .line 35
    sget-object v8, Lcom/squareup/transactionhistory/CardBrand;->MASTERCARD:Lcom/squareup/transactionhistory/CardBrand;

    const-string v4, "MASTERCARD"

    const/4 v5, 0x2

    move-object v3, v1

    .line 32
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->MASTERCARD:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    .line 38
    sget-object v6, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 39
    sget-object v7, Lcom/squareup/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/Card$Brand;

    .line 40
    sget-object v8, Lcom/squareup/transactionhistory/CardBrand;->AMERICAN_EXPRESS:Lcom/squareup/transactionhistory/CardBrand;

    const-string v4, "AMERICAN_EXPRESS"

    const/4 v5, 0x3

    move-object v3, v1

    .line 37
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->AMERICAN_EXPRESS:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    .line 43
    sget-object v6, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->DISCOVER:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 44
    sget-object v7, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    .line 45
    sget-object v8, Lcom/squareup/transactionhistory/CardBrand;->DISCOVER:Lcom/squareup/transactionhistory/CardBrand;

    const-string v4, "DISCOVER"

    const/4 v5, 0x4

    move-object v3, v1

    .line 42
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->DISCOVER:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    .line 48
    sget-object v6, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->DISCOVER_DINERS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 49
    sget-object v7, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    .line 50
    sget-object v8, Lcom/squareup/transactionhistory/CardBrand;->DISCOVER_DINERS:Lcom/squareup/transactionhistory/CardBrand;

    const-string v4, "DISCOVER_DINERS"

    const/4 v5, 0x5

    move-object v3, v1

    .line 47
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->DISCOVER_DINERS:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    .line 53
    sget-object v6, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->JCB:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 54
    sget-object v7, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    .line 55
    sget-object v8, Lcom/squareup/transactionhistory/CardBrand;->JCB:Lcom/squareup/transactionhistory/CardBrand;

    const-string v4, "JCB"

    const/4 v5, 0x6

    move-object v3, v1

    .line 52
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->JCB:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    .line 58
    sget-object v6, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->CHINA_UNIONPAY:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 59
    sget-object v7, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    .line 60
    sget-object v8, Lcom/squareup/transactionhistory/CardBrand;->CHINA_UNIONPAY:Lcom/squareup/transactionhistory/CardBrand;

    const-string v4, "CHINA_UNION_PAY"

    const/4 v5, 0x7

    move-object v3, v1

    .line 57
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->CHINA_UNION_PAY:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    .line 63
    sget-object v6, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 64
    sget-object v7, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    .line 65
    sget-object v8, Lcom/squareup/transactionhistory/CardBrand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/transactionhistory/CardBrand;

    const-string v4, "SQUARE_GIFT_CARD_V2"

    const/16 v5, 0x8

    move-object v3, v1

    .line 62
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->SQUARE_GIFT_CARD_V2:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    .line 68
    sget-object v6, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->INTERAC:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 69
    sget-object v7, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    .line 70
    sget-object v8, Lcom/squareup/transactionhistory/CardBrand;->INTERAC:Lcom/squareup/transactionhistory/CardBrand;

    const-string v4, "INTERAC"

    const/16 v5, 0x9

    move-object v3, v1

    .line 67
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->INTERAC:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    .line 73
    sget-object v6, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 74
    sget-object v7, Lcom/squareup/Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/Card$Brand;

    .line 75
    sget-object v8, Lcom/squareup/transactionhistory/CardBrand;->SQUARE_CAPITAL_CARD:Lcom/squareup/transactionhistory/CardBrand;

    const-string v4, "SQUARE_CAPITAL_CARD"

    const/16 v5, 0xa

    move-object v3, v1

    .line 72
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->SQUARE_CAPITAL_CARD:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    .line 78
    sget-object v6, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->EFTPOS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 79
    sget-object v7, Lcom/squareup/Card$Brand;->EFTPOS:Lcom/squareup/Card$Brand;

    .line 80
    sget-object v8, Lcom/squareup/transactionhistory/CardBrand;->EFTPOS:Lcom/squareup/transactionhistory/CardBrand;

    const-string v4, "EFTPOS"

    const/16 v5, 0xb

    move-object v3, v1

    .line 77
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->EFTPOS:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    .line 83
    sget-object v6, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->FELICA:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 84
    sget-object v7, Lcom/squareup/Card$Brand;->FELICA:Lcom/squareup/Card$Brand;

    .line 85
    sget-object v8, Lcom/squareup/transactionhistory/CardBrand;->FELICA:Lcom/squareup/transactionhistory/CardBrand;

    const-string v4, "FELICA"

    const/16 v5, 0xc

    move-object v3, v1

    .line 82
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->FELICA:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    .line 88
    sget-object v6, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ALIPAY:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 89
    sget-object v7, Lcom/squareup/Card$Brand;->ALIPAY:Lcom/squareup/Card$Brand;

    .line 90
    sget-object v8, Lcom/squareup/transactionhistory/CardBrand;->ALIPAY:Lcom/squareup/transactionhistory/CardBrand;

    const-string v4, "ALIPAY"

    const/16 v5, 0xd

    move-object v3, v1

    .line 87
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->ALIPAY:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    .line 93
    sget-object v6, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->CASH_APP:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 94
    sget-object v7, Lcom/squareup/Card$Brand;->CASH_APP:Lcom/squareup/Card$Brand;

    .line 95
    sget-object v8, Lcom/squareup/transactionhistory/CardBrand;->CASH_APP:Lcom/squareup/transactionhistory/CardBrand;

    const-string v4, "CASH_APP"

    const/16 v5, 0xe

    move-object v3, v1

    .line 92
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->CASH_APP:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    .line 98
    sget-object v6, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->EBT:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 100
    sget-object v7, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    .line 101
    sget-object v8, Lcom/squareup/transactionhistory/CardBrand;->EBT:Lcom/squareup/transactionhistory/CardBrand;

    const-string v4, "EBT"

    const/16 v5, 0xf

    move-object v3, v1

    .line 97
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->EBT:Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->$VALUES:[Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/Card$Brand;Lcom/squareup/transactionhistory/CardBrand;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
            "Lcom/squareup/Card$Brand;",
            "Lcom/squareup/transactionhistory/CardBrand;",
            ")V"
        }
    .end annotation

    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->protoCardBrand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iput-object p4, p0, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->billsCardBrand:Lcom/squareup/Card$Brand;

    iput-object p5, p0, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->transactionHistoryCardBrand:Lcom/squareup/transactionhistory/CardBrand;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/transactionhistory/mappings/CardBrandMapping;
    .locals 1

    const-class v0, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    return-object p0
.end method

.method public static values()[Lcom/squareup/transactionhistory/mappings/CardBrandMapping;
    .locals 1

    sget-object v0, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->$VALUES:[Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    invoke-virtual {v0}, [Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/transactionhistory/mappings/CardBrandMapping;

    return-object v0
.end method


# virtual methods
.method public final getBillsCardBrand$public_release()Lcom/squareup/Card$Brand;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->billsCardBrand:Lcom/squareup/Card$Brand;

    return-object v0
.end method

.method public final getProtoCardBrand$public_release()Lcom/squareup/protos/client/bills/CardTender$Card$Brand;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->protoCardBrand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object v0
.end method

.method public final getTransactionHistoryCardBrand$public_release()Lcom/squareup/transactionhistory/CardBrand;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/transactionhistory/mappings/CardBrandMapping;->transactionHistoryCardBrand:Lcom/squareup/transactionhistory/CardBrand;

    return-object v0
.end method
