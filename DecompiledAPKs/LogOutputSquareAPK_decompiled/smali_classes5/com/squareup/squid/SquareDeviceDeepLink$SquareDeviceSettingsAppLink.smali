.class public abstract Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;
.super Lcom/squareup/squid/SquareDeviceDeepLink;
.source "SquareDeviceDeepLink.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squid/SquareDeviceDeepLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SquareDeviceSettingsAppLink"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$SettingsAppLink;,
        Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$SettingsAppGeneralLink;,
        Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$SettingsAppGeneralAboutLink;,
        Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$SettingsAppNetworkLink;,
        Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareDeviceDeepLink.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareDeviceDeepLink.kt\ncom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink\n*L\n1#1,139:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \r2\u00020\u0001:\u0005\r\u000e\u000f\u0010\u0011B\u0013\u0008\u0002\u0012\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0007\u001a\u00020\u0008J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0008J\u0012\u0010\u000c\u001a\u00020\u00082\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\u0002R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0004\u0012\u0013\u0014\u0015\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;",
        "Lcom/squareup/squid/SquareDeviceDeepLink;",
        "action",
        "",
        "(Ljava/lang/String;)V",
        "getAction",
        "()Ljava/lang/String;",
        "getIntent",
        "Landroid/content/Intent;",
        "matches",
        "",
        "intent",
        "settingsAppIntent",
        "Companion",
        "SettingsAppGeneralAboutLink",
        "SettingsAppGeneralLink",
        "SettingsAppLink",
        "SettingsAppNetworkLink",
        "Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$SettingsAppLink;",
        "Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$SettingsAppGeneralLink;",
        "Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$SettingsAppGeneralAboutLink;",
        "Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$SettingsAppNetworkLink;",
        "square-device_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ACTION_SHOW_ABOUT_REGISTER:Ljava/lang/String;

.field private static final ACTION_SHOW_GENERAL:Ljava/lang/String;

.field private static final ACTION_SHOW_NETWORK:Ljava/lang/String;

.field public static final Companion:Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$Companion;

.field private static final SETTINGS_APP_ACTIVITY_NAME:Ljava/lang/String;

.field private static final SETTINGS_APP_PACKAGE:Ljava/lang/String;


# instance fields
.field private final action:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->Companion:Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$Companion;

    .line 105
    sget-object v0, Lcom/squareup/squid/SquareDeviceApps;->SETTINGS:Lcom/squareup/squid/SquareDeviceApps;

    iget-object v0, v0, Lcom/squareup/squid/SquareDeviceApps;->packageName:Ljava/lang/String;

    sput-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->SETTINGS_APP_PACKAGE:Ljava/lang/String;

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->SETTINGS_APP_PACKAGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".SettingsActivity"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->SETTINGS_APP_ACTIVITY_NAME:Ljava/lang/String;

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->SETTINGS_APP_PACKAGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".ACTION_SHOW_GENERAL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->ACTION_SHOW_GENERAL:Ljava/lang/String;

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->SETTINGS_APP_PACKAGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".ACTION_SHOW_ABOUT_REGISTER"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->ACTION_SHOW_ABOUT_REGISTER:Ljava/lang/String;

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->SETTINGS_APP_PACKAGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".ACTION_SHOW_NETWORK"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->ACTION_SHOW_NETWORK:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 76
    invoke-direct {p0, v0}, Lcom/squareup/squid/SquareDeviceDeepLink;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->action:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 75
    check-cast p1, Ljava/lang/String;

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 74
    invoke-direct {p0, p1}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$getACTION_SHOW_ABOUT_REGISTER$cp()Ljava/lang/String;
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->ACTION_SHOW_ABOUT_REGISTER:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getACTION_SHOW_GENERAL$cp()Ljava/lang/String;
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->ACTION_SHOW_GENERAL:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getACTION_SHOW_NETWORK$cp()Ljava/lang/String;
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->ACTION_SHOW_NETWORK:Ljava/lang/String;

    return-object v0
.end method

.method private final settingsAppIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .line 97
    new-instance v0, Landroid/content/Intent;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, "android.intent.action.MAIN"

    :goto_0
    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string p1, "android.intent.category.LAUNCHER"

    .line 98
    invoke-virtual {v0, p1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 p1, 0x30000000

    .line 99
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 100
    new-instance p1, Landroid/content/ComponentName;

    sget-object v1, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->SETTINGS_APP_PACKAGE:Ljava/lang/String;

    sget-object v2, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->SETTINGS_APP_ACTIVITY_NAME:Ljava/lang/String;

    invoke-direct {p1, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public final getAction()Ljava/lang/String;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->action:Ljava/lang/String;

    return-object v0
.end method

.method public final getIntent()Landroid/content/Intent;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->action:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->settingsAppIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final matches(Landroid/content/Intent;)Z
    .locals 1

    const-string v0, "intent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;->action:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
