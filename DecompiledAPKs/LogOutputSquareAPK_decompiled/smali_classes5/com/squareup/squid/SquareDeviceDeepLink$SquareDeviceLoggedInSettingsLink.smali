.class public abstract Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink;
.super Lcom/squareup/squid/SquareDeviceDeepLink;
.source "SquareDeviceDeepLink.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squid/SquareDeviceDeepLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SquareDeviceLoggedInSettingsLink"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsGeneralLink;,
        Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsGeneralAboutLink;,
        Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsNetworkLink;,
        Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsOfflineModeLink;,
        Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00102\u00020\u0001:\u0005\u0010\u0011\u0012\u0013\u0014B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0003J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tJ.\u0010\n\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00032\u0008\u0008\u0002\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u0003H\u0002J\u0014\u0010\u0006\u001a\u00020\u0007*\u00020\t2\u0006\u0010\u000f\u001a\u00020\u0003H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0001\u0004\u0015\u0016\u0017\u0018\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink;",
        "Lcom/squareup/squid/SquareDeviceDeepLink;",
        "deepLinkId",
        "",
        "(Ljava/lang/String;)V",
        "getDeepLink",
        "matches",
        "",
        "uri",
        "Landroid/net/Uri;",
        "settingsDeepLink",
        "host",
        "navigatePath",
        "navigationIdKey",
        "navigationIdValue",
        "id",
        "Companion",
        "LoggedInSettingsGeneralAboutLink",
        "LoggedInSettingsGeneralLink",
        "LoggedInSettingsNetworkLink",
        "LoggedInSettingsOfflineModeLink",
        "Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsGeneralLink;",
        "Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsGeneralAboutLink;",
        "Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsNetworkLink;",
        "Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsOfflineModeLink;",
        "square-device_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final NAVIGATE_PATH:Ljava/lang/String; = "/navigate"

.field private static final NAVIGATION_ID_KEY:Ljava/lang/String; = "navigationID"

.field private static final SETTINGS_HOST:Ljava/lang/String; = "settings"


# instance fields
.field private final deepLinkId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink;->Companion:Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 33
    invoke-direct {p0, v0}, Lcom/squareup/squid/SquareDeviceDeepLink;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink;->deepLinkId:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private final matches(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 2

    .line 54
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "settings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/navigate"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "navigationID"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final settingsDeepLink(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "square-register://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x3f

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x3d

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic settingsDeepLink$default(Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    if-nez p6, :cond_3

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    const-string p1, "settings"

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    const-string p2, "/navigate"

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    const-string p3, "navigationID"

    .line 60
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink;->settingsDeepLink(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 0
    :cond_3
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: settingsDeepLink"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public final getDeepLink()Ljava/lang/String;
    .locals 7

    .line 37
    iget-object v4, p0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink;->deepLinkId:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink;->settingsDeepLink$default(Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final matches(Landroid/net/Uri;)Z
    .locals 1

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink;->deepLinkId:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink;->matches(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method
