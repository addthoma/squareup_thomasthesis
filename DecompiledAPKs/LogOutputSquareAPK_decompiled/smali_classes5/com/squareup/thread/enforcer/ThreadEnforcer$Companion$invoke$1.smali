.class public final Lcom/squareup/thread/enforcer/ThreadEnforcer$Companion$invoke$1;
.super Ljava/lang/Object;
.source "ThreadEnforcer.kt"

# interfaces
.implements Lcom/squareup/thread/enforcer/ThreadEnforcer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/thread/enforcer/ThreadEnforcer$Companion;->invoke(Lkotlin/jvm/functions/Function0;)Lcom/squareup/thread/enforcer/ThreadEnforcer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001R\u0014\u0010\u0002\u001a\u00020\u00038VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "com/squareup/thread/enforcer/ThreadEnforcer$Companion$invoke$1",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "isTargetThread",
        "",
        "()Z",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $targetThread:Lkotlin/jvm/functions/Function0;


# direct methods
.method constructor <init>(Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/squareup/thread/enforcer/ThreadEnforcer$Companion$invoke$1;->$targetThread:Lkotlin/jvm/functions/Function0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public confine()V
    .locals 0

    .line 51
    invoke-static {p0}, Lcom/squareup/thread/enforcer/ThreadEnforcer$DefaultImpls;->confine(Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-void
.end method

.method public confine(Ljava/lang/String;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-static {p0, p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer$DefaultImpls;->confine(Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/lang/String;)V

    return-void
.end method

.method public forbid()V
    .locals 0

    .line 51
    invoke-static {p0}, Lcom/squareup/thread/enforcer/ThreadEnforcer$DefaultImpls;->forbid(Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-void
.end method

.method public forbid(Ljava/lang/String;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-static {p0, p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer$DefaultImpls;->forbid(Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/lang/String;)V

    return-void
.end method

.method public isTargetThread()Z
    .locals 2

    .line 53
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/thread/enforcer/ThreadEnforcer$Companion$invoke$1;->$targetThread:Lkotlin/jvm/functions/Function0;

    invoke-interface {v1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
