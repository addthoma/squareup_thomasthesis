.class public final Lcom/squareup/thread/FileThreadExecutorModule;
.super Ljava/lang/Object;
.source "FileThreadExecutorModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/thread/FileThreadModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/thread/FileThreadExecutorModule;",
        "",
        "()V",
        "provideFileThreadExecutor",
        "Ljava/util/concurrent/Executor;",
        "holder",
        "Lcom/squareup/thread/FileThreadHolder;",
        "impl-file-thread-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/thread/FileThreadExecutorModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/thread/FileThreadExecutorModule;

    invoke-direct {v0}, Lcom/squareup/thread/FileThreadExecutorModule;-><init>()V

    sput-object v0, Lcom/squareup/thread/FileThreadExecutorModule;->INSTANCE:Lcom/squareup/thread/FileThreadExecutorModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideFileThreadExecutor(Lcom/squareup/thread/FileThreadHolder;)Ljava/util/concurrent/Executor;
    .locals 1
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-virtual {p1}, Lcom/squareup/thread/FileThreadHolder;->getExecutor()Ljava/util/concurrent/Executor;

    move-result-object p1

    return-object p1
.end method
