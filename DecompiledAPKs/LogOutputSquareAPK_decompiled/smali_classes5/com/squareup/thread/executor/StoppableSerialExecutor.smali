.class public interface abstract Lcom/squareup/thread/executor/StoppableSerialExecutor;
.super Ljava/lang/Object;
.source "StoppableSerialExecutor.java"

# interfaces
.implements Lcom/squareup/thread/executor/SerialExecutor;


# virtual methods
.method public abstract awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation
.end method

.method public abstract isShutdown()Z
.end method

.method public abstract shutdown()V
.end method
