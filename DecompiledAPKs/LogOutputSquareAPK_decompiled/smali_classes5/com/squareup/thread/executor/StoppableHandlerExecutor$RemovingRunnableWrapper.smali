.class final Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;
.super Ljava/lang/Object;
.source "StoppableHandlerExecutor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/thread/executor/StoppableHandlerExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RemovingRunnableWrapper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/thread/executor/StoppableHandlerExecutor;

.field private final wrapped:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/squareup/thread/executor/StoppableHandlerExecutor;Ljava/lang/Runnable;)V
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;->this$0:Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    iput-object p2, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;->wrapped:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public getWrapped()Ljava/lang/Runnable;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;->wrapped:Ljava/lang/Runnable;

    return-object v0
.end method

.method public run()V
    .locals 3

    .line 148
    iget-object v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;->this$0:Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    invoke-static {v0}, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->access$000(Lcom/squareup/thread/executor/StoppableHandlerExecutor;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 149
    :try_start_0
    iget-object v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;->this$0:Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    invoke-static {v1}, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->access$100(Lcom/squareup/thread/executor/StoppableHandlerExecutor;)Z

    move-result v1

    if-eqz v1, :cond_0

    monitor-exit v0

    return-void

    .line 150
    :cond_0
    iget-object v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;->this$0:Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    invoke-static {v1}, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->access$200(Lcom/squareup/thread/executor/StoppableHandlerExecutor;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 151
    iget-object v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;->this$0:Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    invoke-static {v1}, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->access$300(Lcom/squareup/thread/executor/StoppableHandlerExecutor;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 152
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 155
    :try_start_1
    iget-object v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;->wrapped:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 157
    iget-object v0, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;->this$0:Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    invoke-static {v0}, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->access$000(Lcom/squareup/thread/executor/StoppableHandlerExecutor;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 158
    :try_start_2
    iget-object v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;->this$0:Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    invoke-static {v1}, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->access$300(Lcom/squareup/thread/executor/StoppableHandlerExecutor;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    if-nez v1, :cond_1

    .line 159
    iget-object v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;->this$0:Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    invoke-static {v1}, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->access$400(Lcom/squareup/thread/executor/StoppableHandlerExecutor;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 161
    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :catchall_1
    move-exception v0

    .line 157
    iget-object v1, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;->this$0:Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    invoke-static {v1}, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->access$000(Lcom/squareup/thread/executor/StoppableHandlerExecutor;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 158
    :try_start_3
    iget-object v2, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;->this$0:Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    invoke-static {v2}, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->access$300(Lcom/squareup/thread/executor/StoppableHandlerExecutor;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v2

    if-nez v2, :cond_2

    .line 159
    iget-object v2, p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor$RemovingRunnableWrapper;->this$0:Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    invoke-static {v2}, Lcom/squareup/thread/executor/StoppableHandlerExecutor;->access$400(Lcom/squareup/thread/executor/StoppableHandlerExecutor;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 161
    :cond_2
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 162
    throw v0

    :catchall_2
    move-exception v0

    .line 161
    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0

    :catchall_3
    move-exception v1

    .line 152
    :try_start_5
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v1
.end method
