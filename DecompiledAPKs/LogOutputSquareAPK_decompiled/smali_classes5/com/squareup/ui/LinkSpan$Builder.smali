.class public Lcom/squareup/ui/LinkSpan$Builder;
.super Ljava/lang/Object;
.source "LinkSpan.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/LinkSpan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private clickableSpan:Lcom/squareup/ui/LinkSpan;

.field private clickableText:Ljava/lang/String;

.field private key:Ljava/lang/String;

.field private linkColorResId:I

.field private final marketSpan:Landroid/text/style/CharacterStyle;

.field private onClickRunnable:Ljava/lang/Runnable;

.field private pattern:Ljava/lang/String;

.field private final resources:Landroid/content/res/Resources;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    sget v0, Lcom/squareup/ui/LinkSpan;->DEFAULT_COLOR_ID:I

    iput v0, p0, Lcom/squareup/ui/LinkSpan$Builder;->linkColorResId:I

    const-string v0, "context"

    .line 49
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/LinkSpan$Builder;->resources:Landroid/content/res/Resources;

    .line 51
    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p1, v0}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/LinkSpan$Builder;->marketSpan:Landroid/text/style/CharacterStyle;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/LinkSpan$Builder;)Ljava/lang/Runnable;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/ui/LinkSpan$Builder;->onClickRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/LinkSpan$Builder;)Ljava/lang/String;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/ui/LinkSpan$Builder;->url:Ljava/lang/String;

    return-object p0
.end method

.method private clickableSpan()Lcom/squareup/ui/LinkSpan;
    .locals 3

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan:Lcom/squareup/ui/LinkSpan;

    if-nez v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/LinkSpan$Builder;->resources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/squareup/ui/LinkSpan$Builder;->linkColorResId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 152
    iget-object v1, p0, Lcom/squareup/ui/LinkSpan$Builder;->url:Ljava/lang/String;

    const-string/jumbo v2, "url"

    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 154
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/LinkSpan$Builder$1;-><init>(Lcom/squareup/ui/LinkSpan$Builder;I)V

    return-object v1

    :cond_0
    return-object v0
.end method


# virtual methods
.method public asCharSequence()Ljava/lang/CharSequence;
    .locals 1

    .line 146
    invoke-virtual {p0}, Lcom/squareup/ui/LinkSpan$Builder;->asPhrase()Lcom/squareup/phrase/Phrase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public asPhrase()Lcom/squareup/phrase/Phrase;
    .locals 3

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/LinkSpan$Builder;->pattern:Ljava/lang/String;

    const-string v1, "pattern"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/LinkSpan$Builder;->key:Ljava/lang/String;

    const-string v1, "key"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/LinkSpan$Builder;->pattern:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/LinkSpan$Builder;->key:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/squareup/ui/LinkSpan$Builder;->asSpannable()Landroid/text/Spannable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    return-object v0
.end method

.method public asSpannable()Landroid/text/Spannable;
    .locals 4

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/LinkSpan$Builder;->clickableText:Ljava/lang/String;

    const-string v1, "clickableText"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/LinkSpan$Builder;->clickableText:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/text/style/CharacterStyle;

    invoke-direct {p0}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan()Lcom/squareup/ui/LinkSpan;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/ui/LinkSpan$Builder;->marketSpan:Landroid/text/style/CharacterStyle;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;[Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method public clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;
    .locals 1

    const-string v0, "clickableSpan"

    .line 100
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 102
    iput-object p1, p0, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan:Lcom/squareup/ui/LinkSpan;

    return-object p0
.end method

.method public clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/LinkSpan$Builder;->resources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 95
    invoke-virtual {p0, p1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(Ljava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    return-object p1
.end method

.method public clickableText(Ljava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;
    .locals 1

    const-string v0, "clickableText"

    .line 87
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 88
    invoke-static {p1}, Lcom/squareup/util/Strings;->replaceWhitespaceWithNBSP(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/LinkSpan$Builder;->clickableText:Ljava/lang/String;

    return-object p0
.end method

.method public linkColor(I)Lcom/squareup/ui/LinkSpan$Builder;
    .locals 0

    .line 119
    iput p1, p0, Lcom/squareup/ui/LinkSpan$Builder;->linkColorResId:I

    return-object p0
.end method

.method public onClick(Ljava/lang/Runnable;)Lcom/squareup/ui/LinkSpan$Builder;
    .locals 1

    const-string v0, "onClickRunnable"

    .line 111
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 113
    iput-object p1, p0, Lcom/squareup/ui/LinkSpan$Builder;->onClickRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method public pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/LinkSpan$Builder;->resources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "pattern"

    .line 60
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "key"

    .line 61
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/LinkSpan$Builder;->pattern:Ljava/lang/String;

    .line 64
    iput-object p2, p0, Lcom/squareup/ui/LinkSpan$Builder;->key:Ljava/lang/String;

    return-object p0
.end method

.method public url(I)Lcom/squareup/ui/LinkSpan$Builder;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/LinkSpan$Builder;->resources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 82
    invoke-virtual {p0, p1}, Lcom/squareup/ui/LinkSpan$Builder;->url(Ljava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    return-object p1
.end method

.method public url(Ljava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;
    .locals 1

    const-string/jumbo v0, "url"

    .line 70
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 72
    iput-object p1, p0, Lcom/squareup/ui/LinkSpan$Builder;->url:Ljava/lang/String;

    return-object p0
.end method
