.class Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "AdjustInventorySpecifyNumberCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->displayStockCountNumber(Landroid/view/View;Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;Lcom/squareup/quantity/BigDecimalFormatter;Lcom/squareup/quantity/BigDecimalFormatter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;

.field final synthetic val$displayFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

.field final synthetic val$inputFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

.field final synthetic val$isInitialReceive:Z

.field final synthetic val$reason:Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

.field final synthetic val$serverCount:Ljava/math/BigDecimal;


# direct methods
.method constructor <init>(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;Lcom/squareup/quantity/BigDecimalFormatter;ZLcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;Ljava/math/BigDecimal;Lcom/squareup/quantity/BigDecimalFormatter;)V
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->this$0:Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->val$inputFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    iput-boolean p3, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->val$isInitialReceive:Z

    iput-object p4, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->val$reason:Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    iput-object p5, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->val$serverCount:Ljava/math/BigDecimal;

    iput-object p6, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->val$displayFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 7

    .line 191
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[^0-9^"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->val$inputFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    .line 194
    invoke-virtual {v1}, Lcom/squareup/quantity/BigDecimalFormatter;->getDecimalSeparator()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 197
    invoke-static {p1, v0}, Lcom/squareup/util/Strings;->removePattern(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Numbers;->parseDecimal(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v3

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->this$0:Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->access$000(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;)Lcom/squareup/ui/inventory/AdjustInventoryController;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/squareup/ui/inventory/AdjustInventoryController;->stockCountUpdated(Ljava/math/BigDecimal;)V

    .line 202
    iget-boolean v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->val$isInitialReceive:Z

    if-nez v0, :cond_2

    .line 203
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->val$reason:Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    iget-boolean p1, p1, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->isRecount:Z

    if-eqz p1, :cond_0

    goto :goto_0

    .line 206
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->this$0:Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;

    iget-object v2, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->val$serverCount:Ljava/math/BigDecimal;

    iget-object v4, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->val$inputFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    iget-object v5, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->val$displayFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->val$reason:Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    iget-object v6, p1, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->inventoryAdjustmentReason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->access$200(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/squareup/quantity/BigDecimalFormatter;Lcom/squareup/quantity/BigDecimalFormatter;Lcom/squareup/protos/client/InventoryAdjustmentReason;)V

    goto :goto_1

    .line 204
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->this$0:Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->val$serverCount:Ljava/math/BigDecimal;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->val$displayFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->access$100(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;Ljava/math/BigDecimal;Lcom/squareup/quantity/BigDecimalFormatter;)V

    .line 210
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->this$0:Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->access$400(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;->this$0:Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->access$300(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method
