.class public Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AdjustInventorySpecifyNumberCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

.field private adjustStockContainer:Landroid/view/View;

.field private inventoryTrackingHelpText:Lcom/squareup/widgets/MessageView;

.field private localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final res:Lcom/squareup/util/Res;

.field private saveSpinnerContainer:Landroid/view/View;

.field private stockCountEditRow:Lcom/squareup/noho/NohoEditRow;

.field private stockCountHelpText:Lcom/squareup/widgets/MessageView;

.field private transitionTime:I

.field private tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private unitCostEditText:Lcom/squareup/noho/NohoEditText;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/ui/inventory/AdjustInventoryController;Lcom/squareup/quantity/PerUnitFormatter;Ljavax/inject/Provider;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/ui/inventory/AdjustInventoryController;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 73
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->res:Lcom/squareup/util/Res;

    .line 75
    iput-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 76
    iput-object p3, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

    .line 77
    iput-object p4, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 78
    iput-object p5, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->localeProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p6, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;)Lcom/squareup/ui/inventory/AdjustInventoryController;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;Ljava/math/BigDecimal;Lcom/squareup/quantity/BigDecimalFormatter;)V
    .locals 0

    .line 52
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->setHelpTextToPreviousStockCount(Ljava/math/BigDecimal;Lcom/squareup/quantity/BigDecimalFormatter;)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/squareup/quantity/BigDecimalFormatter;Lcom/squareup/quantity/BigDecimalFormatter;Lcom/squareup/protos/client/InventoryAdjustmentReason;)V
    .locals 0

    .line 52
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->setHelpTextToUpdatedStockCount(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/squareup/quantity/BigDecimalFormatter;Lcom/squareup/quantity/BigDecimalFormatter;Lcom/squareup/protos/client/InventoryAdjustmentReason;)V

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;)Z
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->shouldEnableSaveButton()Z

    move-result p0

    return p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-object p0
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 262
    sget v0, Lcom/squareup/adjustinventory/R$id;->adjust_stock_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->adjustStockContainer:Landroid/view/View;

    .line 263
    sget v0, Lcom/squareup/adjustinventory/R$id;->save_spinner_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->saveSpinnerContainer:Landroid/view/View;

    .line 264
    sget v0, Lcom/squareup/adjustinventory/R$id;->adjust_stock_specify_number_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditRow;

    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->stockCountEditRow:Lcom/squareup/noho/NohoEditRow;

    .line 265
    sget v0, Lcom/squareup/adjustinventory/R$id;->adjust_stock_specify_number_help_text_stock_count:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->stockCountHelpText:Lcom/squareup/widgets/MessageView;

    .line 266
    sget v0, Lcom/squareup/adjustinventory/R$id;->adjust_stock_unit_cost_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->unitCostEditText:Lcom/squareup/noho/NohoEditText;

    .line 267
    sget v0, Lcom/squareup/adjustinventory/R$id;->adjust_stock_specify_number_help_text_inventory_tracking:I

    .line 268
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->inventoryTrackingHelpText:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private dismissSaveSpinner()V
    .locals 2

    .line 301
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->saveSpinnerContainer:Landroid/view/View;

    iget v1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->transitionTime:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    .line 302
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->adjustStockContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private displayScreenData(Landroid/view/View;Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;)V
    .locals 7

    .line 128
    iget-boolean v0, p2, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->shouldShowUnitCost:Z

    .line 129
    iget-object v1, p2, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->serverCount:Ljava/math/BigDecimal;

    .line 130
    iget-boolean v2, p2, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->isInitialReceive:Z

    .line 131
    new-instance v3, Lcom/squareup/quantity/BigDecimalFormatter;

    iget-object v4, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->localeProvider:Ljavax/inject/Provider;

    sget-object v5, Lcom/squareup/quantity/BigDecimalFormatter$Format;->ROUNDING_SCALE:Lcom/squareup/quantity/BigDecimalFormatter$Format;

    iget v6, p2, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->precision:I

    invoke-direct {v3, v4, v5, v6}, Lcom/squareup/quantity/BigDecimalFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/quantity/BigDecimalFormatter$Format;I)V

    .line 133
    new-instance v4, Lcom/squareup/quantity/BigDecimalFormatter;

    iget-object v5, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->localeProvider:Ljavax/inject/Provider;

    sget-object v6, Lcom/squareup/quantity/BigDecimalFormatter$Format;->ROUNDING_SCALE:Lcom/squareup/quantity/BigDecimalFormatter$Format;

    invoke-direct {v4, v5, v6}, Lcom/squareup/quantity/BigDecimalFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/quantity/BigDecimalFormatter$Format;)V

    .line 135
    iget v5, p2, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->precision:I

    invoke-virtual {v4, v5}, Lcom/squareup/quantity/BigDecimalFormatter;->setMaxPaddedFractionDigits(I)V

    if-eqz v0, :cond_0

    .line 139
    invoke-direct {p0, p2}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->displayUnitCost(Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;)V

    .line 142
    :cond_0
    invoke-direct {p0, p1, p2, v3, v4}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->displayStockCountNumber(Landroid/view/View;Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;Lcom/squareup/quantity/BigDecimalFormatter;Lcom/squareup/quantity/BigDecimalFormatter;)V

    if-eqz v2, :cond_1

    .line 146
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->stockCountHelpText:Lcom/squareup/widgets/MessageView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    goto :goto_0

    .line 148
    :cond_1
    invoke-direct {p0, v1, v4}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->setHelpTextToPreviousStockCount(Ljava/math/BigDecimal;Lcom/squareup/quantity/BigDecimalFormatter;)V

    :goto_0
    return-void
.end method

.method private displayStockCountNumber(Landroid/view/View;Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;Lcom/squareup/quantity/BigDecimalFormatter;Lcom/squareup/quantity/BigDecimalFormatter;)V
    .locals 9

    .line 170
    iget-object v4, p2, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->reason:Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    .line 171
    iget-boolean v3, p2, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->isInitialReceive:Z

    .line 172
    iget-object v5, p2, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->serverCount:Ljava/math/BigDecimal;

    .line 173
    iget-object v7, p2, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->userInputCount:Ljava/math/BigDecimal;

    .line 174
    iget-boolean p2, p2, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->shouldShowUnitCost:Z

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->stockCountEditRow:Lcom/squareup/noho/NohoEditRow;

    new-instance v1, Lcom/squareup/noho/LabelPlugin;

    .line 179
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 180
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-direct {p0, v4}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->getCountLabel(Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;)I

    move-result v6

    invoke-virtual {p1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    sget v6, Lcom/squareup/noho/R$style;->TextAppearance_Widget_Noho_Edit_Label:I

    const/4 v8, 0x0

    invoke-direct {v1, v2, p1, v6, v8}, Lcom/squareup/noho/LabelPlugin;-><init>(Landroid/content/Context;Ljava/lang/String;IF)V

    .line 177
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 185
    new-instance p1, Lcom/squareup/text/DecimalScrubber;

    invoke-direct {p1, p3}, Lcom/squareup/text/DecimalScrubber;-><init>(Lcom/squareup/quantity/BigDecimalFormatter;)V

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->stockCountEditRow:Lcom/squareup/noho/NohoEditRow;

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p3, v1}, Lcom/squareup/quantity/BigDecimalFormatter;->format(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setHint(Ljava/lang/CharSequence;)V

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->stockCountEditRow:Lcom/squareup/noho/NohoEditRow;

    new-instance v1, Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-direct {v1, p1, v0}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/SelectableTextScrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setScrubber(Lcom/squareup/text/ScrubbingTextWatcher;)V

    .line 189
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->stockCountEditRow:Lcom/squareup/noho/NohoEditRow;

    new-instance v8, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$2;-><init>(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;Lcom/squareup/quantity/BigDecimalFormatter;ZLcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;Ljava/math/BigDecimal;Lcom/squareup/quantity/BigDecimalFormatter;)V

    invoke-virtual {p1, v8}, Lcom/squareup/noho/NohoEditRow;->addTextChangedListener(Landroid/text/TextWatcher;)V

    if-eqz v7, :cond_0

    .line 216
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->stockCountEditRow:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {p3, v7}, Lcom/squareup/quantity/BigDecimalFormatter;->format(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/noho/NohoEditRow;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    if-eqz p2, :cond_1

    .line 220
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->stockCountEditRow:Lcom/squareup/noho/NohoEditRow;

    const/4 p2, 0x5

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditRow;->setImeOptions(I)V

    .line 222
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->stockCountEditRow:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditRow;->requestFocus()Z

    return-void
.end method

.method private displayUnitCost(Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;)V
    .locals 4

    .line 153
    iget-object v0, p1, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->userInputUnitCost:Lcom/squareup/protos/common/Money;

    .line 154
    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->unitCostEditText:Lcom/squareup/noho/NohoEditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoEditText;->setVisibility(I)V

    .line 156
    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->unitCostEditText:Lcom/squareup/noho/NohoEditText;

    iget-object v2, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v3, p1, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->unitAbbreviation:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->unitCostEditText:Lcom/squareup/noho/NohoEditText;

    iget-object v2, p1, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->unitAbbreviation:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/money/PriceLocaleHelper;->setHintToZeroMoney(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->unitCostEditText:Lcom/squareup/noho/NohoEditText;

    sget-object v2, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANK_ON_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    iget-object p1, p1, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->unitAbbreviation:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Ljava/lang/String;)Lcom/squareup/text/ScrubbingTextWatcher;

    .line 160
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->unitCostEditText:Lcom/squareup/noho/NohoEditText;

    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$1;-><init>(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private getActionBarConfig(Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 5

    .line 244
    iget-boolean v0, p1, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->isInitialReceive:Z

    if-eqz v0, :cond_0

    .line 245
    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->done:I

    .line 246
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->save:I

    .line 247
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 249
    :goto_0
    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 251
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v4, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->res:Lcom/squareup/util/Res;

    iget-object p1, p1, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;->reason:Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    iget p1, p1, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->labelResId:I

    invoke-interface {v4, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v3, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v4, Lcom/squareup/ui/inventory/-$$Lambda$B1Bz5YZysi2nHZ0D3JWVF9kYqcI;

    invoke-direct {v4, v3}, Lcom/squareup/ui/inventory/-$$Lambda$B1Bz5YZysi2nHZ0D3JWVF9kYqcI;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryController;)V

    .line 252
    invoke-virtual {p1, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 253
    invoke-direct {p0}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->shouldEnableSaveButton()Z

    move-result v3

    invoke-virtual {p1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 254
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/inventory/-$$Lambda$tkgkGGtVi_h0YGdEVSDpgziLoT4;

    invoke-direct {v1, v0}, Lcom/squareup/ui/inventory/-$$Lambda$tkgkGGtVi_h0YGdEVSDpgziLoT4;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryController;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/inventory/-$$Lambda$pDWGJdhomwoMtvql5FPCe5mVhlM;

    invoke-direct {v1, v0}, Lcom/squareup/ui/inventory/-$$Lambda$pDWGJdhomwoMtvql5FPCe5mVhlM;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryController;)V

    .line 255
    :goto_1
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 258
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method private getCountLabel(Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;)I
    .locals 1

    .line 119
    iget-boolean v0, p1, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->isRecount:Z

    if-eqz v0, :cond_0

    .line 120
    sget p1, Lcom/squareup/adjustinventory/R$string;->adjust_stock_specify_number_in_stock:I

    return p1

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

    iget-object p1, p1, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->inventoryAdjustmentReason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/inventory/AdjustInventoryController;->shouldIncreaseStockCount(Lcom/squareup/protos/client/InventoryAdjustmentReason;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget p1, Lcom/squareup/adjustinventory/R$string;->adjust_stock_specify_number_add_stock:I

    goto :goto_0

    :cond_1
    sget p1, Lcom/squareup/adjustinventory/R$string;->adjust_stock_specify_number_remove_stock:I

    :goto_0
    return p1
.end method

.method private setHelpTextToPreviousStockCount(Ljava/math/BigDecimal;Lcom/squareup/quantity/BigDecimalFormatter;)V
    .locals 3

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->stockCountHelpText:Lcom/squareup/widgets/MessageView;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/adjustinventory/R$string;->adjust_stock_specify_number_help_text_previous_stock:I

    .line 274
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 275
    invoke-virtual {p2, p1}, Lcom/squareup/quantity/BigDecimalFormatter;->format(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "count"

    invoke-virtual {v1, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 276
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 273
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setHelpTextToUpdatedStockCount(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/squareup/quantity/BigDecimalFormatter;Lcom/squareup/quantity/BigDecimalFormatter;Lcom/squareup/protos/client/InventoryAdjustmentReason;)V
    .locals 4

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

    invoke-virtual {v0, p5}, Lcom/squareup/ui/inventory/AdjustInventoryController;->shouldIncreaseStockCount(Lcom/squareup/protos/client/InventoryAdjustmentReason;)Z

    move-result p5

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->stockCountHelpText:Lcom/squareup/widgets/MessageView;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/adjustinventory/R$string;->adjust_stock_specify_number_help_text_new_stock:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 284
    invoke-virtual {p4, p1}, Lcom/squareup/quantity/BigDecimalFormatter;->format(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "old_count"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    if-eqz p5, :cond_0

    const-string v2, "+"

    goto :goto_0

    :cond_0
    const-string v2, "-"

    :goto_0
    const-string v3, "operator"

    .line 285
    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 286
    invoke-virtual {p3, p2}, Lcom/squareup/quantity/BigDecimalFormatter;->format(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object p3

    const-string v2, "change"

    invoke-virtual {v1, v2, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    if-eqz p5, :cond_1

    .line 289
    invoke-virtual {p1, p2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    goto :goto_1

    .line 290
    :cond_1
    invoke-virtual {p1, p2}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    .line 287
    :goto_1
    invoke-virtual {p4, p1}, Lcom/squareup/quantity/BigDecimalFormatter;->format(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "new_count"

    invoke-virtual {p3, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 291
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 292
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 283
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private shouldEnableSaveButton()Z
    .locals 1

    .line 306
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->stockCountEditRow:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 307
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private showSaveSpinner()V
    .locals 2

    .line 296
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->adjustStockContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 297
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->saveSpinnerContainer:Landroid/view/View;

    iget v1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->transitionTime:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    return-void
.end method

.method private toggleSaveSpinner(Landroid/view/View;Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;)V
    .locals 2

    .line 227
    sget-object v0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator$3;->$SwitchMap$com$squareup$ui$inventory$AdjustInventoryController$InventoryAdjustmentSaveStatus:[I

    invoke-virtual {p2}, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 p1, 0x2

    if-eq v0, p1, :cond_1

    const/4 p1, 0x3

    if-ne v0, p1, :cond_0

    goto :goto_0

    .line 238
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal InventoryAdjustmentSaveStatus:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 233
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->dismissSaveSpinner()V

    goto :goto_0

    .line 229
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->showSaveSpinner()V

    .line 230
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    .line 83
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 84
    invoke-direct {p0, p1}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->bindViews(Landroid/view/View;)V

    .line 86
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 87
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->transitionTime:I

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->inventoryTrackingHelpText:Lcom/squareup/widgets/MessageView;

    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/adjustinventory/R$string;->adjust_stock_help_text:I

    const-string v3, "dashboard"

    .line 90
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->dashboard_items_library_url:I

    .line 91
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->square_dashboard:I

    .line 92
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 93
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 89
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/inventory/-$$Lambda$B1Bz5YZysi2nHZ0D3JWVF9kYqcI;

    invoke-direct {v1, v0}, Lcom/squareup/ui/inventory/-$$Lambda$B1Bz5YZysi2nHZ0D3JWVF9kYqcI;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryController;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 99
    new-instance v0, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventorySpecifyNumberCoordinator$NnJeuXLfUVmPJszQ0U3nrFtE9JA;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventorySpecifyNumberCoordinator$NnJeuXLfUVmPJszQ0U3nrFtE9JA;-><init>(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;Landroid/view/View;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 108
    new-instance v0, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventorySpecifyNumberCoordinator$jTc2lBpRiATgN24s2V9QqaCEzB4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventorySpecifyNumberCoordinator$jTc2lBpRiATgN24s2V9QqaCEzB4;-><init>(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;Landroid/view/View;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Adjust Inventory Screen Shown"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 1

    .line 115
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Adjust Inventory Screen Dismissed"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$attach$1$AdjustInventorySpecifyNumberCoordinator(Landroid/view/View;)Lrx/Subscription;
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

    invoke-virtual {v0}, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventorySpecifyNumberScreenData()Lrx/Observable;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Lrx/Observable;->first()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventorySpecifyNumberCoordinator$bZQ0TwMf-GKbwgzLDc_oot---z0;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventorySpecifyNumberCoordinator$bZQ0TwMf-GKbwgzLDc_oot---z0;-><init>(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;Landroid/view/View;)V

    .line 102
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$3$AdjustInventorySpecifyNumberCoordinator(Landroid/view/View;)Lrx/Subscription;
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

    invoke-virtual {v0}, Lcom/squareup/ui/inventory/AdjustInventoryController;->inventoryAdjustmentSaveStatus()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventorySpecifyNumberCoordinator$wWzSm9L-SbsBlc0dUGfpRFhiO7E;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventorySpecifyNumberCoordinator$wWzSm9L-SbsBlc0dUGfpRFhiO7E;-><init>(Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;Landroid/view/View;)V

    .line 109
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$0$AdjustInventorySpecifyNumberCoordinator(Landroid/view/View;Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;)V
    .locals 0

    .line 103
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->displayScreenData(Landroid/view/View;Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;)V

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-direct {p0, p2}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->getActionBarConfig(Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method public synthetic lambda$null$2$AdjustInventorySpecifyNumberCoordinator(Landroid/view/View;Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;)V
    .locals 0

    .line 109
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;->toggleSaveSpinner(Landroid/view/View;Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;)V

    return-void
.end method
