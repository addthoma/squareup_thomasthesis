.class Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;
.super Ljava/lang/Object;
.source "EditInventoryStateController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditInventoryStateController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InventoryStockCountsDataFromResponse"
.end annotation


# instance fields
.field inventoryStockCounts:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field status:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;


# direct methods
.method constructor <init>(Ljava/util/LinkedHashMap;Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;",
            ")V"
        }
    .end annotation

    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    iput-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;->inventoryStockCounts:Ljava/util/LinkedHashMap;

    .line 248
    iput-object p2, p0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;->status:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    return-void
.end method
