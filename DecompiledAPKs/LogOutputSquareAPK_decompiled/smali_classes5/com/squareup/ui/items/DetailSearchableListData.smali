.class public final Lcom/squareup/ui/items/DetailSearchableListData;
.super Ljava/lang/Object;
.source "DetailSearchableListConfiguration.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J#\u0010\u000e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListData;",
        "",
        "unfilteredListSize",
        "",
        "filteredListData",
        "Lcom/squareup/cycler/DataSource;",
        "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
        "(ILcom/squareup/cycler/DataSource;)V",
        "getFilteredListData",
        "()Lcom/squareup/cycler/DataSource;",
        "getUnfilteredListSize",
        "()I",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final filteredListData:Lcom/squareup/cycler/DataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
            ">;"
        }
    .end annotation
.end field

.field private final unfilteredListSize:I


# direct methods
.method public constructor <init>(ILcom/squareup/cycler/DataSource;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/cycler/DataSource<",
            "+",
            "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
            ">;)V"
        }
    .end annotation

    const-string v0, "filteredListData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/ui/items/DetailSearchableListData;->unfilteredListSize:I

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListData;->filteredListData:Lcom/squareup/cycler/DataSource;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/items/DetailSearchableListData;ILcom/squareup/cycler/DataSource;ILjava/lang/Object;)Lcom/squareup/ui/items/DetailSearchableListData;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget p1, p0, Lcom/squareup/ui/items/DetailSearchableListData;->unfilteredListSize:I

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListData;->filteredListData:Lcom/squareup/cycler/DataSource;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListData;->copy(ILcom/squareup/cycler/DataSource;)Lcom/squareup/ui/items/DetailSearchableListData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListData;->unfilteredListSize:I

    return v0
.end method

.method public final component2()Lcom/squareup/cycler/DataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListData;->filteredListData:Lcom/squareup/cycler/DataSource;

    return-object v0
.end method

.method public final copy(ILcom/squareup/cycler/DataSource;)Lcom/squareup/ui/items/DetailSearchableListData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/cycler/DataSource<",
            "+",
            "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
            ">;)",
            "Lcom/squareup/ui/items/DetailSearchableListData;"
        }
    .end annotation

    const-string v0, "filteredListData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListData;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListData;-><init>(ILcom/squareup/cycler/DataSource;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListData;

    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListData;->unfilteredListSize:I

    iget v1, p1, Lcom/squareup/ui/items/DetailSearchableListData;->unfilteredListSize:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListData;->filteredListData:Lcom/squareup/cycler/DataSource;

    iget-object p1, p1, Lcom/squareup/ui/items/DetailSearchableListData;->filteredListData:Lcom/squareup/cycler/DataSource;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFilteredListData()Lcom/squareup/cycler/DataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
            ">;"
        }
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListData;->filteredListData:Lcom/squareup/cycler/DataSource;

    return-object v0
.end method

.method public final getUnfilteredListSize()I
    .locals 1

    .line 14
    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListData;->unfilteredListSize:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListData;->unfilteredListSize:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListData;->filteredListData:Lcom/squareup/cycler/DataSource;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DetailSearchableListData(unfilteredListSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/items/DetailSearchableListData;->unfilteredListSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", filteredListData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListData;->filteredListData:Lcom/squareup/cycler/DataSource;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
