.class public final Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;
.super Ljava/lang/Object;
.source "AllServicesDetailScreen_AllServicesDetailPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final barcodeScannerTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final convertItemsUrlHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ConvertItemsUrlHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final editItemGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final itemsAppletScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final launcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryAppletGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final topScreenCheckerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ConvertItemsUrlHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 74
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 75
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->busProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 76
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 77
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 78
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 79
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 80
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 81
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 82
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 83
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->topScreenCheckerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 84
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->itemsAppletScopeRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 85
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->editItemGatewayProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->launcherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->convertItemsUrlHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ConvertItemsUrlHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)",
            "Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 111
    new-instance v17, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;

    move-object/from16 v0, v17

    invoke-direct/range {v0 .. v16}, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v17
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/Features;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/ui/items/ConvertItemsUrlHelper;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;
    .locals 18

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 122
    new-instance v17, Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;

    move-object/from16 v0, v17

    invoke-direct/range {v0 .. v16}, Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/Features;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/ui/items/ConvertItemsUrlHelper;Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    return-object v17
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;
    .locals 18

    move-object/from16 v0, p0

    .line 94
    iget-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/cogs/Cogs;

    iget-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->topScreenCheckerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/ui/main/TopScreenChecker;

    iget-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->itemsAppletScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    iget-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->editItemGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/ui/items/EditItemGateway;

    iget-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iget-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->launcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/util/BrowserLauncher;

    iget-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->convertItemsUrlHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/ui/items/ConvertItemsUrlHelper;

    iget-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-static/range {v2 .. v17}, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/Features;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/ui/items/ConvertItemsUrlHelper;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/items/AllServicesDetailScreen_AllServicesDetailPresenter_Factory;->get()Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;

    move-result-object v0

    return-object v0
.end method
