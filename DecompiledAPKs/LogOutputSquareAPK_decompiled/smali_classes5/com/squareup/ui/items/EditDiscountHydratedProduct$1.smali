.class final Lcom/squareup/ui/items/EditDiscountHydratedProduct$1;
.super Ljava/lang/Object;
.source "EditDiscountHydratedProduct.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditDiscountHydratedProduct;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/ui/items/EditDiscountHydratedProduct;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getOrdinal(Lcom/squareup/ui/items/EditDiscountHydratedProduct;)I
    .locals 3

    .line 27
    instance-of v0, p1, Lcom/squareup/ui/items/CategoryWithItemCount;

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 29
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/items/ItemWithVariation;

    if-eqz v0, :cond_1

    const/4 p1, 0x1

    return p1

    .line 31
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/items/VariationWithParentItem;

    if-eqz v0, :cond_2

    const/4 p1, 0x2

    return p1

    .line 34
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported subtype of EditDiscountHydratedProduct for comparison: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public compare(Lcom/squareup/ui/items/EditDiscountHydratedProduct;Lcom/squareup/ui/items/EditDiscountHydratedProduct;)I
    .locals 2

    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditDiscountHydratedProduct$1;->getOrdinal(Lcom/squareup/ui/items/EditDiscountHydratedProduct;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/squareup/ui/items/EditDiscountHydratedProduct;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p2}, Lcom/squareup/ui/items/EditDiscountHydratedProduct$1;->getOrdinal(Lcom/squareup/ui/items/EditDiscountHydratedProduct;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-interface {p2}, Lcom/squareup/ui/items/EditDiscountHydratedProduct;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/ui/items/EditDiscountHydratedProduct;

    check-cast p2, Lcom/squareup/ui/items/EditDiscountHydratedProduct;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/EditDiscountHydratedProduct$1;->compare(Lcom/squareup/ui/items/EditDiscountHydratedProduct;Lcom/squareup/ui/items/EditDiscountHydratedProduct;)I

    move-result p1

    return p1
.end method
