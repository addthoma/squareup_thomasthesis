.class public Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen$Factory;
.super Ljava/lang/Object;
.source "DeleteCategoryConfirmationScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$1(Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;Landroid/content/Context;Lcom/squareup/ui/items/LibraryDeleter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 44
    sget-object p4, Lcom/squareup/catalog/event/CatalogFeature;->CATEGORY_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

    invoke-static {p1}, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;->access$100(Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;)Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p4, p0, p5}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 45
    invoke-static {p2}, Lflow/Flow;->get(Landroid/content/Context;)Lflow/Flow;

    move-result-object p0

    .line 46
    invoke-static {p1}, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;->access$100(Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;)Ljava/lang/String;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/items/-$$Lambda$DeleteCategoryConfirmationScreen$Factory$Sh22fHTUi-HnmL99jU_50gZJLCY;

    invoke-direct {p2, p0}, Lcom/squareup/ui/items/-$$Lambda$DeleteCategoryConfirmationScreen$Factory$Sh22fHTUi-HnmL99jU_50gZJLCY;-><init>(Lflow/Flow;)V

    invoke-virtual {p3, p1, p2}, Lcom/squareup/ui/items/LibraryDeleter;->deleteCategoryById(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$0(Lflow/Flow;)V
    .locals 1

    .line 47
    sget-object v0, Lcom/squareup/ui/items/CategoriesListScreen;->INSTANCE:Lcom/squareup/ui/items/CategoriesListScreen;

    invoke-virtual {p0, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 35
    const-class v0, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen$Component;

    .line 36
    invoke-interface {v0}, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen$Component;->libraryDeleter()Lcom/squareup/ui/items/LibraryDeleter;

    move-result-object v1

    .line 37
    invoke-interface {v0}, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen$Component;->analytics()Lcom/squareup/analytics/Analytics;

    move-result-object v0

    .line 38
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;

    .line 40
    new-instance v3, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v3, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/squareup/editbaseobject/R$string;->item_editing_category_delete_confirmation_delete:I

    new-instance v5, Lcom/squareup/ui/items/-$$Lambda$DeleteCategoryConfirmationScreen$Factory$VuRIzqSITx5NlwQN0Po1UYMkqqY;

    invoke-direct {v5, v0, v2, p1, v1}, Lcom/squareup/ui/items/-$$Lambda$DeleteCategoryConfirmationScreen$Factory$VuRIzqSITx5NlwQN0Po1UYMkqqY;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;Landroid/content/Context;Lcom/squareup/ui/items/LibraryDeleter;)V

    .line 41
    invoke-virtual {v3, v4, v5}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/editbaseobject/R$string;->item_editing_category_delete_confirmation_cancel:I

    .line 49
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 51
    invoke-static {v2}, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;->access$000(Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/editbaseobject/R$string;->item_editing_category_delete_confirmation_title:I

    .line 52
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 54
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 55
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 57
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
