.class public final Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;
.super Ljava/lang/Object;
.source "DetailSearchableListView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/ui/items/DetailSearchableListView;",
        ">",
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/items/DetailSearchableListView<",
        "TT;>;>;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final durationFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final itemPhotosProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final priceFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final tileAppearanceSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)V"
        }
    .end annotation

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->itemPhotosProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->percentageFormatterProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->durationFormatterProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->priceFormatterProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p5, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p7, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p8, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p9, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->tutorialCoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/ui/items/DetailSearchableListView;",
            ">(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/items/DetailSearchableListView<",
            "TT;>;>;"
        }
    .end annotation

    .line 72
    new-instance v10, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static injectCurrencyCode(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/ui/items/DetailSearchableListView;",
            ">(",
            "Lcom/squareup/ui/items/DetailSearchableListView<",
            "TT;>;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")V"
        }
    .end annotation

    .line 115
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method public static injectDevice(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/util/Device;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/ui/items/DetailSearchableListView;",
            ">(",
            "Lcom/squareup/ui/items/DetailSearchableListView<",
            "TT;>;",
            "Lcom/squareup/util/Device;",
            ")V"
        }
    .end annotation

    .line 121
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectDurationFormatter(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/text/DurationFormatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/ui/items/DetailSearchableListView;",
            ">(",
            "Lcom/squareup/ui/items/DetailSearchableListView<",
            "TT;>;",
            "Lcom/squareup/text/DurationFormatter;",
            ")V"
        }
    .end annotation

    .line 103
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    return-void
.end method

.method public static injectItemPhotos(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/ui/items/DetailSearchableListView;",
            ">(",
            "Lcom/squareup/ui/items/DetailSearchableListView<",
            "TT;>;",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ")V"
        }
    .end annotation

    .line 90
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    return-void
.end method

.method public static injectPercentageFormatter(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/ui/items/DetailSearchableListView;",
            ">(",
            "Lcom/squareup/ui/items/DetailSearchableListView<",
            "TT;>;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)V"
        }
    .end annotation

    .line 97
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->percentageFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static injectPriceFormatter(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/quantity/PerUnitFormatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/ui/items/DetailSearchableListView;",
            ">(",
            "Lcom/squareup/ui/items/DetailSearchableListView<",
            "TT;>;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ")V"
        }
    .end annotation

    .line 109
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/ui/items/DetailSearchableListView;",
            ">(",
            "Lcom/squareup/ui/items/DetailSearchableListView<",
            "TT;>;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .line 133
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static injectTileAppearanceSettings(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/ui/items/DetailSearchableListView;",
            ">(",
            "Lcom/squareup/ui/items/DetailSearchableListView<",
            "TT;>;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ")V"
        }
    .end annotation

    .line 127
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    return-void
.end method

.method public static injectTutorialCore(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/ui/items/DetailSearchableListView;",
            ">(",
            "Lcom/squareup/ui/items/DetailSearchableListView<",
            "TT;>;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ")V"
        }
    .end annotation

    .line 139
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/items/DetailSearchableListView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListView<",
            "TT;>;)V"
        }
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->itemPhotosProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->injectItemPhotos(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->percentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->injectPercentageFormatter(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/text/Formatter;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->durationFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/DurationFormatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->injectDurationFormatter(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/text/DurationFormatter;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->priceFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->injectPriceFormatter(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/quantity/PerUnitFormatter;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->injectCurrencyCode(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->injectDevice(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/util/Device;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->injectTileAppearanceSettings(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->injectRes(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/util/Res;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->injectTutorialCore(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/tutorialv2/TutorialCore;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListView_MembersInjector;->injectMembers(Lcom/squareup/ui/items/DetailSearchableListView;)V

    return-void
.end method
