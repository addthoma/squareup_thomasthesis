.class public Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "EditCategoryView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ViewHolder"
.end annotation


# instance fields
.field private row:Landroid/view/View;

.field final synthetic this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

.field private viewType:I


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;Landroid/view/View;I)V
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    .line 117
    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 118
    iput-object p2, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    .line 119
    iput p3, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->viewType:I

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;)Landroid/view/View;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    return-object p0
.end method


# virtual methods
.method public bindDelete()V
    .locals 3

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v1, Lcom/squareup/itemsapplet/R$id;->delete_message:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 189
    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    iget-object v1, v1, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    .line 190
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditCategoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/registerlib/R$string;->category_delete_message:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 189
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    .line 191
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v1, Lcom/squareup/itemsapplet/R$id;->delete_button:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    .line 193
    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    iget-object v1, v1, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object v1, v1, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$XrBCC02W3ofOzDuV7qZE4x5rmgA;

    invoke-direct {v2, v1}, Lcom/squareup/ui/items/-$$Lambda$XrBCC02W3ofOzDuV7qZE4x5rmgA;-><init>(Lcom/squareup/ui/items/EditCategoryScreen$Presenter;)V

    invoke-virtual {v0, v2}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    return-void
.end method

.method public bindItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;IZ)V
    .locals 16

    move-object/from16 v0, p0

    move/from16 v1, p2

    const/4 v2, 0x1

    if-nez v1, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 129
    :goto_0
    iget-object v4, v0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    iget-object v4, v4, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object v4, v4, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    invoke-virtual {v4, v1}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->itemInCurrentCategory(I)Z

    move-result v4

    .line 130
    iget-object v5, v0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    move-object v15, v5

    check-cast v15, Lcom/squareup/ui/items/CategoryAssignmentListRow;

    .line 131
    invoke-virtual {v15, v3, v2}, Lcom/squareup/ui/items/CategoryAssignmentListRow;->setHorizontalBorders(ZZ)V

    if-eqz v4, :cond_1

    .line 133
    iget-object v2, v0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    iget-object v2, v2, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object v2, v2, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    .line 134
    invoke-virtual {v2}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getCategoryName()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    iget-object v2, v0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    iget-object v2, v2, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object v2, v2, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    .line 135
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getCategoryId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getCategoryNameForId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v12, v2

    .line 136
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object v6

    iget-object v2, v0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    iget-object v2, v2, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object v7, v2, Lcom/squareup/ui/items/EditCategoryView;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getImageUrl()Ljava/lang/String;

    move-result-object v9

    .line 137
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getAbbreviation()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getColor()Ljava/lang/String;

    move-result-object v11

    move-object v5, v15

    move v13, v4

    move/from16 v14, p3

    .line 136
    invoke-virtual/range {v5 .. v14}, Lcom/squareup/ui/items/CategoryAssignmentListRow;->showItem(Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto$Factory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    if-eqz v4, :cond_2

    .line 140
    iget-object v2, v0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    invoke-static {v2}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->access$000(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 142
    :cond_2
    new-instance v2, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$1;

    invoke-direct {v2, v0, v1}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$1;-><init>(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;I)V

    invoke-virtual {v15, v2}, Lcom/squareup/ui/items/CategoryAssignmentListRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bindStaticTopRowContent()V
    .locals 6

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->access$100(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v2, Lcom/squareup/itemsapplet/R$id;->banner:I

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/AppliedLocationsBanner;

    .line 153
    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/AppliedLocationsBanner;->setVisibility(I)V

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v2, Lcom/squareup/itemsapplet/R$id;->edit_category_name:I

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketEditText;

    .line 157
    iget-object v2, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v3, Lcom/squareup/itemsapplet/R$id;->edit_category_top_image_tile:I

    .line 158
    invoke-static {v2, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    .line 159
    iget-object v3, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v4, Lcom/squareup/itemsapplet/R$id;->edit_category_top_text_tile:I

    .line 160
    invoke-static {v3, v4}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/orderentry/TextTile;

    .line 161
    iget-object v4, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v5, Lcom/squareup/itemsapplet/R$id;->edit_category_static_top_content_tile:I

    .line 162
    invoke-static {v4, v5}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 164
    iget-object v4, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    invoke-static {v4, v3, v2}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->access$200(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;Lcom/squareup/orderentry/TextTile;Lcom/squareup/register/widgets/EditCatalogObjectLabel;)V

    .line 166
    iget-object v4, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    iget-object v4, v4, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object v4, v4, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    invoke-virtual {v4}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getCategoryName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/squareup/marketfont/MarketEditText;->setText(Ljava/lang/CharSequence;)V

    .line 167
    new-instance v4, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$2;

    invoke-direct {v4, p0, v3, v2}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$2;-><init>(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;Lcom/squareup/orderentry/TextTile;Lcom/squareup/register/widgets/EditCatalogObjectLabel;)V

    invoke-virtual {v0, v4}, Lcom/squareup/marketfont/MarketEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v2, Lcom/squareup/itemsapplet/R$id;->edit_category_title:I

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 180
    iget-object v2, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    invoke-static {v2}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->access$400(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;)I

    move-result v2

    if-nez v2, :cond_1

    const/16 v1, 0x8

    .line 181
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 183
    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public getViewType()I
    .locals 1

    .line 123
    iget v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->viewType:I

    return v0
.end method
