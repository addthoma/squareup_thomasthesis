.class public final Lcom/squareup/ui/items/UnitListObjectModificationHandler;
.super Ljava/lang/Object;
.source "UnitListObjectModificationHandler.kt"

# interfaces
.implements Lcom/squareup/ui/items/DetailSearchableListEditHandler;
.implements Lcom/squareup/ui/items/DetailSearchableListCreateHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/UnitListObjectModificationHandler$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUnitListObjectModificationHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UnitListObjectModificationHandler.kt\ncom/squareup/ui/items/UnitListObjectModificationHandler\n*L\n1#1,102:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 &2\u00020\u00012\u00020\u0002:\u0001&B\u0017\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0002J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\n\u001a\u00020\u000eH\u0002J\u0008\u0010\u000f\u001a\u00020\u0006H\u0002JF\u0010\u0010\u001a$\u0012\u0004\u0012\u00020\u0012\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0013j\u0002`\u00140\u0011j\u0008\u0012\u0004\u0012\u00020\u0012`\u00152\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00190\u00172\u0006\u0010\n\u001a\u00020\u000bH\u0016JF\u0010\u001a\u001a$\u0012\u0004\u0012\u00020\u0012\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0013j\u0002`\u00140\u0011j\u0008\u0012\u0004\u0012\u00020\u0012`\u00152\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00190\u00172\u0006\u0010\n\u001a\u00020\u000eH\u0016J(\u0010\u001b\u001a\u001a\u0012\u0004\u0012\u00020\u001d\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00190\u001e0\u001c2\u0006\u0010\n\u001a\u00020\u0018H\u0002J\u000c\u0010\u001f\u001a\u00020 *\u00020!H\u0002J\u0018\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020$0#*\u0008\u0012\u0004\u0012\u00020!0%H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/ui/items/UnitListObjectModificationHandler;",
        "Lcom/squareup/ui/items/DetailSearchableListEditHandler;",
        "Lcom/squareup/ui/items/DetailSearchableListCreateHandler;",
        "res",
        "Lcom/squareup/util/Res;",
        "editUnitWorkflow",
        "Lcom/squareup/items/unit/EditUnitWorkflow;",
        "(Lcom/squareup/util/Res;Lcom/squareup/items/unit/EditUnitWorkflow;)V",
        "createInput",
        "Lcom/squareup/items/unit/EditUnitInput$CreateUnit;",
        "state",
        "Lcom/squareup/ui/items/DetailSearchableListState$Create;",
        "editInput",
        "Lcom/squareup/items/unit/EditUnitInput$ModifyExistingUnit;",
        "Lcom/squareup/ui/items/DetailSearchableListState$Edit;",
        "getWorkflow",
        "renderCreate",
        "",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "Lcom/squareup/ui/items/DetailSearchableListState;",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        "renderEdit",
        "resultHandler",
        "Lkotlin/Function1;",
        "Lcom/squareup/items/unit/EditUnitResult;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "toCatalogMeasurementUnit",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
        "toSelectableUnitSet",
        "",
        "Lcom/squareup/items/unit/SelectableUnit;",
        "Lcom/squareup/cycler/DataSource;",
        "Companion",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/items/UnitListObjectModificationHandler$Companion;


# instance fields
.field private final editUnitWorkflow:Lcom/squareup/items/unit/EditUnitWorkflow;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/UnitListObjectModificationHandler$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/UnitListObjectModificationHandler$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->Companion:Lcom/squareup/ui/items/UnitListObjectModificationHandler$Companion;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/items/unit/EditUnitWorkflow;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->editUnitWorkflow:Lcom/squareup/items/unit/EditUnitWorkflow;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/items/unit/EditUnitWorkflow;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/UnitListObjectModificationHandler;-><init>(Lcom/squareup/util/Res;Lcom/squareup/items/unit/EditUnitWorkflow;)V

    return-void
.end method

.method private final createInput(Lcom/squareup/ui/items/DetailSearchableListState$Create;)Lcom/squareup/items/unit/EditUnitInput$CreateUnit;
    .locals 3

    .line 61
    new-instance v0, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;

    .line 62
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListState$Create;->getListDataSource()Lcom/squareup/cycler/DataSource;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-direct {p0, v1}, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->toSelectableUnitSet(Lcom/squareup/cycler/DataSource;)Ljava/util/Set;

    move-result-object v1

    .line 63
    sget-object v2, Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;->GLOBAL:Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

    .line 64
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListState$Create;->getSearchTerm()Ljava/lang/String;

    move-result-object p1

    .line 61
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;-><init>(Ljava/util/Set;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;Ljava/lang/String;)V

    return-object v0
.end method

.method private final editInput(Lcom/squareup/ui/items/DetailSearchableListState$Edit;)Lcom/squareup/items/unit/EditUnitInput$ModifyExistingUnit;
    .locals 2

    .line 67
    new-instance v0, Lcom/squareup/items/unit/EditUnitInput$ModifyExistingUnit;

    .line 68
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListState$Edit;->getSelectedObject()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->toCatalogMeasurementUnit(Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    .line 69
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListState$Edit;->getListDataSource()Lcom/squareup/cycler/DataSource;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->toSelectableUnitSet(Lcom/squareup/cycler/DataSource;)Ljava/util/Set;

    move-result-object p1

    .line 67
    invoke-direct {v0, v1, p1}, Lcom/squareup/items/unit/EditUnitInput$ModifyExistingUnit;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Ljava/util/Set;)V

    return-object v0
.end method

.method private final getWorkflow()Lcom/squareup/items/unit/EditUnitWorkflow;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->editUnitWorkflow:Lcom/squareup/items/unit/EditUnitWorkflow;

    return-object v0
.end method

.method private final resultHandler(Lcom/squareup/ui/items/DetailSearchableListState;)Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            ")",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/items/unit/EditUnitResult;",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;>;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/ui/items/UnitListObjectModificationHandler$resultHandler$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/UnitListObjectModificationHandler$resultHandler$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method private final toCatalogMeasurementUnit(Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;
    .locals 1

    .line 87
    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    if-eqz v0, :cond_0

    .line 88
    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p1

    return-object p1

    .line 89
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Found non unit in unit data source"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final toSelectableUnitSet(Lcom/squareup/cycler/DataSource;)Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/DataSource<",
            "+",
            "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
            ">;)",
            "Ljava/util/Set<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;"
        }
    .end annotation

    .line 76
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v0, Ljava/util/Set;

    .line 78
    invoke-interface {p1}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 79
    invoke-interface {p1, v2}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    invoke-direct {p0, v3}, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->toCatalogMeasurementUnit(Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->res:Lcom/squareup/util/Res;

    invoke-static {v3, v4}, Lcom/squareup/items/unit/SelectableUnitKt;->toSelectableUnit(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/util/Res;)Lcom/squareup/items/unit/SelectableUnit;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public renderCreate(Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/items/DetailSearchableListState$Create;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "-",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;",
            "Lcom/squareup/ui/items/DetailSearchableListState$Create;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->getWorkflow()Lcom/squareup/items/unit/EditUnitWorkflow;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 43
    invoke-direct {p0, p2}, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->createInput(Lcom/squareup/ui/items/DetailSearchableListState$Create;)Lcom/squareup/items/unit/EditUnitInput$CreateUnit;

    move-result-object v3

    .line 44
    check-cast p2, Lcom/squareup/ui/items/DetailSearchableListState;

    invoke-direct {p0, p2}, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->resultHandler(Lcom/squareup/ui/items/DetailSearchableListState;)Lkotlin/jvm/functions/Function1;

    move-result-object v5

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p1

    .line 41
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1
.end method

.method public renderEdit(Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/items/DetailSearchableListState$Edit;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "-",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;",
            "Lcom/squareup/ui/items/DetailSearchableListState$Edit;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->getWorkflow()Lcom/squareup/items/unit/EditUnitWorkflow;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 34
    invoke-direct {p0, p2}, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->editInput(Lcom/squareup/ui/items/DetailSearchableListState$Edit;)Lcom/squareup/items/unit/EditUnitInput$ModifyExistingUnit;

    move-result-object v3

    .line 35
    check-cast p2, Lcom/squareup/ui/items/DetailSearchableListState;

    invoke-direct {p0, p2}, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->resultHandler(Lcom/squareup/ui/items/DetailSearchableListState;)Lkotlin/jvm/functions/Function1;

    move-result-object v5

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p1

    .line 32
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1
.end method
