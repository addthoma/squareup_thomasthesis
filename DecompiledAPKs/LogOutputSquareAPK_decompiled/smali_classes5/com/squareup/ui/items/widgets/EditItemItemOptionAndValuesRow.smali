.class public final Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;
.super Landroid/widget/LinearLayout;
.source "EditItemItemOptionAndValuesRow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditItemItemOptionAndValuesRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditItemItemOptionAndValuesRow.kt\ncom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow\n+ 2 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,92:1\n37#2,6:93\n*E\n*S KotlinDebug\n*F\n+ 1 EditItemItemOptionAndValuesRow.kt\ncom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow\n*L\n49#1,6:93\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000b\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u0010\r\u001a\u00020\u000c2\u0006\u0010\u000b\u001a\u00020\u000c8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u0010\u0013\u001a\u00020\u000c2\u0006\u0010\u000b\u001a\u00020\u000c8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0014\u0010\u000f\"\u0004\u0008\u0015\u0010\u0011R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "optionAndValuesPhoneView",
        "Lcom/squareup/noho/NohoRow;",
        "value",
        "",
        "optionName",
        "getOptionName",
        "()Ljava/lang/String;",
        "setOptionName",
        "(Ljava/lang/String;)V",
        "optionNameTabletView",
        "optionValues",
        "getOptionValues",
        "setOptionValues",
        "optionValuesTabletView",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private optionAndValuesPhoneView:Lcom/squareup/noho/NohoRow;

.field private optionNameTabletView:Lcom/squareup/noho/NohoRow;

.field private optionValuesTabletView:Lcom/squareup/noho/NohoRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    sget v0, Lcom/squareup/edititem/R$layout;->edit_item_item_option_and_values_row_content:I

    move-object v1, p0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {p1, v0, v1}, Landroid/widget/LinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 51
    sget-object v0, Lcom/squareup/edititem/R$styleable;->EditItemItemOptionAndValuesRow:[I

    const-string v1, "R.styleable.EditItemItemOptionAndValuesRow"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    sget v1, Lcom/squareup/edititem/R$style;->Widget_EditItemItemOptionAndValueRow:I

    .line 93
    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    const-string p2, "a"

    .line 95
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    sget p2, Lcom/squareup/edititem/R$styleable;->EditItemItemOptionAndValuesRow_sqOptionNamePhoneAppearance:I

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    .line 58
    sget v0, Lcom/squareup/edititem/R$styleable;->EditItemItemOptionAndValuesRow_sqOptionValuesPhoneAppearance:I

    invoke-virtual {p1, v0, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 60
    sget v1, Lcom/squareup/edititem/R$styleable;->EditItemItemOptionAndValuesRow_sqOptionNameTabletAppearance:I

    invoke-virtual {p1, v1, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 62
    sget v2, Lcom/squareup/edititem/R$styleable;->EditItemItemOptionAndValuesRow_sqOptionValuesTabletAppearance:I

    invoke-virtual {p1, v2, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p3

    .line 65
    sget v2, Lcom/squareup/edititem/R$styleable;->EditItemItemOptionAndValuesRow_android_minHeight:I

    .line 66
    sget v3, Lcom/squareup/noho/R$dimen;->noho_row_height:I

    .line 64
    invoke-virtual {p1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 71
    sget v3, Lcom/squareup/edititem/R$id;->edit_item_item_option_and_values_row_tablet_option_row:I

    invoke-virtual {p0, v3}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/noho/NohoRow;

    invoke-static {p0, v3}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->access$setOptionNameTabletView$p(Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;Lcom/squareup/noho/NohoRow;)V

    .line 73
    sget v3, Lcom/squareup/edititem/R$id;->edit_item_item_option_and_values_row_tablet_values_row:I

    invoke-virtual {p0, v3}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/noho/NohoRow;

    invoke-static {p0, v3}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->access$setOptionValuesTabletView$p(Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;Lcom/squareup/noho/NohoRow;)V

    .line 75
    sget v3, Lcom/squareup/edititem/R$id;->edit_item_item_option_and_values_row_phone_option_and_values_row:I

    invoke-virtual {p0, v3}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/noho/NohoRow;

    invoke-static {p0, v3}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->access$setOptionAndValuesPhoneView$p(Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;Lcom/squareup/noho/NohoRow;)V

    .line 78
    invoke-static {p0}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->access$getOptionNameTabletView$p(Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;)Lcom/squareup/noho/NohoRow;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3, v1}, Lcom/squareup/noho/NohoRow;->setLabelAppearanceId(I)V

    .line 79
    :cond_0
    invoke-static {p0}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->access$getOptionValuesTabletView$p(Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;)Lcom/squareup/noho/NohoRow;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, p3}, Lcom/squareup/noho/NohoRow;->setLabelAppearanceId(I)V

    .line 81
    :cond_1
    invoke-static {p0}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->access$getOptionAndValuesPhoneView$p(Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;)Lcom/squareup/noho/NohoRow;

    move-result-object p3

    if-eqz p3, :cond_2

    invoke-virtual {p3, p2}, Lcom/squareup/noho/NohoRow;->setLabelAppearanceId(I)V

    .line 82
    :cond_2
    invoke-static {p0}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->access$getOptionAndValuesPhoneView$p(Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;)Lcom/squareup/noho/NohoRow;

    move-result-object p2

    if-eqz p2, :cond_3

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoRow;->setDescriptionAppearanceId(I)V

    .line 84
    :cond_3
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 86
    invoke-static {p0}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->access$getOptionNameTabletView$p(Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;)Lcom/squareup/noho/NohoRow;

    move-result-object p3

    if-eqz p3, :cond_4

    invoke-virtual {p3, p2}, Lcom/squareup/noho/NohoRow;->setMinHeight(I)V

    .line 87
    :cond_4
    invoke-static {p0}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->access$getOptionValuesTabletView$p(Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;)Lcom/squareup/noho/NohoRow;

    move-result-object p3

    if-eqz p3, :cond_5

    invoke-virtual {p3, p2}, Lcom/squareup/noho/NohoRow;->setMinHeight(I)V

    .line 88
    :cond_5
    invoke-static {p0}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->access$getOptionAndValuesPhoneView$p(Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;)Lcom/squareup/noho/NohoRow;

    move-result-object p3

    if-eqz p3, :cond_6

    invoke-virtual {p3, p2}, Lcom/squareup/noho/NohoRow;->setMinHeight(I)V

    .line 89
    :cond_6
    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 18
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 19
    sget p3, Lcom/squareup/edititem/R$attr;->editItemItemOptionAndValueRowStyle:I

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getOptionAndValuesPhoneView$p(Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;)Lcom/squareup/noho/NohoRow;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionAndValuesPhoneView:Lcom/squareup/noho/NohoRow;

    return-object p0
.end method

.method public static final synthetic access$getOptionNameTabletView$p(Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;)Lcom/squareup/noho/NohoRow;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionNameTabletView:Lcom/squareup/noho/NohoRow;

    return-object p0
.end method

.method public static final synthetic access$getOptionValuesTabletView$p(Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;)Lcom/squareup/noho/NohoRow;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionValuesTabletView:Lcom/squareup/noho/NohoRow;

    return-object p0
.end method

.method public static final synthetic access$setOptionAndValuesPhoneView$p(Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;Lcom/squareup/noho/NohoRow;)V
    .locals 0

    .line 14
    iput-object p1, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionAndValuesPhoneView:Lcom/squareup/noho/NohoRow;

    return-void
.end method

.method public static final synthetic access$setOptionNameTabletView$p(Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;Lcom/squareup/noho/NohoRow;)V
    .locals 0

    .line 14
    iput-object p1, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionNameTabletView:Lcom/squareup/noho/NohoRow;

    return-void
.end method

.method public static final synthetic access$setOptionValuesTabletView$p(Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;Lcom/squareup/noho/NohoRow;)V
    .locals 0

    .line 14
    iput-object p1, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionValuesTabletView:Lcom/squareup/noho/NohoRow;

    return-void
.end method


# virtual methods
.method public final getOptionName()Ljava/lang/String;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionNameTabletView:Lcom/squareup/noho/NohoRow;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionAndValuesPhoneView:Lcom/squareup/noho/NohoRow;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public final getOptionValues()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionValuesTabletView:Lcom/squareup/noho/NohoRow;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionAndValuesPhoneView:Lcom/squareup/noho/NohoRow;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getDescription()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public final setOptionName(Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionNameTabletView:Lcom/squareup/noho/NohoRow;

    if-eqz v0, :cond_0

    move-object v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionAndValuesPhoneView:Lcom/squareup/noho/NohoRow;

    if-eqz v0, :cond_1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 31
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionNameTabletView:Lcom/squareup/noho/NohoRow;

    if-eqz p1, :cond_2

    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p1, v0}, Lcom/squareup/noho/NohoRowKt;->singleLineLabel(Lcom/squareup/noho/NohoRow;Landroid/text/TextUtils$TruncateAt;)V

    .line 32
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionAndValuesPhoneView:Lcom/squareup/noho/NohoRow;

    if-eqz p1, :cond_3

    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p1, v0}, Lcom/squareup/noho/NohoRowKt;->singleLineLabel(Lcom/squareup/noho/NohoRow;Landroid/text/TextUtils$TruncateAt;)V

    :cond_3
    return-void
.end method

.method public final setOptionValues(Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionValuesTabletView:Lcom/squareup/noho/NohoRow;

    if-eqz v0, :cond_0

    move-object v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionAndValuesPhoneView:Lcom/squareup/noho/NohoRow;

    if-eqz v0, :cond_1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 42
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionValuesTabletView:Lcom/squareup/noho/NohoRow;

    if-eqz p1, :cond_2

    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p1, v0}, Lcom/squareup/noho/NohoRowKt;->singleLineLabel(Lcom/squareup/noho/NohoRow;Landroid/text/TextUtils$TruncateAt;)V

    .line 43
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/items/widgets/EditItemItemOptionAndValuesRow;->optionAndValuesPhoneView:Lcom/squareup/noho/NohoRow;

    if-eqz p1, :cond_3

    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p1, v0}, Lcom/squareup/noho/NohoRowKt;->singleLineDescription(Lcom/squareup/noho/NohoRow;Landroid/text/TextUtils$TruncateAt;)V

    :cond_3
    return-void
.end method
