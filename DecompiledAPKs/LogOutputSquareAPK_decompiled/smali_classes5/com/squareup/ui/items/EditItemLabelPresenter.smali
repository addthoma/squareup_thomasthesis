.class Lcom/squareup/ui/items/EditItemLabelPresenter;
.super Lmortar/ViewPresenter;
.source "EditItemLabelPresenter.java"

# interfaces
.implements Lcom/squareup/camerahelper/CameraHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/items/EditItemLabelView;",
        ">;",
        "Lcom/squareup/camerahelper/CameraHelper$Listener;"
    }
.end annotation


# static fields
.field private static final EDITING_LABEL_KEY:Ljava/lang/String; = "editing_label"


# instance fields
.field private final cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

.field private screen:Lcom/squareup/ui/items/EditItemLabelScreen;

.field private final state:Lcom/squareup/ui/items/EditItemState;

.field private final tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemState;Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 47
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 49
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    .line 50
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    .line 51
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    .line 52
    iput-object p5, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->res:Lcom/squareup/util/Res;

    .line 53
    iput-object p6, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->flow:Lflow/Flow;

    .line 54
    iput-object p7, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method

.method private shouldShowItemPhoto()Z
    .locals 2

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    invoke-virtual {v0}, Lcom/squareup/ui/photo/ItemPhoto;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState;->itemImageState:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    sget-object v1, Lcom/squareup/ui/items/EditItemState$ItemImageState;->DELETED:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private updateActionBar(Lcom/squareup/marin/widgets/MarinActionBar;)V
    .locals 3

    .line 173
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditItemLabelPresenter$nFhGGCe9AoA4k1eHtdcyqAqa26I;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemLabelPresenter$nFhGGCe9AoA4k1eHtdcyqAqa26I;-><init>(Lcom/squareup/ui/items/EditItemLabelPresenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 177
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/edititem/R$string;->edit_item_label:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 178
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->hideSecondaryButton()V

    return-void
.end method


# virtual methods
.method choosePhotoClicked()V
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    invoke-interface {v0}, Lcom/squareup/camerahelper/CameraHelper;->startGallery()V

    return-void
.end method

.method colorChanged(Ljava/lang/String;)V
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->setColor(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    return-void
.end method

.method deleteBitmap()V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->deleteBitmap()V

    return-void
.end method

.method public dropView(Lcom/squareup/ui/items/EditItemLabelView;)V
    .locals 1

    .line 123
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemLabelPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    invoke-interface {v0, p0}, Lcom/squareup/camerahelper/CameraHelper;->dropListener(Lcom/squareup/camerahelper/CameraHelper$Listener;)V

    .line 126
    :cond_0
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/ui/items/EditItemLabelView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemLabelPresenter;->dropView(Lcom/squareup/ui/items/EditItemLabelView;)V

    return-void
.end method

.method editLabelClicked()V
    .locals 1

    .line 96
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemLabelPresenter;->shouldShowItemPhoto()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemLabelPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemLabelView;

    if-nez v0, :cond_1

    return-void

    .line 103
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemLabelView;->startEditingLabel()V

    return-void
.end method

.method getLabelColor()Ljava/lang/String;
    .locals 2

    .line 152
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemLabelPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemLabelView;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemLabelView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->defaultItemColor(Landroid/content/res/Resources;)I

    move-result v0

    invoke-static {v0}, Lcom/squareup/util/Colors;->toHex(I)Ljava/lang/String;

    move-result-object v0

    .line 153
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getColor()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getName()Ljava/lang/String;
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isService()Z
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method labelTextChanged(Ljava/lang/String;)V
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->setAbbreviation(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    return-void
.end method

.method public synthetic lambda$updateActionBar$0$EditItemLabelPresenter()V
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemScopeRunner;->itemLabelEditFinished()V

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method onBackPressed()Z
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemScopeRunner;->itemLabelEditFinished()V

    const/4 v0, 0x0

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 58
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemLabelScreen;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->screen:Lcom/squareup/ui/items/EditItemLabelScreen;

    return-void
.end method

.method public onImageCanceled()V
    .locals 0

    return-void
.end method

.method public onImagePicked(Landroid/net/Uri;)V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iput-object p1, v0, Lcom/squareup/ui/items/EditItemState;->newImageToEdit:Landroid/net/Uri;

    .line 131
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/items/EditItemPhotoScreen;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->screen:Lcom/squareup/ui/items/EditItemLabelScreen;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemLabelScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditItemPhotoScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 9

    .line 62
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getName()Ljava/lang/String;

    move-result-object v0

    .line 64
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getAbbreviation()Ljava/lang/String;

    move-result-object v1

    .line 65
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Lcom/squareup/util/Strings;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    move-object v4, v1

    .line 66
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemLabelPresenter;->shouldShowItemPhoto()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    move-object v6, v1

    .line 67
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v1

    .line 69
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemLabelPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lcom/squareup/ui/items/EditItemLabelView;

    .line 70
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemLabelPresenter;->isService()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/edititem/R$string;->new_service:I

    .line 71
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/widgets/pos/R$string;->new_item:I

    .line 72
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 73
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemLabelPresenter;->getLabelColor()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-boolean v5, v5, Lcom/squareup/ui/items/EditItemState;->isNewItem:Z

    if-eqz v5, :cond_3

    .line 74
    invoke-static {v0, v2}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_3
    move-object v5, v0

    move-object v2, v8

    move v7, v1

    .line 73
    invoke-virtual/range {v2 .. v7}, Lcom/squareup/ui/items/EditItemLabelView;->setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto;Z)V

    const/4 v0, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_4

    const-string v3, "editing_label"

    .line 77
    invoke-virtual {p1, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_4

    const/4 v2, 0x1

    :cond_4
    if-eqz v2, :cond_5

    .line 80
    invoke-virtual {v8}, Lcom/squareup/ui/items/EditItemLabelView;->getEditItemLabel()Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->enableAbbreviationEditing()V

    .line 85
    :cond_5
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    invoke-interface {p1}, Lcom/squareup/camerahelper/CameraHelper;->isGalleryAvailable()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 86
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemLabelPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemLabelView;

    xor-int/2addr v0, v1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditItemLabelView;->setPhotoSectionVisibility(Z)V

    .line 89
    :cond_6
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemLabelPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemLabelView;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemLabelView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemLabelPresenter;->updateActionBar(Lcom/squareup/marin/widgets/MarinActionBar;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    invoke-interface {p1, p0}, Lcom/squareup/camerahelper/CameraHelper;->setListener(Lcom/squareup/camerahelper/CameraHelper$Listener;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Edit Item Label Screen Shown"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 115
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 116
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemLabelPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 118
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemLabelPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemLabelView;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemLabelView;->getEditItemLabel()Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 119
    invoke-virtual {v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->isEditingTitle()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    const-string v1, "editing_label"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method takePhotoClicked()V
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelPresenter;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    invoke-interface {v0}, Lcom/squareup/camerahelper/CameraHelper;->startCamera()V

    return-void
.end method
