.class public final Lcom/squareup/ui/items/EditDiscountView_MembersInjector;
.super Ljava/lang/Object;
.source "EditDiscountView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/items/EditDiscountView;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final percentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditDiscountScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private final priceHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final shortMoneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditDiscountScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p2, p0, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->priceHelperProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p3, p0, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p4, p0, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p5, p0, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->percentageFormatterProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p6, p0, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->shortMoneyFormatterProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p7, p0, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p8, p0, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditDiscountScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/items/EditDiscountView;",
            ">;"
        }
    .end annotation

    .line 67
    new-instance v9, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static injectCurrencyCode(Lcom/squareup/ui/items/EditDiscountView;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method public static injectFeatures(Lcom/squareup/ui/items/EditDiscountView;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectMoneyFormatter(Lcom/squareup/ui/items/EditDiscountView;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditDiscountView;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 94
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static injectPercentageFormatter(Lcom/squareup/ui/items/EditDiscountView;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditDiscountView;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)V"
        }
    .end annotation

    .line 106
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->percentageFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/items/EditDiscountView;Ljava/lang/Object;)V
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->presenter:Lcom/squareup/ui/items/EditDiscountScreen$Presenter;

    return-void
.end method

.method public static injectPriceHelper(Lcom/squareup/ui/items/EditDiscountView;Lcom/squareup/money/PriceLocaleHelper;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->priceHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/items/EditDiscountView;Lcom/squareup/util/Res;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static injectShortMoneyFormatter(Lcom/squareup/ui/items/EditDiscountView;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditDiscountView;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 113
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/items/EditDiscountView;)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->injectPresenter(Lcom/squareup/ui/items/EditDiscountView;Ljava/lang/Object;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->priceHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/money/PriceLocaleHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->injectPriceHelper(Lcom/squareup/ui/items/EditDiscountView;Lcom/squareup/money/PriceLocaleHelper;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->injectMoneyFormatter(Lcom/squareup/ui/items/EditDiscountView;Lcom/squareup/text/Formatter;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->injectCurrencyCode(Lcom/squareup/ui/items/EditDiscountView;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->percentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->injectPercentageFormatter(Lcom/squareup/ui/items/EditDiscountView;Lcom/squareup/text/Formatter;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->shortMoneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->injectShortMoneyFormatter(Lcom/squareup/ui/items/EditDiscountView;Lcom/squareup/text/Formatter;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->injectRes(Lcom/squareup/ui/items/EditDiscountView;Lcom/squareup/util/Res;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->injectFeatures(Lcom/squareup/ui/items/EditDiscountView;Lcom/squareup/settings/server/Features;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/ui/items/EditDiscountView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditDiscountView_MembersInjector;->injectMembers(Lcom/squareup/ui/items/EditDiscountView;)V

    return-void
.end method
