.class Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "ModifierSetAssignmentView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;->bindItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;

.field final synthetic val$entry:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

.field final synthetic val$itemPosition:I


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;I)V
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder$1;->this$2:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;

    iput-object p2, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder$1;->val$entry:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    iput p3, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder$1;->val$itemPosition:I

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 129
    iget-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder$1;->this$2:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;

    iget-object p1, p1, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;

    iget-object p1, p1, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;->this$0:Lcom/squareup/ui/items/ModifierSetAssignmentView;

    iget-object p1, p1, Lcom/squareup/ui/items/ModifierSetAssignmentView;->presenter:Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;

    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder$1;->val$entry:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->itemSelected(Ljava/lang/String;)V

    .line 130
    iget-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder$1;->this$2:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;

    iget-object p1, p1, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;

    iget v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder$1;->val$itemPosition:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;->notifyItemChanged(I)V

    return-void
.end method
