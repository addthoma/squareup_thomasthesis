.class final Lcom/squareup/ui/items/EditItemState$TaxState;
.super Ljava/lang/Object;
.source "EditItemState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "TaxState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditItemState$TaxState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field applied:Z

.field final enabled:Z

.field final id:Ljava/lang/String;

.field private final membership:[B

.field final name:Ljava/lang/String;

.field originallyApplied:Z

.field final percentage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1020
    new-instance v0, Lcom/squareup/ui/items/EditItemState$TaxState$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/EditItemState$TaxState$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/EditItemState$TaxState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BZ)V
    .locals 0

    .line 997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 998
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->id:Ljava/lang/String;

    .line 999
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->name:Ljava/lang/String;

    .line 1000
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->percentage:Ljava/lang/String;

    .line 1001
    iput-boolean p4, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->applied:Z

    .line 1002
    iput-boolean p4, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->originallyApplied:Z

    .line 1003
    iput-object p5, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->membership:[B

    .line 1004
    iput-boolean p6, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->enabled:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BZLcom/squareup/ui/items/EditItemState$1;)V
    .locals 0

    .line 987
    invoke-direct/range {p0 .. p6}, Lcom/squareup/ui/items/EditItemState$TaxState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[BZ)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BZZZ)V
    .locals 0

    .line 1009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1010
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->id:Ljava/lang/String;

    .line 1011
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->name:Ljava/lang/String;

    .line 1012
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->percentage:Ljava/lang/String;

    .line 1013
    iput-boolean p6, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->applied:Z

    .line 1014
    iput-boolean p6, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->originallyApplied:Z

    .line 1015
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->membership:[B

    .line 1016
    iput-boolean p5, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->enabled:Z

    .line 1017
    iput-boolean p7, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->originallyApplied:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BZZZLcom/squareup/ui/items/EditItemState$1;)V
    .locals 0

    .line 987
    invoke-direct/range {p0 .. p7}, Lcom/squareup/ui/items/EditItemState$TaxState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BZZZ)V

    return-void
.end method

.method static synthetic access$900(Lcom/squareup/ui/items/EditItemState$TaxState;)[B
    .locals 0

    .line 987
    iget-object p0, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->membership:[B

    return-object p0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1042
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->id:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1043
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->name:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1044
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->percentage:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1045
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->membership:[B

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 1046
    iget-boolean p2, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->enabled:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    .line 1047
    iget-boolean p2, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->applied:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    .line 1048
    iget-boolean p2, p0, Lcom/squareup/ui/items/EditItemState$TaxState;->originallyApplied:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    return-void
.end method
