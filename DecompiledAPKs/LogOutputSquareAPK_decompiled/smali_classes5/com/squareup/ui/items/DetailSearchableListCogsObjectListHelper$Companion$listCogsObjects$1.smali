.class final Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$1;
.super Ljava/lang/Object;
.source "DetailSearchableListCogsObjectListHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;->listCogsObjects(Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Lcom/squareup/analytics/Analytics;Ljava/util/List;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lcom/squareup/cogs/CatalogUpdateEvent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/cogs/CatalogUpdateEvent;",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $objectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/models/CatalogObjectType;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$1;->$objectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final test(Lcom/squareup/cogs/CatalogUpdateEvent;)Z
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper;->Companion:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$1;->$objectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-static {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;->access$updateEventTarget(Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;Lcom/squareup/shared/catalog/models/CatalogObjectType;)[Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v0

    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {p1, v0}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasOneOf([Lcom/squareup/shared/catalog/models/CatalogObjectType;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/cogs/CatalogUpdateEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$1;->test(Lcom/squareup/cogs/CatalogUpdateEvent;)Z

    move-result p1

    return p1
.end method
