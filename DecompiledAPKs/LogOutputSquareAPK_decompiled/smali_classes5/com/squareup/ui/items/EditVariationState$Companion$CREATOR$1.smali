.class public final Lcom/squareup/ui/items/EditVariationState$Companion$CREATOR$1;
.super Ljava/lang/Object;
.source "EditVariationState.kt"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditVariationState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/items/EditVariationState;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditVariationState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditVariationState.kt\ncom/squareup/ui/items/EditVariationState$Companion$CREATOR$1\n+ 2 Parcels.kt\ncom/squareup/util/Parcels\n*L\n1#1,183:1\n23#2:184\n*E\n*S KotlinDebug\n*F\n+ 1 EditVariationState.kt\ncom/squareup/ui/items/EditVariationState$Companion$CREATOR$1\n*L\n151#1:184\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000%\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u001d\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "com/squareup/ui/items/EditVariationState$Companion$CREATOR$1",
        "Landroid/os/Parcelable$Creator;",
        "Lcom/squareup/ui/items/EditVariationState;",
        "createFromParcel",
        "in",
        "Landroid/os/Parcel;",
        "newArray",
        "",
        "size",
        "",
        "(I)[Lcom/squareup/ui/items/EditVariationState;",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditVariationState;
    .locals 10

    const-string v0, "in"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-static {}, Lcom/squareup/ui/items/ItemVariationEditingState;->values()[Lcom/squareup/ui/items/ItemVariationEditingState;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v3, v0, v1

    .line 129
    sget-object v0, Lcom/squareup/ui/items/ItemVariationEditingState;->NOT_STARTED:Lcom/squareup/ui/items/ItemVariationEditingState;

    const/4 v1, 0x0

    if-eq v3, v0, :cond_0

    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->fromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    move-object v4, v0

    goto :goto_0

    :cond_0
    move-object v4, v1

    .line 134
    :goto_0
    sget-object v0, Lcom/squareup/ui/items/ItemVariationEditingState;->NOT_STARTED:Lcom/squareup/ui/items/ItemVariationEditingState;

    const-string v2, "null cannot be cast to non-null type kotlin.Boolean"

    const/4 v5, 0x0

    if-eq v3, v0, :cond_2

    .line 135
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_1

    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    .line 140
    sget-object v0, Lcom/squareup/ui/items/ItemVariationEditingState;->NOT_STARTED:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-eq v3, v0, :cond_3

    .line 141
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->fromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 145
    :goto_2
    sget-object v6, Lcom/squareup/ui/items/ItemVariationEditingState;->NOT_STARTED:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-eq v3, v6, :cond_4

    .line 146
    const-class v6, Ljava/math/BigDecimal;

    invoke-virtual {v6}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/math/BigDecimal;

    goto :goto_3

    :cond_4
    move-object v6, v1

    .line 150
    :goto_3
    sget-object v7, Lcom/squareup/ui/items/ItemVariationEditingState;->NOT_STARTED:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-eq v3, v7, :cond_5

    .line 184
    const-class v7, Lcom/squareup/protos/common/Money;

    invoke-static {p1, v7}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v7

    check-cast v7, Lcom/squareup/protos/common/Money;

    goto :goto_4

    :cond_5
    move-object v7, v1

    .line 155
    :goto_4
    sget-object v8, Lcom/squareup/ui/items/ItemVariationEditingState;->NOT_STARTED:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-eq v3, v8, :cond_7

    .line 156
    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_6

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    goto :goto_5

    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    :goto_5
    if-eqz v5, :cond_8

    .line 160
    sget-object v2, Lcom/squareup/ui/items/ItemVariationEditingState;->NOT_STARTED:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-eq v3, v2, :cond_8

    .line 161
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->fromByteArray([B)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p1

    move-object v8, p1

    goto :goto_6

    :cond_8
    move-object v8, v1

    .line 165
    :goto_6
    new-instance p1, Lcom/squareup/ui/items/EditVariationState;

    const/4 v9, 0x0

    move-object v2, p1

    move-object v5, v0

    invoke-direct/range {v2 .. v9}, Lcom/squareup/ui/items/EditVariationState;-><init>(Lcom/squareup/ui/items/ItemVariationEditingState;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 125
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditVariationState$Companion$CREATOR$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditVariationState;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/items/EditVariationState;
    .locals 0

    .line 172
    new-array p1, p1, [Lcom/squareup/ui/items/EditVariationState;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 125
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditVariationState$Companion$CREATOR$1;->newArray(I)[Lcom/squareup/ui/items/EditVariationState;

    move-result-object p1

    return-object p1
.end method
