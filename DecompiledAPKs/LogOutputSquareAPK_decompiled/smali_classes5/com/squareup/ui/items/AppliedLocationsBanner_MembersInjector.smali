.class public final Lcom/squareup/ui/items/AppliedLocationsBanner_MembersInjector;
.super Ljava/lang/Object;
.source "AppliedLocationsBanner_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/items/AppliedLocationsBanner;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/items/AppliedLocationsBanner_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 22
    iput-object p2, p0, Lcom/squareup/ui/items/AppliedLocationsBanner_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/items/AppliedLocationsBanner;",
            ">;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/ui/items/AppliedLocationsBanner_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/AppliedLocationsBanner_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectDevice(Lcom/squareup/ui/items/AppliedLocationsBanner;Lcom/squareup/util/Device;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/items/AppliedLocationsBanner;Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/items/AppliedLocationsBanner;->presenter:Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/items/AppliedLocationsBanner;)V
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/AppliedLocationsBanner_MembersInjector;->injectPresenter(Lcom/squareup/ui/items/AppliedLocationsBanner;Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBanner_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/AppliedLocationsBanner_MembersInjector;->injectDevice(Lcom/squareup/ui/items/AppliedLocationsBanner;Lcom/squareup/util/Device;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/items/AppliedLocationsBanner;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/AppliedLocationsBanner_MembersInjector;->injectMembers(Lcom/squareup/ui/items/AppliedLocationsBanner;)V

    return-void
.end method
