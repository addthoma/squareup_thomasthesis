.class public final Lcom/squareup/ui/items/EditItemTooManyVariationsErrorDialogScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "EditItemTooManyVariationsErrorDialogScreen.kt"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/items/EditItemTooManyVariationsErrorDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditItemTooManyVariationsErrorDialogScreen$Factory;,
        Lcom/squareup/ui/items/EditItemTooManyVariationsErrorDialogScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0007\u0018\u0000 \u00082\u00020\u00012\u00020\u0002:\u0002\u0008\tB\u000f\u0008\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditItemTooManyVariationsErrorDialogScreen;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "Lcom/squareup/container/MaybePersistent;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "Companion",
        "Factory",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/items/EditItemTooManyVariationsErrorDialogScreen$Companion;

.field public static final MAXIMUM_VARIATION_PER_ITEM:I = 0xfa


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/EditItemTooManyVariationsErrorDialogScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditItemTooManyVariationsErrorDialogScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/EditItemTooManyVariationsErrorDialogScreen;->Companion:Lcom/squareup/ui/items/EditItemTooManyVariationsErrorDialogScreen$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemTooManyVariationsErrorDialogScreen;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public final getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemTooManyVariationsErrorDialogScreen;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    .line 19
    invoke-static {p0}, Lcom/squareup/container/MaybePersistent$DefaultImpls;->isPersistent(Lcom/squareup/container/MaybePersistent;)Z

    move-result v0

    return v0
.end method
