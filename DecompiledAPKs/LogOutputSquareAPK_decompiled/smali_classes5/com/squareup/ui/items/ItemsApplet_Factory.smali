.class public final Lcom/squareup/ui/items/ItemsApplet_Factory;
.super Ljava/lang/Object;
.source "ItemsApplet_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/ItemsApplet;",
        ">;"
    }
.end annotation


# instance fields
.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final sectionsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSectionList;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSectionList;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/items/ItemsApplet_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/items/ItemsApplet_Factory;->sectionsProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/items/ItemsApplet_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/ItemsApplet_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletSectionList;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)",
            "Lcom/squareup/ui/items/ItemsApplet_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/ui/items/ItemsApplet_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/items/ItemsApplet_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ldagger/Lazy;Lcom/squareup/ui/items/ItemsAppletSectionList;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/ui/items/ItemsApplet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/ui/items/ItemsAppletSectionList;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ")",
            "Lcom/squareup/ui/items/ItemsApplet;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/ui/items/ItemsApplet;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/items/ItemsApplet;-><init>(Ldagger/Lazy;Lcom/squareup/ui/items/ItemsAppletSectionList;Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/ItemsApplet;
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsApplet_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/ItemsApplet_Factory;->sectionsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/items/ItemsAppletSectionList;

    iget-object v2, p0, Lcom/squareup/ui/items/ItemsApplet_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/items/ItemsApplet_Factory;->newInstance(Ldagger/Lazy;Lcom/squareup/ui/items/ItemsAppletSectionList;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/ui/items/ItemsApplet;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemsApplet_Factory;->get()Lcom/squareup/ui/items/ItemsApplet;

    move-result-object v0

    return-object v0
.end method
