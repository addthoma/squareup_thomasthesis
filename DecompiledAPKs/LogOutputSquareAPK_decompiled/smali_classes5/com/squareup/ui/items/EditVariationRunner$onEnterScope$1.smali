.class final Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditVariationRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditVariationRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditVariationRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditVariationRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 70
    check-cast p1, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->invoke(Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;)V
    .locals 6

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-virtual {p1}, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;->getToken()J

    move-result-wide v0

    .line 125
    invoke-virtual {p1}, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;->getDuration()Lorg/threeten/bp/Duration;

    move-result-object p1

    .line 128
    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditVariationRunner;->getDurationToken()Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v0, v4

    if-nez v2, :cond_2

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/EditVariationRunner;->access$ensureEditingStarted(Lcom/squareup/ui/items/EditVariationRunner;)V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationRunner;->getEditItemVariationState()Lcom/squareup/ui/items/EditVariationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setDuration(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 131
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/EditVariationRunner;->access$updateServiceScreenData(Lcom/squareup/ui/items/EditVariationRunner;)V

    .line 132
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {p1, v3}, Lcom/squareup/ui/items/EditVariationRunner;->setDurationToken(Ljava/lang/Long;)V

    goto/16 :goto_4

    .line 135
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditVariationRunner;->getInitialDurationToken()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_3

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v0, v4

    if-nez v2, :cond_5

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/EditVariationRunner;->access$ensureEditingStarted(Lcom/squareup/ui/items/EditVariationRunner;)V

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationRunner;->getEditItemVariationState()Lcom/squareup/ui/items/EditVariationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    invoke-static {v0, p1}, Lcom/squareup/intermission/IntermissionHelperKt;->updateInitialTime(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lorg/threeten/bp/Duration;)V

    .line 138
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/EditVariationRunner;->access$updateServiceScreenData(Lcom/squareup/ui/items/EditVariationRunner;)V

    .line 139
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {p1, v3}, Lcom/squareup/ui/items/EditVariationRunner;->setInitialDurationToken(Ljava/lang/Long;)V

    .line 140
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/EditVariationRunner;->access$getAnalytics$p(Lcom/squareup/ui/items/EditVariationRunner;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->GAP_TIME_MODIFIED_SERVICE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    goto/16 :goto_4

    .line 143
    :cond_5
    :goto_1
    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditVariationRunner;->getGapDurationToken()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_6

    goto :goto_2

    :cond_6
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v0, v4

    if-nez v2, :cond_8

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/EditVariationRunner;->access$ensureEditingStarted(Lcom/squareup/ui/items/EditVariationRunner;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationRunner;->getEditItemVariationState()Lcom/squareup/ui/items/EditVariationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-nez v0, :cond_7

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_7
    invoke-static {v0, p1}, Lcom/squareup/intermission/IntermissionHelperKt;->updateGapTime(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lorg/threeten/bp/Duration;)V

    .line 146
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/EditVariationRunner;->access$updateServiceScreenData(Lcom/squareup/ui/items/EditVariationRunner;)V

    .line 147
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {p1, v3}, Lcom/squareup/ui/items/EditVariationRunner;->setGapDurationToken(Ljava/lang/Long;)V

    .line 148
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/EditVariationRunner;->access$getAnalytics$p(Lcom/squareup/ui/items/EditVariationRunner;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->GAP_TIME_MODIFIED_SERVICE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    goto/16 :goto_4

    .line 151
    :cond_8
    :goto_2
    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditVariationRunner;->getFinalDurationToken()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_9

    goto :goto_3

    :cond_9
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v0, v4

    if-nez v2, :cond_b

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/EditVariationRunner;->access$ensureEditingStarted(Lcom/squareup/ui/items/EditVariationRunner;)V

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationRunner;->getEditItemVariationState()Lcom/squareup/ui/items/EditVariationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-nez v0, :cond_a

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_a
    invoke-static {v0, p1}, Lcom/squareup/intermission/IntermissionHelperKt;->updateFinalTime(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lorg/threeten/bp/Duration;)V

    .line 154
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/EditVariationRunner;->access$updateServiceScreenData(Lcom/squareup/ui/items/EditVariationRunner;)V

    .line 155
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {p1, v3}, Lcom/squareup/ui/items/EditVariationRunner;->setFinalDurationToken(Ljava/lang/Long;)V

    .line 156
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/EditVariationRunner;->access$getAnalytics$p(Lcom/squareup/ui/items/EditVariationRunner;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->GAP_TIME_MODIFIED_SERVICE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    goto :goto_4

    .line 159
    :cond_b
    :goto_3
    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditVariationRunner;->getExtraTimeToken()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_c

    goto :goto_4

    :cond_c
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v0, v4

    if-nez v2, :cond_e

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/EditVariationRunner;->access$ensureEditingStarted(Lcom/squareup/ui/items/EditVariationRunner;)V

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationRunner;->getEditItemVariationState()Lcom/squareup/ui/items/EditVariationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setTransitionTime(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 162
    :cond_d
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/EditVariationRunner;->access$updateServiceScreenData(Lcom/squareup/ui/items/EditVariationRunner;)V

    .line 163
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {p1, v3}, Lcom/squareup/ui/items/EditVariationRunner;->setExtraTimeToken(Ljava/lang/Long;)V

    :cond_e
    :goto_4
    return-void
.end method
