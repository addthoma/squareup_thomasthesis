.class public final Lcom/squareup/ui/items/DetailSearchableListInput$CREATOR;
.super Ljava/lang/Object;
.source "DetailSearchableListWorkflow.kt"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DetailSearchableListInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CREATOR"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/items/DetailSearchableListInput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u001d\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016\u00a2\u0006\u0002\u0010\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListInput$CREATOR;",
        "Landroid/os/Parcelable$Creator;",
        "Lcom/squareup/ui/items/DetailSearchableListInput;",
        "()V",
        "createFromParcel",
        "parcel",
        "Landroid/os/Parcel;",
        "newArray",
        "",
        "size",
        "",
        "(I)[Lcom/squareup/ui/items/DetailSearchableListInput;",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 67
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListInput$CREATOR;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/items/DetailSearchableListInput;
    .locals 2

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListInput;

    .line 70
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v1, Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    .line 71
    invoke-static {p1}, Lcom/squareup/util/Parcels;->readBooleanFromInt(Landroid/os/Parcel;)Z

    move-result p1

    .line 69
    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/items/DetailSearchableListInput;-><init>(Lcom/squareup/ui/items/DetailSearchableListConfiguration;Z)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 67
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListInput$CREATOR;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/items/DetailSearchableListInput;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/items/DetailSearchableListInput;
    .locals 0

    .line 75
    new-array p1, p1, [Lcom/squareup/ui/items/DetailSearchableListInput;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 67
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListInput$CREATOR;->newArray(I)[Lcom/squareup/ui/items/DetailSearchableListInput;

    move-result-object p1

    return-object p1
.end method
