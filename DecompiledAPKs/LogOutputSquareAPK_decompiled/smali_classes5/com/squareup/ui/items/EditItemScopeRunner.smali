.class public Lcom/squareup/ui/items/EditItemScopeRunner;
.super Ljava/lang/Object;
.source "EditItemScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/ui/inventory/AdjustInventoryHost;
.implements Lcom/squareup/ui/items/EditServicePriceTypeSelectionScreen$EditServicePriceTypeSelectionRunner;
.implements Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;
.implements Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutputHandler;
.implements Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditItemScopeRunner$NextSaveStep;,
        Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;,
        Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;,
        Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;
    }
.end annotation


# static fields
.field static final DEFAULT_SERVICE_DURATION:J = 0x1b7740L

.field private static final INITIAL_LOAD:Z = false

.field private static final RELOAD_FOR_INVENTORY_ASSIGNMENT:Z = true


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

.field private addSingleVariationWithOptionRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final adjustInventoryStarter:Lcom/squareup/ui/inventory/AdjustInventoryStarter;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

.field private bookableChangeRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private final catalogServiceEndpoint:Lcom/squareup/catalog/CatalogServiceEndpoint;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private defaultVariationUnitTypeChangeRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private durationChangeRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

.field private editItemPath:Lcom/squareup/ui/items/EditItemScope;

.field private final editItemState:Lcom/squareup/ui/items/EditItemState;

.field private final editVariationRunner:Lcom/squareup/ui/items/EditVariationRunner;

.field private final eventLogger:Lcom/squareup/ui/items/EditItemEventLogger;

.field private final favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private itemLabelEditFinishedRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private oneEditItemStateCached:Lrx/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Single<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private saveStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;",
            ">;"
        }
    .end annotation
.end field

.field private final servicesCustomization:Lcom/squareup/appointmentsapi/ServicesCustomization;

.field private final setupGuideIntegrationRunner:Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

.field private final subscriptions:Lrx/subscriptions/CompositeSubscription;

.field private tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private final unsupportedItemOptionActionDialogRunner:Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/EditItemState;Lcom/squareup/cogs/Cogs;Lcom/squareup/catalog/CatalogServiceEndpoint;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/analytics/Analytics;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/util/BrowserLauncher;Ljavax/inject/Provider;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/appointmentsapi/ServicesCustomization;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/inventory/AdjustInventoryStarter;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/items/EditVariationRunner;Lcom/squareup/ui/items/EditItemEventLogger;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/setupguide/SetupGuideIntegrationRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditItemState;",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/catalog/CatalogServiceEndpoint;",
            "Lcom/squareup/util/Res;",
            "Lflow/Flow;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            "Lcom/squareup/util/BrowserLauncher;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            "Lcom/squareup/appointmentsapi/ServicesCustomization;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/ui/inventory/AdjustInventoryStarter;",
            "Lcom/squareup/ui/items/EditInventoryStateController;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/ui/items/EditVariationRunner;",
            "Lcom/squareup/ui/items/EditItemEventLogger;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            "Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    new-instance v1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    .line 162
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 171
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    move-object v1, p1

    .line 197
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    move-object v1, p2

    .line 198
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    move-object v1, p3

    .line 199
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->catalogServiceEndpoint:Lcom/squareup/catalog/CatalogServiceEndpoint;

    move-object v1, p4

    .line 200
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->res:Lcom/squareup/util/Res;

    move-object v1, p5

    .line 201
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->flow:Lflow/Flow;

    move-object v1, p7

    .line 202
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    move-object v1, p8

    .line 203
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p9

    .line 204
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    move-object v1, p10

    .line 205
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    move-object v1, p12

    .line 206
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    move-object/from16 v1, p14

    .line 207
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object v1, p13

    .line 208
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->servicesCustomization:Lcom/squareup/appointmentsapi/ServicesCustomization;

    move-object/from16 v1, p15

    .line 209
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->adjustInventoryStarter:Lcom/squareup/ui/inventory/AdjustInventoryStarter;

    move-object v1, p6

    .line 210
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v1, p16

    .line 211
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    move-object/from16 v1, p20

    .line 212
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    move-object/from16 v1, p21

    .line 213
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->setupGuideIntegrationRunner:Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    .line 214
    sget-object v1, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->DEFAULT:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    invoke-static {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->saveStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 215
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->durationChangeRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 216
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->bookableChangeRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 217
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->itemLabelEditFinishedRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 218
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->defaultVariationUnitTypeChangeRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 219
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->addSingleVariationWithOptionRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-object/from16 v1, p17

    .line 220
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    move-object v1, p11

    .line 221
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->localeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 222
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->editVariationRunner:Lcom/squareup/ui/items/EditVariationRunner;

    move-object/from16 v1, p19

    .line 223
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->eventLogger:Lcom/squareup/ui/items/EditItemEventLogger;

    move-object/from16 v1, p22

    .line 224
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v1, p23

    .line 225
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    move-object/from16 v1, p24

    .line 226
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->unsupportedItemOptionActionDialogRunner:Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;

    return-void
.end method

.method private assertNoExistingVariationRemoved(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;",
            ">;)V"
        }
    .end annotation

    .line 1098
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 1099
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 1100
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1102
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 1103
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    .line 1104
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1105
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " is removed from the updated list."

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    return-void
.end method

.method private checkAndGetAppliedLocationCount(Lcom/squareup/catalog/online/CatalogAppliedLocationCount;)I
    .locals 3

    .line 531
    instance-of v0, p1, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;

    if-eqz v0, :cond_0

    .line 537
    check-cast p1, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;

    invoke-virtual {p1}, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->getAppliedLocationCount()I

    move-result p1

    return p1

    .line 532
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The applied location count must be fetched here, but got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private createItemAndVariationsListener()Lrx/functions/Action1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/Action1<",
            "Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;",
            ">;"
        }
    .end annotation

    .line 957
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$LrxO_jFOzE8OFC9rpMnn_y6S0zo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$LrxO_jFOzE8OFC9rpMnn_y6S0zo;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;)V

    return-object v0
.end method

.method private createNewItem()Lrx/Single;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Single<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;"
        }
    .end annotation

    .line 323
    new-instance v4, Lcom/squareup/ui/items/EditItemState$TaxStates;

    invoke-direct {v4}, Lcom/squareup/ui/items/EditItemState$TaxStates;-><init>()V

    .line 324
    new-instance v5, Lcom/squareup/ui/items/EditItemState$ModifierStates;

    invoke-direct {v5}, Lcom/squareup/ui/items/EditItemState$ModifierStates;-><init>()V

    .line 325
    new-instance v2, Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemScope;->type:Lcom/squareup/api/items/Item$Type;

    invoke-direct {v2, v0}, Lcom/squareup/ui/items/EditItemState$ItemData;-><init>(Lcom/squareup/api/items/Item$Type;)V

    .line 327
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-wide v0, v0, Lcom/squareup/ui/items/EditItemScope;->itemAmount:J

    const-wide/16 v6, 0x0

    cmp-long v3, v0, v6

    if-lez v3, :cond_0

    .line 331
    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-wide v8, v3, Lcom/squareup/ui/items/EditItemScope;->itemAmount:J

    .line 332
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v8, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v1, v3, v8}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 331
    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setPrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemScope;->type:Lcom/squareup/api/items/Item$Type;

    sget-object v1, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    if-ne v0, v1, :cond_2

    .line 336
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-wide v0, v0, Lcom/squareup/ui/items/EditItemScope;->itemAmount:J

    cmp-long v3, v0, v6

    if-nez v3, :cond_1

    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->isUnitPricedServiceCreationEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 338
    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/protos/common/Money;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v6, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v1, v3, v6}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setPrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 341
    :cond_1
    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    const-wide/32 v6, 0x1b7740

    invoke-virtual {v0, v6, v7}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setDuration(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 343
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v0}, Lcom/squareup/catalogapi/CatalogIntegrationController;->hasExtraServiceOptions()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 344
    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setAvailableOnOnlineBookingSite(Z)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 347
    :cond_2
    iget-object v0, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getId()Ljava/lang/String;

    move-result-object v3

    .line 348
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemScope;->categoryId:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemScope;->categoryName:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lcom/squareup/ui/items/EditItemState$ItemData;->setItemCategory(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    iget-object v0, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/widgets/pos/R$color;->edit_item_gray:I

    invoke-interface {v1, v6}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    invoke-static {v1}, Lcom/squareup/util/Colors;->toHex(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->setColor(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    .line 357
    iget-object v0, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemScope;->itemName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    .line 358
    iget-object v0, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    if-ne v0, v1, :cond_3

    .line 359
    iget-object v0, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemScope;->itemAbbreviation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->setAbbreviation(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    .line 361
    :cond_3
    iget-object v0, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 362
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/checkout/R$string;->item_variation_default_name_regular:I

    .line 363
    invoke-interface {v1, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 362
    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 364
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemScope;->sku:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 365
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemScope;->sku:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setSku(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 367
    :cond_4
    invoke-virtual {v4, v3}, Lcom/squareup/ui/items/EditItemState$TaxStates;->reset(Ljava/lang/String;)V

    .line 368
    invoke-virtual {v5, v3}, Lcom/squareup/ui/items/EditItemState$ModifierStates;->reset(Ljava/lang/String;)V

    .line 370
    new-instance v6, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$nD1wPojN5oOwAyC8S6Teax3Cw94;

    move-object v0, v6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$nD1wPojN5oOwAyC8S6Teax3Cw94;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/ui/items/EditItemState$ItemData;Ljava/lang/String;Lcom/squareup/ui/items/EditItemState$TaxStates;Lcom/squareup/ui/items/EditItemState$ModifierStates;)V

    .line 403
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-interface {v0, v6}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object v0

    return-object v0
.end method

.method private goToInventoryAssignmentSaveFailedDialog()V
    .locals 3

    .line 929
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/AssignInventoryErrorDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/items/AssignInventoryErrorDialogScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method private goToItemSaveFailedDialog()V
    .locals 5

    .line 923
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    new-instance v2, Lcom/squareup/widgets/warning/WarningIds;

    sget v3, Lcom/squareup/edititem/R$string;->item_editing_save_failed_title:I

    sget v4, Lcom/squareup/edititem/R$string;->item_editing_save_failed_message:I

    invoke-direct {v2, v3, v4}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method private goToRetrieveVariationServerTokensErrorDialogScreen(Z)V
    .locals 3

    .line 934
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v1, v2, p1}, Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;Z)V

    sget-object p1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, p1}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method private isNewItem()Z
    .locals 2

    .line 319
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemScope;->itemId:Ljava/lang/String;

    sget-object v1, Lcom/squareup/ui/items/EditItemScope;->NEW_ITEM:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isUnitPricedServiceCreationEnabled()Z
    .locals 2

    .line 1011
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_RATE_BASED_SERVICES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method static synthetic lambda$editItemMainScreenRefreshNeeded$0(Lcom/squareup/ui/items/EditItemState;)Lkotlin/Unit;
    .locals 0

    .line 300
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method

.method private loadEditItemState()Lrx/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Single<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;"
        }
    .end annotation

    .line 311
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->isNewItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->createNewItem()Lrx/Single;

    move-result-object v0

    return-object v0

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemScope;->itemId:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/items/EditItemScopeRunner;->loadExistingItemFromLibrary(Ljava/lang/String;Z)Lrx/Single;

    move-result-object v0

    return-object v0
.end method

.method private loadExistingItemFromLibrary(Ljava/lang/String;Z)Lrx/Single;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lrx/Single<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;"
        }
    .end annotation

    .line 407
    new-instance v3, Lcom/squareup/ui/items/EditItemState$TaxStates;

    invoke-direct {v3}, Lcom/squareup/ui/items/EditItemState$TaxStates;-><init>()V

    .line 408
    new-instance v4, Lcom/squareup/ui/items/EditItemState$ModifierStates;

    invoke-direct {v4}, Lcom/squareup/ui/items/EditItemState$ModifierStates;-><init>()V

    .line 409
    invoke-virtual {v3, p1}, Lcom/squareup/ui/items/EditItemState$TaxStates;->reset(Ljava/lang/String;)V

    .line 410
    invoke-virtual {v4, p1}, Lcom/squareup/ui/items/EditItemState$ModifierStates;->reset(Ljava/lang/String;)V

    .line 411
    new-instance v6, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$haPumRLUpz-5PVVL5dtSGB79_JI;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$haPumRLUpz-5PVVL5dtSGB79_JI;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;Ljava/lang/String;Lcom/squareup/ui/items/EditItemState$TaxStates;Lcom/squareup/ui/items/EditItemState$ModifierStates;Z)V

    .line 434
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-interface {p1, v6}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method private logEditCatalogObjectEvent(Ljava/lang/String;Z)V
    .locals 4

    .line 994
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/items/EditCatalogObjectEvent;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    .line 995
    invoke-interface {v2}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->getActiveLocationCount()I

    move-result v2

    xor-int/lit8 p2, p2, 0x1

    .line 996
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->isNewItem()Z

    move-result v3

    invoke-direct {v1, v2, p1, p2, v3}, Lcom/squareup/log/items/EditCatalogObjectEvent;-><init>(ILjava/lang/String;ZZ)V

    .line 994
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private logInventoryStockActionForInventoryAssignments(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/InventoryAdjustment;",
            ">;)V"
        }
    .end annotation

    .line 904
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    .line 905
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->getVariationIdsByMerchantCatalogTokens()Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 907
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v1

    .line 908
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/Item;

    iget-object v1, v1, Lcom/squareup/api/items/Item;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v1, v1, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    .line 910
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/InventoryAdjustment;

    .line 911
    iget-object v3, v2, Lcom/squareup/protos/client/InventoryAdjustment;->variation_token:Ljava/lang/String;

    .line 913
    invoke-virtual {v0, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    const-string/jumbo v4, "variation id"

    invoke-static {v3, v4}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 915
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->isNewItem()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    iget-object v4, v4, Lcom/squareup/ui/items/EditItemState;->existingVariationIds:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    goto :goto_2

    :cond_1
    :goto_1
    const/4 v3, 0x1

    .line 917
    :goto_2
    iget-object v4, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    .line 918
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->isNewItem()Z

    move-result v5

    invoke-static {v2, v1, v5, v3}, Lcom/squareup/log/inventory/InventoryStockActionEvent;->assignInventoryStockCount(Lcom/squareup/protos/client/InventoryAdjustment;Ljava/lang/String;ZZ)Lcom/squareup/log/inventory/InventoryStockActionEvent;

    move-result-object v2

    .line 917
    invoke-interface {v4, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private saveItemOptionsAndValues(Lcom/squareup/ui/items/EditItemScopeRunner$NextSaveStep;)V
    .locals 6

    .line 541
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->findChangedItemOptionsAndValues()Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;

    move-result-object v0

    .line 542
    invoke-virtual {v0}, Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;->toBytes()[B

    move-result-object v1

    invoke-static {v1}, Ljava/util/UUID;->nameUUIDFromBytes([B)Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 543
    invoke-virtual {v0}, Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;->listOfAllChangedItemOptions()Ljava/util/List;

    move-result-object v2

    .line 544
    invoke-virtual {v0}, Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 545
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->saveStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v4, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->SAVE_IN_PROGRESS:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    invoke-virtual {v3, v4}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 546
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v4, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v5, Lcom/squareup/ui/items/EditItemScopeRunner$1;

    invoke-direct {v5, p0, v2, v1}, Lcom/squareup/ui/items/EditItemScopeRunner$1;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Lcom/squareup/cogs/Cogs;->asSingleOnlineThenLocal(Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;)Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$ndH_LpX1b_pnfx6A1atRsWSMfcE;

    invoke-direct {v2, p0, v0, p1}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$ndH_LpX1b_pnfx6A1atRsWSMfcE;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;Lcom/squareup/ui/items/EditItemScopeRunner$NextSaveStep;)V

    .line 562
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 546
    invoke-virtual {v3, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    goto :goto_0

    .line 589
    :cond_0
    invoke-interface {p1}, Lcom/squareup/ui/items/EditItemScopeRunner$NextSaveStep;->proceed()V

    :goto_0
    return-void
.end method

.method private saveItemVariationListener(Z)Lrx/functions/Action1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lrx/functions/Action1<",
            "Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;",
            ">;"
        }
    .end annotation

    .line 940
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$S9l_23IVATF0o9ljJhkVImbOTVE;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$S9l_23IVATF0o9ljJhkVImbOTVE;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;Z)V

    return-object v0
.end method

.method private saveItemVariationsWithItemV3(Z)V
    .locals 11

    .line 594
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 595
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->saveStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->ERROR:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 596
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->goToItemSaveFailedDialog()V

    return-void

    .line 600
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->saveStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->SAVE_IN_PROGRESS:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 601
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v0

    .line 602
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getId()Ljava/lang/String;

    move-result-object v8

    .line 603
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->hasItemEverUsedItemOptions()Z

    move-result v9

    .line 605
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->assignedItemOptionsHaveChanged()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 606
    iget-object v10, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->catalogServiceEndpoint:Lcom/squareup/catalog/CatalogServiceEndpoint;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    .line 608
    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    .line 609
    invoke-virtual {v3}, Lcom/squareup/ui/items/EditItemState;->getModifiedItemVariations()Lcom/squareup/catalog/EditItemVariationsState;

    move-result-object v3

    .line 611
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->saveItemVariationListener(Z)Lrx/functions/Action1;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    move v4, p1

    move-object v7, v0

    .line 607
    invoke-virtual/range {v1 .. v9}, Lcom/squareup/catalog/CatalogServiceEndpoint;->updateItemOptionsOnItemAndSaveVariations(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLrx/functions/Action1;Lcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/lang/String;Z)Lrx/Subscription;

    move-result-object p1

    .line 606
    invoke-virtual {v10, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    goto :goto_0

    .line 619
    :cond_1
    iget-object v10, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->catalogServiceEndpoint:Lcom/squareup/catalog/CatalogServiceEndpoint;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    .line 621
    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState;->getModifiedItemVariations()Lcom/squareup/catalog/EditItemVariationsState;

    move-result-object v2

    .line 623
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->saveItemVariationListener(Z)Lrx/functions/Action1;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    move v3, p1

    move-object v6, v0

    move-object v7, v8

    move v8, v9

    .line 620
    invoke-virtual/range {v1 .. v8}, Lcom/squareup/catalog/CatalogServiceEndpoint;->saveItemVariations(Lcom/squareup/catalog/EditItemVariationsState;ZLrx/functions/Action1;Lcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/lang/String;Z)Lrx/Subscription;

    move-result-object p1

    .line 619
    invoke-virtual {v10, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    :goto_0
    return-void
.end method

.method private validateItemName()Z
    .locals 1

    .line 987
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 990
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method


# virtual methods
.method public assignedEmployeesFinish()V
    .locals 1

    .line 895
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method bookableChange()V
    .locals 2

    .line 295
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->bookableChangeRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method createStockCountRowAction(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/items/widgets/StockCountRow;ILjava/lang/String;)Lcom/squareup/ui/items/StockCountRowAction;
    .locals 13

    move-object v11, p0

    .line 440
    new-instance v7, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$Ahy6LEH014UJ0LZrAakNLlY4ToY;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move/from16 v4, p4

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$Ahy6LEH014UJ0LZrAakNLlY4ToY;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 452
    new-instance v12, Lcom/squareup/ui/items/StockCountRowAction;

    iget-object v3, v11, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    iget-object v6, v11, Lcom/squareup/ui/items/EditItemScopeRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    iget-object v8, v11, Lcom/squareup/ui/items/EditItemScopeRunner;->res:Lcom/squareup/util/Res;

    iget-object v9, v11, Lcom/squareup/ui/items/EditItemScopeRunner;->localeProvider:Ljavax/inject/Provider;

    move-object v0, v12

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v4, p3

    move-object v5, p0

    move/from16 v10, p4

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/items/StockCountRowAction;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/items/EditItemState;Lcom/squareup/ui/items/widgets/StockCountRow;Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/ui/items/StockCountRowAction$RowClickedCallback;Lcom/squareup/util/Res;Ljavax/inject/Provider;I)V

    return-object v12
.end method

.method defaultVariationUnitTypeChanged()V
    .locals 2

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->defaultVariationUnitTypeChangeRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public deferInitialStockAdjustment(Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 746
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/ui/items/EditInventoryStateController;->updateInventoryAssignment(Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method discardItemUnsavedChanges()V
    .locals 2

    .line 721
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    invoke-virtual {v0}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->discardChanges()V

    .line 722
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CONFIRM_DISCARD_CHANGES:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 723
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->exitEditItemFlow()V

    .line 724
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Item Changes Discarded"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method durationChange()V
    .locals 2

    .line 287
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->durationChangeRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method editItemMainScreenRefreshNeeded()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 300
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemStateLoaded()Lrx/Single;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$yYRIizl-swrir3rwN1WQDoOUkOQ;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$yYRIizl-swrir3rwN1WQDoOUkOQ;

    invoke-virtual {v0, v1}, Lrx/Single;->map(Lrx/functions/Func1;)Lrx/Single;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->durationChangeRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 301
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->asObservable()Lrx/Observable;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->itemLabelEditFinishedRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 302
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->asObservable()Lrx/Observable;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->bookableChangeRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 303
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->asObservable()Lrx/Observable;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editVariationRunner:Lcom/squareup/ui/items/EditVariationRunner;

    .line 304
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationRunner;->variationEditingFinished()Lrx/Observable;

    move-result-object v5

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->defaultVariationUnitTypeChangeRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 305
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->asObservable()Lrx/Observable;

    move-result-object v6

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->addSingleVariationWithOptionRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 306
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->asObservable()Lrx/Observable;

    move-result-object v7

    .line 299
    invoke-static/range {v1 .. v7}, Lrx/Observable;->merge(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method editItemStateLoaded()Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Single<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;"
        }
    .end annotation

    .line 736
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->oneEditItemStateCached:Lrx/Single;

    return-object v0
.end method

.method exitEditItemFlow()V
    .locals 4

    .line 732
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/items/InEditItemScope;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method finishSavingItem()V
    .locals 5

    .line 831
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->saveStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->COMPLETE:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 833
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->isNewItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 834
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemState;->itemId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->createCatalogItem(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)V

    .line 837
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemScope;->type:Lcom/squareup/api/items/Item$Type;

    sget-object v1, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->CREATE_SERVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_ITEMS:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 839
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->setupGuideIntegrationRunner:Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    sget-object v3, Lcom/squareup/ui/main/MainActivityScope;->INSTANCE:Lcom/squareup/ui/main/MainActivityScope;

    new-instance v4, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$_QciSthaQxqImwsim4F2bz9Wzks;

    invoke-direct {v4, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$_QciSthaQxqImwsim4F2bz9Wzks;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;)V

    .line 840
    invoke-interface {v2, v0, v3, v4}, Lcom/squareup/setupguide/SetupGuideIntegrationRunner;->handleCompletion(Lcom/squareup/protos/checklist/common/ActionItemName;Lcom/squareup/ui/main/RegisterTreeKey;Lkotlin/jvm/functions/Function0;)Lio/reactivex/Completable;

    move-result-object v0

    .line 845
    invoke-virtual {v0}, Lio/reactivex/Completable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 839
    invoke-virtual {v1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public getStaffItemState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;",
            ">;"
        }
    .end annotation

    .line 882
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->servicesCustomization:Lcom/squareup/appointmentsapi/ServicesCustomization;

    invoke-interface {v0}, Lcom/squareup/appointmentsapi/ServicesCustomization;->allStaffInfo()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$sDi70olcjYzAzEVrvy3wmpVK83Y;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$sDi70olcjYzAzEVrvy3wmpVK83Y;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;)V

    .line 883
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    .line 882
    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method goToDisallowInventoryApiDialog()V
    .locals 3

    .line 855
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/DisallowInventoryApiDialog;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/items/DisallowInventoryApiDialog;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method gotoAddVariationWithOptionsWorkflow()V
    .locals 11

    .line 458
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 459
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 460
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemOptionData()Lcom/squareup/ui/items/ItemOptionData;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    .line 464
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    .line 466
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v1

    .line 464
    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/ItemOptionData;->findAssignedOptionsForItem(Lcom/squareup/shared/catalog/models/CatalogItem;)Ljava/util/List;

    move-result-object v5

    .line 468
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->hasLoallyDisabledVariation()Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->NO:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 469
    :goto_1
    iget-object v9, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->flow:Lflow/Flow;

    new-instance v10, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    .line 472
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getId()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    .line 473
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    .line 477
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getDeletedVariationCatalogTokens()Ljava/util/List;

    move-result-object v7

    move-object v0, v10

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;)V

    .line 469
    invoke-virtual {v9, v10}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public handle(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;)V
    .locals 4

    .line 1066
    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput$VariationCreated;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 1067
    move-object v0, p1

    check-cast v0, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput$VariationCreated;

    .line 1070
    invoke-virtual {v0}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput$VariationCreated;->getUpdatedVariations()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    .line 1071
    invoke-virtual {v3}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    .line 1070
    invoke-direct {p0, v2, v3}, Lcom/squareup/ui/items/EditItemScopeRunner;->assertNoExistingVariationRemoved(Ljava/util/List;Ljava/util/List;)V

    .line 1072
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput$VariationCreated;->getUpdatedVariations()Ljava/util/List;

    move-result-object v3

    iput-object v3, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    .line 1074
    invoke-virtual {v0}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput$VariationCreated;->getNewOptionValues()Ljava/util/List;

    move-result-object v0

    .line 1075
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    .line 1076
    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState;->getItemOptionData()Lcom/squareup/ui/items/ItemOptionData;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/squareup/ui/items/ItemOptionData;->updateWithNewItemOptionValues(Ljava/util/List;)Lcom/squareup/ui/items/ItemOptionData;

    move-result-object v0

    .line 1077
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v2, v0}, Lcom/squareup/ui/items/EditItemState;->setItemOptionData(Lcom/squareup/ui/items/ItemOptionData;)V

    .line 1078
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditItemState;->setHasLocallyDisabledVariation(Z)V

    .line 1079
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->addSingleVariationWithOptionRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 1080
    :cond_0
    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput$AbortedWithRemoteVariations;

    if-eqz v0, :cond_1

    .line 1081
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditItemState;->setHasLocallyDisabledVariation(Z)V

    goto :goto_0

    .line 1082
    :cond_1
    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput$AddingVariationCanceled;

    if-eqz v0, :cond_2

    .line 1083
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditItemState;->setHasLocallyDisabledVariation(Z)V

    .line 1086
    :cond_2
    :goto_0
    instance-of p1, p1, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput$AbortedWithRemoteVariations;

    if-eqz p1, :cond_3

    .line 1087
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->unsupportedItemOptionActionDialogRunner:Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;

    .line 1088
    invoke-virtual {p1}, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;->showUnsupportedAddVariationWithOptionsAndLocallyDisabledVariationDialog()V

    goto :goto_1

    .line 1090
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditItemMainScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method public handleOutput(Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput;)V
    .locals 8

    .line 1015
    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput$AssignmentUpdated;

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    .line 1016
    move-object v0, p1

    check-cast v0, Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput$AssignmentUpdated;

    .line 1017
    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput$AssignmentUpdated;->getAllItemOptionsByIds()Ljava/util/Map;

    move-result-object v2

    .line 1018
    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput$AssignmentUpdated;->getAllVariations()Ljava/util/List;

    move-result-object v3

    .line 1020
    iget-object v4, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v4}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->removeAllOptions()Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    move-result-object v4

    .line 1021
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1022
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1024
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 1025
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getAllItemOptionValues()Ljava/util/List;

    move-result-object v3

    .line 1024
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/api/items/ItemOptionValueForItemVariation;

    .line 1026
    iget-object v6, v6, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_id:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1029
    :cond_0
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1030
    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    invoke-virtual {v4, v5}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->addOption(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    goto :goto_1

    .line 1033
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1034
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    .line 1035
    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput$AssignmentUpdated;->getAllVariations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 1036
    new-instance v6, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-direct {v6, v5}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1037
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1040
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 1041
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v6

    .line 1042
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 1043
    iget-object v7, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    iget-object v7, v7, Lcom/squareup/ui/items/EditItemState;->pendingDeletions:Ljava/util/Set;

    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v5

    invoke-interface {v7, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1044
    iget-object v5, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    invoke-virtual {v5, v6}, Lcom/squareup/ui/items/EditInventoryStateController;->removeInventoryAssignment(Ljava/lang/String;)V

    goto :goto_3

    .line 1047
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iput-object v3, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    .line 1049
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    new-instance v3, Lcom/squareup/ui/items/ItemOptionData;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/squareup/ui/items/ItemOptionData;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v3}, Lcom/squareup/ui/items/EditItemState;->setItemOptionData(Lcom/squareup/ui/items/ItemOptionData;)V

    .line 1050
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditItemState;->setHasLocallyDisabledVariation(Z)V

    goto :goto_4

    .line 1051
    :cond_5
    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput$AssignmentAbortedWithRemoteVariations;

    if-eqz v0, :cond_6

    .line 1052
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditItemState;->setHasLocallyDisabledVariation(Z)V

    goto :goto_4

    .line 1053
    :cond_6
    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput$AssignmentCanceled;

    if-eqz v0, :cond_7

    .line 1054
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditItemState;->setHasLocallyDisabledVariation(Z)V

    .line 1057
    :cond_7
    :goto_4
    instance-of p1, p1, Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput$AssignmentAbortedWithRemoteVariations;

    if-eqz p1, :cond_8

    .line 1058
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->unsupportedItemOptionActionDialogRunner:Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;

    .line 1059
    invoke-virtual {p1}, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;->showUnsupportedEditOptionWithLocallyDisabledVariationsDialog()V

    goto :goto_5

    .line 1061
    :cond_8
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditItemMainScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_5
    return-void
.end method

.method public isVariablePriceType()Z
    .locals 1

    .line 860
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->isVariablePricing()Z

    move-result v0

    return v0
.end method

.method itemLabelEditFinished()V
    .locals 2

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Edit Item Label Screen Dismissed"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 279
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->itemLabelEditFinishedRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$createItemAndVariationsListener$17$EditItemScopeRunner(Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;)V
    .locals 2

    .line 958
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    .line 959
    iget-boolean v0, p1, Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;->success:Z

    if-eqz v0, :cond_0

    .line 963
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    iget-object v1, p1, Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;->createdCatalogObjectMerchantCatalogReferencesByClientIds:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditInventoryStateController;->updateCreatedVariationIds(Ljava/util/Map;)V

    .line 967
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    iget-object p1, p1, Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;->createdCatalogObjectMerchantCatalogReferencesByClientIds:Ljava/util/Map;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditItemState;->addServerIdAndVersionToItemAndVariations(Ljava/util/Map;)V

    .line 971
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->saveItemAndVariationsUsingCogs()V

    .line 974
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->saveInventoryAssignment()V

    goto :goto_0

    .line 976
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->saveStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->ERROR:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    const/4 p1, 0x1

    .line 981
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->goToRetrieveVariationServerTokensErrorDialogScreen(Z)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$createNewItem$1$EditItemScopeRunner(Lcom/squareup/ui/items/EditItemState$ItemData;Ljava/lang/String;Lcom/squareup/ui/items/EditItemState$TaxStates;Lcom/squareup/ui/items/EditItemState$ModifierStates;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/ui/items/EditItemState;
    .locals 9

    .line 371
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemScope;->type:Lcom/squareup/api/items/Item$Type;

    sget-object v3, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    if-eq v1, v3, :cond_2

    .line 373
    invoke-interface {p5}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllTaxes()Ljava/util/List;

    move-result-object v1

    .line 375
    const-class v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    invoke-interface {p5, v3}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllCatalogConnectV2Objects(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v3

    .line 377
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    .line 378
    iget-object v5, p1, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 383
    :cond_0
    invoke-interface {p5, p2}, Lcom/squareup/shared/catalog/Catalog$Local;->findItemModifierRelations(Ljava/lang/String;)Lcom/squareup/shared/catalog/TypedCursor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/TypedCursor;->toList()Ljava/util/List;

    move-result-object v3

    .line 385
    invoke-interface {p5}, Lcom/squareup/shared/catalog/Catalog$Local;->findAllItemModifiers()Ljava/util/Map;

    move-result-object v4

    .line 387
    invoke-virtual {p3, v1}, Lcom/squareup/ui/items/EditItemState$TaxStates;->loadTaxesForNewItem(Ljava/util/List;)V

    .line 388
    invoke-virtual {p4, v3, v4}, Lcom/squareup/ui/items/EditItemState$ModifierStates;->load(Ljava/util/List;Ljava/util/Map;)V

    .line 389
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogTax;

    .line 390
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogTax;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 391
    iget-object v4, p1, Lcom/squareup/ui/items/EditItemState$ItemData;->allEnabledTaxes:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 395
    :cond_2
    invoke-interface {p5}, Lcom/squareup/shared/catalog/Catalog$Local;->readAppliedServerVersion()J

    move-result-wide v7

    .line 396
    invoke-static {p5}, Lcom/squareup/ui/items/EditItemState$ReadItemOptionDataCogsTask;->readItemOptionDataFromCogs(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/ui/items/ItemOptionData;

    move-result-object v3

    .line 397
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    const/4 v1, 0x0

    const/4 v4, 0x0

    move-object v2, p1

    move-object v5, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v8}, Lcom/squareup/ui/items/EditItemState;->loadDataFromLibrary(ZLcom/squareup/ui/items/EditItemState$ItemData;Lcom/squareup/ui/items/ItemOptionData;Ljava/lang/String;Lcom/squareup/ui/items/EditItemState$TaxStates;Lcom/squareup/ui/items/EditItemState$ModifierStates;J)V

    .line 400
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    return-object v0
.end method

.method public synthetic lambda$createStockCountRowAction$3$EditItemScopeRunner(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZLjava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V
    .locals 10

    move-object v0, p0

    .line 442
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->adjustInventoryStarter:Lcom/squareup/ui/inventory/AdjustInventoryStarter;

    iget-object v2, v0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    move v3, p5

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move v8, p3

    move-object v9, p4

    invoke-interface/range {v1 .. v9}, Lcom/squareup/ui/inventory/AdjustInventoryStarter;->startAdjustInventoryFlow(Lcom/squareup/ui/main/RegisterTreeKey;ZLjava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;ILjava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$finishSavingItem$14$EditItemScopeRunner()Lkotlin/Unit;
    .locals 1

    .line 842
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->exitEditItemFlow()V

    .line 843
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public synthetic lambda$getStaffItemState$15$EditItemScopeRunner(Ljava/util/List;)Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 883
    new-instance v0, Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    .line 884
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getEmployeeTokens()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public synthetic lambda$loadExistingItemFromLibrary$2$EditItemScopeRunner(Ljava/lang/String;Lcom/squareup/ui/items/EditItemState$TaxStates;Lcom/squareup/ui/items/EditItemState$ModifierStates;ZLcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/ui/items/EditItemState;
    .locals 10

    .line 412
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemScope;->type:Lcom/squareup/api/items/Item$Type;

    invoke-static {p5, p1, v0}, Lcom/squareup/ui/items/EditItemState$ReadItemDataCogsTask;->readItemDataFromCogs(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;)Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v3

    .line 414
    invoke-static {p5}, Lcom/squareup/ui/items/EditItemState$ReadItemOptionDataCogsTask;->readItemOptionDataFromCogs(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/ui/items/ItemOptionData;

    move-result-object v4

    .line 418
    invoke-interface {p5, p1}, Lcom/squareup/shared/catalog/Catalog$Local;->findItemTaxRelations(Ljava/lang/String;)Lcom/squareup/shared/catalog/TypedCursor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/TypedCursor;->toList()Ljava/util/List;

    move-result-object v0

    .line 422
    invoke-interface {p5, p1}, Lcom/squareup/shared/catalog/Catalog$Local;->findItemModifierRelations(Ljava/lang/String;)Lcom/squareup/shared/catalog/TypedCursor;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/TypedCursor;->toList()Ljava/util/List;

    move-result-object p1

    .line 424
    invoke-interface {p5}, Lcom/squareup/shared/catalog/Catalog$Local;->findAllItemModifiers()Ljava/util/Map;

    move-result-object v1

    .line 426
    invoke-virtual {p2, v0}, Lcom/squareup/ui/items/EditItemState$TaxStates;->load(Ljava/util/List;)V

    .line 427
    invoke-virtual {p3, p1, v1}, Lcom/squareup/ui/items/EditItemState$ModifierStates;->load(Ljava/util/List;Ljava/util/Map;)V

    .line 428
    invoke-interface {p5}, Lcom/squareup/shared/catalog/Catalog$Local;->readAppliedServerVersion()J

    move-result-wide v8

    .line 429
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v5, p1, Lcom/squareup/ui/items/EditItemScope;->imageUrl:Ljava/lang/String;

    move v2, p4

    move-object v6, p2

    move-object v7, p3

    invoke-virtual/range {v1 .. v9}, Lcom/squareup/ui/items/EditItemState;->loadDataFromLibrary(ZLcom/squareup/ui/items/EditItemState$ItemData;Lcom/squareup/ui/items/ItemOptionData;Ljava/lang/String;Lcom/squareup/ui/items/EditItemState$TaxStates;Lcom/squareup/ui/items/EditItemState$ModifierStates;J)V

    .line 431
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    return-object p1
.end method

.method public synthetic lambda$null$11$EditItemScopeRunner(Lcom/squareup/ui/items/EditItemState;)V
    .locals 0

    .line 816
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->saveInventoryAssignment()V

    return-void
.end method

.method public synthetic lambda$null$12$EditItemScopeRunner(ZLcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 1

    .line 800
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    .line 802
    iget-object p2, p2, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-eqz p2, :cond_0

    .line 803
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->saveStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->ERROR:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    invoke-virtual {p2, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 807
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->goToRetrieveVariationServerTokensErrorDialogScreen(Z)V

    return-void

    .line 812
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object p2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    iget-object p2, p2, Lcom/squareup/ui/items/EditItemState;->itemId:Ljava/lang/String;

    const/4 v0, 0x1

    .line 813
    invoke-direct {p0, p2, v0}, Lcom/squareup/ui/items/EditItemScopeRunner;->loadExistingItemFromLibrary(Ljava/lang/String;Z)Lrx/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$3bwRUJlJ44PP7UVXAst6HmyvDo4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$3bwRUJlJ44PP7UVXAst6HmyvDo4;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;)V

    .line 815
    invoke-virtual {p2, v0}, Lrx/Single;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p2

    .line 812
    invoke-virtual {p1, p2}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method public synthetic lambda$null$8$EditItemScopeRunner(Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 764
    iget-object p1, p1, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->adjustments:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->logInventoryStockActionForInventoryAssignments(Ljava/util/List;)V

    .line 766
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->finishSavingItem()V

    return-void
.end method

.method public synthetic lambda$null$9$EditItemScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 769
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->saveStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->ERROR:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 770
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->goToInventoryAssignmentSaveFailedDialog()V

    return-void
.end method

.method public synthetic lambda$saveChanges$7$EditItemScopeRunner(Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;)V
    .locals 2

    .line 665
    sget-object v0, Lcom/squareup/ui/items/EditItemScopeRunner$2;->$SwitchMap$com$squareup$ui$items$EditItemScopeRunner$VariationSavePath:[I

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 673
    :cond_0
    invoke-virtual {p0, v0, v0}, Lcom/squareup/ui/items/EditItemScopeRunner;->saveChangesUsingCogs(ZZ)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 670
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->saveItemVariationsWithItemV3(Z)V

    goto :goto_0

    .line 667
    :cond_2
    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditItemScopeRunner;->saveItemVariationsWithItemV3(Z)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$saveInventoryAssignment$10$EditItemScopeRunner(Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 761
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$rv9iy8h8SlY748H6sC3GfwnWyCk;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$rv9iy8h8SlY748H6sC3GfwnWyCk;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;)V

    new-instance p1, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$N7c8rbiqO9egGP9f1aLq92sajyQ;

    invoke-direct {p1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$N7c8rbiqO9egGP9f1aLq92sajyQ;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;)V

    invoke-virtual {p2, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$saveItemAndInventoryAssignment$13$EditItemScopeRunner(ZLjava/lang/Boolean;)V
    .locals 3

    .line 786
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 787
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->isNewItem()Z

    move-result p1

    const-string p2, "POS must be creating new item when it initiates edit item flow from items grid"

    invoke-static {p1, p2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 789
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 790
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {p2}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 791
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 793
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->catalogServiceEndpoint:Lcom/squareup/catalog/CatalogServiceEndpoint;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    .line 794
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v1

    .line 795
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->createItemAndVariationsListener()Lrx/functions/Action1;

    move-result-object v2

    .line 793
    invoke-virtual {v0, v1, p1, v2}, Lcom/squareup/catalog/CatalogServiceEndpoint;->createItemAndVariationsViaV3Endpoint(Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/util/List;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    invoke-virtual {p2, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    goto :goto_1

    .line 797
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$NQVyENhgeuM8SK42oNohyrSEMOs;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$NQVyENhgeuM8SK42oNohyrSEMOs;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;Z)V

    invoke-virtual {p2, v0, p1, v1}, Lcom/squareup/ui/items/EditItemState;->saveToCatalogStoreAndSync(Lcom/squareup/cogs/Cogs;ZLcom/squareup/shared/catalog/sync/SyncCallback;)V

    :goto_1
    return-void
.end method

.method public synthetic lambda$saveItemOptionsAndValues$6$EditItemScopeRunner(Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;Lcom/squareup/ui/items/EditItemScopeRunner$NextSaveStep;Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 563
    iget-object v0, p3, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-nez v0, :cond_2

    .line 564
    invoke-virtual {p3}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;

    .line 565
    iget-object v0, p3, Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;->objects:Ljava/util/List;

    .line 566
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 567
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    .line 568
    instance-of v3, v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Object: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, " is supposed to be a CatalogItemOption because it is from a batch upsertion of item options"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 572
    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 573
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 575
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditItemState;->updateItemOptionDataAfterSave(Ljava/util/Map;)V

    .line 576
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 577
    iget-object p3, p3, Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;->idMappings:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;

    .line 578
    iget-object v2, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;->client_object_id:Ljava/lang/String;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;->object_id:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 580
    :cond_1
    iget-object p3, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {p3, v0}, Lcom/squareup/ui/items/EditItemState;->replaceItemOptionClientIdsWithServerIds(Ljava/util/Map;)V

    .line 581
    iget-object p3, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->eventLogger:Lcom/squareup/ui/items/EditItemEventLogger;

    invoke-virtual {p3, p1, v0}, Lcom/squareup/ui/items/EditItemEventLogger;->logItemOptionChanges(Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;Ljava/util/Map;)V

    .line 582
    invoke-interface {p2}, Lcom/squareup/ui/items/EditItemScopeRunner$NextSaveStep;->proceed()V

    goto :goto_2

    .line 584
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->saveStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object p2, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->ERROR:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 585
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->goToItemSaveFailedDialog()V

    :goto_2
    return-void
.end method

.method public synthetic lambda$saveItemVariationListener$16$EditItemScopeRunner(ZLcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;)V
    .locals 1

    .line 941
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    .line 942
    iget-boolean v0, p2, Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;->success:Z

    if-eqz v0, :cond_0

    .line 943
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    iget-object p2, p2, Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;->createdCatalogObjectMerchantCatalogReferencesByClientIds:Ljava/util/Map;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/items/EditInventoryStateController;->updateCreatedVariationIds(Ljava/util/Map;)V

    const/4 p2, 0x0

    .line 945
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/EditItemScopeRunner;->saveChangesUsingCogs(ZZ)V

    goto :goto_0

    .line 947
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->saveStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object p2, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->ERROR:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 948
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->goToItemSaveFailedDialog()V

    .line 951
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->saveStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object p2, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->DEFAULT:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$showConfirmGlobalPriceDialog$5$EditItemScopeRunner(Lcom/squareup/catalog/online/CatalogAppliedLocationCount;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 510
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->checkAndGetAppliedLocationCount(Lcom/squareup/catalog/online/CatalogAppliedLocationCount;)I

    move-result p1

    const/4 v0, 0x2

    if-le p1, v0, :cond_0

    .line 514
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/edititem/R$string;->item_editing_save_will_affect_other_locations_item:I

    .line 515
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    add-int/lit8 p1, p1, -0x1

    const-string v1, "num_locations"

    .line 516
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 517
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 518
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 520
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/edititem/R$string;->item_editing_save_will_affect_other_locations_one_other_item:I

    .line 521
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 525
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/EditItemConfirmGlobalPriceDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v1, v2, p1}, Lcom/squareup/ui/items/EditItemConfirmGlobalPriceDialogScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;Ljava/lang/String;)V

    sget-object p1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, p1}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method public synthetic lambda$showPriceChangedDialog$4$EditItemScopeRunner(Lcom/squareup/catalog/online/CatalogAppliedLocationCount;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 490
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->checkAndGetAppliedLocationCount(Lcom/squareup/catalog/online/CatalogAppliedLocationCount;)I

    move-result p1

    const/4 v0, 0x2

    if-le p1, v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/edititem/R$string;->item_editing_select_location_update_price_item:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    add-int/lit8 p1, p1, -0x1

    const-string v1, "num_locations"

    .line 495
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 496
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 497
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 499
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/edititem/R$string;->item_editing_select_location_update_price_one_other_item:I

    .line 500
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 503
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v1, v2, p1}, Lcom/squareup/ui/items/EditItemPriceChangedDialogScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method maxCancellationFeeMoney()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 899
    sget-object v0, Lcom/squareup/ui/items/CancellationFeePriceHelper;->INSTANCE:Lcom/squareup/ui/items/CancellationFeePriceHelper;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/CancellationFeePriceHelper;->getMaxCancellationFee(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method maybeShowGapTimeEducation()V
    .locals 3

    .line 291
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->servicesCustomization:Lcom/squareup/appointmentsapi/ServicesCustomization;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/squareup/appointmentsapi/ServicesCustomization;->maybeShowGapTimeEducation(Lcom/squareup/ui/main/RegisterTreeKey;Z)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 230
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemScope;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->isNewItem()Z

    move-result v1

    iput-boolean v1, v0, Lcom/squareup/ui/items/EditItemState;->isNewItem:Z

    .line 232
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    .line 233
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 234
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 235
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editVariationRunner:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 237
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->loadEditItemState()Lrx/Single;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Single;->cache()Lrx/Single;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->oneEditItemStateCached:Lrx/Single;

    .line 239
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->oneEditItemStateCached:Lrx/Single;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$20-FKA56YFqTTOv0fgK-kS9WSoI;

    invoke-direct {v2, v1}, Lcom/squareup/ui/items/-$$Lambda$20-FKA56YFqTTOv0fgK-kS9WSoI;-><init>(Lcom/squareup/ui/items/EditInventoryStateController;)V

    invoke-virtual {v0, v2}, Lrx/Single;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 244
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->isNewItem()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    invoke-interface {v0}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->getActiveLocationCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->catalogServiceEndpoint:Lcom/squareup/catalog/CatalogServiceEndpoint;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 247
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemScope;->itemId:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->fetchAppliedLocationCount(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->unsubscribe()V

    .line 253
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    return-void
.end method

.method onUpdateAppClicked()V
    .locals 3

    .line 850
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->google_play_square_pos_url:I

    .line 851
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 850
    invoke-interface {v0, v1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method

.method resumeEditing()V
    .locals 2

    .line 728
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CANCEL_DISCARD_CHANGES:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method saveChanges(Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;)V
    .locals 1

    .line 664
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$eF2WjvUW3eYLIhBMtuLSq5DSdcU;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$eF2WjvUW3eYLIhBMtuLSq5DSdcU;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;)V

    .line 677
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->canEditItemWithItemOptions()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 678
    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditItemScopeRunner;->saveItemOptionsAndValues(Lcom/squareup/ui/items/EditItemScopeRunner$NextSaveStep;)V

    goto :goto_0

    .line 680
    :cond_0
    invoke-interface {v0}, Lcom/squareup/ui/items/EditItemScopeRunner$NextSaveStep;->proceed()V

    :goto_0
    return-void
.end method

.method saveChangesUsingCogs(ZZ)V
    .locals 3

    .line 687
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    if-nez v0, :cond_0

    .line 688
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->saveStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object p2, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->COMPLETE:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 689
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->exitEditItemFlow()V

    return-void

    .line 693
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->validateItemName()Z

    move-result v0

    if-nez v0, :cond_1

    .line 694
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->flow:Lflow/Flow;

    new-instance p2, Lcom/squareup/register/widgets/WarningDialogScreen;

    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/edititem/R$string;->edit_item_name_required_dialog_title:I

    sget v2, Lcom/squareup/edititem/R$string;->edit_item_name_required_dialog_message:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-direct {p2, v0}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 699
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->isNewItem()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 700
    sget-object v0, Lcom/squareup/catalog/event/CatalogFeature;->ITEM_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 701
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getMerchantToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/adanalytics/AdAnalytics;->recordCreateItem(Ljava/lang/String;)V

    goto :goto_0

    .line 703
    :cond_2
    sget-object v0, Lcom/squareup/catalog/event/CatalogFeature;->ITEM_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 706
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->eventLogger:Lcom/squareup/ui/items/EditItemEventLogger;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getModifiedItemVariations()Lcom/squareup/catalog/EditItemVariationsState;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    .line 707
    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    .line 706
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/items/EditItemEventLogger;->logVariationChanges(Lcom/squareup/catalog/EditItemVariationsState;Ljava/util/Map;)V

    .line 709
    sget-object v0, Lsquareup/items/merchant/CatalogObjectType;->ITEM:Lsquareup/items/merchant/CatalogObjectType;

    invoke-virtual {v0}, Lsquareup/items/merchant/CatalogObjectType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->logEditCatalogObjectEvent(Ljava/lang/String;Z)V

    .line 711
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditInventoryStateController;->hasPendingInventoryAssignments()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 712
    invoke-virtual {p0, p2}, Lcom/squareup/ui/items/EditItemScopeRunner;->saveItemAndInventoryAssignment(Z)V

    goto :goto_1

    .line 714
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    .line 715
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object v1

    .line 714
    invoke-virtual {p1, v0, p2, v1}, Lcom/squareup/ui/items/EditItemState;->saveToCatalogStoreAndSync(Lcom/squareup/cogs/Cogs;ZLcom/squareup/shared/catalog/sync/SyncCallback;)V

    .line 716
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->finishSavingItem()V

    :goto_1
    return-void
.end method

.method saveInventoryAssignment()V
    .locals 4

    .line 751
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    .line 752
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->getVariationMerchantCatalogTokensByIds()Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 754
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    iget-wide v2, v2, Lcom/squareup/ui/items/EditItemState;->catalogVersion:J

    .line 755
    invoke-virtual {v1, v0, v2, v3}, Lcom/squareup/ui/items/EditInventoryStateController;->buildBatchAdjustRequestForInventoryAssignments(Ljava/util/Map;J)Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    move-result-object v0

    .line 759
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    .line 760
    invoke-virtual {v2, v0}, Lcom/squareup/ui/items/EditInventoryStateController;->saveInventoryAssignments(Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;)Lio/reactivex/Single;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$YxRa9C5JlMDz7nq9Md8DS6etGjU;

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$YxRa9C5JlMDz7nq9Md8DS6etGjU;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;)V

    .line 761
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 759
    invoke-virtual {v1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method saveItemAndInventoryAssignment(Z)V
    .locals 3

    .line 777
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->saveStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->SAVE_IN_PROGRESS:Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 779
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    invoke-virtual {v1}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->isSelectingItem()Lrx/Observable;

    move-result-object v1

    .line 780
    invoke-virtual {v1}, Lrx/Observable;->first()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$qi9lu5hmB9KhAPyzXr6xunYl26k;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$qi9lu5hmB9KhAPyzXr6xunYl26k;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;Z)V

    .line 781
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 779
    invoke-virtual {v0, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method saveItemAndVariationsUsingCogs()V
    .locals 4

    .line 825
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Lcom/squareup/ui/items/EditItemState;->saveToCatalogStoreAndSync(Lcom/squareup/cogs/Cogs;ZLcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method saveState()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;",
            ">;"
        }
    .end annotation

    .line 740
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->saveStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->asObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public servicePriceTypeFinish()V
    .locals 1

    .line 878
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public setEmployeeAssigned(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 889
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    .line 890
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 891
    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setEmployeeTokens(Ljava/util/List;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    return-void
.end method

.method public setVariablePriceType(Z)V
    .locals 3

    if-eqz p1, :cond_0

    .line 866
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/ui/items/PriceTypeActionEvent;

    sget-object v1, Lcom/squareup/ui/items/PriceTypeActionOption;->VARIABLE:Lcom/squareup/ui/items/PriceTypeActionOption;

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/PriceTypeActionEvent;-><init>(Lcom/squareup/ui/items/PriceTypeActionOption;)V

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 867
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->clearPrice()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    goto :goto_0

    .line 869
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/ui/items/PriceTypeActionEvent;

    sget-object v1, Lcom/squareup/ui/items/PriceTypeActionOption;->FIXED:Lcom/squareup/ui/items/PriceTypeActionOption;

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/PriceTypeActionEvent;-><init>(Lcom/squareup/ui/items/PriceTypeActionOption;)V

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 870
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/protos/common/Money;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setPrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 871
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setPriceDescription(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 873
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemScopeRunner;->servicePriceTypeFinish()V

    return-void
.end method

.method public shouldDeferInitialStockAdjustment()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method showConfirmGlobalPriceDialog()V
    .locals 3

    .line 508
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    invoke-interface {v1}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->getAppliedLocationCount()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$42mKtQdC6edKGwr6Q1FWEViHqsA;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$42mKtQdC6edKGwr6Q1FWEViHqsA;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;)V

    .line 509
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 508
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method showPriceChangedDialog()V
    .locals 3

    .line 488
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    invoke-interface {v1}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->getAppliedLocationCount()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$OXcSsExy7UnyFpTdhuFOQOflWNk;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$OXcSsExy7UnyFpTdhuFOQOflWNk;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;)V

    .line 489
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 488
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method showTooManyVariationsErrorDialog()V
    .locals 3

    .line 484
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/EditItemTooManyVariationsErrorDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-direct {v1, v2}, Lcom/squareup/ui/items/EditItemTooManyVariationsErrorDialogScreen;-><init>(Lcom/squareup/util/Res;)V

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method updateActionBar(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/items/InEditItemScope;)V
    .locals 2

    .line 259
    instance-of v0, p2, Lcom/squareup/ui/items/EditItemMainScreen;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/items/InEditItemScope;->isPrimaryButtonEnabled(Lcom/squareup/ui/items/EditItemState;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    goto :goto_0

    .line 262
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->hidePrimaryButton()V

    .line 265
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v1, v0}, Lcom/squareup/ui/items/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 266
    invoke-virtual {p2}, Lcom/squareup/ui/items/InEditItemScope;->getUpButton()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-virtual {p2, v1}, Lcom/squareup/ui/items/InEditItemScope;->getUpButtonText(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 268
    invoke-virtual {p2}, Lcom/squareup/ui/items/InEditItemScope;->getSecondaryButton()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 270
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-virtual {p2, v1}, Lcom/squareup/ui/items/InEditItemScope;->getToolTipText(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object p2

    .line 271
    invoke-virtual {p1, v0, p2}, Lcom/squareup/marin/widgets/MarinActionBar;->setSecondaryButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 273
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->hideSecondaryButton()V

    :goto_1
    return-void
.end method

.method public updateInventoryStockCountAfterAdjustment(Ljava/lang/String;Lcom/squareup/protos/client/InventoryAdjustmentReason;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)V
    .locals 0

    .line 1002
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemScopeRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    invoke-virtual {p2, p1, p4}, Lcom/squareup/ui/items/EditInventoryStateController;->updateInventoryStockCountAfterAdjustment(Ljava/lang/String;Ljava/math/BigDecimal;)V

    return-void
.end method
