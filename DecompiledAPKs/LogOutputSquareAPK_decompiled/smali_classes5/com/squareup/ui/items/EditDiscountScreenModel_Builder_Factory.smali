.class public final Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;
.super Ljava/lang/Object;
.source "EditDiscountScreenModel_Builder_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private final localizerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/i18n/Localizer;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final shortDateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final timeFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/i18n/Localizer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;->shortDateFormatProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;->timeFormatProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;->localizerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/i18n/Localizer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;"
        }
    .end annotation

    .line 50
    new-instance v6, Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/shared/i18n/Localizer;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;"
        }
    .end annotation

    .line 55
    new-instance v6, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;-><init>(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/util/Res;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;
    .locals 5

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;->shortDateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;->timeFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/text/DateFormat;

    iget-object v3, p0, Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;->localizerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/i18n/Localizer;

    iget-object v4, p0, Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;->newInstance(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreenModel_Builder_Factory;->get()Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;

    move-result-object v0

    return-object v0
.end method
