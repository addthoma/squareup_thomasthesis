.class public final Lcom/squareup/ui/items/EditItemMainView_MembersInjector;
.super Ljava/lang/Object;
.source "EditItemMainView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/items/EditItemMainView;",
        ">;"
    }
.end annotation


# instance fields
.field private final durationFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final errorsBarPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemMainPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemMainPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;->errorsBarPresenterProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;->durationFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemMainPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/items/EditItemMainView;",
            ">;"
        }
    .end annotation

    .line 47
    new-instance v6, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static injectDurationFormatter(Lcom/squareup/ui/items/EditItemMainView;Lcom/squareup/text/DurationFormatter;)V
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainView;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    return-void
.end method

.method public static injectErrorsBarPresenter(Lcom/squareup/ui/items/EditItemMainView;Lcom/squareup/ui/ErrorsBarPresenter;)V
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainView;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    return-void
.end method

.method public static injectMoneyFormatter(Lcom/squareup/ui/items/EditItemMainView;Lcom/squareup/quantity/PerUnitFormatter;)V
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainView;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/items/EditItemMainView;Ljava/lang/Object;)V
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/ui/items/EditItemMainPresenter;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/items/EditItemMainView;Lcom/squareup/util/Res;)V
    .locals 0

    .line 77
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainView;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/items/EditItemMainView;)V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;->injectPresenter(Lcom/squareup/ui/items/EditItemMainView;Ljava/lang/Object;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;->injectMoneyFormatter(Lcom/squareup/ui/items/EditItemMainView;Lcom/squareup/quantity/PerUnitFormatter;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;->errorsBarPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;->injectErrorsBarPresenter(Lcom/squareup/ui/items/EditItemMainView;Lcom/squareup/ui/ErrorsBarPresenter;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;->injectRes(Lcom/squareup/ui/items/EditItemMainView;Lcom/squareup/util/Res;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;->durationFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/DurationFormatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;->injectDurationFormatter(Lcom/squareup/ui/items/EditItemMainView;Lcom/squareup/text/DurationFormatter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/ui/items/EditItemMainView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemMainView_MembersInjector;->injectMembers(Lcom/squareup/ui/items/EditItemMainView;)V

    return-void
.end method
