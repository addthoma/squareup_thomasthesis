.class final Lcom/squareup/ui/items/EditItemState$ModifierState;
.super Ljava/lang/Object;
.source "EditItemState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ModifierState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditItemState$ModifierState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field enabled:Z

.field final id:Ljava/lang/String;

.field final membership:[B

.field final name:Ljava/lang/String;

.field final options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private originallyEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1215
    new-instance v0, Lcom/squareup/ui/items/EditItemState$ModifierState$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/EditItemState$ModifierState$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/EditItemState$ModifierState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;[BZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;[BZZ)V"
        }
    .end annotation

    .line 1196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1197
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->id:Ljava/lang/String;

    .line 1198
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->name:Ljava/lang/String;

    .line 1199
    iput-boolean p5, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->enabled:Z

    .line 1200
    iput-boolean p6, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->originallyEnabled:Z

    .line 1201
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->membership:[B

    .line 1202
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->options:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;[BZZLcom/squareup/ui/items/EditItemState$1;)V
    .locals 0

    .line 1176
    invoke-direct/range {p0 .. p6}, Lcom/squareup/ui/items/EditItemState$ModifierState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;[BZZ)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Z[BLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z[B",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1186
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->id:Ljava/lang/String;

    .line 1187
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->name:Ljava/lang/String;

    .line 1188
    iput-boolean p3, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->enabled:Z

    .line 1189
    iput-boolean p3, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->originallyEnabled:Z

    .line 1190
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->membership:[B

    .line 1191
    iput-object p5, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->options:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Z[BLjava/util/List;Lcom/squareup/ui/items/EditItemState$1;)V
    .locals 0

    .line 1176
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/items/EditItemState$ModifierState;-><init>(Ljava/lang/String;Ljava/lang/String;Z[BLjava/util/List;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/squareup/ui/items/EditItemState$ModifierState;)Z
    .locals 0

    .line 1176
    iget-boolean p0, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->originallyEnabled:Z

    return p0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getOptionsCommaSeparatedList(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 1207
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->options:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const-string p1, ""

    return-object p1

    .line 1209
    :cond_0
    sget v0, Lcom/squareup/utilities/R$string;->list_pattern_short:I

    .line 1210
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/ListPhrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->options:Ljava/util/List;

    .line 1211
    invoke-virtual {p1, v0}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 1212
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1236
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->id:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1237
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->name:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1238
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->options:Ljava/util/List;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1239
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->membership:[B

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 1240
    iget-boolean p2, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->enabled:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    .line 1241
    iget-boolean p2, p0, Lcom/squareup/ui/items/EditItemState$ModifierState;->originallyEnabled:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    return-void
.end method
