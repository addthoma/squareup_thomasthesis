.class public final Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory$Companion;
.super Ljava/lang/Object;
.source "DetailSearchableListViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00050\u0004*\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory$Companion;",
        "",
        "()V",
        "getSection",
        "Ljava/lang/Class;",
        "Lcom/squareup/ui/items/ItemsAppletSection;",
        "Lcom/squareup/ui/items/DetailSearchableListDataType;",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 92
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getSection(Lcom/squareup/ui/items/DetailSearchableListDataType;)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListDataType;",
            ")",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/items/ItemsAppletSection;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$getSection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListDataType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 99
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    const-class p1, Lcom/squareup/ui/items/ItemsAppletSection$Units;

    goto :goto_0

    .line 98
    :pswitch_1
    const-class p1, Lcom/squareup/ui/items/ItemsAppletSection$Modifiers;

    goto :goto_0

    .line 97
    :pswitch_2
    const-class p1, Lcom/squareup/ui/items/ItemsAppletSection$Discounts;

    goto :goto_0

    .line 96
    :pswitch_3
    const-class p1, Lcom/squareup/ui/items/ItemsAppletSection$Categories;

    goto :goto_0

    .line 95
    :pswitch_4
    const-class p1, Lcom/squareup/ui/items/ItemsAppletSection$AllServices;

    goto :goto_0

    .line 94
    :pswitch_5
    const-class p1, Lcom/squareup/ui/items/ItemsAppletSection$AllItems;

    :goto_0
    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
