.class public abstract Lcom/squareup/ui/items/AllItemsDetailScreen$Module;
.super Ljava/lang/Object;
.source "AllItemsDetailScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/AllItemsDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "Module"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/AllItemsDetailScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/AllItemsDetailScreen;)V
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/ui/items/AllItemsDetailScreen$Module;->this$0:Lcom/squareup/ui/items/AllItemsDetailScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideAllElementDetailPresenter(Lcom/squareup/ui/items/AllItemsDetailScreen$AllItemsDetailPresenter;)Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
