.class public Lcom/squareup/ui/items/ConfirmDiscardItemChangesDialogScreen;
.super Lcom/squareup/ui/items/InEditItemScope;
.source "ConfirmDiscardItemChangesDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/items/ConfirmDiscardItemChangesDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/ConfirmDiscardItemChangesDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/ConfirmDiscardItemChangesDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$ConfirmDiscardItemChangesDialogScreen$Vtr2b-QZYs2Flpz2YhLCWC5MbjU;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$ConfirmDiscardItemChangesDialogScreen$Vtr2b-QZYs2Flpz2YhLCWC5MbjU;

    .line 38
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/ConfirmDiscardItemChangesDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/items/EditItemScope;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/InEditItemScope;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/ConfirmDiscardItemChangesDialogScreen;
    .locals 1

    .line 39
    const-class v0, Lcom/squareup/ui/items/EditItemScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/EditItemScope;

    .line 40
    new-instance v0, Lcom/squareup/ui/items/ConfirmDiscardItemChangesDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/ConfirmDiscardItemChangesDialogScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/items/ConfirmDiscardItemChangesDialogScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
