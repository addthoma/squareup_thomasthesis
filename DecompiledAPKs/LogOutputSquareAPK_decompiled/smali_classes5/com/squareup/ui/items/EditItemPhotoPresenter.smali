.class final Lcom/squareup/ui/items/EditItemPhotoPresenter;
.super Lmortar/ViewPresenter;
.source "EditItemPhotoPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/items/EditItemPhotoView;",
        ">;"
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final fileThreadExecutor:Ljava/util/concurrent/Executor;

.field private final flow:Lflow/Flow;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

.field private screen:Lcom/squareup/ui/items/EditItemPhotoScreen;

.field private final state:Lcom/squareup/ui/items/EditItemState;

.field private final tempPhotoDir:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemState;Ljava/io/File;Lcom/squareup/thread/executor/MainThread;Ljava/util/concurrent/Executor;Lcom/squareup/ui/items/EditItemScopeRunner;Lflow/Flow;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 39
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->tempPhotoDir:Ljava/io/File;

    .line 40
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 41
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    .line 42
    iput-object p5, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    .line 43
    iput-object p6, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->flow:Lflow/Flow;

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/ui/items/EditItemPhotoView;)V
    .locals 1

    const/4 v0, 0x0

    .line 82
    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/EditItemPhotoView;->photoSaved(Z)V

    return-void
.end method


# virtual methods
.method bitmapFailed()V
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    return-void
.end method

.method bitmapLoaded()V
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    return-void
.end method

.method cancelClicked()V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method contentMeasured(Lcom/squareup/ui/items/EditItemPhotoView;)V
    .locals 2

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState;->newImageToEdit:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditItemPhotoView;->setContent(Landroid/net/Uri;)V

    return-void
.end method

.method public synthetic lambda$null$1$EditItemPhotoPresenter(Lcom/squareup/ui/items/EditItemPhotoView;Ljava/io/File;)V
    .locals 1

    const/4 v0, 0x1

    .line 87
    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditItemPhotoView;->photoSaved(Z)V

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/items/EditItemState;->setBitmapFile(Ljava/io/File;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public synthetic lambda$onBackPressed$2$EditItemPhotoPresenter(Ljava/lang/String;Landroid/graphics/Bitmap;Lcom/squareup/ui/items/EditItemPhotoView;)V
    .locals 2

    const/4 v0, 0x0

    .line 79
    :try_start_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->tempPhotoDir:Ljava/io/File;

    invoke-static {p1, v0, v1}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object p1

    .line 80
    invoke-static {p2, p1}, Lcom/squareup/util/Images;->writeToFile(Landroid/graphics/Bitmap;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditItemPhotoPresenter$z6AsS2GI9RxHn0UoNi_Vww9C9DQ;

    invoke-direct {v0, p0, p3, p1}, Lcom/squareup/ui/items/-$$Lambda$EditItemPhotoPresenter$z6AsS2GI9RxHn0UoNi_Vww9C9DQ;-><init>(Lcom/squareup/ui/items/EditItemPhotoPresenter;Lcom/squareup/ui/items/EditItemPhotoView;Ljava/io/File;)V

    invoke-interface {p2, v0}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void

    :catch_0
    move-exception p1

    .line 82
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditItemPhotoPresenter$ZZoCWrYTpIy08UcpT2G_C_HbrRw;

    invoke-direct {v0, p3}, Lcom/squareup/ui/items/-$$Lambda$EditItemPhotoPresenter$ZZoCWrYTpIy08UcpT2G_C_HbrRw;-><init>(Lcom/squareup/ui/items/EditItemPhotoView;)V

    invoke-interface {p2, v0}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    const-string p2, "Failed to create bitmap file for item upload!"

    .line 83
    invoke-static {p1, p2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    return-void
.end method

.method onBackPressed(Lcom/squareup/ui/items/EditItemPhotoView;Landroid/graphics/Bitmap;)Z
    .locals 3

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState;->itemId:Ljava/lang/String;

    .line 76
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditItemPhotoPresenter$GnZpVxeO4BI6KyukGpC3g_5rmDs;

    invoke-direct {v2, p0, v0, p2, p1}, Lcom/squareup/ui/items/-$$Lambda$EditItemPhotoPresenter$GnZpVxeO4BI6KyukGpC3g_5rmDs;-><init>(Lcom/squareup/ui/items/EditItemPhotoPresenter;Ljava/lang/String;Landroid/graphics/Bitmap;Lcom/squareup/ui/items/EditItemPhotoView;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    const/4 p1, 0x1

    return p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 47
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemPhotoScreen;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->screen:Lcom/squareup/ui/items/EditItemPhotoScreen;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 51
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemPhotoPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemPhotoView;

    .line 52
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->screen:Lcom/squareup/ui/items/EditItemPhotoScreen;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/items/EditItemScopeRunner;->updateActionBar(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/items/InEditItemScope;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$e8XgDpxkp50H6K8PjUeAHtg9oaY;

    invoke-direct {v1, p1}, Lcom/squareup/ui/items/-$$Lambda$e8XgDpxkp50H6K8PjUeAHtg9oaY;-><init>(Lcom/squareup/ui/items/EditItemPhotoView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    return-void
.end method
