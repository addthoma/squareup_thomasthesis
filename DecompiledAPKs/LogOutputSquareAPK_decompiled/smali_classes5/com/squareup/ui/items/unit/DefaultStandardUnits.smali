.class public final Lcom/squareup/ui/items/unit/DefaultStandardUnits;
.super Ljava/lang/Object;
.source "DefaultStandardUnits.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u000c\u001a\u00020\rR\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u0010\u0012\u000c\u0012\n \n*\u0004\u0018\u00010\u00070\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/items/unit/DefaultStandardUnits;",
        "",
        "locale",
        "Ljava/util/Locale;",
        "(Ljava/util/Locale;)V",
        "itemUnits",
        "",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
        "serviceUnits",
        "sharedItemUnits",
        "kotlin.jvm.PlatformType",
        "units",
        "itemType",
        "Lcom/squareup/ui/items/unit/ItemType;",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final itemUnits:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final serviceUnits:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final sharedItemUnits:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Locale;)V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v1, v0, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 28
    new-instance v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v3, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->METRIC_LITER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 29
    new-instance v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_HOUR:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v2, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->time_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    .line 27
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/items/unit/DefaultStandardUnits;->sharedItemUnits:Ljava/util/List;

    .line 32
    iget-object v1, p0, Lcom/squareup/ui/items/unit/DefaultStandardUnits;->sharedItemUnits:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 33
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const/4 v5, 0x3

    if-eqz v2, :cond_0

    new-array p1, v5, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 34
    new-instance v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v6, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_POUND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v2, v6}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v2

    aput-object v2, p1, v3

    .line 35
    new-instance v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v6, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_WEIGHT_OUNCE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v2, v6}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v2

    aput-object v2, p1, v4

    .line 36
    new-instance v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v6, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v2, v6}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->length_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v2

    aput-object v2, p1, v0

    .line 33
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto/16 :goto_1

    .line 38
    :cond_0
    sget-object v2, Ljava/util/Locale;->CANADA:Ljava/util/Locale;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    sget-object v2, Ljava/util/Locale;->CANADA_FRENCH:Ljava/util/Locale;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    :goto_0
    new-array p1, v5, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 39
    new-instance v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v6, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_GRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v2, v6}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v2

    aput-object v2, p1, v3

    .line 40
    new-instance v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v6, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_KILOGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v2, v6}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v2

    aput-object v2, p1, v4

    .line 41
    new-instance v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v6, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v2, v6}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->length_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v2

    aput-object v2, p1, v0

    .line 38
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_1

    :cond_2
    new-array p1, v5, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 44
    new-instance v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v6, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_GRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v2, v6}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v2

    aput-object v2, p1, v3

    .line 45
    new-instance v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v6, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_KILOGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v2, v6}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v2

    aput-object v2, p1, v4

    .line 46
    new-instance v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v6, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_METER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v2, v6}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->length_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v2

    aput-object v2, p1, v0

    .line 43
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 32
    :goto_1
    check-cast p1, Ljava/lang/Iterable;

    invoke-static {v1, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/unit/DefaultStandardUnits;->itemUnits:Ljava/util/List;

    new-array p1, v5, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 51
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_DAY:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->time_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    aput-object v1, p1, v3

    .line 52
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_HOUR:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->time_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    aput-object v1, p1, v4

    .line 53
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_MINUTE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->time_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    aput-object v1, p1, v0

    .line 50
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/unit/DefaultStandardUnits;->serviceUnits:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final units(Lcom/squareup/ui/items/unit/ItemType;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/unit/ItemType;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
            ">;"
        }
    .end annotation

    const-string v0, "itemType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    sget-object v0, Lcom/squareup/ui/items/unit/DefaultStandardUnits$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/ItemType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 59
    iget-object p1, p0, Lcom/squareup/ui/items/unit/DefaultStandardUnits;->serviceUnits:Ljava/util/List;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 58
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/items/unit/DefaultStandardUnits;->itemUnits:Ljava/util/List;

    :goto_0
    return-object p1
.end method
