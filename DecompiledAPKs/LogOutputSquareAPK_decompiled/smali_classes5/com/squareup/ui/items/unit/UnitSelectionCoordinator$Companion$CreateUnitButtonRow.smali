.class public final Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;
.super Ljava/lang/Object;
.source "UnitSelectionCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateUnitButtonRow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000b\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B#\u0012\u0014\u0010\u0002\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0017\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0007H\u00c6\u0003J+\u0010\u000f\u001a\u00020\u00002\u0016\u0008\u0002\u0010\u0002\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0010\u001a\u00020\u00072\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u001f\u0010\u0002\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;",
        "",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event;",
        "",
        "shouldDisableUnitCreation",
        "",
        "(Lkotlin/jvm/functions/Function1;Z)V",
        "getOnEvent",
        "()Lkotlin/jvm/functions/Function1;",
        "getShouldDisableUnitCreation",
        "()Z",
        "component1",
        "component2",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final onEvent:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final shouldDisableUnitCreation:Z


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event;",
            "Lkotlin/Unit;",
            ">;Z)V"
        }
    .end annotation

    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->onEvent:Lkotlin/jvm/functions/Function1;

    iput-boolean p2, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->shouldDisableUnitCreation:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;Lkotlin/jvm/functions/Function1;ZILjava/lang/Object;)Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->onEvent:Lkotlin/jvm/functions/Function1;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->shouldDisableUnitCreation:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->copy(Lkotlin/jvm/functions/Function1;Z)Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->onEvent:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->shouldDisableUnitCreation:Z

    return v0
.end method

.method public final copy(Lkotlin/jvm/functions/Function1;Z)Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event;",
            "Lkotlin/Unit;",
            ">;Z)",
            "Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;"
        }
    .end annotation

    new-instance v0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;-><init>(Lkotlin/jvm/functions/Function1;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;

    iget-object v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->onEvent:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->onEvent:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->shouldDisableUnitCreation:Z

    iget-boolean p1, p1, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->shouldDisableUnitCreation:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getOnEvent()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->onEvent:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getShouldDisableUnitCreation()Z
    .locals 1

    .line 236
    iget-boolean v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->shouldDisableUnitCreation:Z

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->onEvent:Lkotlin/jvm/functions/Function1;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->shouldDisableUnitCreation:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CreateUnitButtonRow(onEvent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->onEvent:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", shouldDisableUnitCreation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->shouldDisableUnitCreation:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
