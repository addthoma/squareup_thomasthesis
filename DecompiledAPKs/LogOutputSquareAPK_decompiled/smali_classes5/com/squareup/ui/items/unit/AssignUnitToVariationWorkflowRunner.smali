.class public final Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "AssignUnitToVariationWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;,
        Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;,
        Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;,
        Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$FullSheetBootstrapScreen;,
        Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;,
        Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAssignUnitToVariationWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AssignUnitToVariationWorkflowRunner.kt\ncom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner\n+ 2 PosContainer.kt\ncom/squareup/ui/main/PosContainers\n*L\n1#1,225:1\n152#2:226\n*E\n*S KotlinDebug\n*F\n+ 1 AssignUnitToVariationWorkflowRunner.kt\ncom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner\n*L\n66#1:226\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\u0018\u0000 #2\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0006 !\"#$%B/\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0014J\u0010\u0010\u001c\u001a\u00020\u00192\u0006\u0010\u001d\u001a\u00020\u0003H\u0002J\u000e\u0010\u001e\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000RH\u0010\u000f\u001a6\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0012\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0013j\u0002`\u00140\u0011j\u0008\u0012\u0004\u0012\u00020\u0012`\u00150\u00108TX\u0094\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;",
        "viewFactory",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "resultHandler",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;",
        "assignUnitToVariationWorkflow",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;",
        "layoutConfig",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;",
        "(Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;)V",
        "workflow",
        "Lcom/squareup/workflow/Workflow;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "getWorkflow",
        "()Lcom/squareup/workflow/Workflow;",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "onUnitAssigned",
        "result",
        "updateAssignUnitToVariationInput",
        "input",
        "AssignUnitLayoutConfiguration",
        "AssignUnitToVariationWorkflowResultHandler",
        "CardOverSheetBootstrapScreen",
        "Companion",
        "FullSheetBootstrapScreen",
        "Scope",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Companion;


# instance fields
.field private final assignUnitToVariationWorkflow:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;

.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final layoutConfig:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;

.field private final resultHandler:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "viewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resultHandler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignUnitToVariationWorkflow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "layoutConfig"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p5}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;->getServiceName()Ljava/lang/String;

    move-result-object v2

    .line 41
    invoke-interface {p2}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 42
    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 39
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p3, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->resultHandler:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;

    iput-object p4, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->assignUnitToVariationWorkflow:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;

    iput-object p5, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->layoutConfig:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;

    return-void
.end method

.method public static final synthetic access$ensureWorkflow(Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;)V
    .locals 0

    .line 32
    invoke-virtual {p0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->ensureWorkflow()V

    return-void
.end method

.method public static final synthetic access$getLayoutConfig$p(Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;)Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->layoutConfig:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;

    return-object p0
.end method

.method public static final synthetic access$onUnitAssigned(Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->onUnitAssigned(Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;)V

    return-void
.end method

.method private final onUnitAssigned(Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;)V
    .locals 3

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->resultHandler:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;

    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;->getSelectedUnitId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;->getCreatedUnits()Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;->onUnitAssignedToVariation(Ljava/lang/String;Ljava/util/List;)V

    .line 66
    iget-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    .line 226
    const-class v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/PosContainer;->goBackPast([Ljava/lang/Class;)V

    return-void
.end method

.method public static final startNewWorkflow(Lmortar/MortarScope;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Companion;->startNewWorkflow(Lmortar/MortarScope;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;)V

    return-void
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Workflow<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->assignUnitToVariationWorkflow:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;

    check-cast v0, Lcom/squareup/workflow/Workflow;

    new-instance v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$workflow$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$workflow$1;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/workflow/StatelessWorkflowKt;->mapRendering(Lcom/squareup/workflow/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Workflow;

    move-result-object v0

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 53
    invoke-virtual {p0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onUpdateScreens().subscribe(container::pushStack)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 56
    invoke-virtual {p0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$onEnterScope$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onResult().subscribe(::onUnitAssigned)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public final updateAssignUnitToVariationInput(Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;)V
    .locals 1

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->setProps(Ljava/lang/Object;)V

    return-void
.end method
