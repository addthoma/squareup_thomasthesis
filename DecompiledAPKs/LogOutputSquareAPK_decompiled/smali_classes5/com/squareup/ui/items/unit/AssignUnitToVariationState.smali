.class public abstract Lcom/squareup/ui/items/unit/AssignUnitToVariationState;
.super Ljava/lang/Object;
.source "AssignUnitToVariationState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;,
        Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;,
        Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnInventoryNotUpdatedOnUnitChange;,
        Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;,
        Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnUnsavedUnitSelectionChange;,
        Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00062\u00020\u0001:\u0006\u0005\u0006\u0007\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u0082\u0001\u0005\u000b\u000c\r\u000e\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState;",
        "",
        "()V",
        "toSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "AddDefaultStandardUnit",
        "Companion",
        "CreateUnit",
        "SelectUnit",
        "WarnInventoryNotUpdatedOnUnitChange",
        "WarnUnsavedUnitSelectionChange",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnInventoryNotUpdatedOnUnitChange;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnUnsavedUnitSelectionChange;",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;-><init>()V

    return-void
.end method


# virtual methods
.method public final toSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 57
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$toSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$toSnapshot$1;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
