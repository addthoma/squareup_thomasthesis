.class Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;
.super Ljava/lang/Object;
.source "ModifierSetEditState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ModifierSetEditState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ItemKeys"
.end annotation


# instance fields
.field final ASSIGNED_ITEMS_KEY:Ljava/lang/String;

.field final MODIFIER_OPTIONS_KEY:Ljava/lang/String;

.field final MODIFIER_SET_KEY:Ljava/lang/String;

.field final REMOVED_OPTIONS_KEY:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;->MODIFIER_SET_KEY:Ljava/lang/String;

    .line 187
    iput-object p2, p0, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;->ASSIGNED_ITEMS_KEY:Ljava/lang/String;

    .line 188
    iput-object p3, p0, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;->MODIFIER_OPTIONS_KEY:Ljava/lang/String;

    .line 189
    iput-object p4, p0, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;->REMOVED_OPTIONS_KEY:Ljava/lang/String;

    return-void
.end method
