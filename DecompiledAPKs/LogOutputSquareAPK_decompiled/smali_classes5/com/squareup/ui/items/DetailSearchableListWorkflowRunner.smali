.class public final Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "DetailSearchableListWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;,
        Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;,
        Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/ui/items/DetailSearchableListInput;",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDetailSearchableListWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DetailSearchableListWorkflowRunner.kt\ncom/squareup/ui/items/DetailSearchableListWorkflowRunner\n*L\n1#1,267:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 +2\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0003+,-Bo\u0008\u0002\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u00a2\u0006\u0002\u0010\u001eJ\u0008\u0010!\u001a\u00020\"H\u0002J\u0010\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&H\u0014J\u0008\u0010\'\u001a\u00020$H\u0002J\u000e\u0010(\u001a\u00020$2\u0006\u0010)\u001a\u00020*R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u00020\tX\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 \u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/ui/items/DetailSearchableListInput;",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        "viewFactory",
        "Lcom/squareup/ui/items/DetailSearchableListViewFactory;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "workflow",
        "Lcom/squareup/ui/items/DetailSearchableListWorkflow;",
        "flow",
        "Lflow/Flow;",
        "orderEntryAppletGateway",
        "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
        "permissionGatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "editItemGateway",
        "Lcom/squareup/ui/items/EditItemGateway;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "itemsAppletScopeRunner",
        "Lcom/squareup/ui/items/ItemsAppletScopeRunner;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "convertItemsUrlHelper",
        "Lcom/squareup/ui/items/ConvertItemsUrlHelper;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "(Lcom/squareup/ui/items/DetailSearchableListViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/items/DetailSearchableListWorkflow;Lflow/Flow;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/ConvertItemsUrlHelper;Lcom/squareup/util/BrowserLauncher;)V",
        "getWorkflow",
        "()Lcom/squareup/ui/items/DetailSearchableListWorkflow;",
        "isDetailSearchableListScreenInForeground",
        "",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "showHomeScreenInEditModeFromItemsApplet",
        "updateConfiguration",
        "configuration",
        "Lcom/squareup/ui/items/DetailSearchableListConfiguration;",
        "Companion",
        "DetailSearchableListScope",
        "Factory",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Companion;

.field private static final NAME:Ljava/lang/String;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final convertItemsUrlHelper:Lcom/squareup/ui/items/ConvertItemsUrlHelper;

.field private final editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final itemsAppletScopeRunner:Lcom/squareup/ui/items/ItemsAppletScopeRunner;

.field private final orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final workflow:Lcom/squareup/ui/items/DetailSearchableListWorkflow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->Companion:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Companion;

    .line 191
    const-class v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DetailSearchableListWork\u2026owRunner::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->NAME:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/items/DetailSearchableListViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/items/DetailSearchableListWorkflow;Lflow/Flow;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/ConvertItemsUrlHelper;Lcom/squareup/util/BrowserLauncher;)V
    .locals 9

    move-object v8, p0

    .line 68
    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->NAME:Ljava/lang/String;

    .line 69
    invoke-interface {p2}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v2

    .line 70
    move-object v3, p1

    check-cast v3, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x18

    const/4 v7, 0x0

    move-object v0, p0

    .line 67
    invoke-direct/range {v0 .. v7}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v0, p2

    iput-object v0, v8, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    move-object v0, p3

    iput-object v0, v8, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->workflow:Lcom/squareup/ui/items/DetailSearchableListWorkflow;

    move-object v0, p4

    iput-object v0, v8, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->flow:Lflow/Flow;

    move-object v0, p5

    iput-object v0, v8, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    move-object v0, p6

    iput-object v0, v8, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object/from16 v0, p7

    iput-object v0, v8, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    move-object/from16 v0, p8

    iput-object v0, v8, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->analytics:Lcom/squareup/analytics/Analytics;

    move-object/from16 v0, p9

    iput-object v0, v8, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v0, p10

    iput-object v0, v8, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->itemsAppletScopeRunner:Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    move-object/from16 v0, p11

    iput-object v0, v8, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->cogs:Lcom/squareup/cogs/Cogs;

    move-object/from16 v0, p12

    iput-object v0, v8, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->convertItemsUrlHelper:Lcom/squareup/ui/items/ConvertItemsUrlHelper;

    move-object/from16 v0, p13

    iput-object v0, v8, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/items/DetailSearchableListViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/items/DetailSearchableListWorkflow;Lflow/Flow;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/ConvertItemsUrlHelper;Lcom/squareup/util/BrowserLauncher;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 52
    invoke-direct/range {p0 .. p13}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;-><init>(Lcom/squareup/ui/items/DetailSearchableListViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/items/DetailSearchableListWorkflow;Lflow/Flow;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/ConvertItemsUrlHelper;Lcom/squareup/util/BrowserLauncher;)V

    return-void
.end method

.method public static final synthetic access$ensureWorkflow(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)V
    .locals 0

    .line 52
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->ensureWorkflow()V

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getBrowserLauncher$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/util/BrowserLauncher;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-object p0
.end method

.method public static final synthetic access$getCogs$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/cogs/Cogs;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->cogs:Lcom/squareup/cogs/Cogs;

    return-object p0
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-object p0
.end method

.method public static final synthetic access$getConvertItemsUrlHelper$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/ui/items/ConvertItemsUrlHelper;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->convertItemsUrlHelper:Lcom/squareup/ui/items/ConvertItemsUrlHelper;

    return-object p0
.end method

.method public static final synthetic access$getEditItemGateway$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/ui/items/EditItemGateway;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    return-object p0
.end method

.method public static final synthetic access$getFeatures$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method public static final synthetic access$getItemsAppletScopeRunner$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/ui/items/ItemsAppletScopeRunner;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->itemsAppletScopeRunner:Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    return-object p0
.end method

.method public static final synthetic access$getNAME$cp()Ljava/lang/String;
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->NAME:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getOrderEntryAppletGateway$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/orderentry/OrderEntryAppletGateway;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    return-object p0
.end method

.method public static final synthetic access$getPermissionGatekeeper$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/permissions/PermissionGatekeeper;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    return-object p0
.end method

.method public static final synthetic access$isDetailSearchableListScreenInForeground(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Z
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->isDetailSearchableListScreenInForeground()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$showHomeScreenInEditModeFromItemsApplet(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)V
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->showHomeScreenInEditModeFromItemsApplet()V

    return-void
.end method

.method private final isDetailSearchableListScreenInForeground()Z
    .locals 2

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->getHistory()Lflow/History;

    move-result-object v0

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    .line 187
    instance-of v1, v0, Lcom/squareup/container/WorkflowTreeKey;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/squareup/container/WorkflowTreeKey;

    iget-object v0, v0, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListScreen;->Companion:Lcom/squareup/ui/items/DetailSearchableListScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final showHomeScreenInEditModeFromItemsApplet()V
    .locals 4

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$showHomeScreenInEditModeFromItemsApplet$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$showHomeScreenInEditModeFromItemsApplet$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)V

    check-cast v2, Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

    const-string v3, "showHomeScreenInEditModeFromItemsApplet"

    invoke-direct {v1, v3, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public static final startNewWorkflow(Lmortar/MortarScope;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->Companion:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Companion;->startNewWorkflow(Lmortar/MortarScope;)V

    return-void
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/ui/items/DetailSearchableListWorkflow;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->workflow:Lcom/squareup/ui/items/DetailSearchableListWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 52
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->getWorkflow()Lcom/squareup/ui/items/DetailSearchableListWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 75
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onUpdateScreens().subscribe(container::pushStack)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 78
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onResult().subscribe {\n \u2026}\n        }\n      }\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public final updateConfiguration(Lcom/squareup/ui/items/DetailSearchableListConfiguration;)V
    .locals 2

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListInput;

    .line 140
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {v1}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->hasFavoritesEditor()Z

    move-result v1

    .line 138
    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/items/DetailSearchableListInput;-><init>(Lcom/squareup/ui/items/DetailSearchableListConfiguration;Z)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->setProps(Ljava/lang/Object;)V

    return-void
.end method
