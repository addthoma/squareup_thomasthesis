.class public final Lcom/squareup/ui/items/EditItemLabelScreen;
.super Lcom/squareup/ui/items/InEditItemScope;
.source "EditItemLabelScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/EditItemLabelScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditItemLabelScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditItemLabelScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$EditItemLabelScreen$Nvx7oS4I6Wtqe76K55CmSfPhdu4;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditItemLabelScreen$Nvx7oS4I6Wtqe76K55CmSfPhdu4;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/EditItemLabelScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/items/EditItemScope;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/InEditItemScope;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditItemLabelScreen;
    .locals 1

    .line 30
    const-class v0, Lcom/squareup/ui/items/EditItemScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/EditItemScope;

    .line 31
    new-instance v0, Lcom/squareup/ui/items/EditItemLabelScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditItemLabelScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 35
    sget v0, Lcom/squareup/edititem/R$layout;->edit_item_label_view:I

    return v0
.end method
