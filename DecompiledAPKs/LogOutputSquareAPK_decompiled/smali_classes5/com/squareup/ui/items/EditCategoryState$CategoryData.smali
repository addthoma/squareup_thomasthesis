.class Lcom/squareup/ui/items/EditCategoryState$CategoryData;
.super Ljava/lang/Object;
.source "EditCategoryState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditCategoryState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CategoryData"
.end annotation


# instance fields
.field private addedItems:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private categoryBuilder:Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

.field private categoryOriginal:Lcom/squareup/shared/catalog/models/CatalogItemCategory;

.field private isNewCategory:Z

.field private removedItems:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->addedItems:Ljava/util/Set;

    .line 182
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->removedItems:Ljava/util/Set;

    const/4 v0, 0x1

    .line 183
    iput-boolean v0, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->isNewCategory:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;
    .locals 0

    .line 173
    iget-object p0, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->categoryBuilder:Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    return-object p0
.end method

.method static synthetic access$002(Lcom/squareup/ui/items/EditCategoryState$CategoryData;Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->categoryBuilder:Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    return-object p1
.end method

.method static synthetic access$100(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Ljava/util/Set;
    .locals 0

    .line 173
    iget-object p0, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->addedItems:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$102(Lcom/squareup/ui/items/EditCategoryState$CategoryData;Ljava/util/Set;)Ljava/util/Set;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->addedItems:Ljava/util/Set;

    return-object p1
.end method

.method static synthetic access$200(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Ljava/util/Set;
    .locals 0

    .line 173
    iget-object p0, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->removedItems:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$202(Lcom/squareup/ui/items/EditCategoryState$CategoryData;Ljava/util/Set;)Ljava/util/Set;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->removedItems:Ljava/util/Set;

    return-object p1
.end method

.method static synthetic access$300(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Lcom/squareup/shared/catalog/models/CatalogItemCategory;
    .locals 0

    .line 173
    iget-object p0, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->categoryOriginal:Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    return-object p0
.end method

.method static synthetic access$302(Lcom/squareup/ui/items/EditCategoryState$CategoryData;Lcom/squareup/shared/catalog/models/CatalogItemCategory;)Lcom/squareup/shared/catalog/models/CatalogItemCategory;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->categoryOriginal:Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    return-object p1
.end method

.method static synthetic access$400(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Z
    .locals 0

    .line 173
    iget-boolean p0, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->isNewCategory:Z

    return p0
.end method

.method static synthetic access$402(Lcom/squareup/ui/items/EditCategoryState$CategoryData;Z)Z
    .locals 0

    .line 173
    iput-boolean p1, p0, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->isNewCategory:Z

    return p1
.end method
