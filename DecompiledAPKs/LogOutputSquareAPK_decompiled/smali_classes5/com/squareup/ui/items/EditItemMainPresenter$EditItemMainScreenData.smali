.class Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;
.super Ljava/lang/Object;
.source "EditItemMainPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemMainPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "EditItemMainScreenData"
.end annotation


# instance fields
.field final abbreviation:Ljava/lang/String;

.field final categoryName:Ljava/lang/String;

.field final checkoutLink:Ljava/lang/String;

.field final checkoutLinkToggleState:Z

.field final defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

.field final deleteButtonText:Ljava/lang/String;

.field final description:Ljava/lang/String;

.field final detailsHeader:Ljava/lang/String;

.field final employeeDescription:Ljava/lang/String;

.field final enableRateBasedServices:Z

.field final errorKeys:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final isNewItem:Z

.field final isService:Z

.field final itemName:Ljava/lang/String;

.field final itemOptionsAndValuesList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field final itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

.field final itemType:Lcom/squareup/api/items/Item$Type;

.field final labelColor:Ljava/lang/String;

.field final labelText:Ljava/lang/String;

.field final measurementUnits:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;"
        }
    .end annotation
.end field

.field final modifierLists:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditItemState$ModifierState;",
            ">;"
        }
    .end annotation
.end field

.field final shouldNameTakeExtraSpace:Z

.field final shouldShowInlineVariation:Z

.field final showCancellationFeeField:Z

.field final showCheckoutLink:Z

.field final showLocationBanner:Z

.field final showOptionSection:Z

.field final showProcessingTime:Z

.field final showTextTile:Z

.field final taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditItemState$TaxState;",
            ">;"
        }
    .end annotation
.end field

.field final variations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;",
            ">;"
        }
    .end annotation
.end field

.field final variationsAreDraggable:Z


# direct methods
.method constructor <init>(ZZLjava/lang/String;Lcom/squareup/ui/photo/ItemPhoto;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;ZZZLcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/Set;ZZLjava/util/Map;Ljava/lang/String;ZZZLjava/util/List;ZLjava/lang/String;ZZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/photo/ItemPhoto;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;",
            ">;",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;",
            "ZZZ",
            "Lcom/squareup/api/items/Item$Type;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditItemState$TaxState;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditItemState$ModifierState;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;ZZ",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;",
            "Ljava/lang/String;",
            "ZZZ",
            "Ljava/util/List<",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;Z",
            "Ljava/lang/String;",
            "ZZ)V"
        }
    .end annotation

    move-object v0, p0

    .line 1111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move v1, p1

    .line 1112
    iput-boolean v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->showLocationBanner:Z

    move v1, p2

    .line 1113
    iput-boolean v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->showTextTile:Z

    move-object v1, p3

    .line 1114
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->itemName:Ljava/lang/String;

    move-object v1, p4

    .line 1115
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    move-object v1, p5

    .line 1116
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->abbreviation:Ljava/lang/String;

    move-object v1, p6

    .line 1117
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->labelColor:Ljava/lang/String;

    move-object v1, p7

    .line 1118
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->labelText:Ljava/lang/String;

    move-object v1, p8

    .line 1119
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->variations:Ljava/util/List;

    move-object v1, p9

    .line 1120
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move v1, p11

    .line 1121
    iput-boolean v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->isNewItem:Z

    move v1, p12

    .line 1122
    iput-boolean v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->isService:Z

    move-object/from16 v1, p13

    .line 1123
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->itemType:Lcom/squareup/api/items/Item$Type;

    move-object/from16 v1, p14

    .line 1124
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->categoryName:Ljava/lang/String;

    move-object/from16 v1, p15

    .line 1125
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->description:Ljava/lang/String;

    move-object/from16 v1, p16

    .line 1126
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->detailsHeader:Ljava/lang/String;

    move-object/from16 v1, p17

    .line 1127
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->taxes:Ljava/util/List;

    move-object/from16 v1, p18

    .line 1128
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->modifierLists:Ljava/util/List;

    move-object/from16 v1, p19

    .line 1129
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->deleteButtonText:Ljava/lang/String;

    move-object/from16 v1, p20

    .line 1130
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->errorKeys:Ljava/util/Set;

    move/from16 v1, p21

    .line 1131
    iput-boolean v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->showCancellationFeeField:Z

    move/from16 v1, p22

    .line 1132
    iput-boolean v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->shouldNameTakeExtraSpace:Z

    move-object/from16 v1, p23

    .line 1133
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->measurementUnits:Ljava/util/Map;

    move-object/from16 v1, p24

    .line 1134
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->employeeDescription:Ljava/lang/String;

    move/from16 v1, p25

    .line 1135
    iput-boolean v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->variationsAreDraggable:Z

    move/from16 v1, p26

    .line 1136
    iput-boolean v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->enableRateBasedServices:Z

    move/from16 v1, p27

    .line 1137
    iput-boolean v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->showOptionSection:Z

    move-object/from16 v1, p28

    .line 1138
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->itemOptionsAndValuesList:Ljava/util/List;

    move/from16 v2, p29

    .line 1139
    iput-boolean v2, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->showCheckoutLink:Z

    move-object/from16 v2, p30

    .line 1140
    iput-object v2, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->checkoutLink:Ljava/lang/String;

    move/from16 v2, p31

    .line 1141
    iput-boolean v2, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->checkoutLinkToggleState:Z

    move/from16 v2, p32

    .line 1142
    iput-boolean v2, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->showProcessingTime:Z

    if-eqz p10, :cond_0

    .line 1145
    invoke-interface/range {p28 .. p28}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->shouldShowInlineVariation:Z

    return-void
.end method


# virtual methods
.method public getDefaultVariationPrecision()I
    .locals 1

    .line 1153
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->getPrecision(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)I

    move-result v0

    return v0
.end method

.method public getMeasurementUnit(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;
    .locals 1

    .line 1164
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getCatalogMeasurementUnitToken()Ljava/lang/String;

    move-result-object p1

    .line 1165
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->measurementUnits:Ljava/util/Map;

    .line 1167
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    :goto_0
    return-object p1
.end method

.method public getPrecision(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)I
    .locals 0

    .line 1157
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->getMeasurementUnit(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 1160
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result p1

    :goto_0
    return p1
.end method

.method public isTransitionTimeRequired()Z
    .locals 5

    .line 1149
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getTransitionTime()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
