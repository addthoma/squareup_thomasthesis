.class public final Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;
.super Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData;
.source "EditVariationRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EditItemVariationScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u001f\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001Ba\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u000c\u001a\u0004\u0018\u00010\r\u0012\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u0003\u0012\u0006\u0010\u000f\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0010J\t\u0010 \u001a\u00020\u0003H\u00c6\u0003J\t\u0010!\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0005H\u00c6\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\u0008H\u00c6\u0003J\t\u0010%\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010&\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\'\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010(\u001a\u0004\u0018\u00010\rH\u00c6\u0003J\t\u0010)\u001a\u00020\u0003H\u00c6\u0003Jw\u0010*\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\u00032\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\r2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010+\u001a\u00020\u00032\u0008\u0010,\u001a\u0004\u0018\u00010-H\u00d6\u0003J\t\u0010.\u001a\u00020/H\u00d6\u0001J\t\u00100\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\t\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0012R\u001c\u0010\u000c\u001a\u0004\u0018\u00010\rX\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0011\u0010\u000f\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0012R\u0011\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u0012R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u0018R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u0018R\u0013\u0010\n\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u0018\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;",
        "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData;",
        "isNewVariation",
        "",
        "variationId",
        "",
        "name",
        "price",
        "Lcom/squareup/protos/common/Money;",
        "hasInclusiveTaxesApplied",
        "variationMerchantCatalogToken",
        "sku",
        "measurementUnit",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "shouldUseReadOnlyNameField",
        "shouldHideDeleteButton",
        "(ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;ZZ)V",
        "getHasInclusiveTaxesApplied",
        "()Z",
        "getMeasurementUnit",
        "()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "setMeasurementUnit",
        "(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V",
        "getName",
        "()Ljava/lang/String;",
        "getPrice",
        "()Lcom/squareup/protos/common/Money;",
        "getShouldHideDeleteButton",
        "getShouldUseReadOnlyNameField",
        "getSku",
        "getVariationId",
        "getVariationMerchantCatalogToken",
        "component1",
        "component10",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final hasInclusiveTaxesApplied:Z

.field private final isNewVariation:Z

.field private measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

.field private final name:Ljava/lang/String;

.field private final price:Lcom/squareup/protos/common/Money;

.field private final shouldHideDeleteButton:Z

.field private final shouldUseReadOnlyNameField:Z

.field private final sku:Ljava/lang/String;

.field private final variationId:Ljava/lang/String;

.field private final variationMerchantCatalogToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;ZZ)V
    .locals 1

    const-string/jumbo v0, "variationId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 685
    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->isNewVariation:Z

    iput-object p2, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->variationId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->name:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->price:Lcom/squareup/protos/common/Money;

    iput-boolean p5, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->hasInclusiveTaxesApplied:Z

    iput-object p6, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->variationMerchantCatalogToken:Ljava/lang/String;

    iput-object p7, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->sku:Ljava/lang/String;

    iput-object p8, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    iput-boolean p9, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->shouldUseReadOnlyNameField:Z

    iput-boolean p10, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->shouldHideDeleteButton:Z

    return-void
.end method

.method public synthetic constructor <init>(ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 12

    move/from16 v0, p11

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v10, 0x0

    goto :goto_0

    :cond_0
    move/from16 v10, p9

    :goto_0
    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v11, p10

    .line 683
    invoke-direct/range {v1 .. v11}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;-><init>(ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;ZZ)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;ZZILjava/lang/Object;)Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;
    .locals 11

    move-object v0, p0

    move/from16 v1, p11

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->isNewVariation()Z

    move-result v2

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getVariationId()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getName()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v5

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getHasInclusiveTaxesApplied()Z

    move-result v6

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->variationMerchantCatalogToken:Ljava/lang/String;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->sku:Ljava/lang/String;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v9

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-boolean v10, v0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->shouldUseReadOnlyNameField:Z

    goto :goto_8

    :cond_8
    move/from16 v10, p9

    :goto_8
    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    iget-boolean v1, v0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->shouldHideDeleteButton:Z

    goto :goto_9

    :cond_9
    move/from16 v1, p10

    :goto_9
    move p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move/from16 p9, v10

    move/from16 p10, v1

    invoke-virtual/range {p0 .. p10}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->copy(ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;ZZ)Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->isNewVariation()Z

    move-result v0

    return v0
.end method

.method public final component10()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->shouldHideDeleteButton:Z

    return v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getVariationId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component4()Lcom/squareup/protos/common/Money;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getHasInclusiveTaxesApplied()Z

    move-result v0

    return v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->variationMerchantCatalogToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component7()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->sku:Ljava/lang/String;

    return-object v0
.end method

.method public final component8()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v0

    return-object v0
.end method

.method public final component9()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->shouldUseReadOnlyNameField:Z

    return v0
.end method

.method public final copy(ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;ZZ)Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;
    .locals 12

    const-string/jumbo v0, "variationId"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;

    move-object v1, v0

    move v2, p1

    move-object v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    invoke-direct/range {v1 .. v11}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;-><init>(ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->isNewVariation()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->isNewVariation()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getVariationId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getVariationId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getHasInclusiveTaxesApplied()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getHasInclusiveTaxesApplied()Z

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->variationMerchantCatalogToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->variationMerchantCatalogToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->sku:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->sku:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->shouldUseReadOnlyNameField:Z

    iget-boolean v1, p1, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->shouldUseReadOnlyNameField:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->shouldHideDeleteButton:Z

    iget-boolean p1, p1, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->shouldHideDeleteButton:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getHasInclusiveTaxesApplied()Z
    .locals 1

    .line 679
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->hasInclusiveTaxesApplied:Z

    return v0
.end method

.method public getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;
    .locals 1

    .line 682
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 677
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPrice()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 678
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->price:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getShouldHideDeleteButton()Z
    .locals 1

    .line 684
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->shouldHideDeleteButton:Z

    return v0
.end method

.method public final getShouldUseReadOnlyNameField()Z
    .locals 1

    .line 683
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->shouldUseReadOnlyNameField:Z

    return v0
.end method

.method public final getSku()Ljava/lang/String;
    .locals 1

    .line 681
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->sku:Ljava/lang/String;

    return-object v0
.end method

.method public getVariationId()Ljava/lang/String;
    .locals 1

    .line 676
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->variationId:Ljava/lang/String;

    return-object v0
.end method

.method public final getVariationMerchantCatalogToken()Ljava/lang/String;
    .locals 1

    .line 680
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->variationMerchantCatalogToken:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->isNewVariation()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getVariationId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getHasInclusiveTaxesApplied()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->variationMerchantCatalogToken:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->sku:Ljava/lang/String;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_6
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_7
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->shouldUseReadOnlyNameField:Z

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :cond_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->shouldHideDeleteButton:Z

    if-eqz v2, :cond_9

    goto :goto_5

    :cond_9
    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    return v0
.end method

.method public isNewVariation()Z
    .locals 1

    .line 675
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->isNewVariation:Z

    return v0
.end method

.method public setMeasurementUnit(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
    .locals 0

    .line 682
    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EditItemVariationScreenData(isNewVariation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->isNewVariation()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", variationId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getVariationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", price="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", hasInclusiveTaxesApplied="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getHasInclusiveTaxesApplied()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", variationMerchantCatalogToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->variationMerchantCatalogToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", sku="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->sku:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", measurementUnit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", shouldUseReadOnlyNameField="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->shouldUseReadOnlyNameField:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", shouldHideDeleteButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->shouldHideDeleteButton:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
