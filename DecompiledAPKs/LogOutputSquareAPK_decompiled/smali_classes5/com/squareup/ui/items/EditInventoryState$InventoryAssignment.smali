.class Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;
.super Ljava/lang/Object;
.source "EditInventoryState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditInventoryState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InventoryAssignment"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field count:Ljava/math/BigDecimal;

.field unitCost:Lcom/squareup/protos/common/Money;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 186
    new-instance v0, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    iput-object p1, p0, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;->count:Ljava/math/BigDecimal;

    .line 183
    iput-object p2, p0, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;->unitCost:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 203
    iget-object p2, p0, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;->count:Ljava/math/BigDecimal;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 204
    iget-object p2, p0, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;->unitCost:Lcom/squareup/protos/common/Money;

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    return-void
.end method
