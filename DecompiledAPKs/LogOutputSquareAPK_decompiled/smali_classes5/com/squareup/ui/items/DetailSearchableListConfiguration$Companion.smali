.class public final Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;
.super Ljava/lang/Object;
.source "DetailSearchableListConfiguration.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DetailSearchableListConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDetailSearchableListConfiguration.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DetailSearchableListConfiguration.kt\ncom/squareup/ui/items/DetailSearchableListConfiguration$Companion\n*L\n1#1,69:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u0011R\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\t\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008R\u0011\u0010\u000b\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u0008R\u0011\u0010\r\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u0008\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;",
        "",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "Lcom/squareup/ui/items/DetailSearchableListConfiguration;",
        "DISCOUNTS",
        "getDISCOUNTS",
        "()Lcom/squareup/ui/items/DetailSearchableListConfiguration;",
        "ITEMS",
        "getITEMS",
        "SERVICES",
        "getSERVICES",
        "UNITS",
        "getUNITS",
        "ofDataType",
        "dataType",
        "Lcom/squareup/ui/items/DetailSearchableListDataType;",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getDISCOUNTS()Lcom/squareup/ui/items/DetailSearchableListConfiguration;
    .locals 1

    .line 47
    invoke-static {}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->access$getDISCOUNTS$cp()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public final getITEMS()Lcom/squareup/ui/items/DetailSearchableListConfiguration;
    .locals 1

    .line 51
    invoke-static {}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->access$getITEMS$cp()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public final getSERVICES()Lcom/squareup/ui/items/DetailSearchableListConfiguration;
    .locals 1

    .line 55
    invoke-static {}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->access$getSERVICES$cp()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public final getUNITS()Lcom/squareup/ui/items/DetailSearchableListConfiguration;
    .locals 1

    .line 43
    invoke-static {}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->access$getUNITS$cp()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public final ofDataType(Lcom/squareup/ui/items/DetailSearchableListDataType;)Lcom/squareup/ui/items/DetailSearchableListConfiguration;
    .locals 2

    const-string v0, "dataType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListDataType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 64
    move-object p1, p0

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;->getSERVICES()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object p1

    goto :goto_0

    .line 65
    :cond_0
    new-instance p1, Lkotlin/NotImplementedError;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "An operation is not implemented: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "CATALOG-8676: refactor old lists to use the workflow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 63
    :cond_1
    move-object p1, p0

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;->getITEMS()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object p1

    goto :goto_0

    .line 62
    :cond_2
    move-object p1, p0

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;->getDISCOUNTS()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object p1

    goto :goto_0

    .line 61
    :cond_3
    move-object p1, p0

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;->getUNITS()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object p1

    :goto_0
    return-object p1
.end method
