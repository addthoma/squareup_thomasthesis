.class public final Lcom/squareup/ui/items/ItemsClientActionTranslator;
.super Ljava/lang/Object;
.source "ItemsClientActionTranslator.kt"

# interfaces
.implements Lcom/squareup/clientactiontranslation/ClientActionTranslator;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/items/ItemsClientActionTranslator;",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslator;",
        "()V",
        "translate",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;",
        "clientAction",
        "Lcom/squareup/protos/client/ClientAction;",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public translate(Lcom/squareup/protos/client/ClientAction;)Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;
    .locals 1

    const-string v0, "clientAction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    const-string v0, "square-register://items"

    invoke-direct {p1, v0}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    goto :goto_0

    .line 14
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/ClientAction;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    const-string v0, "square-register://items/create"

    invoke-direct {p1, v0}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    goto :goto_0

    .line 15
    :cond_1
    sget-object p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Unhandled;->INSTANCE:Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Unhandled;

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    :goto_0
    return-object p1
.end method
