.class public final Lcom/squareup/ui/items/ModifierSetAssignmentScreen;
.super Lcom/squareup/ui/items/InEditModifierSetScope;
.source "ModifierSetAssignmentScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Component;,
        Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/ModifierSetAssignmentScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 226
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$ModifierSetAssignmentScreen$Yf6V2j0i-Vc4H1TsiOR9zXkk6Dk;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$ModifierSetAssignmentScreen$Yf6V2j0i-Vc4H1TsiOR9zXkk6Dk;

    .line 227
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/InEditModifierSetScope;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/ModifierSetAssignmentScreen;
    .locals 1

    .line 228
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 229
    new-instance v0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 222
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/items/InEditModifierSetScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 223
    iget-object p2, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen;->modifierSetId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 233
    sget v0, Lcom/squareup/itemsapplet/R$layout;->modifier_assignment:I

    return v0
.end method
