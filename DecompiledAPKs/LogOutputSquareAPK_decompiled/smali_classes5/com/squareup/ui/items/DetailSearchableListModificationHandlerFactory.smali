.class public final Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;
.super Ljava/lang/Object;
.source "DetailSearchableListModificationHandlerFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "editUnitWorkflow",
        "Lcom/squareup/items/unit/EditUnitWorkflow;",
        "(Lcom/squareup/util/Res;Lcom/squareup/items/unit/EditUnitWorkflow;)V",
        "createHandler",
        "Lcom/squareup/ui/items/DetailSearchableListCreateHandler;",
        "type",
        "Lcom/squareup/ui/items/DetailSearchableListDataType;",
        "editHandler",
        "Lcom/squareup/ui/items/DetailSearchableListEditHandler;",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final editUnitWorkflow:Lcom/squareup/items/unit/EditUnitWorkflow;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/items/unit/EditUnitWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editUnitWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;->editUnitWorkflow:Lcom/squareup/items/unit/EditUnitWorkflow;

    return-void
.end method


# virtual methods
.method public final createHandler(Lcom/squareup/ui/items/DetailSearchableListDataType;)Lcom/squareup/ui/items/DetailSearchableListCreateHandler;
    .locals 2

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListDataType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 16
    sget-object p1, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->Companion:Lcom/squareup/ui/items/UnitListObjectModificationHandler$Companion;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;->editUnitWorkflow:Lcom/squareup/items/unit/EditUnitWorkflow;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/items/UnitListObjectModificationHandler$Companion;->of(Lcom/squareup/util/Res;Lcom/squareup/items/unit/EditUnitWorkflow;)Lcom/squareup/ui/items/UnitListObjectModificationHandler;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListCreateHandler;

    return-object p1

    .line 17
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "No handler exists for type"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final editHandler(Lcom/squareup/ui/items/DetailSearchableListDataType;)Lcom/squareup/ui/items/DetailSearchableListEditHandler;
    .locals 2

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListDataType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 24
    sget-object p1, Lcom/squareup/ui/items/UnitListObjectModificationHandler;->Companion:Lcom/squareup/ui/items/UnitListObjectModificationHandler$Companion;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;->editUnitWorkflow:Lcom/squareup/items/unit/EditUnitWorkflow;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/items/UnitListObjectModificationHandler$Companion;->of(Lcom/squareup/util/Res;Lcom/squareup/items/unit/EditUnitWorkflow;)Lcom/squareup/ui/items/UnitListObjectModificationHandler;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListEditHandler;

    return-object p1

    .line 25
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "No handler exists for type"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
