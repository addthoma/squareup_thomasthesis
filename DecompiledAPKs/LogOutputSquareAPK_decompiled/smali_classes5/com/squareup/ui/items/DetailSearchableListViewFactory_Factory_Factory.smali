.class public final Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;
.super Ljava/lang/Object;
.source "DetailSearchableListViewFactory_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final barcodeScannerTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final durationFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final editUnitViewBindingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/ui/EditUnitViewBindings;",
            ">;"
        }
    .end annotation
.end field

.field private final itemPhotosProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final priceFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final tileAppearanceSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/ui/EditUnitViewBindings;",
            ">;)V"
        }
    .end annotation

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->recyclerFactoryProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->itemPhotosProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p5, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->priceFormatterProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p7, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->durationFormatterProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p8, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p9, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p10, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p11, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p12, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p13, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->editUnitViewBindingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/ui/EditUnitViewBindings;",
            ">;)",
            "Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;"
        }
    .end annotation

    .line 98
    new-instance v14, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static newInstance(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;Lcom/squareup/items/unit/ui/EditUnitViewBindings;)Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/items/unit/ui/EditUnitViewBindings;",
            ")",
            "Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;"
        }
    .end annotation

    .line 107
    new-instance v14, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;-><init>(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;Lcom/squareup/items/unit/ui/EditUnitViewBindings;)V

    return-object v14
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;
    .locals 14

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->recyclerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/recycler/RecyclerFactory;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->itemPhotosProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->priceFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->durationFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/text/DurationFormatter;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->editUnitViewBindingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/items/unit/ui/EditUnitViewBindings;

    invoke-static/range {v1 .. v13}, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->newInstance(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;Lcom/squareup/items/unit/ui/EditUnitViewBindings;)Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListViewFactory_Factory_Factory;->get()Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;

    move-result-object v0

    return-object v0
.end method
