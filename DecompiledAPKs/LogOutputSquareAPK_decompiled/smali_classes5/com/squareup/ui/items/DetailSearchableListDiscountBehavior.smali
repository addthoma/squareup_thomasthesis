.class public final Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;
.super Ljava/lang/Object;
.source "DetailSearchableListDiscountBehavior.kt"

# interfaces
.implements Lcom/squareup/ui/items/DetailSearchableListBehavior;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001BY\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0002\u0010\u0016JF\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020 0\u001f2\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010!\u001a\u00020\"2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J$\u0010#\u001a\u000e\u0012\u0004\u0012\u00020%\u0012\u0004\u0012\u00020&0$2\u0006\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020*H\u0016J$\u0010+\u001a\u000e\u0012\u0004\u0012\u00020%\u0012\u0004\u0012\u00020&0$2\u0006\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020,H\u0016J\u0016\u0010-\u001a\u0008\u0012\u0004\u0012\u00020 0\u001f2\u0006\u0010!\u001a\u00020\"H\u0016R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0019\u001a\u0004\u0008\u0017\u0010\u0018R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;",
        "Lcom/squareup/ui/items/DetailSearchableListBehavior;",
        "textBag",
        "Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "shouldShowCreateFromSearchButton",
        "",
        "objectNumberLimit",
        "",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "badBus",
        "Lcom/squareup/badbus/BadBus;",
        "discountCursorFactory",
        "Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;",
        "discountBundleFactory",
        "Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;",
        "timeZone",
        "Ljava/util/TimeZone;",
        "catalogLocalizer",
        "Lcom/squareup/shared/i18n/Localizer;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;ZLjava/lang/Integer;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Ljava/util/TimeZone;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/analytics/Analytics;)V",
        "getObjectNumberLimit",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "getShouldShowCreateFromSearchButton",
        "()Z",
        "getTextBag",
        "()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "getLatestDiscountsListData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/items/DetailSearchableListData;",
        "searchTerm",
        "",
        "handlePressingCreateButton",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/items/DetailSearchableListState;",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        "currentState",
        "Lcom/squareup/ui/items/DetailSearchableListState$ShowList;",
        "event",
        "Lcom/squareup/ui/items/DetailSearchableListScreen$Event$CreateButtonPressed;",
        "handleSelectingObjectFromList",
        "Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;",
        "list",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final catalogLocalizer:Lcom/squareup/shared/i18n/Localizer;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final discountBundleFactory:Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

.field private final discountCursorFactory:Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;

.field private final objectNumberLimit:Ljava/lang/Integer;

.field private final shouldShowCreateFromSearchButton:Z

.field private final textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

.field private final timeZone:Ljava/util/TimeZone;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;ZLjava/lang/Integer;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Ljava/util/TimeZone;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string/jumbo v0, "textBag"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cogs"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badBus"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountCursorFactory"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountBundleFactory"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "timeZone"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "catalogLocalizer"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    iput-boolean p2, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->shouldShowCreateFromSearchButton:Z

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->objectNumberLimit:Ljava/lang/Integer;

    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->cogs:Lcom/squareup/cogs/Cogs;

    iput-object p5, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->badBus:Lcom/squareup/badbus/BadBus;

    iput-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->discountCursorFactory:Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;

    iput-object p7, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->discountBundleFactory:Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    iput-object p8, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->timeZone:Ljava/util/TimeZone;

    iput-object p9, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->catalogLocalizer:Lcom/squareup/shared/i18n/Localizer;

    iput-object p10, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;ZLjava/lang/Integer;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Ljava/util/TimeZone;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/analytics/Analytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 12

    and-int/lit8 v0, p11, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    move v3, p2

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    .line 20
    invoke-direct/range {v1 .. v11}, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;ZLjava/lang/Integer;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Ljava/util/TimeZone;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method

.method private final getLatestDiscountsListData(Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljava/lang/String;Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Ljava/util/TimeZone;Lcom/squareup/shared/i18n/Localizer;)Lio/reactivex/Observable;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/badbus/BadBus;",
            "Ljava/lang/String;",
            "Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;",
            "Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;",
            "Ljava/util/TimeZone;",
            "Lcom/squareup/shared/i18n/Localizer;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/items/DetailSearchableListData;",
            ">;"
        }
    .end annotation

    .line 59
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper;->Companion:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;

    .line 62
    sget-object v3, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-object v10, p0

    .line 64
    iget-object v5, v10, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->analytics:Lcom/squareup/analytics/Analytics;

    .line 65
    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior$getLatestDiscountsListData$1;

    move-object v2, p4

    move-object/from16 v4, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-direct {v1, p4, v6, v4, v7}, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior$getLatestDiscountsListData$1;-><init>(Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;Ljava/util/TimeZone;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Lcom/squareup/shared/i18n/Localizer;)V

    move-object v7, v1

    check-cast v7, Lkotlin/jvm/functions/Function2;

    const/4 v6, 0x0

    const/16 v8, 0x20

    const/4 v9, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    .line 59
    invoke-static/range {v0 .. v9}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;->listCogsObjects$default(Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Lcom/squareup/analytics/Analytics;Ljava/util/List;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getItemTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation

    .line 18
    invoke-static {p0}, Lcom/squareup/ui/items/DetailSearchableListBehavior$DefaultImpls;->getItemTypes(Lcom/squareup/ui/items/DetailSearchableListBehavior;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getObjectNumberLimit()Ljava/lang/Integer;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->objectNumberLimit:Ljava/lang/Integer;

    return-object v0
.end method

.method public getShouldShowCreateFromSearchButton()Z
    .locals 1

    .line 20
    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->shouldShowCreateFromSearchButton:Z

    return v0
.end method

.method public getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    return-object v0
.end method

.method public handlePressingCreateButton(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lcom/squareup/ui/items/DetailSearchableListScreen$Event$CreateButtonPressed;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListState$ShowList;",
            "Lcom/squareup/ui/items/DetailSearchableListScreen$Event$CreateButtonPressed;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;"
        }
    .end annotation

    const-string v0, "currentState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "event"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 34
    sget-object p2, Lcom/squareup/ui/items/DetailSearchableListResult$GoToCreateDiscount;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListResult$GoToCreateDiscount;

    .line 33
    invoke-virtual {p1, p2}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public handleSelectingObjectFromList(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListState$ShowList;",
            "Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;"
        }
    .end annotation

    const-string v0, "currentState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "event"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditDiscount;

    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;->getSelectedObject()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;->getId()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p2}, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditDiscount;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public list(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/items/DetailSearchableListData;",
            ">;"
        }
    .end annotation

    const-string v0, "searchTerm"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->cogs:Lcom/squareup/cogs/Cogs;

    iget-object v3, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->badBus:Lcom/squareup/badbus/BadBus;

    iget-object v5, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->discountCursorFactory:Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;

    iget-object v6, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->discountBundleFactory:Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    iget-object v7, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->timeZone:Ljava/util/TimeZone;

    .line 47
    iget-object v8, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->catalogLocalizer:Lcom/squareup/shared/i18n/Localizer;

    move-object v1, p0

    move-object v4, p1

    .line 45
    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->getLatestDiscountsListData(Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljava/lang/String;Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Ljava/util/TimeZone;Lcom/squareup/shared/i18n/Localizer;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
