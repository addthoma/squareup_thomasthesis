.class public Lcom/squareup/ui/items/ModifiersDetailView;
.super Lcom/squareup/ui/items/DetailSearchableListView;
.source "ModifiersDetailView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/DetailSearchableListView<",
        "Lcom/squareup/ui/items/ModifiersDetailView;",
        ">;"
    }
.end annotation


# instance fields
.field presenter:Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field settings:Lcom/squareup/settings/server/AccountStatusSettings;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected buildAdapter(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            ")",
            "Lcom/squareup/ui/items/DetailSearchableListView<",
            "Lcom/squareup/ui/items/ModifiersDetailView;",
            ">.DetailSearchable",
            "ListAdapter;"
        }
    .end annotation

    .line 66
    new-instance v0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;-><init>(Lcom/squareup/ui/items/ModifiersDetailView;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    return-object v0
.end method

.method bridge synthetic getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifiersDetailView;->getPresenter()Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method getPresenter()Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/items/ModifiersDetailView;->presenter:Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;

    return-object v0
.end method

.method protected inject()V
    .locals 2

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifiersDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/items/ModifiersDetailScreen$Component;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/ModifiersDetailScreen$Component;

    invoke-interface {v0, p0}, Lcom/squareup/ui/items/ModifiersDetailScreen$Component;->inject(Lcom/squareup/ui/items/ModifiersDetailView;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 40
    invoke-super {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->onFinishInflate()V

    .line 41
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifiersDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/squareup/ui/items/ModifiersDetailView;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifiersDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/itemsapplet/R$string;->items_applet_search_modifier_sets:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    .line 43
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifiersDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/itemsapplet/R$string;->items_applet_modifiers_null_message:I

    const-string v3, "learn_more"

    .line 44
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/registerlib/R$string;->modifier_hint_url:I

    .line 45
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->learn_more_lowercase_more:I

    .line 46
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 47
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 49
    iget-object v2, p0, Lcom/squareup/ui/items/ModifiersDetailView;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldDisplayModifierInsteadOfOption()Z

    move-result v2

    if-eqz v2, :cond_0

    sget v2, Lcom/squareup/itemsapplet/R$string;->modifiers_fixed_null_title:I

    goto :goto_0

    :cond_0
    sget v2, Lcom/squareup/itemsapplet/R$string;->items_applet_modifiers_null_title:I

    .line 52
    :goto_0
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/items/ModifiersDetailView;->setNullState(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/items/ModifiersDetailView;->listView:Landroid/widget/ListView;

    check-cast v0, Lcom/mobeta/android/dslv/DragSortListView;

    .line 55
    new-instance v1, Lcom/squareup/ui/library/DragController;

    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifiersDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/squareup/ui/library/DragController;-><init>(Landroid/content/Context;Lcom/mobeta/android/dslv/DragSortListView;)V

    const/4 v2, 0x1

    .line 56
    invoke-virtual {v1, v2}, Lcom/squareup/ui/library/DragController;->setStaticTopRows(I)V

    .line 57
    invoke-virtual {v0, v1}, Lcom/mobeta/android/dslv/DragSortListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 58
    invoke-virtual {v0, v1}, Lcom/mobeta/android/dslv/DragSortListView;->setFloatViewManager(Lcom/mobeta/android/dslv/DragSortListView$FloatViewManager;)V

    return-void
.end method
