.class public final Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "AddSingleVariationWithOptionsBootstrapScreen.kt"


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001BY\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\n\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00050\n\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0008\u0010\u0016\u001a\u00020\u0017H\u0016R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00050\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "editItemScope",
        "Lcom/squareup/ui/items/EditItemScope;",
        "itemCogsId",
        "",
        "itemToken",
        "canSkipFetchingVariations",
        "",
        "assignedItemOptions",
        "",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
        "existingVariations",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        "locallyDeletedVariationTokens",
        "outputHandler",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;",
        "(Lcom/squareup/ui/items/EditItemScope;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;)V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope;",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final assignedItemOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation
.end field

.field private final canSkipFetchingVariations:Z

.field private final editItemScope:Lcom/squareup/ui/items/EditItemScope;

.field private final existingVariations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field private final itemCogsId:Ljava/lang/String;

.field private final itemToken:Ljava/lang/String;

.field private final locallyDeletedVariationTokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final outputHandler:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/EditItemScope;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditItemScope;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;",
            ")V"
        }
    .end annotation

    const-string v0, "editItemScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemCogsId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignedItemOptions"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "existingVariations"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locallyDeletedVariationTokens"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "outputHandler"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->editItemScope:Lcom/squareup/ui/items/EditItemScope;

    iput-object p2, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->itemCogsId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->itemToken:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->canSkipFetchingVariations:Z

    iput-object p5, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->assignedItemOptions:Ljava/util/List;

    iput-object p6, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->existingVariations:Ljava/util/List;

    iput-object p7, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->locallyDeletedVariationTokens:Ljava/util/List;

    iput-object p8, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->outputHandler:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 8

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    new-instance v0, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;

    .line 28
    iget-object v2, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->itemCogsId:Ljava/lang/String;

    .line 29
    iget-object v3, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->itemToken:Ljava/lang/String;

    .line 30
    iget-object v4, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->locallyDeletedVariationTokens:Ljava/util/List;

    .line 31
    iget-boolean v5, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->canSkipFetchingVariations:Z

    .line 32
    iget-object v6, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->assignedItemOptions:Ljava/util/List;

    .line 33
    iget-object v7, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->existingVariations:Ljava/util/List;

    move-object v1, v0

    .line 27
    invoke-direct/range {v1 .. v7}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLjava/util/List;Ljava/util/List;)V

    .line 35
    sget-object v1, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflowRunner;->Companion:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflowRunner$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflowRunner;

    move-result-object p1

    .line 38
    iget-object v1, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->outputHandler:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;

    .line 36
    invoke-interface {p1, v0, v1}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflowRunner;->start(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;)V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope;
    .locals 2

    .line 42
    new-instance v0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope;

    iget-object v1, p0, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->editItemScope:Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsBootstrapScreen;->getParentKey()Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope;

    move-result-object v0

    return-object v0
.end method
