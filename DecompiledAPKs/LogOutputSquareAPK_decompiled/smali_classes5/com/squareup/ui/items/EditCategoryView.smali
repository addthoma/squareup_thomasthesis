.class public Lcom/squareup/ui/items/EditCategoryView;
.super Lcom/squareup/widgets/PairLayout;
.source "EditCategoryView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/ui/items/BaseEditObjectView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;,
        Lcom/squareup/ui/items/EditCategoryView$CategoryNameChangeListener;
    }
.end annotation


# instance fields
.field private adapter:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

.field itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

.field presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/PairLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/ui/items/EditCategoryScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditCategoryScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/items/EditCategoryScreen$Component;->inject(Lcom/squareup/ui/items/EditCategoryView;)V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 90
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method initializeAdapter(ZZ)V
    .locals 1

    .line 73
    new-instance v0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;-><init>(Lcom/squareup/ui/items/EditCategoryView;ZZ)V

    iput-object v0, p0, Lcom/squareup/ui/items/EditCategoryView;->adapter:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    .line 74
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object p2, p0, Lcom/squareup/ui/items/EditCategoryView;->adapter:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->showConfirmDiscardDialogOrFinish()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 65
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 52
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onFinishInflate()V

    .line 54
    sget v0, Lcom/squareup/itemsapplet/R$id;->recycler:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/EditCategoryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditCategoryView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 56
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/ui/items/EditCategoryView;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryView;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public showMultiUnitContent()V
    .locals 5

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/items/EditCategoryView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v2}, Landroidx/recyclerview/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/ui/items/EditCategoryView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 81
    invoke-virtual {v3}, Landroidx/recyclerview/widget/RecyclerView;->getPaddingBottom()I

    move-result v3

    const/4 v4, 0x0

    .line 80
    invoke-virtual {v0, v1, v4, v2, v3}, Landroidx/recyclerview/widget/RecyclerView;->setPadding(IIII)V

    return-void
.end method

.method public updateCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView;->adapter:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->changeCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    return-void
.end method
