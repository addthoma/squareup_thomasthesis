.class public Lcom/squareup/ui/LocationActivityBackHandler$CancelActivityOnBackPressed;
.super Ljava/lang/Object;
.source "LocationActivityBackHandler.java"

# interfaces
.implements Lcom/squareup/ui/LocationActivityBackHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/LocationActivityBackHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CancelActivityOnBackPressed"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationActivityBackPressed(Lcom/squareup/ui/LocationActivity;)V
    .locals 1

    const/4 v0, 0x0

    .line 26
    invoke-virtual {p1, v0}, Lcom/squareup/ui/LocationActivity;->setResult(I)V

    .line 27
    invoke-virtual {p1}, Lcom/squareup/ui/LocationActivity;->finish()V

    return-void
.end method
