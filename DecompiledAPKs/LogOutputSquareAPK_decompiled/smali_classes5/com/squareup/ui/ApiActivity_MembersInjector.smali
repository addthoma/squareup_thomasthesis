.class public final Lcom/squareup/ui/ApiActivity_MembersInjector;
.super Ljava/lang/Object;
.source "ApiActivity_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/ApiActivity;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityResultHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final apiActivityControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiActivityController;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;"
        }
    .end annotation
.end field

.field private final configurationChangeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final contentViewInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final focusedActivityScannerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;"
        }
    .end annotation
.end field

.field private final internetStatusMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final locationMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final mediaButtonDisablerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;"
        }
    .end annotation
.end field

.field private final minesweeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;"
        }
    .end annotation
.end field

.field private final persistentBundleManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiActivityController;",
            ">;)V"
        }
    .end annotation

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p2, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->contentViewInitializerProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p3, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->locationMonitorProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p4, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->focusedActivityScannerProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p5, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p6, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->mediaButtonDisablerProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p7, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->minesweeperProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p8, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->internetStatusMonitorProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p9, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p10, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->persistentBundleManagerProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p11, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    .line 82
    iput-object p12, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->activityResultHandlerProvider:Ljavax/inject/Provider;

    .line 83
    iput-object p13, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->configurationChangeMonitorProvider:Ljavax/inject/Provider;

    .line 84
    iput-object p14, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->apiActivityControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiActivityController;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/ApiActivity;",
            ">;"
        }
    .end annotation

    .line 101
    new-instance v15, Lcom/squareup/ui/ApiActivity_MembersInjector;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/ui/ApiActivity_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v15
.end method

.method public static injectApiActivityController(Lcom/squareup/ui/ApiActivity;Lcom/squareup/api/ApiActivityController;)V
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/ui/ApiActivity;->apiActivityController:Lcom/squareup/api/ApiActivityController;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/ApiActivity;)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectAnalytics(Lcom/squareup/ui/SquareActivity;Lcom/squareup/analytics/Analytics;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->contentViewInitializerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/development/drawer/ContentViewInitializer;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectContentViewInitializer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/development/drawer/ContentViewInitializer;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->locationMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectLocationMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->focusedActivityScannerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/radiography/FocusedActivityScanner;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFocusedActivityScanner(Lcom/squareup/ui/SquareActivity;Lcom/squareup/radiography/FocusedActivityScanner;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectCardReaderPauseAndResumer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/cardreader/CardReaderPauseAndResumer;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->mediaButtonDisablerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/MediaButtonDisabler;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMediaButtonDisabler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/MediaButtonDisabler;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->minesweeperProvider:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMinesweeperProvider(Lcom/squareup/ui/SquareActivity;Ljavax/inject/Provider;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->internetStatusMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/internet/InternetStatusMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectInternetStatusMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/internet/InternetStatusMonitor;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFeatures(Lcom/squareup/ui/SquareActivity;Lcom/squareup/settings/server/Features;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->persistentBundleManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/persistentbundle/PersistentBundleManager;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectPersistentBundleManager(Lcom/squareup/ui/SquareActivity;Lcom/squareup/persistentbundle/PersistentBundleManager;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectDevice(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/Device;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->activityResultHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ActivityResultHandler;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectActivityResultHandler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/ActivityResultHandler;)V

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->configurationChangeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/AndroidConfigurationChangeMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectConfigurationChangeMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/AndroidConfigurationChangeMonitor;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity_MembersInjector;->apiActivityControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ApiActivityController;

    invoke-static {p1, v0}, Lcom/squareup/ui/ApiActivity_MembersInjector;->injectApiActivityController(Lcom/squareup/ui/ApiActivity;Lcom/squareup/api/ApiActivityController;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/ui/ApiActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ApiActivity_MembersInjector;->injectMembers(Lcom/squareup/ui/ApiActivity;)V

    return-void
.end method
