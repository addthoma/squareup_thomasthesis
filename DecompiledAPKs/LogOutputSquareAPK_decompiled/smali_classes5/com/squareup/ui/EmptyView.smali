.class public Lcom/squareup/ui/EmptyView;
.super Landroid/widget/LinearLayout;
.source "EmptyView.java"


# instance fields
.field private glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field private message:Lcom/squareup/widgets/MessageView;

.field private title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .line 31
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 32
    sget v0, Lcom/squareup/widgets/pos/R$id;->glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/EmptyView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 33
    sget v0, Lcom/squareup/widgets/pos/R$id;->empty_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/EmptyView;->title:Landroid/widget/TextView;

    .line 34
    sget v0, Lcom/squareup/widgets/pos/R$id;->empty_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/EmptyView;->message:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/EmptyView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method public setGlyphColor(I)V
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/EmptyView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p0}, Lcom/squareup/ui/EmptyView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    return-void
.end method

.method public setGlyphVisibility(I)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/EmptyView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    return-void
.end method

.method public setMessage(I)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/EmptyView;->message:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/EmptyView;->message:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setMessagePadding(IIII)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/EmptyView;->message:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/widgets/MessageView;->setPadding(IIII)V

    return-void
.end method

.method public setMessageVisibility(I)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/EmptyView;->message:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/EmptyView;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/EmptyView;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitleColorRes(I)V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/EmptyView;->title:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/squareup/ui/EmptyView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setTitleVisibility(I)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/EmptyView;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method
