.class public final Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;
.super Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;
.source "BuyerCartAdapterItem.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BranCartItem"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u000c\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00072\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u000cH\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u000cX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;",
        "displayItem",
        "Lcom/squareup/comms/protos/seller/DisplayItem;",
        "quantity",
        "",
        "showDiffHighlight",
        "",
        "(Lcom/squareup/comms/protos/seller/DisplayItem;Ljava/lang/String;Z)V",
        "getDisplayItem",
        "()Lcom/squareup/comms/protos/seller/DisplayItem;",
        "layoutId",
        "",
        "getLayoutId",
        "()I",
        "getQuantity",
        "()Ljava/lang/String;",
        "getShowDiffHighlight",
        "()Z",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "buyer-cart_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

.field private final layoutId:I

.field private final quantity:Ljava/lang/String;

.field private final showDiffHighlight:Z


# direct methods
.method public constructor <init>(Lcom/squareup/comms/protos/seller/DisplayItem;Ljava/lang/String;Z)V
    .locals 1

    const-string v0, "displayItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "quantity"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 64
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;-><init>(Lcom/squareup/comms/protos/seller/DisplayItem;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

    iput-object p2, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->quantity:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->showDiffHighlight:Z

    .line 65
    sget p1, Lcom/squareup/ui/buyercart/R$layout;->bran_cart_list_item:I

    iput p1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->layoutId:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;Lcom/squareup/comms/protos/seller/DisplayItem;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->getDisplayItem()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->getQuantity()Ljava/lang/String;

    move-result-object p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->showDiffHighlight:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->copy(Lcom/squareup/comms/protos/seller/DisplayItem;Ljava/lang/String;Z)Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/comms/protos/seller/DisplayItem;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->getDisplayItem()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->getQuantity()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->showDiffHighlight:Z

    return v0
.end method

.method public final copy(Lcom/squareup/comms/protos/seller/DisplayItem;Ljava/lang/String;Z)Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;
    .locals 1

    const-string v0, "displayItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "quantity"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;-><init>(Lcom/squareup/comms/protos/seller/DisplayItem;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;

    invoke-virtual {p0}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->getDisplayItem()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->getDisplayItem()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->getQuantity()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->getQuantity()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->showDiffHighlight:Z

    iget-boolean p1, p1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->showDiffHighlight:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getDisplayItem()Lcom/squareup/comms/protos/seller/DisplayItem;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->displayItem:Lcom/squareup/comms/protos/seller/DisplayItem;

    return-object v0
.end method

.method public getLayoutId()I
    .locals 1

    .line 65
    iget v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->layoutId:I

    return v0
.end method

.method public getQuantity()Ljava/lang/String;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->quantity:Ljava/lang/String;

    return-object v0
.end method

.method public final getShowDiffHighlight()Z
    .locals 1

    .line 63
    iget-boolean v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->showDiffHighlight:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->getDisplayItem()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->getQuantity()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->showDiffHighlight:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BranCartItem(displayItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->getDisplayItem()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->getQuantity()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", showDiffHighlight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->showDiffHighlight:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
