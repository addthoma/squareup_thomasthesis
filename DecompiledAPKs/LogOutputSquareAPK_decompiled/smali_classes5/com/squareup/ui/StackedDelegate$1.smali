.class Lcom/squareup/ui/StackedDelegate$1;
.super Ljava/lang/Object;
.source "StackedDelegate.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/StackedDelegate;->registerDelegate(Lmortar/MortarScope;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/StackedDelegate;

.field final synthetic val$delegate:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/squareup/ui/StackedDelegate;Ljava/lang/Object;)V
    .locals 0

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/StackedDelegate$1;->this$0:Lcom/squareup/ui/StackedDelegate;

    iput-object p2, p0, Lcom/squareup/ui/StackedDelegate$1;->val$delegate:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 25
    iget-object p1, p0, Lcom/squareup/ui/StackedDelegate$1;->this$0:Lcom/squareup/ui/StackedDelegate;

    iget-object v0, p0, Lcom/squareup/ui/StackedDelegate$1;->val$delegate:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/StackedDelegate;->registerDelegate(Ljava/lang/Object;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/StackedDelegate$1;->this$0:Lcom/squareup/ui/StackedDelegate;

    iget-object v1, p0, Lcom/squareup/ui/StackedDelegate$1;->val$delegate:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/StackedDelegate;->unregisterDelegate(Ljava/lang/Object;)V

    return-void
.end method
