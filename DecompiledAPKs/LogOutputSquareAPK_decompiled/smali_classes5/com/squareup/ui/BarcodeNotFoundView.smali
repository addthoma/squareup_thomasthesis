.class public Lcom/squareup/ui/BarcodeNotFoundView;
.super Landroid/widget/LinearLayout;
.source "BarcodeNotFoundView.java"


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private okButton:Lcom/squareup/noho/NohoButton;

.field presenter:Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private skuNotFoundTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    const-class p2, Lcom/squareup/ui/BarcodeNotFoundScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/BarcodeNotFoundScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/BarcodeNotFoundScreen$Component;->inject(Lcom/squareup/ui/BarcodeNotFoundView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 53
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/BarcodeNotFoundView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 54
    sget v0, Lcom/squareup/orderentry/R$id;->sku_not_found_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/BarcodeNotFoundView;->skuNotFoundTitle:Landroid/widget/TextView;

    .line 55
    sget v0, Lcom/squareup/orderentry/R$id;->sku_not_found_ok_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/ui/BarcodeNotFoundView;->okButton:Lcom/squareup/noho/NohoButton;

    return-void
.end method


# virtual methods
.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/BarcodeNotFoundView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 29
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 30
    invoke-direct {p0}, Lcom/squareup/ui/BarcodeNotFoundView;->bindViews()V

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/BarcodeNotFoundView;->presenter:Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/BarcodeNotFoundView;->presenter:Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 37
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public setOnClickListenerOnOkButton(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/BarcodeNotFoundView;->okButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/BarcodeNotFoundView;->skuNotFoundTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
