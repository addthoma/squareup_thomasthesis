.class abstract Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action;
.super Ljava/lang/Object;
.source "RealEnterAmountWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$CloseDebitCardRequiredDialog;,
        Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$FinishWith;,
        Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$Submit;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0003\u0007\u0008\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016\u0082\u0001\u0003\n\u000b\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountResult;",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "CloseDebitCardRequiredDialog",
        "FinishWith",
        "Submit",
        "Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$CloseDebitCardRequiredDialog;",
        "Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$FinishWith;",
        "Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$Submit;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 133
    invoke-direct {p0}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;",
            ">;)",
            "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountResult;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    sget-object v0, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$CloseDebitCardRequiredDialog;->INSTANCE:Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$CloseDebitCardRequiredDialog;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 137
    sget-object v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState$ShowingEnterAmount;->INSTANCE:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState$ShowingEnterAmount;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_1

    .line 140
    :cond_0
    instance-of v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$FinishWith;

    if-eqz v0, :cond_1

    move-object p1, p0

    check-cast p1, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$FinishWith;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$FinishWith;->getResult()Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountResult;

    move-result-object v1

    goto :goto_1

    .line 141
    :cond_1
    instance-of v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$Submit;

    if-eqz v0, :cond_3

    .line 143
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$Submit;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$Submit;->getCardInfo()Lcom/squareup/protos/client/deposits/CardInfo;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 144
    new-instance v1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountResult$SubmitAmount;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$Submit;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v0}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$Submit;->getCardInfo()Lcom/squareup/protos/client/deposits/CardInfo;

    move-result-object v0

    invoke-direct {v1, p1, v0}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountResult$SubmitAmount;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/CardInfo;)V

    goto :goto_0

    .line 146
    :cond_2
    sget-object v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState$ShowingDebitCardRequiredDialog;->INSTANCE:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState$ShowingDebitCardRequiredDialog;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    .line 143
    :goto_0
    check-cast v1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountResult;

    :goto_1
    return-object v1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 133
    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountResult;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountState;",
            "-",
            "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
