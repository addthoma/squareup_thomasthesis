.class public final Lcom/squareup/ui/balance/addmoney/AddMoneyScope;
.super Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;
.source "AddMoneyScope.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/addmoney/AddMoneyScope$ParentComponent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAddMoneyScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AddMoneyScope.kt\ncom/squareup/ui/balance/addmoney/AddMoneyScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,24:1\n35#2:25\n*E\n*S KotlinDebug\n*F\n+ 1 AddMoneyScope.kt\ncom/squareup/ui/balance/addmoney/AddMoneyScope\n*L\n11#1:25\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0001\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016R\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/AddMoneyScope;",
        "Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "ParentComponent",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyScope;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 8
    new-instance v0, Lcom/squareup/ui/balance/addmoney/AddMoneyScope;

    invoke-direct {v0}, Lcom/squareup/ui/balance/addmoney/AddMoneyScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/balance/addmoney/AddMoneyScope;->INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyScope;

    .line 22
    sget-object v0, Lcom/squareup/ui/balance/addmoney/AddMoneyScope;->INSTANCE:Lcom/squareup/ui/balance/addmoney/AddMoneyScope;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "PathCreator.forSingleton(AddMoneyScope)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/ui/balance/addmoney/AddMoneyScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-super {p0, p1}, Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 25
    const-class v1, Lcom/squareup/ui/balance/addmoney/AddMoneyScope$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/balance/addmoney/AddMoneyScope$ParentComponent;

    .line 12
    invoke-interface {p1}, Lcom/squareup/ui/balance/addmoney/AddMoneyScope$ParentComponent;->addMoneyWorkflowRunner()Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;

    move-result-object p1

    const-string v1, "scopeBuilder"

    .line 13
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    return-object v0
.end method
