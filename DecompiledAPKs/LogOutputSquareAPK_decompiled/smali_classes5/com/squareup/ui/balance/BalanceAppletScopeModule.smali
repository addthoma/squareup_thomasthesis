.class abstract Lcom/squareup/ui/balance/BalanceAppletScopeModule;
.super Ljava/lang/Object;
.source "BalanceAppletScopeModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/ui/balance/addmoney/AddMoneyModule;,
        Lcom/squareup/ui/balance/BalanceAppletScopeModule$BalanceModule;,
        Lcom/squareup/balance/squarecard/BalanceSquareCardModule;,
        Lcom/squareup/capital/flexloan/CapitalFlexLoanModule;,
        Lcom/squareup/ui/balance/BalanceAppletScopeModule$OrderSquareCardModule;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/BalanceAppletScopeModule$OrderSquareCardModule;,
        Lcom/squareup/ui/balance/BalanceAppletScopeModule$BalanceModule;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAppletMasterViewPresenter(Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/util/Res;Lcom/squareup/ui/balance/BalanceApplet;Lcom/squareup/applet/AppletSelection;)Lcom/squareup/applet/AppletMasterViewPresenter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 84
    new-instance p2, Lcom/squareup/applet/AppletMasterViewPresenter;

    sget v0, Lcom/squareup/balance/applet/impl/R$string;->titlecase_balance_applet_name:I

    .line 85
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/applet/AppletMasterViewPresenter;-><init>(Lcom/squareup/applet/ActionBarNavigationHelper;Ljava/lang/String;Lcom/squareup/applet/AppletSelection;)V

    return-object p2
.end method

.method static provideBalanceAppletSectionsListPresenter(Lflow/Flow;Lcom/squareup/ui/balance/BalanceApplet;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;Landroid/content/res/Resources;Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;)Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;
    .locals 15
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/ui/balance/BalanceApplet;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Ljava/util/Locale;",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;",
            "Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;",
            ")",
            "Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;"
        }
    .end annotation

    .line 71
    new-instance v14, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/balance/BalanceApplet;->getSections()Lcom/squareup/ui/balance/BalanceAppletSectionsList;

    move-result-object v1

    move-object v0, v14

    move-object v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;-><init>(Lcom/squareup/applet/AppletSectionsList;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;Landroid/content/res/Resources;Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;)V

    return-object v14
.end method


# virtual methods
.method abstract bindsAppletSectionsListPresenter(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;)Lcom/squareup/applet/AppletSectionsListPresenter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
