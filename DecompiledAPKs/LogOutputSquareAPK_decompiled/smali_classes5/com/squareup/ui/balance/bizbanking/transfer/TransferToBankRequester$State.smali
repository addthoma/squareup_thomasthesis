.class public final Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;
.super Ljava/lang/Object;
.source "TransferToBankRequester.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "State"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B=\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b\u0012\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\rJ\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\tH\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u000bH\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u000bH\u00c6\u0003JE\u0010\u0014\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u000bH\u00c6\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u000bH\u00d6\u0001R\u0010\u0010\u0008\u001a\u00020\t8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;",
        "",
        "transferSpeed",
        "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;",
        "transferAmount",
        "Lcom/squareup/protos/common/Money;",
        "fee",
        "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;",
        "depositStatus",
        "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;",
        "title",
        "",
        "message",
        "(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;Ljava/lang/String;Ljava/lang/String;)V",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final depositStatus:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

.field public final fee:Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

.field public final message:Ljava/lang/String;

.field public final title:Ljava/lang/String;

.field public final transferAmount:Lcom/squareup/protos/common/Money;

.field public final transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "transferSpeed"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transferAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fee"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositStatus"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferAmount:Lcom/squareup/protos/common/Money;

    iput-object p3, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->fee:Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    iput-object p4, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->depositStatus:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    iput-object p5, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->title:Ljava/lang/String;

    iput-object p6, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->message:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    .line 233
    sget-object p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;->NO_TRANSFER_SPEED:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    :cond_0
    move-object v1, p1

    and-int/lit8 p1, p7, 0x8

    if-eqz p1, :cond_1

    .line 236
    sget-object p4, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->BUILDING:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    :cond_1
    move-object v4, p4

    and-int/lit8 p1, p7, 0x10

    const-string p4, ""

    if-eqz p1, :cond_2

    move-object v5, p4

    goto :goto_0

    :cond_2
    move-object v5, p5

    :goto_0
    and-int/lit8 p1, p7, 0x20

    if-eqz p1, :cond_3

    move-object v6, p4

    goto :goto_1

    :cond_3
    move-object v6, p6

    :goto_1
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    .line 238
    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferAmount:Lcom/squareup/protos/common/Money;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->fee:Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->depositStatus:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->title:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->message:Ljava/lang/String;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->copy(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->fee:Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    return-object v0
.end method

.method public final component4()Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->depositStatus:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;
    .locals 8

    const-string/jumbo v0, "transferSpeed"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transferAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fee"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositStatus"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->fee:Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->fee:Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->depositStatus:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->depositStatus:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->message:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->message:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->fee:Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->depositStatus:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->title:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->message:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "State(transferSpeed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", transferAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", fee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->fee:Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", depositStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->depositStatus:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
