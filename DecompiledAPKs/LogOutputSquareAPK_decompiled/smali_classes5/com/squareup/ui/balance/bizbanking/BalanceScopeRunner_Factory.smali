.class public final Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;
.super Ljava/lang/Object;
.source "BalanceScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final arg10Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final arg11Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg12Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/RateFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg13Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final arg14Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg15Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final arg16Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/data/BalanceActivityRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final arg17Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final arg18Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg19Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
            ">;"
        }
    .end annotation
.end field

.field private final arg20Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final arg21Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            ">;"
        }
    .end annotation
.end field

.field private final arg22Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;",
            ">;"
        }
    .end annotation
.end field

.field private final arg23Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
            ">;"
        }
    .end annotation
.end field

.field private final arg24Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg25Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg26Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final arg9Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/RateFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/data/BalanceActivityRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 110
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg3Provider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 112
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg4Provider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 113
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg5Provider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 114
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg6Provider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 115
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg7Provider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 116
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg8Provider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 117
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg9Provider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 118
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg10Provider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 119
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg11Provider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 120
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg12Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 121
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg13Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 122
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg14Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 123
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg15Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 124
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg16Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 125
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg17Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 126
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg18Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 127
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg19Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 128
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg20Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 129
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg21Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 130
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg22Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 131
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg23Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 132
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg24Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 133
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg25Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 134
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg26Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;
    .locals 29
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/RateFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/data/BalanceActivityRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)",
            "Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    .line 158
    new-instance v28, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;

    move-object/from16 v0, v28

    invoke-direct/range {v0 .. v27}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v28
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;Lcom/squareup/ui/settings/SettingsAppletGateway;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/RateFormatter;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/balance/activity/data/BalanceActivityRepository;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/accountfreeze/AccountFreeze;Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;Ljavax/inject/Provider;)Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;
    .locals 29
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;",
            "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;",
            "Lcom/squareup/ui/settings/SettingsAppletGateway;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/RateFormatter;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            "Lcom/squareup/balance/activity/data/BalanceActivityRepository;",
            "Lcom/squareup/util/BrowserLauncher;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            "Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;",
            "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)",
            "Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    .line 170
    new-instance v28, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;

    move-object/from16 v0, v28

    invoke-direct/range {v0 .. v27}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;-><init>(Landroid/app/Application;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;Lcom/squareup/ui/settings/SettingsAppletGateway;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/RateFormatter;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/balance/activity/data/BalanceActivityRepository;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/accountfreeze/AccountFreeze;Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;Ljavax/inject/Provider;)V

    return-object v28
.end method


# virtual methods
.method public get()Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;
    .locals 29

    move-object/from16 v0, p0

    .line 139
    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/app/Application;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/balance/squarecard/SquareCardDataRequester;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/settings/SettingsAppletGateway;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg9Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Ljava/text/DateFormat;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg10Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Ljava/text/DateFormat;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg11Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg12Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/text/RateFormatter;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg13Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg14Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg15Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/instantdeposit/InstantDepositRunner;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg16Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/balance/activity/data/BalanceActivityRepository;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg17Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/util/BrowserLauncher;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg18Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg19Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg20Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg21Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/accountfreeze/AccountFreeze;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg22Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg23Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/balance/squarecard/ManageSquareCardSection;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg24Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg25Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;

    iget-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->arg26Provider:Ljavax/inject/Provider;

    move-object/from16 v28, v1

    invoke-static/range {v2 .. v28}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;Lcom/squareup/ui/settings/SettingsAppletGateway;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/RateFormatter;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/balance/activity/data/BalanceActivityRepository;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/accountfreeze/AccountFreeze;Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;Ljavax/inject/Provider;)Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->get()Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;

    move-result-object v0

    return-object v0
.end method
