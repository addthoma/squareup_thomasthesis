.class public final Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;
.super Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;
.source "RealCapitalFlexLoanSection.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u0001\u001bB7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0008\u0010\u0017\u001a\u00020\u0018H\u0016J\u0008\u0010\u0019\u001a\u00020\u001aH\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u00020\u0010X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0013\u001a\u0010\u0012\u000c\u0012\n \u0016*\u0004\u0018\u00010\u00150\u00150\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;",
        "capitalSectionAccess",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;",
        "capitalData",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;",
        "res",
        "Lcom/squareup/util/Res;",
        "capitalOfferFormatter",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;",
        "analytics",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;Lcom/squareup/util/Res;Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Lcom/squareup/util/Device;)V",
        "appletSectionListEntry",
        "Lcom/squareup/applet/AppletSectionsListEntry;",
        "getAppletSectionListEntry",
        "()Lcom/squareup/applet/AppletSectionsListEntry;",
        "flexStatus",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
        "kotlin.jvm.PlatformType",
        "getInitialScreen",
        "Lcom/squareup/ui/balance/bizbanking/capital/CapitalBootstrapScreen;",
        "logSectionClick",
        "",
        "ManageCapitalEntry",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

.field private final appletSectionListEntry:Lcom/squareup/applet/AppletSectionsListEntry;

.field private final device:Lcom/squareup/util/Device;

.field private final flexStatus:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/capital/flexloan/CapitalFlexLoanSectionAccess;Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;Lcom/squareup/util/Res;Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Lcom/squareup/util/Device;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "capitalSectionAccess"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capitalData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capitalOfferFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    check-cast p1, Lcom/squareup/applet/SectionAccess;

    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    iput-object p5, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    iput-object p6, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;->device:Lcom/squareup/util/Device;

    .line 30
    invoke-interface {p2}, Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;->fetchFlexLoanStatus()Lio/reactivex/Observable;

    move-result-object p1

    const/4 p2, 0x1

    .line 31
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lio/reactivex/observables/ConnectableObservable;->refCount()Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "capitalData.fetchFlexLoa\u2026play(1)\n      .refCount()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;->flexStatus:Lio/reactivex/Observable;

    .line 35
    new-instance p1, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;

    move-object v1, p0

    check-cast v1, Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;

    iget-object v3, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    iget-object v4, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;->flexStatus:Lio/reactivex/Observable;

    move-object v0, p1

    move-object v2, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;-><init>(Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;Lcom/squareup/util/Res;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Lio/reactivex/Observable;Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;)V

    check-cast p1, Lcom/squareup/applet/AppletSectionsListEntry;

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;->appletSectionListEntry:Lcom/squareup/applet/AppletSectionsListEntry;

    return-void
.end method


# virtual methods
.method public getAppletSectionListEntry()Lcom/squareup/applet/AppletSectionsListEntry;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;->appletSectionListEntry:Lcom/squareup/applet/AppletSectionsListEntry;

    return-object v0
.end method

.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;->getInitialScreen()Lcom/squareup/ui/balance/bizbanking/capital/CapitalBootstrapScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/balance/bizbanking/capital/CapitalBootstrapScreen;
    .locals 2

    .line 37
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/capital/CapitalBootstrapScreen;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;->device:Lcom/squareup/util/Device;

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/bizbanking/capital/CapitalBootstrapScreen;-><init>(Lcom/squareup/util/Device;)V

    return-object v0
.end method

.method public logSectionClick()V
    .locals 2

    .line 40
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;->getAppletSectionListEntry()Lcom/squareup/applet/AppletSectionsListEntry;

    move-result-object v0

    if-eqz v0, :cond_4

    check-cast v0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection$ManageCapitalEntry;->getDisplayedFlexStatus()Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    move-result-object v0

    .line 41
    instance-of v1, v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;

    if-eqz v1, :cond_0

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logClickCapitalActiveOfferRow()V

    goto :goto_0

    .line 43
    :cond_0
    instance-of v1, v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$CurrentPlan;

    if-eqz v1, :cond_1

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logClickCapitalActivePlanRow()V

    goto :goto_0

    .line 45
    :cond_1
    instance-of v1, v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$PreviousPlan;

    if-eqz v1, :cond_2

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logClickCapitalClosedPlanRow()V

    goto :goto_0

    .line 47
    :cond_2
    instance-of v0, v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$FinancingRequest;

    if-eqz v0, :cond_3

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/capital/RealCapitalFlexLoanSection;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logClickCapitalPendingApplicationRow()V

    :cond_3
    :goto_0
    return-void

    .line 40
    :cond_4
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.squareup.ui.balance.bizbanking.capital.RealCapitalFlexLoanSection.ManageCapitalEntry"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
