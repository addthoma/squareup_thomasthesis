.class public final Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen;
.super Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;
.source "InstantDepositResultScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;,
        Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;,
        Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInstantDepositResultScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InstantDepositResultScreen.kt\ncom/squareup/ui/balance/bizbanking/InstantDepositResultScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,59:1\n43#2:60\n*E\n*S KotlinDebug\n*F\n+ 1 InstantDepositResultScreen.kt\ncom/squareup/ui/balance/bizbanking/InstantDepositResultScreen\n*L\n34#1:60\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0007\u0018\u0000 \u000b2\u00020\u00012\u00020\u00022\u00020\u0003:\u0003\u000b\u000c\rB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen;",
        "Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "()V",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "",
        "Companion",
        "Runner",
        "ScreenData",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Companion;

.field public static final INSTANCE:Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen;->Companion:Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Companion;

    .line 26
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen;

    invoke-direct {v0}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen;

    .line 28
    sget-object v0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "forSingleton(INSTANCE)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    const-class v0, Lcom/squareup/ui/balance/BalanceAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/balance/BalanceAppletScope$Component;

    .line 35
    invoke-interface {p1}, Lcom/squareup/ui/balance/BalanceAppletScope$Component;->instantDepositResultCoordinator()Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;

    move-result-object p1

    const-string/jumbo v0, "view.component<BalanceAp\u2026epositResultCoordinator()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 31
    sget v0, Lcom/squareup/balance/applet/impl/R$layout;->square_card_instant_deposit_view:I

    return v0
.end method
