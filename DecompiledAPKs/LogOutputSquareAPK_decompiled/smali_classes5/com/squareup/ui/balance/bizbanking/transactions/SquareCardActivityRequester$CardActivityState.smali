.class public final Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;
.super Ljava/lang/Object;
.source "SquareCardActivityRequester.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CardActivityState"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B7\u0008\u0007\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u000f\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u00c6\u0003J9\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00182\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0006H\u00d6\u0001R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;",
        "",
        "status",
        "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;",
        "loadMoreStatus",
        "batchToken",
        "",
        "cardActivities",
        "",
        "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
        "(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;)V",
        "getBatchToken",
        "()Ljava/lang/String;",
        "getCardActivities",
        "()Ljava/util/List;",
        "getLoadMoreStatus",
        "()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;",
        "getStatus",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final batchToken:Ljava/lang/String;

.field private final cardActivities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final loadMoreStatus:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

.field private final status:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xf

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;)V
    .locals 7

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;)V
    .locals 7

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loadMoreStatus"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardActivities"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->status:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->loadMoreStatus:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    iput-object p3, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->batchToken:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->cardActivities:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    .line 230
    sget-object p1, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;->LOADING:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    .line 231
    sget-object p2, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;->CARD_ACTIVITY_LOADED:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    const/4 p3, 0x0

    .line 232
    check-cast p3, Ljava/lang/String;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    .line 233
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p4

    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->status:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->loadMoreStatus:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->batchToken:Ljava/lang/String;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->cardActivities:Ljava/util/List;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->copy(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->status:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    return-object v0
.end method

.method public final component2()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->loadMoreStatus:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->batchToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->cardActivities:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;)",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;"
        }
    .end annotation

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loadMoreStatus"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardActivities"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->status:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->status:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->loadMoreStatus:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->loadMoreStatus:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->batchToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->batchToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->cardActivities:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->cardActivities:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBatchToken()Ljava/lang/String;
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->batchToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getCardActivities()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;"
        }
    .end annotation

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->cardActivities:Ljava/util/List;

    return-object v0
.end method

.method public final getLoadMoreStatus()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->loadMoreStatus:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    return-object v0
.end method

.method public final getStatus()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;
    .locals 1

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->status:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->status:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->loadMoreStatus:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->batchToken:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->cardActivities:Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CardActivityState(status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->status:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", loadMoreStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->loadMoreStatus:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", batchToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->batchToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", cardActivities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->cardActivities:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
