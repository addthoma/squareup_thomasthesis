.class public final Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "ManageSquareCardWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardWorkflowViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner$ManageSquareCardWorkflowResultHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardWorkflowViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner$ManageSquareCardWorkflowResultHandler;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 30
    iput-object p4, p0, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardWorkflowViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner$ManageSquareCardWorkflowResultHandler;",
            ">;)",
            "Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/balance/squarecard/ManageSquareCardWorkflowViewFactory;Lcom/squareup/balance/squarecard/ManageSquareCardWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner$ManageSquareCardWorkflowResultHandler;)Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner;
    .locals 1

    .line 48
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner;-><init>(Lcom/squareup/balance/squarecard/ManageSquareCardWorkflowViewFactory;Lcom/squareup/balance/squarecard/ManageSquareCardWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner$ManageSquareCardWorkflowResultHandler;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner;
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/squarecard/ManageSquareCardWorkflowViewFactory;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/balance/squarecard/ManageSquareCardWorkflow;

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/PosContainer;

    iget-object v3, p0, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner$ManageSquareCardWorkflowResultHandler;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner_Factory;->newInstance(Lcom/squareup/balance/squarecard/ManageSquareCardWorkflowViewFactory;Lcom/squareup/balance/squarecard/ManageSquareCardWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner$ManageSquareCardWorkflowResultHandler;)Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner_Factory;->get()Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
