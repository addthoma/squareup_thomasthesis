.class public final Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsDetailBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "TransferReportsDetailBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u0012H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\u0004\u0018\u00010\nX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsDetailBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "device",
        "Lcom/squareup/util/Device;",
        "depositType",
        "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
        "settlementReport",
        "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
        "(Lcom/squareup/util/Device;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)V",
        "directionOverride",
        "Lflow/Direction;",
        "getDirectionOverride",
        "()Lflow/Direction;",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/transferreports/TransferReportsScope;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final depositType:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

.field private final directionOverride:Lflow/Direction;

.field private final settlementReport:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)V
    .locals 1

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsDetailBootstrapScreen;->depositType:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    iput-object p3, p0, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsDetailBootstrapScreen;->settlementReport:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    .line 28
    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    sget-object p1, Lflow/Direction;->REPLACE:Lflow/Direction;

    :goto_0
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsDetailBootstrapScreen;->directionOverride:Lflow/Direction;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "scope"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v2, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    .line 34
    new-instance v15, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    sget-object v4, Lcom/squareup/transferreports/TransferReportsLoader$State;->LOADING:Lcom/squareup/transferreports/TransferReportsLoader$State;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0xffe

    const/16 v18, 0x0

    move-object v3, v15

    move-object/from16 v19, v15

    move/from16 v15, v16

    move/from16 v16, v17

    move-object/from16 v17, v18

    invoke-direct/range {v3 .. v17}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 35
    iget-object v3, v0, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsDetailBootstrapScreen;->depositType:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    .line 36
    iget-object v4, v0, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsDetailBootstrapScreen;->settlementReport:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    move-object/from16 v6, v19

    .line 33
    invoke-direct {v2, v6, v3, v4, v5}, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;)V

    .line 39
    sget-object v3, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->Companion:Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Companion;

    check-cast v2, Lcom/squareup/transferreports/TransferReportsProps;

    invoke-virtual {v3, v1, v2}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Companion;->startWorkflow(Lmortar/MortarScope;Lcom/squareup/transferreports/TransferReportsProps;)V

    return-void
.end method

.method public getDirectionOverride()Lflow/Direction;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsDetailBootstrapScreen;->directionOverride:Lflow/Direction;

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/transferreports/TransferReportsScope;
    .locals 3

    .line 30
    new-instance v0, Lcom/squareup/transferreports/TransferReportsScope;

    sget-object v1, Lcom/squareup/ui/balance/BalanceAppletScope;->INSTANCE:Lcom/squareup/ui/balance/BalanceAppletScope;

    const-string v2, "BalanceAppletScope.INSTANCE"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/ui/main/RegisterTreeKey;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/transferreports/TransferReportsScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/lang/Class;)V

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsDetailBootstrapScreen;->getParentKey()Lcom/squareup/transferreports/TransferReportsScope;

    move-result-object v0

    return-object v0
.end method
