.class public final Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "BalanceTransactionsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBalanceTransactionsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BalanceTransactionsCoordinator.kt\ncom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 5 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 6 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt\n+ 7 BinderRowSpec.kt\ncom/squareup/cycler/BinderRowSpec\n+ 8 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n+ 9 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,270:1\n49#2:271\n50#2,3:277\n53#2:332\n599#3,4:272\n601#3:276\n310#4,3:280\n313#4,3:289\n342#4,5:295\n344#4,2:300\n347#4:304\n355#4,3:306\n358#4,3:315\n355#4,3:318\n358#4,3:327\n35#5,6:283\n35#5,6:309\n35#5,6:321\n32#6,3:292\n36#6:305\n24#7,2:302\n43#8,2:330\n1360#9:333\n1429#9,3:334\n*E\n*S KotlinDebug\n*F\n+ 1 BalanceTransactionsCoordinator.kt\ncom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator\n*L\n141#1:271\n141#1,3:277\n141#1:332\n141#1,4:272\n141#1:276\n141#1,3:280\n141#1,3:289\n141#1,5:295\n141#1,2:300\n141#1:304\n141#1,3:306\n141#1,3:315\n141#1,3:318\n141#1,3:327\n141#1,6:283\n141#1,6:309\n141#1,6:321\n141#1,3:292\n141#1:305\n141#1,2:302\n141#1,2:330\n219#1:333\n219#1,3:334\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u00011B?\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0008\u0008\u0001\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0010\u0010!\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0008\u0010\"\u001a\u00020\u001eH\u0002J\u0010\u0010#\u001a\u00020\u001e2\u0006\u0010$\u001a\u00020%H\u0002J\u0010\u0010&\u001a\u00020\u001e2\u0006\u0010\'\u001a\u00020(H\u0002J\u0008\u0010)\u001a\u00020\u001eH\u0002J*\u0010*\u001a\u00020\u001e2\u000e\u0010+\u001a\n\u0012\u0004\u0012\u00020-\u0018\u00010,2\u0008\u0010.\u001a\u0004\u0018\u00010/2\u0006\u00100\u001a\u00020%H\u0002R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00062"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;",
        "device",
        "Lcom/squareup/util/Device;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "timeFormatter",
        "Ljava/text/DateFormat;",
        "dayAndDateFormatter",
        "Lcom/squareup/text/DayAndDateFormatter;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/text/DayAndDateFormatter;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "emptyCardActivity",
        "Lcom/squareup/widgets/MessageView;",
        "errorCardActivity",
        "Lcom/squareup/noho/NohoMessageView;",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "spinner",
        "Landroid/widget/ProgressBar;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "configurePhoneActionBar",
        "configureTabletActionBar",
        "status",
        "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;",
        "onScreenData",
        "screenData",
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;",
        "setupRecycler",
        "updateRecyclerData",
        "transactions",
        "",
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;",
        "batchToken",
        "",
        "loadMoreStatus",
        "TransactionRow",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final dayAndDateFormatter:Lcom/squareup/text/DayAndDateFormatter;

.field private final device:Lcom/squareup/util/Device;

.field private emptyCardActivity:Lcom/squareup/widgets/MessageView;

.field private errorCardActivity:Lcom/squareup/noho/NohoMessageView;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final runner:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;

.field private spinner:Landroid/widget/ProgressBar;

.field private final timeFormatter:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/text/DayAndDateFormatter;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .param p4    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/TimeFormat;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/DayAndDateFormatter;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "timeFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dayAndDateFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;

    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->device:Lcom/squareup/util/Device;

    iput-object p3, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->timeFormatter:Ljava/text/DateFormat;

    iput-object p5, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->dayAndDateFormatter:Lcom/squareup/text/DayAndDateFormatter;

    iput-object p6, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-void
.end method

.method public static final synthetic access$getDayAndDateFormatter$p(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;)Lcom/squareup/text/DayAndDateFormatter;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->dayAndDateFormatter:Lcom/squareup/text/DayAndDateFormatter;

    return-object p0
.end method

.method public static final synthetic access$getMoneyFormatter$p(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$getTimeFormatter$p(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;)Ljava/text/DateFormat;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->timeFormatter:Ljava/text/DateFormat;

    return-object p0
.end method

.method public static final synthetic access$onScreenData(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->onScreenData(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 240
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->balance_spinner:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.balance_spinner)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->spinner:Landroid/widget/ProgressBar;

    .line 241
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transactions:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.transactions)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 242
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->balance_empty_card_activity:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.b\u2026ance_empty_card_activity)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->emptyCardActivity:Lcom/squareup/widgets/MessageView;

    .line 243
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->balance_error_card_activity:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.b\u2026ance_error_card_activity)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->errorCardActivity:Lcom/squareup/noho/NohoMessageView;

    .line 244
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    return-void
.end method

.method private final configurePhoneActionBar()V
    .locals 4

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 83
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 84
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/applet/impl/R$string;->card_spend:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 85
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$configurePhoneActionBar$1;

    invoke-direct {v3, p0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$configurePhoneActionBar$1;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 86
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final configureTabletActionBar(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;)V
    .locals 3

    .line 90
    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;->CARD_ACTIVITY_LOADED:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    const-string v1, "actionBar"

    if-eq p1, v0, :cond_2

    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;->COULD_NOT_LOAD_CARD_ACTIVITY:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 97
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    goto :goto_1

    .line 91
    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    .line 95
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p1, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 92
    :cond_4
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 93
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->card_spend:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    :goto_1
    return-void
.end method

.method private final onScreenData(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;)V
    .locals 9

    .line 102
    iget-object v0, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->cardActivityState:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->getStatus()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    move-result-object v0

    .line 104
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v1

    if-nez v1, :cond_0

    .line 105
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->configureTabletActionBar(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;)V

    .line 108
    :cond_0
    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "emptyCardActivity"

    const-string v4, "spinner"

    const-string v5, "recyclerView"

    const-string v6, "errorCardActivity"

    const/16 v7, 0x8

    if-eq v0, v1, :cond_e

    const/4 v8, 0x2

    if-eq v0, v8, :cond_7

    const/4 v8, 0x3

    if-eq v0, v8, :cond_1

    goto/16 :goto_0

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->spinner:Landroid/widget/ProgressBar;

    if-nez v0, :cond_2

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 127
    iget-object v0, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->transactions:Ljava/util/List;

    .line 128
    iget-object v4, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->cardActivityState:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    invoke-virtual {v4}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->getBatchToken()Ljava/lang/String;

    move-result-object v4

    .line 129
    iget-object v8, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->cardActivityState:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    invoke-virtual {v8}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->getLoadMoreStatus()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    move-result-object v8

    .line 126
    invoke-direct {p0, v0, v4, v8}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->updateRecyclerData(Ljava/util/List;Ljava/lang/String;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_3

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->emptyCardActivity:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    iget-object p1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->transactions:Ljava/util/List;

    if-eqz p1, :cond_5

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    :cond_5
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 133
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->errorCardActivity:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_6

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1, v7}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    goto :goto_0

    .line 116
    :cond_7
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->spinner:Landroid/widget/ProgressBar;

    if-nez p1, :cond_8

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p1, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 117
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_9

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p1, v7}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    .line 118
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->emptyCardActivity:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_a

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p1, v7}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 119
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->errorCardActivity:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_b

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    sget v0, Lcom/squareup/balance/applet/impl/R$string;->load_card_activity_error_message_title:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 120
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->errorCardActivity:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_c

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    sget v0, Lcom/squareup/balance/applet/impl/R$string;->load_card_activity_error_message_body:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    .line 121
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->errorCardActivity:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_d

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    goto :goto_0

    .line 110
    :cond_e
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_f

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    invoke-virtual {p1, v7}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->emptyCardActivity:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_10

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    invoke-virtual {p1, v7}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->errorCardActivity:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_11

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    invoke-virtual {p1, v7}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->spinner:Landroid/widget/ProgressBar;

    if-nez p1, :cond_12

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private final setupRecycler()V
    .locals 6

    .line 139
    new-instance v0, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v0}, Landroid/text/style/StrikethroughSpan;-><init>()V

    .line 141
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v2, :cond_0

    const-string v3, "recyclerView"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 271
    :cond_0
    sget-object v3, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 272
    invoke-virtual {v2}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 273
    new-instance v3, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v3}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 277
    invoke-virtual {v1}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 278
    invoke-virtual {v1}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 142
    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$1$1;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$1$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v3, v1}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 281
    new-instance v1, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v4, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$$special$$inlined$row$1;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-direct {v1, v4}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 145
    sget v4, Lcom/squareup/balance/applet/impl/R$layout;->date_row:I

    .line 283
    new-instance v5, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$1;

    invoke-direct {v5, v4, p0, v0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;Landroid/text/style/StrikethroughSpan;)V

    check-cast v5, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v1, v5}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 281
    check-cast v1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 280
    invoke-virtual {v3, v1}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 293
    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$$special$$inlined$nohoRow$1;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$$special$$inlined$nohoRow$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 296
    new-instance v4, Lcom/squareup/cycler/BinderRowSpec;

    .line 300
    sget-object v5, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$$special$$inlined$nohoRow$2;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$$special$$inlined$nohoRow$2;

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 296
    invoke-direct {v4, v5, v1}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 302
    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$2;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$2;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;Landroid/text/style/StrikethroughSpan;)V

    check-cast v1, Lkotlin/jvm/functions/Function3;

    invoke-virtual {v4, v1}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 178
    check-cast v4, Lcom/squareup/cycler/Recycler$RowSpec;

    const/16 v1, 0x8

    invoke-static {v4, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 295
    invoke-virtual {v3, v4}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 307
    new-instance v1, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v4, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$$special$$inlined$extraItem$1;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$$special$$inlined$extraItem$1;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-direct {v1, v4}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 182
    sget v4, Lcom/squareup/balance/applet/impl/R$layout;->transaction_load_more_error:I

    .line 309
    new-instance v5, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$3;

    invoke-direct {v5, v4, p0, v0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$3;-><init>(ILcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;Landroid/text/style/StrikethroughSpan;)V

    check-cast v5, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v1, v5}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 307
    check-cast v1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 306
    invoke-virtual {v3, v1}, Lcom/squareup/cycler/Recycler$Config;->extraItem(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 319
    new-instance v1, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v4, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$$special$$inlined$extraItem$2;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$$special$$inlined$extraItem$2;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-direct {v1, v4}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 194
    sget v4, Lcom/squareup/balance/applet/impl/R$layout;->transaction_load_more:I

    .line 321
    new-instance v5, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$4;

    invoke-direct {v5, v4, p0, v0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$4;-><init>(ILcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;Landroid/text/style/StrikethroughSpan;)V

    check-cast v5, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v1, v5}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 319
    check-cast v1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 318
    invoke-virtual {v3, v1}, Lcom/squareup/cycler/Recycler$Config;->extraItem(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 330
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/4 v1, 0x0

    .line 207
    invoke-virtual {v0, v1}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    .line 208
    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    .line 330
    invoke-virtual {v3, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 275
    invoke-virtual {v3, v2}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    return-void

    .line 272
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final updateRecyclerData(Ljava/util/List;Ljava/lang/String;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;",
            ")V"
        }
    .end annotation

    if-eqz p1, :cond_2

    .line 219
    check-cast p1, Ljava/lang/Iterable;

    .line 333
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 334
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 335
    check-cast v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;

    .line 220
    iget-boolean v2, v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->isDate:Z

    if-eqz v2, :cond_0

    .line 221
    new-instance v2, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$CardTransactionDateRow;

    iget-wide v3, v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->id:J

    iget-object v1, v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->date:Ljava/util/Date;

    invoke-direct {v2, v3, v4, v1}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$CardTransactionDateRow;-><init>(JLjava/util/Date;)V

    check-cast v2, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow;

    goto :goto_1

    .line 223
    :cond_0
    new-instance v2, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$CardTransactionRow;

    invoke-direct {v2, v1}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$CardTransactionRow;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;)V

    check-cast v2, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow;

    .line 224
    :goto_1
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 336
    :cond_1
    check-cast v0, Ljava/util/List;

    goto :goto_2

    .line 225
    :cond_2
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 227
    :goto_2
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez p1, :cond_3

    const-string v1, "recycler"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$updateRecyclerData$1;

    invoke-direct {v1, v0, p2, p3}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$updateRecyclerData$1;-><init>(Ljava/util/List;Ljava/lang/String;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->bindViews(Landroid/view/View;)V

    .line 73
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->setupRecycler()V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->configurePhoneActionBar()V

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;->balanceTransactionsScreenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 79
    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$attach$1;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
