.class final Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$1;
.super Ljava/lang/Object;
.source "TransferToBankRequester.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->deposit(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;",
        "state",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $idempotenceKey:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$1;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$1;->$idempotenceKey:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;",
            ">;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$1;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$1;->$idempotenceKey:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->access$deposit(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Ljava/lang/String;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$1;->apply(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
