.class public final Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;
.super Ljava/lang/Object;
.source "InstantDepositResultScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0016\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001BE\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\t\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\tH\u00c6\u0003J\t\u0010\u001a\u001a\u00020\tH\u00c6\u0003JI\u0010\u001b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u001c\u001a\u00020\t2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001e\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\rR\u0011\u0010\n\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0012R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\r\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;",
        "",
        "actionBarTitle",
        "",
        "glyphId",
        "",
        "title",
        "message",
        "showSquareCardUpsell",
        "",
        "showLearnMoreButton",
        "(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V",
        "getActionBarTitle",
        "()Ljava/lang/CharSequence;",
        "getGlyphId",
        "()I",
        "getMessage",
        "getShowLearnMoreButton",
        "()Z",
        "getShowSquareCardUpsell",
        "getTitle",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "hashCode",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBarTitle:Ljava/lang/CharSequence;

.field private final glyphId:I

.field private final message:Ljava/lang/CharSequence;

.field private final showLearnMoreButton:Z

.field private final showSquareCardUpsell:Z

.field private final title:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 9

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3e

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;-><init>(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;I)V
    .locals 9

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3c

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;-><init>(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;ILjava/lang/CharSequence;)V
    .locals 9

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x38

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;-><init>(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 9

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x30

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;-><init>(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 9

    const/4 v6, 0x0

    const/16 v7, 0x20

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;-><init>(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V
    .locals 1

    const-string v0, "actionBarTitle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->actionBarTitle:Ljava/lang/CharSequence;

    iput p2, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->glyphId:I

    iput-object p3, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->title:Ljava/lang/CharSequence;

    iput-object p4, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->message:Ljava/lang/CharSequence;

    iput-boolean p5, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->showSquareCardUpsell:Z

    iput-boolean p6, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->showLearnMoreButton:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_0

    const/4 p2, -0x1

    const/4 v2, -0x1

    goto :goto_0

    :cond_0
    move v2, p2

    :goto_0
    and-int/lit8 p2, p7, 0x4

    const/4 p8, 0x0

    if-eqz p2, :cond_1

    .line 41
    move-object p3, p8

    check-cast p3, Ljava/lang/CharSequence;

    :cond_1
    move-object v3, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_2

    .line 42
    move-object p4, p8

    check-cast p4, Ljava/lang/CharSequence;

    :cond_2
    move-object v4, p4

    and-int/lit8 p2, p7, 0x10

    const/4 p3, 0x0

    if-eqz p2, :cond_3

    const/4 v5, 0x0

    goto :goto_1

    :cond_3
    move v5, p5

    :goto_1
    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_4

    const/4 v6, 0x0

    goto :goto_2

    :cond_4
    move v6, p6

    :goto_2
    move-object v0, p0

    move-object v1, p1

    .line 44
    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;-><init>(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZILjava/lang/Object;)Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->actionBarTitle:Ljava/lang/CharSequence;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget p2, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->glyphId:I

    :cond_1
    move p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->title:Ljava/lang/CharSequence;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->message:Ljava/lang/CharSequence;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->showSquareCardUpsell:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->showLearnMoreButton:Z

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move p4, p8

    move-object p5, v0

    move-object p6, v1

    move p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->copy(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->actionBarTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->glyphId:I

    return v0
.end method

.method public final component3()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->title:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component4()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->message:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->showSquareCardUpsell:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->showLearnMoreButton:Z

    return v0
.end method

.method public final copy(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;
    .locals 8

    const-string v0, "actionBarTitle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;

    move-object v1, v0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;-><init>(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->actionBarTitle:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->actionBarTitle:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->glyphId:I

    iget v1, p1, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->glyphId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->title:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->title:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->message:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->message:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->showSquareCardUpsell:Z

    iget-boolean v1, p1, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->showSquareCardUpsell:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->showLearnMoreButton:Z

    iget-boolean p1, p1, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->showLearnMoreButton:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getActionBarTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->actionBarTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getGlyphId()I
    .locals 1

    .line 40
    iget v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->glyphId:I

    return v0
.end method

.method public final getMessage()Ljava/lang/CharSequence;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->message:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getShowLearnMoreButton()Z
    .locals 1

    .line 44
    iget-boolean v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->showLearnMoreButton:Z

    return v0
.end method

.method public final getShowSquareCardUpsell()Z
    .locals 1

    .line 43
    iget-boolean v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->showSquareCardUpsell:Z

    return v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->title:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->actionBarTitle:Ljava/lang/CharSequence;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->glyphId:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->title:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->message:Ljava/lang/CharSequence;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->showSquareCardUpsell:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->showLearnMoreButton:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ScreenData(actionBarTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->actionBarTitle:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", glyphId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->glyphId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->title:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->message:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", showSquareCardUpsell="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->showSquareCardUpsell:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showLearnMoreButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;->showLearnMoreButton:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
