.class public interface abstract Lcom/squareup/ui/balance/BalanceAppletScopeComponent;
.super Ljava/lang/Object;
.source "BalanceAppletScopeComponent.java"

# interfaces
.implements Lcom/squareup/ui/balance/BalanceAppletScope$Component;
.implements Lcom/squareup/debitcard/LinkDebitCardScope$ParentComponent;
.implements Lcom/squareup/banklinking/LinkBankAccountScope$ParentComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/balance/BalanceAppletScopeModule;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/BalanceAppletScopeComponent$ParentComponent;
    }
.end annotation
