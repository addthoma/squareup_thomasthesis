.class public final Lcom/squareup/ui/balance/unifiedactivity/UnifiedActionBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "UnifiedActionBootstrapScreen.kt"

# interfaces
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0008\u0010\u000c\u001a\u00020\rH\u0016R\u0018\u0010\u0004\u001a\u0006\u0012\u0002\u0008\u00030\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/balance/unifiedactivity/UnifiedActionBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "Lcom/squareup/container/layer/InSection;",
        "()V",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/balance/unifiedactivity/UnifiedActionBootstrapScreen;

.field private static final section:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActionBootstrapScreen;

    invoke-direct {v0}, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActionBootstrapScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActionBootstrapScreen;->INSTANCE:Lcom/squareup/ui/balance/unifiedactivity/UnifiedActionBootstrapScreen;

    .line 14
    const-class v0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;

    sput-object v0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActionBootstrapScreen;->section:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    sget-object v0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;->Companion:Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner$Companion;->startWorkflow(Lmortar/MortarScope;)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityScope;->INSTANCE:Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityScope;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 14
    sget-object v0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActionBootstrapScreen;->section:Ljava/lang/Class;

    return-object v0
.end method
