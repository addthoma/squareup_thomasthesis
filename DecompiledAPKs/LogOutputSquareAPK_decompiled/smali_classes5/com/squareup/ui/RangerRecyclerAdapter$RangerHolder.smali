.class public Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "RangerRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/RangerRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RangerHolder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/RangerRecyclerAdapter;


# direct methods
.method protected constructor <init>(Lcom/squareup/ui/RangerRecyclerAdapter;Landroid/view/ViewGroup;I)V
    .locals 1

    .line 18
    iput-object p1, p0, Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;->this$0:Lcom/squareup/ui/RangerRecyclerAdapter;

    .line 19
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method protected bindRow(Ljava/lang/Enum;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;II)V"
        }
    .end annotation

    return-void
.end method

.method protected final itemView()Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">()TV;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;->itemView:Landroid/view/View;

    return-object v0
.end method
