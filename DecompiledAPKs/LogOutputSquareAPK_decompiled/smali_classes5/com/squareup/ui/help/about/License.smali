.class public final enum Lcom/squareup/ui/help/about/License;
.super Ljava/lang/Enum;
.source "License.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/help/about/License;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0003\u0008\u00fd\u0002\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u001f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0008R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008j\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018j\u0002\u0008\u0019j\u0002\u0008\u001aj\u0002\u0008\u001bj\u0002\u0008\u001cj\u0002\u0008\u001dj\u0002\u0008\u001ej\u0002\u0008\u001fj\u0002\u0008 j\u0002\u0008!j\u0002\u0008\"j\u0002\u0008#j\u0002\u0008$j\u0002\u0008%j\u0002\u0008&j\u0002\u0008\'j\u0002\u0008(j\u0002\u0008)j\u0002\u0008*j\u0002\u0008+j\u0002\u0008,j\u0002\u0008-j\u0002\u0008.j\u0002\u0008/j\u0002\u00080j\u0002\u00081j\u0002\u00082j\u0002\u00083j\u0002\u00084j\u0002\u00085j\u0002\u00086j\u0002\u00087j\u0002\u00088j\u0002\u00089j\u0002\u0008:j\u0002\u0008;j\u0002\u0008<j\u0002\u0008=j\u0002\u0008>j\u0002\u0008?j\u0002\u0008@j\u0002\u0008Aj\u0002\u0008Bj\u0002\u0008Cj\u0002\u0008Dj\u0002\u0008Ej\u0002\u0008Fj\u0002\u0008Gj\u0002\u0008Hj\u0002\u0008Ij\u0002\u0008Jj\u0002\u0008Kj\u0002\u0008Lj\u0002\u0008Mj\u0002\u0008Nj\u0002\u0008Oj\u0002\u0008Pj\u0002\u0008Qj\u0002\u0008Rj\u0002\u0008Sj\u0002\u0008Tj\u0002\u0008Uj\u0002\u0008Vj\u0002\u0008Wj\u0002\u0008Xj\u0002\u0008Yj\u0002\u0008Zj\u0002\u0008[j\u0002\u0008\\j\u0002\u0008]j\u0002\u0008^j\u0002\u0008_j\u0002\u0008`j\u0002\u0008aj\u0002\u0008bj\u0002\u0008cj\u0002\u0008dj\u0002\u0008ej\u0002\u0008fj\u0002\u0008gj\u0002\u0008hj\u0002\u0008ij\u0002\u0008jj\u0002\u0008kj\u0002\u0008lj\u0002\u0008mj\u0002\u0008nj\u0002\u0008oj\u0002\u0008pj\u0002\u0008qj\u0002\u0008rj\u0002\u0008sj\u0002\u0008tj\u0002\u0008uj\u0002\u0008vj\u0002\u0008wj\u0002\u0008xj\u0002\u0008yj\u0002\u0008zj\u0002\u0008{j\u0002\u0008|j\u0002\u0008}j\u0002\u0008~j\u0002\u0008\u007fj\u0003\u0008\u0080\u0001j\u0003\u0008\u0081\u0001j\u0003\u0008\u0082\u0001j\u0003\u0008\u0083\u0001j\u0003\u0008\u0084\u0001j\u0003\u0008\u0085\u0001j\u0003\u0008\u0086\u0001j\u0003\u0008\u0087\u0001j\u0003\u0008\u0088\u0001j\u0003\u0008\u0089\u0001j\u0003\u0008\u008a\u0001j\u0003\u0008\u008b\u0001j\u0003\u0008\u008c\u0001j\u0003\u0008\u008d\u0001j\u0003\u0008\u008e\u0001j\u0003\u0008\u008f\u0001j\u0003\u0008\u0090\u0001j\u0003\u0008\u0091\u0001j\u0003\u0008\u0092\u0001j\u0003\u0008\u0093\u0001j\u0003\u0008\u0094\u0001j\u0003\u0008\u0095\u0001j\u0003\u0008\u0096\u0001j\u0003\u0008\u0097\u0001j\u0003\u0008\u0098\u0001j\u0003\u0008\u0099\u0001j\u0003\u0008\u009a\u0001j\u0003\u0008\u009b\u0001j\u0003\u0008\u009c\u0001j\u0003\u0008\u009d\u0001j\u0003\u0008\u009e\u0001j\u0003\u0008\u009f\u0001j\u0003\u0008\u00a0\u0001j\u0003\u0008\u00a1\u0001j\u0003\u0008\u00a2\u0001j\u0003\u0008\u00a3\u0001j\u0003\u0008\u00a4\u0001j\u0003\u0008\u00a5\u0001j\u0003\u0008\u00a6\u0001j\u0003\u0008\u00a7\u0001j\u0003\u0008\u00a8\u0001j\u0003\u0008\u00a9\u0001j\u0003\u0008\u00aa\u0001j\u0003\u0008\u00ab\u0001j\u0003\u0008\u00ac\u0001j\u0003\u0008\u00ad\u0001j\u0003\u0008\u00ae\u0001j\u0003\u0008\u00af\u0001j\u0003\u0008\u00b0\u0001j\u0003\u0008\u00b1\u0001j\u0003\u0008\u00b2\u0001j\u0003\u0008\u00b3\u0001j\u0003\u0008\u00b4\u0001j\u0003\u0008\u00b5\u0001j\u0003\u0008\u00b6\u0001j\u0003\u0008\u00b7\u0001j\u0003\u0008\u00b8\u0001j\u0003\u0008\u00b9\u0001j\u0003\u0008\u00ba\u0001j\u0003\u0008\u00bb\u0001j\u0003\u0008\u00bc\u0001j\u0003\u0008\u00bd\u0001j\u0003\u0008\u00be\u0001j\u0003\u0008\u00bf\u0001j\u0003\u0008\u00c0\u0001j\u0003\u0008\u00c1\u0001j\u0003\u0008\u00c2\u0001j\u0003\u0008\u00c3\u0001j\u0003\u0008\u00c4\u0001j\u0003\u0008\u00c5\u0001j\u0003\u0008\u00c6\u0001j\u0003\u0008\u00c7\u0001j\u0003\u0008\u00c8\u0001j\u0003\u0008\u00c9\u0001j\u0003\u0008\u00ca\u0001j\u0003\u0008\u00cb\u0001j\u0003\u0008\u00cc\u0001j\u0003\u0008\u00cd\u0001j\u0003\u0008\u00ce\u0001j\u0003\u0008\u00cf\u0001j\u0003\u0008\u00d0\u0001j\u0003\u0008\u00d1\u0001j\u0003\u0008\u00d2\u0001j\u0003\u0008\u00d3\u0001j\u0003\u0008\u00d4\u0001j\u0003\u0008\u00d5\u0001j\u0003\u0008\u00d6\u0001j\u0003\u0008\u00d7\u0001j\u0003\u0008\u00d8\u0001j\u0003\u0008\u00d9\u0001j\u0003\u0008\u00da\u0001j\u0003\u0008\u00db\u0001j\u0003\u0008\u00dc\u0001j\u0003\u0008\u00dd\u0001j\u0003\u0008\u00de\u0001j\u0003\u0008\u00df\u0001j\u0003\u0008\u00e0\u0001j\u0003\u0008\u00e1\u0001j\u0003\u0008\u00e2\u0001j\u0003\u0008\u00e3\u0001j\u0003\u0008\u00e4\u0001j\u0003\u0008\u00e5\u0001j\u0003\u0008\u00e6\u0001j\u0003\u0008\u00e7\u0001j\u0003\u0008\u00e8\u0001j\u0003\u0008\u00e9\u0001j\u0003\u0008\u00ea\u0001j\u0003\u0008\u00eb\u0001j\u0003\u0008\u00ec\u0001j\u0003\u0008\u00ed\u0001j\u0003\u0008\u00ee\u0001j\u0003\u0008\u00ef\u0001j\u0003\u0008\u00f0\u0001j\u0003\u0008\u00f1\u0001j\u0003\u0008\u00f2\u0001j\u0003\u0008\u00f3\u0001j\u0003\u0008\u00f4\u0001j\u0003\u0008\u00f5\u0001j\u0003\u0008\u00f6\u0001j\u0003\u0008\u00f7\u0001j\u0003\u0008\u00f8\u0001j\u0003\u0008\u00f9\u0001j\u0003\u0008\u00fa\u0001j\u0003\u0008\u00fb\u0001j\u0003\u0008\u00fc\u0001j\u0003\u0008\u00fd\u0001j\u0003\u0008\u00fe\u0001j\u0003\u0008\u00ff\u0001j\u0003\u0008\u0080\u0002j\u0003\u0008\u0081\u0002j\u0003\u0008\u0082\u0002j\u0003\u0008\u0083\u0002j\u0003\u0008\u0084\u0002j\u0003\u0008\u0085\u0002j\u0003\u0008\u0086\u0002j\u0003\u0008\u0087\u0002j\u0003\u0008\u0088\u0002j\u0003\u0008\u0089\u0002j\u0003\u0008\u008a\u0002j\u0003\u0008\u008b\u0002j\u0003\u0008\u008c\u0002j\u0003\u0008\u008d\u0002j\u0003\u0008\u008e\u0002j\u0003\u0008\u008f\u0002j\u0003\u0008\u0090\u0002j\u0003\u0008\u0091\u0002j\u0003\u0008\u0092\u0002j\u0003\u0008\u0093\u0002j\u0003\u0008\u0094\u0002j\u0003\u0008\u0095\u0002j\u0003\u0008\u0096\u0002j\u0003\u0008\u0097\u0002j\u0003\u0008\u0098\u0002j\u0003\u0008\u0099\u0002j\u0003\u0008\u009a\u0002j\u0003\u0008\u009b\u0002j\u0003\u0008\u009c\u0002j\u0003\u0008\u009d\u0002j\u0003\u0008\u009e\u0002j\u0003\u0008\u009f\u0002j\u0003\u0008\u00a0\u0002j\u0003\u0008\u00a1\u0002j\u0003\u0008\u00a2\u0002j\u0003\u0008\u00a3\u0002j\u0003\u0008\u00a4\u0002j\u0003\u0008\u00a5\u0002j\u0003\u0008\u00a6\u0002j\u0003\u0008\u00a7\u0002j\u0003\u0008\u00a8\u0002j\u0003\u0008\u00a9\u0002j\u0003\u0008\u00aa\u0002j\u0003\u0008\u00ab\u0002j\u0003\u0008\u00ac\u0002j\u0003\u0008\u00ad\u0002j\u0003\u0008\u00ae\u0002j\u0003\u0008\u00af\u0002j\u0003\u0008\u00b0\u0002j\u0003\u0008\u00b1\u0002j\u0003\u0008\u00b2\u0002j\u0003\u0008\u00b3\u0002j\u0003\u0008\u00b4\u0002j\u0003\u0008\u00b5\u0002j\u0003\u0008\u00b6\u0002j\u0003\u0008\u00b7\u0002j\u0003\u0008\u00b8\u0002j\u0003\u0008\u00b9\u0002j\u0003\u0008\u00ba\u0002j\u0003\u0008\u00bb\u0002j\u0003\u0008\u00bc\u0002j\u0003\u0008\u00bd\u0002j\u0003\u0008\u00be\u0002j\u0003\u0008\u00bf\u0002j\u0003\u0008\u00c0\u0002j\u0003\u0008\u00c1\u0002j\u0003\u0008\u00c2\u0002j\u0003\u0008\u00c3\u0002j\u0003\u0008\u00c4\u0002j\u0003\u0008\u00c5\u0002j\u0003\u0008\u00c6\u0002j\u0003\u0008\u00c7\u0002j\u0003\u0008\u00c8\u0002j\u0003\u0008\u00c9\u0002j\u0003\u0008\u00ca\u0002j\u0003\u0008\u00cb\u0002j\u0003\u0008\u00cc\u0002j\u0003\u0008\u00cd\u0002j\u0003\u0008\u00ce\u0002j\u0003\u0008\u00cf\u0002j\u0003\u0008\u00d0\u0002j\u0003\u0008\u00d1\u0002j\u0003\u0008\u00d2\u0002j\u0003\u0008\u00d3\u0002j\u0003\u0008\u00d4\u0002j\u0003\u0008\u00d5\u0002j\u0003\u0008\u00d6\u0002j\u0003\u0008\u00d7\u0002j\u0003\u0008\u00d8\u0002j\u0003\u0008\u00d9\u0002j\u0003\u0008\u00da\u0002j\u0003\u0008\u00db\u0002j\u0003\u0008\u00dc\u0002j\u0003\u0008\u00dd\u0002j\u0003\u0008\u00de\u0002j\u0003\u0008\u00df\u0002j\u0003\u0008\u00e0\u0002j\u0003\u0008\u00e1\u0002j\u0003\u0008\u00e2\u0002j\u0003\u0008\u00e3\u0002j\u0003\u0008\u00e4\u0002j\u0003\u0008\u00e5\u0002j\u0003\u0008\u00e6\u0002j\u0003\u0008\u00e7\u0002j\u0003\u0008\u00e8\u0002j\u0003\u0008\u00e9\u0002j\u0003\u0008\u00ea\u0002j\u0003\u0008\u00eb\u0002j\u0003\u0008\u00ec\u0002j\u0003\u0008\u00ed\u0002j\u0003\u0008\u00ee\u0002j\u0003\u0008\u00ef\u0002j\u0003\u0008\u00f0\u0002j\u0003\u0008\u00f1\u0002j\u0003\u0008\u00f2\u0002j\u0003\u0008\u00f3\u0002j\u0003\u0008\u00f4\u0002j\u0003\u0008\u00f5\u0002j\u0003\u0008\u00f6\u0002j\u0003\u0008\u00f7\u0002j\u0003\u0008\u00f8\u0002j\u0003\u0008\u00f9\u0002j\u0003\u0008\u00fa\u0002j\u0003\u0008\u00fb\u0002j\u0003\u0008\u00fc\u0002j\u0003\u0008\u00fd\u0002j\u0003\u0008\u00fe\u0002j\u0003\u0008\u00ff\u0002\u00a8\u0006\u0080\u0003"
    }
    d2 = {
        "Lcom/squareup/ui/help/about/License;",
        "",
        "id",
        "",
        "licenseName",
        "url",
        "(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
        "getId",
        "()Ljava/lang/String;",
        "getLicenseName",
        "getUrl",
        "ZERO_BSD",
        "AAL",
        "ABSTYLES",
        "ADOBE_2006",
        "ADOBE_GLYPH",
        "ADSL",
        "AFL_1_1",
        "AFL_1_2",
        "AFL_2_0",
        "AFL_2_1",
        "AFL_3_0",
        "AFMPARSE",
        "AGPL_1_0_ONLY",
        "AGPL_1_0_OR_LATER",
        "AGPL_3_0_ONLY",
        "AGPL_3_0_OR_LATER",
        "ALADDIN",
        "AMDPLPA",
        "AML",
        "AMPAS",
        "ANTLR_PD",
        "APACHE_1_0",
        "APACHE_1_1",
        "APACHE_2_0",
        "APAFML",
        "APL_1_0",
        "APSL_1_0",
        "APSL_1_1",
        "APSL_1_2",
        "APSL_2_0",
        "ARTISTIC_1_0_CL8",
        "ARTISTIC_1_0_PERL",
        "ARTISTIC_1_0",
        "ARTISTIC_2_0",
        "BAHYPH",
        "BARR",
        "BEERWARE",
        "BITTORRENT_1_0",
        "BITTORRENT_1_1",
        "BORCEUX",
        "BSD_1_CLAUSE",
        "BSD_2_CLAUSE_FREEBSD",
        "BSD_2_CLAUSE_NETBSD",
        "BSD_2_CLAUSE_PATENT",
        "BSD_2_CLAUSE",
        "BSD_3_CLAUSE_ATTRIBUTION",
        "BSD_3_CLAUSE_CLEAR",
        "BSD_3_CLAUSE_LBNL",
        "BSD_3_CLAUSE_NO_NUCLEAR_LICENSE_2014",
        "BSD_3_CLAUSE_NO_NUCLEAR_LICENSE",
        "BSD_3_CLAUSE_NO_NUCLEAR_WARRANTY",
        "BSD_3_CLAUSE",
        "BSD_4_CLAUSE_UC",
        "BSD_4_CLAUSE",
        "BSD_PROTECTION",
        "BSD_SOURCE_CODE",
        "BSL_1_0",
        "BZIP2_1_0_5",
        "BZIP2_1_0_6",
        "CALDERA",
        "CATOSL_1_1",
        "CC_BY_1_0",
        "CC_BY_2_0",
        "CC_BY_2_5",
        "CC_BY_3_0",
        "CC_BY_4_0",
        "CC_BY_NC_1_0",
        "CC_BY_NC_2_0",
        "CC_BY_NC_2_5",
        "CC_BY_NC_3_0",
        "CC_BY_NC_4_0",
        "CC_BY_NC_ND_1_0",
        "CC_BY_NC_ND_2_0",
        "CC_BY_NC_ND_2_5",
        "CC_BY_NC_ND_3_0",
        "CC_BY_NC_ND_4_0",
        "CC_BY_NC_SA_1_0",
        "CC_BY_NC_SA_2_0",
        "CC_BY_NC_SA_2_5",
        "CC_BY_NC_SA_3_0",
        "CC_BY_NC_SA_4_0",
        "CC_BY_ND_1_0",
        "CC_BY_ND_2_0",
        "CC_BY_ND_2_5",
        "CC_BY_ND_3_0",
        "CC_BY_ND_4_0",
        "CC_BY_SA_1_0",
        "CC_BY_SA_2_0",
        "CC_BY_SA_2_5",
        "CC_BY_SA_3_0",
        "CC_BY_SA_4_0",
        "CC0_1_0",
        "CDDL_1_0",
        "CDDL_1_1",
        "CDLA_PERMISSIVE_1_0",
        "CDLA_SHARING_1_0",
        "CECILL_1_0",
        "CECILL_1_1",
        "CECILL_2_0",
        "CECILL_2_1",
        "CECILL_B",
        "CECILL_C",
        "CLARTISTIC",
        "CNRI_JYTHON",
        "CNRI_PYTHON_GPL_COMPATIBLE",
        "CNRI_PYTHON",
        "CONDOR_1_1",
        "CPAL_1_0",
        "CPL_1_0",
        "CPOL_1_02",
        "CROSSWORD",
        "CRYSTALSTACKER",
        "CUA_OPL_1_0",
        "CUBE",
        "CURL",
        "D_FSL_1_0",
        "DIFFMARK",
        "DOC",
        "DOTSEQN",
        "DSDP",
        "DVIPDFM",
        "ECL_1_0",
        "ECL_2_0",
        "EFL_1_0",
        "EFL_2_0",
        "EGENIX",
        "ENTESSA",
        "EPL_1_0",
        "EPL_2_0",
        "ERLPL_1_1",
        "EUDATAGRID",
        "EUPL_1_0",
        "EUPL_1_1",
        "EUPL_1_2",
        "EUROSYM",
        "FAIR",
        "FRAMEWORX_1_0",
        "FREEIMAGE",
        "FSFAP",
        "FSFUL",
        "FSFULLR",
        "FTL",
        "GFDL_1_1_ONLY",
        "GFDL_1_1_OR_LATER",
        "GFDL_1_2_ONLY",
        "GFDL_1_2_OR_LATER",
        "GFDL_1_3_ONLY",
        "GFDL_1_3_OR_LATER",
        "GIFTWARE",
        "GL2PS",
        "GLIDE",
        "GLULXE",
        "GNUPLOT",
        "GPL_1_0_ONLY",
        "GPL_1_0_OR_LATER",
        "GPL_2_0_ONLY",
        "GPL_2_0_OR_LATER",
        "GPL_3_0_ONLY",
        "GPL_3_0_OR_LATER",
        "GSOAP_1_3B",
        "HASKELLREPORT",
        "HPND",
        "IBM_PIBS",
        "ICU",
        "IJG",
        "IMAGEMAGICK",
        "IMATIX",
        "IMLIB2",
        "INFO_ZIP",
        "INTEL_ACPI",
        "INTEL",
        "INTERBASE_1_0",
        "IPA",
        "IPL_1_0",
        "ISC",
        "JASPER_2_0",
        "JSON",
        "LAL_1_2",
        "LAL_1_3",
        "LATEX2E",
        "LEPTONICA",
        "LGPL_2_0_ONLY",
        "LGPL_2_0_OR_LATER",
        "LGPL_2_1_ONLY",
        "LGPL_2_1_OR_LATER",
        "LGPL_3_0_ONLY",
        "LGPL_3_0_OR_LATER",
        "LGPLLR",
        "LIBPNG",
        "LIBTIFF",
        "LILIQ_P_1_1",
        "LILIQ_R_1_1",
        "LILIQ_RPLUS_1_1",
        "LINUX_OPENIB",
        "LPL_1_0",
        "LPL_1_02",
        "LPPL_1_0",
        "LPPL_1_1",
        "LPPL_1_2",
        "LPPL_1_3A",
        "LPPL_1_3C",
        "MAKEINDEX",
        "MIROS",
        "MIT_0",
        "MIT_ADVERTISING",
        "MIT_CMU",
        "MIT_ENNA",
        "MIT_FEH",
        "MIT",
        "MITNFA",
        "MOTOSOTO",
        "MPICH2",
        "MPL_1_0",
        "MPL_1_1",
        "MPL_2_0_NO_COPYLEFT_EXCEPTION",
        "MPL_2_0",
        "MS_PL",
        "MS_RL",
        "MTLL",
        "MULTICS",
        "MUP",
        "NASA_1_3",
        "NAUMEN",
        "NBPL_1_0",
        "NCSA",
        "NET_SNMP",
        "NETCDF",
        "NEWSLETR",
        "NGPL",
        "NLOD_1_0",
        "NLPL",
        "NOKIA",
        "NOSL",
        "NOWEB",
        "NPL_1_0",
        "NPL_1_1",
        "NPOSL_3_0",
        "NRL",
        "NTP",
        "OCCT_PL",
        "OCLC_2_0",
        "ODBL_1_0",
        "OFL_1_0",
        "OFL_1_1",
        "OGTSL",
        "OLDAP_1_1",
        "OLDAP_1_2",
        "OLDAP_1_3",
        "OLDAP_1_4",
        "OLDAP_2_0_1",
        "OLDAP_2_0",
        "OLDAP_2_1",
        "OLDAP_2_2_1",
        "OLDAP_2_2_2",
        "OLDAP_2_2",
        "OLDAP_2_3",
        "OLDAP_2_4",
        "OLDAP_2_5",
        "OLDAP_2_6",
        "OLDAP_2_7",
        "OLDAP_2_8",
        "OML",
        "OPENSSL",
        "OPL_1_0",
        "OSET_PL_2_1",
        "OSL_1_0",
        "OSL_1_1",
        "OSL_2_0",
        "OSL_2_1",
        "OSL_3_0",
        "PDDL_1_0",
        "PHP_3_0",
        "PHP_3_01",
        "PLEXUS",
        "POSTGRESQL",
        "PSFRAG",
        "PSUTILS",
        "PYTHON_2_0",
        "QHULL",
        "QPL_1_0",
        "RDISC",
        "RHECOS_1_1",
        "RPL_1_1",
        "RPL_1_5",
        "RPSL_1_0",
        "RSA_MD",
        "RSCPL",
        "RUBY",
        "SAX_PD",
        "SAXPATH",
        "SCEA",
        "SENDMAIL",
        "SGI_B_1_0",
        "SGI_B_1_1",
        "SGI_B_2_0",
        "SIMPL_2_0",
        "SISSL_1_2",
        "SISSL",
        "SLEEPYCAT",
        "SMLNJ",
        "SMPPL",
        "SNIA",
        "SPENCER_86",
        "SPENCER_94",
        "SPENCER_99",
        "SPL_1_0",
        "SUGARCRM_1_1_3",
        "SWL",
        "TCL",
        "TCP_WRAPPERS",
        "TMATE",
        "TORQUE_1_1",
        "TOSL",
        "UNICODE_DFS_2015",
        "UNICODE_DFS_2016",
        "UNICODE_TOU",
        "UNLICENSE",
        "UPL_1_0",
        "VIM",
        "VOSTROM",
        "VSL_1_0",
        "W3C_19980720",
        "W3C_20150513",
        "W3C",
        "WATCOM_1_0",
        "WSUIPA",
        "WTFPL",
        "X11",
        "XEROX",
        "XFREE86_1_1",
        "XINETD",
        "XNET",
        "XPP",
        "XSKAT",
        "YPL_1_0",
        "YPL_1_1",
        "ZED",
        "ZEND_2_0",
        "ZIMBRA_1_3",
        "ZIMBRA_1_4",
        "ZLIB_ACKNOWLEDGEMENT",
        "ZLIB",
        "ZPL_1_1",
        "ZPL_2_0",
        "ZPL_2_1",
        "AGPL_1_0",
        "AGPL_3_0",
        "ECOS_2_0",
        "GFDL_1_1",
        "GFDL_1_2",
        "GFDL_1_3",
        "GPL_1_0_PLUS",
        "GPL_1_0",
        "GPL_2_0_PLUS",
        "GPL_2_0_WITH_AUTOCONF_EXCEPTION",
        "GPL_2_0_WITH_BISON_EXCEPTION",
        "GPL_2_0_WITH_CLASSPATH_EXCEPTION",
        "GPL_2_0_WITH_FONT_EXCEPTION",
        "GPL_2_0_WITH_GCC_EXCEPTION",
        "GPL_2_0",
        "GPL_3_0_PLUS",
        "GPL_3_0_WITH_AUTOCONF_EXCEPTION",
        "GPL_3_0_WITH_GCC_EXCEPTION",
        "GPL_3_0",
        "LGPL_2_0_PLUS",
        "LGPL_2_0",
        "LGPL_2_1_PLUS",
        "LGPL_2_1",
        "LGPL_3_0_PLUS",
        "LGPL_3_0",
        "NUNIT",
        "STANDARDML_NJ",
        "WXWINDOWS",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/help/about/License;

.field public static final enum AAL:Lcom/squareup/ui/help/about/License;

.field public static final enum ABSTYLES:Lcom/squareup/ui/help/about/License;

.field public static final enum ADOBE_2006:Lcom/squareup/ui/help/about/License;

.field public static final enum ADOBE_GLYPH:Lcom/squareup/ui/help/about/License;

.field public static final enum ADSL:Lcom/squareup/ui/help/about/License;

.field public static final enum AFL_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum AFL_1_2:Lcom/squareup/ui/help/about/License;

.field public static final enum AFL_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum AFL_2_1:Lcom/squareup/ui/help/about/License;

.field public static final enum AFL_3_0:Lcom/squareup/ui/help/about/License;

.field public static final enum AFMPARSE:Lcom/squareup/ui/help/about/License;

.field public static final enum AGPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum AGPL_1_0_ONLY:Lcom/squareup/ui/help/about/License;

.field public static final enum AGPL_1_0_OR_LATER:Lcom/squareup/ui/help/about/License;

.field public static final enum AGPL_3_0:Lcom/squareup/ui/help/about/License;

.field public static final enum AGPL_3_0_ONLY:Lcom/squareup/ui/help/about/License;

.field public static final enum AGPL_3_0_OR_LATER:Lcom/squareup/ui/help/about/License;

.field public static final enum ALADDIN:Lcom/squareup/ui/help/about/License;

.field public static final enum AMDPLPA:Lcom/squareup/ui/help/about/License;

.field public static final enum AML:Lcom/squareup/ui/help/about/License;

.field public static final enum AMPAS:Lcom/squareup/ui/help/about/License;

.field public static final enum ANTLR_PD:Lcom/squareup/ui/help/about/License;

.field public static final enum APACHE_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum APACHE_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum APACHE_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum APAFML:Lcom/squareup/ui/help/about/License;

.field public static final enum APL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum APSL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum APSL_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum APSL_1_2:Lcom/squareup/ui/help/about/License;

.field public static final enum APSL_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum ARTISTIC_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum ARTISTIC_1_0_CL8:Lcom/squareup/ui/help/about/License;

.field public static final enum ARTISTIC_1_0_PERL:Lcom/squareup/ui/help/about/License;

.field public static final enum ARTISTIC_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum BAHYPH:Lcom/squareup/ui/help/about/License;

.field public static final enum BARR:Lcom/squareup/ui/help/about/License;

.field public static final enum BEERWARE:Lcom/squareup/ui/help/about/License;

.field public static final enum BITTORRENT_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum BITTORRENT_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum BORCEUX:Lcom/squareup/ui/help/about/License;

.field public static final enum BSD_1_CLAUSE:Lcom/squareup/ui/help/about/License;

.field public static final enum BSD_2_CLAUSE:Lcom/squareup/ui/help/about/License;

.field public static final enum BSD_2_CLAUSE_FREEBSD:Lcom/squareup/ui/help/about/License;

.field public static final enum BSD_2_CLAUSE_NETBSD:Lcom/squareup/ui/help/about/License;

.field public static final enum BSD_2_CLAUSE_PATENT:Lcom/squareup/ui/help/about/License;

.field public static final enum BSD_3_CLAUSE:Lcom/squareup/ui/help/about/License;

.field public static final enum BSD_3_CLAUSE_ATTRIBUTION:Lcom/squareup/ui/help/about/License;

.field public static final enum BSD_3_CLAUSE_CLEAR:Lcom/squareup/ui/help/about/License;

.field public static final enum BSD_3_CLAUSE_LBNL:Lcom/squareup/ui/help/about/License;

.field public static final enum BSD_3_CLAUSE_NO_NUCLEAR_LICENSE:Lcom/squareup/ui/help/about/License;

.field public static final enum BSD_3_CLAUSE_NO_NUCLEAR_LICENSE_2014:Lcom/squareup/ui/help/about/License;

.field public static final enum BSD_3_CLAUSE_NO_NUCLEAR_WARRANTY:Lcom/squareup/ui/help/about/License;

.field public static final enum BSD_4_CLAUSE:Lcom/squareup/ui/help/about/License;

.field public static final enum BSD_4_CLAUSE_UC:Lcom/squareup/ui/help/about/License;

.field public static final enum BSD_PROTECTION:Lcom/squareup/ui/help/about/License;

.field public static final enum BSD_SOURCE_CODE:Lcom/squareup/ui/help/about/License;

.field public static final enum BSL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum BZIP2_1_0_5:Lcom/squareup/ui/help/about/License;

.field public static final enum BZIP2_1_0_6:Lcom/squareup/ui/help/about/License;

.field public static final enum CALDERA:Lcom/squareup/ui/help/about/License;

.field public static final enum CATOSL_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum CC0_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_2_5:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_3_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_4_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_NC_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_NC_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_NC_2_5:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_NC_3_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_NC_4_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_NC_ND_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_NC_ND_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_NC_ND_2_5:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_NC_ND_3_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_NC_ND_4_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_NC_SA_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_NC_SA_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_NC_SA_2_5:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_NC_SA_3_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_NC_SA_4_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_ND_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_ND_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_ND_2_5:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_ND_3_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_ND_4_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_SA_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_SA_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_SA_2_5:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_SA_3_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CC_BY_SA_4_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CDDL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CDDL_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum CDLA_PERMISSIVE_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CDLA_SHARING_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CECILL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CECILL_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum CECILL_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CECILL_2_1:Lcom/squareup/ui/help/about/License;

.field public static final enum CECILL_B:Lcom/squareup/ui/help/about/License;

.field public static final enum CECILL_C:Lcom/squareup/ui/help/about/License;

.field public static final enum CLARTISTIC:Lcom/squareup/ui/help/about/License;

.field public static final enum CNRI_JYTHON:Lcom/squareup/ui/help/about/License;

.field public static final enum CNRI_PYTHON:Lcom/squareup/ui/help/about/License;

.field public static final enum CNRI_PYTHON_GPL_COMPATIBLE:Lcom/squareup/ui/help/about/License;

.field public static final enum CONDOR_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum CPAL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CPOL_1_02:Lcom/squareup/ui/help/about/License;

.field public static final enum CROSSWORD:Lcom/squareup/ui/help/about/License;

.field public static final enum CRYSTALSTACKER:Lcom/squareup/ui/help/about/License;

.field public static final enum CUA_OPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum CUBE:Lcom/squareup/ui/help/about/License;

.field public static final enum CURL:Lcom/squareup/ui/help/about/License;

.field public static final enum DIFFMARK:Lcom/squareup/ui/help/about/License;

.field public static final enum DOC:Lcom/squareup/ui/help/about/License;

.field public static final enum DOTSEQN:Lcom/squareup/ui/help/about/License;

.field public static final enum DSDP:Lcom/squareup/ui/help/about/License;

.field public static final enum DVIPDFM:Lcom/squareup/ui/help/about/License;

.field public static final enum D_FSL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum ECL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum ECL_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum ECOS_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum EFL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum EFL_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum EGENIX:Lcom/squareup/ui/help/about/License;

.field public static final enum ENTESSA:Lcom/squareup/ui/help/about/License;

.field public static final enum EPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum EPL_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum ERLPL_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum EUDATAGRID:Lcom/squareup/ui/help/about/License;

.field public static final enum EUPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum EUPL_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum EUPL_1_2:Lcom/squareup/ui/help/about/License;

.field public static final enum EUROSYM:Lcom/squareup/ui/help/about/License;

.field public static final enum FAIR:Lcom/squareup/ui/help/about/License;

.field public static final enum FRAMEWORX_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum FREEIMAGE:Lcom/squareup/ui/help/about/License;

.field public static final enum FSFAP:Lcom/squareup/ui/help/about/License;

.field public static final enum FSFUL:Lcom/squareup/ui/help/about/License;

.field public static final enum FSFULLR:Lcom/squareup/ui/help/about/License;

.field public static final enum FTL:Lcom/squareup/ui/help/about/License;

.field public static final enum GFDL_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum GFDL_1_1_ONLY:Lcom/squareup/ui/help/about/License;

.field public static final enum GFDL_1_1_OR_LATER:Lcom/squareup/ui/help/about/License;

.field public static final enum GFDL_1_2:Lcom/squareup/ui/help/about/License;

.field public static final enum GFDL_1_2_ONLY:Lcom/squareup/ui/help/about/License;

.field public static final enum GFDL_1_2_OR_LATER:Lcom/squareup/ui/help/about/License;

.field public static final enum GFDL_1_3:Lcom/squareup/ui/help/about/License;

.field public static final enum GFDL_1_3_ONLY:Lcom/squareup/ui/help/about/License;

.field public static final enum GFDL_1_3_OR_LATER:Lcom/squareup/ui/help/about/License;

.field public static final enum GIFTWARE:Lcom/squareup/ui/help/about/License;

.field public static final enum GL2PS:Lcom/squareup/ui/help/about/License;

.field public static final enum GLIDE:Lcom/squareup/ui/help/about/License;

.field public static final enum GLULXE:Lcom/squareup/ui/help/about/License;

.field public static final enum GNUPLOT:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_1_0_ONLY:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_1_0_OR_LATER:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_1_0_PLUS:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_2_0_ONLY:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_2_0_OR_LATER:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_2_0_PLUS:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_2_0_WITH_AUTOCONF_EXCEPTION:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_2_0_WITH_BISON_EXCEPTION:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_2_0_WITH_CLASSPATH_EXCEPTION:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_2_0_WITH_FONT_EXCEPTION:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_2_0_WITH_GCC_EXCEPTION:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_3_0:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_3_0_ONLY:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_3_0_OR_LATER:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_3_0_PLUS:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_3_0_WITH_AUTOCONF_EXCEPTION:Lcom/squareup/ui/help/about/License;

.field public static final enum GPL_3_0_WITH_GCC_EXCEPTION:Lcom/squareup/ui/help/about/License;

.field public static final enum GSOAP_1_3B:Lcom/squareup/ui/help/about/License;

.field public static final enum HASKELLREPORT:Lcom/squareup/ui/help/about/License;

.field public static final enum HPND:Lcom/squareup/ui/help/about/License;

.field public static final enum IBM_PIBS:Lcom/squareup/ui/help/about/License;

.field public static final enum ICU:Lcom/squareup/ui/help/about/License;

.field public static final enum IJG:Lcom/squareup/ui/help/about/License;

.field public static final enum IMAGEMAGICK:Lcom/squareup/ui/help/about/License;

.field public static final enum IMATIX:Lcom/squareup/ui/help/about/License;

.field public static final enum IMLIB2:Lcom/squareup/ui/help/about/License;

.field public static final enum INFO_ZIP:Lcom/squareup/ui/help/about/License;

.field public static final enum INTEL:Lcom/squareup/ui/help/about/License;

.field public static final enum INTEL_ACPI:Lcom/squareup/ui/help/about/License;

.field public static final enum INTERBASE_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum IPA:Lcom/squareup/ui/help/about/License;

.field public static final enum IPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum ISC:Lcom/squareup/ui/help/about/License;

.field public static final enum JASPER_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum JSON:Lcom/squareup/ui/help/about/License;

.field public static final enum LAL_1_2:Lcom/squareup/ui/help/about/License;

.field public static final enum LAL_1_3:Lcom/squareup/ui/help/about/License;

.field public static final enum LATEX2E:Lcom/squareup/ui/help/about/License;

.field public static final enum LEPTONICA:Lcom/squareup/ui/help/about/License;

.field public static final enum LGPLLR:Lcom/squareup/ui/help/about/License;

.field public static final enum LGPL_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum LGPL_2_0_ONLY:Lcom/squareup/ui/help/about/License;

.field public static final enum LGPL_2_0_OR_LATER:Lcom/squareup/ui/help/about/License;

.field public static final enum LGPL_2_0_PLUS:Lcom/squareup/ui/help/about/License;

.field public static final enum LGPL_2_1:Lcom/squareup/ui/help/about/License;

.field public static final enum LGPL_2_1_ONLY:Lcom/squareup/ui/help/about/License;

.field public static final enum LGPL_2_1_OR_LATER:Lcom/squareup/ui/help/about/License;

.field public static final enum LGPL_2_1_PLUS:Lcom/squareup/ui/help/about/License;

.field public static final enum LGPL_3_0:Lcom/squareup/ui/help/about/License;

.field public static final enum LGPL_3_0_ONLY:Lcom/squareup/ui/help/about/License;

.field public static final enum LGPL_3_0_OR_LATER:Lcom/squareup/ui/help/about/License;

.field public static final enum LGPL_3_0_PLUS:Lcom/squareup/ui/help/about/License;

.field public static final enum LIBPNG:Lcom/squareup/ui/help/about/License;

.field public static final enum LIBTIFF:Lcom/squareup/ui/help/about/License;

.field public static final enum LILIQ_P_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum LILIQ_RPLUS_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum LILIQ_R_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum LINUX_OPENIB:Lcom/squareup/ui/help/about/License;

.field public static final enum LPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum LPL_1_02:Lcom/squareup/ui/help/about/License;

.field public static final enum LPPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum LPPL_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum LPPL_1_2:Lcom/squareup/ui/help/about/License;

.field public static final enum LPPL_1_3A:Lcom/squareup/ui/help/about/License;

.field public static final enum LPPL_1_3C:Lcom/squareup/ui/help/about/License;

.field public static final enum MAKEINDEX:Lcom/squareup/ui/help/about/License;

.field public static final enum MIROS:Lcom/squareup/ui/help/about/License;

.field public static final enum MIT:Lcom/squareup/ui/help/about/License;

.field public static final enum MITNFA:Lcom/squareup/ui/help/about/License;

.field public static final enum MIT_0:Lcom/squareup/ui/help/about/License;

.field public static final enum MIT_ADVERTISING:Lcom/squareup/ui/help/about/License;

.field public static final enum MIT_CMU:Lcom/squareup/ui/help/about/License;

.field public static final enum MIT_ENNA:Lcom/squareup/ui/help/about/License;

.field public static final enum MIT_FEH:Lcom/squareup/ui/help/about/License;

.field public static final enum MOTOSOTO:Lcom/squareup/ui/help/about/License;

.field public static final enum MPICH2:Lcom/squareup/ui/help/about/License;

.field public static final enum MPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum MPL_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum MPL_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum MPL_2_0_NO_COPYLEFT_EXCEPTION:Lcom/squareup/ui/help/about/License;

.field public static final enum MS_PL:Lcom/squareup/ui/help/about/License;

.field public static final enum MS_RL:Lcom/squareup/ui/help/about/License;

.field public static final enum MTLL:Lcom/squareup/ui/help/about/License;

.field public static final enum MULTICS:Lcom/squareup/ui/help/about/License;

.field public static final enum MUP:Lcom/squareup/ui/help/about/License;

.field public static final enum NASA_1_3:Lcom/squareup/ui/help/about/License;

.field public static final enum NAUMEN:Lcom/squareup/ui/help/about/License;

.field public static final enum NBPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum NCSA:Lcom/squareup/ui/help/about/License;

.field public static final enum NETCDF:Lcom/squareup/ui/help/about/License;

.field public static final enum NET_SNMP:Lcom/squareup/ui/help/about/License;

.field public static final enum NEWSLETR:Lcom/squareup/ui/help/about/License;

.field public static final enum NGPL:Lcom/squareup/ui/help/about/License;

.field public static final enum NLOD_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum NLPL:Lcom/squareup/ui/help/about/License;

.field public static final enum NOKIA:Lcom/squareup/ui/help/about/License;

.field public static final enum NOSL:Lcom/squareup/ui/help/about/License;

.field public static final enum NOWEB:Lcom/squareup/ui/help/about/License;

.field public static final enum NPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum NPL_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum NPOSL_3_0:Lcom/squareup/ui/help/about/License;

.field public static final enum NRL:Lcom/squareup/ui/help/about/License;

.field public static final enum NTP:Lcom/squareup/ui/help/about/License;

.field public static final enum NUNIT:Lcom/squareup/ui/help/about/License;

.field public static final enum OCCT_PL:Lcom/squareup/ui/help/about/License;

.field public static final enum OCLC_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum ODBL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum OFL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum OFL_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum OGTSL:Lcom/squareup/ui/help/about/License;

.field public static final enum OLDAP_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum OLDAP_1_2:Lcom/squareup/ui/help/about/License;

.field public static final enum OLDAP_1_3:Lcom/squareup/ui/help/about/License;

.field public static final enum OLDAP_1_4:Lcom/squareup/ui/help/about/License;

.field public static final enum OLDAP_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum OLDAP_2_0_1:Lcom/squareup/ui/help/about/License;

.field public static final enum OLDAP_2_1:Lcom/squareup/ui/help/about/License;

.field public static final enum OLDAP_2_2:Lcom/squareup/ui/help/about/License;

.field public static final enum OLDAP_2_2_1:Lcom/squareup/ui/help/about/License;

.field public static final enum OLDAP_2_2_2:Lcom/squareup/ui/help/about/License;

.field public static final enum OLDAP_2_3:Lcom/squareup/ui/help/about/License;

.field public static final enum OLDAP_2_4:Lcom/squareup/ui/help/about/License;

.field public static final enum OLDAP_2_5:Lcom/squareup/ui/help/about/License;

.field public static final enum OLDAP_2_6:Lcom/squareup/ui/help/about/License;

.field public static final enum OLDAP_2_7:Lcom/squareup/ui/help/about/License;

.field public static final enum OLDAP_2_8:Lcom/squareup/ui/help/about/License;

.field public static final enum OML:Lcom/squareup/ui/help/about/License;

.field public static final enum OPENSSL:Lcom/squareup/ui/help/about/License;

.field public static final enum OPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum OSET_PL_2_1:Lcom/squareup/ui/help/about/License;

.field public static final enum OSL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum OSL_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum OSL_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum OSL_2_1:Lcom/squareup/ui/help/about/License;

.field public static final enum OSL_3_0:Lcom/squareup/ui/help/about/License;

.field public static final enum PDDL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum PHP_3_0:Lcom/squareup/ui/help/about/License;

.field public static final enum PHP_3_01:Lcom/squareup/ui/help/about/License;

.field public static final enum PLEXUS:Lcom/squareup/ui/help/about/License;

.field public static final enum POSTGRESQL:Lcom/squareup/ui/help/about/License;

.field public static final enum PSFRAG:Lcom/squareup/ui/help/about/License;

.field public static final enum PSUTILS:Lcom/squareup/ui/help/about/License;

.field public static final enum PYTHON_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum QHULL:Lcom/squareup/ui/help/about/License;

.field public static final enum QPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum RDISC:Lcom/squareup/ui/help/about/License;

.field public static final enum RHECOS_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum RPL_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum RPL_1_5:Lcom/squareup/ui/help/about/License;

.field public static final enum RPSL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum RSA_MD:Lcom/squareup/ui/help/about/License;

.field public static final enum RSCPL:Lcom/squareup/ui/help/about/License;

.field public static final enum RUBY:Lcom/squareup/ui/help/about/License;

.field public static final enum SAXPATH:Lcom/squareup/ui/help/about/License;

.field public static final enum SAX_PD:Lcom/squareup/ui/help/about/License;

.field public static final enum SCEA:Lcom/squareup/ui/help/about/License;

.field public static final enum SENDMAIL:Lcom/squareup/ui/help/about/License;

.field public static final enum SGI_B_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum SGI_B_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum SGI_B_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum SIMPL_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum SISSL:Lcom/squareup/ui/help/about/License;

.field public static final enum SISSL_1_2:Lcom/squareup/ui/help/about/License;

.field public static final enum SLEEPYCAT:Lcom/squareup/ui/help/about/License;

.field public static final enum SMLNJ:Lcom/squareup/ui/help/about/License;

.field public static final enum SMPPL:Lcom/squareup/ui/help/about/License;

.field public static final enum SNIA:Lcom/squareup/ui/help/about/License;

.field public static final enum SPENCER_86:Lcom/squareup/ui/help/about/License;

.field public static final enum SPENCER_94:Lcom/squareup/ui/help/about/License;

.field public static final enum SPENCER_99:Lcom/squareup/ui/help/about/License;

.field public static final enum SPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum STANDARDML_NJ:Lcom/squareup/ui/help/about/License;

.field public static final enum SUGARCRM_1_1_3:Lcom/squareup/ui/help/about/License;

.field public static final enum SWL:Lcom/squareup/ui/help/about/License;

.field public static final enum TCL:Lcom/squareup/ui/help/about/License;

.field public static final enum TCP_WRAPPERS:Lcom/squareup/ui/help/about/License;

.field public static final enum TMATE:Lcom/squareup/ui/help/about/License;

.field public static final enum TORQUE_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum TOSL:Lcom/squareup/ui/help/about/License;

.field public static final enum UNICODE_DFS_2015:Lcom/squareup/ui/help/about/License;

.field public static final enum UNICODE_DFS_2016:Lcom/squareup/ui/help/about/License;

.field public static final enum UNICODE_TOU:Lcom/squareup/ui/help/about/License;

.field public static final enum UNLICENSE:Lcom/squareup/ui/help/about/License;

.field public static final enum UPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum VIM:Lcom/squareup/ui/help/about/License;

.field public static final enum VOSTROM:Lcom/squareup/ui/help/about/License;

.field public static final enum VSL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum W3C:Lcom/squareup/ui/help/about/License;

.field public static final enum W3C_19980720:Lcom/squareup/ui/help/about/License;

.field public static final enum W3C_20150513:Lcom/squareup/ui/help/about/License;

.field public static final enum WATCOM_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum WSUIPA:Lcom/squareup/ui/help/about/License;

.field public static final enum WTFPL:Lcom/squareup/ui/help/about/License;

.field public static final enum WXWINDOWS:Lcom/squareup/ui/help/about/License;

.field public static final enum X11:Lcom/squareup/ui/help/about/License;

.field public static final enum XEROX:Lcom/squareup/ui/help/about/License;

.field public static final enum XFREE86_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum XINETD:Lcom/squareup/ui/help/about/License;

.field public static final enum XNET:Lcom/squareup/ui/help/about/License;

.field public static final enum XPP:Lcom/squareup/ui/help/about/License;

.field public static final enum XSKAT:Lcom/squareup/ui/help/about/License;

.field public static final enum YPL_1_0:Lcom/squareup/ui/help/about/License;

.field public static final enum YPL_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum ZED:Lcom/squareup/ui/help/about/License;

.field public static final enum ZEND_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum ZERO_BSD:Lcom/squareup/ui/help/about/License;

.field public static final enum ZIMBRA_1_3:Lcom/squareup/ui/help/about/License;

.field public static final enum ZIMBRA_1_4:Lcom/squareup/ui/help/about/License;

.field public static final enum ZLIB:Lcom/squareup/ui/help/about/License;

.field public static final enum ZLIB_ACKNOWLEDGEMENT:Lcom/squareup/ui/help/about/License;

.field public static final enum ZPL_1_1:Lcom/squareup/ui/help/about/License;

.field public static final enum ZPL_2_0:Lcom/squareup/ui/help/about/License;

.field public static final enum ZPL_2_1:Lcom/squareup/ui/help/about/License;


# instance fields
.field private final id:Ljava/lang/String;

.field private final licenseName:Ljava/lang/String;

.field private final url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/16 v0, 0x175

    new-array v0, v0, [Lcom/squareup/ui/help/about/License;

    new-instance v7, Lcom/squareup/ui/help/about/License;

    const-string v2, "ZERO_BSD"

    const/4 v3, 0x0

    const-string v4, "0BSD"

    const-string v5, "BSD Zero Clause License"

    const-string v6, "http://landley.net/toybox/license.html"

    move-object v1, v7

    .line 13
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v7, Lcom/squareup/ui/help/about/License;->ZERO_BSD:Lcom/squareup/ui/help/about/License;

    const/4 v1, 0x0

    aput-object v7, v0, v1

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v9, "AAL"

    const/4 v10, 0x1

    const-string v11, "AAL"

    const-string v12, "Attribution Assurance License"

    const-string v13, "http://www.opensource.org/licenses/attribution"

    move-object v8, v1

    .line 18
    invoke-direct/range {v8 .. v13}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->AAL:Lcom/squareup/ui/help/about/License;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ABSTYLES"

    const/4 v5, 0x2

    const-string v6, "Abstyles"

    const-string v7, "Abstyles License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Abstyles"

    move-object v3, v1

    .line 23
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ABSTYLES:Lcom/squareup/ui/help/about/License;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ADOBE_2006"

    const/4 v5, 0x3

    const-string v6, "Adobe-2006"

    const-string v7, "Adobe Systems Incorporated Source Code License Agreement"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/AdobeLicense"

    move-object v3, v1

    .line 28
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ADOBE_2006:Lcom/squareup/ui/help/about/License;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ADOBE_GLYPH"

    const/4 v5, 0x4

    const-string v6, "Adobe-Glyph"

    const-string v7, "Adobe Glyph List License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/MIT#AdobeGlyph"

    move-object v3, v1

    .line 33
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ADOBE_GLYPH:Lcom/squareup/ui/help/about/License;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ADSL"

    const/4 v5, 0x5

    const-string v6, "ADSL"

    const-string v7, "Amazon Digital Services License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/AmazonDigitalServicesLicense"

    move-object v3, v1

    .line 38
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ADSL:Lcom/squareup/ui/help/about/License;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "AFL_1_1"

    const/4 v5, 0x6

    const-string v6, "AFL-1.1"

    const-string v7, "Academic Free License v1.1"

    const-string v8, "http://opensource.linux-mirror.org/licenses/afl-1.1.txt"

    move-object v3, v1

    .line 43
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->AFL_1_1:Lcom/squareup/ui/help/about/License;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "AFL_1_2"

    const/4 v5, 0x7

    const-string v6, "AFL-1.2"

    const-string v7, "Academic Free License v1.2"

    const-string v8, "http://opensource.linux-mirror.org/licenses/afl-1.2.txt"

    move-object v3, v1

    .line 48
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->AFL_1_2:Lcom/squareup/ui/help/about/License;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "AFL_2_0"

    const/16 v5, 0x8

    const-string v6, "AFL-2.0"

    const-string v7, "Academic Free License v2.0"

    const-string v8, "http://opensource.linux-mirror.org/licenses/afl-2.0.txt"

    move-object v3, v1

    .line 53
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->AFL_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "AFL_2_1"

    const/16 v5, 0x9

    const-string v6, "AFL-2.1"

    const-string v7, "Academic Free License v2.1"

    const-string v8, "http://opensource.linux-mirror.org/licenses/afl-2.1.txt"

    move-object v3, v1

    .line 58
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->AFL_2_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "AFL_3_0"

    const/16 v5, 0xa

    const-string v6, "AFL-3.0"

    const-string v7, "Academic Free License v3.0"

    const-string v8, "http://www.rosenlaw.com/AFL3.0.htm"

    move-object v3, v1

    .line 63
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->AFL_3_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "AFMPARSE"

    const/16 v5, 0xb

    const-string v6, "Afmparse"

    const-string v7, "Afmparse License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Afmparse"

    move-object v3, v1

    .line 68
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->AFMPARSE:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "AGPL_1_0_ONLY"

    const/16 v5, 0xc

    const-string v6, "AGPL-1.0-only"

    const-string v7, "Affero General Public License v1.0 only"

    const-string v8, "http://www.affero.org/oagpl.html"

    move-object v3, v1

    .line 73
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->AGPL_1_0_ONLY:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "AGPL_1_0_OR_LATER"

    const/16 v5, 0xd

    const-string v6, "AGPL-1.0-or-later"

    const-string v7, "Affero General Public License v1.0 or later"

    const-string v8, "http://www.affero.org/oagpl.html"

    move-object v3, v1

    .line 78
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->AGPL_1_0_OR_LATER:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "AGPL_3_0_ONLY"

    const/16 v5, 0xe

    const-string v6, "AGPL-3.0-only"

    const-string v7, "GNU Affero General Public License v3.0 only"

    const-string v8, "http://www.gnu.org/licenses/agpl.txt"

    move-object v3, v1

    .line 83
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->AGPL_3_0_ONLY:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "AGPL_3_0_OR_LATER"

    const/16 v5, 0xf

    const-string v6, "AGPL-3.0-or-later"

    const-string v7, "GNU Affero General Public License v3.0 or later"

    const-string v8, "http://www.gnu.org/licenses/agpl.txt"

    move-object v3, v1

    .line 88
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->AGPL_3_0_OR_LATER:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ALADDIN"

    const/16 v5, 0x10

    const-string v6, "Aladdin"

    const-string v7, "Aladdin Free Public License"

    const-string v8, "http://pages.cs.wisc.edu/~ghost/doc/AFPL/6.01/Public.htm"

    move-object v3, v1

    .line 93
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ALADDIN:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "AMDPLPA"

    const/16 v5, 0x11

    const-string v6, "AMDPLPA"

    const-string v7, "AMD\'s plpa_map.c License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/AMD_plpa_map_License"

    move-object v3, v1

    .line 98
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->AMDPLPA:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "AML"

    const/16 v5, 0x12

    const-string v6, "AML"

    const-string v7, "Apple MIT License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Apple_MIT_License"

    move-object v3, v1

    .line 103
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->AML:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "AMPAS"

    const/16 v5, 0x13

    const-string v6, "AMPAS"

    const-string v7, "Academy of Motion Picture Arts and Sciences BSD"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/BSD#AMPASBSD"

    move-object v3, v1

    .line 108
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->AMPAS:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ANTLR_PD"

    const/16 v5, 0x14

    const-string v6, "ANTLR-PD"

    const-string v7, "ANTLR Software Rights Notice"

    const-string v8, "http://www.antlr2.org/license.html"

    move-object v3, v1

    .line 113
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ANTLR_PD:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "APACHE_1_0"

    const/16 v5, 0x15

    const-string v6, "Apache-1.0"

    const-string v7, "Apache License 1.0"

    const-string v8, "http://www.apache.org/licenses/LICENSE-1.0"

    move-object v3, v1

    .line 118
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->APACHE_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "APACHE_1_1"

    const/16 v5, 0x16

    const-string v6, "Apache-1.1"

    const-string v7, "Apache License 1.1"

    const-string v8, "http://apache.org/licenses/LICENSE-1.1"

    move-object v3, v1

    .line 123
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->APACHE_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "APACHE_2_0"

    const/16 v5, 0x17

    const-string v6, "Apache-2.0"

    const-string v7, "Apache License 2.0"

    const-string v8, "http://www.apache.org/licenses/LICENSE-2.0"

    move-object v3, v1

    .line 128
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "APAFML"

    const/16 v5, 0x18

    const-string v6, "APAFML"

    const-string v7, "Adobe Postscript AFM License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/AdobePostscriptAFM"

    move-object v3, v1

    .line 133
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->APAFML:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "APL_1_0"

    const/16 v5, 0x19

    const-string v6, "APL-1.0"

    const-string v7, "Adaptive Public License 1.0"

    const-string v8, "http://www.opensource.org/licenses/APL-1.0"

    move-object v3, v1

    .line 138
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->APL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "APSL_1_0"

    const/16 v5, 0x1a

    const-string v6, "APSL-1.0"

    const-string v7, "Apple Public Source License 1.0"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Apple_Public_Source_License_1.0"

    move-object v3, v1

    .line 143
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->APSL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "APSL_1_1"

    const/16 v5, 0x1b

    const-string v6, "APSL-1.1"

    const-string v7, "Apple Public Source License 1.1"

    const-string v8, "http://www.opensource.apple.com/source/IOSerialFamily/IOSerialFamily-7/APPLE_LICENSE"

    move-object v3, v1

    .line 148
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->APSL_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "APSL_1_2"

    const/16 v5, 0x1c

    const-string v6, "APSL-1.2"

    const-string v7, "Apple Public Source License 1.2"

    const-string v8, "http://www.samurajdata.se/opensource/mirror/licenses/apsl.php"

    move-object v3, v1

    .line 153
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->APSL_1_2:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "APSL_2_0"

    const/16 v5, 0x1d

    const-string v6, "APSL-2.0"

    const-string v7, "Apple Public Source License 2.0"

    const-string v8, "http://www.opensource.apple.com/license/apsl/"

    move-object v3, v1

    .line 158
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->APSL_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ARTISTIC_1_0_CL8"

    const/16 v5, 0x1e

    const-string v6, "Artistic-1.0-cl8"

    const-string v7, "Artistic License 1.0 w/clause 8"

    const-string v8, "http://opensource.org/licenses/Artistic-1.0"

    move-object v3, v1

    .line 163
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ARTISTIC_1_0_CL8:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ARTISTIC_1_0_PERL"

    const/16 v5, 0x1f

    const-string v6, "Artistic-1.0-Perl"

    const-string v7, "Artistic License 1.0 (Perl)"

    const-string v8, "http://dev.perl.org/licenses/artistic.html"

    move-object v3, v1

    .line 168
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ARTISTIC_1_0_PERL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ARTISTIC_1_0"

    const/16 v5, 0x20

    const-string v6, "Artistic-1.0"

    const-string v7, "Artistic License 1.0"

    const-string v8, "http://opensource.org/licenses/Artistic-1.0"

    move-object v3, v1

    .line 173
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ARTISTIC_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ARTISTIC_2_0"

    const/16 v5, 0x21

    const-string v6, "Artistic-2.0"

    const-string v7, "Artistic License 2.0"

    const-string v8, "http://www.perlfoundation.org/artistic_license_2_0"

    move-object v3, v1

    .line 178
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ARTISTIC_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BAHYPH"

    const/16 v5, 0x22

    const-string v6, "Bahyph"

    const-string v7, "Bahyph License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Bahyph"

    move-object v3, v1

    .line 183
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BAHYPH:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BARR"

    const/16 v5, 0x23

    const-string v6, "Barr"

    const-string v7, "Barr License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Barr"

    move-object v3, v1

    .line 188
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BARR:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BEERWARE"

    const/16 v5, 0x24

    const-string v6, "Beerware"

    const-string v7, "Beerware License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Beerware"

    move-object v3, v1

    .line 193
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BEERWARE:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BITTORRENT_1_0"

    const/16 v5, 0x25

    const-string v6, "BitTorrent-1.0"

    const-string v7, "BitTorrent Open Source License v1.0"

    const-string v8, "http://sources.gentoo.org/cgi-bin/viewvc.cgi/gentoo-x86/licenses/BitTorrent?r1=1.1&r2=1.1.1.1&diff_format=s"

    move-object v3, v1

    .line 198
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BITTORRENT_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BITTORRENT_1_1"

    const/16 v5, 0x26

    const-string v6, "BitTorrent-1.1"

    const-string v7, "BitTorrent Open Source License v1.1"

    const-string v8, "http://directory.fsf.org/wiki/License:BitTorrentOSL1.1"

    move-object v3, v1

    .line 203
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BITTORRENT_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BORCEUX"

    const/16 v5, 0x27

    const-string v6, "Borceux"

    const-string v7, "Borceux license"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Borceux"

    move-object v3, v1

    .line 208
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BORCEUX:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSD_1_CLAUSE"

    const/16 v5, 0x28

    const-string v6, "BSD-1-Clause"

    const-string v7, "BSD 1-Clause License"

    const-string v8, "https://svnweb.freebsd.org/base/head/include/ifaddrs.h?revision=326823"

    move-object v3, v1

    .line 213
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSD_1_CLAUSE:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSD_2_CLAUSE_FREEBSD"

    const/16 v5, 0x29

    const-string v6, "BSD-2-Clause-FreeBSD"

    const-string v7, "BSD 2-Clause FreeBSD License"

    const-string v8, "http://www.freebsd.org/copyright/freebsd-license.html"

    move-object v3, v1

    .line 218
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSD_2_CLAUSE_FREEBSD:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSD_2_CLAUSE_NETBSD"

    const/16 v5, 0x2a

    const-string v6, "BSD-2-Clause-NetBSD"

    const-string v7, "BSD 2-Clause NetBSD License"

    const-string v8, "http://www.netbsd.org/about/redistribution.html#default"

    move-object v3, v1

    .line 223
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSD_2_CLAUSE_NETBSD:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSD_2_CLAUSE_PATENT"

    const/16 v5, 0x2b

    const-string v6, "BSD-2-Clause-Patent"

    const-string v7, "BSD-2-Clause Plus Patent License"

    const-string v8, "https://opensource.org/licenses/BSDplusPatent"

    move-object v3, v1

    .line 228
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSD_2_CLAUSE_PATENT:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSD_2_CLAUSE"

    const/16 v5, 0x2c

    const-string v6, "BSD-2-Clause"

    const-string v7, "BSD 2-Clause \"Simplified\" License"

    const-string v8, "http://www.opensource.org/licenses/BSD-2-Clause"

    move-object v3, v1

    .line 233
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSD_2_CLAUSE:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSD_3_CLAUSE_ATTRIBUTION"

    const/16 v5, 0x2d

    const-string v6, "BSD-3-Clause-Attribution"

    const-string v7, "BSD with attribution"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/BSD_with_Attribution"

    move-object v3, v1

    .line 238
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSD_3_CLAUSE_ATTRIBUTION:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSD_3_CLAUSE_CLEAR"

    const/16 v5, 0x2e

    const-string v6, "BSD-3-Clause-Clear"

    const-string v7, "BSD 3-Clause Clear License"

    const-string v8, "http://labs.metacarta.com/license-explanation.html#license"

    move-object v3, v1

    .line 243
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSD_3_CLAUSE_CLEAR:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSD_3_CLAUSE_LBNL"

    const/16 v5, 0x2f

    const-string v6, "BSD-3-Clause-LBNL"

    const-string v7, "Lawrence Berkeley National Labs BSD variant license"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/LBNLBSD"

    move-object v3, v1

    .line 248
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSD_3_CLAUSE_LBNL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSD_3_CLAUSE_NO_NUCLEAR_LICENSE_2014"

    const/16 v5, 0x30

    const-string v6, "BSD-3-Clause-No-Nuclear-License-2014"

    const-string v7, "BSD 3-Clause No Nuclear License 2014"

    const-string v8, "https://java.net/projects/javaeetutorial/pages/BerkeleyLicense"

    move-object v3, v1

    .line 253
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSD_3_CLAUSE_NO_NUCLEAR_LICENSE_2014:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSD_3_CLAUSE_NO_NUCLEAR_LICENSE"

    const/16 v5, 0x31

    const-string v6, "BSD-3-Clause-No-Nuclear-License"

    const-string v7, "BSD 3-Clause No Nuclear License"

    const-string v8, "http://download.oracle.com/otn-pub/java/licenses/bsd.txt?AuthParam=1467140197_43d516ce1776bd08a58235a7785be1cc"

    move-object v3, v1

    .line 258
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSD_3_CLAUSE_NO_NUCLEAR_LICENSE:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSD_3_CLAUSE_NO_NUCLEAR_WARRANTY"

    const/16 v5, 0x32

    const-string v6, "BSD-3-Clause-No-Nuclear-Warranty"

    const-string v7, "BSD 3-Clause No Nuclear Warranty"

    const-string v8, "https://jogamp.org/git/?p=gluegen.git;a=blob_plain;f=LICENSE.txt"

    move-object v3, v1

    .line 263
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSD_3_CLAUSE_NO_NUCLEAR_WARRANTY:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSD_3_CLAUSE"

    const/16 v5, 0x33

    const-string v6, "BSD-3-Clause"

    const-string v7, "BSD 3-Clause \"New\" or \"Revised\" License"

    const-string v8, "http://www.opensource.org/licenses/BSD-3-Clause"

    move-object v3, v1

    .line 268
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSD_3_CLAUSE:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSD_4_CLAUSE_UC"

    const/16 v5, 0x34

    const-string v6, "BSD-4-Clause-UC"

    const-string v7, "BSD-4-Clause (University of California-Specific)"

    const-string v8, "http://www.freebsd.org/copyright/license.html"

    move-object v3, v1

    .line 273
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSD_4_CLAUSE_UC:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSD_4_CLAUSE"

    const/16 v5, 0x35

    const-string v6, "BSD-4-Clause"

    const-string v7, "BSD 4-Clause \"Original\" or \"Old\" License"

    const-string v8, "http://directory.fsf.org/wiki/License:BSD_4Clause"

    move-object v3, v1

    .line 278
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSD_4_CLAUSE:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSD_PROTECTION"

    const/16 v5, 0x36

    const-string v6, "BSD-Protection"

    const-string v7, "BSD Protection License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/BSD_Protection_License"

    move-object v3, v1

    .line 283
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSD_PROTECTION:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSD_SOURCE_CODE"

    const/16 v5, 0x37

    const-string v6, "BSD-Source-Code"

    const-string v7, "BSD Source Code Attribution"

    const-string v8, "https://github.com/robbiehanson/CocoaHTTPServer/blob/master/LICENSE.txt"

    move-object v3, v1

    .line 288
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSD_SOURCE_CODE:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BSL_1_0"

    const/16 v5, 0x38

    const-string v6, "BSL-1.0"

    const-string v7, "Boost Software License 1.0"

    const-string v8, "http://www.boost.org/LICENSE_1_0.txt"

    move-object v3, v1

    .line 293
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BSL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BZIP2_1_0_5"

    const/16 v5, 0x39

    const-string v6, "bzip2-1.0.5"

    const-string v7, "bzip2 and libbzip2 License v1.0.5"

    const-string v8, "http://bzip.org/1.0.5/bzip2-manual-1.0.5.html"

    move-object v3, v1

    .line 298
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BZIP2_1_0_5:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "BZIP2_1_0_6"

    const/16 v5, 0x3a

    const-string v6, "bzip2-1.0.6"

    const-string v7, "bzip2 and libbzip2 License v1.0.6"

    const-string v8, "https://github.com/asimonov-im/bzip2/blob/master/LICENSE"

    move-object v3, v1

    .line 303
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->BZIP2_1_0_6:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CALDERA"

    const/16 v5, 0x3b

    const-string v6, "Caldera"

    const-string v7, "Caldera License"

    const-string v8, "http://www.lemis.com/grog/UNIX/ancient-source-all.pdf"

    move-object v3, v1

    .line 308
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CALDERA:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CATOSL_1_1"

    const/16 v5, 0x3c

    const-string v6, "CATOSL-1.1"

    const-string v7, "Computer Associates Trusted Open Source License 1.1"

    const-string v8, "http://opensource.org/licenses/CATOSL-1.1"

    move-object v3, v1

    .line 313
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CATOSL_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_1_0"

    const/16 v5, 0x3d

    const-string v6, "CC-BY-1.0"

    const-string v7, "Creative Commons Attribution 1.0 Generic"

    const-string v8, "http://creativecommons.org/licenses/by/1.0/legalcode"

    move-object v3, v1

    .line 318
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_2_0"

    const/16 v5, 0x3e

    const-string v6, "CC-BY-2.0"

    const-string v7, "Creative Commons Attribution 2.0 Generic"

    const-string v8, "http://creativecommons.org/licenses/by/2.0/legalcode"

    move-object v3, v1

    .line 323
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_2_5"

    const/16 v5, 0x3f

    const-string v6, "CC-BY-2.5"

    const-string v7, "Creative Commons Attribution 2.5 Generic"

    const-string v8, "http://creativecommons.org/licenses/by/2.5/legalcode"

    move-object v3, v1

    .line 328
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_2_5:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_3_0"

    const/16 v5, 0x40

    const-string v6, "CC-BY-3.0"

    const-string v7, "Creative Commons Attribution 3.0 Unported"

    const-string v8, "http://creativecommons.org/licenses/by/3.0/legalcode"

    move-object v3, v1

    .line 333
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_3_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_4_0"

    const/16 v5, 0x41

    const-string v6, "CC-BY-4.0"

    const-string v7, "Creative Commons Attribution 4.0 International"

    const-string v8, "http://creativecommons.org/licenses/by/4.0/legalcode"

    move-object v3, v1

    .line 338
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_4_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_NC_1_0"

    const/16 v5, 0x42

    const-string v6, "CC-BY-NC-1.0"

    const-string v7, "Creative Commons Attribution Non Commercial 1.0 Generic"

    const-string v8, "http://creativecommons.org/licenses/by-nc/1.0/legalcode"

    move-object v3, v1

    .line 343
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_NC_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_NC_2_0"

    const/16 v5, 0x43

    const-string v6, "CC-BY-NC-2.0"

    const-string v7, "Creative Commons Attribution Non Commercial 2.0 Generic"

    const-string v8, "http://creativecommons.org/licenses/by-nc/2.0/legalcode"

    move-object v3, v1

    .line 348
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_NC_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_NC_2_5"

    const/16 v5, 0x44

    const-string v6, "CC-BY-NC-2.5"

    const-string v7, "Creative Commons Attribution Non Commercial 2.5 Generic"

    const-string v8, "http://creativecommons.org/licenses/by-nc/2.5/legalcode"

    move-object v3, v1

    .line 353
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_NC_2_5:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_NC_3_0"

    const/16 v5, 0x45

    const-string v6, "CC-BY-NC-3.0"

    const-string v7, "Creative Commons Attribution Non Commercial 3.0 Unported"

    const-string v8, "http://creativecommons.org/licenses/by-nc/3.0/legalcode"

    move-object v3, v1

    .line 358
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_NC_3_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_NC_4_0"

    const/16 v5, 0x46

    const-string v6, "CC-BY-NC-4.0"

    const-string v7, "Creative Commons Attribution Non Commercial 4.0 International"

    const-string v8, "http://creativecommons.org/licenses/by-nc/4.0/legalcode"

    move-object v3, v1

    .line 363
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_NC_4_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_NC_ND_1_0"

    const/16 v5, 0x47

    const-string v6, "CC-BY-NC-ND-1.0"

    const-string v7, "Creative Commons Attribution Non Commercial No Derivatives 1.0 Generic"

    const-string v8, "http://creativecommons.org/licenses/by-nd-nc/1.0/legalcode"

    move-object v3, v1

    .line 368
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_NC_ND_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_NC_ND_2_0"

    const/16 v5, 0x48

    const-string v6, "CC-BY-NC-ND-2.0"

    const-string v7, "Creative Commons Attribution Non Commercial No Derivatives 2.0 Generic"

    const-string v8, "http://creativecommons.org/licenses/by-nc-nd/2.0/legalcode"

    move-object v3, v1

    .line 373
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_NC_ND_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_NC_ND_2_5"

    const/16 v5, 0x49

    const-string v6, "CC-BY-NC-ND-2.5"

    const-string v7, "Creative Commons Attribution Non Commercial No Derivatives 2.5 Generic"

    const-string v8, "http://creativecommons.org/licenses/by-nc-nd/2.5/legalcode"

    move-object v3, v1

    .line 378
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_NC_ND_2_5:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_NC_ND_3_0"

    const/16 v5, 0x4a

    const-string v6, "CC-BY-NC-ND-3.0"

    const-string v7, "Creative Commons Attribution Non Commercial No Derivatives 3.0 Unported"

    const-string v8, "http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode"

    move-object v3, v1

    .line 383
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_NC_ND_3_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_NC_ND_4_0"

    const/16 v5, 0x4b

    const-string v6, "CC-BY-NC-ND-4.0"

    const-string v7, "Creative Commons Attribution Non Commercial No Derivatives 4.0 International"

    const-string v8, "http://creativecommons.org/licenses/by-nc-nd/4.0/legalcode"

    move-object v3, v1

    .line 388
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_NC_ND_4_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_NC_SA_1_0"

    const/16 v5, 0x4c

    const-string v6, "CC-BY-NC-SA-1.0"

    const-string v7, "Creative Commons Attribution Non Commercial Share Alike 1.0 Generic"

    const-string v8, "http://creativecommons.org/licenses/by-nc-sa/1.0/legalcode"

    move-object v3, v1

    .line 393
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_NC_SA_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_NC_SA_2_0"

    const/16 v5, 0x4d

    const-string v6, "CC-BY-NC-SA-2.0"

    const-string v7, "Creative Commons Attribution Non Commercial Share Alike 2.0 Generic"

    const-string v8, "http://creativecommons.org/licenses/by-nc-sa/2.0/legalcode"

    move-object v3, v1

    .line 398
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_NC_SA_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_NC_SA_2_5"

    const/16 v5, 0x4e

    const-string v6, "CC-BY-NC-SA-2.5"

    const-string v7, "Creative Commons Attribution Non Commercial Share Alike 2.5 Generic"

    const-string v8, "http://creativecommons.org/licenses/by-nc-sa/2.5/legalcode"

    move-object v3, v1

    .line 403
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_NC_SA_2_5:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_NC_SA_3_0"

    const/16 v5, 0x4f

    const-string v6, "CC-BY-NC-SA-3.0"

    const-string v7, "Creative Commons Attribution Non Commercial Share Alike 3.0 Unported"

    const-string v8, "http://creativecommons.org/licenses/by-nc-sa/3.0/legalcode"

    move-object v3, v1

    .line 408
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_NC_SA_3_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_NC_SA_4_0"

    const/16 v5, 0x50

    const-string v6, "CC-BY-NC-SA-4.0"

    const-string v7, "Creative Commons Attribution Non Commercial Share Alike 4.0 International"

    const-string v8, "http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode"

    move-object v3, v1

    .line 413
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_NC_SA_4_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_ND_1_0"

    const/16 v5, 0x51

    const-string v6, "CC-BY-ND-1.0"

    const-string v7, "Creative Commons Attribution No Derivatives 1.0 Generic"

    const-string v8, "http://creativecommons.org/licenses/by-nd/1.0/legalcode"

    move-object v3, v1

    .line 418
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_ND_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_ND_2_0"

    const/16 v5, 0x52

    const-string v6, "CC-BY-ND-2.0"

    const-string v7, "Creative Commons Attribution No Derivatives 2.0 Generic"

    const-string v8, "https://creativecommons.org/licenses/by-nd/2.0/legalcode"

    move-object v3, v1

    .line 423
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_ND_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_ND_2_5"

    const/16 v5, 0x53

    const-string v6, "CC-BY-ND-2.5"

    const-string v7, "Creative Commons Attribution No Derivatives 2.5 Generic"

    const-string v8, "http://creativecommons.org/licenses/by-nd/2.5/legalcode"

    move-object v3, v1

    .line 428
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_ND_2_5:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_ND_3_0"

    const/16 v5, 0x54

    const-string v6, "CC-BY-ND-3.0"

    const-string v7, "Creative Commons Attribution No Derivatives 3.0 Unported"

    const-string v8, "http://creativecommons.org/licenses/by-nd/3.0/legalcode"

    move-object v3, v1

    .line 433
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_ND_3_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_ND_4_0"

    const/16 v5, 0x55

    const-string v6, "CC-BY-ND-4.0"

    const-string v7, "Creative Commons Attribution No Derivatives 4.0 International"

    const-string v8, "http://creativecommons.org/licenses/by-nd/4.0/legalcode"

    move-object v3, v1

    .line 438
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_ND_4_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_SA_1_0"

    const/16 v5, 0x56

    const-string v6, "CC-BY-SA-1.0"

    const-string v7, "Creative Commons Attribution Share Alike 1.0 Generic"

    const-string v8, "http://creativecommons.org/licenses/by-sa/1.0/legalcode"

    move-object v3, v1

    .line 443
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_SA_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_SA_2_0"

    const/16 v5, 0x57

    const-string v6, "CC-BY-SA-2.0"

    const-string v7, "Creative Commons Attribution Share Alike 2.0 Generic"

    const-string v8, "http://creativecommons.org/licenses/by-sa/2.0/legalcode"

    move-object v3, v1

    .line 448
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_SA_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_SA_2_5"

    const/16 v5, 0x58

    const-string v6, "CC-BY-SA-2.5"

    const-string v7, "Creative Commons Attribution Share Alike 2.5 Generic"

    const-string v8, "http://creativecommons.org/licenses/by-sa/2.5/legalcode"

    move-object v3, v1

    .line 453
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_SA_2_5:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_SA_3_0"

    const/16 v5, 0x59

    const-string v6, "CC-BY-SA-3.0"

    const-string v7, "Creative Commons Attribution Share Alike 3.0 Unported"

    const-string v8, "http://creativecommons.org/licenses/by-sa/3.0/legalcode"

    move-object v3, v1

    .line 458
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_SA_3_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC_BY_SA_4_0"

    const/16 v5, 0x5a

    const-string v6, "CC-BY-SA-4.0"

    const-string v7, "Creative Commons Attribution Share Alike 4.0 International"

    const-string v8, "http://creativecommons.org/licenses/by-sa/4.0/legalcode"

    move-object v3, v1

    .line 463
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC_BY_SA_4_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CC0_1_0"

    const/16 v5, 0x5b

    const-string v6, "CC0-1.0"

    const-string v7, "Creative Commons Zero v1.0 Universal"

    const-string v8, "http://creativecommons.org/publicdomain/zero/1.0/legalcode"

    move-object v3, v1

    .line 468
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CC0_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CDDL_1_0"

    const/16 v5, 0x5c

    const-string v6, "CDDL-1.0"

    const-string v7, "Common Development and Distribution License 1.0"

    const-string v8, "http://www.opensource.org/licenses/cddl1"

    move-object v3, v1

    .line 473
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CDDL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CDDL_1_1"

    const/16 v5, 0x5d

    const-string v6, "CDDL-1.1"

    const-string v7, "Common Development and Distribution License 1.1"

    const-string v8, "http://glassfish.java.net/public/CDDL+GPL_1_1.html"

    move-object v3, v1

    .line 478
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CDDL_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CDLA_PERMISSIVE_1_0"

    const/16 v5, 0x5e

    const-string v6, "CDLA-Permissive-1.0"

    const-string v7, "Community Data License Agreement Permissive 1.0"

    const-string v8, "https://cdla.io/permissive-1-0"

    move-object v3, v1

    .line 483
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CDLA_PERMISSIVE_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CDLA_SHARING_1_0"

    const/16 v5, 0x5f

    const-string v6, "CDLA-Sharing-1.0"

    const-string v7, "Community Data License Agreement Sharing 1.0"

    const-string v8, "https://cdla.io/sharing-1-0"

    move-object v3, v1

    .line 488
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CDLA_SHARING_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CECILL_1_0"

    const/16 v5, 0x60

    const-string v6, "CECILL-1.0"

    const-string v7, "CeCILL Free Software License Agreement v1.0"

    const-string v8, "http://www.cecill.info/licences/Licence_CeCILL_V1-fr.html"

    move-object v3, v1

    .line 493
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CECILL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CECILL_1_1"

    const/16 v5, 0x61

    const-string v6, "CECILL-1.1"

    const-string v7, "CeCILL Free Software License Agreement v1.1"

    const-string v8, "http://www.cecill.info/licences/Licence_CeCILL_V1.1-US.html"

    move-object v3, v1

    .line 498
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CECILL_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CECILL_2_0"

    const/16 v5, 0x62

    const-string v6, "CECILL-2.0"

    const-string v7, "CeCILL Free Software License Agreement v2.0"

    const-string v8, "http://www.cecill.info/licences/Licence_CeCILL_V2-en.html"

    move-object v3, v1

    .line 503
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CECILL_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CECILL_2_1"

    const/16 v5, 0x63

    const-string v6, "CECILL-2.1"

    const-string v7, "CeCILL Free Software License Agreement v2.1"

    const-string v8, "http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html"

    move-object v3, v1

    .line 508
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CECILL_2_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CECILL_B"

    const/16 v5, 0x64

    const-string v6, "CECILL-B"

    const-string v7, "CeCILL-B Free Software License Agreement"

    const-string v8, "http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html"

    move-object v3, v1

    .line 513
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CECILL_B:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CECILL_C"

    const/16 v5, 0x65

    const-string v6, "CECILL-C"

    const-string v7, "CeCILL-C Free Software License Agreement"

    const-string v8, "http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html"

    move-object v3, v1

    .line 518
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CECILL_C:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CLARTISTIC"

    const/16 v5, 0x66

    const-string v6, "ClArtistic"

    const-string v7, "Clarified Artistic License"

    const-string v8, "http://gianluca.dellavedova.org/2011/01/03/clarified-artistic-license/"

    move-object v3, v1

    .line 523
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CLARTISTIC:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CNRI_JYTHON"

    const/16 v5, 0x67

    const-string v6, "CNRI-Jython"

    const-string v7, "CNRI Jython License"

    const-string v8, "http://www.jython.org/license.html"

    move-object v3, v1

    .line 528
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CNRI_JYTHON:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CNRI_PYTHON_GPL_COMPATIBLE"

    const/16 v5, 0x68

    const-string v6, "CNRI-Python-GPL-Compatible"

    const-string v7, "CNRI Python Open Source GPL Compatible License Agreement"

    const-string v8, "http://www.python.org/download/releases/1.6.1/download_win/"

    move-object v3, v1

    .line 533
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CNRI_PYTHON_GPL_COMPATIBLE:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CNRI_PYTHON"

    const/16 v5, 0x69

    const-string v6, "CNRI-Python"

    const-string v7, "CNRI Python License"

    const-string v8, "http://www.opensource.org/licenses/CNRI-Python"

    move-object v3, v1

    .line 538
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CNRI_PYTHON:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CONDOR_1_1"

    const/16 v5, 0x6a

    const-string v6, "Condor-1.1"

    const-string v7, "Condor Public License v1.1"

    const-string v8, "http://research.cs.wisc.edu/condor/license.html#condor"

    move-object v3, v1

    .line 543
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CONDOR_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CPAL_1_0"

    const/16 v5, 0x6b

    const-string v6, "CPAL-1.0"

    const-string v7, "Common Public Attribution License 1.0"

    const-string v8, "http://www.opensource.org/licenses/CPAL-1.0"

    move-object v3, v1

    .line 548
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CPAL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CPL_1_0"

    const/16 v5, 0x6c

    const-string v6, "CPL-1.0"

    const-string v7, "Common Public License 1.0"

    const-string v8, "http://opensource.org/licenses/CPL-1.0"

    move-object v3, v1

    .line 553
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CPOL_1_02"

    const/16 v5, 0x6d

    const-string v6, "CPOL-1.02"

    const-string v7, "Code Project Open License 1.02"

    const-string v8, "http://www.codeproject.com/info/cpol10.aspx"

    move-object v3, v1

    .line 558
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CPOL_1_02:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CROSSWORD"

    const/16 v5, 0x6e

    const-string v6, "Crossword"

    const-string v7, "Crossword License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Crossword"

    move-object v3, v1

    .line 563
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CROSSWORD:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CRYSTALSTACKER"

    const/16 v5, 0x6f

    const-string v6, "CrystalStacker"

    const-string v7, "CrystalStacker License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing:CrystalStacker?rd=Licensing/CrystalStacker"

    move-object v3, v1

    .line 568
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CRYSTALSTACKER:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CUA_OPL_1_0"

    const/16 v5, 0x70

    const-string v6, "CUA-OPL-1.0"

    const-string v7, "CUA Office Public License v1.0"

    const-string v8, "http://opensource.org/licenses/CUA-OPL-1.0"

    move-object v3, v1

    .line 573
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CUA_OPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CUBE"

    const/16 v5, 0x71

    const-string v6, "Cube"

    const-string v7, "Cube License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Cube"

    move-object v3, v1

    .line 578
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CUBE:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "CURL"

    const/16 v5, 0x72

    const-string v6, "curl"

    const-string v7, "curl License"

    const-string v8, "https://github.com/bagder/curl/blob/master/COPYING"

    move-object v3, v1

    .line 583
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->CURL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "D_FSL_1_0"

    const/16 v5, 0x73

    const-string v6, "D-FSL-1.0"

    const-string v7, "Deutsche Freie Software Lizenz"

    const-string v8, "http://www.dipp.nrw.de/d-fsl/lizenzen/"

    move-object v3, v1

    .line 588
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->D_FSL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "DIFFMARK"

    const/16 v5, 0x74

    const-string v6, "diffmark"

    const-string v7, "diffmark license"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/diffmark"

    move-object v3, v1

    .line 593
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->DIFFMARK:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "DOC"

    const/16 v5, 0x75

    const-string v6, "DOC"

    const-string v7, "DOC License"

    const-string v8, "http://www.cs.wustl.edu/~schmidt/ACE-copying.html"

    move-object v3, v1

    .line 598
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->DOC:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "DOTSEQN"

    const/16 v5, 0x76

    const-string v6, "Dotseqn"

    const-string v7, "Dotseqn License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Dotseqn"

    move-object v3, v1

    .line 603
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->DOTSEQN:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "DSDP"

    const/16 v5, 0x77

    const-string v6, "DSDP"

    const-string v7, "DSDP License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/DSDP"

    move-object v3, v1

    .line 608
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->DSDP:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "DVIPDFM"

    const/16 v5, 0x78

    const-string v6, "dvipdfm"

    const-string v7, "dvipdfm License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/dvipdfm"

    move-object v3, v1

    .line 613
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->DVIPDFM:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ECL_1_0"

    const/16 v5, 0x79

    const-string v6, "ECL-1.0"

    const-string v7, "Educational Community License v1.0"

    const-string v8, "http://opensource.org/licenses/ECL-1.0"

    move-object v3, v1

    .line 618
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ECL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ECL_2_0"

    const/16 v5, 0x7a

    const-string v6, "ECL-2.0"

    const-string v7, "Educational Community License v2.0"

    const-string v8, "http://opensource.org/licenses/ECL-2.0"

    move-object v3, v1

    .line 623
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ECL_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "EFL_1_0"

    const/16 v5, 0x7b

    const-string v6, "EFL-1.0"

    const-string v7, "Eiffel Forum License v1.0"

    const-string v8, "http://www.eiffel-nice.org/license/forum.txt"

    move-object v3, v1

    .line 628
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->EFL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "EFL_2_0"

    const/16 v5, 0x7c

    const-string v6, "EFL-2.0"

    const-string v7, "Eiffel Forum License v2.0"

    const-string v8, "http://www.eiffel-nice.org/license/eiffel-forum-license-2.html"

    move-object v3, v1

    .line 633
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->EFL_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "EGENIX"

    const/16 v5, 0x7d

    const-string v6, "eGenix"

    const-string v7, "eGenix.com Public License 1.1.0"

    const-string v8, "http://www.egenix.com/products/eGenix.com-Public-License-1.1.0.pdf"

    move-object v3, v1

    .line 638
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->EGENIX:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ENTESSA"

    const/16 v5, 0x7e

    const-string v6, "Entessa"

    const-string v7, "Entessa Public License v1.0"

    const-string v8, "http://opensource.org/licenses/Entessa"

    move-object v3, v1

    .line 643
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ENTESSA:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "EPL_1_0"

    const/16 v5, 0x7f

    const-string v6, "EPL-1.0"

    const-string v7, "Eclipse Public License 1.0"

    const-string v8, "http://www.eclipse.org/legal/epl-v10.html"

    move-object v3, v1

    .line 648
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->EPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "EPL_2_0"

    const/16 v5, 0x80

    const-string v6, "EPL-2.0"

    const-string v7, "Eclipse Public License 2.0"

    const-string v8, "https://www.eclipse.org/legal/epl-2.0"

    move-object v3, v1

    .line 653
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->EPL_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ERLPL_1_1"

    const/16 v5, 0x81

    const-string v6, "ErlPL-1.1"

    const-string v7, "Erlang Public License v1.1"

    const-string v8, "http://www.erlang.org/EPLICENSE"

    move-object v3, v1

    .line 658
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ERLPL_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "EUDATAGRID"

    const/16 v5, 0x82

    const-string v6, "EUDatagrid"

    const-string v7, "EU DataGrid Software License"

    const-string v8, "http://eu-datagrid.web.cern.ch/eu-datagrid/license.html"

    move-object v3, v1

    .line 663
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->EUDATAGRID:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "EUPL_1_0"

    const/16 v5, 0x83

    const-string v6, "EUPL-1.0"

    const-string v7, "European Union Public License 1.0"

    const-string v8, "http://ec.europa.eu/idabc/en/document/7330.html"

    move-object v3, v1

    .line 668
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->EUPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "EUPL_1_1"

    const/16 v5, 0x84

    const-string v6, "EUPL-1.1"

    const-string v7, "European Union Public License 1.1"

    const-string v8, "https://joinup.ec.europa.eu/software/page/eupl/licence-eupl"

    move-object v3, v1

    .line 673
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->EUPL_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "EUPL_1_2"

    const/16 v5, 0x85

    const-string v6, "EUPL-1.2"

    const-string v7, "European Union Public License 1.2"

    const-string v8, "https://joinup.ec.europa.eu/page/eupl-text-11-12"

    move-object v3, v1

    .line 678
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->EUPL_1_2:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "EUROSYM"

    const/16 v5, 0x86

    const-string v6, "Eurosym"

    const-string v7, "Eurosym License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Eurosym"

    move-object v3, v1

    .line 683
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->EUROSYM:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "FAIR"

    const/16 v5, 0x87

    const-string v6, "Fair"

    const-string v7, "Fair License"

    const-string v8, "http://fairlicense.org/"

    move-object v3, v1

    .line 688
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->FAIR:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "FRAMEWORX_1_0"

    const/16 v5, 0x88

    const-string v6, "Frameworx-1.0"

    const-string v7, "Frameworx Open License 1.0"

    const-string v8, "http://www.opensource.org/licenses/Frameworx-1.0"

    move-object v3, v1

    .line 693
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->FRAMEWORX_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "FREEIMAGE"

    const/16 v5, 0x89

    const-string v6, "FreeImage"

    const-string v7, "FreeImage Public License v1.0"

    const-string v8, "http://freeimage.sourceforge.net/freeimage-license.txt"

    move-object v3, v1

    .line 698
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->FREEIMAGE:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "FSFAP"

    const/16 v5, 0x8a

    const-string v6, "FSFAP"

    const-string v7, "FSF All Permissive License"

    const-string v8, "http://www.gnu.org/prep/maintain/html_node/License-Notices-for-Other-Files.html"

    move-object v3, v1

    .line 703
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->FSFAP:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "FSFUL"

    const/16 v5, 0x8b

    const-string v6, "FSFUL"

    const-string v7, "FSF Unlimited License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/FSF_Unlimited_License"

    move-object v3, v1

    .line 708
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->FSFUL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "FSFULLR"

    const/16 v5, 0x8c

    const-string v6, "FSFULLR"

    const-string v7, "FSF Unlimited License (with License Retention)"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/FSF_Unlimited_License#License_Retention_Variant"

    move-object v3, v1

    .line 713
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->FSFULLR:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "FTL"

    const/16 v5, 0x8d

    const-string v6, "FTL"

    const-string v7, "Freetype Project License"

    const-string v8, "http://freetype.fis.uniroma2.it/FTL.TXT"

    move-object v3, v1

    .line 718
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->FTL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GFDL_1_1_ONLY"

    const/16 v5, 0x8e

    const-string v6, "GFDL-1.1-only"

    const-string v7, "GNU Free Documentation License v1.1 only"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/fdl-1.1.txt"

    move-object v3, v1

    .line 723
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GFDL_1_1_ONLY:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GFDL_1_1_OR_LATER"

    const/16 v5, 0x8f

    const-string v6, "GFDL-1.1-or-later"

    const-string v7, "GNU Free Documentation License v1.1 or later"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/fdl-1.1.txt"

    move-object v3, v1

    .line 728
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GFDL_1_1_OR_LATER:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GFDL_1_2_ONLY"

    const/16 v5, 0x90

    const-string v6, "GFDL-1.2-only"

    const-string v7, "GNU Free Documentation License v1.2 only"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/fdl-1.2.txt"

    move-object v3, v1

    .line 733
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GFDL_1_2_ONLY:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GFDL_1_2_OR_LATER"

    const/16 v5, 0x91

    const-string v6, "GFDL-1.2-or-later"

    const-string v7, "GNU Free Documentation License v1.2 or later"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/fdl-1.2.txt"

    move-object v3, v1

    .line 738
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GFDL_1_2_OR_LATER:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GFDL_1_3_ONLY"

    const/16 v5, 0x92

    const-string v6, "GFDL-1.3-only"

    const-string v7, "GNU Free Documentation License v1.3 only"

    const-string v8, "http://www.gnu.org/licenses/fdl-1.3.txt"

    move-object v3, v1

    .line 743
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GFDL_1_3_ONLY:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GFDL_1_3_OR_LATER"

    const/16 v5, 0x93

    const-string v6, "GFDL-1.3-or-later"

    const-string v7, "GNU Free Documentation License v1.3 or later"

    const-string v8, "http://www.gnu.org/licenses/fdl-1.3.txt"

    move-object v3, v1

    .line 748
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GFDL_1_3_OR_LATER:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GIFTWARE"

    const/16 v5, 0x94

    const-string v6, "Giftware"

    const-string v7, "Giftware License"

    const-string v8, "http://liballeg.org/license.html#allegro-4-the-giftware-license"

    move-object v3, v1

    .line 753
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GIFTWARE:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GL2PS"

    const/16 v5, 0x95

    const-string v6, "GL2PS"

    const-string v7, "GL2PS License"

    const-string v8, "http://www.geuz.org/gl2ps/COPYING.GL2PS"

    move-object v3, v1

    .line 758
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GL2PS:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GLIDE"

    const/16 v5, 0x96

    const-string v6, "Glide"

    const-string v7, "3dfx Glide License"

    const-string v8, "http://www.users.on.net/~triforce/glidexp/COPYING.txt"

    move-object v3, v1

    .line 763
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GLIDE:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GLULXE"

    const/16 v5, 0x97

    const-string v6, "Glulxe"

    const-string v7, "Glulxe License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Glulxe"

    move-object v3, v1

    .line 768
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GLULXE:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GNUPLOT"

    const/16 v5, 0x98

    const-string v6, "gnuplot"

    const-string v7, "gnuplot License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Gnuplot"

    move-object v3, v1

    .line 773
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GNUPLOT:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_1_0_ONLY"

    const/16 v5, 0x99

    const-string v6, "GPL-1.0-only"

    const-string v7, "GNU General Public License v1.0 only"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/gpl-1.0-standalone.html"

    move-object v3, v1

    .line 778
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_1_0_ONLY:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_1_0_OR_LATER"

    const/16 v5, 0x9a

    const-string v6, "GPL-1.0-or-later"

    const-string v7, "GNU General Public License v1.0 or later"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/gpl-1.0-standalone.html"

    move-object v3, v1

    .line 783
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_1_0_OR_LATER:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_2_0_ONLY"

    const/16 v5, 0x9b

    const-string v6, "GPL-2.0-only"

    const-string v7, "GNU General Public License v2.0 only"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html"

    move-object v3, v1

    .line 788
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_2_0_ONLY:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_2_0_OR_LATER"

    const/16 v5, 0x9c

    const-string v6, "GPL-2.0-or-later"

    const-string v7, "GNU General Public License v2.0 or later"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html"

    move-object v3, v1

    .line 793
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_2_0_OR_LATER:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_3_0_ONLY"

    const/16 v5, 0x9d

    const-string v6, "GPL-3.0-only"

    const-string v7, "GNU General Public License v3.0 only"

    const-string v8, "http://www.gnu.org/licenses/gpl-3.0-standalone.html"

    move-object v3, v1

    .line 798
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_3_0_ONLY:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_3_0_OR_LATER"

    const/16 v5, 0x9e

    const-string v6, "GPL-3.0-or-later"

    const-string v7, "GNU General Public License v3.0 or later"

    const-string v8, "http://www.gnu.org/licenses/gpl-3.0-standalone.html"

    move-object v3, v1

    .line 803
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_3_0_OR_LATER:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GSOAP_1_3B"

    const/16 v5, 0x9f

    const-string v6, "gSOAP-1.3b"

    const-string v7, "gSOAP Public License v1.3b"

    const-string v8, "http://www.cs.fsu.edu/~engelen/license.html"

    move-object v3, v1

    .line 808
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GSOAP_1_3B:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "HASKELLREPORT"

    const/16 v5, 0xa0

    const-string v6, "HaskellReport"

    const-string v7, "Haskell Language Report License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Haskell_Language_Report_License"

    move-object v3, v1

    .line 813
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->HASKELLREPORT:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "HPND"

    const/16 v5, 0xa1

    const-string v6, "HPND"

    const-string v7, "Historical Permission Notice and Disclaimer"

    const-string v8, "http://www.opensource.org/licenses/HPND"

    move-object v3, v1

    .line 818
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->HPND:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "IBM_PIBS"

    const/16 v5, 0xa2

    const-string v6, "IBM-pibs"

    const-string v7, "IBM PowerPC Initialization and Boot Software"

    const-string v8, "http://git.denx.de/?p=u-boot.git;a=blob;f=arch/powerpc/cpu/ppc4xx/miiphy.c;h=297155fdafa064b955e53e9832de93bfb0cfb85b;hb=9fab4bf4cc077c21e43941866f3f2c196f28670d"

    move-object v3, v1

    .line 823
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->IBM_PIBS:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ICU"

    const/16 v5, 0xa3

    const-string v6, "ICU"

    const-string v7, "ICU License"

    const-string v8, "http://source.icu-project.org/repos/icu/icu/trunk/license.html"

    move-object v3, v1

    .line 828
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ICU:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "IJG"

    const/16 v5, 0xa4

    const-string v6, "IJG"

    const-string v7, "Independent JPEG Group License"

    const-string v8, "http://dev.w3.org/cvsweb/Amaya/libjpeg/Attic/README?rev=1.2"

    move-object v3, v1

    .line 833
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->IJG:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "IMAGEMAGICK"

    const/16 v5, 0xa5

    const-string v6, "ImageMagick"

    const-string v7, "ImageMagick License"

    const-string v8, "http://www.imagemagick.org/script/license.php"

    move-object v3, v1

    .line 838
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->IMAGEMAGICK:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "IMATIX"

    const/16 v5, 0xa6

    const-string v6, "iMatix"

    const-string v7, "iMatix Standard Function Library Agreement"

    const-string v8, "http://legacy.imatix.com/html/sfl/sfl4.htm#license"

    move-object v3, v1

    .line 843
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->IMATIX:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "IMLIB2"

    const/16 v5, 0xa7

    const-string v6, "Imlib2"

    const-string v7, "Imlib2 License"

    const-string v8, "https://git.enlightenment.org/legacy/imlib2.git/tree/COPYING"

    move-object v3, v1

    .line 848
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->IMLIB2:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "INFO_ZIP"

    const/16 v5, 0xa8

    const-string v6, "Info-ZIP"

    const-string v7, "Info-ZIP License"

    const-string v8, "http://www.info-zip.org/license.html"

    move-object v3, v1

    .line 853
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->INFO_ZIP:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "INTEL_ACPI"

    const/16 v5, 0xa9

    const-string v6, "Intel-ACPI"

    const-string v7, "Intel ACPI Software License Agreement"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Intel_ACPI_Software_License_Agreement"

    move-object v3, v1

    .line 858
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->INTEL_ACPI:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "INTEL"

    const/16 v5, 0xaa

    const-string v6, "Intel"

    const-string v7, "Intel Open Source License"

    const-string v8, "http://opensource.org/licenses/Intel"

    move-object v3, v1

    .line 863
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->INTEL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "INTERBASE_1_0"

    const/16 v5, 0xab

    const-string v6, "Interbase-1.0"

    const-string v7, "Interbase Public License v1.0"

    const-string v8, "https://web.archive.org/web/20060319014854/http://info.borland.com/devsupport/interbase/opensource/IPL.html"

    move-object v3, v1

    .line 868
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->INTERBASE_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "IPA"

    const/16 v5, 0xac

    const-string v6, "IPA"

    const-string v7, "IPA Font License"

    const-string v8, "http://www.opensource.org/licenses/IPA"

    move-object v3, v1

    .line 873
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->IPA:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "IPL_1_0"

    const/16 v5, 0xad

    const-string v6, "IPL-1.0"

    const-string v7, "IBM Public License v1.0"

    const-string v8, "http://www.opensource.org/licenses/IPL-1.0"

    move-object v3, v1

    .line 878
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->IPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ISC"

    const/16 v5, 0xae

    const-string v6, "ISC"

    const-string v7, "ISC License"

    const-string v8, "https://www.isc.org/downloads/software-support-policy/isc-license/"

    move-object v3, v1

    .line 883
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ISC:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "JASPER_2_0"

    const/16 v5, 0xaf

    const-string v6, "JasPer-2.0"

    const-string v7, "JasPer License"

    const-string v8, "http://www.ece.uvic.ca/~mdadams/jasper/LICENSE"

    move-object v3, v1

    .line 888
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->JASPER_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "JSON"

    const/16 v5, 0xb0

    const-string v6, "JSON"

    const-string v7, "JSON License"

    const-string v8, "http://www.json.org/license.html"

    move-object v3, v1

    .line 893
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->JSON:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LAL_1_2"

    const/16 v5, 0xb1

    const-string v6, "LAL-1.2"

    const-string v7, "Licence Art Libre 1.2"

    const-string v8, "http://artlibre.org/licence/lal/licence-art-libre-12/"

    move-object v3, v1

    .line 898
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LAL_1_2:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LAL_1_3"

    const/16 v5, 0xb2

    const-string v6, "LAL-1.3"

    const-string v7, "Licence Art Libre 1.3"

    const-string v8, "http://artlibre.org/"

    move-object v3, v1

    .line 903
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LAL_1_3:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xb2

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LATEX2E"

    const/16 v5, 0xb3

    const-string v6, "Latex2e"

    const-string v7, "Latex2e License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Latex2e"

    move-object v3, v1

    .line 908
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LATEX2E:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xb3

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LEPTONICA"

    const/16 v5, 0xb4

    const-string v6, "Leptonica"

    const-string v7, "Leptonica License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Leptonica"

    move-object v3, v1

    .line 913
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LEPTONICA:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xb4

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LGPL_2_0_ONLY"

    const/16 v5, 0xb5

    const-string v6, "LGPL-2.0-only"

    const-string v7, "GNU Library General Public License v2 only"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/lgpl-2.0-standalone.html"

    move-object v3, v1

    .line 918
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LGPL_2_0_ONLY:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xb5

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LGPL_2_0_OR_LATER"

    const/16 v5, 0xb6

    const-string v6, "LGPL-2.0-or-later"

    const-string v7, "GNU Library General Public License v2 or later"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/lgpl-2.0-standalone.html"

    move-object v3, v1

    .line 923
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LGPL_2_0_OR_LATER:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xb6

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LGPL_2_1_ONLY"

    const/16 v5, 0xb7

    const-string v6, "LGPL-2.1-only"

    const-string v7, "GNU Lesser General Public License v2.1 only"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/lgpl-2.1-standalone.html"

    move-object v3, v1

    .line 928
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LGPL_2_1_ONLY:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xb7

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LGPL_2_1_OR_LATER"

    const/16 v5, 0xb8

    const-string v6, "LGPL-2.1-or-later"

    const-string v7, "GNU Lesser General Public License v2.1 or later"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/lgpl-2.1-standalone.html"

    move-object v3, v1

    .line 933
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LGPL_2_1_OR_LATER:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xb8

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LGPL_3_0_ONLY"

    const/16 v5, 0xb9

    const-string v6, "LGPL-3.0-only"

    const-string v7, "GNU Lesser General Public License v3.0 only"

    const-string v8, "http://www.gnu.org/licenses/lgpl-3.0-standalone.html"

    move-object v3, v1

    .line 938
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LGPL_3_0_ONLY:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xb9

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LGPL_3_0_OR_LATER"

    const/16 v5, 0xba

    const-string v6, "LGPL-3.0-or-later"

    const-string v7, "GNU Lesser General Public License v3.0 or later"

    const-string v8, "http://www.gnu.org/licenses/lgpl-3.0-standalone.html"

    move-object v3, v1

    .line 943
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LGPL_3_0_OR_LATER:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xba

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LGPLLR"

    const/16 v5, 0xbb

    const-string v6, "LGPLLR"

    const-string v7, "Lesser General Public License For Linguistic Resources"

    const-string v8, "http://www-igm.univ-mlv.fr/~unitex/lgpllr.html"

    move-object v3, v1

    .line 948
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LGPLLR:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xbb

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LIBPNG"

    const/16 v5, 0xbc

    const-string v6, "Libpng"

    const-string v7, "libpng License"

    const-string v8, "http://www.libpng.org/pub/png/src/libpng-LICENSE.txt"

    move-object v3, v1

    .line 953
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LIBPNG:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xbc

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LIBTIFF"

    const/16 v5, 0xbd

    const-string v6, "libtiff"

    const-string v7, "libtiff License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/libtiff"

    move-object v3, v1

    .line 958
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LIBTIFF:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xbd

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LILIQ_P_1_1"

    const/16 v5, 0xbe

    const-string v6, "LiLiQ-P-1.1"

    const-string v7, "Licence Libre du Qu\u00e9bec \u2013 Permissive version 1.1"

    const-string v8, "https://forge.gouv.qc.ca/licence/fr/liliq-v1-1/"

    move-object v3, v1

    .line 963
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LILIQ_P_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xbe

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LILIQ_R_1_1"

    const/16 v5, 0xbf

    const-string v6, "LiLiQ-R-1.1"

    const-string v7, "Licence Libre du Qu\u00e9bec \u2013 R\u00e9ciprocit\u00e9 version 1.1"

    const-string v8, "https://www.forge.gouv.qc.ca/participez/licence-logicielle/licence-libre-du-quebec-liliq-en-francais/licence-libre-du-quebec-reciprocite-liliq-r-v1-1/"

    move-object v3, v1

    .line 968
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LILIQ_R_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xbf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LILIQ_RPLUS_1_1"

    const/16 v5, 0xc0

    const-string v6, "LiLiQ-Rplus-1.1"

    const-string v7, "Licence Libre du Qu\u00e9bec \u2013 R\u00e9ciprocit\u00e9 forte version 1.1"

    const-string v8, "https://www.forge.gouv.qc.ca/participez/licence-logicielle/licence-libre-du-quebec-liliq-en-francais/licence-libre-du-quebec-reciprocite-forte-liliq-r-v1-1/"

    move-object v3, v1

    .line 973
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LILIQ_RPLUS_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xc0

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LINUX_OPENIB"

    const/16 v5, 0xc1

    const-string v6, "Linux-OpenIB"

    const-string v7, "Linux Kernel Variant of OpenIB.org license"

    const-string v8, "https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/drivers/infiniband/core/sa.h"

    move-object v3, v1

    .line 978
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LINUX_OPENIB:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xc1

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LPL_1_0"

    const/16 v5, 0xc2

    const-string v6, "LPL-1.0"

    const-string v7, "Lucent Public License Version 1.0"

    const-string v8, "http://opensource.org/licenses/LPL-1.0"

    move-object v3, v1

    .line 983
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xc2

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LPL_1_02"

    const/16 v5, 0xc3

    const-string v6, "LPL-1.02"

    const-string v7, "Lucent Public License v1.02"

    const-string v8, "http://plan9.bell-labs.com/plan9/license.html"

    move-object v3, v1

    .line 988
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LPL_1_02:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xc3

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LPPL_1_0"

    const/16 v5, 0xc4

    const-string v6, "LPPL-1.0"

    const-string v7, "LaTeX Project Public License v1.0"

    const-string v8, "http://www.latex-project.org/lppl/lppl-1-0.txt"

    move-object v3, v1

    .line 993
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LPPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xc4

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LPPL_1_1"

    const/16 v5, 0xc5

    const-string v6, "LPPL-1.1"

    const-string v7, "LaTeX Project Public License v1.1"

    const-string v8, "http://www.latex-project.org/lppl/lppl-1-1.txt"

    move-object v3, v1

    .line 998
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LPPL_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xc5

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LPPL_1_2"

    const/16 v5, 0xc6

    const-string v6, "LPPL-1.2"

    const-string v7, "LaTeX Project Public License v1.2"

    const-string v8, "http://www.latex-project.org/lppl/lppl-1-2.txt"

    move-object v3, v1

    .line 1003
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LPPL_1_2:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xc6

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LPPL_1_3A"

    const/16 v5, 0xc7

    const-string v6, "LPPL-1.3a"

    const-string v7, "LaTeX Project Public License v1.3a"

    const-string v8, "http://www.latex-project.org/lppl/lppl-1-3a.txt"

    move-object v3, v1

    .line 1008
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LPPL_1_3A:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xc7

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LPPL_1_3C"

    const/16 v5, 0xc8

    const-string v6, "LPPL-1.3c"

    const-string v7, "LaTeX Project Public License v1.3c"

    const-string v8, "http://www.latex-project.org/lppl/lppl-1-3c.txt"

    move-object v3, v1

    .line 1013
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LPPL_1_3C:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xc8

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MAKEINDEX"

    const/16 v5, 0xc9

    const-string v6, "MakeIndex"

    const-string v7, "MakeIndex License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/MakeIndex"

    move-object v3, v1

    .line 1018
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MAKEINDEX:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xc9

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MIROS"

    const/16 v5, 0xca

    const-string v6, "MirOS"

    const-string v7, "MirOS License"

    const-string v8, "http://www.opensource.org/licenses/MirOS"

    move-object v3, v1

    .line 1023
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MIROS:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xca

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MIT_0"

    const/16 v5, 0xcb

    const-string v6, "MIT-0"

    const-string v7, "MIT No Attribution"

    const-string v8, "https://github.com/aws/mit-0"

    move-object v3, v1

    .line 1028
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MIT_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xcb

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MIT_ADVERTISING"

    const/16 v5, 0xcc

    const-string v6, "MIT-advertising"

    const-string v7, "Enlightenment License (e16)"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/MIT_With_Advertising"

    move-object v3, v1

    .line 1033
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MIT_ADVERTISING:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xcc

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MIT_CMU"

    const/16 v5, 0xcd

    const-string v6, "MIT-CMU"

    const-string v7, "CMU License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing:MIT?rd=Licensing/MIT#CMU_Style"

    move-object v3, v1

    .line 1038
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MIT_CMU:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xcd

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MIT_ENNA"

    const/16 v5, 0xce

    const-string v6, "MIT-enna"

    const-string v7, "enna License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/MIT#enna"

    move-object v3, v1

    .line 1043
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MIT_ENNA:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xce

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MIT_FEH"

    const/16 v5, 0xcf

    const-string v6, "MIT-feh"

    const-string v7, "feh License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/MIT#feh"

    move-object v3, v1

    .line 1048
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MIT_FEH:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xcf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MIT"

    const/16 v5, 0xd0

    const-string v6, "MIT"

    const-string v7, "MIT License"

    const-string v8, "http://www.opensource.org/licenses/MIT"

    move-object v3, v1

    .line 1053
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MIT:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xd0

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MITNFA"

    const/16 v5, 0xd1

    const-string v6, "MITNFA"

    const-string v7, "MIT +no-false-attribs license"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/MITNFA"

    move-object v3, v1

    .line 1058
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MITNFA:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xd1

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MOTOSOTO"

    const/16 v5, 0xd2

    const-string v6, "Motosoto"

    const-string v7, "Motosoto License"

    const-string v8, "http://www.opensource.org/licenses/Motosoto"

    move-object v3, v1

    .line 1063
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MOTOSOTO:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xd2

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MPICH2"

    const/16 v5, 0xd3

    const-string v6, "mpich2"

    const-string v7, "mpich2 License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/MIT"

    move-object v3, v1

    .line 1068
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MPICH2:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xd3

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MPL_1_0"

    const/16 v5, 0xd4

    const-string v6, "MPL-1.0"

    const-string v7, "Mozilla Public License 1.0"

    const-string v8, "http://www.mozilla.org/MPL/MPL-1.0.html"

    move-object v3, v1

    .line 1073
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xd4

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MPL_1_1"

    const/16 v5, 0xd5

    const-string v6, "MPL-1.1"

    const-string v7, "Mozilla Public License 1.1"

    const-string v8, "http://www.mozilla.org/MPL/MPL-1.1.html"

    move-object v3, v1

    .line 1078
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MPL_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xd5

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MPL_2_0_NO_COPYLEFT_EXCEPTION"

    const/16 v5, 0xd6

    const-string v6, "MPL-2.0-no-copyleft-exception"

    const-string v7, "Mozilla Public License 2.0 (no copyleft exception)"

    const-string v8, "http://www.mozilla.org/MPL/2.0/"

    move-object v3, v1

    .line 1083
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MPL_2_0_NO_COPYLEFT_EXCEPTION:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xd6

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MPL_2_0"

    const/16 v5, 0xd7

    const-string v6, "MPL-2.0"

    const-string v7, "Mozilla Public License 2.0"

    const-string v8, "http://www.mozilla.org/MPL/2.0/"

    move-object v3, v1

    .line 1088
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MPL_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xd7

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MS_PL"

    const/16 v5, 0xd8

    const-string v6, "MS-PL"

    const-string v7, "Microsoft Public License"

    const-string v8, "http://www.microsoft.com/opensource/licenses.mspx"

    move-object v3, v1

    .line 1093
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MS_PL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xd8

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MS_RL"

    const/16 v5, 0xd9

    const-string v6, "MS-RL"

    const-string v7, "Microsoft Reciprocal License"

    const-string v8, "http://www.microsoft.com/opensource/licenses.mspx"

    move-object v3, v1

    .line 1098
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MS_RL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xd9

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MTLL"

    const/16 v5, 0xda

    const-string v6, "MTLL"

    const-string v7, "Matrix Template Library License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Matrix_Template_Library_License"

    move-object v3, v1

    .line 1103
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MTLL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xda

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MULTICS"

    const/16 v5, 0xdb

    const-string v6, "Multics"

    const-string v7, "Multics License"

    const-string v8, "http://www.opensource.org/licenses/Multics"

    move-object v3, v1

    .line 1108
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MULTICS:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xdb

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "MUP"

    const/16 v5, 0xdc

    const-string v6, "Mup"

    const-string v7, "Mup License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Mup"

    move-object v3, v1

    .line 1113
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->MUP:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xdc

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NASA_1_3"

    const/16 v5, 0xdd

    const-string v6, "NASA-1.3"

    const-string v7, "NASA Open Source Agreement 1.3"

    const-string v8, "http://ti.arc.nasa.gov/opensource/nosa/"

    move-object v3, v1

    .line 1118
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NASA_1_3:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xdd

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NAUMEN"

    const/16 v5, 0xde

    const-string v6, "Naumen"

    const-string v7, "Naumen Public License"

    const-string v8, "http://www.opensource.org/licenses/Naumen"

    move-object v3, v1

    .line 1123
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NAUMEN:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xde

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NBPL_1_0"

    const/16 v5, 0xdf

    const-string v6, "NBPL-1.0"

    const-string v7, "Net Boolean Public License v1"

    const-string v8, "http://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=blob;f=LICENSE;hb=37b4b3f6cc4bf34e1d3dec61e69914b9819d8894"

    move-object v3, v1

    .line 1128
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NBPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xdf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NCSA"

    const/16 v5, 0xe0

    const-string v6, "NCSA"

    const-string v7, "University of Illinois/NCSA Open Source License"

    const-string v8, "http://otm.illinois.edu/uiuc_openSource"

    move-object v3, v1

    .line 1133
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NCSA:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xe0

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NET_SNMP"

    const/16 v5, 0xe1

    const-string v6, "Net-SNMP"

    const-string v7, "Net-SNMP License"

    const-string v8, "http://net-snmp.sourceforge.net/about/license.html"

    move-object v3, v1

    .line 1138
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NET_SNMP:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xe1

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NETCDF"

    const/16 v5, 0xe2

    const-string v6, "NetCDF"

    const-string v7, "NetCDF license"

    const-string v8, "http://www.unidata.ucar.edu/software/netcdf/copyright.html"

    move-object v3, v1

    .line 1143
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NETCDF:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xe2

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NEWSLETR"

    const/16 v5, 0xe3

    const-string v6, "Newsletr"

    const-string v7, "Newsletr License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Newsletr"

    move-object v3, v1

    .line 1148
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NEWSLETR:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xe3

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NGPL"

    const/16 v5, 0xe4

    const-string v6, "NGPL"

    const-string v7, "Nethack General Public License"

    const-string v8, "http://www.opensource.org/licenses/NGPL"

    move-object v3, v1

    .line 1153
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NGPL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xe4

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NLOD_1_0"

    const/16 v5, 0xe5

    const-string v6, "NLOD-1.0"

    const-string v7, "Norwegian Licence for Open Government Data"

    const-string v8, "http://data.norge.no/nlod/en/1.0"

    move-object v3, v1

    .line 1158
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NLOD_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xe5

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NLPL"

    const/16 v5, 0xe6

    const-string v6, "NLPL"

    const-string v7, "No Limit Public License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/NLPL"

    move-object v3, v1

    .line 1163
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NLPL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xe6

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NOKIA"

    const/16 v5, 0xe7

    const-string v6, "Nokia"

    const-string v7, "Nokia Open Source License"

    const-string v8, "http://www.opensource.org/licenses/nokia"

    move-object v3, v1

    .line 1168
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NOKIA:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xe7

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NOSL"

    const/16 v5, 0xe8

    const-string v6, "NOSL"

    const-string v7, "Netizen Open Source License"

    const-string v8, "http://bits.netizen.com.au/licenses/NOSL/nosl.txt"

    move-object v3, v1

    .line 1173
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NOSL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xe8

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NOWEB"

    const/16 v5, 0xe9

    const-string v6, "Noweb"

    const-string v7, "Noweb License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Noweb"

    move-object v3, v1

    .line 1178
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NOWEB:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xe9

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NPL_1_0"

    const/16 v5, 0xea

    const-string v6, "NPL-1.0"

    const-string v7, "Netscape Public License v1.0"

    const-string v8, "http://www.mozilla.org/MPL/NPL/1.0/"

    move-object v3, v1

    .line 1183
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xea

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NPL_1_1"

    const/16 v5, 0xeb

    const-string v6, "NPL-1.1"

    const-string v7, "Netscape Public License v1.1"

    const-string v8, "http://www.mozilla.org/MPL/NPL/1.1/"

    move-object v3, v1

    .line 1188
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NPL_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xeb

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NPOSL_3_0"

    const/16 v5, 0xec

    const-string v6, "NPOSL-3.0"

    const-string v7, "Non-Profit Open Software License 3.0"

    const-string v8, "http://www.opensource.org/licenses/NOSL3.0"

    move-object v3, v1

    .line 1193
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NPOSL_3_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xec

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NRL"

    const/16 v5, 0xed

    const-string v6, "NRL"

    const-string v7, "NRL License"

    const-string v8, "http://web.mit.edu/network/isakmp/nrllicense.html"

    move-object v3, v1

    .line 1198
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NRL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xed

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NTP"

    const/16 v5, 0xee

    const-string v6, "NTP"

    const-string v7, "NTP License"

    const-string v8, "http://www.opensource.org/licenses/NTP"

    move-object v3, v1

    .line 1203
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NTP:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xee

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OCCT_PL"

    const/16 v5, 0xef

    const-string v6, "OCCT-PL"

    const-string v7, "Open CASCADE Technology Public License"

    const-string v8, "http://www.opencascade.com/content/occt-public-license"

    move-object v3, v1

    .line 1208
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OCCT_PL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xef

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OCLC_2_0"

    const/16 v5, 0xf0

    const-string v6, "OCLC-2.0"

    const-string v7, "OCLC Research Public License 2.0"

    const-string v8, "http://www.oclc.org/research/activities/software/license/v2final.htm"

    move-object v3, v1

    .line 1213
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OCLC_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xf0

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ODBL_1_0"

    const/16 v5, 0xf1

    const-string v6, "ODbL-1.0"

    const-string v7, "ODC Open Database License v1.0"

    const-string v8, "http://www.opendatacommons.org/licenses/odbl/1.0/"

    move-object v3, v1

    .line 1218
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ODBL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xf1

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OFL_1_0"

    const/16 v5, 0xf2

    const-string v6, "OFL-1.0"

    const-string v7, "SIL Open Font License 1.0"

    const-string v8, "http://scripts.sil.org/cms/scripts/page.php?item_id=OFL10_web"

    move-object v3, v1

    .line 1223
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OFL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xf2

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OFL_1_1"

    const/16 v5, 0xf3

    const-string v6, "OFL-1.1"

    const-string v7, "SIL Open Font License 1.1"

    const-string v8, "http://scripts.sil.org/cms/scripts/page.php?item_id=OFL_web"

    move-object v3, v1

    .line 1228
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OFL_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xf3

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OGTSL"

    const/16 v5, 0xf4

    const-string v6, "OGTSL"

    const-string v7, "Open Group Test Suite License"

    const-string v8, "http://www.opengroup.org/testing/downloads/The_Open_Group_TSL.txt"

    move-object v3, v1

    .line 1233
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OGTSL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xf4

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OLDAP_1_1"

    const/16 v5, 0xf5

    const-string v6, "OLDAP-1.1"

    const-string v7, "Open LDAP Public License v1.1"

    const-string v8, "http://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=blob;f=LICENSE;hb=806557a5ad59804ef3a44d5abfbe91d706b0791f"

    move-object v3, v1

    .line 1238
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OLDAP_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xf5

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OLDAP_1_2"

    const/16 v5, 0xf6

    const-string v6, "OLDAP-1.2"

    const-string v7, "Open LDAP Public License v1.2"

    const-string v8, "http://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=blob;f=LICENSE;hb=42b0383c50c299977b5893ee695cf4e486fb0dc7"

    move-object v3, v1

    .line 1243
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OLDAP_1_2:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xf6

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OLDAP_1_3"

    const/16 v5, 0xf7

    const-string v6, "OLDAP-1.3"

    const-string v7, "Open LDAP Public License v1.3"

    const-string v8, "http://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=blob;f=LICENSE;hb=e5f8117f0ce088d0bd7a8e18ddf37eaa40eb09b1"

    move-object v3, v1

    .line 1248
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OLDAP_1_3:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xf7

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OLDAP_1_4"

    const/16 v5, 0xf8

    const-string v6, "OLDAP-1.4"

    const-string v7, "Open LDAP Public License v1.4"

    const-string v8, "http://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=blob;f=LICENSE;hb=c9f95c2f3f2ffb5e0ae55fe7388af75547660941"

    move-object v3, v1

    .line 1253
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OLDAP_1_4:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xf8

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OLDAP_2_0_1"

    const/16 v5, 0xf9

    const-string v6, "OLDAP-2.0.1"

    const-string v7, "Open LDAP Public License v2.0.1"

    const-string v8, "http://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=blob;f=LICENSE;hb=b6d68acd14e51ca3aab4428bf26522aa74873f0e"

    move-object v3, v1

    .line 1258
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OLDAP_2_0_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xf9

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OLDAP_2_0"

    const/16 v5, 0xfa

    const-string v6, "OLDAP-2.0"

    const-string v7, "Open LDAP Public License v2.0 (or possibly 2.0A and 2.0B)"

    const-string v8, "http://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=blob;f=LICENSE;hb=cbf50f4e1185a21abd4c0a54d3f4341fe28f36ea"

    move-object v3, v1

    .line 1263
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OLDAP_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xfa

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OLDAP_2_1"

    const/16 v5, 0xfb

    const-string v6, "OLDAP-2.1"

    const-string v7, "Open LDAP Public License v2.1"

    const-string v8, "http://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=blob;f=LICENSE;hb=b0d176738e96a0d3b9f85cb51e140a86f21be715"

    move-object v3, v1

    .line 1268
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OLDAP_2_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xfb

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OLDAP_2_2_1"

    const/16 v5, 0xfc

    const-string v6, "OLDAP-2.2.1"

    const-string v7, "Open LDAP Public License v2.2.1"

    const-string v8, "http://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=blob;f=LICENSE;hb=4bc786f34b50aa301be6f5600f58a980070f481e"

    move-object v3, v1

    .line 1273
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OLDAP_2_2_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xfc

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OLDAP_2_2_2"

    const/16 v5, 0xfd

    const-string v6, "OLDAP-2.2.2"

    const-string v7, "Open LDAP Public License 2.2.2"

    const-string v8, "http://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=blob;f=LICENSE;hb=df2cc1e21eb7c160695f5b7cffd6296c151ba188"

    move-object v3, v1

    .line 1278
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OLDAP_2_2_2:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xfd

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OLDAP_2_2"

    const/16 v5, 0xfe

    const-string v6, "OLDAP-2.2"

    const-string v7, "Open LDAP Public License v2.2"

    const-string v8, "http://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=blob;f=LICENSE;hb=470b0c18ec67621c85881b2733057fecf4a1acc3"

    move-object v3, v1

    .line 1283
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OLDAP_2_2:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xfe

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OLDAP_2_3"

    const/16 v5, 0xff

    const-string v6, "OLDAP-2.3"

    const-string v7, "Open LDAP Public License v2.3"

    const-string v8, "http://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=blob;f=LICENSE;hb=d32cf54a32d581ab475d23c810b0a7fbaf8d63c3"

    move-object v3, v1

    .line 1288
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OLDAP_2_3:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0xff

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OLDAP_2_4"

    const/16 v5, 0x100

    const-string v6, "OLDAP-2.4"

    const-string v7, "Open LDAP Public License v2.4"

    const-string v8, "http://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=blob;f=LICENSE;hb=cd1284c4a91a8a380d904eee68d1583f989ed386"

    move-object v3, v1

    .line 1293
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OLDAP_2_4:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x100

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OLDAP_2_5"

    const/16 v5, 0x101

    const-string v6, "OLDAP-2.5"

    const-string v7, "Open LDAP Public License v2.5"

    const-string v8, "http://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=blob;f=LICENSE;hb=6852b9d90022e8593c98205413380536b1b5a7cf"

    move-object v3, v1

    .line 1298
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OLDAP_2_5:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x101

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OLDAP_2_6"

    const/16 v5, 0x102

    const-string v6, "OLDAP-2.6"

    const-string v7, "Open LDAP Public License v2.6"

    const-string v8, "http://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=blob;f=LICENSE;hb=1cae062821881f41b73012ba816434897abf4205"

    move-object v3, v1

    .line 1303
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OLDAP_2_6:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x102

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OLDAP_2_7"

    const/16 v5, 0x103

    const-string v6, "OLDAP-2.7"

    const-string v7, "Open LDAP Public License v2.7"

    const-string v8, "http://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=blob;f=LICENSE;hb=47c2415c1df81556eeb39be6cad458ef87c534a2"

    move-object v3, v1

    .line 1308
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OLDAP_2_7:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x103

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OLDAP_2_8"

    const/16 v5, 0x104

    const-string v6, "OLDAP-2.8"

    const-string v7, "Open LDAP Public License v2.8"

    const-string v8, "http://www.openldap.org/software/release/license.html"

    move-object v3, v1

    .line 1313
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OLDAP_2_8:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x104

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OML"

    const/16 v5, 0x105

    const-string v6, "OML"

    const-string v7, "Open Market License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Open_Market_License"

    move-object v3, v1

    .line 1318
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OML:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x105

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OPENSSL"

    const/16 v5, 0x106

    const-string v6, "OpenSSL"

    const-string v7, "OpenSSL License"

    const-string v8, "http://www.openssl.org/source/license.html"

    move-object v3, v1

    .line 1323
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OPENSSL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x106

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OPL_1_0"

    const/16 v5, 0x107

    const-string v6, "OPL-1.0"

    const-string v7, "Open Public License v1.0"

    const-string v8, "http://old.koalateam.com/jackaroo/OPL_1_0.TXT"

    move-object v3, v1

    .line 1328
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x107

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OSET_PL_2_1"

    const/16 v5, 0x108

    const-string v6, "OSET-PL-2.1"

    const-string v7, "OSET Public License version 2.1"

    const-string v8, "http://www.osetfoundation.org/public-license"

    move-object v3, v1

    .line 1333
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OSET_PL_2_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x108

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OSL_1_0"

    const/16 v5, 0x109

    const-string v6, "OSL-1.0"

    const-string v7, "Open Software License 1.0"

    const-string v8, "http://opensource.org/licenses/OSL-1.0"

    move-object v3, v1

    .line 1338
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OSL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x109

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OSL_1_1"

    const/16 v5, 0x10a

    const-string v6, "OSL-1.1"

    const-string v7, "Open Software License 1.1"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/OSL1.1"

    move-object v3, v1

    .line 1343
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OSL_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x10a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OSL_2_0"

    const/16 v5, 0x10b

    const-string v6, "OSL-2.0"

    const-string v7, "Open Software License 2.0"

    const-string v8, "http://web.archive.org/web/20041020171434/http://www.rosenlaw.com/osl2.0.html"

    move-object v3, v1

    .line 1348
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OSL_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x10b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OSL_2_1"

    const/16 v5, 0x10c

    const-string v6, "OSL-2.1"

    const-string v7, "Open Software License 2.1"

    const-string v8, "http://web.archive.org/web/20050212003940/http://www.rosenlaw.com/osl21.htm"

    move-object v3, v1

    .line 1353
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OSL_2_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x10c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "OSL_3_0"

    const/16 v5, 0x10d

    const-string v6, "OSL-3.0"

    const-string v7, "Open Software License 3.0"

    const-string v8, "http://www.rosenlaw.com/OSL3.0.htm"

    move-object v3, v1

    .line 1358
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->OSL_3_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x10d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "PDDL_1_0"

    const/16 v5, 0x10e

    const-string v6, "PDDL-1.0"

    const-string v7, "ODC Public Domain Dedication & License 1.0"

    const-string v8, "http://opendatacommons.org/licenses/pddl/1.0/"

    move-object v3, v1

    .line 1363
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->PDDL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x10e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "PHP_3_0"

    const/16 v5, 0x10f

    const-string v6, "PHP-3.0"

    const-string v7, "PHP License v3.0"

    const-string v8, "http://www.php.net/license/3_0.txt"

    move-object v3, v1

    .line 1368
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->PHP_3_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x10f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "PHP_3_01"

    const/16 v5, 0x110

    const-string v6, "PHP-3.01"

    const-string v7, "PHP License v3.01"

    const-string v8, "http://www.php.net/license/3_01.txt"

    move-object v3, v1

    .line 1373
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->PHP_3_01:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x110

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "PLEXUS"

    const/16 v5, 0x111

    const-string v6, "Plexus"

    const-string v7, "Plexus Classworlds License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Plexus_Classworlds_License"

    move-object v3, v1

    .line 1378
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->PLEXUS:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x111

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "POSTGRESQL"

    const/16 v5, 0x112

    const-string v6, "PostgreSQL"

    const-string v7, "PostgreSQL License"

    const-string v8, "http://www.postgresql.org/about/licence"

    move-object v3, v1

    .line 1383
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->POSTGRESQL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x112

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "PSFRAG"

    const/16 v5, 0x113

    const-string v6, "psfrag"

    const-string v7, "psfrag License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/psfrag"

    move-object v3, v1

    .line 1388
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->PSFRAG:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x113

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "PSUTILS"

    const/16 v5, 0x114

    const-string v6, "psutils"

    const-string v7, "psutils License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/psutils"

    move-object v3, v1

    .line 1393
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->PSUTILS:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x114

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "PYTHON_2_0"

    const/16 v5, 0x115

    const-string v6, "Python-2.0"

    const-string v7, "Python License 2.0"

    const-string v8, "http://www.opensource.org/licenses/Python-2.0"

    move-object v3, v1

    .line 1398
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->PYTHON_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x115

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "QHULL"

    const/16 v5, 0x116

    const-string v6, "Qhull"

    const-string v7, "Qhull License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Qhull"

    move-object v3, v1

    .line 1403
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->QHULL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x116

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "QPL_1_0"

    const/16 v5, 0x117

    const-string v6, "QPL-1.0"

    const-string v7, "Q Public License 1.0"

    const-string v8, "http://doc.qt.nokia.com/3.3/license.html"

    move-object v3, v1

    .line 1408
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->QPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x117

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "RDISC"

    const/16 v5, 0x118

    const-string v6, "Rdisc"

    const-string v7, "Rdisc License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Rdisc_License"

    move-object v3, v1

    .line 1413
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->RDISC:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x118

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "RHECOS_1_1"

    const/16 v5, 0x119

    const-string v6, "RHeCos-1.1"

    const-string v7, "Red Hat eCos Public License v1.1"

    const-string v8, "http://ecos.sourceware.org/old-license.html"

    move-object v3, v1

    .line 1418
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->RHECOS_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x119

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "RPL_1_1"

    const/16 v5, 0x11a

    const-string v6, "RPL-1.1"

    const-string v7, "Reciprocal Public License 1.1"

    const-string v8, "http://opensource.org/licenses/RPL-1.1"

    move-object v3, v1

    .line 1423
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->RPL_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x11a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "RPL_1_5"

    const/16 v5, 0x11b

    const-string v6, "RPL-1.5"

    const-string v7, "Reciprocal Public License 1.5"

    const-string v8, "http://www.opensource.org/licenses/RPL-1.5"

    move-object v3, v1

    .line 1428
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->RPL_1_5:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x11b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "RPSL_1_0"

    const/16 v5, 0x11c

    const-string v6, "RPSL-1.0"

    const-string v7, "RealNetworks Public Source License v1.0"

    const-string v8, "https://helixcommunity.org/content/rpsl"

    move-object v3, v1

    .line 1433
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->RPSL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x11c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "RSA_MD"

    const/16 v5, 0x11d

    const-string v6, "RSA-MD"

    const-string v7, "RSA Message-Digest License "

    const-string v8, "http://www.faqs.org/rfcs/rfc1321.html"

    move-object v3, v1

    .line 1438
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->RSA_MD:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x11d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "RSCPL"

    const/16 v5, 0x11e

    const-string v6, "RSCPL"

    const-string v7, "Ricoh Source Code Public License"

    const-string v8, "http://wayback.archive.org/web/20060715140826/http://www.risource.org/RPL/RPL-1.0A.shtml"

    move-object v3, v1

    .line 1443
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->RSCPL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x11e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "RUBY"

    const/16 v5, 0x11f

    const-string v6, "Ruby"

    const-string v7, "Ruby License"

    const-string v8, "http://www.ruby-lang.org/en/LICENSE.txt"

    move-object v3, v1

    .line 1448
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->RUBY:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x11f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SAX_PD"

    const/16 v5, 0x120

    const-string v6, "SAX-PD"

    const-string v7, "Sax Public Domain Notice"

    const-string v8, "http://www.saxproject.org/copying.html"

    move-object v3, v1

    .line 1453
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SAX_PD:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x120

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SAXPATH"

    const/16 v5, 0x121

    const-string v6, "Saxpath"

    const-string v7, "Saxpath License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Saxpath_License"

    move-object v3, v1

    .line 1458
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SAXPATH:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x121

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SCEA"

    const/16 v5, 0x122

    const-string v6, "SCEA"

    const-string v7, "SCEA Shared Source License"

    const-string v8, "http://research.scea.com/scea_shared_source_license.html"

    move-object v3, v1

    .line 1463
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SCEA:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x122

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SENDMAIL"

    const/16 v5, 0x123

    const-string v6, "Sendmail"

    const-string v7, "Sendmail License"

    const-string v8, "http://www.sendmail.com/pdfs/open_source/sendmail_license.pdf"

    move-object v3, v1

    .line 1468
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SENDMAIL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x123

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SGI_B_1_0"

    const/16 v5, 0x124

    const-string v6, "SGI-B-1.0"

    const-string v7, "SGI Free Software License B v1.0"

    const-string v8, "http://oss.sgi.com/projects/FreeB/SGIFreeSWLicB.1.0.html"

    move-object v3, v1

    .line 1473
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SGI_B_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x124

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SGI_B_1_1"

    const/16 v5, 0x125

    const-string v6, "SGI-B-1.1"

    const-string v7, "SGI Free Software License B v1.1"

    const-string v8, "http://oss.sgi.com/projects/FreeB/"

    move-object v3, v1

    .line 1478
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SGI_B_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x125

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SGI_B_2_0"

    const/16 v5, 0x126

    const-string v6, "SGI-B-2.0"

    const-string v7, "SGI Free Software License B v2.0"

    const-string v8, "http://oss.sgi.com/projects/FreeB/SGIFreeSWLicB.2.0.pdf"

    move-object v3, v1

    .line 1483
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SGI_B_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x126

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SIMPL_2_0"

    const/16 v5, 0x127

    const-string v6, "SimPL-2.0"

    const-string v7, "Simple Public License 2.0"

    const-string v8, "http://www.opensource.org/licenses/SimPL-2.0"

    move-object v3, v1

    .line 1488
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SIMPL_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x127

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SISSL_1_2"

    const/16 v5, 0x128

    const-string v6, "SISSL-1.2"

    const-string v7, "Sun Industry Standards Source License v1.2"

    const-string v8, "http://gridscheduler.sourceforge.net/Gridengine_SISSL_license.html"

    move-object v3, v1

    .line 1493
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SISSL_1_2:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x128

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SISSL"

    const/16 v5, 0x129

    const-string v6, "SISSL"

    const-string v7, "Sun Industry Standards Source License v1.1"

    const-string v8, "http://www.openoffice.org/licenses/sissl_license.html"

    move-object v3, v1

    .line 1498
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SISSL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x129

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SLEEPYCAT"

    const/16 v5, 0x12a

    const-string v6, "Sleepycat"

    const-string v7, "Sleepycat License"

    const-string v8, "http://www.opensource.org/licenses/Sleepycat"

    move-object v3, v1

    .line 1503
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SLEEPYCAT:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x12a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SMLNJ"

    const/16 v5, 0x12b

    const-string v6, "SMLNJ"

    const-string v7, "Standard ML of New Jersey License"

    const-string v8, "https://www.smlnj.org/license.html"

    move-object v3, v1

    .line 1508
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SMLNJ:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x12b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SMPPL"

    const/16 v5, 0x12c

    const-string v6, "SMPPL"

    const-string v7, "Secure Messaging Protocol Public License"

    const-string v8, "https://github.com/dcblake/SMP/blob/master/Documentation/License.txt"

    move-object v3, v1

    .line 1513
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SMPPL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x12c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SNIA"

    const/16 v5, 0x12d

    const-string v6, "SNIA"

    const-string v7, "SNIA Public License 1.1"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/SNIA_Public_License"

    move-object v3, v1

    .line 1518
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SNIA:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x12d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SPENCER_86"

    const/16 v5, 0x12e

    const-string v6, "Spencer-86"

    const-string v7, "Spencer License 86"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Henry_Spencer_Reg-Ex_Library_License"

    move-object v3, v1

    .line 1523
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SPENCER_86:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x12e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SPENCER_94"

    const/16 v5, 0x12f

    const-string v6, "Spencer-94"

    const-string v7, "Spencer License 94"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Henry_Spencer_Reg-Ex_Library_License"

    move-object v3, v1

    .line 1528
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SPENCER_94:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x12f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SPENCER_99"

    const/16 v5, 0x130

    const-string v6, "Spencer-99"

    const-string v7, "Spencer License 99"

    const-string v8, "http://www.opensource.apple.com/source/tcl/tcl-5/tcl/generic/regfronts.c"

    move-object v3, v1

    .line 1533
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SPENCER_99:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x130

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SPL_1_0"

    const/16 v5, 0x131

    const-string v6, "SPL-1.0"

    const-string v7, "Sun Public License v1.0"

    const-string v8, "http://www.opensource.org/licenses/SPL-1.0"

    move-object v3, v1

    .line 1538
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x131

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SUGARCRM_1_1_3"

    const/16 v5, 0x132

    const-string v6, "SugarCRM-1.1.3"

    const-string v7, "SugarCRM Public License v1.1.3"

    const-string v8, "http://www.sugarcrm.com/crm/SPL"

    move-object v3, v1

    .line 1543
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SUGARCRM_1_1_3:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x132

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "SWL"

    const/16 v5, 0x133

    const-string v6, "SWL"

    const-string v7, "Scheme Widget Library (SWL) Software License Agreement"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/SWL"

    move-object v3, v1

    .line 1548
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->SWL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x133

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "TCL"

    const/16 v5, 0x134

    const-string v6, "TCL"

    const-string v7, "TCL/TK License"

    const-string v8, "http://www.tcl.tk/software/tcltk/license.html"

    move-object v3, v1

    .line 1553
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->TCL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x134

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "TCP_WRAPPERS"

    const/16 v5, 0x135

    const-string v6, "TCP-wrappers"

    const-string v7, "TCP Wrappers License"

    const-string v8, "http://rc.quest.com/topics/openssh/license.php#tcpwrappers"

    move-object v3, v1

    .line 1558
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->TCP_WRAPPERS:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x135

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "TMATE"

    const/16 v5, 0x136

    const-string v6, "TMate"

    const-string v7, "TMate Open Source License"

    const-string v8, "http://svnkit.com/license.html"

    move-object v3, v1

    .line 1563
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->TMATE:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x136

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "TORQUE_1_1"

    const/16 v5, 0x137

    const-string v6, "TORQUE-1.1"

    const-string v7, "TORQUE v2.5+ Software License v1.1"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/TORQUEv1.1"

    move-object v3, v1

    .line 1568
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->TORQUE_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x137

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "TOSL"

    const/16 v5, 0x138

    const-string v6, "TOSL"

    const-string v7, "Trusster Open Source License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/TOSL"

    move-object v3, v1

    .line 1573
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->TOSL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x138

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "UNICODE_DFS_2015"

    const/16 v5, 0x139

    const-string v6, "Unicode-DFS-2015"

    const-string v7, "Unicode License Agreement - Data Files and Software (2015)"

    const-string v8, "https://web.archive.org/web/20151224134844/http://unicode.org/copyright.html"

    move-object v3, v1

    .line 1578
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->UNICODE_DFS_2015:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x139

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "UNICODE_DFS_2016"

    const/16 v5, 0x13a

    const-string v6, "Unicode-DFS-2016"

    const-string v7, "Unicode License Agreement - Data Files and Software (2016)"

    const-string v8, "http://www.unicode.org/copyright.html"

    move-object v3, v1

    .line 1583
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->UNICODE_DFS_2016:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x13a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "UNICODE_TOU"

    const/16 v5, 0x13b

    const-string v6, "Unicode-TOU"

    const-string v7, "Unicode Terms of Use"

    const-string v8, "http://www.unicode.org/copyright.html"

    move-object v3, v1

    .line 1588
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->UNICODE_TOU:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x13b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "UNLICENSE"

    const/16 v5, 0x13c

    const-string v6, "Unlicense"

    const-string v7, "The Unlicense"

    const-string v8, "http://unlicense.org/"

    move-object v3, v1

    .line 1593
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->UNLICENSE:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x13c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "UPL_1_0"

    const/16 v5, 0x13d

    const-string v6, "UPL-1.0"

    const-string v7, "Universal Permissive License v1.0"

    const-string v8, "http://opensource.org/licenses/UPL"

    move-object v3, v1

    .line 1598
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->UPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x13d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "VIM"

    const/16 v5, 0x13e

    const-string v6, "Vim"

    const-string v7, "Vim License"

    const-string v8, "http://vimdoc.sourceforge.net/htmldoc/uganda.html"

    move-object v3, v1

    .line 1603
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->VIM:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x13e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "VOSTROM"

    const/16 v5, 0x13f

    const-string v6, "VOSTROM"

    const-string v7, "VOSTROM Public License for Open Source"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/VOSTROM"

    move-object v3, v1

    .line 1608
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->VOSTROM:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x13f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "VSL_1_0"

    const/16 v5, 0x140

    const-string v6, "VSL-1.0"

    const-string v7, "Vovida Software License v1.0"

    const-string v8, "http://www.opensource.org/licenses/VSL-1.0"

    move-object v3, v1

    .line 1613
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->VSL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x140

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "W3C_19980720"

    const/16 v5, 0x141

    const-string v6, "W3C-19980720"

    const-string v7, "W3C Software Notice and License (1998-07-20)"

    const-string v8, "http://www.w3.org/Consortium/Legal/copyright-software-19980720.html"

    move-object v3, v1

    .line 1618
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->W3C_19980720:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x141

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "W3C_20150513"

    const/16 v5, 0x142

    const-string v6, "W3C-20150513"

    const-string v7, "W3C Software Notice and Document License (2015-05-13)"

    const-string v8, "https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document"

    move-object v3, v1

    .line 1623
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->W3C_20150513:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x142

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "W3C"

    const/16 v5, 0x143

    const-string v6, "W3C"

    const-string v7, "W3C Software Notice and License (2002-12-31)"

    const-string v8, "http://www.w3.org/Consortium/Legal/2002/copyright-software-20021231.html"

    move-object v3, v1

    .line 1628
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->W3C:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x143

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "WATCOM_1_0"

    const/16 v5, 0x144

    const-string v6, "Watcom-1.0"

    const-string v7, "Sybase Open Watcom Public License 1.0"

    const-string v8, "http://www.opensource.org/licenses/Watcom-1.0"

    move-object v3, v1

    .line 1633
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->WATCOM_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x144

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "WSUIPA"

    const/16 v5, 0x145

    const-string v6, "Wsuipa"

    const-string v7, "Wsuipa License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Wsuipa"

    move-object v3, v1

    .line 1638
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->WSUIPA:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x145

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "WTFPL"

    const/16 v5, 0x146

    const-string v6, "WTFPL"

    const-string v7, "Do What The F*ck You Want To Public License"

    const-string v8, "http://sam.zoy.org/wtfpl/COPYING"

    move-object v3, v1

    .line 1643
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->WTFPL:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x146

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "X11"

    const/16 v5, 0x147

    const-string v6, "X11"

    const-string v7, "X11 License"

    const-string v8, "http://www.xfree86.org/3.3.6/COPYRIGHT2.html#3"

    move-object v3, v1

    .line 1648
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->X11:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x147

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "XEROX"

    const/16 v5, 0x148

    const-string v6, "Xerox"

    const-string v7, "Xerox License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Xerox"

    move-object v3, v1

    .line 1653
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->XEROX:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x148

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "XFREE86_1_1"

    const/16 v5, 0x149

    const-string v6, "XFree86-1.1"

    const-string v7, "XFree86 License 1.1"

    const-string v8, "http://www.xfree86.org/current/LICENSE4.html"

    move-object v3, v1

    .line 1658
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->XFREE86_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x149

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "XINETD"

    const/16 v5, 0x14a

    const-string/jumbo v6, "xinetd"

    const-string/jumbo v7, "xinetd License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Xinetd_License"

    move-object v3, v1

    .line 1663
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->XINETD:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x14a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "XNET"

    const/16 v5, 0x14b

    const-string v6, "Xnet"

    const-string v7, "X.Net License"

    const-string v8, "http://opensource.org/licenses/Xnet"

    move-object v3, v1

    .line 1668
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->XNET:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x14b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "XPP"

    const/16 v5, 0x14c

    const-string/jumbo v6, "xpp"

    const-string v7, "XPP License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/xpp"

    move-object v3, v1

    .line 1673
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->XPP:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x14c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "XSKAT"

    const/16 v5, 0x14d

    const-string v6, "XSkat"

    const-string v7, "XSkat License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/XSkat_License"

    move-object v3, v1

    .line 1678
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->XSKAT:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x14d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "YPL_1_0"

    const/16 v5, 0x14e

    const-string v6, "YPL-1.0"

    const-string v7, "Yahoo! Public License v1.0"

    const-string v8, "http://www.zimbra.com/license/yahoo_public_license_1.0.html"

    move-object v3, v1

    .line 1683
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->YPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x14e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "YPL_1_1"

    const/16 v5, 0x14f

    const-string v6, "YPL-1.1"

    const-string v7, "Yahoo! Public License v1.1"

    const-string v8, "http://www.zimbra.com/license/yahoo_public_license_1.1.html"

    move-object v3, v1

    .line 1688
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->YPL_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x14f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ZED"

    const/16 v5, 0x150

    const-string v6, "Zed"

    const-string v7, "Zed License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Zed"

    move-object v3, v1

    .line 1693
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ZED:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x150

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ZEND_2_0"

    const/16 v5, 0x151

    const-string v6, "Zend-2.0"

    const-string v7, "Zend License v2.0"

    const-string v8, "https://web.archive.org/web/20130517195954/http://www.zend.com/license/2_00.txt"

    move-object v3, v1

    .line 1698
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ZEND_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x151

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ZIMBRA_1_3"

    const/16 v5, 0x152

    const-string v6, "Zimbra-1.3"

    const-string v7, "Zimbra Public License v1.3"

    const-string v8, "http://web.archive.org/web/20100302225219/http://www.zimbra.com/license/zimbra-public-license-1-3.html"

    move-object v3, v1

    .line 1703
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ZIMBRA_1_3:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x152

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ZIMBRA_1_4"

    const/16 v5, 0x153

    const-string v6, "Zimbra-1.4"

    const-string v7, "Zimbra Public License v1.4"

    const-string v8, "http://www.zimbra.com/legal/zimbra-public-license-1-4"

    move-object v3, v1

    .line 1708
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ZIMBRA_1_4:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x153

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ZLIB_ACKNOWLEDGEMENT"

    const/16 v5, 0x154

    const-string/jumbo v6, "zlib-acknowledgement"

    const-string/jumbo v7, "zlib/libpng License with Acknowledgement"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/ZlibWithAcknowledgement"

    move-object v3, v1

    .line 1713
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ZLIB_ACKNOWLEDGEMENT:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x154

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ZLIB"

    const/16 v5, 0x155

    const-string v6, "Zlib"

    const-string/jumbo v7, "zlib License"

    const-string v8, "http://www.zlib.net/zlib_license.html"

    move-object v3, v1

    .line 1718
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ZLIB:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x155

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ZPL_1_1"

    const/16 v5, 0x156

    const-string v6, "ZPL-1.1"

    const-string v7, "Zope Public License 1.1"

    const-string v8, "http://old.zope.org/Resources/License/ZPL-1.1"

    move-object v3, v1

    .line 1723
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ZPL_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x156

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ZPL_2_0"

    const/16 v5, 0x157

    const-string v6, "ZPL-2.0"

    const-string v7, "Zope Public License 2.0"

    const-string v8, "http://old.zope.org/Resources/License/ZPL-2.0"

    move-object v3, v1

    .line 1728
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ZPL_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x157

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ZPL_2_1"

    const/16 v5, 0x158

    const-string v6, "ZPL-2.1"

    const-string v7, "Zope Public License 2.1"

    const-string v8, "http://old.zope.org/Resources/ZPL/"

    move-object v3, v1

    .line 1733
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ZPL_2_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x158

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "AGPL_1_0"

    const/16 v5, 0x159

    const-string v6, "AGPL-1.0"

    const-string v7, "Affero General Public License v1.0"

    const-string v8, "http://www.affero.org/oagpl.html"

    move-object v3, v1

    .line 1738
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->AGPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x159

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "AGPL_3_0"

    const/16 v5, 0x15a

    const-string v6, "AGPL-3.0"

    const-string v7, "GNU Affero General Public License v3.0"

    const-string v8, "http://www.gnu.org/licenses/agpl.txt"

    move-object v3, v1

    .line 1743
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->AGPL_3_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x15a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "ECOS_2_0"

    const/16 v5, 0x15b

    const-string v6, "eCos-2.0"

    const-string v7, "eCos license version 2.0"

    const-string v8, "http://www.gnu.org/licenses/ecos-license.html"

    move-object v3, v1

    .line 1748
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->ECOS_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x15b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GFDL_1_1"

    const/16 v5, 0x15c

    const-string v6, "GFDL-1.1"

    const-string v7, "GNU Free Documentation License v1.1"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/fdl-1.1.txt"

    move-object v3, v1

    .line 1753
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GFDL_1_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x15c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GFDL_1_2"

    const/16 v5, 0x15d

    const-string v6, "GFDL-1.2"

    const-string v7, "GNU Free Documentation License v1.2"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/fdl-1.2.txt"

    move-object v3, v1

    .line 1758
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GFDL_1_2:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x15d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GFDL_1_3"

    const/16 v5, 0x15e

    const-string v6, "GFDL-1.3"

    const-string v7, "GNU Free Documentation License v1.3"

    const-string v8, "http://www.gnu.org/licenses/fdl-1.3.txt"

    move-object v3, v1

    .line 1763
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GFDL_1_3:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x15e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_1_0_PLUS"

    const/16 v5, 0x15f

    const-string v6, "GPL-1.0+"

    const-string v7, "GNU General Public License v1.0 or later"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/gpl-1.0-standalone.html"

    move-object v3, v1

    .line 1768
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_1_0_PLUS:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x15f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_1_0"

    const/16 v5, 0x160

    const-string v6, "GPL-1.0"

    const-string v7, "GNU General Public License v1.0 only"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/gpl-1.0-standalone.html"

    move-object v3, v1

    .line 1773
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_1_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x160

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_2_0_PLUS"

    const/16 v5, 0x161

    const-string v6, "GPL-2.0+"

    const-string v7, "GNU General Public License v2.0 or later"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html"

    move-object v3, v1

    .line 1778
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_2_0_PLUS:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x161

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_2_0_WITH_AUTOCONF_EXCEPTION"

    const/16 v5, 0x162

    const-string v6, "GPL-2.0-with-autoconf-exception"

    const-string v7, "GNU General Public License v2.0 w/Autoconf exception"

    const-string v8, "http://ac-archive.sourceforge.net/doc/copyright.html"

    move-object v3, v1

    .line 1783
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_2_0_WITH_AUTOCONF_EXCEPTION:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x162

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_2_0_WITH_BISON_EXCEPTION"

    const/16 v5, 0x163

    const-string v6, "GPL-2.0-with-bison-exception"

    const-string v7, "GNU General Public License v2.0 w/Bison exception"

    const-string v8, "http://git.savannah.gnu.org/cgit/bison.git/tree/data/yacc.c?id=193d7c7054ba7197b0789e14965b739162319b5e#n141"

    move-object v3, v1

    .line 1788
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_2_0_WITH_BISON_EXCEPTION:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x163

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_2_0_WITH_CLASSPATH_EXCEPTION"

    const/16 v5, 0x164

    const-string v6, "GPL-2.0-with-classpath-exception"

    const-string v7, "GNU General Public License v2.0 w/Classpath exception"

    const-string v8, "http://www.gnu.org/software/classpath/license.html"

    move-object v3, v1

    .line 1793
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_2_0_WITH_CLASSPATH_EXCEPTION:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x164

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_2_0_WITH_FONT_EXCEPTION"

    const/16 v5, 0x165

    const-string v6, "GPL-2.0-with-font-exception"

    const-string v7, "GNU General Public License v2.0 w/Font exception"

    const-string v8, "http://www.gnu.org/licenses/gpl-faq.html#FontException"

    move-object v3, v1

    .line 1798
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_2_0_WITH_FONT_EXCEPTION:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x165

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_2_0_WITH_GCC_EXCEPTION"

    const/16 v5, 0x166

    const-string v6, "GPL-2.0-with-GCC-exception"

    const-string v7, "GNU General Public License v2.0 w/GCC Runtime Library exception"

    const-string v8, "https://gcc.gnu.org/git/?p=gcc.git;a=blob;f=gcc/libgcc1.c;h=762f5143fc6eed57b6797c82710f3538aa52b40b;hb=cb143a3ce4fb417c68f5fa2691a1b1b1053dfba9#l10"

    move-object v3, v1

    .line 1803
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_2_0_WITH_GCC_EXCEPTION:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x166

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_2_0"

    const/16 v5, 0x167

    const-string v6, "GPL-2.0"

    const-string v7, "GNU General Public License v2.0 only"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html"

    move-object v3, v1

    .line 1808
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x167

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_3_0_PLUS"

    const/16 v5, 0x168

    const-string v6, "GPL-3.0+"

    const-string v7, "GNU General Public License v3.0 or later"

    const-string v8, "http://www.gnu.org/licenses/gpl-3.0-standalone.html"

    move-object v3, v1

    .line 1813
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_3_0_PLUS:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x168

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_3_0_WITH_AUTOCONF_EXCEPTION"

    const/16 v5, 0x169

    const-string v6, "GPL-3.0-with-autoconf-exception"

    const-string v7, "GNU General Public License v3.0 w/Autoconf exception"

    const-string v8, "http://www.gnu.org/licenses/autoconf-exception-3.0.html"

    move-object v3, v1

    .line 1818
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_3_0_WITH_AUTOCONF_EXCEPTION:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x169

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_3_0_WITH_GCC_EXCEPTION"

    const/16 v5, 0x16a

    const-string v6, "GPL-3.0-with-GCC-exception"

    const-string v7, "GNU General Public License v3.0 w/GCC Runtime Library exception"

    const-string v8, "http://www.gnu.org/licenses/gcc-exception-3.1.html"

    move-object v3, v1

    .line 1823
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_3_0_WITH_GCC_EXCEPTION:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x16a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "GPL_3_0"

    const/16 v5, 0x16b

    const-string v6, "GPL-3.0"

    const-string v7, "GNU General Public License v3.0 only"

    const-string v8, "http://www.gnu.org/licenses/gpl-3.0-standalone.html"

    move-object v3, v1

    .line 1828
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->GPL_3_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x16b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LGPL_2_0_PLUS"

    const/16 v5, 0x16c

    const-string v6, "LGPL-2.0+"

    const-string v7, "GNU Library General Public License v2 or later"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/lgpl-2.0-standalone.html"

    move-object v3, v1

    .line 1833
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LGPL_2_0_PLUS:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x16c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LGPL_2_0"

    const/16 v5, 0x16d

    const-string v6, "LGPL-2.0"

    const-string v7, "GNU Library General Public License v2 only"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/lgpl-2.0-standalone.html"

    move-object v3, v1

    .line 1838
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LGPL_2_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x16d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LGPL_2_1_PLUS"

    const/16 v5, 0x16e

    const-string v6, "LGPL-2.1+"

    const-string v7, "GNU Library General Public License v2.1 or later"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/lgpl-2.1-standalone.html"

    move-object v3, v1

    .line 1843
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LGPL_2_1_PLUS:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x16e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LGPL_2_1"

    const/16 v5, 0x16f

    const-string v6, "LGPL-2.1"

    const-string v7, "GNU Lesser General Public License v2.1 only"

    const-string v8, "http://www.gnu.org/licenses/old-licenses/lgpl-2.1-standalone.html"

    move-object v3, v1

    .line 1848
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LGPL_2_1:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x16f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LGPL_3_0_PLUS"

    const/16 v5, 0x170

    const-string v6, "LGPL-3.0+"

    const-string v7, "GNU Lesser General Public License v3.0 or later"

    const-string v8, "http://www.gnu.org/licenses/lgpl-3.0-standalone.html"

    move-object v3, v1

    .line 1853
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LGPL_3_0_PLUS:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x170

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "LGPL_3_0"

    const/16 v5, 0x171

    const-string v6, "LGPL-3.0"

    const-string v7, "GNU Lesser General Public License v3.0 only"

    const-string v8, "http://www.gnu.org/licenses/lgpl-3.0-standalone.html"

    move-object v3, v1

    .line 1858
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->LGPL_3_0:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x171

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "NUNIT"

    const/16 v5, 0x172

    const-string v6, "Nunit"

    const-string v7, "Nunit License"

    const-string v8, "https://fedoraproject.org/wiki/Licensing/Nunit"

    move-object v3, v1

    .line 1863
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->NUNIT:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x172

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "STANDARDML_NJ"

    const/16 v5, 0x173

    const-string v6, "StandardML-NJ"

    const-string v7, "Standard ML of New Jersey License"

    const-string v8, "http://www.smlnj.org//license.html"

    move-object v3, v1

    .line 1868
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->STANDARDML_NJ:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x173

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/License;

    const-string v4, "WXWINDOWS"

    const/16 v5, 0x174

    const-string/jumbo v6, "wxWindows"

    const-string/jumbo v7, "wxWindows Library License"

    const-string v8, "http://www.opensource.org/licenses/WXwindows"

    move-object v3, v1

    .line 1873
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/help/about/License;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/help/about/License;->WXWINDOWS:Lcom/squareup/ui/help/about/License;

    const/16 v2, 0x174

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/help/about/License;->$VALUES:[Lcom/squareup/ui/help/about/License;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/ui/help/about/License;->id:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/ui/help/about/License;->licenseName:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/ui/help/about/License;->url:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/help/about/License;
    .locals 1

    const-class v0, Lcom/squareup/ui/help/about/License;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/help/about/License;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/help/about/License;
    .locals 1

    sget-object v0, Lcom/squareup/ui/help/about/License;->$VALUES:[Lcom/squareup/ui/help/about/License;

    invoke-virtual {v0}, [Lcom/squareup/ui/help/about/License;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/help/about/License;

    return-object v0
.end method


# virtual methods
.method public final getId()Ljava/lang/String;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/ui/help/about/License;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final getLicenseName()Ljava/lang/String;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/ui/help/about/License;->licenseName:Ljava/lang/String;

    return-object v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/ui/help/about/License;->url:Ljava/lang/String;

    return-object v0
.end method
