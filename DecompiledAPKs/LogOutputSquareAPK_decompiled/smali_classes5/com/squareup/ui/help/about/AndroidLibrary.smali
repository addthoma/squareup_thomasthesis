.class public final Lcom/squareup/ui/help/about/AndroidLibrary;
.super Ljava/lang/Object;
.source "AndroidLibrary.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0010\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/help/about/AndroidLibrary;",
        "",
        "name",
        "",
        "author",
        "url",
        "license",
        "Lcom/squareup/ui/help/about/License;",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final author:Ljava/lang/String;

.field public final license:Lcom/squareup/ui/help/about/License;

.field public final name:Ljava/lang/String;

.field public final url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "author"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "url"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "license"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/about/AndroidLibrary;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/ui/help/about/AndroidLibrary;->author:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/help/about/AndroidLibrary;->url:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/ui/help/about/AndroidLibrary;->license:Lcom/squareup/ui/help/about/License;

    .line 17
    iget-object p1, p0, Lcom/squareup/ui/help/about/AndroidLibrary;->url:Ljava/lang/String;

    const-string p2, "https"

    const/4 p3, 0x0

    const/4 p4, 0x2

    const/4 v0, 0x0

    invoke-static {p1, p2, p3, p4, v0}, Lkotlin/text/StringsKt;->startsWith$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result p1

    const-string p2, "URLs must start with https"

    invoke-static {p1, p2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    return-void
.end method
