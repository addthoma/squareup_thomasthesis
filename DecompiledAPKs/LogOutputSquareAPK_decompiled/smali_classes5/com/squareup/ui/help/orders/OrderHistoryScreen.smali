.class public final Lcom/squareup/ui/help/orders/OrderHistoryScreen;
.super Lcom/squareup/ui/help/InHelpAppletScope;
.source "OrderHistoryScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/help/orders/OrderHistoryScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/orders/OrderHistoryScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/help/orders/OrderHistoryScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/help/orders/OrderHistoryScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/ui/help/orders/OrderHistoryScreen;

    invoke-direct {v0}, Lcom/squareup/ui/help/orders/OrderHistoryScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/orders/OrderHistoryScreen;->INSTANCE:Lcom/squareup/ui/help/orders/OrderHistoryScreen;

    .line 48
    sget-object v0, Lcom/squareup/ui/help/orders/OrderHistoryScreen;->INSTANCE:Lcom/squareup/ui/help/orders/OrderHistoryScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/help/orders/OrderHistoryScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/help/InHelpAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 45
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->HELP_FLOW_HISTORY:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 41
    const-class v0, Lcom/squareup/ui/help/orders/OrdersSection;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 55
    const-class v0, Lcom/squareup/ui/help/orders/OrderHistoryScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/help/orders/OrderHistoryScreen$Component;

    invoke-interface {p1}, Lcom/squareup/ui/help/orders/OrderHistoryScreen$Component;->coordinator()Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 36
    const-class v0, Lcom/squareup/ui/help/orders/OrderHistoryScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/orders/OrderHistoryScreen$Component;

    .line 37
    invoke-interface {v0}, Lcom/squareup/ui/help/orders/OrderHistoryScreen$Component;->orderHistory()Lcom/squareup/ui/help/orders/OrderHistory;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 51
    sget v0, Lcom/squareup/applet/help/R$layout;->order_history_view:I

    return v0
.end method
