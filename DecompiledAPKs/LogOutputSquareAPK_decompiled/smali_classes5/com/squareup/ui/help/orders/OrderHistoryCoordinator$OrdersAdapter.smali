.class Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "OrderHistoryCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OrdersAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final ORDERS_BEFORE_SHOWING_MORE:I = 0xa

.field private static final TYPE_ENTRY:I = 0x0

.field private static final TYPE_MORE:I = 0x1


# instance fields
.field final orders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/solidshop/Order;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;)V
    .locals 1

    .line 143
    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;->this$0:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 147
    new-instance p1, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;->orders:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$1;)V
    .locals 0

    .line 143
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;-><init>(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;)V

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 2

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;->orders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;->orders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 2

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;->orders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_1

    .line 163
    check-cast p1, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;

    if-nez p2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;->orders:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/solidshop/Order;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;->bind(ZLcom/squareup/protos/client/solidshop/Order;)V

    :cond_1
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 2

    const/4 v0, 0x0

    if-nez p2, :cond_0

    .line 151
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget v1, Lcom/squareup/applet/help/R$layout;->order_history_entry:I

    .line 152
    invoke-virtual {p2, v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 153
    new-instance p2, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;

    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;->this$0:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;

    invoke-direct {p2, v0, p1}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;-><init>(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;Landroid/view/View;)V

    return-object p2

    .line 156
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget v1, Lcom/squareup/applet/help/R$layout;->order_history_more:I

    .line 157
    invoke-virtual {p2, v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 158
    new-instance p2, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$MoreViewHolder;

    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;->this$0:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;

    invoke-direct {p2, v0, p1}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$MoreViewHolder;-><init>(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;Landroid/view/View;)V

    return-object p2
.end method

.method setOrders(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/solidshop/Order;",
            ">;)V"
        }
    .end annotation

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;->orders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;->orders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 179
    invoke-virtual {p0}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;->notifyDataSetChanged()V

    return-void
.end method
