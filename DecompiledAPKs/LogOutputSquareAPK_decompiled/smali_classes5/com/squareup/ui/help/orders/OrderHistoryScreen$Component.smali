.class public interface abstract Lcom/squareup/ui/help/orders/OrderHistoryScreen$Component;
.super Ljava/lang/Object;
.source "OrderHistoryScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/orders/OrderHistoryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract coordinator()Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;
.end method

.method public abstract orderHistory()Lcom/squareup/ui/help/orders/OrderHistory;
.end method
