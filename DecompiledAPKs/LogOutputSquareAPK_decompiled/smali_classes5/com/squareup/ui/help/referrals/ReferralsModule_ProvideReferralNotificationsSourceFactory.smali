.class public final Lcom/squareup/ui/help/referrals/ReferralsModule_ProvideReferralNotificationsSourceFactory;
.super Ljava/lang/Object;
.source "ReferralsModule_ProvideReferralNotificationsSourceFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/Set<",
        "Lcom/squareup/notificationcenterdata/NotificationsSource;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final referralNotificationsSourceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/help/referrals/ReferralsModule_ProvideReferralNotificationsSourceFactory;->referralNotificationsSourceProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/help/referrals/ReferralsModule_ProvideReferralNotificationsSourceFactory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/referrals/ReferralsModule_ProvideReferralNotificationsSourceFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/help/referrals/ReferralsModule_ProvideReferralNotificationsSourceFactory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/ui/help/referrals/ReferralsModule_ProvideReferralNotificationsSourceFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/referrals/ReferralsModule_ProvideReferralNotificationsSourceFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideReferralNotificationsSource(Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;Lcom/squareup/settings/server/Features;)Ljava/util/Set;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource;",
            ">;"
        }
    .end annotation

    .line 44
    invoke-static {p0, p1}, Lcom/squareup/ui/help/referrals/ReferralsModule;->provideReferralNotificationsSource(Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;Lcom/squareup/settings/server/Features;)Ljava/util/Set;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/help/referrals/ReferralsModule_ProvideReferralNotificationsSourceFactory;->get()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource;",
            ">;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/help/referrals/ReferralsModule_ProvideReferralNotificationsSourceFactory;->referralNotificationsSourceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;

    iget-object v1, p0, Lcom/squareup/ui/help/referrals/ReferralsModule_ProvideReferralNotificationsSourceFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1}, Lcom/squareup/ui/help/referrals/ReferralsModule_ProvideReferralNotificationsSourceFactory;->provideReferralNotificationsSource(Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;Lcom/squareup/settings/server/Features;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
