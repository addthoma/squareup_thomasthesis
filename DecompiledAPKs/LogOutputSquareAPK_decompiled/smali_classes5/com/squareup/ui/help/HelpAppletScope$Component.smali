.class public interface abstract Lcom/squareup/ui/help/HelpAppletScope$Component;
.super Ljava/lang/Object;
.source "HelpAppletScope.java"

# interfaces
.implements Lcom/squareup/ui/help/HelpAppletScope$SupportComponents;
.implements Lcom/squareup/ui/help/HelpAppletMasterScreen$ParentComponent;
.implements Lcom/squareup/ui/help/jedi/JediHelpScope$ParentComponent;
.implements Lcom/squareup/referrals/ReferralScreen$ParentComponent;
.implements Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope$ParentComponent;
.implements Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner$ParentComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/help/HelpAppletScope$ReferralModule;,
        Lcom/squareup/ui/help/HelpAppletMasterScreen$Module;,
        Lcom/squareup/ui/help/HelpAppletScope$HelpAppletModule;,
        Lcom/squareup/ordermagstripe/OrderMagstripeModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/HelpAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract aboutCoordinator()Lcom/squareup/ui/help/about/AboutCoordinator;
.end method

.method public abstract announcementDetailCoordinator()Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;
.end method

.method public abstract announcementsCoordinator()Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;
.end method

.method public abstract helpAppletScopeRunner()Lcom/squareup/ui/help/HelpAppletScopeRunner;
.end method

.method public abstract helpCoordinator()Lcom/squareup/ui/help/help/HelpCoordinator;
.end method

.method public abstract legalCoordinator()Lcom/squareup/ui/help/legal/PosLegalCoordinator;
.end method

.method public abstract librariesCoordinator()Lcom/squareup/ui/help/about/LibrariesCoordinator;
.end method

.method public abstract ntepCoordinator()Lcom/squareup/ui/help/about/NtepCoordinator;
.end method

.method public abstract ordersCoordinator()Lcom/squareup/ui/help/orders/OrdersCoordinator;
.end method

.method public abstract troubleshootingCoordinator()Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;
.end method

.method public abstract troubleshootingScopeRunner()Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;
.end method

.method public abstract tutorialsCoordinator()Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;
.end method
