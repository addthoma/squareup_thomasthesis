.class public interface abstract Lcom/squareup/ui/help/MessagingController;
.super Ljava/lang/Object;
.source "MessagingController.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/MessagingController$NoMessagingController;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008f\u0018\u00002\u00020\u0001:\u0001\u0017J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H&J\u0008\u0010\t\u001a\u00020\nH&J\u0008\u0010\u000b\u001a\u00020\nH&J\u0008\u0010\u000c\u001a\u00020\u0006H&J\u0016\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u00032\u0006\u0010\u000f\u001a\u00020\nH&J\u0010\u0010\u0010\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u000eH&J\u0018\u0010\u0010\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0012H&J\u0008\u0010\u0013\u001a\u00020\nH&J\u0010\u0010\u0014\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\u0004H&J\u0008\u0010\u0016\u001a\u00020\nH&\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/help/MessagingController;",
        "Lmortar/Scoped;",
        "badgeCount",
        "Lio/reactivex/Observable;",
        "",
        "getNotificationCount",
        "",
        "successHandler",
        "Landroid/os/Handler;",
        "helpshiftConversationActive",
        "",
        "helpshiftSDKInstalled",
        "launchMessagingActivity",
        "messagingViewState",
        "Lcom/squareup/ui/help/MessagingViewState;",
        "requireInstall",
        "openMessagingActivity",
        "flow",
        "Lflow/Flow;",
        "showMessagingIcon",
        "updateUnreadMessagesCount",
        "count",
        "useHelpshift",
        "NoMessagingController",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract badgeCount()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getNotificationCount(Landroid/os/Handler;)V
.end method

.method public abstract helpshiftConversationActive()Z
.end method

.method public abstract helpshiftSDKInstalled()Z
.end method

.method public abstract launchMessagingActivity()V
.end method

.method public abstract messagingViewState(Z)Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/help/MessagingViewState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract openMessagingActivity(Lcom/squareup/ui/help/MessagingViewState;)V
.end method

.method public abstract openMessagingActivity(Lcom/squareup/ui/help/MessagingViewState;Lflow/Flow;)V
.end method

.method public abstract showMessagingIcon()Z
.end method

.method public abstract updateUnreadMessagesCount(I)V
.end method

.method public abstract useHelpshift()Z
.end method
