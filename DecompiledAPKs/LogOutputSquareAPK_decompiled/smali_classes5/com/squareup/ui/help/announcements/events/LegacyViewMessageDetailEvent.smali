.class public Lcom/squareup/ui/help/announcements/events/LegacyViewMessageDetailEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "LegacyViewMessageDetailEvent.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field final tracker_token:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/server/messages/Message;)V
    .locals 2

    .line 14
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->VIEW:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->MESSAGE_CENTER_MESSAGE:Lcom/squareup/analytics/RegisterViewName;

    iget-object v1, v1, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 15
    iget-object p1, p1, Lcom/squareup/server/messages/Message;->tracker_token:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/help/announcements/events/LegacyViewMessageDetailEvent;->tracker_token:Ljava/lang/String;

    return-void
.end method
