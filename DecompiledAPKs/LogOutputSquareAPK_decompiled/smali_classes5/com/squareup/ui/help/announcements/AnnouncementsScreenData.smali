.class public final Lcom/squareup/ui/help/announcements/AnnouncementsScreenData;
.super Ljava/lang/Object;
.source "AnnouncementsScreenData.java"


# instance fields
.field public final announcements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;)V"
        }
    .end annotation

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/squareup/ui/help/announcements/AnnouncementsScreenData;->announcements:Ljava/util/List;

    return-void
.end method
