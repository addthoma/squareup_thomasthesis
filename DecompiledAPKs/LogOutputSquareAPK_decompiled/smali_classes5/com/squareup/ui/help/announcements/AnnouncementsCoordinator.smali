.class public Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AnnouncementsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;
    }
.end annotation


# static fields
.field private static final EMPTY_CHILD:I = 0x1

.field private static final LIST_CHILD:I


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private animator:Landroid/widget/ViewAnimator;

.field private emptyView:Lcom/squareup/ui/EmptyView;

.field private final helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

.field private listView:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 39
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 73
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 74
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 75
    sget v0, Lcom/squareup/applet/help/R$id;->announcements_list_animator:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewAnimator;

    iput-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->animator:Landroid/widget/ViewAnimator;

    .line 76
    sget v0, Lcom/squareup/applet/help/R$id;->list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->listView:Landroid/widget/ListView;

    .line 77
    sget v0, Lcom/squareup/applet/help/R$id;->empty_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/EmptyView;

    iput-object p1, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    return-void
.end method

.method private getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    sget v1, Lcom/squareup/applet/help/R$string;->announcements:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->getActionBarConfig(Ljava/lang/String;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$null$1(Lcom/squareup/ui/help/announcements/AnnouncementsScreenData;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 61
    iget-object p0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsScreenData;->announcements:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->bindViews(Landroid/view/View;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/EmptyView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    sget v1, Lcom/squareup/applet/help/R$string;->no_announcements_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/EmptyView;->setTitle(I)V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    sget v1, Lcom/squareup/applet/help/R$string;->no_announcements_description:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/EmptyView;->setMessage(I)V

    .line 52
    new-instance v0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;

    iget-object v1, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;-><init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$1;)V

    .line 54
    iget-object v1, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->listView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 56
    iget-object v1, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->listView:Landroid/widget/ListView;

    new-instance v2, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 57
    iget-object v1, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->listView:Landroid/widget/ListView;

    new-instance v2, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementsCoordinator$5StFm3QelNR0I9XO3aMJw-fPFFg;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementsCoordinator$5StFm3QelNR0I9XO3aMJw-fPFFg;-><init>(Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 60
    new-instance v1, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementsCoordinator$18-KAv35Jp61gHSGP9_fjATOoL4;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementsCoordinator$18-KAv35Jp61gHSGP9_fjATOoL4;-><init>(Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$AnnouncementsCoordinator(Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 58
    iget-object p2, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {p1, p4}, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;->getItem(I)Lcom/squareup/server/messages/Message;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->announcementSelected(Lcom/squareup/server/messages/Message;)V

    return-void
.end method

.method public synthetic lambda$attach$3$AnnouncementsCoordinator(Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->announcementsScreenData()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementsCoordinator$_bijhJzaZuzTcqUXcxZNeAPg0pM;->INSTANCE:Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementsCoordinator$_bijhJzaZuzTcqUXcxZNeAPg0pM;

    .line 61
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementsCoordinator$6E1H6sEHp1muuUrQSkJuML8kymg;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementsCoordinator$6E1H6sEHp1muuUrQSkJuML8kymg;-><init>(Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;)V

    .line 62
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$2$AnnouncementsCoordinator(Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;->animator:Landroid/widget/ViewAnimator;

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    .line 64
    invoke-virtual {p1, p2}, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;->setAnnouncements(Ljava/util/List;)V

    return-void
.end method
