.class public final Lcom/squareup/ui/help/RealHelpBadge_Factory;
.super Ljava/lang/Object;
.source "RealHelpBadge_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/RealHelpBadge;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final firstInvoiceTutorialViewedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final helpAppletSectionsListProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletSectionsList;",
            ">;"
        }
    .end annotation
.end field

.field private final helpAppletSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/api/HelpAppletSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyAdjustPointsTutorialViewedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final loyaltyEnrollTutorialViewedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final loyaltyRedeemRewardsTutorialViewedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final tutorialsSectionsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/TutorialsSections;",
            ">;"
        }
    .end annotation
.end field

.field private final whatsNewBadgeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewBadge;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewBadge;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletSectionsList;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/api/HelpAppletSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/TutorialsSections;",
            ">;)V"
        }
    .end annotation

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->whatsNewBadgeProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p2, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p3, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p4, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->firstInvoiceTutorialViewedProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p5, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->loyaltyEnrollTutorialViewedProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p6, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->loyaltyAdjustPointsTutorialViewedProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p7, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->loyaltyRedeemRewardsTutorialViewedProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p8, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->helpAppletSectionsListProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p9, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->helpAppletSettingsProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p10, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->tutorialsSectionsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/RealHelpBadge_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewBadge;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletSectionsList;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/api/HelpAppletSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/TutorialsSections;",
            ">;)",
            "Lcom/squareup/ui/help/RealHelpBadge_Factory;"
        }
    .end annotation

    .line 79
    new-instance v11, Lcom/squareup/ui/help/RealHelpBadge_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/help/RealHelpBadge_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/tour/WhatsNewBadge;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/help/HelpAppletSectionsList;Lcom/squareup/ui/help/api/HelpAppletSettings;Lcom/squareup/ui/help/tutorials/TutorialsSections;)Lcom/squareup/ui/help/RealHelpBadge;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tour/WhatsNewBadge;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/ui/help/HelpAppletSectionsList;",
            "Lcom/squareup/ui/help/api/HelpAppletSettings;",
            "Lcom/squareup/ui/help/tutorials/TutorialsSections;",
            ")",
            "Lcom/squareup/ui/help/RealHelpBadge;"
        }
    .end annotation

    .line 90
    new-instance v11, Lcom/squareup/ui/help/RealHelpBadge;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/help/RealHelpBadge;-><init>(Lcom/squareup/tour/WhatsNewBadge;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/help/HelpAppletSectionsList;Lcom/squareup/ui/help/api/HelpAppletSettings;Lcom/squareup/ui/help/tutorials/TutorialsSections;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/RealHelpBadge;
    .locals 11

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->whatsNewBadgeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/tour/WhatsNewBadge;

    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->firstInvoiceTutorialViewedProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->loyaltyEnrollTutorialViewedProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->loyaltyAdjustPointsTutorialViewedProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->loyaltyRedeemRewardsTutorialViewedProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->helpAppletSectionsListProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/help/HelpAppletSectionsList;

    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->helpAppletSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/help/api/HelpAppletSettings;

    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge_Factory;->tutorialsSectionsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/ui/help/tutorials/TutorialsSections;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/help/RealHelpBadge_Factory;->newInstance(Lcom/squareup/tour/WhatsNewBadge;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/help/HelpAppletSectionsList;Lcom/squareup/ui/help/api/HelpAppletSettings;Lcom/squareup/ui/help/tutorials/TutorialsSections;)Lcom/squareup/ui/help/RealHelpBadge;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/help/RealHelpBadge_Factory;->get()Lcom/squareup/ui/help/RealHelpBadge;

    move-result-object v0

    return-object v0
.end method
