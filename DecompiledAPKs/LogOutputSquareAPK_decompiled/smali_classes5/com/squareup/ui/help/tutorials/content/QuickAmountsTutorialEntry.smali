.class public final Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;
.super Lcom/squareup/ui/help/HelpAppletContent;
.source "QuickAmountsTutorialEntry.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B5\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0008\u0010\u0013\u001a\u00020\u0014H\u0016J\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0016H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000e\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0011\u0010\u0012\u001a\u0004\u0008\u000f\u0010\u0010R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;",
        "Lcom/squareup/ui/help/HelpAppletContent;",
        "quickAmountsTutorialVisibility",
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;",
        "quickAmountsTutorialCreator",
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
        "orderEntryAppletGateway",
        "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
        "lazyContainer",
        "Ldagger/Lazy;",
        "Lcom/squareup/ui/main/PosContainer;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;Lcom/squareup/orderentry/OrderEntryAppletGateway;Ldagger/Lazy;Lcom/squareup/util/Res;)V",
        "posContainer",
        "getPosContainer",
        "()Lcom/squareup/ui/main/PosContainer;",
        "posContainer$delegate",
        "Ldagger/Lazy;",
        "handleClick",
        "",
        "shouldDisplayObservable",
        "Lio/reactivex/Observable;",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private final posContainer$delegate:Ldagger/Lazy;

.field private final quickAmountsTutorialCreator:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;

.field private final quickAmountsTutorialVisibility:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "posContainer"

    const-string v4, "getPosContainer()Lcom/squareup/ui/main/PosContainer;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;Lcom/squareup/orderentry/OrderEntryAppletGateway;Ldagger/Lazy;Lcom/squareup/util/Res;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "quickAmountsTutorialVisibility"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "quickAmountsTutorialCreator"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEntryAppletGateway"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyContainer"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    sget v2, Lcom/squareup/applet/help/R$string;->quickamounts_walkthrough:I

    .line 27
    sget v0, Lcom/squareup/applet/help/R$string;->quickamounts_walkthrough_subtext:I

    invoke-interface {p5, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p5

    move-object v3, p5

    check-cast v3, Ljava/lang/CharSequence;

    .line 28
    sget-object v5, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_QUICK_AMOUNTS_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x14

    const/4 v8, 0x0

    move-object v1, p0

    .line 25
    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/help/HelpAppletContent;-><init>(ILjava/lang/CharSequence;Ljava/lang/Integer;Lcom/squareup/analytics/RegisterTapName;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;->quickAmountsTutorialVisibility:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;

    iput-object p2, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;->quickAmountsTutorialCreator:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;

    iput-object p3, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    .line 30
    iput-object p4, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;->posContainer$delegate:Ldagger/Lazy;

    return-void
.end method

.method private final getPosContainer()Lcom/squareup/ui/main/PosContainer;
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;->posContainer$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    return-object v0
.end method


# virtual methods
.method public handleClick()Z
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;->quickAmountsTutorialCreator:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;

    sget-object v1, Lcom/squareup/quickamounts/ui/StartMode$ManualEligibilityStart;->INSTANCE:Lcom/squareup/quickamounts/ui/StartMode$ManualEligibilityStart;

    check-cast v1, Lcom/squareup/quickamounts/ui/StartMode;

    invoke-virtual {v0, v1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;->activate(Lcom/squareup/quickamounts/ui/StartMode;)V

    .line 38
    invoke-direct {p0}, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;->getPosContainer()Lcom/squareup/ui/main/PosContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    sget-object v2, Lcom/squareup/orderentry/OrderEntryMode;->KEYPAD:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-interface {v1, v2}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->historyFactoryForMode(Lcom/squareup/orderentry/OrderEntryMode;)Lcom/squareup/ui/main/HistoryFactory;

    move-result-object v1

    sget-object v2, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-interface {v0, v1, v2}, Lcom/squareup/ui/main/PosContainer;->resetHistory(Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V

    const/4 v0, 0x1

    return v0
.end method

.method public shouldDisplayObservable()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;->quickAmountsTutorialVisibility:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;

    invoke-interface {v0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;->canShow()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
