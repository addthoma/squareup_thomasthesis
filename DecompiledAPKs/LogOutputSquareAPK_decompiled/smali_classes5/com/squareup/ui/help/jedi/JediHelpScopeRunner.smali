.class public final Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;
.super Ljava/lang/Object;
.source "JediHelpScopeRunner.kt"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/jedi/JediComponentInputHandler;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/help/jedi/JediHelpScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nJediHelpScopeRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 JediHelpScopeRunner.kt\ncom/squareup/ui/help/jedi/JediHelpScopeRunner\n+ 2 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,408:1\n67#2:409\n92#2,3:410\n*E\n*S KotlinDebug\n*F\n+ 1 JediHelpScopeRunner.kt\ncom/squareup/ui/help/jedi/JediHelpScopeRunner\n*L\n262#1:409\n262#1,3:410\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00f0\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002B_\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u00a2\u0006\u0002\u0010\u0019J\u0008\u0010&\u001a\u00020%H\u0002J\u000e\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020%0(H\u0016J\u0018\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.H\u0002J\u000c\u0010/\u001a\u0008\u0012\u0004\u0012\u0002000(J6\u00101\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u000203022\u0008\u00104\u001a\u0004\u0018\u00010\u001c2\u000c\u00105\u001a\u0008\u0012\u0004\u0012\u0002060\u001e2\u0008\u00107\u001a\u0004\u0018\u00010\u001cH\u0002J\u000e\u00108\u001a\u0008\u0012\u0004\u0012\u0002060\u001eH\u0002J\u0010\u00109\u001a\u00020\"2\u0006\u0010:\u001a\u00020\u001cH\u0002J\u0010\u0010;\u001a\u00020\"2\u0006\u0010<\u001a\u00020=H\u0002J\u0014\u0010>\u001a\u0008\u0012\u0004\u0012\u0002000(2\u0006\u0010?\u001a\u00020@J\u000e\u0010A\u001a\u0008\u0012\u0004\u0012\u00020\u001c0(H\u0002J \u0010B\u001a\u00020\"2\u0006\u0010C\u001a\u00020\u001c2\u0006\u0010D\u001a\u00020\u001c2\u0006\u00104\u001a\u00020\u001cH\u0016J\u0010\u0010E\u001a\u00020\"2\u0006\u0010F\u001a\u00020\u001cH\u0016J\u0010\u0010G\u001a\u00020\"2\u0006\u0010H\u001a\u00020IH\u0016J\u0008\u0010J\u001a\u00020\"H\u0016J\u0018\u0010K\u001a\u00020\"2\u0006\u0010L\u001a\u00020M2\u0006\u0010:\u001a\u00020\u001cH\u0016J\u0008\u0010N\u001a\u00020\"H\u0016J \u0010O\u001a\u00020\"2\u0006\u0010C\u001a\u00020\u001c2\u0006\u0010P\u001a\u00020\u001c2\u0006\u0010Q\u001a\u00020,H\u0016J(\u0010R\u001a\u00020\"2\u0006\u0010C\u001a\u00020\u001c2\u0006\u0010S\u001a\u00020\u001c2\u0006\u0010D\u001a\u00020\u001c2\u0006\u00104\u001a\u00020\u001cH\u0016J\u0018\u0010T\u001a\u00020\"2\u0006\u0010C\u001a\u00020\u001c2\u0006\u0010P\u001a\u00020\u001cH\u0016J\u0010\u0010U\u001a\u00020V2\u0006\u0010W\u001a\u00020XH\u0002J\u001c\u0010Y\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u000200022\u0006\u0010Z\u001a\u00020=H\u0002J\u001a\u0010[\u001a\u00020\\2\u0006\u0010]\u001a\u00020^2\u0008\u00104\u001a\u0004\u0018\u00010\u001cH\u0002J \u0010_\u001a\u0002002\u0006\u0010`\u001a\u00020\\2\u0006\u0010a\u001a\u00020\u001c2\u0006\u0010b\u001a\u00020cH\u0002J\u0010\u0010_\u001a\u00020\\2\u0006\u0010b\u001a\u000203H\u0002J\u001a\u0010d\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u000200022\u0006\u0010?\u001a\u00020@R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001c0\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010 \u001a\u0010\u0012\u000c\u0012\n #*\u0004\u0018\u00010\"0\"0!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010$\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020%0\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006e"
    }
    d2 = {
        "Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;",
        "Lmortar/Scoped;",
        "Lcom/squareup/jedi/JediComponentInputHandler;",
        "flow",
        "Lflow/Flow;",
        "jediService",
        "Lcom/squareup/server/help/JediService;",
        "deepLinks",
        "Lcom/squareup/ui/main/DeepLinks;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "res",
        "Lcom/squareup/util/Res;",
        "jediHelpSettings",
        "Lcom/squareup/jedi/JediHelpSettings;",
        "resources",
        "Landroid/content/res/Resources;",
        "locale",
        "Ljava/util/Locale;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "telephonyManager",
        "Landroid/telephony/TelephonyManager;",
        "messagingController",
        "Lcom/squareup/ui/help/MessagingController;",
        "(Lflow/Flow;Lcom/squareup/server/help/JediService;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;Lcom/squareup/jedi/JediHelpSettings;Landroid/content/res/Resources;Ljava/util/Locale;Lcom/squareup/settings/server/AccountStatusSettings;Landroid/telephony/TelephonyManager;Lcom/squareup/ui/help/MessagingController;)V",
        "currentTextInputs",
        "Ljava/util/LinkedHashMap;",
        "",
        "noResultsComponents",
        "",
        "Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;",
        "requiredTextInputsUpdated",
        "Lio/reactivex/subjects/BehaviorSubject;",
        "",
        "kotlin.jvm.PlatformType",
        "requiredTextStatus",
        "",
        "canCall",
        "canPressButton",
        "Lio/reactivex/Observable;",
        "createComponentItem",
        "Lcom/squareup/jedi/ui/JediComponentItem;",
        "index",
        "",
        "component",
        "Lcom/squareup/protos/jedi/service/Component;",
        "getJediPhoneScreenData",
        "Lcom/squareup/jedi/JediHelpScreenData;",
        "getPanelForSession",
        "Lio/reactivex/ObservableTransformer;",
        "Lcom/squareup/protos/jedi/service/PanelResponse;",
        "transitionId",
        "inputs",
        "Lcom/squareup/protos/jedi/service/Input;",
        "panelToken",
        "getTextInputsAndClear",
        "goToLink",
        "link",
        "goToScreenWithInput",
        "input",
        "Lcom/squareup/ui/help/jedi/JediHelpInput;",
        "jediHelpScreenData",
        "screen",
        "Lcom/squareup/ui/help/jedi/JediHelpScreen;",
        "newSession",
        "onButtonPressInput",
        "componentName",
        "sessionToken",
        "onCall",
        "phoneLink",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onLink",
        "context",
        "Landroid/content/Context;",
        "onMessageUs",
        "onRequiredTextInput",
        "text",
        "minLength",
        "onSearchResultButtonPressInput",
        "linkPanelToken",
        "onTextInput",
        "resumeFromErrors",
        "Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;",
        "throwable",
        "",
        "screenDataForSession",
        "screenInput",
        "screenDataFromPanel",
        "Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;",
        "panel",
        "Lcom/squareup/protos/jedi/service/Panel;",
        "screenDataFromSuccessfulResponse",
        "screenData",
        "searchText",
        "response",
        "Lcom/squareup/protos/jedi/service/SearchResponse;",
        "searchResponseForString",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final currentTextInputs:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final deepLinks:Lcom/squareup/ui/main/DeepLinks;

.field private final flow:Lflow/Flow;

.field private final jediHelpSettings:Lcom/squareup/jedi/JediHelpSettings;

.field private final jediService:Lcom/squareup/server/help/JediService;

.field private final locale:Ljava/util/Locale;

.field private final messagingController:Lcom/squareup/ui/help/MessagingController;

.field private final noResultsComponents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;",
            ">;"
        }
    .end annotation
.end field

.field private final requiredTextInputsUpdated:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final requiredTextStatus:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final resources:Landroid/content/res/Resources;

.field private final telephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/server/help/JediService;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;Lcom/squareup/jedi/JediHelpSettings;Landroid/content/res/Resources;Ljava/util/Locale;Lcom/squareup/settings/server/AccountStatusSettings;Landroid/telephony/TelephonyManager;Lcom/squareup/ui/help/MessagingController;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jediService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deepLinks"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "browserLauncher"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jediHelpSettings"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "telephonyManager"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messagingController"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->jediService:Lcom/squareup/server/help/JediService;

    iput-object p3, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->deepLinks:Lcom/squareup/ui/main/DeepLinks;

    iput-object p4, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iput-object p5, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->res:Lcom/squareup/util/Res;

    iput-object p6, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->jediHelpSettings:Lcom/squareup/jedi/JediHelpSettings;

    iput-object p7, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->resources:Landroid/content/res/Resources;

    iput-object p8, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->locale:Ljava/util/Locale;

    iput-object p9, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p10, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->telephonyManager:Landroid/telephony/TelephonyManager;

    iput-object p11, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->messagingController:Lcom/squareup/ui/help/MessagingController;

    .line 91
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-static {p1}, Lio/reactivex/subjects/BehaviorSubject;->createDefault(Ljava/lang/Object;)Lio/reactivex/subjects/BehaviorSubject;

    move-result-object p1

    const-string p2, "BehaviorSubject.createDefault(Unit)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->requiredTextInputsUpdated:Lio/reactivex/subjects/BehaviorSubject;

    .line 94
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->currentTextInputs:Ljava/util/LinkedHashMap;

    .line 97
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->requiredTextStatus:Ljava/util/LinkedHashMap;

    .line 100
    new-instance p1, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;

    .line 101
    new-instance p2, Lcom/squareup/protos/jedi/service/Component;

    .line 102
    sget-object p3, Lcom/squareup/protos/jedi/service/ComponentKind;->PARAGRAPH:Lcom/squareup/protos/jedi/service/ComponentKind;

    const/4 p4, 0x3

    new-array p4, p4, [Lkotlin/Pair;

    .line 105
    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p6, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->res:Lcom/squareup/util/Res;

    sget p7, Lcom/squareup/applet/help/R$string;->no_results:I

    invoke-interface {p6, p7}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p6

    invoke-virtual {p5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p6, "<br>"

    invoke-virtual {p5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p6, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->res:Lcom/squareup/util/Res;

    .line 106
    sget p7, Lcom/squareup/applet/help/R$string;->no_results_subtext:I

    .line 105
    invoke-interface {p6, p7}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p6

    invoke-virtual {p5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    const-string/jumbo p6, "text"

    invoke-static {p6, p5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p5

    const/4 p6, 0x0

    aput-object p5, p4, p6

    const-string p5, "emphasis"

    const-string p6, "high"

    .line 107
    invoke-static {p5, p6}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p5

    const/4 p6, 0x1

    aput-object p5, p4, p6

    const-string p5, "is_html"

    const-string/jumbo p6, "true"

    .line 108
    invoke-static {p5, p6}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p5

    const/4 p6, 0x2

    aput-object p5, p4, p6

    .line 104
    invoke-static {p4}, Lkotlin/collections/MapsKt;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p4

    .line 110
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p5

    const-string p6, ""

    .line 101
    invoke-direct {p2, p3, p6, p4, p5}, Lcom/squareup/protos/jedi/service/Component;-><init>(Lcom/squareup/protos/jedi/service/ComponentKind;Ljava/lang/String;Ljava/util/Map;Ljava/util/List;)V

    .line 100
    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    .line 99
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->noResultsComponents:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$getJediService$p(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;)Lcom/squareup/server/help/JediService;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->jediService:Lcom/squareup/server/help/JediService;

    return-object p0
.end method

.method public static final synthetic access$getPanelForSession(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lio/reactivex/ObservableTransformer;
    .locals 0

    .line 76
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->getPanelForSession(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lio/reactivex/ObservableTransformer;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getRequiredTextStatus$p(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;)Ljava/util/LinkedHashMap;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->requiredTextStatus:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method public static final synthetic access$resumeFromErrors(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;Ljava/lang/Throwable;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;
    .locals 0

    .line 76
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->resumeFromErrors(Ljava/lang/Throwable;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$screenDataFromSuccessfulResponse(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;Lcom/squareup/protos/jedi/service/PanelResponse;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;
    .locals 0

    .line 76
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->screenDataFromSuccessfulResponse(Lcom/squareup/protos/jedi/service/PanelResponse;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$screenDataFromSuccessfulResponse(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;Lcom/squareup/protos/jedi/service/SearchResponse;)Lcom/squareup/jedi/JediHelpScreenData;
    .locals 0

    .line 76
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->screenDataFromSuccessfulResponse(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;Lcom/squareup/protos/jedi/service/SearchResponse;)Lcom/squareup/jedi/JediHelpScreenData;

    move-result-object p0

    return-object p0
.end method

.method private final canCall()Z
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final createComponentItem(ILcom/squareup/protos/jedi/service/Component;)Lcom/squareup/jedi/ui/JediComponentItem;
    .locals 2

    .line 372
    iget-object v0, p2, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    if-eqz v0, :cond_1

    sget-object v1, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 382
    :pswitch_0
    new-instance p1, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    goto :goto_1

    .line 381
    :pswitch_1
    new-instance v0, Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItem;

    const/4 v1, 0x1

    if-le p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-direct {v0, p2, p1}, Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;Ljava/lang/Boolean;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    goto :goto_1

    .line 379
    :pswitch_2
    new-instance p1, Lcom/squareup/jedi/ui/components/JediRowComponentItem;

    invoke-direct {p0}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->canCall()Z

    move-result v0

    invoke-direct {p1, p2, v0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;Z)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    goto :goto_1

    .line 378
    :pswitch_3
    new-instance p1, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    goto :goto_1

    .line 377
    :pswitch_4
    new-instance p1, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->resources:Landroid/content/res/Resources;

    invoke-direct {p1, p2, v0}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;Landroid/content/res/Resources;)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    goto :goto_1

    .line 376
    :pswitch_5
    new-instance p1, Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItem;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    goto :goto_1

    .line 375
    :pswitch_6
    new-instance p1, Lcom/squareup/jedi/ui/components/JediIconComponentItem;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediIconComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    goto :goto_1

    .line 374
    :pswitch_7
    new-instance p1, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    goto :goto_1

    .line 373
    :pswitch_8
    new-instance p1, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    :goto_1
    return-object p1

    .line 384
    :cond_1
    :goto_2
    new-instance p1, Lcom/squareup/ui/help/jedi/JediResponseException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Component with kind: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p2, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, " is not supported"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/ui/help/jedi/JediResponseException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final getPanelForSession(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/jedi/service/Input;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/ObservableTransformer<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/jedi/service/PanelResponse;",
            ">;"
        }
    .end annotation

    .line 289
    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;-><init>(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method private final getTextInputsAndClear()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/jedi/service/Input;",
            ">;"
        }
    .end annotation

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->currentTextInputs:Ljava/util/LinkedHashMap;

    check-cast v0, Ljava/util/Map;

    .line 409
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 410
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 263
    new-instance v3, Lcom/squareup/protos/jedi/service/Input$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/jedi/service/Input$Builder;-><init>()V

    .line 264
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/squareup/protos/jedi/service/Input$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/Input$Builder;

    move-result-object v3

    .line 265
    sget-object v4, Lcom/squareup/protos/jedi/service/InputKind;->TEXT:Lcom/squareup/protos/jedi/service/InputKind;

    invoke-virtual {v3, v4}, Lcom/squareup/protos/jedi/service/Input$Builder;->kind(Lcom/squareup/protos/jedi/service/InputKind;)Lcom/squareup/protos/jedi/service/Input$Builder;

    move-result-object v3

    .line 266
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/squareup/protos/jedi/service/Input$Builder;->value(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/Input$Builder;

    move-result-object v2

    .line 267
    invoke-virtual {v2}, Lcom/squareup/protos/jedi/service/Input$Builder;->build()Lcom/squareup/protos/jedi/service/Input;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 412
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->currentTextInputs:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->requiredTextStatus:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 273
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    const-string/jumbo v1, "unmodifiableList(textInputs)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final goToLink(Ljava/lang/String;)V
    .locals 1

    .line 255
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->deepLinks:Lcom/squareup/ui/main/DeepLinks;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/DeepLinks;->handleInternalDeepLink(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private final goToScreenWithInput(Lcom/squareup/ui/help/jedi/JediHelpInput;)V
    .locals 9

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/help/jedi/JediHelpScreen;

    new-instance v8, Lcom/squareup/ui/help/jedi/JediHelpScreen;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object v2, v8

    move-object v3, p1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/help/jedi/JediHelpScreen;-><init>(Lcom/squareup/ui/help/jedi/JediHelpInput;ZLcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v8, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0, v1, v8}, Lcom/squareup/container/Flows;->goFromTo(Lflow/Flow;Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method private final newSession()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->jediHelpSettings:Lcom/squareup/jedi/JediHelpSettings;

    invoke-interface {v0}, Lcom/squareup/jedi/JediHelpSettings;->getJediClientKey()Ljava/lang/String;

    move-result-object v0

    .line 205
    iget-object v1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    const-string v2, "accountStatusSettings.userSettings"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v1

    .line 206
    new-instance v2, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;-><init>()V

    .line 207
    invoke-virtual {v2, v0}, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->client(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;

    move-result-object v0

    .line 208
    invoke-virtual {v1}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/protos/common/countries/Country;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/countries/Country;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->country_code(Lcom/squareup/protos/common/countries/Country;)Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;

    move-result-object v0

    .line 209
    iget-object v1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "locale.language"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "(this as java.lang.String).toUpperCase()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/protos/common/languages/Language;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/languages/Language;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->language_code(Lcom/squareup/protos/common/languages/Language;)Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;

    move-result-object v0

    .line 210
    invoke-virtual {v0}, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->build()Lcom/squareup/protos/jedi/service/StartSessionRequest;

    move-result-object v0

    .line 211
    iget-object v1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->jediService:Lcom/squareup/server/help/JediService;

    const-string v2, "startSessionRequest"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lcom/squareup/server/help/JediService;->startSession(Lcom/squareup/protos/jedi/service/StartSessionRequest;)Lcom/squareup/server/help/JediService$JediServiceStandardResponse;

    move-result-object v0

    .line 212
    invoke-virtual {v0}, Lcom/squareup/server/help/JediService$JediServiceStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 213
    sget-object v1, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$newSession$1;->INSTANCE:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$newSession$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 219
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "jediService.startSession\u2026}\n        .toObservable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 209
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final resumeFromErrors(Ljava/lang/Throwable;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;
    .locals 2

    .line 390
    const-class v0, Lcom/squareup/ui/help/jedi/JediResponseException;

    invoke-static {v0, p1}, Lcom/squareup/util/Throwables;->matches(Ljava/lang/Class;Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Can\'t handle support content"

    .line 391
    invoke-static {p1, v1, v0}, Ltimber/log/Timber;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 392
    new-instance p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/applet/help/R$string;->jedi_content_unavailable_text:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;-><init>(Ljava/lang/String;)V

    return-object p1

    .line 395
    :cond_0
    const-class v0, Lretrofit/RetrofitError;

    invoke-static {v0, p1}, Lcom/squareup/util/Throwables;->matches(Ljava/lang/Class;Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 397
    const-class v0, Lretrofit/RetrofitError;

    invoke-static {v0, p1}, Lcom/squareup/util/Throwables;->getMatchingThrowable(Ljava/lang/Class;Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    check-cast p1, Lretrofit/RetrofitError;

    .line 398
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object p1

    sget-object v0, Lretrofit/RetrofitError$Kind;->NETWORK:Lretrofit/RetrofitError$Kind;

    if-ne p1, v0, :cond_1

    .line 399
    iget-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/applet/help/R$string;->jedi_network_unavailable_text:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 401
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/applet/help/R$string;->jedi_content_unavailable_text:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 405
    :cond_2
    invoke-static {p1}, Lcom/squareup/util/Throwables;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final screenDataForSession(Lcom/squareup/ui/help/jedi/JediHelpInput;)Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/help/jedi/JediHelpInput;",
            ")",
            "Lio/reactivex/ObservableTransformer<",
            "Ljava/lang/String;",
            "Lcom/squareup/jedi/JediHelpScreenData;",
            ">;"
        }
    .end annotation

    .line 278
    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$screenDataForSession$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$screenDataForSession$1;-><init>(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;Lcom/squareup/ui/help/jedi/JediHelpInput;)V

    check-cast v0, Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method private final screenDataFromPanel(Lcom/squareup/protos/jedi/service/Panel;Ljava/lang/String;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;
    .locals 13

    .line 333
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 334
    iget-object v1, p1, Lcom/squareup/protos/jedi/service/Panel;->display_search_bar:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v3, 0x0

    .line 335
    move-object v4, v3

    check-cast v4, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    .line 336
    check-cast v3, Lcom/squareup/jedi/ui/components/JediBannerComponentItem;

    .line 338
    iget-object v5, p1, Lcom/squareup/protos/jedi/service/Panel;->components:Ljava/util/List;

    if-eqz v5, :cond_5

    const/4 v5, 0x0

    .line 339
    iget-object v6, p1, Lcom/squareup/protos/jedi/service/Panel;->components:Ljava/util/List;

    const-string v7, "panel.components"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Ljava/lang/Iterable;

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/protos/jedi/service/Component;

    .line 340
    iget-object v8, v7, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    if-eqz v8, :cond_4

    sget-object v9, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v8}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result v8

    aget v8, v9, v8

    if-eq v8, v2, :cond_3

    const/4 v9, 0x2

    if-eq v8, v9, :cond_2

    const/4 v9, 0x3

    if-eq v8, v9, :cond_0

    const-string v8, "component"

    .line 355
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v5, v7}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->createComponentItem(ILcom/squareup/protos/jedi/service/Component;)Lcom/squareup/jedi/ui/JediComponentItem;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 347
    :cond_0
    new-instance v8, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    invoke-direct {v8, v7}, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    .line 348
    invoke-virtual {v8}, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;->type()Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem$HeadlineType;

    move-result-object v7

    sget-object v9, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem$HeadlineType;->HEADING:Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem$HeadlineType;

    if-ne v7, v9, :cond_1

    move-object v4, v8

    goto :goto_1

    .line 351
    :cond_1
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    goto :goto_1

    .line 341
    :cond_3
    new-instance v3, Lcom/squareup/jedi/ui/components/JediBannerComponentItem;

    invoke-direct {v3, v7}, Lcom/squareup/jedi/ui/components/JediBannerComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 354
    :cond_4
    new-instance p1, Lcom/squareup/ui/help/jedi/JediResponseException;

    const-string p2, "Could not parse component kind properly."

    invoke-direct {p1, p2}, Lcom/squareup/ui/help/jedi/JediResponseException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_5
    move v5, v1

    move-object v6, v3

    .line 360
    new-instance v12, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    .line 362
    iget-object v2, p1, Lcom/squareup/protos/jedi/service/Panel;->session_token:Ljava/lang/String;

    const-string p1, "panel.session_token"

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 363
    iget-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->jediHelpSettings:Lcom/squareup/jedi/JediHelpSettings;

    invoke-interface {p1}, Lcom/squareup/jedi/JediHelpSettings;->getShowInlineLinks()Z

    move-result v7

    .line 364
    move-object v3, v0

    check-cast v3, Ljava/util/List;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x180

    const/4 v11, 0x0

    move-object v0, v12

    move-object v1, p2

    .line 360
    invoke-direct/range {v0 .. v11}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;ZLcom/squareup/jedi/ui/components/JediBannerComponentItem;ZLjava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v12
.end method

.method private final screenDataFromSuccessfulResponse(Lcom/squareup/protos/jedi/service/PanelResponse;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;
    .locals 2

    .line 314
    iget-object v0, p1, Lcom/squareup/protos/jedi/service/PanelResponse;->panel:Lcom/squareup/protos/jedi/service/Panel;

    const-string v1, "response.panel"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lcom/squareup/protos/jedi/service/PanelResponse;->transition_id:Ljava/lang/String;

    .line 313
    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->screenDataFromPanel(Lcom/squareup/protos/jedi/service/Panel;Ljava/lang/String;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-result-object p1

    return-object p1
.end method

.method private final screenDataFromSuccessfulResponse(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;Lcom/squareup/protos/jedi/service/SearchResponse;)Lcom/squareup/jedi/JediHelpScreenData;
    .locals 12

    .line 321
    iget-object p3, p3, Lcom/squareup/protos/jedi/service/SearchResponse;->panel:Lcom/squareup/protos/jedi/service/Panel;

    const-string v0, "response.panel"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p3, v0}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->screenDataFromPanel(Lcom/squareup/protos/jedi/service/Panel;Ljava/lang/String;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-result-object p3

    .line 324
    iget-object v0, p3, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->components:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 325
    iget-object p3, p3, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->components:Ljava/util/List;

    goto :goto_0

    .line 327
    :cond_0
    iget-object p3, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->noResultsComponents:Ljava/util/List;

    :goto_0
    move-object v9, p3

    const/16 v10, 0x7f

    const/4 v11, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    move-object v8, p2

    .line 322
    invoke-static/range {v0 .. v11}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->copy$default(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;ZLcom/squareup/jedi/ui/components/JediBannerComponentItem;ZLjava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-result-object p1

    check-cast p1, Lcom/squareup/jedi/JediHelpScreenData;

    return-object p1
.end method


# virtual methods
.method public canPressButton()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->requiredTextInputsUpdated:Lio/reactivex/subjects/BehaviorSubject;

    new-instance v1, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$canPressButton$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$canPressButton$1;-><init>(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "requiredTextInputsUpdate\u2026s.values.all { it }\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getJediPhoneScreenData()Lio/reactivex/Observable;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/jedi/JediHelpScreenData;",
            ">;"
        }
    .end annotation

    .line 183
    invoke-direct {p0}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->newSession()Lio/reactivex/Observable;

    move-result-object v0

    .line 184
    new-instance v8, Lcom/squareup/ui/help/jedi/JediHelpInput;

    .line 185
    iget-object v1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->jediHelpSettings:Lcom/squareup/jedi/JediHelpSettings;

    invoke-interface {v1}, Lcom/squareup/jedi/JediHelpSettings;->getCallUsPanelToken()Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xb

    const/4 v7, 0x0

    move-object v1, v8

    .line 184
    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/help/jedi/JediHelpInput;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p0, v8}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->screenDataForSession(Lcom/squareup/ui/help/jedi/JediHelpInput;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 187
    sget-object v1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpLoadingScreenData;->INSTANCE:Lcom/squareup/jedi/JediHelpScreenData$JediHelpLoadingScreenData;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "newSession()\n        .co\u2026ediHelpLoadingScreenData)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final jediHelpScreenData(Lcom/squareup/ui/help/jedi/JediHelpScreen;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/help/jedi/JediHelpScreen;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/jedi/JediHelpScreenData;",
            ">;"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    invoke-virtual {p1}, Lcom/squareup/ui/help/jedi/JediHelpScreen;->getScreenData()Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "Observable.just(it)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 193
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/help/jedi/JediHelpScreen;->getInput()Lcom/squareup/ui/help/jedi/JediHelpInput;

    move-result-object p1

    .line 194
    invoke-virtual {p1}, Lcom/squareup/ui/help/jedi/JediHelpInput;->getSessionToken()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 195
    invoke-direct {p0}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->newSession()Lio/reactivex/Observable;

    move-result-object v0

    goto :goto_0

    .line 197
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/help/jedi/JediHelpInput;->getSessionToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.just(screenInput.sessionToken)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->screenDataForSession(Lcom/squareup/ui/help/jedi/JediHelpInput;)Lio/reactivex/ObservableTransformer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object p1

    .line 200
    sget-object v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpLoadingScreenData;->INSTANCE:Lcom/squareup/jedi/JediHelpScreenData$JediHelpLoadingScreenData;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "if (screenInput.sessionT\u2026ediHelpLoadingScreenData)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public onButtonPressInput(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transitionId"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    new-instance v0, Lcom/squareup/protos/jedi/service/Input$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/Input$Builder;-><init>()V

    .line 139
    invoke-virtual {v0, p1}, Lcom/squareup/protos/jedi/service/Input$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/Input$Builder;

    move-result-object p1

    .line 140
    sget-object v0, Lcom/squareup/protos/jedi/service/InputKind;->BUTTON_PRESS:Lcom/squareup/protos/jedi/service/InputKind;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/jedi/service/Input$Builder;->kind(Lcom/squareup/protos/jedi/service/InputKind;)Lcom/squareup/protos/jedi/service/Input$Builder;

    move-result-object p1

    .line 141
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/jedi/service/Input$Builder;->value(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/Input$Builder;

    move-result-object p1

    .line 142
    invoke-virtual {p1}, Lcom/squareup/protos/jedi/service/Input$Builder;->build()Lcom/squareup/protos/jedi/service/Input;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/protos/jedi/service/Input;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 144
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->arrayListOf([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object p1

    .line 145
    invoke-direct {p0}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->getTextInputsAndClear()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 146
    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpInput;

    .line 149
    move-object v5, p1

    check-cast v5, Ljava/util/List;

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p2

    move-object v3, p3

    .line 146
    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/help/jedi/JediHelpInput;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->goToScreenWithInput(Lcom/squareup/ui/help/jedi/JediHelpInput;)V

    return-void
.end method

.method public onCall(Ljava/lang/String;)V
    .locals 1

    const-string v0, "phoneLink"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->goToLink(Ljava/lang/String;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLink(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "link"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    invoke-direct {p0, p2}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->goToLink(Ljava/lang/String;)V

    return-void
.end method

.method public onMessageUs()V
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->messagingController:Lcom/squareup/ui/help/MessagingController;

    invoke-interface {v0}, Lcom/squareup/ui/help/MessagingController;->launchMessagingActivity()V

    return-void
.end method

.method public onRequiredTextInput(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "text"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->currentTextInputs:Ljava/util/LinkedHashMap;

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->requiredTextStatus:Ljava/util/LinkedHashMap;

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    if-lt p2, p3, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    iget-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->requiredTextInputsUpdated:Lio/reactivex/subjects/BehaviorSubject;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public onSearchResultButtonPressInput(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "linkPanelToken"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "sessionToken"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "transitionId"

    invoke-static {p4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    new-instance p1, Lcom/squareup/ui/help/jedi/JediHelpInput;

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p1

    move-object v1, p3

    move-object v2, p4

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/help/jedi/JediHelpInput;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->goToScreenWithInput(Lcom/squareup/ui/help/jedi/JediHelpInput;)V

    return-void
.end method

.method public onTextInput(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "text"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->currentTextInputs:Ljava/util/LinkedHashMap;

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final searchResponseForString(Lcom/squareup/ui/help/jedi/JediHelpScreen;)Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/help/jedi/JediHelpScreen;",
            ")",
            "Lio/reactivex/ObservableTransformer<",
            "Ljava/lang/String;",
            "Lcom/squareup/jedi/JediHelpScreenData;",
            ">;"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 224
    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;-><init>(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;Lcom/squareup/ui/help/jedi/JediHelpScreen;)V

    check-cast v0, Lio/reactivex/ObservableTransformer;

    return-object v0
.end method
