.class final Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1$1;
.super Ljava/lang/Object;
.source "JediHelpScopeRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;->apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/protos/jedi/service/PanelRequest;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/PanelRequest;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    new-instance v0, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;-><init>()V

    .line 293
    iget-object v1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;

    iget-object v1, v1, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;->$transitionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->transition_id(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/PanelRequest$Builder;

    move-result-object v0

    .line 294
    iget-object v1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;

    iget-object v1, v1, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;->$inputs:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->inputs(Ljava/util/List;)Lcom/squareup/protos/jedi/service/PanelRequest$Builder;

    move-result-object v0

    .line 295
    invoke-virtual {v0, p1}, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->session_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/PanelRequest$Builder;

    move-result-object p1

    .line 296
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;

    iget-object v0, v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;->$panelToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->panel_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/PanelRequest$Builder;

    move-result-object p1

    .line 297
    invoke-virtual {p1}, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->build()Lcom/squareup/protos/jedi/service/PanelRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1$1;->apply(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/PanelRequest;

    move-result-object p1

    return-object p1
.end method
