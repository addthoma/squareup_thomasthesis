.class final Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner$onEnterScope$2;
.super Lkotlin/jvm/internal/Lambda;
.source "JediWorkflowRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/jedi/JediWorkflowOutput;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "result",
        "Lcom/squareup/jedi/JediWorkflowOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/jedi/JediWorkflowOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner$onEnterScope$2;->invoke(Lcom/squareup/jedi/JediWorkflowOutput;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/jedi/JediWorkflowOutput;)V
    .locals 3

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    instance-of v0, p1, Lcom/squareup/jedi/JediWorkflowOutput$GoBackFromJedi;

    if-eqz v0, :cond_0

    .line 41
    iget-object p1, p0, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;->access$getContainer$p(Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;

    aput-object v2, v0, v1

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/PosContainer;->goBackPast([Ljava/lang/Class;)V

    goto :goto_0

    .line 44
    :cond_0
    instance-of v0, p1, Lcom/squareup/jedi/JediWorkflowOutput$GoToLink;

    if-eqz v0, :cond_1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;->access$getBrowserLauncher$p(Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;)Lcom/squareup/util/BrowserLauncher;

    move-result-object v0

    check-cast p1, Lcom/squareup/jedi/JediWorkflowOutput$GoToLink;

    invoke-virtual {p1}, Lcom/squareup/jedi/JediWorkflowOutput$GoToLink;->getLink()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method
