.class final Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6$3;
.super Ljava/lang/Object;
.source "JediHelpCoordinator.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6;->invoke()Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Lcom/squareup/ui/help/MessagingViewState;",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "it",
        "Lcom/squareup/ui/help/MessagingViewState;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6$3;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6$3;

    invoke-direct {v0}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6$3;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6$3;->INSTANCE:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6$3;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/ui/help/MessagingViewState;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 3

    .line 128
    sget-object p1, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v0, v0, v1, v2}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/ui/help/MessagingViewState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6$3;->call(Lcom/squareup/ui/help/MessagingViewState;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method
