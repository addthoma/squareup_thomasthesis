.class public interface abstract Lcom/squareup/ui/help/api/HelpAppletSettings;
.super Ljava/lang/Object;
.source "HelpAppletSettings.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&J\u000e\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H&J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0007H&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/help/api/HelpAppletSettings;",
        "",
        "aboutVersionString",
        "",
        "canClickLibraryLinks",
        "",
        "helpAppletSectionsEntries",
        "",
        "Lcom/squareup/ui/help/HelpAppletSectionsListEntry;",
        "helpAppletTutorials",
        "Lcom/squareup/ui/help/HelpAppletContent;",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract aboutVersionString()Ljava/lang/String;
.end method

.method public abstract canClickLibraryLinks()Z
.end method

.method public abstract helpAppletSectionsEntries()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletSectionsListEntry;",
            ">;"
        }
    .end annotation
.end method

.method public abstract helpAppletTutorials()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;"
        }
    .end annotation
.end method
