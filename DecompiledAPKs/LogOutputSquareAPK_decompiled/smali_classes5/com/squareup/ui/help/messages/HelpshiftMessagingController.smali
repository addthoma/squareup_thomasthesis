.class public final Lcom/squareup/ui/help/messages/HelpshiftMessagingController;
.super Ljava/lang/Object;
.source "HelpshiftMessagingController.kt"

# interfaces
.implements Lcom/squareup/ui/help/MessagingController;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/messages/HelpshiftMessagingController$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0007\u0018\u0000 /2\u00020\u0001:\u0001/B7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016H\u0016J\u0010\u0010\u0018\u001a\u00020\u00132\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0008\u0010\u001b\u001a\u00020\u0010H\u0016J\u0008\u0010\u001c\u001a\u00020\u0010H\u0016J\u0010\u0010\u001d\u001a\u00020\u00102\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0008\u0010 \u001a\u00020\u0013H\u0016J\u0016\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\"0\u00162\u0006\u0010#\u001a\u00020\u0010H\u0016J\u0010\u0010$\u001a\u00020\u00132\u0006\u0010%\u001a\u00020&H\u0016J\u0008\u0010\'\u001a\u00020\u0013H\u0016J\u0010\u0010(\u001a\u00020\u00132\u0006\u0010!\u001a\u00020\"H\u0016J\u0018\u0010(\u001a\u00020\u00132\u0006\u0010!\u001a\u00020\"2\u0006\u0010)\u001a\u00020*H\u0016J\u0008\u0010+\u001a\u00020\u0010H\u0016J\u0010\u0010,\u001a\u00020\u00132\u0006\u0010-\u001a\u00020\u0017H\u0016J\u0008\u0010.\u001a\u00020\u0010H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0011\u001a\u0010\u0012\u000c\u0012\n \u0014*\u0004\u0018\u00010\u00130\u00130\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/ui/help/messages/HelpshiftMessagingController;",
        "Lcom/squareup/ui/help/MessagingController;",
        "messagehubState",
        "Lcom/squareup/ui/help/messages/MessagehubState;",
        "helpshiftService",
        "Lcom/squareup/helpshift/api/HelpshiftService;",
        "messagingNotifier",
        "Lcom/squareup/ui/help/SupportMessagingNotifier;",
        "messagesVisibility",
        "Lcom/squareup/ui/help/messages/MessagesVisibility;",
        "context",
        "Landroid/app/Application;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "(Lcom/squareup/ui/help/messages/MessagehubState;Lcom/squareup/helpshift/api/HelpshiftService;Lcom/squareup/ui/help/SupportMessagingNotifier;Lcom/squareup/ui/help/messages/MessagesVisibility;Landroid/app/Application;Lcom/squareup/tutorialv2/TutorialCore;)V",
        "helpshiftInstalled",
        "",
        "onLaunchHelpshift",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "badgeCount",
        "Lio/reactivex/Observable;",
        "",
        "getNotificationCount",
        "successHandler",
        "Landroid/os/Handler;",
        "helpshiftConversationActive",
        "helpshiftSDKInstalled",
        "installHelpshift",
        "messagingEligibilityState",
        "Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;",
        "launchMessagingActivity",
        "messagingViewState",
        "Lcom/squareup/ui/help/MessagingViewState;",
        "requireInstall",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "openMessagingActivity",
        "flow",
        "Lflow/Flow;",
        "showMessagingIcon",
        "updateUnreadMessagesCount",
        "count",
        "useHelpshift",
        "Companion",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/help/messages/HelpshiftMessagingController$Companion;

.field public static final MESSAGING_ACTIVITY_TRIGGERED:Ljava/lang/String; = "Messaging activity has been triggered"


# instance fields
.field private final context:Landroid/app/Application;

.field private helpshiftInstalled:Z

.field private final helpshiftService:Lcom/squareup/helpshift/api/HelpshiftService;

.field private final messagehubState:Lcom/squareup/ui/help/messages/MessagehubState;

.field private final messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;

.field private final messagingNotifier:Lcom/squareup/ui/help/SupportMessagingNotifier;

.field private final onLaunchHelpshift:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->Companion:Lcom/squareup/ui/help/messages/HelpshiftMessagingController$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/help/messages/MessagehubState;Lcom/squareup/helpshift/api/HelpshiftService;Lcom/squareup/ui/help/SupportMessagingNotifier;Lcom/squareup/ui/help/messages/MessagesVisibility;Landroid/app/Application;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "messagehubState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "helpshiftService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messagingNotifier"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messagesVisibility"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialCore"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->messagehubState:Lcom/squareup/ui/help/messages/MessagehubState;

    iput-object p2, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->helpshiftService:Lcom/squareup/helpshift/api/HelpshiftService;

    iput-object p3, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->messagingNotifier:Lcom/squareup/ui/help/SupportMessagingNotifier;

    iput-object p4, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;

    iput-object p5, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->context:Landroid/app/Application;

    iput-object p6, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    .line 47
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->onLaunchHelpshift:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$installHelpshift(Lcom/squareup/ui/help/messages/HelpshiftMessagingController;Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;)Z
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->installHelpshift(Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;)Z

    move-result p0

    return p0
.end method

.method private final installHelpshift(Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;)Z
    .locals 5

    .line 208
    iget-boolean v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->helpshiftInstalled:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->helpshiftService:Lcom/squareup/helpshift/api/HelpshiftService;

    .line 211
    invoke-virtual {p1}, Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;->getApiKey()Ljava/lang/String;

    move-result-object v2

    .line 212
    invoke-virtual {p1}, Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;->getDomainName()Ljava/lang/String;

    move-result-object v3

    .line 213
    invoke-virtual {p1}, Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;->getAppId()Ljava/lang/String;

    move-result-object v4

    .line 210
    invoke-interface {v0, v2, v3, v4}, Lcom/squareup/helpshift/api/HelpshiftService;->installHelpshift(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->helpshiftService:Lcom/squareup/helpshift/api/HelpshiftService;

    .line 218
    invoke-virtual {p1}, Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;->getHmacToken()Ljava/lang/String;

    move-result-object v2

    .line 219
    invoke-virtual {p1}, Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;->getDeviceLookupToken()Ljava/lang/String;

    move-result-object p1

    .line 217
    invoke-interface {v0, v2, p1}, Lcom/squareup/helpshift/api/HelpshiftService;->loginHelpshift(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    iput-boolean v1, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->helpshiftInstalled:Z

    .line 223
    :cond_1
    iget-boolean p1, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->helpshiftInstalled:Z

    return p1
.end method


# virtual methods
.method public badgeCount()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;

    invoke-virtual {v0}, Lcom/squareup/ui/help/messages/MessagesVisibility;->badgeCount()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationCount(Landroid/os/Handler;)V
    .locals 1

    const-string v0, "successHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->helpshiftSDKInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->helpshiftService:Lcom/squareup/helpshift/api/HelpshiftService;

    invoke-interface {v0, p1}, Lcom/squareup/helpshift/api/HelpshiftService;->getNotificationCount(Landroid/os/Handler;)V

    :cond_0
    return-void
.end method

.method public helpshiftConversationActive()Z
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->helpshiftSDKInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->helpshiftService:Lcom/squareup/helpshift/api/HelpshiftService;

    invoke-interface {v0}, Lcom/squareup/helpshift/api/HelpshiftService;->isConversationActive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public helpshiftSDKInstalled()Z
    .locals 1

    .line 108
    iget-boolean v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->helpshiftInstalled:Z

    return v0
.end method

.method public launchMessagingActivity()V
    .locals 2

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->onLaunchHelpshift:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public messagingViewState(Z)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/help/MessagingViewState;",
            ">;"
        }
    .end annotation

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->messagehubState:Lcom/squareup/ui/help/messages/MessagehubState;

    invoke-virtual {v0}, Lcom/squareup/ui/help/messages/MessagehubState;->singleMessagingEligibilityState()Lio/reactivex/Single;

    move-result-object v0

    .line 73
    new-instance v1, Lcom/squareup/ui/help/messages/HelpshiftMessagingController$messagingViewState$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController$messagingViewState$1;-><init>(Lcom/squareup/ui/help/messages/HelpshiftMessagingController;Z)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 101
    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    .line 103
    sget-object v0, Lcom/squareup/ui/help/MessagingViewState$UnableToLoad;->INSTANCE:Lcom/squareup/ui/help/MessagingViewState$UnableToLoad;

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    check-cast v0, Lio/reactivex/ObservableSource;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->onErrorResumeNext(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "messagehubState.singleMe\u2026gViewState.UnableToLoad))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->onLaunchHelpshift:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 54
    new-instance v1, Lcom/squareup/ui/help/messages/HelpshiftMessagingController$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController$onEnterScope$1;-><init>(Lcom/squareup/ui/help/messages/HelpshiftMessagingController;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 55
    new-instance v1, Lcom/squareup/ui/help/messages/HelpshiftMessagingController$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController$onEnterScope$2;-><init>(Lcom/squareup/ui/help/messages/HelpshiftMessagingController;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onLaunchHelpshift\n      \u2026enMessagingActivity(it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public openMessagingActivity(Lcom/squareup/ui/help/MessagingViewState;)V
    .locals 3

    const-string v0, "messagingViewState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    instance-of v0, p1, Lcom/squareup/ui/help/MessagingViewState$Ready;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 185
    invoke-virtual {p0, v0}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->updateUnreadMessagesCount(I)V

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Messaging activity has been triggered"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 189
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->context:Landroid/app/Application;

    check-cast v1, Landroid/content/Context;

    const-class v2, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    .line 190
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 192
    invoke-virtual {p0}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->useHelpshift()Z

    move-result v1

    const-string/jumbo v2, "useHelpshift"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 194
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 196
    check-cast p1, Lcom/squareup/ui/help/MessagingViewState$Ready;

    invoke-virtual {p1}, Lcom/squareup/ui/help/MessagingViewState$Ready;->getCustomIssueFields()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Ljava/io/Serializable;

    const-string v2, "customIssueFields"

    .line 195
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string p1, "IntentBundle"

    .line 197
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 200
    iget-object p1, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->context:Landroid/app/Application;

    invoke-virtual {p1, v0}, Landroid/app/Application;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 196
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.io.Serializable"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method public openMessagingActivity(Lcom/squareup/ui/help/MessagingViewState;Lflow/Flow;)V
    .locals 3

    const-string v0, "messagingViewState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    instance-of v0, p1, Lcom/squareup/ui/help/MessagingViewState$Disallowed;

    if-eqz v0, :cond_0

    .line 159
    new-instance v0, Lcom/squareup/register/widgets/WarningDialogScreen;

    .line 160
    new-instance v1, Lcom/squareup/widgets/warning/WarningStrings;

    check-cast p1, Lcom/squareup/ui/help/MessagingViewState$Disallowed;

    invoke-virtual {p1}, Lcom/squareup/ui/help/MessagingViewState$Disallowed;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/ui/help/MessagingViewState$Disallowed;->getHelpTipTitle()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/widgets/warning/Warning;

    .line 159
    invoke-direct {v0, v1}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    .line 158
    invoke-virtual {p2, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 165
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/help/MessagingViewState$UnableToLoad;

    if-eqz v0, :cond_1

    .line 167
    new-instance p1, Lcom/squareup/register/widgets/WarningDialogScreen;

    .line 168
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    .line 169
    sget v1, Lcom/squareup/pinpad/R$string;->please_try_again:I

    .line 170
    sget v2, Lcom/squareup/applet/help/R$string;->please_check_your_network_connection_try_again:I

    .line 168
    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    check-cast v0, Lcom/squareup/widgets/warning/Warning;

    .line 167
    invoke-direct {p1, v0}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    .line 166
    invoke-virtual {p2, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 176
    :cond_1
    instance-of p2, p1, Lcom/squareup/ui/help/MessagingViewState$Ready;

    if-eqz p2, :cond_2

    .line 177
    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->openMessagingActivity(Lcom/squareup/ui/help/MessagingViewState;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public showMessagingIcon()Z
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;

    invoke-virtual {v0}, Lcom/squareup/ui/help/messages/MessagesVisibility;->showMessagingIcon()Z

    move-result v0

    return v0
.end method

.method public updateUnreadMessagesCount(I)V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/messages/MessagesVisibility;->updateUnreadMessagesCount(I)V

    if-nez p1, :cond_0

    .line 139
    iget-object p1, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->messagingNotifier:Lcom/squareup/ui/help/SupportMessagingNotifier;

    invoke-interface {p1}, Lcom/squareup/ui/help/SupportMessagingNotifier;->removeNotification()V

    :cond_0
    return-void
.end method

.method public useHelpshift()Z
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->helpshiftService:Lcom/squareup/helpshift/api/HelpshiftService;

    instance-of v0, v0, Lcom/squareup/helpshift/RealHelpshiftService;

    return v0
.end method
