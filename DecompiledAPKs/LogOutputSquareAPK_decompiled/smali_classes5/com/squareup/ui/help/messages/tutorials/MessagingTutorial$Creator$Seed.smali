.class public final Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator$Seed;
.super Lcom/squareup/tutorialv2/TutorialSeed;
.source "MessagingTutorial.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "Seed"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0014\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator$Seed;",
        "Lcom/squareup/tutorialv2/TutorialSeed;",
        "(Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;)V",
        "doCreate",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 129
    iput-object p1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator$Seed;->this$0:Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;

    const/4 p1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p1}, Lcom/squareup/tutorialv2/TutorialSeed;-><init>(Lcom/squareup/tutorialv2/TutorialSeed$Priority;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method protected doCreate()Lcom/squareup/tutorialv2/Tutorial;
    .locals 2

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator$Seed;->this$0:Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;

    invoke-static {v0}, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;->access$getTutorialProvider$p(Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v1, "tutorialProvider.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/tutorialv2/Tutorial;

    return-object v0
.end method
