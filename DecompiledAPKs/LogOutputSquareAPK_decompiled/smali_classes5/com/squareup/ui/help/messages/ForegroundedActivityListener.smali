.class public final Lcom/squareup/ui/help/messages/ForegroundedActivityListener;
.super Ljava/lang/Object;
.source "ForegroundedActivityListener.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u0002J\u0008\u0010\u000b\u001a\u00020\u000cH\u0002J\u0008\u0010\r\u001a\u00020\u000cH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/help/messages/ForegroundedActivityListener;",
        "",
        "activityListener",
        "Lcom/squareup/ActivityListener;",
        "(Lcom/squareup/ActivityListener;)V",
        "foregroundedActivity",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/help/messages/ForegroundedActivity;",
        "getForegroundedActivity",
        "()Lio/reactivex/Observable;",
        "onForegroundedActivity",
        "resumedActivityIsHelpshift",
        "",
        "resumedActivityIsMain",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final activityListener:Lcom/squareup/ActivityListener;

.field private final foregroundedActivity:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/help/messages/ForegroundedActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ActivityListener;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "activityListener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;->activityListener:Lcom/squareup/ActivityListener;

    .line 25
    invoke-direct {p0}, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;->onForegroundedActivity()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;->foregroundedActivity:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getActivityListener$p(Lcom/squareup/ui/help/messages/ForegroundedActivityListener;)Lcom/squareup/ActivityListener;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;->activityListener:Lcom/squareup/ActivityListener;

    return-object p0
.end method

.method public static final synthetic access$resumedActivityIsHelpshift(Lcom/squareup/ui/help/messages/ForegroundedActivityListener;)Z
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;->resumedActivityIsHelpshift()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$resumedActivityIsMain(Lcom/squareup/ui/help/messages/ForegroundedActivityListener;)Z
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;->resumedActivityIsMain()Z

    move-result p0

    return p0
.end method

.method private final onForegroundedActivity()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/help/messages/ForegroundedActivity;",
            ">;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;-><init>(Lcom/squareup/ui/help/messages/ForegroundedActivityListener;)V

    check-cast v0, Lio/reactivex/ObservableOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026.onNext(Background)\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final resumedActivityIsHelpshift()Z
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;->activityListener:Lcom/squareup/ActivityListener;

    invoke-virtual {v0}, Lcom/squareup/ActivityListener;->getResumedActivityClassName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private final resumedActivityIsMain()Z
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;->activityListener:Lcom/squareup/ActivityListener;

    invoke-virtual {v0}, Lcom/squareup/ActivityListener;->getResumedActivityClassName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/main/MainActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final getForegroundedActivity()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/help/messages/ForegroundedActivity;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;->foregroundedActivity:Lio/reactivex/Observable;

    return-object v0
.end method
