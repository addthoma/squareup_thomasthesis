.class final Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource$notifications$1;
.super Ljava/lang/Object;
.source "SupportMessagesNotificationsSource.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->notifications()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;",
        "kotlin.jvm.PlatformType",
        "count",
        "",
        "apply",
        "(Ljava/lang/Integer;)Lio/reactivex/Observable;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource$notifications$1;->this$0:Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Integer;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;",
            ">;"
        }
    .end annotation

    const-string v0, "count"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource$notifications$1;->this$0:Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->access$id(Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->access$supportMessageState(Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource$notifications$1$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource$notifications$1$1;-><init>(Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource$notifications$1;Ljava/lang/Integer;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource$notifications$1;->apply(Ljava/lang/Integer;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
