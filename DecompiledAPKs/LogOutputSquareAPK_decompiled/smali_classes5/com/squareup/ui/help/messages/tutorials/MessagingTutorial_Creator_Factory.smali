.class public final Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial_Creator_Factory;
.super Ljava/lang/Object;
.source "MessagingTutorial_Creator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;",
        ">;"
    }
.end annotation


# instance fields
.field private final tutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial_Creator_Factory;->tutorialProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial_Creator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;",
            ">;)",
            "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial_Creator_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial_Creator_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial_Creator_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;)Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial;",
            ">;)",
            "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial_Creator_Factory;->tutorialProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial_Creator_Factory;->newInstance(Ljavax/inject/Provider;)Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial_Creator_Factory;->get()Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;

    move-result-object v0

    return-object v0
.end method
