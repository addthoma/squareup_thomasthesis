.class public final Lcom/squareup/ui/help/help/HelpSection_ListEntry_Factory;
.super Ljava/lang/Object;
.source "HelpSection_ListEntry_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/help/HelpSection$ListEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private final messagesVisibilityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/MessagesVisibility;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final sectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/help/HelpSection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/help/HelpSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/MessagesVisibility;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/help/help/HelpSection_ListEntry_Factory;->sectionProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/help/help/HelpSection_ListEntry_Factory;->resProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/ui/help/help/HelpSection_ListEntry_Factory;->messagesVisibilityProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/help/HelpSection_ListEntry_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/help/HelpSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/MessagesVisibility;",
            ">;)",
            "Lcom/squareup/ui/help/help/HelpSection_ListEntry_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/ui/help/help/HelpSection_ListEntry_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/help/help/HelpSection_ListEntry_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/help/help/HelpSection;Lcom/squareup/util/Res;Lcom/squareup/ui/help/messages/MessagesVisibility;)Lcom/squareup/ui/help/help/HelpSection$ListEntry;
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/ui/help/help/HelpSection$ListEntry;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/help/help/HelpSection$ListEntry;-><init>(Lcom/squareup/ui/help/help/HelpSection;Lcom/squareup/util/Res;Lcom/squareup/ui/help/messages/MessagesVisibility;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/help/HelpSection$ListEntry;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/help/help/HelpSection_ListEntry_Factory;->sectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/help/HelpSection;

    iget-object v1, p0, Lcom/squareup/ui/help/help/HelpSection_ListEntry_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/help/help/HelpSection_ListEntry_Factory;->messagesVisibilityProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/help/messages/MessagesVisibility;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/help/help/HelpSection_ListEntry_Factory;->newInstance(Lcom/squareup/ui/help/help/HelpSection;Lcom/squareup/util/Res;Lcom/squareup/ui/help/messages/MessagesVisibility;)Lcom/squareup/ui/help/help/HelpSection$ListEntry;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/help/help/HelpSection_ListEntry_Factory;->get()Lcom/squareup/ui/help/help/HelpSection$ListEntry;

    move-result-object v0

    return-object v0
.end method
