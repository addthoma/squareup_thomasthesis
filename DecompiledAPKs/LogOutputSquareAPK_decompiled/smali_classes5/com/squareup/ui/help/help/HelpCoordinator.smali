.class public Lcom/squareup/ui/help/help/HelpCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "HelpCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private frequentlyAskedQuestionsContentView:Landroid/view/ViewGroup;

.field private frequentlyAskedQuestionsHeader:Landroid/view/View;

.field private final helpScreenRunner:Lcom/squareup/ui/help/help/HelpScreen$Runner;

.field private learnMoreHeader:Landroid/view/View;

.field private learnMoreView:Landroid/view/ViewGroup;

.field private troubleshootingHeader:Landroid/view/View;

.field private troubleshootingView:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/help/HelpScreen$Runner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->helpScreenRunner:Lcom/squareup/ui/help/help/HelpScreen$Runner;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 99
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 100
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 102
    sget v0, Lcom/squareup/applet/help/R$id;->learn_more_header:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->learnMoreHeader:Landroid/view/View;

    .line 103
    sget v0, Lcom/squareup/applet/help/R$id;->learn_more_content:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->learnMoreView:Landroid/view/ViewGroup;

    .line 104
    sget v0, Lcom/squareup/applet/help/R$id;->frequently_asked_questions_header:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->frequentlyAskedQuestionsHeader:Landroid/view/View;

    .line 105
    sget v0, Lcom/squareup/applet/help/R$id;->frequently_asked_questions_content:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->frequentlyAskedQuestionsContentView:Landroid/view/ViewGroup;

    .line 106
    sget v0, Lcom/squareup/applet/help/R$id;->troubleshooting_header:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->troubleshootingHeader:Landroid/view/View;

    .line 107
    sget v0, Lcom/squareup/applet/help/R$id;->troubleshooting_content:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->troubleshootingView:Landroid/view/ViewGroup;

    return-void
.end method

.method private createRow(Landroid/view/ViewGroup;Landroid/content/res/Resources;Lcom/squareup/ui/help/HelpAppletContent;Landroid/view/View$OnClickListener;)Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 1

    .line 84
    sget v0, Lcom/squareup/applet/help/R$layout;->help_content_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 85
    iget v0, p3, Lcom/squareup/ui/help/HelpAppletContent;->titleStringId:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object p2, p3, Lcom/squareup/ui/help/HelpAppletContent;->subtitle:Ljava/lang/CharSequence;

    if-eqz p2, :cond_0

    .line 87
    iget-object p2, p3, Lcom/squareup/ui/help/HelpAppletContent;->subtitle:Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    const/4 p2, 0x1

    .line 88
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 90
    :cond_0
    invoke-static {p4}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p1
.end method

.method private getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->helpScreenRunner:Lcom/squareup/ui/help/help/HelpScreen$Runner;

    sget v1, Lcom/squareup/applet/help/R$string;->help:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/help/help/HelpScreen$Runner;->getActionBarConfig(Ljava/lang/String;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method private updateContent(Landroid/content/Context;Lcom/squareup/ui/help/help/HelpScreenData;)V
    .locals 5

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->learnMoreHeader:Landroid/view/View;

    iget-object v1, p2, Lcom/squareup/ui/help/help/HelpScreenData;->learnMoreContent:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->learnMoreView:Landroid/view/ViewGroup;

    iget-object v1, p2, Lcom/squareup/ui/help/help/HelpScreenData;->learnMoreContent:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 53
    iget-object v0, p2, Lcom/squareup/ui/help/help/HelpScreenData;->learnMoreContent:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/help/HelpAppletContent;

    .line 54
    iget-object v2, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->learnMoreView:Landroid/view/ViewGroup;

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-instance v4, Lcom/squareup/ui/help/help/-$$Lambda$HelpCoordinator$RaMYtAahGAGCeBj0uj3BYzE5QBM;

    invoke-direct {v4, p0, p1, v1}, Lcom/squareup/ui/help/help/-$$Lambda$HelpCoordinator$RaMYtAahGAGCeBj0uj3BYzE5QBM;-><init>(Lcom/squareup/ui/help/help/HelpCoordinator;Landroid/content/Context;Lcom/squareup/ui/help/HelpAppletContent;)V

    invoke-direct {p0, v2, v3, v1, v4}, Lcom/squareup/ui/help/help/HelpCoordinator;->createRow(Landroid/view/ViewGroup;Landroid/content/res/Resources;Lcom/squareup/ui/help/HelpAppletContent;Landroid/view/View$OnClickListener;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v1

    .line 54
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->frequentlyAskedQuestionsHeader:Landroid/view/View;

    iget-object v1, p2, Lcom/squareup/ui/help/help/HelpScreenData;->frequentlyAskedQuestionsContent:Ljava/util/List;

    .line 61
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    .line 60
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->frequentlyAskedQuestionsContentView:Landroid/view/ViewGroup;

    iget-object v1, p2, Lcom/squareup/ui/help/help/HelpScreenData;->frequentlyAskedQuestionsContent:Ljava/util/List;

    .line 63
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    .line 62
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 64
    iget-object v0, p2, Lcom/squareup/ui/help/help/HelpScreenData;->frequentlyAskedQuestionsContent:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/help/HelpAppletContent;

    .line 65
    iget-object v2, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->frequentlyAskedQuestionsContentView:Landroid/view/ViewGroup;

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-instance v4, Lcom/squareup/ui/help/help/-$$Lambda$HelpCoordinator$RKDb7jJ-Rgw0q68zU-2ZooIare0;

    invoke-direct {v4, p0, p1, v1}, Lcom/squareup/ui/help/help/-$$Lambda$HelpCoordinator$RKDb7jJ-Rgw0q68zU-2ZooIare0;-><init>(Lcom/squareup/ui/help/help/HelpCoordinator;Landroid/content/Context;Lcom/squareup/ui/help/HelpAppletContent;)V

    invoke-direct {p0, v2, v3, v1, v4}, Lcom/squareup/ui/help/help/HelpCoordinator;->createRow(Landroid/view/ViewGroup;Landroid/content/res/Resources;Lcom/squareup/ui/help/HelpAppletContent;Landroid/view/View$OnClickListener;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v1

    .line 65
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->troubleshootingHeader:Landroid/view/View;

    iget-object v1, p2, Lcom/squareup/ui/help/help/HelpScreenData;->troubleshootingContent:Ljava/util/List;

    .line 72
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    .line 71
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->troubleshootingView:Landroid/view/ViewGroup;

    iget-object v1, p2, Lcom/squareup/ui/help/help/HelpScreenData;->troubleshootingContent:Ljava/util/List;

    .line 74
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    .line 73
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 75
    iget-object p2, p2, Lcom/squareup/ui/help/help/HelpScreenData;->troubleshootingContent:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/HelpAppletContent;

    .line 76
    iget-object v1, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->troubleshootingView:Landroid/view/ViewGroup;

    .line 77
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/help/help/-$$Lambda$HelpCoordinator$d8AHVCifj1gs9PAwVja_4yIWxIY;

    invoke-direct {v3, p0, p1, v0}, Lcom/squareup/ui/help/help/-$$Lambda$HelpCoordinator$d8AHVCifj1gs9PAwVja_4yIWxIY;-><init>(Lcom/squareup/ui/help/help/HelpCoordinator;Landroid/content/Context;Lcom/squareup/ui/help/HelpAppletContent;)V

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/squareup/ui/help/help/HelpCoordinator;->createRow(Landroid/view/ViewGroup;Landroid/content/res/Resources;Lcom/squareup/ui/help/HelpAppletContent;Landroid/view/View$OnClickListener;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v0

    .line 76
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_2

    :cond_2
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/help/HelpCoordinator;->bindViews(Landroid/view/View;)V

    .line 41
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/ui/help/help/HelpCoordinator;->getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 45
    new-instance v1, Lcom/squareup/ui/help/help/-$$Lambda$HelpCoordinator$GztBGmZ-94IcpicHJwx2Aft8BtQ;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/help/help/-$$Lambda$HelpCoordinator$GztBGmZ-94IcpicHJwx2Aft8BtQ;-><init>(Lcom/squareup/ui/help/help/HelpCoordinator;Landroid/content/Context;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$1$HelpCoordinator(Landroid/content/Context;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->helpScreenRunner:Lcom/squareup/ui/help/help/HelpScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/help/help/HelpScreen$Runner;->helpScreenData()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/help/-$$Lambda$HelpCoordinator$g69qv-rn0aMzD3TQuv-0v8fEFgY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/help/help/-$$Lambda$HelpCoordinator$g69qv-rn0aMzD3TQuv-0v8fEFgY;-><init>(Lcom/squareup/ui/help/help/HelpCoordinator;Landroid/content/Context;)V

    .line 46
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$0$HelpCoordinator(Landroid/content/Context;Lcom/squareup/ui/help/help/HelpScreenData;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/help/help/HelpCoordinator;->updateContent(Landroid/content/Context;Lcom/squareup/ui/help/help/HelpScreenData;)V

    return-void
.end method

.method public synthetic lambda$updateContent$2$HelpCoordinator(Landroid/content/Context;Lcom/squareup/ui/help/HelpAppletContent;Landroid/view/View;)V
    .locals 0

    .line 56
    iget-object p3, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->helpScreenRunner:Lcom/squareup/ui/help/help/HelpScreen$Runner;

    invoke-interface {p3, p1, p2}, Lcom/squareup/ui/help/help/HelpScreen$Runner;->helpContentClicked(Landroid/content/Context;Lcom/squareup/ui/help/HelpAppletContent;)V

    return-void
.end method

.method public synthetic lambda$updateContent$3$HelpCoordinator(Landroid/content/Context;Lcom/squareup/ui/help/HelpAppletContent;Landroid/view/View;)V
    .locals 0

    .line 67
    iget-object p3, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->helpScreenRunner:Lcom/squareup/ui/help/help/HelpScreen$Runner;

    invoke-interface {p3, p1, p2}, Lcom/squareup/ui/help/help/HelpScreen$Runner;->helpContentClicked(Landroid/content/Context;Lcom/squareup/ui/help/HelpAppletContent;)V

    return-void
.end method

.method public synthetic lambda$updateContent$4$HelpCoordinator(Landroid/content/Context;Lcom/squareup/ui/help/HelpAppletContent;Landroid/view/View;)V
    .locals 0

    .line 78
    iget-object p3, p0, Lcom/squareup/ui/help/help/HelpCoordinator;->helpScreenRunner:Lcom/squareup/ui/help/help/HelpScreen$Runner;

    invoke-interface {p3, p1, p2}, Lcom/squareup/ui/help/help/HelpScreen$Runner;->helpContentClicked(Landroid/content/Context;Lcom/squareup/ui/help/HelpAppletContent;)V

    return-void
.end method
