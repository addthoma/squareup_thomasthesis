.class final Lcom/squareup/ui/component/PosComponentFactory$WithComponentFactory;
.super Ljava/lang/Object;
.source "PosComponentFactory.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/component/PosComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "WithComponentFactory"
.end annotation


# instance fields
.field private final componentClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)V"
        }
    .end annotation

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p1, p0, Lcom/squareup/ui/component/PosComponentFactory$WithComponentFactory;->componentClass:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 0

    .line 87
    iget-object p2, p0, Lcom/squareup/ui/component/PosComponentFactory$WithComponentFactory;->componentClass:Ljava/lang/Class;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->createChildComponent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
