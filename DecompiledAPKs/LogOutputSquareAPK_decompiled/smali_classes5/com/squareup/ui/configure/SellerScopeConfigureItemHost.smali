.class public Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;
.super Ljava/lang/Object;
.source "SellerScopeConfigureItemHost.java"

# interfaces
.implements Lcom/squareup/configure/item/ConfigureItemHost;


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final diningOptionCache:Lcom/squareup/ui/main/DiningOptionCache;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionComps:Lcom/squareup/payment/TransactionComps;

.field private final transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionComps;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/main/DiningOptionCache;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->device:Lcom/squareup/util/Device;

    .line 46
    iput-object p2, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transaction:Lcom/squareup/payment/Transaction;

    .line 47
    iput-object p3, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transactionComps:Lcom/squareup/payment/TransactionComps;

    .line 48
    iput-object p4, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    .line 49
    iput-object p5, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 50
    iput-object p6, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->diningOptionCache:Lcom/squareup/ui/main/DiningOptionCache;

    return-void
.end method


# virtual methods
.method public commitCartItem(Lcom/squareup/checkout/CartItem;)V
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->addOrderItem(Lcom/squareup/checkout/CartItem;)V

    return-void
.end method

.method public compItem(ILcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;)V
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transactionComps:Lcom/squareup/payment/TransactionComps;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/payment/TransactionComps;->compItem(ILcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;)V

    return-void
.end method

.method public getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getAvailableTaxRules()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAvailableTaxRules()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAvailableTaxes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAvailableTaxes()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentDiningOption()Lcom/squareup/checkout/DiningOption;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCurrentDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    return-object v0
.end method

.method public getDiningOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/DiningOption;",
            ">;"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->diningOptionCache:Lcom/squareup/ui/main/DiningOptionCache;

    invoke-virtual {v0}, Lcom/squareup/ui/main/DiningOptionCache;->getDiningOptions()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getGiftCardTotal()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getGiftCardTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getOrderItem(I)Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->getOrderItem(I)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    return-object p1
.end method

.method public getOrderItemCopy(I)Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->getOrderItem(I)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    return-object p1
.end method

.method public hasTicket()Z
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    return v0
.end method

.method public isVoidCompAllowed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onCancelSelected(Z)Z
    .locals 0

    if-eqz p1, :cond_0

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryScreenState;->clearFlyByAnimationData()V

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onCommit(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V
    .locals 1

    if-nez p2, :cond_0

    .line 69
    iget-object p2, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    invoke-virtual {p2, p1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;)V

    goto :goto_0

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V

    :goto_0
    return-void
.end method

.method public onCommitSuccess(Lcom/squareup/configure/item/ConfigureItemState;Z)Z
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->device:Lcom/squareup/util/Device;

    invoke-static {v0}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->supportsFlyByAnimations(Lcom/squareup/util/Device;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 79
    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemState;->getQuantity()Ljava/math/BigDecimal;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/BigDecimals;->asIntOr1(Ljava/math/BigDecimal;)I

    move-result p1

    .line 80
    iget-object p2, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p2}, Lcom/squareup/orderentry/OrderEntryScreenState;->getFlyByAnimationData()Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->setQuantity(I)V

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onDeleteSelected(Lcom/squareup/configure/item/ConfigureItemState;)Z
    .locals 0

    .line 95
    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemState;->delete()V

    const/4 p1, 0x0

    return p1
.end method

.method public onSelectedVariationClicked()V
    .locals 3

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL:Lcom/squareup/analytics/RegisterViewName;

    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL_SELECTED_VARIATION_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public onStartVisualTransition(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logItemEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V

    return-void
.end method

.method public onVariablePriceButtonClicked()V
    .locals 3

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL:Lcom/squareup/analytics/RegisterViewName;

    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL_VARIABLE_PRICE_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public onVariationCheckChanged()V
    .locals 3

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL:Lcom/squareup/analytics/RegisterViewName;

    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL_NOT_SELECTED_VARIATION_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public removeItem(I)V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->removeItem(I)V

    return-void
.end method

.method public replaceItem(ILcom/squareup/checkout/CartItem;)V
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/payment/Transaction;->replaceItem(ILcom/squareup/checkout/CartItem;)V

    return-void
.end method

.method public uncompItem(I)V
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transactionComps:Lcom/squareup/payment/TransactionComps;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/TransactionComps;->uncompItem(I)V

    return-void
.end method

.method public voidItem(ILjava/lang/String;Lcom/squareup/protos/client/Employee;)V
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/payment/Transaction;->voidItem(ILjava/lang/String;Lcom/squareup/protos/client/Employee;)V

    return-void
.end method
