.class Lcom/squareup/ui/DropDownContainer$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "DropDownContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/DropDownContainer;->openDropDown()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/DropDownContainer;


# direct methods
.method constructor <init>(Lcom/squareup/ui/DropDownContainer;)V
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/squareup/ui/DropDownContainer$1;->this$0:Lcom/squareup/ui/DropDownContainer;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .line 316
    iget-object p1, p0, Lcom/squareup/ui/DropDownContainer$1;->this$0:Lcom/squareup/ui/DropDownContainer;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/squareup/ui/DropDownContainer;->access$200(Lcom/squareup/ui/DropDownContainer;Z)V

    .line 317
    iget-object p1, p0, Lcom/squareup/ui/DropDownContainer$1;->this$0:Lcom/squareup/ui/DropDownContainer;

    iget-object p1, p1, Lcom/squareup/ui/DropDownContainer;->listener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/DropDownContainer$1;->this$0:Lcom/squareup/ui/DropDownContainer;

    iget-object p1, p1, Lcom/squareup/ui/DropDownContainer;->listener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer$1;->this$0:Lcom/squareup/ui/DropDownContainer;

    iget-object v0, v0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-interface {p1, v0}, Lcom/squareup/ui/DropDownContainer$DropDownListener;->onDropDownShown(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    .line 312
    iget-object p1, p0, Lcom/squareup/ui/DropDownContainer$1;->this$0:Lcom/squareup/ui/DropDownContainer;

    iget-object p1, p1, Lcom/squareup/ui/DropDownContainer;->listener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/DropDownContainer$1;->this$0:Lcom/squareup/ui/DropDownContainer;

    iget-object p1, p1, Lcom/squareup/ui/DropDownContainer;->listener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer$1;->this$0:Lcom/squareup/ui/DropDownContainer;

    iget-object v0, v0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-interface {p1, v0}, Lcom/squareup/ui/DropDownContainer$DropDownListener;->onDropDownOpening(Landroid/view/View;)V

    :cond_0
    return-void
.end method
