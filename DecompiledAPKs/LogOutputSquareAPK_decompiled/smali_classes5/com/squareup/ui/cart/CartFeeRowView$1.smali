.class Lcom/squareup/ui/cart/CartFeeRowView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "CartFeeRowView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/cart/CartFeeRowView;-><init>(Landroid/content/Context;Lcom/squareup/ui/cart/AbstractCartFeesPresenter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/CartFeeRowView;

.field final synthetic val$presenter:Lcom/squareup/ui/cart/AbstractCartFeesPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/CartFeeRowView;Lcom/squareup/ui/cart/AbstractCartFeesPresenter;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/cart/CartFeeRowView$1;->this$0:Lcom/squareup/ui/cart/CartFeeRowView;

    iput-object p2, p0, Lcom/squareup/ui/cart/CartFeeRowView$1;->val$presenter:Lcom/squareup/ui/cart/AbstractCartFeesPresenter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/cart/CartFeeRowView$1;->val$presenter:Lcom/squareup/ui/cart/AbstractCartFeesPresenter;

    instance-of v0, p1, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;

    if-eqz v0, :cond_0

    .line 52
    check-cast p1, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeeRowView$1;->this$0:Lcom/squareup/ui/cart/CartFeeRowView;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->deleteIfPermitted(Lcom/squareup/ui/cart/CartFeeRowView;)V

    goto :goto_0

    .line 54
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/cart/CartFeeRowView$1;->this$0:Lcom/squareup/ui/cart/CartFeeRowView;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartFeeRowView;->doDelete()V

    :goto_0
    return-void
.end method
