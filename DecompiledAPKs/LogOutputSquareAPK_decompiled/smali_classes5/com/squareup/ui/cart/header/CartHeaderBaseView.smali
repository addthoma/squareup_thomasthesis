.class public abstract Lcom/squareup/ui/cart/header/CartHeaderBaseView;
.super Landroid/widget/RelativeLayout;
.source "CartHeaderBaseView.java"


# static fields
.field protected static final NORMAL_ANIMATION_DURATION_MS:I = 0xfa

.field protected static final QUICK_ANIMATION_DURATION_MS:I = 0x96


# instance fields
.field protected currentSaleContainer:Landroid/view/View;

.field protected currentSaleLabel:Landroid/widget/TextView;

.field protected currentSaleOutSet:Landroid/view/animation/AnimationSet;

.field protected fadeOutNoSale:Landroid/view/animation/AlphaAnimation;

.field protected flyout:Landroid/widget/TextView;

.field protected noSale:Landroid/view/View;

.field protected saleQuantity:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .line 38
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 40
    sget v0, Lcom/squareup/orderentry/R$id;->cart_header_no_sale:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBaseView;->noSale:Landroid/view/View;

    .line 41
    sget v0, Lcom/squareup/orderentry/R$id;->cart_header_current_sale:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBaseView;->currentSaleContainer:Landroid/view/View;

    .line 42
    sget v0, Lcom/squareup/orderentry/R$id;->cart_header_sale_quantity:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBaseView;->saleQuantity:Landroid/widget/TextView;

    .line 43
    sget v0, Lcom/squareup/orderentry/R$id;->cart_header_current_sale_label:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBaseView;->currentSaleLabel:Landroid/widget/TextView;

    .line 44
    sget v0, Lcom/squareup/orderentry/R$id;->cart_header_flyout:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBaseView;->flyout:Landroid/widget/TextView;

    return-void
.end method

.method protected setSaleQuantity(I)V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBaseView;->saleQuantity:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method protected abstract showCurrentSale(ZLjava/lang/String;)V
.end method

.method protected abstract showNoSale(Z)V
.end method
