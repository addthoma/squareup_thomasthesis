.class public Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;
.super Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;
.source "CartHeaderTabletPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/cart/header/CartHeaderBasePresenter<",
        "Lcom/squareup/ui/cart/header/CartHeaderTabletView;",
        ">;"
    }
.end annotation


# instance fields
.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final cartMenuDropDownPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

.field private final device:Lcom/squareup/util/Device;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/util/Res;Lcom/squareup/util/Device;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p6

    move-object v5, p7

    .line 35
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/util/Res;)V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->bus:Lcom/squareup/badbus/BadBus;

    .line 37
    iput-object p5, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->cartMenuDropDownPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    .line 38
    iput-object p6, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 39
    iput-object p8, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method private onTicketEdited()V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->updateState()V

    return-void
.end method

.method private updateDropDownState()V
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 91
    iget-object v1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->cartMenuDropDownPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->hasAtLeastOneEnabledOption()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method private updateState()V
    .locals 0

    .line 81
    invoke-virtual {p0}, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->updateSaleTextAndQuantity()V

    .line 82
    invoke-direct {p0}, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->updateDropDownState()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$1$CartHeaderTabletPresenter(Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/squareup/ui/cart/header/CartHeaderTabletView;Lkotlin/Unit;)V
    .locals 1

    .line 59
    iget-object p3, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {p3}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result p3

    if-eqz p3, :cond_0

    .line 60
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 61
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/orderentry/R$string;->crm_customer_added_format:I

    invoke-interface {p1, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p3, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 62
    invoke-virtual {p3}, Lcom/squareup/payment/Transaction;->getCustomerDisplayNameOrBlank()Ljava/lang/String;

    move-result-object p3

    const-string v0, "name"

    invoke-virtual {p1, v0, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 63
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 64
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 61
    invoke-virtual {p2, p1}, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->animateFlyout(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$0$CartHeaderTabletPresenter(Lcom/squareup/tickets/TicketEdited;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->onTicketEdited()V

    return-void
.end method

.method public synthetic lambda$onLoad$2$CartHeaderTabletPresenter(Lcom/squareup/ui/cart/header/CartHeaderTabletView;)Lrx/Subscription;
    .locals 3

    .line 56
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 57
    iget-object v1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->onCustomerChanged()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderTabletPresenter$KhouZ6y28af3gJU-qXPdTHCz1Uk;

    invoke-direct {v2, p0, v0, p1}, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderTabletPresenter$KhouZ6y28af3gJU-qXPdTHCz1Uk;-><init>(Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/squareup/ui/cart/header/CartHeaderTabletView;)V

    .line 58
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    const/4 v1, 0x1

    .line 67
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-object p1
.end method

.method onCartUpdated()V
    .locals 0

    .line 77
    invoke-direct {p0}, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->updateState()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 43
    invoke-super {p0, p1}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/tickets/TicketEdited;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderTabletPresenter$9Wm_39i-5q6Yrb74vHJz5iWyK2A;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderTabletPresenter$9Wm_39i-5q6Yrb74vHJz5iWyK2A;-><init>(Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 48
    invoke-super {p0, p1}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->onLoad(Landroid/os/Bundle;)V

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/header/CartHeaderTabletView;

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->updateDropDownState()V

    .line 54
    new-instance v0, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderTabletPresenter$B2x5LxU4XsR8A-ISGAHhS2opmy4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderTabletPresenter$B2x5LxU4XsR8A-ISGAHhS2opmy4;-><init>(Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;Lcom/squareup/ui/cart/header/CartHeaderTabletView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
