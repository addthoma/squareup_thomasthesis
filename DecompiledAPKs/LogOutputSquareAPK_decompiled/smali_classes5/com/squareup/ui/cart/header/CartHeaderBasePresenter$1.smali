.class Lcom/squareup/ui/cart/header/CartHeaderBasePresenter$1;
.super Ljava/lang/Object;
.source "CartHeaderBasePresenter.java"

# interfaces
.implements Lcom/squareup/pauses/PausesAndResumes;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter$1;->this$0:Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPause()V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter$1;->this$0:Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->updateSaleTextAndQuantity()V

    return-void
.end method
