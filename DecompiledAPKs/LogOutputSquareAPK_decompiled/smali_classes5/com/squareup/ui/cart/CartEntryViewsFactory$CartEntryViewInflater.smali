.class public interface abstract Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;
.super Ljava/lang/Object;
.source "CartEntryViewsFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartEntryViewsFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CartEntryViewInflater"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater$CheckoutCartEntryViewInflater;,
        Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\tJ\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0018\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;",
        "",
        "inflate",
        "Lcom/squareup/ui/cart/CartEntryView;",
        "context",
        "Landroid/content/Context;",
        "mapToEntryViews",
        "model",
        "Lcom/squareup/ui/cart/CartEntryViewModel;",
        "CheckoutCartEntryViewInflater",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract inflate(Landroid/content/Context;)Lcom/squareup/ui/cart/CartEntryView;
.end method

.method public abstract mapToEntryViews(Landroid/content/Context;Lcom/squareup/ui/cart/CartEntryViewModel;)Lcom/squareup/ui/cart/CartEntryView;
.end method
