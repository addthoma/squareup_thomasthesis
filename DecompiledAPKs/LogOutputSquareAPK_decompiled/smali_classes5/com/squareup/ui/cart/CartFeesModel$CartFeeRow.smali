.class public Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;
.super Ljava/lang/Object;
.source "CartFeesModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartFeesModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CartFeeRow"
.end annotation


# instance fields
.field public applicableQuantity:Ljava/lang/String;

.field appliedAmount:J

.field count:I

.field public final fee:Lcom/squareup/checkout/Adjustment;

.field name:Ljava/lang/String;

.field private warning:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/checkout/Adjustment;)V
    .locals 0

    .line 345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346
    iput-object p1, p0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->fee:Lcom/squareup/checkout/Adjustment;

    return-void
.end method

.method static synthetic access$002(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 337
    iput-object p1, p0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->warning:Ljava/lang/String;

    return-object p1
.end method

.method public static getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator<",
            "Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;",
            ">;"
        }
    .end annotation

    .line 373
    sget-object v0, Lcom/squareup/ui/cart/-$$Lambda$CartFeesModel$CartFeeRow$vlWHB_m8pGDZiaKtUhIqSG-mw1A;->INSTANCE:Lcom/squareup/ui/cart/-$$Lambda$CartFeesModel$CartFeeRow$vlWHB_m8pGDZiaKtUhIqSG-mw1A;

    return-object v0
.end method

.method static synthetic lambda$getComparator$0(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;)I
    .locals 1

    .line 373
    sget-object v0, Lcom/squareup/checkout/Adjustment;->DISPLAY_COMPARATOR:Ljava/util/Comparator;

    iget-object p0, p0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->fee:Lcom/squareup/checkout/Adjustment;

    iget-object p1, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->fee:Lcom/squareup/checkout/Adjustment;

    invoke-interface {v0, p0, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p0

    return p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 358
    instance-of v0, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 360
    :cond_0
    check-cast p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    .line 361
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->fee:Lcom/squareup/checkout/Adjustment;

    iget-object v2, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->fee:Lcom/squareup/checkout/Adjustment;

    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->count:I

    iget v2, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->count:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->applicableQuantity:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->applicableQuantity:Ljava/lang/String;

    .line 363
    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->name:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->name:Ljava/lang/String;

    .line 364
    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->warning:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->warning:Ljava/lang/String;

    .line 365
    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 350
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getWarning()Ljava/lang/String;
    .locals 1

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->warning:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    .line 369
    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->fee:Lcom/squareup/checkout/Adjustment;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->applicableQuantity:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->name:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->count:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->warning:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
