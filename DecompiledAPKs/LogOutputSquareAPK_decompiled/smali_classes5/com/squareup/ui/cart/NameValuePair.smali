.class public final Lcom/squareup/ui/cart/NameValuePair;
.super Ljava/lang/Object;
.source "CartEntryViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/NameValuePair$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B#\u0008\u0016\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u0019\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0008J\u000b\u0010\u000c\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\r\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J!\u0010\u000e\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\n\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/cart/NameValuePair;",
        "",
        "name",
        "",
        "value",
        "appearance",
        "Lcom/squareup/ui/cart/Appearance;",
        "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/ui/cart/Appearance;)V",
        "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V",
        "getName",
        "()Ljava/lang/CharSequence;",
        "getValue",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/cart/NameValuePair$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private final name:Ljava/lang/CharSequence;

.field private final value:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/cart/NameValuePair$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/cart/NameValuePair$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/cart/NameValuePair;->Companion:Lcom/squareup/ui/cart/NameValuePair$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/cart/NameValuePair;->name:Ljava/lang/CharSequence;

    iput-object p2, p0, Lcom/squareup/ui/cart/NameValuePair;->value:Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/ui/cart/Appearance;)V
    .locals 1

    const-string v0, "appearance"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    sget-object v0, Lcom/squareup/ui/cart/NameValuePair;->Companion:Lcom/squareup/ui/cart/NameValuePair$Companion;

    invoke-virtual {v0, p1, p3}, Lcom/squareup/ui/cart/NameValuePair$Companion;->maybeStrikethrough(Ljava/lang/CharSequence;Lcom/squareup/ui/cart/Appearance;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 74
    sget-object v0, Lcom/squareup/ui/cart/NameValuePair;->Companion:Lcom/squareup/ui/cart/NameValuePair$Companion;

    invoke-virtual {v0, p2, p3}, Lcom/squareup/ui/cart/NameValuePair$Companion;->maybeStrikethrough(Ljava/lang/CharSequence;Lcom/squareup/ui/cart/Appearance;)Ljava/lang/CharSequence;

    move-result-object p2

    .line 72
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/cart/NameValuePair;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/cart/NameValuePair;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/Object;)Lcom/squareup/ui/cart/NameValuePair;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/cart/NameValuePair;->name:Ljava/lang/CharSequence;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/cart/NameValuePair;->value:Ljava/lang/CharSequence;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/cart/NameValuePair;->copy(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/ui/cart/NameValuePair;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/NameValuePair;->name:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component2()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/NameValuePair;->value:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final copy(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/ui/cart/NameValuePair;
    .locals 1

    new-instance v0, Lcom/squareup/ui/cart/NameValuePair;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/cart/NameValuePair;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/cart/NameValuePair;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/cart/NameValuePair;

    iget-object v0, p0, Lcom/squareup/ui/cart/NameValuePair;->name:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/ui/cart/NameValuePair;->name:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/NameValuePair;->value:Ljava/lang/CharSequence;

    iget-object p1, p1, Lcom/squareup/ui/cart/NameValuePair;->value:Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getName()Ljava/lang/CharSequence;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/cart/NameValuePair;->name:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getValue()Ljava/lang/CharSequence;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/cart/NameValuePair;->value:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/cart/NameValuePair;->name:Ljava/lang/CharSequence;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/cart/NameValuePair;->value:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NameValuePair(name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/NameValuePair;->name:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/NameValuePair;->value:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
