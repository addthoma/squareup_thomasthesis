.class public Lcom/squareup/ui/cart/CartAdapterItem$TicketNoteRow;
.super Ljava/lang/Object;
.source "CartAdapterItem.java"

# interfaces
.implements Lcom/squareup/ui/cart/CartAdapterItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartAdapterItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TicketNoteRow"
.end annotation


# instance fields
.field public final note:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lcom/squareup/ui/cart/CartAdapterItem$TicketNoteRow;->note:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .line 103
    sget-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TICKET_NOTE:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    invoke-static {v0}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->access$000(Lcom/squareup/ui/cart/CartAdapterItem$RowType;)I

    move-result v0

    return v0
.end method

.method public getType()Lcom/squareup/ui/cart/CartAdapterItem$RowType;
    .locals 1

    .line 99
    sget-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TICKET_NOTE:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object v0
.end method
