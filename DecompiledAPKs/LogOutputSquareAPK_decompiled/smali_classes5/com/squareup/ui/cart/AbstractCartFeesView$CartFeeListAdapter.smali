.class Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;
.super Landroid/widget/BaseAdapter;
.source "AbstractCartFeesView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/AbstractCartFeesView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CartFeeListAdapter"
.end annotation


# static fields
.field private static final FEE_ROW:I = 0x0

.field private static final HELP_TEXT_ROW:I = 0x1

.field private static final ROW_TYPE_COUNT:I = 0x2


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/AbstractCartFeesView;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/cart/AbstractCartFeesView;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;->this$0:Lcom/squareup/ui/cart/AbstractCartFeesView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/cart/AbstractCartFeesView;Lcom/squareup/ui/cart/AbstractCartFeesView$1;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;-><init>(Lcom/squareup/ui/cart/AbstractCartFeesView;)V

    return-void
.end method

.method private buildFeeRow(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    if-nez p2, :cond_0

    .line 91
    new-instance p2, Lcom/squareup/ui/cart/CartFeeRowView;

    .line 92
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p3

    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;->this$0:Lcom/squareup/ui/cart/AbstractCartFeesView;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/AbstractCartFeesView;->getPresenter()Lcom/squareup/ui/cart/AbstractCartFeesPresenter;

    move-result-object v0

    invoke-direct {p2, p3, v0}, Lcom/squareup/ui/cart/CartFeeRowView;-><init>(Landroid/content/Context;Lcom/squareup/ui/cart/AbstractCartFeesPresenter;)V

    goto :goto_0

    :cond_0
    check-cast p2, Lcom/squareup/ui/cart/CartFeeRowView;

    .line 94
    :goto_0
    iget-object p3, p0, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;->this$0:Lcom/squareup/ui/cart/AbstractCartFeesView;

    invoke-virtual {p3}, Lcom/squareup/ui/cart/AbstractCartFeesView;->getPresenter()Lcom/squareup/ui/cart/AbstractCartFeesPresenter;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->initRowView(ILcom/squareup/ui/cart/CartFeeRowView;)V

    .line 95
    invoke-virtual {p2}, Lcom/squareup/ui/cart/CartFeeRowView;->enter()V

    return-object p2
.end method

.method private buildHelpTextRow(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    if-nez p1, :cond_0

    .line 102
    new-instance p1, Lcom/squareup/register/widgets/list/HelpRow;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;->this$0:Lcom/squareup/ui/cart/AbstractCartFeesView;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/AbstractCartFeesView;->getPresenter()Lcom/squareup/ui/cart/AbstractCartFeesPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->getHelpText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/squareup/register/widgets/list/HelpRow;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    :cond_0
    return-object p1
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;->this$0:Lcom/squareup/ui/cart/AbstractCartFeesView;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/AbstractCartFeesView;->getPresenter()Lcom/squareup/ui/cart/AbstractCartFeesPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->showHelpText()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getCount()I
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;->this$0:Lcom/squareup/ui/cart/AbstractCartFeesView;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/AbstractCartFeesView;->getPresenter()Lcom/squareup/ui/cart/AbstractCartFeesPresenter;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->getRowCount()I

    move-result v1

    invoke-virtual {v0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->showHelpText()Z

    move-result v0

    add-int/2addr v1, v0

    return v1
.end method

.method public getItem(I)Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;->this$0:Lcom/squareup/ui/cart/AbstractCartFeesView;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/AbstractCartFeesView;->getPresenter()Lcom/squareup/ui/cart/AbstractCartFeesPresenter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->getRow(I)Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 44
    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;->getItem(I)Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;->this$0:Lcom/squareup/ui/cart/AbstractCartFeesView;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/AbstractCartFeesView;->getPresenter()Lcom/squareup/ui/cart/AbstractCartFeesPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->showHelpText()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;->getCount()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    if-ne p1, v0, :cond_0

    return v1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 84
    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 85
    invoke-direct {p0, p2, p3}, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;->buildHelpTextRow(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 87
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;->buildFeeRow(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .line 76
    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;->getItemViewType(I)I

    move-result p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
