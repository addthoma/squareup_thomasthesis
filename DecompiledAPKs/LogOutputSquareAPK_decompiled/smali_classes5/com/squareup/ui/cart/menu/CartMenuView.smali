.class public Lcom/squareup/ui/cart/menu/CartMenuView;
.super Landroid/widget/ScrollView;
.source "CartMenuView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/menu/CartMenuView$Listener;
    }
.end annotation


# instance fields
.field private addCustomerRow:Landroid/widget/TextView;

.field private clearSaleRow:Landroid/widget/TextView;

.field private final dropDownRowHeight:I

.field private editTicketRow:Landroid/widget/TextView;

.field private mergeTicketRow:Landroid/widget/TextView;

.field private moveTicketRow:Landroid/widget/TextView;

.field presenter:Lcom/squareup/ui/cart/menu/CartMenuPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private printBillRow:Landroid/widget/TextView;

.field private reprintTicketRow:Landroid/widget/TextView;

.field private splitTicketRow:Landroid/widget/TextView;

.field private transferTicketRow:Landroid/widget/TextView;

.field private viewCustomerRow:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_drop_down_row_height:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->dropDownRowHeight:I

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuView;->isInEditMode()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/ui/cart/CartComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartComponent;

    invoke-interface {p1, p0}, Lcom/squareup/ui/cart/CartComponent;->inject(Lcom/squareup/ui/cart/menu/CartMenuView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 215
    sget v0, Lcom/squareup/orderentry/R$id;->cart_menu_add_customer:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->addCustomerRow:Landroid/widget/TextView;

    .line 216
    sget v0, Lcom/squareup/orderentry/R$id;->cart_menu_view_customer:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->viewCustomerRow:Landroid/widget/TextView;

    .line 217
    sget v0, Lcom/squareup/orderentry/R$id;->cart_menu_clear_new_items:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->clearSaleRow:Landroid/widget/TextView;

    .line 218
    sget v0, Lcom/squareup/orderentry/R$id;->cart_menu_edit_ticket:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->editTicketRow:Landroid/widget/TextView;

    .line 219
    sget v0, Lcom/squareup/orderentry/R$id;->cart_menu_print_bill:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->printBillRow:Landroid/widget/TextView;

    .line 220
    sget v0, Lcom/squareup/orderentry/R$id;->cart_menu_merge_ticket:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->mergeTicketRow:Landroid/widget/TextView;

    .line 221
    sget v0, Lcom/squareup/orderentry/R$id;->cart_menu_move_ticket:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->moveTicketRow:Landroid/widget/TextView;

    .line 222
    sget v0, Lcom/squareup/orderentry/R$id;->cart_menu_reprint_ticket:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->reprintTicketRow:Landroid/widget/TextView;

    .line 223
    sget v0, Lcom/squareup/orderentry/R$id;->cart_menu_split_ticket:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->splitTicketRow:Landroid/widget/TextView;

    .line 224
    sget v0, Lcom/squareup/orderentry/R$id;->cart_menu_transfer_ticket:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->transferTicketRow:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public getVisibleHeight()I
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 127
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuView;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 128
    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/menu/CartMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    add-int/lit8 v2, v2, 0x1

    .line 130
    iget v3, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->dropDownRowHeight:I

    add-int/2addr v1, v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v2, -0x1

    add-int/2addr v1, v2

    return v1
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->presenter:Lcom/squareup/ui/cart/menu/CartMenuPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->dropView(Ljava/lang/Object;)V

    .line 120
    invoke-super {p0}, Landroid/widget/ScrollView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 50
    invoke-super {p0}, Landroid/widget/ScrollView;->onFinishInflate()V

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/cart/menu/CartMenuView;->bindViews()V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->addCustomerRow:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/cart/menu/CartMenuView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/menu/CartMenuView$1;-><init>(Lcom/squareup/ui/cart/menu/CartMenuView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->viewCustomerRow:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/cart/menu/CartMenuView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/menu/CartMenuView$2;-><init>(Lcom/squareup/ui/cart/menu/CartMenuView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->clearSaleRow:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/cart/menu/CartMenuView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/menu/CartMenuView$3;-><init>(Lcom/squareup/ui/cart/menu/CartMenuView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->editTicketRow:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/cart/menu/CartMenuView$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/menu/CartMenuView$4;-><init>(Lcom/squareup/ui/cart/menu/CartMenuView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->printBillRow:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/cart/menu/CartMenuView$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/menu/CartMenuView$5;-><init>(Lcom/squareup/ui/cart/menu/CartMenuView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->mergeTicketRow:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/cart/menu/CartMenuView$6;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/menu/CartMenuView$6;-><init>(Lcom/squareup/ui/cart/menu/CartMenuView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->moveTicketRow:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/cart/menu/CartMenuView$7;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/menu/CartMenuView$7;-><init>(Lcom/squareup/ui/cart/menu/CartMenuView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->reprintTicketRow:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/cart/menu/CartMenuView$8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/menu/CartMenuView$8;-><init>(Lcom/squareup/ui/cart/menu/CartMenuView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->splitTicketRow:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/cart/menu/CartMenuView$9;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/menu/CartMenuView$9;-><init>(Lcom/squareup/ui/cart/menu/CartMenuView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->transferTicketRow:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/cart/menu/CartMenuView$10;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/menu/CartMenuView$10;-><init>(Lcom/squareup/ui/cart/menu/CartMenuView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->presenter:Lcom/squareup/ui/cart/menu/CartMenuPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method setAddCustomerVisible(Z)V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->addCustomerRow:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setClearSaleEnabled(Z)V
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->clearSaleRow:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method setClearSaleText(Ljava/lang/String;)V
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->clearSaleRow:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setEditTicketEnabled(Z)V
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->editTicketRow:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method setEditTicketVisible(Z)V
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->editTicketRow:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setMergeTicketEnabled(Z)V
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->mergeTicketRow:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method setMergeTicketVisible(Z)V
    .locals 2

    if-eqz p1, :cond_1

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->moveTicketRow:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "Cannot show both Merge Ticket and Move Ticket at the same time!"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->mergeTicketRow:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setMoveTicketEnabled(Z)V
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->moveTicketRow:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method setMoveTicketVisible(Z)V
    .locals 2

    if-eqz p1, :cond_1

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->mergeTicketRow:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "Cannot show both Move Ticket and Merge Ticket at the same time!"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->moveTicketRow:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setPrintBillEnabled(Z)V
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->printBillRow:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method setPrintBillVisible(Z)V
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->printBillRow:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setReprintTicketEnabled(Z)V
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->reprintTicketRow:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method setReprintTicketVisible(Z)V
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->reprintTicketRow:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setSplitTicketEnabled(Z)V
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->splitTicketRow:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method setSplitTicketVisible(Z)V
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->splitTicketRow:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setTransferTicketEnabled(Z)V
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->transferTicketRow:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method setTransferTicketVisible(Z)V
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->transferTicketRow:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setViewCustomerVisible(Z)V
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuView;->viewCustomerRow:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
