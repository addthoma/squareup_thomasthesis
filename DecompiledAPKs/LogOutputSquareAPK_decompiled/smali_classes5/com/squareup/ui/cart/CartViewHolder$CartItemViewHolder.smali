.class Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;
.super Lcom/squareup/ui/cart/CartViewHolder;
.source "CartViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CartItemViewHolder"
.end annotation


# instance fields
.field cartItem:Lcom/squareup/checkout/CartItem;

.field private final cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

.field cartRowItem:Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;

.field private final entryView:Lcom/squareup/ui/cart/CartEntryView;

.field private final features:Lcom/squareup/settings/server/Features;

.field recoveringSwipe:Z


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/Features;Landroid/view/View;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V
    .locals 0

    .line 74
    invoke-direct {p0, p2, p4}, Lcom/squareup/ui/cart/CartViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    const/4 p4, 0x0

    .line 70
    iput-boolean p4, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->recoveringSwipe:Z

    .line 75
    iput-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->features:Lcom/squareup/settings/server/Features;

    .line 76
    iput-object p3, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    .line 77
    check-cast p2, Lcom/squareup/ui/cart/CartEntryView;

    iput-object p2, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    return-void
.end method


# virtual methods
.method bind(Lcom/squareup/ui/cart/CartAdapterItem;)V
    .locals 8

    .line 81
    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;

    iput-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartRowItem:Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartRowItem:Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;

    iget-object p1, p1, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->cartItem:Lcom/squareup/checkout/CartItem;

    iput-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartItem:Lcom/squareup/checkout/CartItem;

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    new-instance v0, Lcom/squareup/ui/cart/-$$Lambda$CartViewHolder$CartItemViewHolder$lhg4utogB-Gq8__eDUYJdntt3ms;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/-$$Lambda$CartViewHolder$CartItemViewHolder$lhg4utogB-Gq8__eDUYJdntt3ms;-><init>(Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;)V

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/cart/CartEntryView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->isClickable()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/cart/CartEntryView;->setClickable(Z)V

    .line 89
    iget-object v1, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    iget-object v2, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartItem:Lcom/squareup/checkout/CartItem;

    iget-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartRowItem:Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;

    iget-object v4, p1, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->diningOption:Ljava/lang/String;

    iget-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartItem:Lcom/squareup/checkout/CartItem;

    .line 91
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result p1

    xor-int/lit8 v6, p1, 0x1

    iget-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_SEATING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v7

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 90
    invoke-interface/range {v1 .. v7}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->orderItem(Lcom/squareup/checkout/CartItem;ZLjava/lang/String;ZZZ)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object p1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartEntryView;->present(Lcom/squareup/ui/cart/CartEntryViewModel;)V

    return-void
.end method

.method getEntryView()Lcom/squareup/ui/cart/CartEntryView;
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    return-object v0
.end method

.method isClickable()Z
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method isCustomAmount()Z
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->isCustomItem()Z

    move-result v0

    return v0
.end method

.method isDiscountChanged()Z
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartRowItem:Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;

    iget-boolean v0, v0, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->isDiscountEvent:Z

    return v0
.end method

.method isKeypadCommitted()Z
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartRowItem:Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;

    iget-boolean v0, v0, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->isKeypadCommittedEvent:Z

    return v0
.end method

.method public synthetic lambda$bind$0$CartViewHolder$CartItemViewHolder()V
    .locals 4

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartRowItem:Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;

    iget-object v1, v1, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->cartItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->getAdapterPosition()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartRowItem:Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;

    iget v3, v3, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->orderItemIndex:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->onItemClicked(Lcom/squareup/checkout/CartItem;II)V

    return-void
.end method

.method shouldAllowSwipeToDelete()Z
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->VOID_COMP:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartItem:Lcom/squareup/checkout/CartItem;

    iget-boolean v0, v0, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;->cartItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method
