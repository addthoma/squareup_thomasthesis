.class public Lcom/squareup/ui/cart/DiningOptionViewPager;
.super Landroidx/viewpager/widget/ViewPager;
.source "DiningOptionViewPager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;
    }
.end annotation


# instance fields
.field private final borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

.field presenter:Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 40
    invoke-direct {p0, p1, p2}, Landroidx/viewpager/widget/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    const-class p2, Lcom/squareup/ui/cart/CartComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/cart/CartComponent;

    invoke-interface {p2, p0}, Lcom/squareup/ui/cart/CartComponent;->inject(Lcom/squareup/ui/cart/DiningOptionViewPager;)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/cart/DiningOptionViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 44
    new-instance v0, Lcom/squareup/marin/widgets/BorderPainter;

    sget v1, Lcom/squareup/marin/R$dimen;->marin_divider_width_1px:I

    invoke-direct {v0, p0, v1}, Lcom/squareup/marin/widgets/BorderPainter;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPager;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPager;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/BorderPainter;->addBorder(I)V

    .line 46
    sget v0, Lcom/squareup/utilities/ui/R$bool;->sq_is_tablet:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/squareup/util/Views;->isPortrait(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 47
    iget-object p1, p0, Lcom/squareup/ui/cart/DiningOptionViewPager;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    sget v0, Lcom/squareup/marin/R$dimen;->marin_cart_item_gutter_half:I

    .line 48
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 47
    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/BorderPainter;->setHorizontalInsets(I)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$installAdapter$0(Landroid/view/View;F)V
    .locals 4

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, -0x40800000    # -1.0f

    cmpg-float v2, p1, v2

    if-lez v2, :cond_1

    cmpl-float v2, p1, v1

    if-ltz v2, :cond_0

    goto :goto_0

    .line 81
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    neg-float v3, p1

    mul-float v2, v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    invoke-virtual {p0, v2}, Landroid/view/View;->setTranslationX(F)V

    const v2, 0x3f8ccccd    # 1.1f

    mul-float p1, p1, v2

    .line 82
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    sub-float/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    invoke-virtual {p0, p1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1

    .line 76
    :cond_1
    :goto_0
    invoke-virtual {p0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 77
    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationX(F)V

    :goto_1
    return-void
.end method


# virtual methods
.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 67
    invoke-super {p0, p1}, Landroidx/viewpager/widget/ViewPager;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPager;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/BorderPainter;->drawBorders(Landroid/graphics/Canvas;)V

    return-void
.end method

.method installAdapter()V
    .locals 2

    .line 72
    new-instance v0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;-><init>(Lcom/squareup/ui/cart/DiningOptionViewPager;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/DiningOptionViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 73
    sget-object v0, Lcom/squareup/ui/cart/-$$Lambda$DiningOptionViewPager$c28vnDYXw-iSUndj73J05JtJy4o;->INSTANCE:Lcom/squareup/ui/cart/-$$Lambda$DiningOptionViewPager$c28vnDYXw-iSUndj73J05JtJy4o;

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/squareup/ui/cart/DiningOptionViewPager;->setPageTransformer(ZLandroidx/viewpager/widget/ViewPager$PageTransformer;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPager;->presenter:Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->currentDiningOptionIndex()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/DiningOptionViewPager;->setCurrentItem(I)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPager;->presenter:Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->dropView(Ljava/lang/Object;)V

    .line 59
    invoke-super {p0}, Landroidx/viewpager/widget/ViewPager;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 53
    invoke-super {p0}, Landroidx/viewpager/widget/ViewPager;->onFinishInflate()V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPager;->presenter:Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V
    .locals 1

    .line 63
    new-instance v0, Lcom/squareup/ui/main/FixScrollOffsetPageChangeListener;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/main/FixScrollOffsetPageChangeListener;-><init>(Landroidx/viewpager/widget/ViewPager;Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    invoke-super {p0, v0}, Landroidx/viewpager/widget/ViewPager;->setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    return-void
.end method

.method updateAdapter()V
    .locals 2

    .line 90
    invoke-virtual {p0}, Lcom/squareup/ui/cart/DiningOptionViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/squareup/ui/cart/DiningOptionViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/viewpager/widget/PagerAdapter;->notifyDataSetChanged()V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPager;->presenter:Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->currentDiningOptionIndex()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/cart/DiningOptionViewPager;->setCurrentItem(IZ)V

    goto :goto_0

    .line 95
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/cart/DiningOptionViewPager;->installAdapter()V

    :goto_0
    return-void
.end method
