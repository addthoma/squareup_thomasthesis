.class public Lcom/squareup/ui/cart/DiscountFormatter;
.super Ljava/lang/Object;
.source "DiscountFormatter.java"

# interfaces
.implements Lcom/squareup/text/Formatter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/text/Formatter<",
        "Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;",
        ">;"
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final negatingPercentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/cart/DiscountFormatter;->negatingPercentageFormatter:Lcom/squareup/text/Formatter;

    .line 21
    iput-object p2, p0, Lcom/squareup/ui/cart/DiscountFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method public format(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;)Ljava/lang/CharSequence;
    .locals 3

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 26
    :cond_0
    iget-object v0, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->fee:Lcom/squareup/checkout/Adjustment;

    iget-object v0, v0, Lcom/squareup/checkout/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/cart/DiscountFormatter;->negatingPercentageFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->fee:Lcom/squareup/checkout/Adjustment;

    iget-object p1, p1, Lcom/squareup/checkout/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    .line 27
    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/cart/DiscountFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-wide v1, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->appliedAmount:J

    iget-object p1, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->fee:Lcom/squareup/checkout/Adjustment;

    iget-object p1, p1, Lcom/squareup/checkout/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 28
    invoke-static {v1, v2, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic format(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/DiscountFormatter;->format(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method
