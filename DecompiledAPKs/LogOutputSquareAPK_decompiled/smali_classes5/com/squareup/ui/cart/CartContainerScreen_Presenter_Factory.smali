.class public final Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "CartContainerScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/cart/CartContainerScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;"
        }
    .end annotation
.end field

.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final barcodeScannerTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final cartDropDownPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final phoneCartScreenFinisherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartScreenFinisher$Phone;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final sellerScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final topScreenCheckerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionInteractionsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartScreenFinisher$Phone;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;)V"
        }
    .end annotation

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p2, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p3, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p4, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p5, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p6, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p7, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->cartDropDownPresenterProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p8, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->phoneCartScreenFinisherProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p9, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->sellerScopeRunnerProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p10, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p11, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->topScreenCheckerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartScreenFinisher$Phone;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;)",
            "Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;"
        }
    .end annotation

    .line 86
    new-instance v12, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;Lcom/squareup/ui/cart/CartScreenFinisher$Phone;Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/ui/main/TopScreenChecker;)Lcom/squareup/ui/cart/CartContainerScreen$Presenter;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            "Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;",
            "Lcom/squareup/ui/cart/CartScreenFinisher$Phone;",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ")",
            "Lcom/squareup/ui/cart/CartContainerScreen$Presenter;"
        }
    .end annotation

    .line 95
    new-instance v12, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;Lcom/squareup/ui/cart/CartScreenFinisher$Phone;Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/ui/main/TopScreenChecker;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/ui/cart/CartContainerScreen$Presenter;
    .locals 12

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/log/cart/TransactionInteractionsLogger;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->cartDropDownPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->phoneCartScreenFinisherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/cart/CartScreenFinisher$Phone;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->sellerScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/seller/SellerScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->topScreenCheckerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/ui/main/TopScreenChecker;

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->newInstance(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;Lcom/squareup/ui/cart/CartScreenFinisher$Phone;Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/ui/main/TopScreenChecker;)Lcom/squareup/ui/cart/CartContainerScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartContainerScreen_Presenter_Factory;->get()Lcom/squareup/ui/cart/CartContainerScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
