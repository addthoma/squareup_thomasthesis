.class public final Lcom/squareup/ui/cart/CartTaxesScreen;
.super Lcom/squareup/ui/seller/InSellerScope;
.source "CartTaxesScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/cart/CartTaxesScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/CartTaxesScreen$Component;,
        Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/cart/CartTaxesScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/cart/CartTaxesScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/ui/cart/CartTaxesScreen;

    invoke-direct {v0}, Lcom/squareup/ui/cart/CartTaxesScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/cart/CartTaxesScreen;->INSTANCE:Lcom/squareup/ui/cart/CartTaxesScreen;

    .line 58
    sget-object v0, Lcom/squareup/ui/cart/CartTaxesScreen;->INSTANCE:Lcom/squareup/ui/cart/CartTaxesScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/cart/CartTaxesScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/seller/InSellerScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_TAXES:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 48
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 44
    sget v0, Lcom/squareup/orderentry/R$layout;->cart_taxes_view:I

    return v0
.end method
