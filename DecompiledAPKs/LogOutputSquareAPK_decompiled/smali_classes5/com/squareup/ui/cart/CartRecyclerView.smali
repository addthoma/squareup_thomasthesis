.class public Lcom/squareup/ui/cart/CartRecyclerView;
.super Landroidx/coordinatorlayout/widget/CoordinatorLayout;
.source "CartRecyclerView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/CartRecyclerView$CartItemAnimator;
    }
.end annotation


# instance fields
.field private cartAdapter:Lcom/squareup/ui/cart/CartAdapter;

.field cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private cartSwipeController:Lcom/squareup/ui/cart/CartSwipeController;

.field private diningOptions:Landroid/view/View;

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final isPortrait:Z

.field presenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2}, Landroidx/coordinatorlayout/widget/CoordinatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    const-class p2, Lcom/squareup/ui/cart/CartComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/cart/CartComponent;

    invoke-interface {p2, p0}, Lcom/squareup/ui/cart/CartComponent;->inject(Lcom/squareup/ui/cart/CartRecyclerView;)V

    .line 38
    invoke-static {p1}, Lcom/squareup/util/Views;->isPortrait(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/cart/CartRecyclerView;->isPortrait:Z

    return-void
.end method

.method private getLastItemIndex()I
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->cartAdapter:Lcom/squareup/ui/cart/CartAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartAdapter;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 127
    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerView;->presenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->getFooterRowCount()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private getLastRowIndex()I
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->cartAdapter:Lcom/squareup/ui/cart/CartAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartAdapter;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method


# virtual methods
.method getCartItem(I)Lcom/squareup/ui/cart/CartAdapterItem;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->cartAdapter:Lcom/squareup/ui/cart/CartAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartAdapter;->getCartItem(I)Lcom/squareup/ui/cart/CartAdapterItem;

    move-result-object p1

    return-object p1
.end method

.method isPortrait()Z
    .locals 1

    .line 75
    iget-boolean v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->isPortrait:Z

    return v0
.end method

.method public onAttachedToWindow()V
    .locals 1

    .line 58
    invoke-super {p0}, Landroidx/coordinatorlayout/widget/CoordinatorLayout;->onAttachedToWindow()V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->presenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->presenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->dropView(Ljava/lang/Object;)V

    .line 64
    invoke-super {p0}, Landroidx/coordinatorlayout/widget/CoordinatorLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 42
    invoke-super {p0}, Landroidx/coordinatorlayout/widget/CoordinatorLayout;->onFinishInflate()V

    .line 43
    new-instance v0, Lcom/squareup/ui/cart/CartAdapter;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerView;->features:Lcom/squareup/settings/server/Features;

    iget-object v2, p0, Lcom/squareup/ui/cart/CartRecyclerView;->presenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    iget-object v3, p0, Lcom/squareup/ui/cart/CartRecyclerView;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/cart/CartAdapter;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    iput-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->cartAdapter:Lcom/squareup/ui/cart/CartAdapter;

    .line 44
    sget v0, Lcom/squareup/orderentry/R$id;->cart_recycler_view:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/CartRecyclerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 45
    sget v0, Lcom/squareup/orderentry/R$id;->dining_option_app_bar:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/CartRecyclerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->diningOptions:Landroid/view/View;

    .line 47
    new-instance v0, Lcom/squareup/ui/cart/CartSwipeController;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v2, p0, Lcom/squareup/ui/cart/CartRecyclerView;->presenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/cart/CartSwipeController;-><init>(Landroidx/recyclerview/widget/RecyclerView;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;)V

    iput-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->cartSwipeController:Lcom/squareup/ui/cart/CartSwipeController;

    .line 48
    new-instance v0, Landroidx/recyclerview/widget/ItemTouchHelper;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerView;->cartSwipeController:Lcom/squareup/ui/cart/CartSwipeController;

    invoke-direct {v0, v1}, Landroidx/recyclerview/widget/ItemTouchHelper;-><init>(Landroidx/recyclerview/widget/ItemTouchHelper$Callback;)V

    .line 49
    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/ItemTouchHelper;->attachToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerView;->cartAdapter:Lcom/squareup/ui/cart/CartAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Lcom/squareup/ui/cart/CartRecyclerView$CartItemAnimator;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/cart/CartRecyclerView$CartItemAnimator;-><init>(Lcom/squareup/ui/cart/CartRecyclerView;Lcom/squareup/ui/cart/CartRecyclerView$1;)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->presenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->showDiningOption()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/CartRecyclerView;->updateDiningOptionVisibility(Z)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 68
    invoke-super {p0, p1, p2, p3, p4}, Landroidx/coordinatorlayout/widget/CoordinatorLayout;->onSizeChanged(IIII)V

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerView;->cartAdapter:Lcom/squareup/ui/cart/CartAdapter;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method recoverPreviousSwipe(I)V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->cartSwipeController:Lcom/squareup/ui/cart/CartSwipeController;

    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartSwipeController;->recoverPreviousSwipe(I)V

    :cond_0
    return-void
.end method

.method resetDiningOptionsTranslationY()V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->diningOptions:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    return-void
.end method

.method setCartItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;)V"
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->cartAdapter:Lcom/squareup/ui/cart/CartAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartAdapter;->setCartItems(Ljava/util/List;)V

    return-void
.end method

.method smoothScrollToLastItem()V
    .locals 1

    .line 108
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerView;->getLastItemIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/CartRecyclerView;->smoothScrollToPosition(I)V

    return-void
.end method

.method smoothScrollToLastRow()V
    .locals 1

    .line 104
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerView;->getLastRowIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/CartRecyclerView;->smoothScrollToPosition(I)V

    return-void
.end method

.method smoothScrollToPosition(I)V
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollToPosition(I)V

    return-void
.end method

.method snapToLastItem()V
    .locals 2

    .line 99
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerView;->getLastItemIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/CartRecyclerView;->smoothScrollToPosition(I)V

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerView;->getLastItemIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    return-void
.end method

.method snapToLastRow()V
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerView;->getLastRowIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    return-void
.end method

.method updateDiningOptionVisibility(Z)V
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView;->diningOptions:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
