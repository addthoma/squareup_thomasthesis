.class Lcom/squareup/ui/cart/CartViewHolder$TicketNoteViewHolder;
.super Lcom/squareup/ui/cart/CartViewHolder;
.source "CartViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TicketNoteViewHolder"
.end annotation


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/cart/CartViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    return-void
.end method


# virtual methods
.method bind(Lcom/squareup/ui/cart/CartAdapterItem;)V
    .locals 1

    .line 49
    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem$TicketNoteRow;

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$TicketNoteViewHolder;->itemView:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    .line 52
    iget-object p1, p1, Lcom/squareup/ui/cart/CartAdapterItem$TicketNoteRow;->note:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    invoke-virtual {v0}, Landroid/widget/TextView;->requestLayout()V

    return-void
.end method
