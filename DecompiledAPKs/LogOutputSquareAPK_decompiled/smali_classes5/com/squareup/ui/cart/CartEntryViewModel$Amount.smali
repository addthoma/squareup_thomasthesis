.class public final Lcom/squareup/ui/cart/CartEntryViewModel$Amount;
.super Ljava/lang/Object;
.source "CartEntryViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartEntryViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Amount"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0017\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B9\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\tH\u00c6\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003JI\u0010\u001f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\u0005H\u00c6\u0001J\u0013\u0010 \u001a\u00020\u00072\u0008\u0010!\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\"\u001a\u00020#H\u00d6\u0001J\t\u0010$\u001a\u00020%H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0013\u0010\u000c\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/ui/cart/CartEntryViewModel$Amount;",
        "",
        "label",
        "",
        "color",
        "Landroid/content/res/ColorStateList;",
        "enabled",
        "",
        "weight",
        "Lcom/squareup/marketfont/MarketFont$Weight;",
        "glyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "glyphColor",
        "(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/glyph/GlyphTypeface$Glyph;Landroid/content/res/ColorStateList;)V",
        "getColor",
        "()Landroid/content/res/ColorStateList;",
        "getEnabled",
        "()Z",
        "getGlyph",
        "()Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "getGlyphColor",
        "getLabel",
        "()Ljava/lang/CharSequence;",
        "getWeight",
        "()Lcom/squareup/marketfont/MarketFont$Weight;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final color:Landroid/content/res/ColorStateList;

.field private final enabled:Z

.field private final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private final glyphColor:Landroid/content/res/ColorStateList;

.field private final label:Ljava/lang/CharSequence;

.field private final weight:Lcom/squareup/marketfont/MarketFont$Weight;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/glyph/GlyphTypeface$Glyph;Landroid/content/res/ColorStateList;)V
    .locals 1

    const-string v0, "label"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "color"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "weight"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->label:Ljava/lang/CharSequence;

    iput-object p2, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->color:Landroid/content/res/ColorStateList;

    iput-boolean p3, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->enabled:Z

    iput-object p4, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    iput-object p5, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput-object p6, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->glyphColor:Landroid/content/res/ColorStateList;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/glyph/GlyphTypeface$Glyph;Landroid/content/res/ColorStateList;ILjava/lang/Object;)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->label:Ljava/lang/CharSequence;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->color:Landroid/content/res/ColorStateList;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->enabled:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->glyphColor:Landroid/content/res/ColorStateList;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->copy(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/glyph/GlyphTypeface$Glyph;Landroid/content/res/ColorStateList;)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->label:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component2()Landroid/content/res/ColorStateList;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->color:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->enabled:Z

    return v0
.end method

.method public final component4()Lcom/squareup/marketfont/MarketFont$Weight;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    return-object v0
.end method

.method public final component5()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public final component6()Landroid/content/res/ColorStateList;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->glyphColor:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public final copy(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/glyph/GlyphTypeface$Glyph;Landroid/content/res/ColorStateList;)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;
    .locals 8

    const-string v0, "label"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "color"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "weight"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;-><init>(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/glyph/GlyphTypeface$Glyph;Landroid/content/res/ColorStateList;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->label:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->label:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->color:Landroid/content/res/ColorStateList;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->color:Landroid/content/res/ColorStateList;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->enabled:Z

    iget-boolean v1, p1, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->enabled:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->glyphColor:Landroid/content/res/ColorStateList;

    iget-object p1, p1, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->glyphColor:Landroid/content/res/ColorStateList;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getColor()Landroid/content/res/ColorStateList;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->color:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public final getEnabled()Z
    .locals 1

    .line 37
    iget-boolean v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->enabled:Z

    return v0
.end method

.method public final getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public final getGlyphColor()Landroid/content/res/ColorStateList;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->glyphColor:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public final getLabel()Ljava/lang/CharSequence;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->label:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getWeight()Lcom/squareup/marketfont/MarketFont$Weight;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->label:Ljava/lang/CharSequence;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->color:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->enabled:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->glyphColor:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Amount(label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->label:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->color:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->enabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", weight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", glyph="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", glyphColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;->glyphColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
