.class public abstract Lcom/squareup/ui/SquareActivity;
.super Landroidx/appcompat/app/AppCompatActivity;
.source "SquareActivity.java"

# interfaces
.implements Lcom/squareup/CoreIds;
.implements Lcom/squareup/dialog/CanDisableClicks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/SquareActivity$ParentAppComponent;,
        Lcom/squareup/ui/SquareActivity$ParentLoggedInComponent;,
        Lcom/squareup/ui/SquareActivity$Preconditions;
    }
.end annotation


# static fields
.field protected static final EXTRA_EXIT_APP:Ljava/lang/String; = "com.squareup.EXIT_APP"

.field private static final KEY_FINISHING:Ljava/lang/String;


# instance fields
.field private activityDestroyed:Z

.field activityResultHandler:Lcom/squareup/ui/ActivityResultHandler;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private activityScope:Lmortar/MortarScope;

.field analytics:Lcom/squareup/analytics/Analytics;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field cardReaderPauseAndResumer:Lcom/squareup/cardreader/CardReaderPauseAndResumer;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private configurationChangeIncoming:Z

.field configurationChangeMonitor:Lcom/squareup/util/AndroidConfigurationChangeMonitor;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field contentViewInitializer:Lcom/squareup/development/drawer/ContentViewInitializer;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final dialogDestroyer:Lcom/squareup/ui/DialogDestroyer;

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field focusedActivityScanner:Lcom/squareup/radiography/FocusedActivityScanner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private inputEnabled:Z

.field internetStatusMonitor:Lcom/squareup/internet/InternetStatusMonitor;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mediaButtonDisabler:Lcom/squareup/ui/MediaButtonDisabler;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field minesweeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field protected ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

.field persistentBundleManager:Lcom/squareup/persistentbundle/PersistentBundleManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final preconditions:Lcom/squareup/ui/SquareActivity$Preconditions;

.field private scopeCleaner:Lcom/squareup/ui/ActivityScopeCleaner;

.field private scopeName:Ljava/lang/String;

.field private useDefaultAnim:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/squareup/ui/SquareActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".finishing"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/SquareActivity;->KEY_FINISHING:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/ui/SquareActivity$Preconditions;)V
    .locals 1

    .line 150
    invoke-direct {p0}, Landroidx/appcompat/app/AppCompatActivity;-><init>()V

    const/4 v0, 0x0

    .line 130
    iput-boolean v0, p0, Lcom/squareup/ui/SquareActivity;->useDefaultAnim:Z

    .line 140
    new-instance v0, Lcom/squareup/ui/DialogDestroyer;

    invoke-direct {v0}, Lcom/squareup/ui/DialogDestroyer;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/SquareActivity;->dialogDestroyer:Lcom/squareup/ui/DialogDestroyer;

    .line 151
    iput-object p1, p0, Lcom/squareup/ui/SquareActivity;->preconditions:Lcom/squareup/ui/SquareActivity$Preconditions;

    return-void
.end method

.method private endMortarScope()V
    .locals 1

    .line 654
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    if-eqz v0, :cond_1

    .line 657
    invoke-virtual {v0}, Lmortar/MortarScope;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 658
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    invoke-virtual {v0}, Lmortar/MortarScope;->destroy()V

    :cond_0
    const/4 v0, 0x0

    .line 660
    iput-object v0, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    :cond_1
    return-void
.end method

.method private finishIfNotAuthorized()Z
    .locals 4

    .line 413
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->preconditions:Lcom/squareup/ui/SquareActivity$Preconditions;

    iget-boolean v0, v0, Lcom/squareup/ui/SquareActivity$Preconditions;->requiresAuth:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/squareup/AppDelegate$Locator;->getAppDelegate()Lcom/squareup/AppDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/AppDelegate;->isLoggedIn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 414
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->FINISH:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->getOhSnapMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ": User is not logged in"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 418
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->finish()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private getOhSnapMessage()Ljava/lang/String;
    .locals 4

    .line 488
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 489
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->getTaskId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->getScopeNameOrNull()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const-string v2, "%s [%X] task=%d scope=%s"

    .line 488
    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSavedScopeName()Ljava/lang/String;
    .locals 1

    .line 364
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->scopeName:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->getLastCustomNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method private getScopeNameOrNull()Ljava/lang/String;
    .locals 1

    .line 359
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private static interceptKey(I)Z
    .locals 1

    const/16 v0, 0x1b

    if-eq p0, v0, :cond_0

    const/16 v0, 0x4f

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_0

    packed-switch p0, :pswitch_data_1

    const/4 p0, 0x0

    return p0

    :cond_0
    :pswitch_0
    const/4 p0, 0x1

    return p0

    :pswitch_data_0
    .packed-switch 0x55
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private replacePreviewWindowBackground()V
    .locals 2

    .line 273
    sget-object v0, Lcom/squareup/widgets/R$styleable;->WindowBackground:[I

    .line 274
    invoke-virtual {p0, v0}, Lcom/squareup/ui/SquareActivity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 275
    sget v1, Lcom/squareup/widgets/R$styleable;->WindowBackground_marinWindowBackground:I

    .line 276
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 278
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 279
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private setStartActivityTransition(Landroid/content/Intent;)V
    .locals 3

    .line 584
    invoke-static {p1}, Lcom/squareup/util/Intents;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const-string v2, "com.squareup"

    .line 585
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "com.squareup.EXIT_APP"

    .line 586
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 587
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->useDefaultExitAnimation()V

    goto :goto_0

    .line 589
    :cond_0
    sget p1, Lcom/squareup/widgets/R$anim;->slide_in_from_right:I

    sget v0, Lcom/squareup/widgets/R$anim;->slide_out_to_left:I

    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/SquareActivity;->overridePendingTransition(II)V

    goto :goto_0

    .line 593
    :cond_1
    invoke-virtual {p0, v1, v1}, Lcom/squareup/ui/SquareActivity;->overridePendingTransition(II)V

    :goto_0
    return-void
.end method

.method private startMortarScope(Landroid/os/Bundle;)V
    .locals 8

    .line 283
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v0

    .line 287
    iget-object v1, p0, Lcom/squareup/ui/SquareActivity;->preconditions:Lcom/squareup/ui/SquareActivity$Preconditions;

    iget-boolean v1, v1, Lcom/squareup/ui/SquareActivity$Preconditions;->requiresAuth:Z

    if-eqz v1, :cond_0

    .line 288
    invoke-static {}, Lcom/squareup/MortarLoggedIn;->getLoggedInScope()Lmortar/MortarScope;

    move-result-object v1

    .line 289
    const-class v2, Lcom/squareup/ui/SquareActivity$ParentLoggedInComponent;

    invoke-static {v1, v2}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 292
    :cond_0
    const-class v1, Lcom/squareup/ui/SquareActivity$ParentAppComponent;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    move-object v1, v0

    .line 295
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->getSavedScopeName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/squareup/ui/SquareActivity;->scopeName:Ljava/lang/String;

    if-nez p1, :cond_2

    .line 299
    iget-object v3, p0, Lcom/squareup/ui/SquareActivity;->scopeName:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 300
    invoke-virtual {v1, v3}, Lmortar/MortarScope;->findChild(Ljava/lang/String;)Lmortar/MortarScope;

    move-result-object v3

    iput-object v3, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    .line 301
    iget-object v3, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    .line 302
    iget-object v3, p0, Lcom/squareup/ui/SquareActivity;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v5, Lcom/squareup/log/OhSnapLogger$EventType;->API:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "found \'old\' MortarScope "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/squareup/ui/SquareActivity;->scopeName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " when starting a new activity with a null bundle.  Destroying it."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 304
    iget-object v3, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    invoke-virtual {v3}, Lmortar/MortarScope;->destroy()V

    .line 305
    iput-object v4, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    .line 307
    :cond_1
    iput-object v4, p0, Lcom/squareup/ui/SquareActivity;->scopeName:Ljava/lang/String;

    .line 311
    :cond_2
    iget-object v3, p0, Lcom/squareup/ui/SquareActivity;->scopeName:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 312
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/squareup/ui/SquareActivity;->scopeName:Ljava/lang/String;

    .line 314
    :cond_3
    iget-object v3, p0, Lcom/squareup/ui/SquareActivity;->scopeName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lmortar/MortarScope;->findChild(Ljava/lang/String;)Lmortar/MortarScope;

    move-result-object v3

    iput-object v3, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    .line 315
    iget-object v3, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    if-nez v3, :cond_5

    .line 316
    const-class v3, Lcom/squareup/ui/SquareActivity$ParentAppComponent;

    invoke-static {v0, v3}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/SquareActivity$ParentAppComponent;

    .line 318
    invoke-interface {v0}, Lcom/squareup/ui/SquareActivity$ParentAppComponent;->componentByActivity()Ljava/util/Map;

    move-result-object v0

    .line 319
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-eqz v0, :cond_4

    .line 328
    invoke-static {v2, v0}, Lcom/squareup/dagger/Components;->createChildComponent(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 330
    invoke-virtual {v1}, Lmortar/MortarScope;->buildChild()Lmortar/MortarScope$Builder;

    move-result-object v1

    .line 331
    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/SquareActivity;->buildActivityScope(Ljava/lang/Object;Lmortar/MortarScope$Builder;)Lmortar/MortarScope$Builder;

    move-result-object v1

    .line 332
    iget-object v2, p0, Lcom/squareup/ui/SquareActivity;->scopeName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lmortar/MortarScope$Builder;->build(Ljava/lang/String;)Lmortar/MortarScope;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    .line 333
    iget-object v1, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    invoke-static {v1, v0}, Lcom/squareup/leakcanary/ScopedObjectWatcher;->watchForLeaks(Lmortar/MortarScope;Ljava/lang/Object;)V

    goto :goto_1

    .line 322
    :cond_4
    new-instance p1, Ljava/lang/AssertionError;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Could not find "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " in the component map multibinding. Make sure to add it to ActivityComponentModule or DevActivityComponentModule."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 335
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/SquareActivity;->inject(Lmortar/MortarScope;)V

    .line 336
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    invoke-static {v0}, Lmortar/bundler/BundleServiceRunner;->getBundleServiceRunner(Lmortar/MortarScope;)Lmortar/bundler/BundleServiceRunner;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmortar/bundler/BundleServiceRunner;->onCreate(Landroid/os/Bundle;)V

    .line 339
    iget-object p1, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->scopeCleaner:Lcom/squareup/ui/ActivityScopeCleaner;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method


# virtual methods
.method protected buildActivityScope(Ljava/lang/Object;Lmortar/MortarScope$Builder;)Lmortar/MortarScope$Builder;
    .locals 1

    .line 345
    invoke-static {p2, p1}, Lcom/squareup/dagger/Components;->addAsScopeService(Lmortar/MortarScope$Builder;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    .line 346
    sget-object p1, Lmortar/bundler/BundleServiceRunner;->SERVICE_NAME:Ljava/lang/String;

    new-instance v0, Lmortar/bundler/BundleServiceRunner;

    invoke-direct {v0}, Lmortar/bundler/BundleServiceRunner;-><init>()V

    .line 347
    invoke-virtual {p2, p1, v0}, Lmortar/MortarScope$Builder;->withService(Ljava/lang/String;Lmortar/Scoped;)Lmortar/MortarScope$Builder;

    move-result-object p1

    return-object p1
.end method

.method public disableClicks()V
    .locals 1

    const/4 v0, 0x0

    .line 426
    iput-boolean v0, p0, Lcom/squareup/ui/SquareActivity;->inputEnabled:Z

    .line 429
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    invoke-static {}, Lcom/squareup/dialog/GlassDialogUtil;->createCancelEvent()Landroid/view/MotionEvent;

    move-result-object v0

    .line 431
    invoke-super {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 432
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 441
    iget-boolean v0, p0, Lcom/squareup/ui/SquareActivity;->inputEnabled:Z

    if-eqz v0, :cond_1

    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method protected doBackPressed()V
    .locals 0

    .line 617
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onBackPressed()V

    return-void
.end method

.method protected doCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 354
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/Window;->setFormat(I)V

    return-void
.end method

.method protected doSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public enableClicks()V
    .locals 1

    const/4 v0, 0x1

    .line 437
    iput-boolean v0, p0, Lcom/squareup/ui/SquareActivity;->inputEnabled:Z

    return-void
.end method

.method public findById(I)Landroid/view/View;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .line 202
    invoke-virtual {p0, p1}, Lcom/squareup/ui/SquareActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public finish()V
    .locals 3

    .line 557
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->FINISH:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->getOhSnapMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 558
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->finish()V

    .line 559
    iget-boolean v0, p0, Lcom/squareup/ui/SquareActivity;->useDefaultAnim:Z

    if-nez v0, :cond_0

    .line 560
    sget v0, Lcom/squareup/widgets/R$anim;->slide_in_from_left:I

    sget v1, Lcom/squareup/widgets/R$anim;->slide_out_to_right:I

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/SquareActivity;->overridePendingTransition(II)V

    :cond_0
    return-void
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .line 190
    invoke-static {}, Lcom/squareup/mortar/AppContextWrapper;->appContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 1

    .line 186
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 3

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lmortar/MortarScope;->hasService(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 156
    iget-boolean v0, p0, Lcom/squareup/ui/SquareActivity;->activityDestroyed:Z

    if-nez v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    invoke-virtual {v0, p1}, Lmortar/MortarScope;->getService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 157
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Should not ask for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " after activity has been destroyed."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :cond_1
    invoke-static {p1}, Lcom/squareup/ui/DialogDestroyer;->isSystemService(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164
    iget-object p1, p0, Lcom/squareup/ui/SquareActivity;->dialogDestroyer:Lcom/squareup/ui/DialogDestroyer;

    return-object p1

    :cond_2
    const-string v0, "audio"

    .line 170
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 173
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 176
    :cond_3
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method protected abstract inject(Lmortar/MortarScope;)V
.end method

.method public isRunning()Z
    .locals 1

    .line 613
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->cardReaderPauseAndResumer:Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->isRunning()Z

    move-result v0

    return v0
.end method

.method public maybeRestoreFromPersistentBundle(Landroid/os/Bundle;)V
    .locals 1

    .line 684
    invoke-static {p1}, Lcom/squareup/persistentbundle/PersistentBundleManager;->canRestoreFromPersistentBundle(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 687
    const-class v0, Lcom/squareup/ui/SquareActivity$ParentAppComponent;

    invoke-static {p0, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/SquareActivity$ParentAppComponent;

    .line 688
    invoke-interface {v0}, Lcom/squareup/ui/SquareActivity$ParentAppComponent;->persistentBundleManager()Lcom/squareup/persistentbundle/PersistentBundleManager;

    move-result-object v0

    .line 689
    invoke-virtual {v0, p1}, Lcom/squareup/persistentbundle/PersistentBundleManager;->restoreFromPersistentBundle(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public maybeSaveToPersistentBundle(Landroid/os/Bundle;)V
    .locals 2

    .line 699
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PERSISTENT_BUNDLE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 700
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->persistentBundleManager:Lcom/squareup/persistentbundle/PersistentBundleManager;

    invoke-virtual {v0, p1}, Lcom/squareup/persistentbundle/PersistentBundleManager;->saveToPersistentBundle(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 571
    invoke-super {p0, p1, p2, p3}, Landroidx/appcompat/app/AppCompatActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 572
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->activityResultHandler:Lcom/squareup/ui/ActivityResultHandler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/ui/ActivityResultHandler;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .line 607
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->attemptPerform(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->doBackPressed()V

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 268
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 269
    iget-object p1, p0, Lcom/squareup/ui/SquareActivity;->configurationChangeMonitor:Lcom/squareup/util/AndroidConfigurationChangeMonitor;

    invoke-interface {p1}, Lcom/squareup/util/AndroidConfigurationChangeMonitor;->fireOnConfigurationMaybeChanged()V

    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .line 228
    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->replacePreviewWindowBackground()V

    if-eqz p1, :cond_0

    .line 231
    invoke-virtual {p0, p1}, Lcom/squareup/ui/SquareActivity;->maybeRestoreFromPersistentBundle(Landroid/os/Bundle;)V

    .line 236
    :cond_0
    invoke-static {}, Lcom/squareup/AppDelegate$Locator;->getAppDelegate()Lcom/squareup/AppDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/AppDelegate;->getOhSnapLogger()Lcom/squareup/log/OhSnapLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/SquareActivity;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 238
    new-instance v0, Lcom/squareup/ui/ActivityScopeCleaner;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ActivityScopeCleaner;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/squareup/ui/SquareActivity;->scopeCleaner:Lcom/squareup/ui/ActivityScopeCleaner;

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 241
    sget-object v1, Lcom/squareup/ui/SquareActivity;->KEY_FINISHING:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 242
    iget-object v1, p0, Lcom/squareup/ui/SquareActivity;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->FINISH:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->getOhSnapMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ": Found \"finishing\" in bundle"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 243
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->finish()V

    .line 247
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->isFinishing()Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->finishIfNotAuthorized()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    if-nez v0, :cond_4

    .line 250
    invoke-direct {p0, p1}, Lcom/squareup/ui/SquareActivity;->startMortarScope(Landroid/os/Bundle;)V

    .line 251
    iget-object v1, p0, Lcom/squareup/ui/SquareActivity;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v3, Lcom/squareup/log/OhSnapLogger$EventType;->CREATE:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->getOhSnapMessage()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 256
    :cond_4
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    if-nez v0, :cond_6

    .line 259
    invoke-virtual {p0, p1}, Lcom/squareup/ui/SquareActivity;->doCreate(Landroid/os/Bundle;)V

    .line 260
    iget-object p1, p0, Lcom/squareup/ui/SquareActivity;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isAndroidOWithBadAspectRatio()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 261
    invoke-virtual {p0, v2}, Lcom/squareup/ui/SquareActivity;->setRequestedOrientation(I)V

    .line 263
    :cond_5
    iget-object p1, p0, Lcom/squareup/ui/SquareActivity;->configurationChangeMonitor:Lcom/squareup/util/AndroidConfigurationChangeMonitor;

    invoke-interface {p1}, Lcom/squareup/util/AndroidConfigurationChangeMonitor;->fireOnConfigurationMaybeChanged()V

    :cond_6
    return-void
.end method

.method protected onDestroy()V
    .locals 7

    .line 640
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->dialogDestroyer:Lcom/squareup/ui/DialogDestroyer;

    invoke-virtual {v0}, Lcom/squareup/ui/DialogDestroyer;->onDestroy()V

    .line 641
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onDestroy()V

    .line 642
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Views;->nullViewDrawablesRecursive(Landroid/view/View;)V

    .line 643
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->DESTROY:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 644
    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->getOhSnapMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Object;

    iget-boolean v5, p0, Lcom/squareup/ui/SquareActivity;->configurationChangeIncoming:Z

    xor-int/2addr v5, v3

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const-string v5, " end scope? %s"

    invoke-static {v5, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 643
    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 646
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->scopeCleaner:Lcom/squareup/ui/ActivityScopeCleaner;

    invoke-virtual {v0}, Lcom/squareup/ui/ActivityScopeCleaner;->forget()V

    .line 647
    iget-boolean v0, p0, Lcom/squareup/ui/SquareActivity;->configurationChangeIncoming:Z

    if-nez v0, :cond_0

    .line 648
    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->endMortarScope()V

    .line 650
    :cond_0
    iput-boolean v3, p0, Lcom/squareup/ui/SquareActivity;->activityDestroyed:Z

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 598
    invoke-static {p1}, Lcom/squareup/ui/SquareActivity;->interceptKey(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2}, Landroidx/appcompat/app/AppCompatActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 602
    invoke-static {p1}, Lcom/squareup/ui/SquareActivity;->interceptKey(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2}, Landroidx/appcompat/app/AppCompatActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method protected onPause()V
    .locals 3

    .line 470
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->minesweeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ms/Minesweeper;

    invoke-interface {v0}, Lcom/squareup/ms/Minesweeper;->onPause()V

    .line 476
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->cardReaderPauseAndResumer:Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->isChangingConfigurations()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->onPause(Z)V

    .line 477
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->mediaButtonDisabler:Lcom/squareup/ui/MediaButtonDisabler;

    invoke-virtual {v0}, Lcom/squareup/ui/MediaButtonDisabler;->onPause()V

    .line 480
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->disableClicks()V

    .line 481
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->PAUSE:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->getOhSnapMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 483
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onPause()V

    .line 484
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->focusedActivityScanner:Lcom/squareup/radiography/FocusedActivityScanner;

    invoke-virtual {v0}, Lcom/squareup/radiography/FocusedActivityScanner;->resetFocusedActivity()V

    return-void
.end method

.method protected onResume()V
    .locals 3

    .line 445
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->focusedActivityScanner:Lcom/squareup/radiography/FocusedActivityScanner;

    invoke-virtual {v0, p0}, Lcom/squareup/radiography/FocusedActivityScanner;->setFocusedActivity(Landroid/app/Activity;)V

    .line 446
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onResume()V

    .line 447
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->RESUME:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->getOhSnapMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 450
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->finishIfNotAuthorized()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 452
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->enableClicks()V

    .line 454
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->minesweeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ms/Minesweeper;

    invoke-interface {v0}, Lcom/squareup/ms/Minesweeper;->onResume()V

    .line 455
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->cardReaderPauseAndResumer:Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->onResume()V

    .line 456
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->mediaButtonDisabler:Lcom/squareup/ui/MediaButtonDisabler;

    invoke-virtual {v0}, Lcom/squareup/ui/MediaButtonDisabler;->onResume()V

    .line 457
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->internetStatusMonitor:Lcom/squareup/internet/InternetStatusMonitor;

    invoke-interface {v0}, Lcom/squareup/internet/InternetStatusMonitor;->logNetworkConnectionStatus()V

    :cond_1
    :goto_0
    return-void
.end method

.method public onRetainCustomNonConfigurationInstance()Ljava/lang/Object;
    .locals 3

    .line 627
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->RETAIN_NONCONFIG:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->getOhSnapMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 628
    iput-boolean v0, p0, Lcom/squareup/ui/SquareActivity;->configurationChangeIncoming:Z

    .line 635
    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->getScopeNameOrNull()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .line 374
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 375
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->ON_SAVE:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->getOhSnapMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 376
    sget-object v0, Lcom/squareup/ui/SquareActivity;->KEY_FINISHING:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->isFinishing()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 381
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    invoke-virtual {v0}, Lmortar/MortarScope;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->activityScope:Lmortar/MortarScope;

    invoke-static {v0}, Lmortar/bundler/BundleServiceRunner;->getBundleServiceRunner(Lmortar/MortarScope;)Lmortar/bundler/BundleServiceRunner;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmortar/bundler/BundleServiceRunner;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 388
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BUNDLE_LOGGING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PERSISTENT_BUNDLE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 389
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->ON_SAVE:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-virtual {p1}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->logFull(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 393
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/SquareActivity;->doSaveInstanceState(Landroid/os/Bundle;)V

    .line 395
    invoke-virtual {p0, p1}, Lcom/squareup/ui/SquareActivity;->maybeSaveToPersistentBundle(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    .line 548
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onStart()V

    .line 549
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0}, Lcom/squareup/analytics/Analytics;->onActivityStart()V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    .line 497
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->requiresLocation()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 499
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-interface {v0}, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;->hasPermission()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 500
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-interface {v0}, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;->start()V

    .line 503
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-interface {v0}, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 504
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/squareup/ui/LocationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/SquareActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 509
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->stopFrequentLocationUpdates()V

    .line 510
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-interface {v0}, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;->pause()V

    .line 513
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->onWindowFocusChanged(Z)V

    return-void
.end method

.method protected requestFrequentLocationUpdates()V
    .locals 2

    .line 528
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    sget-object v1, Lcom/squareup/core/location/monitors/LocationInterval;->FREQUENT:Lcom/squareup/core/location/monitors/LocationInterval;

    invoke-interface {v0, v1}, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;->setInterval(Lcom/squareup/core/location/monitors/LocationInterval;)V

    return-void
.end method

.method protected requiresLocation()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setContentView(I)V
    .locals 3

    .line 666
    invoke-static {p0}, Lflow/path/PathContext;->root(Landroid/content/Context;)Lflow/path/PathContext;

    move-result-object v0

    .line 667
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 668
    invoke-virtual {v1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 669
    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 670
    invoke-virtual {p0, p1}, Lcom/squareup/ui/SquareActivity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    .line 674
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->contentViewInitializer:Lcom/squareup/development/drawer/ContentViewInitializer;

    invoke-interface {v0, p0, p1}, Lcom/squareup/development/drawer/ContentViewInitializer;->setActivityContentView(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .line 678
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->contentViewInitializer:Lcom/squareup/development/drawer/ContentViewInitializer;

    invoke-interface {v0, p0, p1, p2}, Lcom/squareup/development/drawer/ContentViewInitializer;->setActivityContentView(Landroid/app/Activity;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 4

    .line 542
    invoke-virtual {p0}, Lcom/squareup/ui/SquareActivity;->disableClicks()V

    .line 543
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->START_ACTIVITY:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/squareup/ui/SquareActivity;->getOhSnapMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 544
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 0

    .line 566
    invoke-super {p0, p1, p2}, Landroidx/appcompat/app/AppCompatActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 567
    invoke-direct {p0, p1}, Lcom/squareup/ui/SquareActivity;->setStartActivityTransition(Landroid/content/Intent;)V

    return-void
.end method

.method protected stopFrequentLocationUpdates()V
    .locals 2

    .line 536
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    sget-object v1, Lcom/squareup/core/location/monitors/LocationInterval;->STANDARD:Lcom/squareup/core/location/monitors/LocationInterval;

    invoke-interface {v0, v1}, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;->setInterval(Lcom/squareup/core/location/monitors/LocationInterval;)V

    return-void
.end method

.method public useDefaultExitAnimation()V
    .locals 1

    const/4 v0, 0x1

    .line 553
    iput-boolean v0, p0, Lcom/squareup/ui/SquareActivity;->useDefaultAnim:Z

    return-void
.end method
