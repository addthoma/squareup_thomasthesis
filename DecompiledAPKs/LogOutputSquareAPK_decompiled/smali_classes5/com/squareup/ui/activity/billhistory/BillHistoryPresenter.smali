.class final Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;
.super Lmortar/ViewPresenter;
.source "BillHistoryPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/activity/billhistory/BillHistoryView;",
        ">;"
    }
.end annotation


# instance fields
.field private final billHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;"
        }
    .end annotation
.end field

.field private final clock:Lcom/squareup/util/Clock;

.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final exchangesHost:Lcom/squareup/ui/activity/ExchangesHost;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final giftCardCheckBalanceStarter:Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;

.field private final isTextTileMode:Z

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final res:Lcom/squareup/util/Res;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private final rowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tenderWithCustomerInfoCache:Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;Lcom/squareup/text/Formatter;Lcom/squareup/print/PrinterStations;Lcom/squareup/util/Clock;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/ui/activity/ExchangesHost;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;)V
    .locals 2
    .param p9    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/ForTaxPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            "Lcom/squareup/ui/activity/ExchangesHost;",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 95
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 86
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->billHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-object v1, p1

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->res:Lcom/squareup/util/Res;

    move-object v1, p2

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p3

    .line 98
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->giftCardCheckBalanceStarter:Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;

    move-object v1, p4

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->features:Lcom/squareup/settings/server/Features;

    move-object v1, p5

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->localeProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->countryCodeProvider:Ljavax/inject/Provider;

    .line 102
    invoke-virtual {p7}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v1

    iput-boolean v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->isTextTileMode:Z

    move-object v1, p8

    .line 103
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->rowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    move-object v1, p9

    .line 104
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->percentageFormatter:Lcom/squareup/text/Formatter;

    move-object v1, p10

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->printerStations:Lcom/squareup/print/PrinterStations;

    move-object v1, p11

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->clock:Lcom/squareup/util/Clock;

    move-object v1, p12

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    move-object v1, p13

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    move-object/from16 v1, p14

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->exchangesHost:Lcom/squareup/ui/activity/ExchangesHost;

    move-object/from16 v1, p15

    .line 110
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    move-object/from16 v1, p16

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->tenderWithCustomerInfoCache:Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;)Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->giftCardCheckBalanceStarter:Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;

    return-object p0
.end method

.method private addAdjustmentRows(Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;Ljava/util/List;Landroid/content/Context;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .line 377
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/OrderAdjustment;

    .line 378
    iget-object v1, v0, Lcom/squareup/payment/OrderAdjustment;->subtotalType:Lcom/squareup/checkout/SubtotalType;

    sget-object v2, Lcom/squareup/checkout/SubtotalType;->SWEDISH_ROUNDING:Lcom/squareup/checkout/SubtotalType;

    if-ne v1, v2, :cond_0

    .line 379
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->rowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    invoke-virtual {v1, p3, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createSwedishRoundingAdjustmentRow(Landroid/content/Context;Lcom/squareup/payment/OrderAdjustment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 381
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->rowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    invoke-virtual {v1, p3, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createAdjustmentRow(Landroid/content/Context;Lcom/squareup/payment/OrderAdjustment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private addSurchargeRows(Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;Ljava/util/List;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Surcharge;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .line 388
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Surcharge;

    .line 389
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->rowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    invoke-virtual {v1, p3, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createSurchargeRow(Landroid/content/Context;Lcom/squareup/checkout/Surcharge;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private addTaxBreakdownSection(Landroid/content/Context;Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/util/TaxBreakdown;)V
    .locals 5

    if-eqz p3, :cond_1

    .line 412
    iget-object v0, p3, Lcom/squareup/util/TaxBreakdown;->taxBreakdownType:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    sget-object v1, Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;->TABLE:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    if-eq v0, v1, :cond_0

    goto :goto_1

    .line 416
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->addTaxBreakdownSection()Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownSection;

    move-result-object p2

    .line 419
    iget-object p3, p3, Lcom/squareup/util/TaxBreakdown;->sortedTaxBreakdownAmountsByTaxId:Ljava/util/SortedMap;

    invoke-interface {p3}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;

    .line 420
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, v0, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->taxPercentage:Lcom/squareup/util/Percentage;

    .line 421
    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 422
    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/print/R$string;->tax_breakdown_table_vat_rate_name_and_percentage:I

    .line 423
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->label:Ljava/lang/String;

    const-string v4, "name"

    .line 424
    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "percentage"

    .line 425
    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 426
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 427
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 428
    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->rowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    .line 429
    invoke-virtual {v2, p1, v1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createTaxBreakdownRow(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;)Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownRow;

    move-result-object v0

    .line 428
    invoke-virtual {p2, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownSection;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method private addTotalSection(Landroid/content/Context;Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/TaxBreakdown;)V
    .locals 7

    .line 327
    invoke-virtual {p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->addTotalSection()Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;

    move-result-object p2

    .line 329
    sget-object v0, Lcom/squareup/calc/constants/CalculationPhase;->SURCHARGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    invoke-direct {p0, p3, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->surchargesWithPhase(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/calc/constants/CalculationPhase;)Ljava/util/List;

    move-result-object v0

    .line 330
    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->SURCHARGE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    invoke-direct {p0, p3, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->surchargesWithPhase(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/calc/constants/CalculationPhase;)Ljava/util/List;

    move-result-object v1

    .line 332
    invoke-virtual {p3}, Lcom/squareup/billhistory/model/BillHistory;->getFirstOrder()Lcom/squareup/payment/Order;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/Order;->getOrderAdjustments()Ljava/util/List;

    move-result-object v2

    .line 333
    sget-object v3, Lcom/squareup/checkout/SubtotalType;->DISCOUNT:Lcom/squareup/checkout/SubtotalType;

    invoke-direct {p0, v2, v3}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->adjustmentsWithType(Ljava/util/List;Lcom/squareup/checkout/SubtotalType;)Ljava/util/List;

    move-result-object v3

    .line 334
    sget-object v4, Lcom/squareup/checkout/SubtotalType;->INCLUSIVE_TAX:Lcom/squareup/checkout/SubtotalType;

    invoke-direct {p0, v2, v4}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->adjustmentsWithType(Ljava/util/List;Lcom/squareup/checkout/SubtotalType;)Ljava/util/List;

    move-result-object v4

    .line 335
    sget-object v5, Lcom/squareup/checkout/SubtotalType;->TAX:Lcom/squareup/checkout/SubtotalType;

    invoke-direct {p0, v2, v5}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->adjustmentsWithType(Ljava/util/List;Lcom/squareup/checkout/SubtotalType;)Ljava/util/List;

    move-result-object v5

    .line 336
    sget-object v6, Lcom/squareup/checkout/SubtotalType;->SWEDISH_ROUNDING:Lcom/squareup/checkout/SubtotalType;

    invoke-direct {p0, v2, v6}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->adjustmentsWithType(Ljava/util/List;Lcom/squareup/checkout/SubtotalType;)Ljava/util/List;

    move-result-object v2

    .line 340
    invoke-direct {p0, p2, v3, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->addAdjustmentRows(Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;Ljava/util/List;Landroid/content/Context;)V

    .line 343
    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->rowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    invoke-virtual {p3}, Lcom/squareup/billhistory/model/BillHistory;->getFirstOrderSubtotal()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-virtual {v3, p1, v6}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createSubtotalRow(Landroid/content/Context;Lcom/squareup/protos/common/Money;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p2, v3}, Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;->addView(Landroid/view/View;)V

    .line 347
    invoke-direct {p0, p2, v0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->addSurchargeRows(Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;Ljava/util/List;Landroid/content/Context;)V

    if-eqz p4, :cond_0

    .line 351
    iget-object p4, p4, Lcom/squareup/util/TaxBreakdown;->taxBreakdownType:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    sget-object v0, Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;->TABLE:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    if-ne p4, v0, :cond_0

    .line 352
    iget-object p4, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->rowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    invoke-virtual {p4, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createTaxMultipleAdjustmentRow(Landroid/content/Context;)Landroid/view/View;

    move-result-object p4

    invoke-virtual {p2, p4}, Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 354
    :cond_0
    invoke-direct {p0, p2, v5, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->addAdjustmentRows(Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;Ljava/util/List;Landroid/content/Context;)V

    .line 355
    invoke-direct {p0, p2, v4, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->addAdjustmentRows(Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;Ljava/util/List;Landroid/content/Context;)V

    .line 360
    :goto_0
    invoke-direct {p0, p2, v1, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->addSurchargeRows(Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;Ljava/util/List;Landroid/content/Context;)V

    .line 363
    invoke-direct {p0, p2, v2, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->addAdjustmentRows(Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;Ljava/util/List;Landroid/content/Context;)V

    .line 366
    invoke-virtual {p3}, Lcom/squareup/billhistory/model/BillHistory;->hasNonZeroTip()Z

    move-result p4

    if-eqz p4, :cond_2

    .line 367
    invoke-virtual {p3}, Lcom/squareup/billhistory/model/BillHistory;->getFirstOrder()Lcom/squareup/payment/Order;

    move-result-object p4

    invoke-virtual {p4}, Lcom/squareup/payment/Order;->getAutoGratuity()Lcom/squareup/checkout/Surcharge$AutoGratuity;

    move-result-object p4

    if-eqz p4, :cond_1

    const/4 p4, 0x1

    goto :goto_1

    :cond_1
    const/4 p4, 0x0

    .line 368
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->rowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    iget-object v1, p3, Lcom/squareup/billhistory/model/BillHistory;->tip:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, p1, v1, p4}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createTipRow(Landroid/content/Context;Lcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p4

    invoke-virtual {p2, p4}, Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;->addView(Landroid/view/View;)V

    .line 372
    :cond_2
    iget-object p4, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->rowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    invoke-virtual {p3}, Lcom/squareup/billhistory/model/BillHistory;->getFirstOrder()Lcom/squareup/payment/Order;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p3

    invoke-virtual {p4, p1, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createTotalRow(Landroid/content/Context;Lcom/squareup/protos/common/Money;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;->addView(Landroid/view/View;)V

    return-void
.end method

.method private adjustmentsWithType(Ljava/util/List;Lcom/squareup/checkout/SubtotalType;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;",
            "Lcom/squareup/checkout/SubtotalType;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;"
        }
    .end annotation

    .line 400
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 401
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/OrderAdjustment;

    .line 402
    iget-object v2, v1, Lcom/squareup/payment/OrderAdjustment;->subtotalType:Lcom/squareup/checkout/SubtotalType;

    if-ne v2, p2, :cond_0

    .line 403
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private createGiftCardOnClickListener(Lcom/squareup/checkout/CartItem;)Lcom/squareup/debounce/DebouncedOnClickListener;
    .locals 1

    .line 570
    new-instance v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter$1;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;Lcom/squareup/checkout/CartItem;)V

    return-object v0
.end method

.method private getItemsTitle(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;
    .locals 3

    .line 276
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getTicketName()Ljava/lang/String;

    move-result-object p1

    .line 277
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 279
    :try_start_0
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->open_tickets_title_with_number:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "number"

    .line 280
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 281
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 282
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 284
    :catch_0
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->open_tickets_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "name"

    .line 285
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 286
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 287
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 291
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->uppercase_items_and_services:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getLoyaltyEarnedStringRes(I)I
    .locals 1

    if-nez p1, :cond_0

    .line 515
    sget p1, Lcom/squareup/billhistoryui/R$string;->bill_history_loyalty_earned_zero:I

    return p1

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 517
    sget p1, Lcom/squareup/billhistoryui/R$string;->bill_history_loyalty_earned_singular:I

    return p1

    .line 519
    :cond_1
    sget p1, Lcom/squareup/billhistoryui/R$string;->bill_history_loyalty_earned:I

    return p1
.end method

.method private getLoyaltySubtitle(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;)Ljava/lang/String;
    .locals 2

    .line 526
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->new_enrollment:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->bill_history_loyalty_enrolled_in_loyalty:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 528
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 529
    sget-object v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter$2;->$SwitchMap$com$squareup$protos$client$bills$Tender$LoyaltyDetails$ReasonForNoStars:[I

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    return-object v1

    .line 558
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->bill_history_loyalty_zero_earned_reason_spend_too_low:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 556
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->bill_history_loyalty_zero_earned_reason_skipped_screen:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 553
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->bill_history_loyalty_zero_earned_reason_returned_from_receipt_screen:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 550
    :pswitch_3
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->bill_history_loyalty_zero_earned_reason_returned_from_loyalty_screen:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 544
    :pswitch_4
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->bill_history_loyalty_zero_earned_reason_purchase_did_not_qualify:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    .line 546
    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->pointsTerms()Lcom/squareup/loyalty/PointsTerms;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyalty/PointsTerms;->getPlural()Ljava/lang/String;

    move-result-object v0

    const-string v1, "points_plural"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 547
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 548
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 541
    :pswitch_5
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->bill_history_loyalty_zero_earned_reason_offline_mode:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 539
    :pswitch_6
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->bill_history_loyalty_zero_earned_reason_not_yet_subscribed:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 536
    :pswitch_7
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->bill_history_loyalty_zero_earned_reason_missing_loyalty_info:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 533
    :pswitch_8
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->bill_history_loyalty_zero_earned_reason_client_disabled_loyalty:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 531
    :pswitch_9
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->bill_history_loyalty_zero_earned_reason_buyer_declined:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic lambda$null$0(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 122
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->showLoading()V

    return-void
.end method

.method private populateItems(Landroid/content/Context;Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/TaxBreakdown;)V
    .locals 4

    .line 439
    invoke-virtual {p3}, Lcom/squareup/billhistory/model/BillHistory;->getFirstOrder()Lcom/squareup/payment/Order;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    const/4 v1, 0x0

    if-eqz p4, :cond_1

    .line 441
    iget-object v2, p4, Lcom/squareup/util/TaxBreakdown;->taxBreakdownType:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    sget-object v3, Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;->TABLE:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    if-ne v2, v3, :cond_1

    .line 442
    iget-object v1, p4, Lcom/squareup/util/TaxBreakdown;->taxLabelsByCartItemId:Ljava/util/Map;

    iget-object v2, v0, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 443
    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    const-string v2, " "

    invoke-static {v1, v2}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 447
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 448
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->rowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->createGiftCardOnClickListener(Lcom/squareup/checkout/CartItem;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v2

    iget-boolean v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->isTextTileMode:Z

    invoke-virtual {v1, p1, v0, v2, v3}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createGiftCardRows(Landroid/content/Context;Lcom/squareup/checkout/CartItem;Lcom/squareup/debounce/DebouncedOnClickListener;Z)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 451
    :cond_2
    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->rowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    iget-boolean v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->isTextTileMode:Z

    invoke-virtual {v2, p1, v0, v3, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createItemizationRows(Landroid/content/Context;Lcom/squareup/checkout/CartItem;ZLjava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 454
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    .line 455
    invoke-virtual {p2, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->addItemization(Landroid/view/View;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method private populateItemsWithDiningOptions(Landroid/content/Context;Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/TaxBreakdown;)V
    .locals 6

    .line 466
    invoke-virtual {p3}, Lcom/squareup/billhistory/model/BillHistory;->getFirstOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    .line 467
    invoke-virtual {p3}, Lcom/squareup/billhistory/model/BillHistory;->getFirstOrder()Lcom/squareup/payment/Order;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/payment/Order;->getDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object p3

    .line 468
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->localeProvider:Ljavax/inject/Provider;

    .line 470
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    .line 469
    invoke-static {v0, p3, v1}, Lcom/squareup/itemsorter/ItemSorter;->groupItemsByDiningOption(Ljava/util/List;Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/Locale;)Ljava/util/List;

    move-result-object p3

    .line 472
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;

    .line 473
    invoke-virtual {v0}, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->addItemsSection(Ljava/util/List;)Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;

    move-result-object v1

    .line 475
    invoke-virtual {v0}, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    const/4 v3, 0x0

    if-eqz p4, :cond_2

    .line 477
    iget-object v4, p4, Lcom/squareup/util/TaxBreakdown;->taxBreakdownType:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    sget-object v5, Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;->TABLE:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    if-ne v4, v5, :cond_2

    .line 478
    iget-object v3, p4, Lcom/squareup/util/TaxBreakdown;->taxLabelsByCartItemId:Ljava/util/Map;

    iget-object v4, v2, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v4, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 479
    invoke-interface {v3}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v3

    const-string v4, " "

    invoke-static {v3, v4}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 483
    :cond_2
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 484
    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->rowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    invoke-direct {p0, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->createGiftCardOnClickListener(Lcom/squareup/checkout/CartItem;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v4

    iget-boolean v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->isTextTileMode:Z

    invoke-virtual {v3, p1, v2, v4, v5}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createGiftCardRows(Landroid/content/Context;Lcom/squareup/checkout/CartItem;Lcom/squareup/debounce/DebouncedOnClickListener;Z)Ljava/util/List;

    move-result-object v2

    goto :goto_0

    .line 487
    :cond_3
    iget-object v4, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->rowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    iget-boolean v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->isTextTileMode:Z

    .line 488
    invoke-virtual {v4, p1, v2, v5, v3}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createItemizationRows(Landroid/content/Context;Lcom/squareup/checkout/CartItem;ZLjava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 491
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    .line 492
    invoke-virtual {v1, v3}, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;->addView(Landroid/view/View;)V

    goto :goto_1

    :cond_4
    return-void
.end method

.method private showDiningOptions(Lcom/squareup/billhistory/model/BillHistory;)Z
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DINING_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getFirstOrder()Lcom/squareup/payment/Order;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private surchargesWithPhase(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/calc/constants/CalculationPhase;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Lcom/squareup/calc/constants/CalculationPhase;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Surcharge;",
            ">;"
        }
    .end annotation

    .line 394
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_AUTO_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 395
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getFirstOrder()Lcom/squareup/payment/Order;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getSurcharges()Ljava/util/List;

    move-result-object p1

    .line 394
    invoke-static {p1, p2}, Lcom/squareup/checkout/Surcharge;->withPhase(Ljava/util/List;Lcom/squareup/calc/constants/CalculationPhase;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    .line 395
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private updateButtons(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 10

    .line 170
    iget-boolean v0, p2, Lcom/squareup/billhistory/model/BillHistory;->isVoided:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {p1, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->showButtons(Z)V

    return-void

    .line 177
    :cond_0
    iget-boolean v0, p2, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    const/4 v2, 0x1

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->hasReceipt()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 179
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->hasNonLostTender()Z

    move-result v3

    .line 181
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->isFullyRefunded()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p2, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    iget-object v4, v4, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v8, v4, v6

    if-lez v8, :cond_2

    if-eqz v3, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    .line 183
    :goto_1
    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->clock:Lcom/squareup/util/Clock;

    const/4 v6, -0x1

    invoke-static {v5, v6}, Lcom/squareup/util/Times;->hoursFromNow(Lcom/squareup/util/Clock;I)Ljava/util/Date;

    move-result-object v5

    .line 184
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->mostRecentTenderTimestamp()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v5

    .line 185
    iget-object v6, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->printerStations:Lcom/squareup/print/PrinterStations;

    new-array v7, v2, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v8, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    aput-object v8, v7, v1

    invoke-interface {v6, v7}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v6

    .line 186
    iget-object v7, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->printerStations:Lcom/squareup/print/PrinterStations;

    new-array v8, v2, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v9, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    aput-object v9, v8, v1

    invoke-interface {v7, v8}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v7

    if-eqz v4, :cond_3

    .line 190
    sget v4, Lcom/squareup/billhistoryui/R$string;->receipt_detail_refunded:I

    goto :goto_2

    .line 192
    :cond_3
    iget-object v4, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->exchangesHost:Lcom/squareup/ui/activity/ExchangesHost;

    invoke-interface {v4}, Lcom/squareup/ui/activity/ExchangesHost;->exchangesEnabled()Z

    move-result v4

    if-eqz v4, :cond_4

    sget v4, Lcom/squareup/activity/R$string;->return_or_exchange:I

    goto :goto_2

    :cond_4
    sget v4, Lcom/squareup/activity/R$string;->refund_issue:I

    .line 197
    :goto_2
    iget-object v8, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v8}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/CountryCode;

    .line 200
    sget-object v9, Lcom/squareup/CountryCode;->FR:Lcom/squareup/CountryCode;

    if-ne v8, v9, :cond_5

    sget v8, Lcom/squareup/activity/R$string;->receipt_reissue:I

    goto :goto_3

    :cond_5
    sget v8, Lcom/squareup/activity/R$string;->receipt_new:I

    :goto_3
    if-eqz v0, :cond_6

    .line 203
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->isFullyRefunded()Z

    move-result p2

    if-nez p2, :cond_6

    const/4 p2, 0x1

    goto :goto_4

    :cond_6
    const/4 p2, 0x0

    :goto_4
    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->setRefundEnabled(Z)V

    if-eqz v0, :cond_7

    if-eqz v3, :cond_7

    const/4 p2, 0x1

    goto :goto_5

    :cond_7
    const/4 p2, 0x0

    .line 204
    :goto_5
    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->setReceiptEnabled(Z)V

    if-eqz v5, :cond_8

    if-eqz v6, :cond_8

    const/4 p2, 0x1

    goto :goto_6

    :cond_8
    const/4 p2, 0x0

    .line 205
    :goto_6
    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->setReprintTicketButtonVisible(Z)V

    if-eqz v0, :cond_9

    if-eqz v7, :cond_9

    const/4 v1, 0x1

    .line 206
    :cond_9
    invoke-virtual {p1, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->setPrintGiftReceiptButtonVisible(Z)V

    .line 207
    invoke-virtual {p1, v4}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->setRefundButtonText(I)V

    .line 208
    invoke-virtual {p1, v8}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->setReceiptButtonText(I)V

    .line 209
    invoke-virtual {p1, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->showButtons(Z)V

    return-void
.end method

.method private updateDeprecatedTenders(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 4

    .line 235
    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->clearTenders()V

    .line 237
    iget-boolean v0, p2, Lcom/squareup/billhistory/model/BillHistory;->isVoided:Z

    if-eqz v0, :cond_0

    return-void

    .line 241
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->getFirstBillTenders()Ljava/util/List;

    move-result-object v0

    .line 243
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_1

    .line 244
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/billhistory/model/TenderHistory;

    const/4 v3, 0x0

    invoke-virtual {p1, p2, v2, v3}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->addTenderSection(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/protos/client/rolodex/Contact;)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private updateItems(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 3

    .line 295
    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->clearItems()V

    .line 296
    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->clearTotal()V

    .line 297
    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->clearTaxBreakdown()V

    .line 299
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->isNoSale()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 305
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v1

    sget-object v2, Lcom/squareup/CountryCode;->GB:Lcom/squareup/CountryCode;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 306
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v1

    sget-object v2, Lcom/squareup/CountryCode;->FR:Lcom/squareup/CountryCode;

    if-ne v1, v2, :cond_2

    .line 307
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->getFirstOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$LineItems;->fee:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/squareup/util/TaxBreakdown;->fromOrderSnapshot(Lcom/squareup/payment/Order;Ljava/util/List;)Lcom/squareup/util/TaxBreakdown;

    move-result-object v0

    .line 310
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 311
    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->showDiningOptions(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 312
    invoke-direct {p0, v1, p1, p2, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->populateItemsWithDiningOptions(Landroid/content/Context;Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/TaxBreakdown;)V

    goto :goto_0

    .line 314
    :cond_3
    invoke-direct {p0, v1, p1, p2, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->populateItems(Landroid/content/Context;Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/TaxBreakdown;)V

    .line 317
    :goto_0
    iget-boolean v2, p2, Lcom/squareup/billhistory/model/BillHistory;->isVoided:Z

    if-eqz v2, :cond_4

    return-void

    .line 321
    :cond_4
    invoke-direct {p0, v1, p1, p2, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->addTotalSection(Landroid/content/Context;Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/TaxBreakdown;)V

    .line 322
    invoke-direct {p0, v1, p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->addTaxBreakdownSection(Landroid/content/Context;Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/util/TaxBreakdown;)V

    return-void
.end method

.method private updateLoyalty(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 4

    .line 499
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->getLoyaltyDetails()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyProgramActive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 501
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->getLoyaltyDetails()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->stars_earned:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 502
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->getLoyaltyDetails()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    .line 504
    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    .line 505
    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->getLoyaltyEarnedStringRes(I)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(II)Ljava/lang/String;

    move-result-object v0

    .line 506
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->getLoyaltyDetails()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->getLoyaltySubtitle(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;)Ljava/lang/String;

    move-result-object p2

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;->display_name:Ljava/lang/String;

    .line 507
    invoke-static {v1}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 504
    :goto_0
    invoke-virtual {p1, v0, p2, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->showLoyaltySection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 509
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->hideLoyalty()V

    :goto_1
    return-void
.end method

.method private updateRelatedBills(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 3

    .line 213
    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->clearRelatedBills()V

    .line 215
    iget-boolean v0, p2, Lcom/squareup/billhistory/model/BillHistory;->isVoided:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 216
    invoke-virtual {p1, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->showRefunds(Z)V

    return-void

    .line 220
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->hasMultipleBills()Z

    move-result v0

    if-nez v0, :cond_1

    .line 221
    invoke-virtual {p1, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->showRefunds(Z)V

    return-void

    :cond_1
    const/4 v0, 0x1

    .line 225
    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->showRefunds(Z)V

    .line 228
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->getRelatedBillsExceptFirst()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 229
    iget-boolean v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->isTextTileMode:Z

    invoke-virtual {p1, p2, v1, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->addRelatedBillSection(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/server/payment/RelatedBillHistory;Z)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private updateTenders(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryView;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
            ">;)V"
        }
    .end annotation

    .line 251
    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->clearTenders()V

    .line 253
    iget-boolean v0, p2, Lcom/squareup/billhistory/model/BillHistory;->isVoided:Z

    if-eqz v0, :cond_0

    return-void

    .line 257
    :cond_0
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;

    .line 258
    iget-object v1, v0, Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    iget-object v0, v0, Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {p1, p2, v1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->addTenderSection(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/protos/client/rolodex/Contact;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private updateTicketData(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    .line 263
    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->hideItemsTitle()V

    .line 264
    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->hideTicketNote()V

    .line 266
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->isNoSale()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->getTicketName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->showDiningOptions(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 267
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->getItemsTitle(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->showItemsTitle(Ljava/lang/String;)V

    .line 268
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->getTicketNote()Ljava/lang/String;

    move-result-object p2

    .line 269
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 270
    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->showTicketNote(Ljava/lang/String;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public dropView(Lcom/squareup/ui/activity/billhistory/BillHistoryView;)V
    .locals 2

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->giftCardCheckBalanceStarter:Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->failurePopup()Lcom/squareup/caller/FailurePopup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;->dropView(Lcom/squareup/caller/FailurePopup;)V

    .line 155
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 66
    check-cast p1, Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->dropView(Lcom/squareup/ui/activity/billhistory/BillHistoryView;)V

    return-void
.end method

.method public synthetic lambda$null$1$BillHistoryPresenter(Lcom/squareup/billhistory/model/BillHistory;)Lio/reactivex/SingleSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->tenderWithCustomerInfoCache:Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt;->addTendersCustomerInfo(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;Lcom/squareup/crm/RolodexServiceHelper;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$2$BillHistoryPresenter(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 126
    iget-object v0, p2, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    .line 127
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->updateButtons(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 128
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->updateRelatedBills(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 129
    iget-object p2, p2, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;->tendersWithCustomerInfo:Ljava/util/List;

    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->updateTenders(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/List;)V

    .line 130
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->updateTicketData(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 131
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->updateItems(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 132
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->updateLoyalty(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 133
    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->showContent()V

    return-void
.end method

.method public synthetic lambda$null$4$BillHistoryPresenter(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 140
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->updateButtons(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 141
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->updateRelatedBills(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 142
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->updateDeprecatedTenders(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 143
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->updateTicketData(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 144
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->updateItems(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 145
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->updateLoyalty(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 146
    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->showContent()V

    return-void
.end method

.method public synthetic lambda$onLoad$3$BillHistoryPresenter(Lcom/squareup/ui/activity/billhistory/BillHistoryView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->billHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 121
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryPresenter$n4dzJQn8QfrlXF7ZYNGJGBrlZMo;

    invoke-direct {v1, p1}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryPresenter$n4dzJQn8QfrlXF7ZYNGJGBrlZMo;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryView;)V

    .line 122
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryPresenter$UNWrb8p6wbUpRV_HxnUyJCd9NlI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryPresenter$UNWrb8p6wbUpRV_HxnUyJCd9NlI;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;)V

    .line 123
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryPresenter$2sOE4LBo2HTsKia1HHbIOx7vRIY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryPresenter$2sOE4LBo2HTsKia1HHbIOx7vRIY;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;Lcom/squareup/ui/activity/billhistory/BillHistoryView;)V

    .line 125
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$BillHistoryPresenter(Lcom/squareup/ui/activity/billhistory/BillHistoryView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->billHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryPresenter$0TlGHh-RhwJzqlbsPjaBFQISP4M;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryPresenter$0TlGHh-RhwJzqlbsPjaBFQISP4M;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;Lcom/squareup/ui/activity/billhistory/BillHistoryView;)V

    .line 139
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 115
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 116
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    new-instance v0, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryPresenter$tB6eiSP2BHCFX8hzjjaOlZfwO6Q;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryPresenter$tB6eiSP2BHCFX8hzjjaOlZfwO6Q;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;Lcom/squareup/ui/activity/billhistory/BillHistoryView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 137
    :cond_0
    new-instance v0, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryPresenter$3FTvEg6MRxa_eVbXooH6Gjkgmh0;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryPresenter$3FTvEg6MRxa_eVbXooH6Gjkgmh0;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;Lcom/squareup/ui/activity/billhistory/BillHistoryView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 150
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->giftCardCheckBalanceStarter:Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->failurePopup()Lcom/squareup/caller/FailurePopup;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;->takeView(Lcom/squareup/caller/FailurePopup;)V

    return-void
.end method

.method show(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->billHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
