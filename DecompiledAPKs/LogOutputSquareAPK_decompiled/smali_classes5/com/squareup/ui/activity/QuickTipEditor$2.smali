.class Lcom/squareup/ui/activity/QuickTipEditor$2;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "QuickTipEditor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/QuickTipEditor;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/QuickTipEditor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/QuickTipEditor;)V
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditor$2;->this$0:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x5

    if-ne p2, p1, :cond_0

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditor$2;->this$0:Lcom/squareup/ui/activity/QuickTipEditor;

    iget-object p1, p1, Lcom/squareup/ui/activity/QuickTipEditor;->presenter:Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->onActionNextPressed()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
