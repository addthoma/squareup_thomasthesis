.class public final Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;
.super Ljava/lang/Object;
.source "PosActivitySearchPaymentStarter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;",
        ">;"
    }
.end annotation


# instance fields
.field private final activeCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final dippedCardTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final emvDipScreenHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final historyPermissionControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;",
            ">;"
        }
    .end annotation
.end field

.field private final nameFetchInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentCounterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentCounter;",
            ">;"
        }
    .end annotation
.end field

.field private final readerSessionIdsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderSessionIds;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderSessionIds;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentCounter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;",
            ">;)V"
        }
    .end annotation

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p2, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->nameFetchInfoProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p3, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->readerSessionIdsProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p4, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->paymentCounterProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p5, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p6, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p7, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p8, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p9, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p10, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->historyPermissionControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderSessionIds;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentCounter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;",
            ">;)",
            "Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;"
        }
    .end annotation

    .line 82
    new-instance v11, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/log/ReaderSessionIds;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/util/Clock;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;)Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;
    .locals 12

    .line 91
    new-instance v11, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;-><init>(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/log/ReaderSessionIds;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/util/Clock;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;
    .locals 11

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->nameFetchInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->readerSessionIdsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/log/ReaderSessionIds;

    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->paymentCounterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/cardreader/PaymentCounter;

    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/NfcProcessor;

    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/cardreader/DippedCardTracker;

    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->historyPermissionControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->newInstance(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/log/ReaderSessionIds;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/util/Clock;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;)Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter_Factory;->get()Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    move-result-object v0

    return-object v0
.end method
