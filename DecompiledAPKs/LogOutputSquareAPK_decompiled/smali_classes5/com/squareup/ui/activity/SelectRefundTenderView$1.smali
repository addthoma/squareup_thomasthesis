.class Lcom/squareup/ui/activity/SelectRefundTenderView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "SelectRefundTenderView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/SelectRefundTenderView;->addTender(Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/SelectRefundTenderView;

.field final synthetic val$tenderId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/SelectRefundTenderView;Ljava/lang/String;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/squareup/ui/activity/SelectRefundTenderView$1;->this$0:Lcom/squareup/ui/activity/SelectRefundTenderView;

    iput-object p2, p0, Lcom/squareup/ui/activity/SelectRefundTenderView$1;->val$tenderId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/activity/SelectRefundTenderView$1;->this$0:Lcom/squareup/ui/activity/SelectRefundTenderView;

    iget-object p1, p1, Lcom/squareup/ui/activity/SelectRefundTenderView;->presenter:Lcom/squareup/ui/activity/SelectRefundTenderPresenter;

    iget-object v0, p0, Lcom/squareup/ui/activity/SelectRefundTenderView$1;->val$tenderId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/SelectRefundTenderPresenter;->tenderSelected(Ljava/lang/String;)V

    return-void
.end method
