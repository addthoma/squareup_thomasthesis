.class public final Lcom/squareup/ui/activity/RealActivityAppletGateway_Factory;
.super Ljava/lang/Object;
.source "RealActivityAppletGateway_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/RealActivityAppletGateway;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/activity/RealActivityAppletGateway_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 20
    iput-object p2, p0, Lcom/squareup/ui/activity/RealActivityAppletGateway_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/RealActivityAppletGateway_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            ">;)",
            "Lcom/squareup/ui/activity/RealActivityAppletGateway_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/ui/activity/RealActivityAppletGateway_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/RealActivityAppletGateway_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/ui/activity/ActivityApplet;)Lcom/squareup/ui/activity/RealActivityAppletGateway;
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/ui/activity/RealActivityAppletGateway;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/RealActivityAppletGateway;-><init>(Lflow/Flow;Lcom/squareup/ui/activity/ActivityApplet;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/RealActivityAppletGateway;
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/activity/RealActivityAppletGateway_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/activity/RealActivityAppletGateway_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/activity/ActivityApplet;

    invoke-static {v0, v1}, Lcom/squareup/ui/activity/RealActivityAppletGateway_Factory;->newInstance(Lflow/Flow;Lcom/squareup/ui/activity/ActivityApplet;)Lcom/squareup/ui/activity/RealActivityAppletGateway;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/activity/RealActivityAppletGateway_Factory;->get()Lcom/squareup/ui/activity/RealActivityAppletGateway;

    move-result-object v0

    return-object v0
.end method
