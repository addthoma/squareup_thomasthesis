.class public final Lcom/squareup/ui/activity/RealCardPresentRefundKt$restartingMonitor$1;
.super Ljava/lang/Object;
.source "RealCardPresentRefund.kt"

# interfaces
.implements Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/RealCardPresentRefundKt;->restartingMonitor(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;Lcom/squareup/ui/NfcProcessor;J)Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0010\u0010\u0008\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "com/squareup/ui/activity/RealCardPresentRefundKt$restartingMonitor$1",
        "Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;",
        "contactlessReaderAdded",
        "",
        "cardReaderId",
        "Lcom/squareup/cardreader/CardReaderId;",
        "contactlessReaderReadyForPayment",
        "contactlessReaderRemoved",
        "contactlessReaderTimedOut",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $amount:J

.field final synthetic $handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

.field final synthetic $nfcProcessor:Lcom/squareup/ui/NfcProcessor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/NfcProcessor;JLcom/squareup/ui/activity/CardPresentRefundEventHandler;)V
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$restartingMonitor$1;->$nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    iput-wide p2, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$restartingMonitor$1;->$amount:J

    iput-object p4, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$restartingMonitor$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public contactlessReaderAdded(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 3

    const-string v0, "cardReaderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 189
    iget-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$restartingMonitor$1;->$nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    iget-wide v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$restartingMonitor$1;->$amount:J

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    invoke-static {p1, v0, v1, v2}, Lcom/squareup/ui/activity/RealCardPresentRefundKt;->access$restartRefundRequestOnReaders(Lcom/squareup/ui/NfcProcessor;JLcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 190
    iget-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$restartingMonitor$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object v0, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v1, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    :cond_0
    return-void
.end method

.method public contactlessReaderReadyForPayment(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 1

    const-string v0, "cardReaderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public contactlessReaderRemoved(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 1

    const-string v0, "cardReaderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public contactlessReaderTimedOut(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 3

    const-string v0, "cardReaderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    iget-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$restartingMonitor$1;->$nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    iget-wide v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$restartingMonitor$1;->$amount:J

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    invoke-static {p1, v0, v1, v2}, Lcom/squareup/ui/activity/RealCardPresentRefundKt;->access$restartRefundRequestOnReaders(Lcom/squareup/ui/NfcProcessor;JLcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 184
    iget-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$restartingMonitor$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object v0, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v1, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    :cond_0
    return-void
.end method
