.class public final Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
.super Ljava/lang/Object;
.source "CardPresentRefundMessageResources.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \u00172\u00020\u0001:\u0001\u0017B!\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0006H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000b\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/activity/CardPresentRefundMessageResources;",
        "",
        "titleId",
        "",
        "messageId",
        "errorState",
        "Lcom/squareup/ui/activity/CardPresentRefundErrorState;",
        "(IILcom/squareup/ui/activity/CardPresentRefundErrorState;)V",
        "getErrorState",
        "()Lcom/squareup/ui/activity/CardPresentRefundErrorState;",
        "getMessageId",
        "()I",
        "getTitleId",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "Companion",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;


# instance fields
.field private final errorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

.field private final messageId:I

.field private final titleId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    return-void
.end method

.method public constructor <init>(IILcom/squareup/ui/activity/CardPresentRefundErrorState;)V
    .locals 1

    const-string v0, "errorState"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->titleId:I

    iput p2, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->messageId:I

    iput-object p3, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->errorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;IILcom/squareup/ui/activity/CardPresentRefundErrorState;ILjava/lang/Object;)Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget p1, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->titleId:I

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->messageId:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->errorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->copy(IILcom/squareup/ui/activity/CardPresentRefundErrorState;)Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object p0

    return-object p0
.end method

.method public static final empty()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->empty()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    return-object v0
.end method

.method public static final fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    return-object v0
.end method

.method public static final ofActionRequired()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->ofActionRequired()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    return-object v0
.end method

.method public static final ofNfcCollision()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->ofNfcCollision()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    return-object v0
.end method

.method public static final ofPleaseTapCard()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->ofPleaseTapCard()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    return-object v0
.end method

.method public static final ofRemoveCard()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->ofRemoveCard()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    return-object v0
.end method

.method public static final ofTapAgain()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->ofTapAgain()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    return-object v0
.end method

.method public static final ofTapNow()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->ofTapNow()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    return-object v0
.end method

.method public static final ofUnlockDevice()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->ofUnlockDevice()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->titleId:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->messageId:I

    return v0
.end method

.method public final component3()Lcom/squareup/ui/activity/CardPresentRefundErrorState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->errorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    return-object v0
.end method

.method public final copy(IILcom/squareup/ui/activity/CardPresentRefundErrorState;)Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 1

    const-string v0, "errorState"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;-><init>(IILcom/squareup/ui/activity/CardPresentRefundErrorState;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    iget v0, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->titleId:I

    iget v1, p1, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->titleId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->messageId:I

    iget v1, p1, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->messageId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->errorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    iget-object p1, p1, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->errorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getErrorState()Lcom/squareup/ui/activity/CardPresentRefundErrorState;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->errorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    return-object v0
.end method

.method public final getMessageId()I
    .locals 1

    .line 18
    iget v0, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->messageId:I

    return v0
.end method

.method public final getTitleId()I
    .locals 1

    .line 17
    iget v0, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->titleId:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->titleId:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->messageId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->errorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CardPresentRefundMessageResources(titleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->titleId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", messageId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->messageId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", errorState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->errorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
