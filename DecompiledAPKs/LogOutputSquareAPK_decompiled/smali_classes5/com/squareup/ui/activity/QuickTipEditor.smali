.class public Lcom/squareup/ui/activity/QuickTipEditor;
.super Landroid/widget/FrameLayout;
.source "QuickTipEditor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/QuickTipEditor$Component;
    }
.end annotation


# instance fields
.field presenter:Lcom/squareup/ui/activity/QuickTipEditorPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

.field tipOptionsView:Lcom/squareup/ui/activity/TipOptionsView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    const-class p2, Lcom/squareup/ui/activity/QuickTipEditor$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/activity/QuickTipEditor$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/activity/QuickTipEditor$Component;->inject(Lcom/squareup/ui/activity/QuickTipEditor;)V

    .line 61
    sget p2, Lcom/squareup/billhistoryui/R$layout;->activity_applet_quicktip_editor:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/activity/QuickTipEditor;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 246
    sget v0, Lcom/squareup/billhistoryui/R$id;->tip_options_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TipOptionsView;

    iput-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipOptionsView:Lcom/squareup/ui/activity/TipOptionsView;

    .line 247
    sget v0, Lcom/squareup/billhistoryui/R$id;->tip_amount_editor:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/OnDeleteEditText;

    iput-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    return-void
.end method

.method private fireFocusChange(Z)V
    .locals 1

    .line 199
    invoke-virtual {p0}, Lcom/squareup/ui/activity/QuickTipEditor;->getOnFocusChangeListener()Landroid/view/View$OnFocusChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201
    invoke-interface {v0, p0, p1}, Landroid/view/View$OnFocusChangeListener;->onFocusChange(Landroid/view/View;Z)V

    :cond_0
    return-void
.end method

.method private static setHorizontalGravity(Landroid/widget/TextView;I)V
    .locals 1

    .line 209
    invoke-virtual {p0}, Landroid/widget/TextView;->getGravity()I

    move-result v0

    .line 210
    invoke-static {v0, p1}, Lcom/squareup/ui/activity/QuickTipEditor;->withHorizontalGravity(II)I

    move-result p1

    if-eq p1, v0, :cond_0

    .line 213
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setGravity(I)V

    :cond_0
    return-void
.end method

.method private static setHorizontalLayoutGravity(Landroid/view/View;I)V
    .locals 2

    .line 222
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 223
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    invoke-static {v1, p1}, Lcom/squareup/ui/activity/QuickTipEditor;->withHorizontalGravity(II)I

    move-result p1

    .line 225
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    if-eq p1, v1, :cond_0

    .line 226
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 227
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method private static withHorizontalGravity(II)I
    .locals 1

    const v0, -0x800008

    and-int/2addr p0, v0

    const v0, 0x800007

    and-int/2addr p1, v0

    or-int/2addr p0, p1

    return p0
.end method


# virtual methods
.method public clearTipAmount()V
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/OnDeleteEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public focusOnTipAmount()V
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/OnDeleteEditText;->requestFocus()Z

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/OnDeleteEditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/OnDeleteEditText;->setSelection(I)V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public getTipAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->presenter:Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->getTipAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getTipOptionAmounts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->presenter:Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->getTipOptionAmounts()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTipOptionLabels()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->presenter:Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->getTipOptionLabels()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isDisplayingPercentages()Z
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->presenter:Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->isDisplayingPercentages()Z

    move-result v0

    return v0
.end method

.method public isQuickTipOptionUsed()Z
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->presenter:Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->isQuickTipOptionUsed()Z

    move-result v0

    return v0
.end method

.method public isTipAmountEmpty()Z
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/OnDeleteEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isTipAmountVisible()Z
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/OnDeleteEditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$onAttachedToWindow$0$QuickTipEditor(Ljava/lang/Long;)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->presenter:Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->onTipOptionSelected(Ljava/lang/Long;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$1$QuickTipEditor(Landroid/view/View;)V
    .locals 0

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditor;->presenter:Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->onDeleteKey()V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$2$QuickTipEditor(Landroid/view/View;Z)V
    .locals 0

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditor;->presenter:Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->onFocusChange(Z)V

    .line 95
    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/QuickTipEditor;->fireFocusChange(Z)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 70
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipOptionsView:Lcom/squareup/ui/activity/TipOptionsView;

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$QuickTipEditor$pC_84gDUcbqZDaov1HbOxM8bZPw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$QuickTipEditor$pC_84gDUcbqZDaov1HbOxM8bZPw;-><init>(Lcom/squareup/ui/activity/QuickTipEditor;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/TipOptionsView;->setOnTipOptionSelectedListener(Lcom/squareup/ui/activity/TipOptionsView$OnTipOptionSelectedListener;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    new-instance v1, Lcom/squareup/ui/activity/QuickTipEditor$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/QuickTipEditor$1;-><init>(Lcom/squareup/ui/activity/QuickTipEditor;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/OnDeleteEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$QuickTipEditor$td8YftPYNOTcAQKQLdv8qnivVqQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$QuickTipEditor$td8YftPYNOTcAQKQLdv8qnivVqQ;-><init>(Lcom/squareup/ui/activity/QuickTipEditor;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/OnDeleteEditText;->setOnDeleteKeyListener(Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    new-instance v1, Lcom/squareup/ui/activity/QuickTipEditor$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/QuickTipEditor$2;-><init>(Lcom/squareup/ui/activity/QuickTipEditor;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/OnDeleteEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$QuickTipEditor$ta2ovRF4L1auahUsOqlVaBCb0FY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$QuickTipEditor$ta2ovRF4L1auahUsOqlVaBCb0FY;-><init>(Lcom/squareup/ui/activity/QuickTipEditor;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/OnDeleteEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->presenter:Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->takeView(Lcom/squareup/ui/activity/QuickTipEditor;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->presenter:Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->dropView()V

    .line 103
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .line 65
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 66
    invoke-direct {p0}, Lcom/squareup/ui/activity/QuickTipEditor;->bindViews()V

    return-void
.end method

.method public setAmountTextBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/OnDeleteEditText;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setHorizontalGravity(I)V
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipOptionsView:Lcom/squareup/ui/activity/TipOptionsView;

    invoke-static {v0, p1}, Lcom/squareup/ui/activity/QuickTipEditor;->setHorizontalLayoutGravity(Landroid/view/View;I)V

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipOptionsView:Lcom/squareup/ui/activity/TipOptionsView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/TipOptionsView;->setHorizontalGravity(I)V

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    invoke-static {v0, p1}, Lcom/squareup/ui/activity/QuickTipEditor;->setHorizontalGravity(Landroid/widget/TextView;I)V

    .line 151
    invoke-virtual {p0}, Lcom/squareup/ui/activity/QuickTipEditor;->requestLayout()V

    return-void
.end method

.method public setOnTipAmountListener(Lcom/squareup/ui/activity/QuickTipEditorPresenter$TipAmountListener;)V
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->presenter:Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->setOnTipAmountListener(Lcom/squareup/ui/activity/QuickTipEditorPresenter$TipAmountListener;)V

    return-void
.end method

.method public setTenderHistory(Lcom/squareup/billhistory/model/TenderHistory;)V
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->presenter:Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->setTenderHistory(Lcom/squareup/billhistory/model/TenderHistory;)V

    return-void
.end method

.method public setTipAmountShowImeNext(Z)V
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    if-eqz p1, :cond_0

    const/4 p1, 0x5

    goto :goto_0

    :cond_0
    const/4 p1, 0x6

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/OnDeleteEditText;->setImeOptions(I)V

    return-void
.end method

.method public setTipOptions(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipOptionsView:Lcom/squareup/ui/activity/TipOptionsView;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/activity/TipOptionsView;->setTipOptions(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public showTipAmount(Ljava/lang/Long;)V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    if-nez p1, :cond_0

    const-string p1, ""

    goto :goto_0

    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/OnDeleteEditText;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipOptionsView:Lcom/squareup/ui/activity/TipOptionsView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/TipOptionsView;->setVisibility(I)V

    .line 119
    iget-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/OnDeleteEditText;->setVisibility(I)V

    return-void
.end method

.method public showTipOptions()V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipOptionsView:Lcom/squareup/ui/activity/TipOptionsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/TipOptionsView;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/OnDeleteEditText;->setVisibility(I)V

    return-void
.end method
