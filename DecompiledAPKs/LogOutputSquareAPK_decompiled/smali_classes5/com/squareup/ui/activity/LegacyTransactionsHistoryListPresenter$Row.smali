.class Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;
.super Ljava/lang/Object;
.source "LegacyTransactionsHistoryListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Row"
.end annotation


# instance fields
.field final bill:Lcom/squareup/billhistory/model/BillHistory;

.field final headerId:I

.field final type:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;Lcom/squareup/billhistory/model/BillHistory;I)V
    .locals 0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->type:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;

    .line 111
    iput-object p2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 112
    iput p3, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Row;->headerId:I

    return-void
.end method
