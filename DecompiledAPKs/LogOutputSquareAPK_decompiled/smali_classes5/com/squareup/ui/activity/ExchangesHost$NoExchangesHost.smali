.class public Lcom/squareup/ui/activity/ExchangesHost$NoExchangesHost;
.super Ljava/lang/Object;
.source "ExchangesHost.java"

# interfaces
.implements Lcom/squareup/ui/activity/ExchangesHost;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/ExchangesHost;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NoExchangesHost"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public exchangesEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getRefundContext()Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onReturnItemsAddedToCart(Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;)V
    .locals 1

    .line 47
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Exchanges are not enabled."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public reset()V
    .locals 2

    .line 51
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Exchanges are not enabled."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
