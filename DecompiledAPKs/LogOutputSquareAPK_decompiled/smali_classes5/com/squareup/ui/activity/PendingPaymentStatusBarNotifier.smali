.class public Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;
.super Ljava/lang/Object;
.source "PendingPaymentStatusBarNotifier.java"

# interfaces
.implements Lcom/squareup/notifications/PendingPaymentNotifier;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier$TransactionsHistoryActivityPendingIntentFactory;
    }
.end annotation


# instance fields
.field private final appContext:Landroid/app/Application;

.field private final clock:Lcom/squareup/util/Clock;

.field private final jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

.field private final notificationManager:Landroid/app/NotificationManager;

.field private final notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

.field private final pendingIntentFactory:Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier$TransactionsHistoryActivityPendingIntentFactory;

.field private final pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Clock;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/notification/NotificationWrapper;Landroid/app/NotificationManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->appContext:Landroid/app/Application;

    .line 45
    iput-object p2, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    .line 46
    iput-object p3, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 47
    iput-object p4, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->clock:Lcom/squareup/util/Clock;

    .line 48
    iput-object p5, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    .line 49
    iput-object p6, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    .line 50
    iput-object p7, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->notificationManager:Landroid/app/NotificationManager;

    .line 51
    new-instance p1, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier$TransactionsHistoryActivityPendingIntentFactory;

    invoke-direct {p1}, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier$TransactionsHistoryActivityPendingIntentFactory;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->pendingIntentFactory:Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier$TransactionsHistoryActivityPendingIntentFactory;

    return-void
.end method


# virtual methods
.method public hideNotification()V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->notificationManager:Landroid/app/NotificationManager;

    sget v1, Lcom/squareup/activity/R$id;->notification_payment:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method public showNotification(Ljava/lang/Integer;)V
    .locals 13

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->appContext:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 57
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 58
    sget v4, Lcom/squareup/cardreader/ui/R$string;->processing_payments_one:I

    goto :goto_1

    :cond_1
    sget v4, Lcom/squareup/cardreader/ui/R$string;->processing_payments_many:I

    .line 61
    :goto_1
    sget v5, Lcom/squareup/activity/R$string;->processing_payments_text:I

    .line 65
    iget-object v6, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    .line 66
    invoke-interface {v6}, Lcom/squareup/payment/pending/PendingTransactionsStore;->oldestStoredTransactionOrPendingCaptureTransactionTimestamp()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v10, v6, v8

    if-eqz v10, :cond_4

    .line 68
    iget-object v8, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v8}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object v8

    .line 69
    invoke-virtual {v8}, Lcom/squareup/settings/server/StoreAndForwardSettings;->getPaymentExpirationMillis()J

    move-result-wide v8

    add-long/2addr v6, v8

    const-wide/32 v8, 0xa4cb800

    sub-long v8, v6, v8

    .line 71
    iget-object v10, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v10}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v10

    cmp-long v12, v10, v8

    if-ltz v12, :cond_3

    if-eqz v1, :cond_2

    .line 72
    sget v1, Lcom/squareup/activity/R$string;->processing_payments_expiring_one:I

    goto :goto_2

    :cond_2
    sget v1, Lcom/squareup/activity/R$string;->processing_payments_expiring_many:I

    :goto_2
    move v4, v1

    .line 75
    sget v5, Lcom/squareup/cardreader/R$string;->processing_payments_expiring_text:I

    const/4 v2, 0x1

    goto :goto_3

    .line 79
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v8, v10

    .line 80
    iget-object v1, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    invoke-static {v6, v7, v8, v9}, Lcom/squareup/payment/pending/PaymentNotifierJobCreator$UpdatePaymentNotifierJob;->updatePaymentNotifierRequest(JJ)Lcom/evernote/android/job/JobRequest;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/squareup/backgroundjob/BackgroundJobManager;->schedule(Lcom/evernote/android/job/JobRequest;)V

    .line 84
    :cond_4
    :goto_3
    iget-object v1, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->appContext:Landroid/app/Application;

    invoke-static {v1, v4}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 85
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const-string v3, "count"

    invoke-virtual {v1, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 86
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 87
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 88
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    iget-object v3, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->appContext:Landroid/app/Application;

    sget-object v4, Lcom/squareup/notification/Channels;->PAYMENTS:Lcom/squareup/notification/Channels;

    .line 91
    invoke-virtual {v1, v3, v4}, Lcom/squareup/notification/NotificationWrapper;->getNotificationBuilder(Landroid/content/Context;Lcom/squareup/notification/Channel;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    .line 92
    invoke-virtual {v1, p1}, Landroidx/core/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    .line 93
    invoke-virtual {p1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    new-instance v1, Landroidx/core/app/NotificationCompat$BigTextStyle;

    invoke-direct {v1}, Landroidx/core/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 94
    invoke-virtual {v1, v0}, Landroidx/core/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$BigTextStyle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setStyle(Landroidx/core/app/NotificationCompat$Style;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->clock:Lcom/squareup/util/Clock;

    .line 95
    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setWhen(J)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->pendingIntentFactory:Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier$TransactionsHistoryActivityPendingIntentFactory;

    iget-object v1, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->appContext:Landroid/app/Application;

    .line 96
    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier$TransactionsHistoryActivityPendingIntentFactory;->createPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    if-eqz v2, :cond_5

    const/4 v0, 0x2

    .line 98
    invoke-virtual {p1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setPriority(I)Landroidx/core/app/NotificationCompat$Builder;

    .line 101
    :cond_5
    invoke-virtual {p1}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    .line 102
    iget v0, p1, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x22

    iput v0, p1, Landroid/app/Notification;->flags:I

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;->notificationManager:Landroid/app/NotificationManager;

    sget v1, Lcom/squareup/activity/R$id;->notification_payment:I

    invoke-virtual {v0, v1, p1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method
