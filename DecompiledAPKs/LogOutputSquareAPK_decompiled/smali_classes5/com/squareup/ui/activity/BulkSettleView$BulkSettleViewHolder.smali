.class public abstract Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "BulkSettleView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/BulkSettleView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BulkSettleViewHolder"
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method abstract onBind(I)V
.end method

.method onKeyboardMovedFromPreviousRow()V
    .locals 0

    return-void
.end method

.method onViewRecycled()V
    .locals 0

    return-void
.end method
