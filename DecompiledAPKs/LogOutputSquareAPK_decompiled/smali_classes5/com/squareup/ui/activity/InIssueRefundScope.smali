.class public abstract Lcom/squareup/ui/activity/InIssueRefundScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InIssueRefundScope.java"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# instance fields
.field protected final parentKey:Lcom/squareup/ui/activity/IssueRefundScope;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/IssueRefundScope;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/ui/activity/InIssueRefundScope;->parentKey:Lcom/squareup/ui/activity/IssueRefundScope;

    return-void
.end method


# virtual methods
.method public final getParentKey()Ljava/lang/Object;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/ui/activity/InIssueRefundScope;->parentKey:Lcom/squareup/ui/activity/IssueRefundScope;

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
