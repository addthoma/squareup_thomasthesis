.class public Lcom/squareup/ui/activity/BulkSettleRunner;
.super Ljava/lang/Object;
.source "BulkSettleRunner.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/ui/activity/TendersAwaitingTipLoader$Listener;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private datasetChanged:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private final internetState:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/connectivity/InternetState;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private settleInFlight:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final settleTipConnectivityUtils:Lcom/squareup/ui/activity/SettleTipConnectivityUtils;

.field final settleableTenders:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/ui/activity/TenderEditState;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderSettler:Lcom/squareup/papersignature/TenderTipSettler;

.field private final tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

.field private final tipCountScheduler:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/TendersAwaitingTipLoader;Lcom/squareup/papersignature/TenderTipSettler;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/activity/SettleTipConnectivityUtils;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleInFlight:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 55
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->datasetChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    .line 64
    iput-object p2, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->tenderSettler:Lcom/squareup/papersignature/TenderTipSettler;

    .line 65
    iput-object p3, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->flow:Lflow/Flow;

    .line 66
    iput-object p4, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->res:Lcom/squareup/util/Res;

    .line 67
    iput-object p5, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->analytics:Lcom/squareup/analytics/Analytics;

    .line 68
    iput-object p6, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleTipConnectivityUtils:Lcom/squareup/ui/activity/SettleTipConnectivityUtils;

    .line 69
    invoke-interface {p7}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->internetState:Lio/reactivex/Observable;

    .line 70
    iput-object p8, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->tipCountScheduler:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    .line 72
    new-instance p2, Ljava/util/LinkedHashSet;

    invoke-direct {p2}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleableTenders:Ljava/util/Set;

    .line 74
    invoke-virtual {p1, p0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->setListener(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$Listener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/BulkSettleRunner;)Lflow/Flow;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->flow:Lflow/Flow;

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/connectivity/InternetState;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 87
    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private logSettleClicked(Ljava/util/Collection;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/ui/activity/TenderEditState;",
            ">;)V"
        }
    .end annotation

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->hasSearchQuery()Z

    move-result v2

    .line 221
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v3

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getTenderCount()I

    move-result v0

    sub-int v4, v0, v3

    .line 225
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v7, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/activity/TenderEditState;

    .line 226
    iget-boolean v1, v1, Lcom/squareup/ui/activity/TenderEditState;->wasQuickTipUsed:Z

    if-eqz v1, :cond_0

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 231
    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p1

    sub-int v6, p1, v7

    .line 232
    iget-object p1, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getSortField()Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    move-result-object p1

    iget-object v8, p1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->nameForAnalytics:Ljava/lang/String;

    .line 234
    iget-object p1, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/papersignature/analytics/SettledTipsFromBatchViewEvent;

    const/4 v5, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/papersignature/analytics/SettledTipsFromBatchViewEvent;-><init>(ZIIIIILjava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private pruneTendersWithTip()V
    .locals 4

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getTenderIds()Ljava/util/Set;

    move-result-object v0

    .line 158
    new-instance v1, Ljava/util/LinkedHashSet;

    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleableTenders:Ljava/util/Set;

    invoke-direct {v1, v2}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/activity/TenderEditState;

    .line 159
    iget-object v3, v2, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    iget-object v3, v3, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 160
    iget-object v3, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleableTenders:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private settleTips(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/ui/activity/TenderEditState;",
            ">;)V"
        }
    .end annotation

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getLoadState()Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->LOADED:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    if-ne v0, v1, :cond_2

    .line 173
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 177
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/BulkSettleRunner;->logSettleClicked(Ljava/util/Collection;)V

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 180
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/activity/TenderEditState;

    .line 181
    iget-object v2, v2, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 184
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleInFlight:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 185
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->tenderSettler:Lcom/squareup/papersignature/TenderTipSettler;

    new-instance v2, Lcom/squareup/ui/activity/BulkSettleRunner$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/activity/BulkSettleRunner$1;-><init>(Lcom/squareup/ui/activity/BulkSettleRunner;Ljava/util/Collection;)V

    invoke-virtual {v1, v0, v2}, Lcom/squareup/papersignature/TenderTipSettler;->settleTips(Ljava/util/Collection;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    return-void

    .line 174
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Trying to settle 0 tips."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 170
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot settle tips in state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    .line 171
    invoke-virtual {v1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getLoadState()Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private showFinishWithoutSettlingPopup(I)V
    .locals 5

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->continue_label:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 247
    iget-object p1, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->bulk_settle_finish_unsettled_title_one:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 248
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->bulk_settle_finish_unsettled_body_one:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 249
    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->bulk_settle_finish_unsettled_discard_tips_one:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 251
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->bulk_settle_finish_unsettled_title_many:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 252
    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->bulk_settle_finish_unsettled_body_many:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "count"

    .line 253
    invoke-virtual {v2, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 254
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 255
    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->bulk_settle_finish_unsettled_discard_tips_many:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v4, v1

    move-object v1, p1

    move-object p1, v4

    .line 259
    :goto_0
    invoke-static {p1, v1, v2, v0}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p1

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen;

    invoke-direct {v1, p1}, Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen;-><init>(Lcom/squareup/register/widgets/FailureAlertDialogFactory;)V

    sget-object p1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, p1}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method private showSettleFailedPopup(I)V
    .locals 4

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->paper_signature_settle_failed_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 205
    iget-object p1, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->bulk_settle_settle_failed_body_one:I

    .line 206
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->bulk_settle_settle_failed_body_many:I

    .line 207
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "count"

    .line 208
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 209
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 210
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->retry:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 211
    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 214
    invoke-static {v0, p1, v2, v1}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p1

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/BulkTipSettlementFailedDialogScreen;

    invoke-direct {v1, p1}, Lcom/squareup/ui/activity/BulkTipSettlementFailedDialogScreen;-><init>(Lcom/squareup/register/widgets/FailureAlertDialogFactory;)V

    sget-object p1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, p1}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method


# virtual methods
.method public addTender(Lcom/squareup/ui/activity/TenderEditState;)V
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleableTenders:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public datasetChanged()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->datasetChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public isSettleInFlight()Ljava/lang/Boolean;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleInFlight:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public synthetic lambda$onEnterScope$1$BulkSettleRunner(Lcom/squareup/connectivity/InternetState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->load()V

    return-void
.end method

.method public numSettleableTenders()I
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleableTenders:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public onBackPressed()V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleableTenders:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleableTenders:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/BulkSettleRunner;->showFinishWithoutSettlingPopup(I)V

    goto :goto_0

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    :goto_0
    return-void
.end method

.method public onCloseWithoutSettlingPopupResult(Ljava/lang/Boolean;)V
    .locals 3

    .line 145
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 146
    iget-object p1, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->flow:Lflow/Flow;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/squareup/ui/activity/BulkSettleScreen;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    :cond_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->internetState:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/ui/activity/-$$Lambda$BulkSettleRunner$beiorHP7HP1UNfvlDvNT2s5CWFE;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$BulkSettleRunner$beiorHP7HP1UNfvlDvNT2s5CWFE;

    .line 87
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$BulkSettleRunner$fnRJlnHXnMr-_bojPtAv0ueIA_U;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$BulkSettleRunner$fnRJlnHXnMr-_bojPtAv0ueIA_U;-><init>(Lcom/squareup/ui/activity/BulkSettleRunner;)V

    .line 88
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 85
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->tipCountScheduler:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    invoke-virtual {p1}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->requestImmediateUpdate()V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoaderStateChanged()V
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getLoadState()Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->LOADED:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    if-ne v0, v1, :cond_0

    .line 79
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettleRunner;->pruneTendersWithTip()V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->datasetChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onSettleClicked()V
    .locals 4

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleTipConnectivityUtils:Lcom/squareup/ui/activity/SettleTipConnectivityUtils;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/SettleTipConnectivityUtils;->isOffline()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleTipConnectivityUtils:Lcom/squareup/ui/activity/SettleTipConnectivityUtils;

    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleableTenders:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v3}, Lcom/squareup/ui/activity/SettleTipConnectivityUtils;->getNoInternetPopupScreen(Z)Lcom/squareup/register/widgets/WarningDialogScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleableTenders:Ljava/util/Set;

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/BulkSettleRunner;->settleTips(Ljava/util/Collection;)V

    return-void
.end method

.method onSettleFailed(I)V
    .locals 2

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleInFlight:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 200
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/BulkSettleRunner;->showSettleFailedPopup(I)V

    return-void
.end method

.method public onTipSettlementFailedPopupResult(Ljava/lang/Boolean;)V
    .locals 1

    .line 139
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettleRunner;->onSettleClicked()V

    :cond_0
    return-void
.end method

.method public removeTender(Lcom/squareup/ui/activity/TenderEditState;)V
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleableTenders:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public settleInFlight()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleRunner;->settleInFlight:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method
