.class public final Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "SelectGiftReceiptTenderScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen$Component;
    }
.end annotation


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 51
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 41
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public screenLayout()I
    .locals 1

    .line 56
    sget v0, Lcom/squareup/billhistoryui/R$layout;->activity_applet_select_gift_receipt_tender_view:I

    return v0
.end method
