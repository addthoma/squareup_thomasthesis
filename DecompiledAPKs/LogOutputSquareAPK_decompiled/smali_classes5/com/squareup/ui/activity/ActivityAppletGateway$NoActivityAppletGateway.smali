.class public final Lcom/squareup/ui/activity/ActivityAppletGateway$NoActivityAppletGateway;
.super Ljava/lang/Object;
.source "ActivityAppletGateway.kt"

# interfaces
.implements Lcom/squareup/ui/activity/ActivityAppletGateway;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/ActivityAppletGateway;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoActivityAppletGateway"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0010\u0010\u0008\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0008\u0010\t\u001a\u00020\u0004H\u0002\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/activity/ActivityAppletGateway$NoActivityAppletGateway;",
        "Lcom/squareup/ui/activity/ActivityAppletGateway;",
        "()V",
        "activateInitialScreen",
        "",
        "goToDisallowInventoryApiDialog",
        "issueRefundScope",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "goToRestockOnItemizedRefundScreen",
        "noActivityAppletAccess",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final noActivityAppletAccess()V
    .locals 2

    .line 48
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot launch the Activity Applet from device without an Activity Applet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public activateInitialScreen()V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/ui/activity/ActivityAppletGateway$NoActivityAppletGateway;->noActivityAppletAccess()V

    return-void
.end method

.method public goToDisallowInventoryApiDialog(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string v0, "issueRefundScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Lcom/squareup/ui/activity/ActivityAppletGateway$NoActivityAppletGateway;->noActivityAppletAccess()V

    return-void
.end method

.method public goToRestockOnItemizedRefundScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string v0, "issueRefundScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/activity/ActivityAppletGateway$NoActivityAppletGateway;->noActivityAppletAccess()V

    return-void
.end method
