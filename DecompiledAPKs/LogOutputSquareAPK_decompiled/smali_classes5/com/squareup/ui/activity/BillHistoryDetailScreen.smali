.class public final Lcom/squareup/ui/activity/BillHistoryDetailScreen;
.super Lcom/squareup/ui/activity/InActivityAppletScope;
.source "BillHistoryDetailScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/activity/BillHistoryDetailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/BillHistoryDetailScreen$Component;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/activity/BillHistoryDetailScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/ui/activity/BillHistoryDetailScreen;

    invoke-direct {v0}, Lcom/squareup/ui/activity/BillHistoryDetailScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/activity/BillHistoryDetailScreen;->INSTANCE:Lcom/squareup/ui/activity/BillHistoryDetailScreen;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/ui/activity/InActivityAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->TRANSACTION_DETAIL:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public screenLayout()I
    .locals 1

    .line 28
    sget v0, Lcom/squareup/billhistoryui/R$layout;->activity_applet_bill_history_detail_view:I

    return v0
.end method
