.class public final Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;
.super Ljava/lang/Object;
.source "CardPresentRefundMessageResources.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007J\u0008\u0010\u0005\u001a\u00020\u0004H\u0007J\u0008\u0010\u0006\u001a\u00020\u0004H\u0007J\u0008\u0010\u0007\u001a\u00020\u0004H\u0007J\u0008\u0010\u0008\u001a\u00020\u0004H\u0007J\u0008\u0010\t\u001a\u00020\u0004H\u0007J\u0008\u0010\n\u001a\u00020\u0004H\u0007J\u0008\u0010\u000b\u001a\u00020\u0004H\u0007J\u0008\u0010\u000c\u001a\u00020\u0004H\u0007J\u001a\u0010\r\u001a\u00020\u00042\u0008\u0008\u0001\u0010\u000e\u001a\u00020\u000f2\u0008\u0008\u0003\u0010\u0010\u001a\u00020\u000f\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;",
        "",
        "()V",
        "empty",
        "Lcom/squareup/ui/activity/CardPresentRefundMessageResources;",
        "fatalError",
        "ofActionRequired",
        "ofNfcCollision",
        "ofPleaseTapCard",
        "ofRemoveCard",
        "ofTapAgain",
        "ofTapNow",
        "ofUnlockDevice",
        "retryableError",
        "titleId",
        "",
        "messageId",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;-><init>()V

    return-void
.end method

.method public static synthetic retryableError$default(Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;IIILjava/lang/Object;)Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 33
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->retryableError(II)Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final empty()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    .line 28
    sget-object v1, Lcom/squareup/ui/activity/CardPresentRefundErrorState;->NONE:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    const/4 v2, 0x0

    .line 25
    invoke-direct {v0, v2, v2, v1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;-><init>(IILcom/squareup/ui/activity/CardPresentRefundErrorState;)V

    return-object v0
.end method

.method public final fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 4
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    .line 42
    sget v1, Lcom/squareup/cardreader/R$string;->cannot_process_refund_title:I

    .line 43
    sget v2, Lcom/squareup/cardreader/R$string;->contactless_interface_unavailable_message:I

    .line 44
    sget-object v3, Lcom/squareup/ui/activity/CardPresentRefundErrorState;->FATAL:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    .line 41
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;-><init>(IILcom/squareup/ui/activity/CardPresentRefundErrorState;)V

    return-object v0
.end method

.method public final ofActionRequired()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 48
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    .line 49
    sget v1, Lcom/squareup/cardreader/R$string;->contactless_action_required_title:I

    .line 50
    sget v2, Lcom/squareup/cardreader/R$string;->contactless_action_required_message:I

    .line 48
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->retryableError(II)Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    return-object v0
.end method

.method public final ofNfcCollision()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 66
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    .line 67
    sget v1, Lcom/squareup/cardreader/R$string;->contactless_one_card_title:I

    .line 68
    sget v2, Lcom/squareup/cardreader/R$string;->contactless_one_card_message:I

    .line 66
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->retryableError(II)Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    return-object v0
.end method

.method public final ofPleaseTapCard()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 80
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    .line 81
    sget v1, Lcom/squareup/cardreader/R$string;->please_remove_card_title:I

    .line 82
    sget v2, Lcom/squareup/cardreader/R$string;->emv_request_tap_refund:I

    .line 80
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->retryableError(II)Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    return-object v0
.end method

.method public final ofRemoveCard()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 5
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 75
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    .line 76
    sget v1, Lcom/squareup/cardreader/R$string;->please_remove_card_title:I

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    .line 75
    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->retryableError$default(Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;IIILjava/lang/Object;)Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    return-object v0
.end method

.method public final ofTapAgain()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 60
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    .line 61
    sget v1, Lcom/squareup/cardreader/R$string;->contactless_tap_again_title:I

    .line 62
    sget v2, Lcom/squareup/cardreader/R$string;->contactless_tap_again_message:I

    .line 60
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->retryableError(II)Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    return-object v0
.end method

.method public final ofTapNow()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 72
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->ofTapAgain()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    return-object v0
.end method

.method public final ofUnlockDevice()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 54
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    .line 55
    sget v1, Lcom/squareup/cardreader/R$string;->contactless_unlock_phone_to_pay_title:I

    .line 56
    sget v2, Lcom/squareup/cardreader/R$string;->contactless_unlock_phone_and_try_again_message:I

    .line 54
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->retryableError(II)Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    return-object v0
.end method

.method public final retryableError(II)Lcom/squareup/ui/activity/CardPresentRefundMessageResources;
    .locals 2

    .line 34
    new-instance v0, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    .line 37
    sget-object v1, Lcom/squareup/ui/activity/CardPresentRefundErrorState;->RETRY:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    .line 34
    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;-><init>(IILcom/squareup/ui/activity/CardPresentRefundErrorState;)V

    return-object v0
.end method
