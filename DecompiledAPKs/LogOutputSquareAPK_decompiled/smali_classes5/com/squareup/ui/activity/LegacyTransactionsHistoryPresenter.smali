.class public Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;
.super Lmortar/ViewPresenter;
.source "LegacyTransactionsHistoryPresenter.java"

# interfaces
.implements Lcom/squareup/ui/activity/ShowFullHistoryPermissionController$OnFullHistoryPermissionGrantedListener;
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;
.implements Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;
.implements Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/activity/TransactionsHistoryView;",
        ">;",
        "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController$OnFullHistoryPermissionGrantedListener;",
        "Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;",
        "Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;",
        "Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;"
    }
.end annotation


# instance fields
.field private final activitySearchPaymentStarter:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final authenticator:Lcom/squareup/account/LegacyAuthenticator;

.field private final badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

.field private final bulkSettleButtonPresenter:Lcom/squareup/ui/activity/BulkSettleButtonPresenter;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field private contactlessReaderSearching:Z

.field private currentSearchQuery:Ljava/lang/String;

.field private final device:Lcom/squareup/util/Device;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final internetState:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/connectivity/InternetState;",
            ">;"
        }
    .end annotation
.end field

.field private final legacyListPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

.field private final legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final showFullHistoryPermission:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

.field private final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Lcom/squareup/util/Res;Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;Lcom/squareup/ui/activity/BulkSettleButtonPresenter;Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/cardreader/CardReaderHubUtils;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 95
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    move-object v1, p1

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p2

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    move-object v1, p3

    .line 98
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    move-object v1, p4

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    move-object v1, p5

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p6

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    move-object v1, p7

    .line 102
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->features:Lcom/squareup/settings/server/Features;

    move-object v1, p8

    .line 103
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->device:Lcom/squareup/util/Device;

    move-object v1, p9

    .line 104
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyListPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    move-object v1, p10

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    move-object v1, p11

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showFullHistoryPermission:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    move-object v1, p12

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->bulkSettleButtonPresenter:Lcom/squareup/ui/activity/BulkSettleButtonPresenter;

    move-object v1, p13

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->activitySearchPaymentStarter:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;

    move-object/from16 v1, p14

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 110
    invoke-interface/range {p15 .. p15}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->internetState:Lio/reactivex/Observable;

    move-object/from16 v1, p16

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-object/from16 v1, p17

    .line 112
    iput-object v1, v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    return-void
.end method

.method private cardSearchRequested(Lcom/squareup/Card;)V
    .locals 1

    .line 317
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->setQuery(Lcom/squareup/Card;)V

    .line 318
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->doSearch()V

    return-void
.end method

.method private cardSearchRequestedWithCardData(Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;)V
    .locals 1

    .line 312
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->setQueryWithInstrumentSearch(Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;)V

    .line 313
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->doSearch()V

    return-void
.end method

.method private doSearch()V
    .locals 2

    .line 322
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ACTIVITY_SEARCH:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 323
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->hasQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->load()V

    .line 327
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateSearchQuery()V

    .line 328
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateHistory()V

    return-void
.end method

.method private handleNoPreviousTransactions(Z)V
    .locals 2

    .line 401
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showNoPreviousTransactions()V

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    .line 404
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateSearchPrompts()V

    goto :goto_0

    .line 406
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showDeprecatedNoPreviousTransactions()V

    :goto_0
    return-void
.end method

.method private handleNoSearchResults()V
    .locals 2

    .line 393
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showNoSearchResults()V

    goto :goto_0

    .line 396
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showDeprecatedNoSearchResults()V

    :goto_0
    return-void
.end method

.method private handleSearchStopped()V
    .locals 1

    const/4 v0, 0x0

    .line 268
    iput-boolean v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->contactlessReaderSearching:Z

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->activitySearchPaymentStarter:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;

    invoke-interface {v0}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;->disableContactlessField()V

    .line 270
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateSearchPrompts()V

    return-void
.end method

.method private handleSwipe(Lcom/squareup/Card;)V
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showFullHistoryPermission:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->isPermissionGranted()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 199
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/Card;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 204
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lflow/Flow;->get(Landroid/view/View;)Lflow/Flow;

    move-result-object v0

    invoke-virtual {v0}, Lflow/Flow;->getHistory()Lflow/History;

    move-result-object v0

    .line 205
    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/activity/IssueRefundScreen;

    if-nez v0, :cond_2

    .line 206
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->cardSearchRequested(Lcom/squareup/Card;)V

    :cond_2
    return-void
.end method

.method public static synthetic lambda$FbuKi4njRy2sIh-vYht-1l34Exk(Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;Lcom/squareup/connectivity/InternetState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->onInternetStateChange(Lcom/squareup/connectivity/InternetState;)V

    return-void
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/connectivity/InternetState;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 122
    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private onInternetStateChange(Lcom/squareup/connectivity/InternetState;)V
    .locals 2

    .line 177
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    sget-object v1, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne p1, v1, :cond_0

    sget v1, Lcom/squareup/billhistoryui/R$string;->search_transactions_history_hint:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/billhistoryui/R$string;->search_transactions_history_hint_offline:I

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->setSearchBarHint(I)V

    .line 180
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    sget-object v1, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne p1, v1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->setSearchBarEnabled(Z)V

    return-void
.end method

.method private showDeprecatedNoPreviousTransactions()V
    .locals 4

    .line 499
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    if-eqz v0, :cond_0

    .line 501
    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->activity_applet_no_payments_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->activity_applet_no_payments_message:I

    .line 502
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 501
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showMessageCenter(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    invoke-virtual {v0}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showDeprecatedSupportCenterButton()V

    :cond_0
    return-void
.end method

.method private showDeprecatedNoSearchResults()V
    .locals 3

    .line 425
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    if-eqz v0, :cond_0

    .line 427
    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_no_payments_title_deprecated:I

    .line 428
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    .line 427
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showDeprecatedTopMessage(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private showDeprecatedSearchPrompts(ZZZZZ)V
    .locals 3

    .line 572
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v1, ""

    if-nez p1, :cond_1

    .line 579
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_none:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showDeprecatedTopMessage(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 583
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isBranReady()Z

    move-result p1

    const/4 v2, 0x1

    if-eqz p1, :cond_3

    if-eqz p5, :cond_2

    .line 585
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_x2_swipe_dip_tap:I

    .line 586
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/billhistoryui/R$string;->activity_applet_tap_enabled:I

    .line 587
    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 585
    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showDeprecatedTopMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 589
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_x2_swipe_dip:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/billhistoryui/R$string;->activity_applet_tap_disabled:I

    .line 590
    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 589
    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showDeprecatedTopMessage(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    xor-int/lit8 p1, p5, 0x1

    .line 592
    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->setPromptEnabled(Z)V

    return-void

    :cond_3
    if-nez p4, :cond_7

    if-nez p2, :cond_4

    if-nez p3, :cond_4

    .line 599
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_none:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showDeprecatedTopMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_4
    if-eqz p2, :cond_5

    if-nez p3, :cond_5

    .line 603
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_swipe:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showDeprecatedTopMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    if-nez p2, :cond_6

    if-eqz p3, :cond_6

    .line 607
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_dip:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showDeprecatedTopMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_6
    if-eqz p2, :cond_b

    if-eqz p3, :cond_b

    .line 611
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_swipe_dip:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showDeprecatedTopMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    xor-int/lit8 p1, p5, 0x1

    .line 614
    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->setPromptEnabled(Z)V

    if-nez p2, :cond_8

    if-eqz p3, :cond_8

    if-eqz p5, :cond_8

    .line 617
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_dip_tap:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/billhistoryui/R$string;->activity_applet_tap_enabled:I

    .line 618
    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 617
    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showDeprecatedTopMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    if-nez p2, :cond_9

    if-eqz p3, :cond_9

    if-nez p5, :cond_9

    .line 622
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_dip:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/billhistoryui/R$string;->activity_applet_tap_disabled:I

    .line 623
    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 622
    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showDeprecatedTopMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_9
    if-eqz p2, :cond_a

    if-eqz p3, :cond_a

    if-eqz p5, :cond_a

    .line 627
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_swipe_dip_tap:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/billhistoryui/R$string;->activity_applet_tap_enabled:I

    .line 628
    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 627
    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showDeprecatedTopMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_a
    if-eqz p2, :cond_c

    if-eqz p3, :cond_c

    if-nez p5, :cond_c

    .line 632
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_swipe_dip:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/billhistoryui/R$string;->activity_applet_tap_disabled:I

    .line 633
    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 632
    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showDeprecatedTopMessage(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    :goto_1
    return-void

    .line 638
    :cond_c
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 640
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    aput-object p2, v0, v1

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    aput-object p2, v0, v2

    const/4 p2, 0x2

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    aput-object p3, v0, p2

    const/4 p2, 0x3

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    aput-object p3, v0, p2

    const-string p2, "Invalid search HW combination, swipe=%b/dip=%b/tap=%b/tapEnabled=%b"

    .line 639
    invoke-static {p2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private showError(Lcom/squareup/activity/LoaderError;)V
    .locals 2

    .line 441
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    if-eqz v0, :cond_0

    .line 443
    iget-object v1, p1, Lcom/squareup/activity/LoaderError;->title:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/activity/LoaderError;->message:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showMessageCenter(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private showLoading()V
    .locals 1

    .line 411
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    if-eqz v0, :cond_0

    .line 413
    invoke-virtual {v0}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showLoading()V

    :cond_0
    return-void
.end method

.method private showNoPreviousTransactions()V
    .locals 4

    .line 490
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    if-eqz v0, :cond_0

    .line 492
    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->activity_applet_no_previous_transactions_title:I

    .line 493
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->activity_applet_no_previous_transactions_message:I

    .line 494
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 492
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showTopMessagePanel(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private showNoSearchResults()V
    .locals 4

    .line 433
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    if-eqz v0, :cond_0

    .line 435
    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_no_payments_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_no_payments_message:I

    .line 436
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 435
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showTopMessagePanel(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private showSearchPrompts(ZZZ)V
    .locals 1

    .line 555
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    .line 561
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_without_card_reader:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showTopMessagePanel(Ljava/lang/String;)V

    goto :goto_1

    .line 562
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isBranReady()Z

    move-result p1

    if-nez p1, :cond_3

    if-eqz p2, :cond_2

    goto :goto_0

    .line 566
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_with_card_reader:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showTopMessagePanel(Ljava/lang/String;)V

    goto :goto_1

    .line 563
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/billhistoryui/R$string;->activity_applet_search_with_card_reader:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, p3}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showTopMessagePanel(Ljava/lang/String;Z)V

    :goto_1
    return-void
.end method

.method private showTransactionsHistory()V
    .locals 1

    .line 418
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    if-eqz v0, :cond_0

    .line 420
    invoke-virtual {v0}, Lcom/squareup/ui/activity/TransactionsHistoryView;->showTransactionsHistory()V

    :cond_0
    return-void
.end method

.method private textSearchRequested(Ljava/lang/String;)V
    .locals 2

    .line 301
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->setQuery(Ljava/lang/String;)V

    .line 302
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->doSearch()V

    .line 304
    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateSearchPrompts()V

    goto :goto_0

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/TransactionsHistorySearchEvent;

    invoke-direct {v1, p1}, Lcom/squareup/log/TransactionsHistorySearchEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :goto_0
    return-void
.end method

.method private updateHistory()V
    .locals 5

    .line 356
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->getBills()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    .line 357
    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v2}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v2

    xor-int/2addr v2, v1

    .line 359
    sget-object v3, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter$1;->$SwitchMap$com$squareup$activity$LoaderState:[I

    iget-object v4, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v4}, Lcom/squareup/activity/LegacyTransactionsHistory;->getState()Lcom/squareup/activity/LoaderState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/activity/LoaderState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    if-eq v3, v1, :cond_8

    const/4 v1, 0x2

    if-eq v3, v1, :cond_6

    const/4 v1, 0x3

    if-eq v3, v1, :cond_3

    const/4 v1, 0x4

    if-ne v3, v1, :cond_2

    if-nez v0, :cond_0

    if-eqz v2, :cond_1

    .line 381
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->hasQuery()Z

    move-result v0

    if-nez v0, :cond_1

    .line 382
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showTransactionsHistory()V

    goto :goto_1

    .line 384
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->getLastError()Lcom/squareup/activity/LoaderError;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showError(Lcom/squareup/activity/LoaderError;)V

    goto :goto_1

    .line 388
    :cond_2
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v2}, Lcom/squareup/activity/LegacyTransactionsHistory;->getState()Lcom/squareup/activity/LoaderState;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_3
    if-eqz v0, :cond_4

    .line 372
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showTransactionsHistory()V

    goto :goto_0

    .line 373
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->hasQuery()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 374
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleNoSearchResults()V

    goto :goto_0

    .line 376
    :cond_5
    invoke-direct {p0, v2}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleNoPreviousTransactions(Z)V

    .line 378
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->activitySearchCardRead()Z

    goto :goto_1

    :cond_6
    if-eqz v0, :cond_7

    .line 365
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showTransactionsHistory()V

    goto :goto_1

    .line 367
    :cond_7
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showLoading()V

    goto :goto_1

    .line 361
    :cond_8
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showLoading()V

    :goto_1
    return-void
.end method

.method private updateSearchBarVisibility()V
    .locals 2

    .line 332
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showFullHistoryPermission:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    invoke-virtual {v1}, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->isPermissionGranted()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->setSearchBarVisible(Z)V

    return-void
.end method


# virtual methods
.method public dropView(Lcom/squareup/ui/activity/TransactionsHistoryView;)V
    .locals 1

    .line 168
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/CardReaderHub;->removeCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showFullHistoryPermission:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->removeOnPermissionGrantedListener(Lcom/squareup/ui/activity/ShowFullHistoryPermissionController$OnFullHistoryPermissionGrantedListener;)V

    .line 173
    :cond_0
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 61
    check-cast p1, Lcom/squareup/ui/activity/TransactionsHistoryView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->dropView(Lcom/squareup/ui/activity/TransactionsHistoryView;)V

    return-void
.end method

.method getDeprecatedHintTextResId()I
    .locals 1

    .line 480
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isBranReady()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/squareup/billhistoryui/R$string;->search_history_x2_hint:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/billhistoryui/R$string;->search_history_hint_deprecated:I

    :goto_0
    return v0
.end method

.method public handleActionRequired()V
    .locals 2

    .line 656
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSearchStopped()V

    .line 657
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_INSERT_OR_SWIPE:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method public handleCardBlocked()V
    .locals 2

    .line 684
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSearchStopped()V

    .line 685
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_INSERT_OR_SWIPE:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method public handleCardDeclined()V
    .locals 2

    .line 679
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSearchStopped()V

    .line 680
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_INSERT_OR_SWIPE:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method public handleCardTapAgain()V
    .locals 2

    .line 689
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSearchStopped()V

    .line 690
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_CARD_TAP_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method public handleCollisionDetected()V
    .locals 2

    .line 694
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSearchStopped()V

    .line 695
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_COLLISION:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method public handleInterfaceUnavailable()V
    .locals 2

    .line 646
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSearchStopped()V

    .line 647
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_INSERT_OR_SWIPE:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method public handleLimitExceededInsertCard()V
    .locals 2

    .line 704
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSearchStopped()V

    .line 705
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_INSERT_OR_SWIPE:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method public handleLimitExceededTryAnotherCard()V
    .locals 2

    .line 699
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSearchStopped()V

    .line 700
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_INSERT_OR_SWIPE:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method public handleOnRequestTapCard()V
    .locals 2

    .line 709
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSearchStopped()V

    .line 710
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_REQUEST_TAP:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method public handleProcessingError()V
    .locals 2

    .line 666
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSearchStopped()V

    .line 670
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHubUtils;->isCardInsertedOnAnyContactlessReader()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 671
    iput-boolean v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->contactlessReaderSearching:Z

    .line 672
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->activitySearchPaymentStarter:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;

    invoke-interface {v0}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;->enableContactlessField()V

    .line 675
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_INSERT_OR_SWIPE:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method public handleTryAnotherCard()V
    .locals 2

    .line 651
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSearchStopped()V

    .line 652
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_INSERT_OR_SWIPE:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method public handleUnlockDevice()V
    .locals 2

    .line 661
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSearchStopped()V

    .line 662
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_NFC_UNLOCK_PAYMENT_DEVICE:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method public synthetic lambda$null$3$LegacyTransactionsHistoryPresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 162
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateSearchQuery()V

    .line 163
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateHistory()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$LegacyTransactionsHistoryPresenter(Lcom/squareup/connectivity/InternetState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 124
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    invoke-interface {p1}, Lcom/squareup/account/LegacyAuthenticator;->isLoggedIn()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 125
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->getBills()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 126
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->load()V

    .line 128
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyListPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->queryInstantDeposits()V

    :cond_1
    return-void
.end method

.method public synthetic lambda$onLoad$2$LegacyTransactionsHistoryPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->internetState:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryPresenter$FbuKi4njRy2sIh-vYht-1l34Exk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryPresenter$FbuKi4njRy2sIh-vYht-1l34Exk;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$4$LegacyTransactionsHistoryPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->onChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryPresenter$KuQYw_Nqb5R-SjL7kJXKrBYEi4Q;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryPresenter$KuQYw_Nqb5R-SjL7kJXKrBYEi4Q;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;)V

    .line 161
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public onCardError()V
    .locals 2

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_REQUEST_SWIPE:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    .line 232
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateHistory()V

    return-void
.end method

.method public onCardInserted()V
    .locals 1

    .line 216
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TransactionsHistoryView;->hideSoftKeyboard()V

    .line 217
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showLoading()V

    return-void
.end method

.method public onCardMustBeReInserted()V
    .locals 2

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->ACTIVITY_SEARCH_MUST_REINSERT_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    .line 248
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateHistory()V

    return-void
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    .line 508
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateHistory()V

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    .line 512
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateHistory()V

    return-void
.end method

.method public onCardRemoved()V
    .locals 0

    .line 221
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateHistory()V

    return-void
.end method

.method public onCardRemovedDuringPayment()V
    .locals 2

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->TRY_INSERTING_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    .line 242
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateHistory()V

    return-void
.end method

.method onDeprecatedSupportCenterClicked()V
    .locals 2

    .line 486
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TransactionsHistoryView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/SupportUrlSettings;->getHelpCenterUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method onEnableTapClicked()V
    .locals 2

    .line 448
    iget-boolean v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->contactlessReaderSearching:Z

    if-eqz v0, :cond_0

    return-void

    .line 452
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHubUtils;->isCardInsertedOnAnyContactlessReader()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 453
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->MUST_REMOVE_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    .line 455
    iput-boolean v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->contactlessReaderSearching:Z

    .line 456
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->activitySearchPaymentStarter:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;

    invoke-interface {v0}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;->enableContactlessField()V

    :goto_0
    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->successfulSwipes()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$pwqXSmiKQokRAsAy9F1eP03sBXs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$pwqXSmiKQokRAsAy9F1eP03sBXs;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->activitySearchPaymentStarter:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;

    invoke-interface {v0, p1, p0, p0}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;->register(Lmortar/MortarScope;Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->internetState:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    .line 121
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryPresenter$wcdIfNBHKYt2N_RXMpeTGTJrjFo;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryPresenter$wcdIfNBHKYt2N_RXMpeTGTJrjFo;

    .line 122
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryPresenter$KOnyqXc82CHpjROg8IXYV5TmaUE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryPresenter$KOnyqXc82CHpjROg8IXYV5TmaUE;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;)V

    .line 123
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 119
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onFieldTimeout()V
    .locals 0

    .line 257
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSearchStopped()V

    return-void
.end method

.method public onInstrumentSearch(Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;)V
    .locals 1

    const/4 v0, 0x0

    .line 225
    iput-boolean v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->contactlessReaderSearching:Z

    .line 226
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->cardSearchRequestedWithCardData(Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 134
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/TransactionsHistoryView;

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showFullHistoryPermission:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->addOnPermissionGrantedListener(Lcom/squareup/ui/activity/ShowFullHistoryPermissionController$OnFullHistoryPermissionGrantedListener;)V

    .line 138
    invoke-virtual {p1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 139
    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->bulkSettleButtonPresenter:Lcom/squareup/ui/activity/BulkSettleButtonPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->shouldShowButton()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 140
    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->bulkSettleButtonPresenter:Lcom/squareup/ui/activity/BulkSettleButtonPresenter;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/activity/-$$Lambda$1-Du_de1or-pv5zdu7W3cVSdqEs;

    invoke-direct {v2, v1}, Lcom/squareup/ui/activity/-$$Lambda$1-Du_de1or-pv5zdu7W3cVSdqEs;-><init>(Lcom/squareup/ui/activity/BulkSettleButtonPresenter;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showCustomView(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/activity/-$$Lambda$sJRwNpPZe74BkmocEKiIGBTN0O0;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$sJRwNpPZe74BkmocEKiIGBTN0O0;

    iget-object v3, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->bulkSettleButtonPresenter:Lcom/squareup/ui/activity/BulkSettleButtonPresenter;

    .line 141
    invoke-virtual {v3}, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->getTooltip()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setCustomView(Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    const/4 v1, 0x0

    .line 143
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextRightPadded(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    .line 145
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextRightPadded(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 147
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 151
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateSearchBarVisibility()V

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryPresenter$XJw2RjLr5AMJMUVAa7Xlf0AQQv4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryPresenter$XJw2RjLr5AMJMUVAa7Xlf0AQQv4;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_1

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 155
    sget v0, Lcom/squareup/billhistoryui/R$string;->search_history_hint_rw:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/TransactionsHistoryView;->setSearchBarHint(I)V

    .line 158
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateHistory()V

    .line 159
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryPresenter$AqbLXbp-WZKPQ7jVH3IeAG-ijDA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/-$$Lambda$LegacyTransactionsHistoryPresenter$AqbLXbp-WZKPQ7jVH3IeAG-ijDA;-><init>(Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public onPaymentTerminated(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 4

    .line 261
    invoke-static {p1}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object p1

    .line 262
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSearchStopped()V

    .line 263
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    invoke-virtual {p1}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1, v2}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->getTitle(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->res:Lcom/squareup/util/Res;

    .line 264
    invoke-virtual {p1, v3}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->getMessage(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    .line 263
    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    return-void
.end method

.method public onPaymentTerminatedDueToSwipe(Lcom/squareup/Card;)V
    .locals 0

    .line 236
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSwipe(Lcom/squareup/Card;)V

    return-void
.end method

.method onSearchAction(Ljava/lang/String;)V
    .locals 1

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showFullHistoryPermission:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->isPermissionGranted()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 280
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    if-eqz v0, :cond_2

    .line 286
    invoke-virtual {v0}, Lcom/squareup/ui/activity/TransactionsHistoryView;->hideSoftKeyboard()V

    .line 289
    invoke-static {p1}, Lcom/squareup/Card;->createFromPAN(Ljava/lang/String;)Lcom/squareup/Card;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 293
    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->cardSearchRequested(Lcom/squareup/Card;)V

    goto :goto_0

    .line 296
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->textSearchRequested(Ljava/lang/String;)V

    :goto_0
    return-void

    .line 282
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "TransactionsHistoryView is not visible, onSearchAction should never be called!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method onSearchBarLostFocus()V
    .locals 0

    .line 461
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSearchStopped()V

    .line 462
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateHistory()V

    return-void
.end method

.method onSearchBarTapped()V
    .locals 2

    .line 466
    iget-boolean v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->contactlessReaderSearching:Z

    if-eqz v0, :cond_0

    .line 467
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateSearchPrompts()V

    return-void

    .line 470
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHubUtils;->isCardInsertedOnAnyContactlessReader()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 471
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->MUST_REMOVE_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    .line 473
    iput-boolean v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->contactlessReaderSearching:Z

    .line 474
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->activitySearchPaymentStarter:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;

    invoke-interface {v0}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;->enableContactlessField()V

    .line 476
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateSearchPrompts()V

    return-void
.end method

.method public onSearching()V
    .locals 1

    const/4 v0, 0x1

    .line 252
    iput-boolean v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->contactlessReaderSearching:Z

    .line 253
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateSearchPrompts()V

    return-void
.end method

.method public onShowFullHistoryPermissionGranted()V
    .locals 0

    .line 211
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->updateSearchBarVisibility()V

    return-void
.end method

.method onSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    .line 190
    iget-object p1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->handleSwipe(Lcom/squareup/Card;)V

    return-void
.end method

.method public onSwipeRefresh()V
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->reset()V

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->load()V

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyListPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->queryInstantDeposits()V

    return-void
.end method

.method setContactlessReaderSearching(Z)V
    .locals 0

    .line 516
    iput-boolean p1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->contactlessReaderSearching:Z

    return-void
.end method

.method updateSearchPrompts()V
    .locals 8

    .line 525
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    .line 526
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v2

    sget-object v3, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->USE_R6:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    .line 529
    :cond_1
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v2

    sget-object v3, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->USE_R12:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 533
    :cond_2
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->supportsSwipes()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_3

    const/4 v4, 0x1

    .line 536
    :cond_3
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->supportsDips()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v5, 0x1

    .line 539
    :cond_4
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTaps()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    .line 544
    :cond_5
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 545
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v0

    iget-boolean v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->contactlessReaderSearching:Z

    invoke-direct {p0, v0, v6, v1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showSearchPrompts(ZZZ)V

    goto :goto_1

    .line 548
    :cond_6
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v3

    iget-boolean v7, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->contactlessReaderSearching:Z

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->showDeprecatedSearchPrompts(ZZZZZ)V

    :goto_1
    return-void
.end method

.method updateSearchQuery()V
    .locals 2

    .line 337
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->currentSearchQuery:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v1}, Lcom/squareup/activity/LegacyTransactionsHistory;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->hasQuery()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 344
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v1}, Lcom/squareup/activity/LegacyTransactionsHistory;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->setSearchBarText(Ljava/lang/CharSequence;)V

    .line 346
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->hasTextQuery()Z

    move-result v0

    if-nez v0, :cond_2

    .line 347
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TransactionsHistoryView;->handleSearchByCard()V

    goto :goto_0

    .line 350
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->setSearchBarText(Ljava/lang/CharSequence;)V

    .line 352
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->getQuery()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->currentSearchQuery:Ljava/lang/String;

    return-void
.end method
