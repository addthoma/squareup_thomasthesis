.class abstract Lcom/squareup/ui/activity/RefundSecureTouchModule;
.super Ljava/lang/Object;
.source "RefundSecureTouchModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityModule;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideSecureTouchWorkflowResultRelay(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)Lcom/squareup/securetouch/SecureTouchWorkflowResultRelay;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSecureTouchWorkflowRunner(Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;)Lcom/squareup/securetouch/SecureTouchWorkflowRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
