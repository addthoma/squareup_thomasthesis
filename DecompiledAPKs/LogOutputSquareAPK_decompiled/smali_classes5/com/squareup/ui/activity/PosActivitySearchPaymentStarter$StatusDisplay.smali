.class final Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$StatusDisplay;
.super Ljava/lang/Object;
.source "PosActivitySearchPaymentStarter.java"

# interfaces
.implements Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "StatusDisplay"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V
    .locals 0

    .line 337
    iput-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$StatusDisplay;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;)V
    .locals 0

    .line 337
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$StatusDisplay;-><init>(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V

    return-void
.end method


# virtual methods
.method public contactlessReaderAdded(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    return-void
.end method

.method public contactlessReaderReadyForPayment(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    .line 340
    iget-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$StatusDisplay;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {p1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$400(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;->onSearching()V

    return-void
.end method

.method public contactlessReaderRemoved(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    return-void
.end method

.method public contactlessReaderTimedOut(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    .line 344
    iget-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$StatusDisplay;->this$0:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;

    invoke-static {p1}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->access$400(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;->onFieldTimeout()V

    return-void
.end method
