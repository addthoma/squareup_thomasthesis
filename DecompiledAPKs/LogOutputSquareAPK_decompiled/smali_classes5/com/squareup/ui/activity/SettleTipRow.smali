.class public Lcom/squareup/ui/activity/SettleTipRow;
.super Landroid/widget/LinearLayout;
.source "SettleTipRow.java"


# instance fields
.field presenter:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

.field private settleButton:Lcom/squareup/ui/ConfirmableButton;

.field private tenderId:Ljava/lang/String;

.field private titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    const-class p2, Lcom/squareup/ui/activity/BillHistoryDetailScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/BillHistoryDetailScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/activity/BillHistoryDetailScreen$Component;->inject(Lcom/squareup/ui/activity/SettleTipRow;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/SettleTipRow;Ljava/lang/Long;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/SettleTipRow;->tipAmountChanged(Ljava/lang/Long;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 104
    sget v0, Lcom/squareup/billhistoryui/R$id;->receipt_detail_tender_tip_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->titleView:Landroid/widget/TextView;

    .line 105
    sget v0, Lcom/squareup/billhistoryui/R$id;->receipt_detail_quicktip_editor:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/QuickTipEditor;

    iput-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    .line 106
    sget v0, Lcom/squareup/billhistoryui/R$id;->receipt_detail_add_tip_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmableButton;

    iput-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->settleButton:Lcom/squareup/ui/ConfirmableButton;

    return-void
.end method

.method private tipAmountChanged(Ljava/lang/Long;)V
    .locals 3

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->presenter:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipRow;->tenderId:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/activity/SettleTipRow;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/QuickTipEditor;->isQuickTipOptionUsed()Z

    move-result v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->onTipAmountChanged(Ljava/lang/String;Ljava/lang/Long;Z)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipRow;->presenter:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    iget-object v2, p0, Lcom/squareup/ui/activity/SettleTipRow;->tenderId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->requireTender(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/QuickTipEditor;->setTenderHistory(Lcom/squareup/billhistory/model/TenderHistory;)V

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 99
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/SettleTipRow;->setSettleButtonEnabled(Z)V

    .line 100
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SettleTipRow;->updateConfirmText()V

    return-void
.end method


# virtual methods
.method public getTenderId()Ljava/lang/String;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->tenderId:Ljava/lang/String;

    return-object v0
.end method

.method public getTipAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditor;->getTipAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onFinishInflate$0$SettleTipRow()V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->presenter:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->onSettleConfirmClicked(Lcom/squareup/ui/activity/SettleTipRow;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 39
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/activity/SettleTipRow;->bindViews()V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/QuickTipEditor;->setAmountTextBackground(Landroid/graphics/drawable/Drawable;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/QuickTipEditor;->setHorizontalGravity(I)V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    new-instance v1, Lcom/squareup/ui/activity/SettleTipRow$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/SettleTipRow$1;-><init>(Lcom/squareup/ui/activity/SettleTipRow;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/QuickTipEditor;->setOnTipAmountListener(Lcom/squareup/ui/activity/QuickTipEditorPresenter$TipAmountListener;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->settleButton:Lcom/squareup/ui/ConfirmableButton;

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$SettleTipRow$O0XXjB2rtXYFdBKItEy9ubY_7Ow;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$SettleTipRow$O0XXjB2rtXYFdBKItEy9ubY_7Ow;-><init>(Lcom/squareup/ui/activity/SettleTipRow;)V

    invoke-interface {v0, v1}, Lcom/squareup/ui/ConfirmableButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    return-void
.end method

.method public setSettleButtonEnabled(Z)V
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->settleButton:Lcom/squareup/ui/ConfirmableButton;

    invoke-interface {v0, p1}, Lcom/squareup/ui/ConfirmableButton;->setEnabled(Z)V

    return-void
.end method

.method public setTenderId(Ljava/lang/String;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/activity/SettleTipRow;->tenderId:Ljava/lang/String;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitleTextColor(I)V
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->titleView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/SettleTipRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public updateConfirmText()V
    .locals 3

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->settleButton:Lcom/squareup/ui/ConfirmableButton;

    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipRow;->presenter:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    iget-object v2, p0, Lcom/squareup/ui/activity/SettleTipRow;->tenderId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->getSettleButtonConfirmText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/ConfirmableButton;->setConfirmText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public updateQuickTipEditor(Lcom/squareup/billhistory/model/TenderHistory;)V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipRow;->quickTipEditor:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/QuickTipEditor;->setTenderHistory(Lcom/squareup/billhistory/model/TenderHistory;)V

    return-void
.end method
