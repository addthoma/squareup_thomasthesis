.class public final synthetic Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$n9Q1s1c2XvZWbgDSbE9Sg1LEbEA;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;

.field private final synthetic f$1:Lcom/squareup/billhistory/model/BillHistory;

.field private final synthetic f$2:Ljava/lang/String;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$n9Q1s1c2XvZWbgDSbE9Sg1LEbEA;->f$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$n9Q1s1c2XvZWbgDSbE9Sg1LEbEA;->f$1:Lcom/squareup/billhistory/model/BillHistory;

    iput-object p3, p0, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$n9Q1s1c2XvZWbgDSbE9Sg1LEbEA;->f$2:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$n9Q1s1c2XvZWbgDSbE9Sg1LEbEA;->f$0:Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$n9Q1s1c2XvZWbgDSbE9Sg1LEbEA;->f$1:Lcom/squareup/billhistory/model/BillHistory;

    iget-object v2, p0, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$n9Q1s1c2XvZWbgDSbE9Sg1LEbEA;->f$2:Ljava/lang/String;

    check-cast p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->lambda$null$12$IssueRefundScopeRunner(Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;Lcom/squareup/protos/client/bills/GetResidualBillResponse;)V

    return-void
.end method
