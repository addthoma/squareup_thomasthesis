.class public interface abstract Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;
.super Ljava/lang/Object;
.source "NfcProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/NfcProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NfcStatusDisplay"
.end annotation


# virtual methods
.method public abstract contactlessReaderAdded(Lcom/squareup/cardreader/CardReaderId;)V
.end method

.method public abstract contactlessReaderReadyForPayment(Lcom/squareup/cardreader/CardReaderId;)V
.end method

.method public abstract contactlessReaderRemoved(Lcom/squareup/cardreader/CardReaderId;)V
.end method

.method public abstract contactlessReaderTimedOut(Lcom/squareup/cardreader/CardReaderId;)V
.end method
