.class public Lcom/squareup/ui/CouponRibbonView;
.super Landroid/widget/FrameLayout;
.source "CouponRibbonView.java"


# instance fields
.field private ribbon:Lcom/squareup/glyph/SquareGlyphView;

.field private final ribbonColor:I

.field private textContainer:Landroid/widget/FrameLayout;

.field private final textSize:I

.field private valueText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 29
    sget p2, Lcom/squareup/glyph/R$dimen;->glyph_font_size:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/CouponRibbonView;->textSize:I

    .line 30
    sget p2, Lcom/squareup/widgets/pos/R$color;->edit_item_yellow:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/CouponRibbonView;->ribbonColor:I

    return-void
.end method


# virtual methods
.method public growBy(F)V
    .locals 6

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/CouponRibbonView;->ribbon:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/CouponRibbonView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RIBBON:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 44
    invoke-virtual {v1, v2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v1

    new-instance v2, Landroid/graphics/Rect;

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/CouponRibbonView;->getMeasuredWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float v3, v3, p1

    float-to-int v3, v3

    .line 46
    invoke-virtual {p0}, Lcom/squareup/ui/CouponRibbonView;->getMeasuredHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float v4, v4, p1

    float-to-int v4, v4

    const/4 v5, 0x0

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 45
    invoke-virtual {v1, v2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->bounds(Landroid/graphics/Rect;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/ui/CouponRibbonView;->ribbonColor:I

    .line 47
    invoke-virtual {v1, v2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->color(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/ui/CouponRibbonView;->textSize:I

    int-to-float v2, v2

    mul-float v2, v2, p1

    .line 48
    invoke-virtual {v1, v2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->textPaintSizeOverride(F)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/CouponRibbonView;->textContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 53
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    int-to-float v1, v1

    mul-float v1, v1, p1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 54
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    int-to-float v1, v1

    mul-float v1, v1, p1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 55
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    int-to-float v1, v1

    mul-float v1, v1, p1

    float-to-int p1, v1

    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 56
    iget-object p1, p0, Lcom/squareup/ui/CouponRibbonView;->textContainer:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 58
    invoke-virtual {p0}, Lcom/squareup/ui/CouponRibbonView;->requestLayout()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 34
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 36
    sget v0, Lcom/squareup/widgets/pos/R$id;->coupon_value:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/CouponRibbonView;->valueText:Landroid/widget/TextView;

    .line 37
    sget v0, Lcom/squareup/widgets/pos/R$id;->text_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/squareup/ui/CouponRibbonView;->textContainer:Landroid/widget/FrameLayout;

    .line 38
    sget v0, Lcom/squareup/widgets/pos/R$id;->ribbon_image:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/CouponRibbonView;->ribbon:Lcom/squareup/glyph/SquareGlyphView;

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/CouponRibbonView;->valueText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
