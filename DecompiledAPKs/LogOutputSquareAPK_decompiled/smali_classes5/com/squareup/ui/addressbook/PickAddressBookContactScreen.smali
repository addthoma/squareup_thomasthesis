.class public final Lcom/squareup/ui/addressbook/PickAddressBookContactScreen;
.super Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;
.source "PickAddressBookContactScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;,
        Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Runner;,
        Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/addressbook/PickAddressBookContactScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 123
    sget-object v0, Lcom/squareup/ui/addressbook/-$$Lambda$PickAddressBookContactScreen$5aZmCGaDaShCKZGq0OQXGfuxQ-k;->INSTANCE:Lcom/squareup/ui/addressbook/-$$Lambda$PickAddressBookContactScreen$5aZmCGaDaShCKZGq0OQXGfuxQ-k;

    .line 124
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/addressbook/PickAddressBookContactScreen;
    .locals 1

    .line 125
    const-class v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    .line 126
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    .line 127
    new-instance v0, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    return-object v0
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 116
    sget v0, Lcom/squareup/crmupdatecustomer/R$layout;->pick_address_book_contact_view:I

    return v0
.end method
