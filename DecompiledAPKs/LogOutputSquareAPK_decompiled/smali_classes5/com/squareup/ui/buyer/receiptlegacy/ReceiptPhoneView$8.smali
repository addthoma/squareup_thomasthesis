.class Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$8;
.super Ljava/lang/Object;
.source "ReceiptPhoneView.java"

# interfaces
.implements Lcom/squareup/picasso/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->lambda$setBackgroundImage$1(Lcom/squareup/picasso/RequestCreator;Landroid/view/View;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

.field final synthetic val$height:I

.field final synthetic val$width:I


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;II)V
    .locals 0

    .line 474
    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$8;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    iput p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$8;->val$height:I

    iput p3, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$8;->val$width:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Failed to load curated image"

    .line 483
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onSuccess()V
    .locals 3

    .line 476
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$8;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    invoke-static {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->access$100(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;)V

    .line 479
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$8;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    iget v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$8;->val$height:I

    iget v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$8;->val$width:I

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->access$200(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;II)V

    return-void
.end method
