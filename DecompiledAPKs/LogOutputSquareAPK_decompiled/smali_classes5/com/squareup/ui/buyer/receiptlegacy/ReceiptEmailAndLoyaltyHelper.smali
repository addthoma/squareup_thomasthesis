.class public final Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;
.super Ljava/lang/Object;
.source "ReceiptEmailAndLoyaltyHelper.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/buyer/BuyerScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReceiptEmailAndLoyaltyHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReceiptEmailAndLoyaltyHelper.kt\ncom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper\n+ 2 Rx.kt\ncom/squareup/util/rx/RxKt\n*L\n1#1,391:1\n18#2:392\n18#2:393\n*E\n*S KotlinDebug\n*F\n+ 1 ReceiptEmailAndLoyaltyHelper.kt\ncom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper\n*L\n223#1:392\n301#1:393\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00bc\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\r\u0008\u0007\u0018\u0000 R2\u00020\u0001:\u0001RBc\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0008\u0008\u0001\u0010\u000e\u001a\u00020\u000f\u0012\u0008\u0008\u0001\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u00a2\u0006\u0002\u0010\u0018J\u0010\u0010/\u001a\u00020\u001b2\u0006\u00100\u001a\u000201H\u0002J\u0008\u00102\u001a\u000203H\u0002J\u000e\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u001e0#H\u0002J\u0006\u00105\u001a\u00020\u001bJ\u001c\u00106\u001a\u00020\u001b2\u000c\u00107\u001a\u0008\u0012\u0004\u0012\u000209082\u0006\u0010:\u001a\u000201J\u000e\u00106\u001a\u00020\u001b2\u0006\u0010;\u001a\u00020<J\u0018\u0010=\u001a\u00020\u001b2\u0006\u0010>\u001a\u00020?2\u0008\u0010@\u001a\u0004\u0018\u000103J\u000e\u0010%\u001a\u00020\u001b2\u0006\u0010A\u001a\u00020&J\u000c\u0010B\u001a\u0008\u0012\u0004\u0012\u00020\u001b0#J\u000c\u0010C\u001a\u0008\u0012\u0004\u0012\u00020\u001e0#J\u000e\u0010D\u001a\u0008\u0012\u0004\u0012\u00020 0#H\u0007J\u0008\u0010E\u001a\u00020FH\u0007J\u0008\u0010G\u001a\u00020FH\u0007J\u000e\u0010H\u001a\u0008\u0012\u0004\u0012\u00020\u001b0#H\u0007J\u000e\u0010I\u001a\u0008\u0012\u0004\u0012\u00020\u001e0#H\u0007J\u0016\u0010J\u001a\u0008\u0012\u0004\u0012\u00020*0#2\u0006\u00100\u001a\u000201H\u0002J\u000c\u0010K\u001a\u0008\u0012\u0004\u0012\u00020*0#J\u000c\u0010L\u001a\u0008\u0012\u0004\u0012\u00020 0#J\u0006\u0010\'\u001a\u00020\u001bJ\u000e\u0010M\u001a\u00020\u001b2\u0006\u0010N\u001a\u00020 J\u0006\u0010O\u001a\u00020\u001bJ\u0006\u0010P\u001a\u00020\u001bJ\u000e\u0010.\u001a\u00020\u001b2\u0006\u0010Q\u001a\u00020 R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u0019\u001a&\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010\u001b0\u001b \u001c*\u0012\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010\u001b0\u001b\u0018\u00010\u001a0\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u001d\u001a&\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010\u001e0\u001e \u001c*\u0012\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010\u001e0\u001e\u0018\u00010\u001a0\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u001f\u001a&\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010 0  \u001c*\u0012\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010 0 \u0018\u00010\u001a0\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010!\u001a&\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010 0  \u001c*\u0012\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010 0 \u0018\u00010\u001a0\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020 0#X\u0082.\u00a2\u0006\u0002\n\u0000R2\u0010$\u001a&\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010 0  \u001c*\u0012\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010 0 \u0018\u00010\u001a0\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010%\u001a&\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010&0& \u001c*\u0012\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010&0&\u0018\u00010\u001a0\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\'\u001a&\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010\u001b0\u001b \u001c*\u0012\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010\u001b0\u001b\u0018\u00010(0(X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010)\u001a&\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010*0* \u001c*\u0012\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010*0*\u0018\u00010\u001a0\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020,X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010-\u001a&\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010\u001b0\u001b \u001c*\u0012\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010\u001b0\u001b\u0018\u00010(0(X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010.\u001a&\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010 0  \u001c*\u0012\u0012\u000c\u0012\n \u001c*\u0004\u0018\u00010 0 \u0018\u00010\u001a0\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006S"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
        "",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "topScreenChecker",
        "Lcom/squareup/ui/main/TopScreenChecker;",
        "skipReceiptScreenSettings",
        "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
        "emailCollectionSettings",
        "Lcom/squareup/crm/EmailCollectionSettings;",
        "rolodexServiceHelper",
        "Lcom/squareup/crm/RolodexServiceHelper;",
        "offlineModeMonitor",
        "Lcom/squareup/payment/OfflineModeMonitor;",
        "mainScheduler",
        "Lrx/Scheduler;",
        "mainThreadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "taskEnqueuer",
        "Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;",
        "loyaltyEventPublisher",
        "Lcom/squareup/loyalty/LoyaltyEventPublisher;",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "(Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/crm/EmailCollectionSettings;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/payment/OfflineModeMonitor;Lrx/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;Lcom/squareup/loyalty/LoyaltyEventPublisher;Lcom/squareup/loyalty/LoyaltySettings;)V",
        "goToEmailCollectionScreen",
        "Lcom/jakewharton/rxrelay/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "goToLoyaltyScreen",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
        "hasEmailAddress",
        "",
        "isFinishing",
        "isReceiptVisible",
        "Lrx/Observable;",
        "loggedMissedLoyalty",
        "maybeStartSeparatedPrintouts",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;",
        "receiptOptionSelected",
        "Lcom/jakewharton/rxrelay/PublishRelay;",
        "startSeparatedPrintoutsWorkflow",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;",
        "subs",
        "Lrx/subscriptions/CompositeSubscription;",
        "transitionToLoyaltyOrEmailCollection",
        "willGoToEmailCollection",
        "addSubscriptions",
        "transactionUniqueKey",
        "",
        "emptyCreatorDetails",
        "Lcom/squareup/protos/client/CreatorDetails;",
        "filteredLoyaltyEvent",
        "goToLoyaltyOrEmailCollection",
        "init",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/container/ContainerTreeKey;",
        "uniqueId",
        "scope",
        "Lmortar/MortarScope;",
        "logMissedLoyalty",
        "reason",
        "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;",
        "creatorDetails",
        "receiptSelection",
        "onGoToEmailCollectionScreen",
        "onGoToLoyaltyScreen",
        "onIsFinishing",
        "onNoEmailCollection",
        "Lrx/Completable;",
        "onNoLoyaltyEvent",
        "onShouldGoToEmailCollectionScreen",
        "onShouldGoToLoyaltyScreen",
        "onShouldGoToSeparatedPrintouts",
        "onStartSeparatedPrintoutsWorkflow",
        "onWillGoToEmailCollection",
        "setHasEmailAddress",
        "hasEmail",
        "setIsFinishing",
        "terminate",
        "willGo",
        "Companion",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$Companion;

.field public static final LOYALTY_RESPONSE_TIMEOUT:J = 0x1388L


# instance fields
.field private final emailCollectionSettings:Lcom/squareup/crm/EmailCollectionSettings;

.field private final goToEmailCollectionScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final goToLoyaltyScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final hasEmailAddress:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final isFinishing:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private isReceiptVisible:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final loggedMissedLoyalty:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyEventPublisher:Lcom/squareup/loyalty/LoyaltyEventPublisher;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final mainScheduler:Lrx/Scheduler;

.field private final mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final maybeStartSeparatedPrintouts:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private final receiptOptionSelected:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexServiceHelper:Lcom/squareup/crm/RolodexServiceHelper;

.field private final skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

.field private final startSeparatedPrintoutsWorkflow:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;",
            ">;"
        }
    .end annotation
.end field

.field private final subs:Lrx/subscriptions/CompositeSubscription;

.field private final taskEnqueuer:Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;

.field private final topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transitionToLoyaltyOrEmailCollection:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final willGoToEmailCollection:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->Companion:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/crm/EmailCollectionSettings;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/payment/OfflineModeMonitor;Lrx/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;Lcom/squareup/loyalty/LoyaltyEventPublisher;Lcom/squareup/loyalty/LoyaltySettings;)V
    .locals 1
    .param p7    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p8    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "topScreenChecker"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "skipReceiptScreenSettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emailCollectionSettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rolodexServiceHelper"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "offlineModeMonitor"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThreadEnforcer"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taskEnqueuer"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyEventPublisher"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltySettings"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    iput-object p3, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    iput-object p4, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->emailCollectionSettings:Lcom/squareup/crm/EmailCollectionSettings;

    iput-object p5, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->rolodexServiceHelper:Lcom/squareup/crm/RolodexServiceHelper;

    iput-object p6, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    iput-object p7, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->mainScheduler:Lrx/Scheduler;

    iput-object p8, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iput-object p9, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->taskEnqueuer:Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;

    iput-object p10, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->loyaltyEventPublisher:Lcom/squareup/loyalty/LoyaltyEventPublisher;

    iput-object p11, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    .line 83
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->receiptOptionSelected:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 84
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->transitionToLoyaltyOrEmailCollection:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 87
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->willGoToEmailCollection:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 88
    sget-object p1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->UNSET:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->maybeStartSeparatedPrintouts:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 p1, 0x0

    .line 89
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->hasEmailAddress:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 90
    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->isFinishing:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 91
    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->loggedMissedLoyalty:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 94
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->goToLoyaltyScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 95
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->goToEmailCollectionScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 96
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->startSeparatedPrintoutsWorkflow:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 147
    new-instance p1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->subs:Lrx/subscriptions/CompositeSubscription;

    return-void
.end method

.method public static final synthetic access$emptyCreatorDetails(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)Lcom/squareup/protos/client/CreatorDetails;
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->emptyCreatorDetails()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getGoToEmailCollectionScreen$p(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->goToEmailCollectionScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getGoToLoyaltyScreen$p(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->goToLoyaltyScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getMainThreadEnforcer$p(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-object p0
.end method

.method public static final synthetic access$getStartSeparatedPrintoutsWorkflow$p(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->startSeparatedPrintoutsWorkflow:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getTransaction$p(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method public static final synthetic access$getWillGoToEmailCollection$p(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->willGoToEmailCollection:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$isFinishing$p(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->isFinishing:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method private final addSubscriptions(Ljava/lang/String;)V
    .locals 3

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->onShouldGoToLoyaltyScreen()Lrx/Observable;

    move-result-object v1

    .line 163
    new-instance v2, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$addSubscriptions$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$addSubscriptions$1;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)V

    check-cast v2, Lrx/functions/Action1;

    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v1

    .line 162
    invoke-virtual {v0, v1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->onShouldGoToEmailCollectionScreen()Lrx/Observable;

    move-result-object v1

    .line 170
    new-instance v2, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$addSubscriptions$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$addSubscriptions$2;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)V

    check-cast v2, Lrx/functions/Action1;

    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v1

    .line 169
    invoke-virtual {v0, v1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->onShouldGoToSeparatedPrintouts(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    .line 177
    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$addSubscriptions$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$addSubscriptions$3;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 176
    invoke-virtual {v0, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    .line 183
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->loyaltyEventPublisher:Lcom/squareup/loyalty/LoyaltyEventPublisher;

    invoke-virtual {v0}, Lcom/squareup/loyalty/LoyaltyEventPublisher;->getMissedLoyaltyReason()Lio/reactivex/Observable;

    move-result-object v0

    check-cast v0, Lio/reactivex/ObservableSource;

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    .line 184
    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$addSubscriptions$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$addSubscriptions$4;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 183
    invoke-virtual {p1, v0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method private final emptyCreatorDetails()Lcom/squareup/protos/client/CreatorDetails;
    .locals 2

    .line 369
    new-instance v0, Lcom/squareup/protos/client/CreatorDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/CreatorDetails$Builder;-><init>()V

    .line 370
    invoke-virtual {v0}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v0

    const-string v1, "CreatorDetails.Builder()\n        .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final filteredLoyaltyEvent()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
            ">;"
        }
    .end annotation

    .line 359
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->loyaltyEventPublisher:Lcom/squareup/loyalty/LoyaltyEventPublisher;

    invoke-virtual {v0}, Lcom/squareup/loyalty/LoyaltyEventPublisher;->getLoyaltyEvent()Lio/reactivex/Observable;

    move-result-object v0

    check-cast v0, Lio/reactivex/ObservableSource;

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    .line 360
    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$filteredLoyaltyEvent$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$filteredLoyaltyEvent$1;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x1

    .line 365
    invoke-virtual {v0, v1}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object v0

    const-string/jumbo v1, "toV1Observable(loyaltyEv\u2026       }\n        .take(1)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final onShouldGoToSeparatedPrintouts(Ljava/lang/String;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;",
            ">;"
        }
    .end annotation

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->maybeStartSeparatedPrintouts:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 332
    sget-object v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$onMaybeStartSeparatedPrintouts$1;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$onMaybeStartSeparatedPrintouts$1;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 333
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->isFinishing:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v2, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$isNotFinishing$1;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$isNotFinishing$1;

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    .line 335
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->receiptOptionSelected:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 336
    new-instance v3, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$1;

    invoke-direct {v3, v1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$1;-><init>(Lrx/Observable;)V

    check-cast v3, Lrx/functions/Func1;

    invoke-virtual {v2, v3}, Lcom/jakewharton/rxrelay/PublishRelay;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    .line 337
    new-instance v2, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$2;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$2;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;Lrx/Observable;)V

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x1

    .line 342
    invoke-virtual {v0, v1}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object v0

    .line 343
    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$3;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;Ljava/lang/String;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    const-string v0, "receiptOptionSelected\n  \u2026  )\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final goToLoyaltyOrEmailCollection()V
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->transitionToLoyaltyOrEmailCollection:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public final init(Lio/reactivex/Observable;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "uniqueId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    sget-object v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$init$1;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$init$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "screens.map {\n      it i\u2026tCompleteScreen.KEY\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/ObservableSource;

    .line 156
    sget-object v0, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {p1, v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->isReceiptVisible:Lrx/Observable;

    .line 158
    invoke-direct {p0, p2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->addSubscriptions(Ljava/lang/String;)V

    return-void
.end method

.method public final init(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;

    .line 139
    iget-object v0, p1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;->uniqueKey:Ljava/lang/String;

    .line 141
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/main/TopScreenChecker;->unobscured(Lcom/squareup/container/ContainerTreeKey;)Lio/reactivex/Observable;

    move-result-object p1

    const-string/jumbo v1, "topScreenChecker.unobscured(receiptScreenLegacy)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/ObservableSource;

    .line 142
    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {p1, v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->isReceiptVisible:Lrx/Observable;

    const-string/jumbo p1, "transactionUniqueKey"

    .line 144
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->addSubscriptions(Ljava/lang/String;)V

    return-void

    .line 138
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.ui.buyer.receiptlegacy.ReceiptScreenLegacy"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final logMissedLoyalty(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;Lcom/squareup/protos/client/CreatorDetails;)V
    .locals 2

    const-string v0, "reason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 377
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->loggedMissedLoyalty:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "loggedMissedLoyalty"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "loggedMissedLoyalty.value"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyProgramActive()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 381
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->taskEnqueuer:Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;

    .line 382
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->transaction:Lcom/squareup/payment/Transaction;

    if-eqz p2, :cond_1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->emptyCreatorDetails()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object p2

    .line 381
    :goto_0
    invoke-interface {v0, p1, v1, p2}, Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;->enqueueMissedLoyaltyOpportunity(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;Lcom/squareup/payment/Transaction;Lcom/squareup/protos/client/CreatorDetails;)V

    .line 384
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->loggedMissedLoyalty:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 p2, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_2
    :goto_1
    return-void
.end method

.method public final maybeStartSeparatedPrintouts(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V
    .locals 1

    const-string v0, "receiptSelection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->maybeStartSeparatedPrintouts:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public final onGoToEmailCollectionScreen()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->goToEmailCollectionScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "goToEmailCollectionScreen"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public final onGoToLoyaltyScreen()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
            ">;"
        }
    .end annotation

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->goToLoyaltyScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "goToLoyaltyScreen"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public final onIsFinishing()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->isFinishing:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "isFinishing"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public final onNoEmailCollection()Lrx/Completable;
    .locals 2

    .line 318
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->willGoToEmailCollection:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 319
    sget-object v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onNoEmailCollection$1;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onNoEmailCollection$1;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x1

    .line 320
    invoke-virtual {v0, v1}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object v0

    .line 321
    invoke-virtual {v0}, Lrx/Observable;->toCompletable()Lrx/Completable;

    move-result-object v0

    const-string/jumbo v1, "willGoToEmailCollection\n\u2026\n        .toCompletable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onNoLoyaltyEvent()Lrx/Completable;
    .locals 5

    .line 294
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    invoke-static {}, Lrx/Completable;->complete()Lrx/Completable;

    move-result-object v0

    const-string v1, "Completable.complete()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->loyaltyEventPublisher:Lcom/squareup/loyalty/LoyaltyEventPublisher;

    invoke-virtual {v0}, Lcom/squareup/loyalty/LoyaltyEventPublisher;->getMaybeLoyaltyEvent()Lio/reactivex/Observable;

    move-result-object v0

    check-cast v0, Lio/reactivex/ObservableSource;

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1388

    .line 299
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {v0, v1, v2, v3, v4}, Lrx/Observable;->timeout(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    .line 300
    sget-object v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onNoLoyaltyEvent$1;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onNoLoyaltyEvent$1;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->onErrorResumeNext(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const-string/jumbo v1, "toV1Observable(loyaltyEv\u2026just(NonLoyaltyEvent()) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 393
    const-class v1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;

    invoke-virtual {v0, v1}, Lrx/Observable;->ofType(Ljava/lang/Class;)Lrx/Observable;

    move-result-object v0

    const-string v1, "ofType(T::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 302
    invoke-virtual {v0, v1}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object v0

    .line 303
    invoke-virtual {v0}, Lrx/Observable;->toCompletable()Lrx/Completable;

    move-result-object v0

    const-string/jumbo v1, "toV1Observable(loyaltyEv\u2026         .toCompletable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public final onShouldGoToEmailCollectionScreen()Lrx/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 232
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->emailCollectionSettings:Lcom/squareup/crm/EmailCollectionSettings;

    invoke-interface {v0}, Lcom/squareup/crm/EmailCollectionSettings;->isEmailCollectionEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    invoke-interface {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;->skipReceiptScreenForFastCheckout()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->isFinishing:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$isNotFinishing$1;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$isNotFinishing$1;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 240
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    const-string/jumbo v2, "transaction.requireReceiptForLastPayment()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 243
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->rolodexServiceHelper:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-interface {v2, v1}, Lcom/squareup/crm/RolodexServiceHelper;->shouldShowEmailCollection(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v1

    .line 244
    sget-object v2, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$shouldShowEmailCollection$1;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$shouldShowEmailCollection$1;

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v1

    const-string v2, "rolodexServiceHelper.sho\u2026            }\n          }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/SingleSource;

    .line 251
    invoke-static {v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object v1

    .line 252
    invoke-virtual {v1}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 254
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->mainScheduler:Lrx/Scheduler;

    invoke-static {v2, v3, v4, v5}, Lrx/Observable;->timer(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$shouldShowEmailCollection$2;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$shouldShowEmailCollection$2;

    check-cast v3, Lrx/functions/Func2;

    invoke-virtual {v1, v2, v3}, Lrx/Observable;->zipWith(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v1

    .line 257
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->filteredLoyaltyEvent()Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;

    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-direct {v3, v5, v5, v4, v5}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-static {v2, v3}, Lcom/squareup/util/rx/RxKt;->currentValueOrDefault(Lrx/Observable;Ljava/lang/Object;)Lrx/Observable;

    move-result-object v2

    .line 258
    iget-object v3, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->isReceiptVisible:Lrx/Observable;

    if-nez v3, :cond_2

    const-string v4, "isReceiptVisible"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 259
    :cond_2
    iget-object v4, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->hasEmailAddress:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v4, Lrx/Observable;

    .line 260
    sget-object v5, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$canShowEmailCollection$1;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$canShowEmailCollection$1;

    check-cast v5, Lrx/functions/Func3;

    .line 256
    invoke-static {v2, v3, v4, v5}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v2

    .line 271
    iget-object v3, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->transitionToLoyaltyOrEmailCollection:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 272
    new-instance v4, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$1;

    invoke-direct {v4, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$1;-><init>(Lrx/Observable;)V

    check-cast v4, Lrx/functions/Func1;

    invoke-virtual {v3, v4}, Lcom/jakewharton/rxrelay/PublishRelay;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 273
    new-instance v3, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$2;

    invoke-direct {v3, v2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$2;-><init>(Lrx/Observable;)V

    check-cast v3, Lrx/functions/Func1;

    invoke-virtual {v0, v3}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const/4 v2, 0x1

    .line 274
    invoke-virtual {v0, v2}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object v0

    .line 275
    new-instance v2, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$3;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$3;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)V

    check-cast v2, Lrx/functions/Action1;

    invoke-virtual {v0, v2}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v0

    .line 280
    sget-object v2, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$4;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$4;

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v0, v2}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 281
    new-instance v2, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$5;

    invoke-direct {v2, v1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$5;-><init>(Lrx/Observable;)V

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v0, v2}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 282
    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$6;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$6;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v0

    .line 283
    sget-object v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$7;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$7;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 284
    sget-object v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$8;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$8;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const-string/jumbo v1, "transitionToLoyaltyOrEma\u2026w }\n        .map { Unit }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 235
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->willGoToEmailCollection:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 236
    invoke-static {}, Lrx/Observable;->empty()Lrx/Observable;

    move-result-object v0

    const-string v1, "empty()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onShouldGoToLoyaltyScreen()Lrx/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
            ">;"
        }
    .end annotation

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->transitionToLoyaltyOrEmailCollection:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v1, 0x1

    .line 199
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->take(I)Lrx/Observable;

    move-result-object v0

    .line 202
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->mainScheduler:Lrx/Scheduler;

    const-wide/16 v3, 0x1

    invoke-virtual {v0, v3, v4, v1, v2}, Lrx/Observable;->delay(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    .line 204
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->filteredLoyaltyEvent()Lrx/Observable;

    move-result-object v1

    .line 205
    sget-object v2, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToLoyaltyScreen$loyaltyEventWithTrigger$1;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToLoyaltyScreen$loyaltyEventWithTrigger$1;

    check-cast v2, Lrx/functions/Func2;

    invoke-virtual {v1, v0, v2}, Lrx/Observable;->zipWith(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 209
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->isReceiptVisible:Lrx/Observable;

    if-nez v1, :cond_0

    const-string v2, "isReceiptVisible"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 210
    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->isFinishing:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v2, Lrx/Observable;

    .line 211
    sget-object v3, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToLoyaltyScreen$shouldGoToLoyalty$1;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToLoyaltyScreen$shouldGoToLoyalty$1;

    check-cast v3, Lrx/functions/Func3;

    .line 207
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    .line 220
    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToLoyaltyScreen$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToLoyaltyScreen$1;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const-string v1, "shouldGoToLoyalty\n      \u2026ymentComplete()\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 392
    const-class v1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    invoke-virtual {v0, v1}, Lrx/Observable;->ofType(Ljava/lang/Class;)Lrx/Observable;

    move-result-object v0

    const-string v1, "ofType(T::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onStartSeparatedPrintoutsWorkflow()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;",
            ">;"
        }
    .end annotation

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->startSeparatedPrintoutsWorkflow:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "startSeparatedPrintoutsWorkflow"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public final onWillGoToEmailCollection()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 389
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->willGoToEmailCollection:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string/jumbo v1, "willGoToEmailCollection"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public final receiptOptionSelected()V
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->receiptOptionSelected:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public final setHasEmailAddress(Z)V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->hasEmailAddress:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public final setIsFinishing()V
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->isFinishing:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public final terminate()V
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    return-void
.end method

.method public final willGoToEmailCollection(Z)V
    .locals 1

    .line 387
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->willGoToEmailCollection:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
