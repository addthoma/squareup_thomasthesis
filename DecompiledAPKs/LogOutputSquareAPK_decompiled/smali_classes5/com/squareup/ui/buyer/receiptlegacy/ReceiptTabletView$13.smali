.class Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$13;
.super Lcom/squareup/text/ScrubbingTextWatcher;
.source "ReceiptTabletView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->initializeSmsInput()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V
    .locals 0

    .line 477
    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$13;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    invoke-direct {p0, p2, p3}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .line 479
    invoke-super {p0, p1}, Lcom/squareup/text/ScrubbingTextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    .line 480
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$13;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    iget-object p1, p1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->updateSmsEnabledStates()V

    return-void
.end method
