.class public final Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_DipRequiredFallbackHandler_Factory;
.super Ljava/lang/Object;
.source "PaymentScreenHandler_DipRequiredFallbackHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$DipRequiredFallbackHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final emvRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_DipRequiredFallbackHandler_Factory;->emvRunnerProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_DipRequiredFallbackHandler_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_DipRequiredFallbackHandler_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;)",
            "Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_DipRequiredFallbackHandler_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_DipRequiredFallbackHandler_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_DipRequiredFallbackHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;)Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$DipRequiredFallbackHandler;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$DipRequiredFallbackHandler;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$DipRequiredFallbackHandler;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$DipRequiredFallbackHandler;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_DipRequiredFallbackHandler_Factory;->emvRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_DipRequiredFallbackHandler_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/permissions/PermissionGatekeeper;

    invoke-static {v0, v1}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_DipRequiredFallbackHandler_Factory;->newInstance(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;)Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$DipRequiredFallbackHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_DipRequiredFallbackHandler_Factory;->get()Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$DipRequiredFallbackHandler;

    move-result-object v0

    return-object v0
.end method
