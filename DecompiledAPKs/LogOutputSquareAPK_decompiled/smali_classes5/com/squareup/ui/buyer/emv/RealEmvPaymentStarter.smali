.class public Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;
.super Ljava/lang/Object;
.source "RealEmvPaymentStarter.java"

# interfaces
.implements Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final clock:Lcom/squareup/util/Clock;

.field private final nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

.field private final paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

.field private final readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

.field private final readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/log/ReaderSessionIds;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/util/Clock;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    .line 39
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    .line 40
    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    .line 41
    iput-object p4, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->clock:Lcom/squareup/util/Clock;

    .line 42
    iput-object p5, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 43
    iput-object p6, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 44
    iput-object p7, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->transaction:Lcom/squareup/payment/Transaction;

    .line 45
    iput-object p8, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    .line 46
    iput-object p9, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    return-void
.end method


# virtual methods
.method public setUpEmvPayment(Z)V
    .locals 1

    if-eqz p1, :cond_0

    return-void

    .line 84
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object p1

    if-nez p1, :cond_1

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p1

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/TenderFactory;->createSmartCard()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    .line 92
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p1, v0}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    :cond_1
    return-void
.end method

.method public startPaymentOnActiveReader()V
    .locals 6

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setDipPaymentStarted()V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/log/ReaderSessionIds;->onEmvPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 62
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    sget-object v3, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_STARTING_PAYMENT_ON_READER:Lcom/squareup/analytics/ReaderEventName;

    sget-object v4, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v5, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->transaction:Lcom/squareup/payment/Transaction;

    .line 63
    invoke-virtual {v5}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v5

    .line 62
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 65
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/PaymentCounter;->onPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 66
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v1}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v1

    .line 68
    invoke-virtual {v1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/squareup/money/MoneyMath;->sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 69
    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v3}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v3

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/CardReader;->startPayment(JJ)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->paymentStartedOnReader()V

    return-void

    .line 56
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tried to start payment without an active card reader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
