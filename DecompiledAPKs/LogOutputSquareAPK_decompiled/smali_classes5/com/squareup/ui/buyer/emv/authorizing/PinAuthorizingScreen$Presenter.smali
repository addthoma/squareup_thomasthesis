.class public Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;
.super Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;
.source "PinAuthorizingScreen.java"

# interfaces
.implements Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation


# instance fields
.field private final emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

.field private isContactless:Z

.field private listenForNfcAuthBytes:Z

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 52
    invoke-direct {p0, p4}, Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;-><init>(Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)V

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    .line 54
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    .line 55
    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$PinAuthorizingScreen$Presenter(Ljava/lang/Boolean;)V
    .locals 2

    .line 71
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;

    iget-boolean v1, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;->isContactless:Z

    xor-int/lit8 v1, v1, 0x1

    .line 72
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->showCardRemovalPrompt(ZZ)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 59
    invoke-super {p0, p1}, Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 60
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;

    .line 61
    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;->emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    iget-boolean v1, v1, Lcom/squareup/ui/buyer/emv/EmvScope;->isContactless:Z

    iput-boolean v1, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;->isContactless:Z

    .line 62
    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;->access$000(Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;->listenForNfcAuthBytes:Z

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/NfcProcessor;->continueMonitoring()V

    .line 66
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;->listenForNfcAuthBytes:Z

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v0, p1, p0}, Lcom/squareup/ui/NfcProcessor;->registerNfcAuthDelegate(Lmortar/MortarScope;Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;)V

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->cardPresenceRequired()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/emv/authorizing/-$$Lambda$PinAuthorizingScreen$Presenter$6eJWZT0U0GwkgvrTiPvuY7yLgHQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/authorizing/-$$Lambda$PinAuthorizingScreen$Presenter$6eJWZT0U0GwkgvrTiPvuY7yLgHQ;-><init>(Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnExit(Lrx/Subscription;Lmortar/MortarScope;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 76
    invoke-super {p0, p1}, Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 77
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;

    .line 78
    sget v0, Lcom/squareup/checkout/R$string;->buyer_authorizing:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->setSpinnerMessageText(I)V

    return-void
.end method

.method public onNfcAuthorizationRequestReceived(Lcom/squareup/ui/NfcAuthData;)V
    .locals 3

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->hasAuthData()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    const-string v2, "We already have auth data for this card, this shouldn\'t happen"

    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 90
    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iget-object p1, p1, Lcom/squareup/ui/NfcAuthData;->authorizationData:[B

    invoke-virtual {v0, v1, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setSmartCardAuthRequestData(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;[B)V

    .line 92
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->authorizeContactlessPayment()V

    return-void
.end method
