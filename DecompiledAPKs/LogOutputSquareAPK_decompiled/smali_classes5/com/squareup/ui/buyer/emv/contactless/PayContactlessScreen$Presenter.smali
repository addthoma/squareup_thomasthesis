.class public Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "PayContactlessScreen.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/buyer/emv/contactless/PayContactlessView;",
        ">;",
        "Lcom/squareup/workflow/ui/HandlesBack;",
        "Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;"
    }
.end annotation


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private disposable:Lio/reactivex/disposables/SerialDisposable;

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private final nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

.field private final smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 88
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 81
    new-instance v0, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->disposable:Lio/reactivex/disposables/SerialDisposable;

    .line 89
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 90
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    .line 91
    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    .line 92
    iput-object p4, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    .line 93
    iput-object p5, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    .line 94
    iput-object p6, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    .line 95
    iput-object p7, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    .line 96
    iput-object p8, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 97
    iput-object p9, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    .line 98
    iput-object p10, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-void
.end method

.method public static synthetic lambda$V4n4ddb46J_t-nQkePbNHxcPWxI(Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->setText(Lcom/squareup/locale/LocaleOverrideFactory;)V

    return-void
.end method

.method private setText(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 4

    .line 159
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessView;

    .line 160
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    .line 161
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v2

    .line 160
    invoke-virtual {v1, v2}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedTotalAmount(Lcom/squareup/text/Formatter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessView;->setTotal(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    .line 163
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v3

    .line 162
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedAmountDueAutoGratuityAndTip(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 165
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/cardreader/R$string;->contactless_waiting_for_tap_title:I

    .line 166
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 164
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessView;->setSpinnerTitleText(Ljava/lang/String;)V

    .line 168
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p1

    sget v1, Lcom/squareup/cardreader/R$string;->contactless_waiting_for_tap_message:I

    .line 169
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 167
    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessView;->setSpinnerMessageText(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public dropView(Lcom/squareup/ui/buyer/emv/contactless/PayContactlessView;)V
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->disposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    .line 108
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 67
    check-cast p1, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->dropView(Lcom/squareup/ui/buyer/emv/contactless/PayContactlessView;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$PayContactlessScreen$Presenter(Lcom/squareup/ui/NfcAuthData;)V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    iget-object v1, p1, Lcom/squareup/ui/NfcAuthData;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    iget-object p1, p1, Lcom/squareup/ui/NfcAuthData;->authorizationData:[B

    .line 122
    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getContinueContactlessTenderMaybeAfterErrorResult(Lcom/squareup/cardreader/CardReaderInfo;[B)Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object p1

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->navigateForSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->exit()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p1, p0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerEmvCardInsertRemoveProcessor(Lmortar/MortarScope;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/NfcProcessor;->startMonitoringWithAutoFieldRestart()V

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    new-instance v1, Lcom/squareup/ui/buyer/emv/contactless/-$$Lambda$PayContactlessScreen$Presenter$lyewiP8zdaxMIex1KneQ4Lz-pjQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/contactless/-$$Lambda$PayContactlessScreen$Presenter$lyewiP8zdaxMIex1KneQ4Lz-pjQ;-><init>(Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;)V

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/NfcProcessor;->registerNfcAuthDelegate(Lmortar/MortarScope;Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->disposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    .line 113
    invoke-super {p0}, Lmortar/ViewPresenter;->onExitScope()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 102
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessView;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 103
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->disposable:Lio/reactivex/disposables/SerialDisposable;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/emv/contactless/-$$Lambda$PayContactlessScreen$Presenter$V4n4ddb46J_t-nQkePbNHxcPWxI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/contactless/-$$Lambda$PayContactlessScreen$Presenter$V4n4ddb46J_t-nQkePbNHxcPWxI;-><init>(Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->setActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->cancelPaymentsOnNonActiveCardReaders()V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setDipPaymentStarted()V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    .line 142
    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 143
    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->paymentStartedOnReader()V

    .line 147
    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->EMV:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setSmartCardEntryMethod(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    .line 149
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/PaymentCounter;->onPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 151
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Presenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->contactlessPaymentStartedAfterPreAuthTip()V

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method
