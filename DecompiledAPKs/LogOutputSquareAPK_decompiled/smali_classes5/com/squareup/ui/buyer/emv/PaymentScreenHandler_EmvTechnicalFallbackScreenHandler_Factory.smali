.class public final Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvTechnicalFallbackScreenHandler_Factory;
.super Ljava/lang/Object;
.source "PaymentScreenHandler_EmvTechnicalFallbackScreenHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvTechnicalFallbackScreenHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final emvRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final magicBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvTechnicalFallbackScreenHandler_Factory;->magicBusProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvTechnicalFallbackScreenHandler_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvTechnicalFallbackScreenHandler_Factory;->emvRunnerProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvTechnicalFallbackScreenHandler_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvTechnicalFallbackScreenHandler_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;)",
            "Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvTechnicalFallbackScreenHandler_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvTechnicalFallbackScreenHandler_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvTechnicalFallbackScreenHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;)Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvTechnicalFallbackScreenHandler;
    .locals 1

    .line 52
    new-instance v0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvTechnicalFallbackScreenHandler;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvTechnicalFallbackScreenHandler;-><init>(Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvTechnicalFallbackScreenHandler;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvTechnicalFallbackScreenHandler_Factory;->magicBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvTechnicalFallbackScreenHandler_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvTechnicalFallbackScreenHandler_Factory;->emvRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    iget-object v3, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvTechnicalFallbackScreenHandler_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/permissions/PermissionGatekeeper;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvTechnicalFallbackScreenHandler_Factory;->newInstance(Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;)Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvTechnicalFallbackScreenHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler_EmvTechnicalFallbackScreenHandler_Factory;->get()Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvTechnicalFallbackScreenHandler;

    move-result-object v0

    return-object v0
.end method
