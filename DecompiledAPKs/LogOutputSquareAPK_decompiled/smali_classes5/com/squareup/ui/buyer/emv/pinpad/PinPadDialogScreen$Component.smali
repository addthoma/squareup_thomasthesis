.class public interface abstract Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$Component;
.super Ljava/lang/Object;
.source "PinPadDialogScreen.java"

# interfaces
.implements Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation
