.class public final Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;
.super Lcom/squareup/ui/buyer/emv/InEmvScope;
.source "PreparingPaymentScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Component;,
        Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Module;,
        Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 88
    sget-object v0, Lcom/squareup/ui/buyer/emv/preparing/-$$Lambda$PreparingPaymentScreen$zpgK5LWBUWaBa6LKm5noLO69_9Y;->INSTANCE:Lcom/squareup/ui/buyer/emv/preparing/-$$Lambda$PreparingPaymentScreen$zpgK5LWBUWaBa6LKm5noLO69_9Y;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/InEmvScope;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;
    .locals 1

    .line 89
    const-class v0, Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/emv/EmvScope;

    .line 90
    new-instance v0, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 84
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/buyer/emv/InEmvScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;->emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 94
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->emv_progress_view:I

    return v0
.end method
