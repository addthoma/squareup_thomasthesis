.class public final Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;
.super Ljava/lang/Object;
.source "RetryEmvTenderStrategy.kt"

# interfaces
.implements Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\u0017\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0002J\u0008\u0010\t\u001a\u00020\nH\u0016J\u0008\u0010\u000b\u001a\u00020\u0008H\u0016J\u0008\u0010\u000c\u001a\u00020\u0008H\u0016J\u0008\u0010\r\u001a\u00020\u0008H\u0016J\u0008\u0010\u000e\u001a\u00020\u0008H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;",
        "Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;",
        "emvScopeRunner",
        "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
        "buyerScopeRunner",
        "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
        "(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/buyer/BuyerScopeRunner;)V",
        "cancelEmvPayment",
        "",
        "checkBillPaymentForOfflineModeButton",
        "",
        "confirmCancelPayment",
        "maybeAutomaticallyRetryInOfflineMode",
        "onEnterOfflineMode",
        "retryPayment",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private final emvScopeRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/buyer/BuyerScopeRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "emvScopeRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerScopeRunner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;->emvScopeRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    return-void
.end method

.method private final cancelEmvPayment()V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->dropPaymentAndUnsetCardReader(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    return-void
.end method


# virtual methods
.method public checkBillPaymentForOfflineModeButton()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public confirmCancelPayment()V
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;->emvScopeRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->exit()V

    return-void
.end method

.method public maybeAutomaticallyRetryInOfflineMode()V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;->cancelEmvPayment()V

    return-void
.end method

.method public onEnterOfflineMode()V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;->cancelEmvPayment()V

    return-void
.end method

.method public retryPayment()V
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;->emvScopeRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->reauthorize()V

    return-void
.end method
