.class public Lcom/squareup/ui/buyer/emv/EmvProcessor;
.super Ljava/lang/Object;
.source "EmvProcessor.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;,
        Lcom/squareup/ui/buyer/emv/EmvProcessor$OfflineApprovedEmvAddTendersConsumer;,
        Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;,
        Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;,
        Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;,
        Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;
    }
.end annotation


# static fields
.field private static final AUTH_TIMEOUT_MILLS:J = 0x2710L


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field authTimeoutRunnerDisposable:Lio/reactivex/disposables/Disposable;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final cardReader:Lcom/squareup/cardreader/CardReader;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

.field private final dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

.field private final emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final internalListener:Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;

.field private isContactless:Z

.field private final listener:Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

.field private final readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

.field private final secureTouchFeature:Lcom/squareup/securetouch/SecureTouchFeature;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/CardReaderPowerMonitor;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/payment/Transaction;Lcom/squareup/hudtoaster/HudToaster;Lio/reactivex/Scheduler;Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/securetouch/SecureTouchFeature;)V
    .locals 3
    .param p10    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param

    move-object v0, p0

    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    move-object v1, p1

    .line 176
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    move-object v1, p2

    .line 177
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    move-object v1, p3

    .line 178
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReader:Lcom/squareup/cardreader/CardReader;

    move-object v1, p4

    .line 179
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-object v1, p5

    .line 180
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    move-object v1, p6

    .line 181
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    move-object v1, p7

    .line 182
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    move-object v1, p8

    .line 183
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p9

    .line 184
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    move-object v1, p10

    .line 185
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->mainScheduler:Lio/reactivex/Scheduler;

    move-object v1, p11

    .line 186
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->listener:Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-object v1, p12

    .line 187
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    move-object/from16 v1, p13

    .line 188
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    move-object/from16 v1, p14

    .line 189
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    move-object/from16 v1, p15

    .line 190
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

    move-object/from16 v1, p16

    .line 191
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    move-object/from16 v1, p17

    .line 192
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    move-object/from16 v1, p18

    .line 193
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->secureTouchFeature:Lcom/squareup/securetouch/SecureTouchFeature;

    .line 195
    new-instance v1, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;-><init>(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/ui/buyer/emv/EmvProcessor$1;)V

    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->internalListener:Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->listener:Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Z
    .locals 0

    .line 96
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->isPaymentContactless()Z

    move-result p0

    return p0
.end method

.method static synthetic access$1100(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    .line 96
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->updateTrackersOnCompletedTransaction(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderPowerMonitor;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    .line 96
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->logPaymentApproved(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    .line 96
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->logPaymentDeclined(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 0

    .line 96
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->onPaymentCanceledOrTerminated(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/protos/client/Status;)V
    .locals 0

    .line 96
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->verifyStatus(Lcom/squareup/protos/client/Status;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/StandardMessageResources$MessageResources;)Z
    .locals 0

    .line 96
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->handleTerminatedPayment(Lcom/squareup/cardreader/StandardMessageResources$MessageResources;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$1800(Lcom/squareup/ui/buyer/emv/EmvProcessor;)V
    .locals 0

    .line 96
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->performPaymentTerminatedTasks()V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReader;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReader:Lcom/squareup/cardreader/CardReader;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderInfo;
    .locals 0

    .line 96
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/log/ReaderEventLogger;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/TenderInEdit;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/hudtoaster/HudToaster;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    return-object p0
.end method

.method private authorize(Lcom/squareup/payment/BillPayment;Z)V
    .locals 5

    .line 558
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->listener:Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresenceRequired()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onAuthorizing(Z)V

    if-eqz p2, :cond_0

    .line 562
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->promoteSmartCardTender()V

    .line 565
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {p2}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 566
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/payment/tender/SmartCardTender;->isFallback()Z

    move-result p2

    const-wide/16 v0, 0x320

    if-eqz p2, :cond_1

    .line 567
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->prepareToAuthorize()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/register/widgets/GlassSpinner;->MIN_SPINNER_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->mainScheduler:Lio/reactivex/Scheduler;

    .line 568
    invoke-static {v0, v1, v3, v4}, Lcom/squareup/util/rx2/Rx2TransformersKt;->delayEmission(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    invoke-virtual {v2, v0}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 569
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->newFallbackAddTendersConsumer()Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 567
    invoke-virtual {p2, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    goto :goto_0

    .line 571
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->prepareToAuthorize()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/register/widgets/GlassSpinner;->MIN_SPINNER_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->mainScheduler:Lio/reactivex/Scheduler;

    .line 572
    invoke-static {v0, v1, v3, v4}, Lcom/squareup/util/rx2/Rx2TransformersKt;->delayEmission(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    invoke-virtual {v2, v0}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 573
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->newEmvAddTendersConsumer(Lcom/squareup/payment/BillPayment;)Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 571
    invoke-virtual {p2, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 575
    :goto_0
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_SEND_ARQC_TO_SERVER:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 576
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 575
    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 577
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->authorize()V

    return-void
.end method

.method private captureOfflineApproved(Lcom/squareup/payment/BillPayment;Z)V
    .locals 6

    .line 524
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->listener:Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresenceRequired()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onAuthorizing(Z)V

    if-eqz p2, :cond_0

    .line 528
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->promoteSmartCardTender()V

    .line 533
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/payment/tender/SmartCardTender;->wasTenderApprovedOffline()Z

    move-result p2

    const-string v0, "EmvProcessor::captureOfflineApproved billPayment tender was not approved offline"

    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 535
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/payment/tender/SmartCardTender;->getSmartCardDataAuth()[B

    move-result-object p2

    .line 537
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTender;->isFallback()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "EmvProcessor::captureOfflineApproved billPayment smart card tender is fallback"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 540
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 541
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->prepareToAuthorize()Lio/reactivex/Observable;

    move-result-object v1

    const-wide/16 v2, 0x320

    sget-object v4, Lcom/squareup/register/widgets/GlassSpinner;->MIN_SPINNER_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->mainScheduler:Lio/reactivex/Scheduler;

    .line 542
    invoke-static {v2, v3, v4, v5}, Lcom/squareup/util/rx2/Rx2TransformersKt;->delayEmission(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/ObservableTransformer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 543
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->newOfflineApprovedEmvAddTendersConsumer()Lcom/squareup/ui/buyer/emv/EmvProcessor$OfflineApprovedEmvAddTendersConsumer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 541
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 545
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_SEND_ARQC_TO_SERVER:Lcom/squareup/analytics/ReaderEventName;

    sget-object v3, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v4, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 546
    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v4

    .line 545
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 547
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->authorize()V

    .line 551
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object p1

    .line 552
    invoke-virtual {p1}, Lcom/squareup/payment/tender/SmartCardTender;->hasSmartCardCaptureArgs()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "EmvProcessor::captureOfflineApproved smartCardTender has card capture args"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 554
    invoke-virtual {p1, p2}, Lcom/squareup/payment/tender/SmartCardTender;->setSmartCardCaptureArgs([B)V

    return-void
.end method

.method private cardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;
    .locals 1

    .line 587
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    return-object v0
.end method

.method private handleCardholderNameResult()V
    .locals 5

    .line 259
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->isPaymentContactless()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->hasAuthData()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 263
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->processTenderInEdit()V

    goto/16 :goto_0

    .line 265
    :cond_1
    sget-object v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$1;->$SwitchMap$com$squareup$ui$buyer$emv$CardholderNameProcessor$Result:[I

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->getResult()Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 290
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v2}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->getResult()Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " not handled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286
    :pswitch_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->listener:Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onCardError()V

    goto :goto_0

    .line 282
    :pswitch_1
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->listener:Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->getSecureTouchPinRequestData()Lcom/squareup/securetouch/SecureTouchPinRequestData;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onHardwarePinRequested(Lcom/squareup/securetouch/SecureTouchPinRequestData;)V

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->NONE:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setResult(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;)V

    goto :goto_0

    .line 278
    :pswitch_2
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->listener:Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->getPinRequestInfo()Lcom/squareup/cardreader/PinRequestData;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onSoftwarePinRequested(Lcom/squareup/cardreader/PinRequestData;)V

    .line 279
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->NONE:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setResult(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;)V

    goto :goto_0

    .line 272
    :pswitch_3
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->listener:Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->getApplications()[Lcom/squareup/cardreader/EmvApplication;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onListApplications([Lcom/squareup/cardreader/EmvApplication;)V

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_APPLICATION_SELECTION:Lcom/squareup/analytics/ReaderEventName;

    sget-object v3, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v4, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 274
    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v4

    .line 273
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 275
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->NONE:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setResult(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;)V

    :goto_0
    :pswitch_4
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private handleTerminatedPayment(Lcom/squareup/cardreader/StandardMessageResources$MessageResources;)Z
    .locals 5

    .line 460
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    .line 461
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->updateTrackersOnCompletedTransaction(Lcom/squareup/cardreader/CardReaderId;)V

    .line 463
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v1, v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->isOtherActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 471
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 473
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    .line 474
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->isPaymentContactless()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/analytics/ReaderEventName;

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/analytics/ReaderEventName;

    :goto_0
    sget-object v3, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v4, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 475
    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v4

    .line 473
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 477
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->performPaymentTerminatedTasks()V

    .line 478
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->listener:Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    invoke-interface {v0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onPaymentCanceled(Lcom/squareup/cardreader/StandardMessageResources$MessageResources;)V

    const/4 p1, 0x1

    return p1
.end method

.method private isPaymentContactless()Z
    .locals 1

    .line 591
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->isContactless:Z

    return v0
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 232
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "secure touch feature app event received: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$onEnterScope$1(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "secure touch feature lcr event received: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$onEnterScope$2(Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 240
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "secure touch feature isSecureTouchEnabled received: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private logPaymentApproved(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 3

    .line 768
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    .line 769
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->isPaymentContactless()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_APPROVED:Lcom/squareup/analytics/ReaderEventName;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_COMPLETE_APPROVED:Lcom/squareup/analytics/ReaderEventName;

    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 770
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    .line 768
    invoke-virtual {v0, p1, v1, p2, v2}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    return-void
.end method

.method private logPaymentDeclined(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 3

    .line 774
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    .line 775
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->isPaymentContactless()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_DECLINED:Lcom/squareup/analytics/ReaderEventName;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_COMPLETE_DECLINED:Lcom/squareup/analytics/ReaderEventName;

    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 776
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    .line 774
    invoke-virtual {v0, p1, v1, p2, v2}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    return-void
.end method

.method private onPaymentCanceledOrTerminated(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 2

    .line 512
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 514
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasSmartCardTenderInFlight()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 516
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/SmartCardTender;->getDeclinedMessageResources(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object p1

    goto :goto_0

    .line 518
    :cond_0
    invoke-static {p1}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object p1

    .line 520
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->handleTerminatedPayment(Lcom/squareup/cardreader/StandardMessageResources$MessageResources;)Z

    return-void
.end method

.method private performPaymentTerminatedTasks()V
    .locals 3

    .line 490
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cancelAuthTimeoutRunner()V

    .line 491
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->transaction:Lcom/squareup/payment/Transaction;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    return-void
.end method

.method private stopPaymentOnOtherReaders(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 4

    .line 502
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    .line 503
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    .line 504
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/squareup/cardreader/CardReaderId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 505
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 506
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->cancelPayment()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private updateTrackersOnCompletedTransaction(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 1

    .line 483
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->isPaymentContactless()Z

    move-result v0

    if-nez v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/DippedCardTracker;->onEmvTransactionCompleted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 486
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->reset()V

    return-void
.end method

.method private verifyStatus(Lcom/squareup/protos/client/Status;)V
    .locals 2

    const-string v0, "status"

    .line 581
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 582
    iget-object v0, p1, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    const-string v1, "localized_title"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 583
    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    const-string v0, "localized_description"

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method askForSig()Z
    .locals 1

    .line 304
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 305
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTender;->askForSignature()Z

    move-result v0

    return v0
.end method

.method askForTip()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 309
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 310
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v1}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 311
    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->askForTip()Z

    move-result v0

    goto :goto_0

    .line 312
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTender;->askForTip()Z

    move-result v0

    :goto_0
    return v0
.end method

.method authorizeSmartCardTender(Lcom/squareup/payment/BillPayment;)V
    .locals 2

    .line 420
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->wasTenderApprovedOffline()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 421
    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->captureOfflineApproved(Lcom/squareup/payment/BillPayment;Z)V

    goto :goto_0

    .line 423
    :cond_0
    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->authorize(Lcom/squareup/payment/BillPayment;Z)V

    :goto_0
    return-void
.end method

.method cancelAuthTimeoutRunner()V
    .locals 1

    .line 495
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->authTimeoutRunnerDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 496
    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    const/4 v0, 0x0

    .line 497
    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->authTimeoutRunnerDisposable:Lio/reactivex/disposables/Disposable;

    :cond_0
    return-void
.end method

.method cancelPayment()V
    .locals 5

    .line 374
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 375
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 376
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getPaymentCompletionListener()Lcom/squareup/cardreader/PaymentCompletionListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->internalListener:Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 377
    iget-object v4, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->internalListener:Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;

    aput-object v4, v1, v3

    iget-object v3, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 379
    invoke-interface {v3}, Lcom/squareup/cardreader/CardReaderListeners;->getPaymentCompletionListener()Lcom/squareup/cardreader/PaymentCompletionListener;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "Expected payment completion listener to be %s. Actual listener is: %s"

    .line 377
    invoke-static {v0, v2, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 380
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->cancelPayment()V

    .line 381
    sget-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NONE:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->onPaymentCanceledOrTerminated(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V

    :cond_1
    return-void
.end method

.method getInternalEmvListener()Lcom/squareup/cardreader/EmvListener;
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->internalListener:Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;

    return-object v0
.end method

.method getInternalPaymentCompletionListener()Lcom/squareup/cardreader/PaymentCompletionListener;
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->internalListener:Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;

    return-object v0
.end method

.method public synthetic lambda$onEnterScope$3$EmvProcessor(Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 243
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 244
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->secureTouchFeature:Lcom/squareup/securetouch/SecureTouchFeature;

    invoke-interface {p1}, Lcom/squareup/securetouch/SecureTouchFeature;->onSecureTouchEnabled()V

    goto :goto_0

    .line 246
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->secureTouchFeature:Lcom/squareup/securetouch/SecureTouchFeature;

    invoke-interface {p1}, Lcom/squareup/securetouch/SecureTouchFeature;->onSecureTouchDisabled()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$sendArpcToReader$4$EmvProcessor()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Canceling an ARPC that timed out!"

    .line 450
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 451
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cancelPayment()V

    return-void
.end method

.method newEmvAddTendersConsumer(Lcom/squareup/payment/BillPayment;)Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;
    .locals 1

    .line 213
    new-instance v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$EmvAddTendersConsumer;-><init>(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/payment/BillPayment;)V

    return-object v0
.end method

.method newFallbackAddTendersConsumer()Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;
    .locals 1

    .line 208
    new-instance v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor$FallbackAddTendersConsumer;-><init>(Lcom/squareup/ui/buyer/emv/EmvProcessor;)V

    return-object v0
.end method

.method newOfflineApprovedEmvAddTendersConsumer()Lcom/squareup/ui/buyer/emv/EmvProcessor$OfflineApprovedEmvAddTendersConsumer;
    .locals 1

    .line 218
    new-instance v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$OfflineApprovedEmvAddTendersConsumer;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor$OfflineApprovedEmvAddTendersConsumer;-><init>(Lcom/squareup/ui/buyer/emv/EmvProcessor;)V

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 222
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/EmvScope;

    .line 223
    iget-boolean v0, v0, Lcom/squareup/ui/buyer/emv/EmvScope;->isContactless:Z

    iput-boolean v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->isContactless:Z

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->internalListener:Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderListeners;->setEmvListener(Lcom/squareup/cardreader/EmvListener;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->internalListener:Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderListeners;->setPaymentCompletionListener(Lcom/squareup/cardreader/PaymentCompletionListener;)V

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->internalListener:Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderListeners;->setPinRequestListener(Lcom/squareup/cardreader/PinRequestListener;)V

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

    iget-boolean v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->isContactless:Z

    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;->setUpEmvPayment(Z)V

    .line 228
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->handleCardholderNameResult()V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "EmvProcessor: subscribe to messages for reader"

    .line 230
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->secureTouchFeature:Lcom/squareup/securetouch/SecureTouchFeature;

    invoke-interface {v0}, Lcom/squareup/securetouch/SecureTouchFeature;->eventsForReader()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$kMLlUIVJZCr017FWJFfK2HXdjbg;->INSTANCE:Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$kMLlUIVJZCr017FWJFfK2HXdjbg;

    .line 232
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/buyer/emv/-$$Lambda$SEaPWB5TNkUrkMfx45JmKgMfrfc;

    invoke-direct {v2, v1}, Lcom/squareup/ui/buyer/emv/-$$Lambda$SEaPWB5TNkUrkMfx45JmKgMfrfc;-><init>(Lcom/squareup/cardreader/CardReader;)V

    .line 233
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 231
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->secureTouchEvents()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$9Ix3AfYmEOsKqmgAPtHY4rqNax0;->INSTANCE:Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$9Ix3AfYmEOsKqmgAPtHY4rqNax0;

    .line 236
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->secureTouchFeature:Lcom/squareup/securetouch/SecureTouchFeature;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/buyer/emv/-$$Lambda$LyRV8dNknUbVYrBPWdGMOe5dU4Q;

    invoke-direct {v2, v1}, Lcom/squareup/ui/buyer/emv/-$$Lambda$LyRV8dNknUbVYrBPWdGMOe5dU4Q;-><init>(Lcom/squareup/securetouch/SecureTouchFeature;)V

    .line 237
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 235
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 239
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->isSecureTouchEnabled()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$yeGfyjJkV_o085Y48n_7Hz9ZvOQ;->INSTANCE:Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$yeGfyjJkV_o085Y48n_7Hz9ZvOQ;

    .line 240
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$B6plg9rEqj3jy3xuV3Oh1sLKm4o;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$B6plg9rEqj3jy3xuV3Oh1sLKm4o;-><init>(Lcom/squareup/ui/buyer/emv/EmvProcessor;)V

    .line 242
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 239
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 296
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetPaymentCompletionListener()V

    .line 297
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetEmvListener()V

    .line 298
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetPinRequestListener()V

    .line 299
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 300
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->reset()V

    return-void
.end method

.method onPinBypass()V
    .locals 1

    .line 407
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->onPinBypass()V

    return-void
.end method

.method onPinDigitEntered(I)V
    .locals 1

    .line 412
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->onPinDigitEntered(I)V

    return-void
.end method

.method onPinPadReset()V
    .locals 1

    .line 403
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->onPinPadReset()V

    return-void
.end method

.method processTenderInEdit()V
    .locals 2

    .line 365
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 366
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v1}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    .line 370
    :cond_0
    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->authorizeSmartCardTender(Lcom/squareup/payment/BillPayment;)V

    return-void
.end method

.method reauthorizeSmartCardTender(Lcom/squareup/payment/BillPayment;)V
    .locals 2

    .line 428
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "EmvProcessor::reauthorizeSmartCardTender tenderInEdit is not smart card tender"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 430
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->hasSmartCardTenderInFlight()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 431
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTender;->wasTenderApprovedOffline()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 432
    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->captureOfflineApproved(Lcom/squareup/payment/BillPayment;Z)V

    goto :goto_0

    .line 434
    :cond_0
    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->authorize(Lcom/squareup/payment/BillPayment;Z)V

    :goto_0
    return-void
.end method

.method restoreAsPaymentCompletionListener()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->internalListener:Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderListeners;->setPaymentCompletionListener(Lcom/squareup/cardreader/PaymentCompletionListener;)V

    return-void
.end method

.method selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V
    .locals 3

    .line 392
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V

    .line 393
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    .line 395
    invoke-interface {v1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v1

    .line 396
    invoke-virtual {v1}, Lio/reactivex/Observable;->blockingFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/locale/LocaleOverrideFactory;

    .line 397
    invoke-virtual {v1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->EMV_ACCOUNT_STRINGS:Ljava/util/HashMap;

    .line 398
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 394
    invoke-virtual {v0, v1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setBuyerSelectedAccountName(Ljava/lang/String;)V

    .line 399
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V

    return-void
.end method

.method selectApplication(Lcom/squareup/cardreader/EmvApplication;)V
    .locals 5

    .line 386
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_APPLICATION_SELECTED:Lcom/squareup/analytics/ReaderEventName;

    sget-object v3, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v4, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 387
    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v4

    .line 386
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 388
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->selectApplication(Lcom/squareup/cardreader/EmvApplication;)V

    return-void
.end method

.method sendArpcToReader(Lcom/squareup/payment/tender/SmartCardTender;)V
    .locals 5

    .line 439
    invoke-virtual {p1}, Lcom/squareup/payment/tender/SmartCardTender;->isFallback()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 443
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_SEND_ARPC_TO_READER:Lcom/squareup/analytics/ReaderEventName;

    sget-object v3, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v4, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 444
    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v4

    .line 443
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 445
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-virtual {p1}, Lcom/squareup/payment/tender/SmartCardTender;->getARPC()[B

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->processARPC([B)V

    const-wide/16 v0, 0x2710

    .line 447
    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->mainScheduler:Lio/reactivex/Scheduler;

    .line 448
    invoke-static {v0, v1, p1, v2}, Lio/reactivex/Completable;->timer(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$rnfgfa1uY4AGttZYW70z8_ioCow;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvProcessor$rnfgfa1uY4AGttZYW70z8_ioCow;-><init>(Lcom/squareup/ui/buyer/emv/EmvProcessor;)V

    .line 449
    invoke-virtual {p1, v0}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->authTimeoutRunnerDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method submitFallbackSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
    .locals 2

    .line 320
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->setActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 330
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    invoke-virtual {v1}, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;->refreshSwipePassthrough()V

    .line 332
    iget-object v1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-direct {p0, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->stopPaymentOnOtherReaders(Lcom/squareup/cardreader/CardReaderId;)V

    .line 333
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v1}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v1

    .line 334
    iget-object p1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    invoke-virtual {v1, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setFallbackSwipeData(Lcom/squareup/Card;)V

    .line 335
    invoke-virtual {v1, p2}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setFallbackType(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V

    .line 337
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    .line 339
    invoke-virtual {v1, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 341
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->authorizeSmartCardTender(Lcom/squareup/payment/BillPayment;)V

    .line 342
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cancelAuthTimeoutRunner()V

    return-void
.end method

.method submitLegacySwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 2

    .line 348
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;->refreshSwipePassthrough()V

    .line 350
    iget-object v0, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    .line 351
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v1, v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->isOtherActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "Ignoring legacy swipe. A different card reader is active."

    .line 352
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 357
    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->stopPaymentOnOtherReaders(Lcom/squareup/cardreader/CardReaderId;)V

    .line 358
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->performPaymentTerminatedTasks()V

    .line 361
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {v0, p1}, Lcom/squareup/ui/tender/TenderStarter;->startTenderFlowWithSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    return-void
.end method

.method submitPinBlock()V
    .locals 1

    .line 416
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->submitPinBlock()V

    return-void
.end method
