.class public Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ChooseEmvOptionCoordinator.java"


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private final buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private emvOptionsContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

.field private final features:Lcom/squareup/settings/server/Features;

.field private selectLocaleButton:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 51
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    .line 53
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    .line 54
    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->features:Lcom/squareup/settings/server/Features;

    .line 55
    iput-object p4, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 150
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->buyer_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 151
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->emv_options_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/BuyerActionContainer;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->emvOptionsContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

    .line 152
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->switch_language_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->selectLocaleButton:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method static synthetic lambda$setSecondaryActionOptions$5(Lrx/functions/Action1;I)Lkotlin/Unit;
    .locals 0

    .line 130
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p0, p1}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    .line 131
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method

.method private setSecondaryActionOptions(Lcom/squareup/util/Res;[ILrx/functions/Action1;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "[I",
            "Lrx/functions/Action1<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;",
            ">;"
        }
    .end annotation

    .line 113
    array-length v0, p2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 115
    :goto_0
    array-length v2, p2

    if-ge v1, v2, :cond_0

    .line 116
    aget v2, p2, v1

    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 119
    :cond_0
    invoke-direct {p0, v0, p3}, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->setSecondaryActionOptions([Ljava/lang/String;Lrx/functions/Action1;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private setSecondaryActionOptions([Ljava/lang/String;Lrx/functions/Action1;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Lrx/functions/Action1<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;",
            ">;"
        }
    .end annotation

    .line 124
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 125
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    .line 127
    new-instance v2, Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;

    new-instance v3, Lcom/squareup/util/ViewString$TextString;

    aget-object v4, p1, v1

    invoke-direct {v3, v4}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v4, Lcom/squareup/ui/buyer/emv/chooseapplication/-$$Lambda$ChooseEmvOptionCoordinator$Z_GgLYzo1gPonGrpQSU1qjeu2LY;

    invoke-direct {v4, p2, v1}, Lcom/squareup/ui/buyer/emv/chooseapplication/-$$Lambda$ChooseEmvOptionCoordinator$Z_GgLYzo1gPonGrpQSU1qjeu2LY;-><init>(Lrx/functions/Action1;I)V

    invoke-direct {v2, v3, v4}, Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;-><init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private updateBuyerLayout(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    .line 140
    invoke-virtual {v2, p2}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedTotalAmount(Lcom/squareup/text/Formatter;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    .line 139
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    .line 142
    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedAmountDueAutoGratuityAndTip(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object p2

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    .line 143
    :cond_0
    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v0, p2}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    move-object p2, v0

    .line 144
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    .line 146
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->emvOptionsContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    sget v1, Lcom/squareup/ui/buyerflow/R$string;->emv_select_account:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p2, v0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->setCallToAction(Lcom/squareup/util/ViewString;)V

    return-void
.end method

.method private updateSelectLocaleButton(Lcom/squareup/locale/LocaleFormatter;Lcom/squareup/util/Res;)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->selectLocaleButton:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleFormatter;->displayLanguage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->selectLocaleButton:Lcom/squareup/marketfont/MarketTextView;

    sget v0, Lcom/squareup/activity/R$drawable;->buyer_language_icon:I

    .line 107
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    const/4 v0, 0x0

    .line 106
    invoke-virtual {p1, p2, v0, v0, v0}, Lcom/squareup/marketfont/MarketTextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private updateViews(Landroid/view/View;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 2

    .line 82
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;

    .line 84
    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->updateBuyerLayout(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)V

    .line 85
    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocaleFormatter()Lcom/squareup/locale/LocaleFormatter;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->updateSelectLocaleButton(Lcom/squareup/locale/LocaleFormatter;Lcom/squareup/util/Res;)V

    .line 88
    sget-object v0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator$1;->$SwitchMap$com$squareup$ui$buyer$emv$chooseapplication$ChooseEmvOptionScreen$ValueType:[I

    iget-object v1, p1, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->valueType:Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 95
    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p2

    iget-object v0, p1, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->stringResOptions:[I

    iget-object p1, p1, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->onOptionSelectedAtIndex:Lrx/functions/Action1;

    invoke-direct {p0, p2, v0, p1}, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->setSecondaryActionOptions(Lcom/squareup/util/Res;[ILrx/functions/Action1;)Ljava/util/List;

    move-result-object p1

    .line 97
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->emvOptionsContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

    new-instance v0, Lcom/squareup/ui/buyer/BuyerActionContainer$Config;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$Config;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {p2, v0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->setConfig(Lcom/squareup/ui/buyer/BuyerActionContainer$Config;)V

    goto :goto_0

    .line 100
    :cond_0
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unsupported value type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->valueType:Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen$ValueType;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 90
    :cond_1
    iget-object p2, p1, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->stringOptions:[Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionScreen;->onOptionSelectedAtIndex:Lrx/functions/Action1;

    .line 91
    invoke-direct {p0, p2, p1}, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->setSecondaryActionOptions([Ljava/lang/String;Lrx/functions/Action1;)Ljava/util/List;

    move-result-object p1

    .line 92
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->emvOptionsContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

    new-instance v0, Lcom/squareup/ui/buyer/BuyerActionContainer$Config;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$Config;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {p2, v0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->setConfig(Lcom/squareup/ui/buyer/BuyerActionContainer$Config;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    .line 59
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->bindViews(Landroid/view/View;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v2, Lcom/squareup/ui/buyer/emv/chooseapplication/-$$Lambda$ChooseEmvOptionCoordinator$3FD7BGUKNiOtitfYgc30E5eR6zo;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/emv/chooseapplication/-$$Lambda$ChooseEmvOptionCoordinator$3FD7BGUKNiOtitfYgc30E5eR6zo;-><init>(Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 67
    new-instance v0, Lcom/squareup/ui/buyer/emv/chooseapplication/-$$Lambda$ChooseEmvOptionCoordinator$kAcsEnk_kK45O6ZZqyCVtmevP1Y;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/emv/chooseapplication/-$$Lambda$ChooseEmvOptionCoordinator$kAcsEnk_kK45O6ZZqyCVtmevP1Y;-><init>(Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_BUYER_LANGUAGE_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->selectLocaleButton:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->selectLocaleButton:Lcom/squareup/marketfont/MarketTextView;

    new-instance v1, Lcom/squareup/ui/buyer/emv/chooseapplication/-$$Lambda$ChooseEmvOptionCoordinator$PYAjZfCptuwVvaULUDd_Fm68UEw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/chooseapplication/-$$Lambda$ChooseEmvOptionCoordinator$PYAjZfCptuwVvaULUDd_Fm68UEw;-><init>(Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->selectLocaleButton:Lcom/squareup/marketfont/MarketTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 76
    :goto_0
    new-instance v0, Lcom/squareup/ui/buyer/emv/chooseapplication/-$$Lambda$ChooseEmvOptionCoordinator$WnTdXjlBvJ7-TWPYmrlQ8ttSttQ;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/buyer/emv/chooseapplication/-$$Lambda$ChooseEmvOptionCoordinator$WnTdXjlBvJ7-TWPYmrlQ8ttSttQ;-><init>(Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;Landroid/view/View;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$ChooseEmvOptionCoordinator()Lkotlin/Unit;
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->confirmCancelPayment(Z)V

    .line 64
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public synthetic lambda$attach$1$ChooseEmvOptionCoordinator()V
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->confirmCancelPayment(Z)V

    return-void
.end method

.method public synthetic lambda$attach$2$ChooseEmvOptionCoordinator(Landroid/view/View;)V
    .locals 0

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->showSelectBuyerLanguageScreen()V

    return-void
.end method

.method public synthetic lambda$attach$4$ChooseEmvOptionCoordinator(Landroid/view/View;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    .line 77
    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/emv/chooseapplication/-$$Lambda$ChooseEmvOptionCoordinator$aqsPCn9RhaYjVBfv0X9cQMz51cQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/emv/chooseapplication/-$$Lambda$ChooseEmvOptionCoordinator$aqsPCn9RhaYjVBfv0X9cQMz51cQ;-><init>(Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;Landroid/view/View;)V

    .line 78
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$3$ChooseEmvOptionCoordinator(Landroid/view/View;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 78
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;->updateViews(Landroid/view/View;Lcom/squareup/locale/LocaleOverrideFactory;)V

    return-void
.end method
