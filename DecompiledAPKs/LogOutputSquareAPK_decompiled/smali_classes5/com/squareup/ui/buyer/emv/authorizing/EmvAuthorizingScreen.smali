.class public final Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;
.super Lcom/squareup/ui/buyer/emv/InEmvScope;
.source "EmvAuthorizingScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Component;,
        Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Module;,
        Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final authorizeOnArrival:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 131
    sget-object v0, Lcom/squareup/ui/buyer/emv/authorizing/-$$Lambda$EmvAuthorizingScreen$MpkHlwFunzk_7O4r65YnOnpHATQ;->INSTANCE:Lcom/squareup/ui/buyer/emv/authorizing/-$$Lambda$EmvAuthorizingScreen$MpkHlwFunzk_7O4r65YnOnpHATQ;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 36
    iget-boolean v0, p1, Lcom/squareup/ui/buyer/emv/EmvScope;->isContactless:Z

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope;Z)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/InEmvScope;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    .line 41
    iput-boolean p2, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;->authorizeOnArrival:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;)Z
    .locals 0

    .line 30
    iget-boolean p0, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;->authorizeOnArrival:Z

    return p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;
    .locals 2

    .line 132
    const-class v0, Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/EmvScope;

    .line 133
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v1, 0x1

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 134
    :goto_0
    new-instance p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;Z)V

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 126
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/buyer/emv/InEmvScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;->emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 128
    iget-boolean p2, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;->authorizeOnArrival:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 49
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;->authorizeOnArrival:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "%s: authorize: %s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 138
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->emv_progress_view:I

    return v0
.end method
