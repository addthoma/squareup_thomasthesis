.class public final Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideEmvPathFactory;
.super Ljava/lang/Object;
.source "EmvScope_Module_ProvideEmvPathFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/emv/EmvScope;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/ui/buyer/emv/EmvScope$Module;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope$Module;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideEmvPathFactory;->module:Lcom/squareup/ui/buyer/emv/EmvScope$Module;

    return-void
.end method

.method public static create(Lcom/squareup/ui/buyer/emv/EmvScope$Module;)Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideEmvPathFactory;
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideEmvPathFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideEmvPathFactory;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Module;)V

    return-object v0
.end method

.method public static provideEmvPath(Lcom/squareup/ui/buyer/emv/EmvScope$Module;)Lcom/squareup/ui/buyer/emv/EmvScope;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/EmvScope$Module;->provideEmvPath()Lcom/squareup/ui/buyer/emv/EmvScope;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/emv/EmvScope;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/emv/EmvScope;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideEmvPathFactory;->module:Lcom/squareup/ui/buyer/emv/EmvScope$Module;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideEmvPathFactory;->provideEmvPath(Lcom/squareup/ui/buyer/emv/EmvScope$Module;)Lcom/squareup/ui/buyer/emv/EmvScope;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvideEmvPathFactory;->get()Lcom/squareup/ui/buyer/emv/EmvScope;

    move-result-object v0

    return-object v0
.end method
