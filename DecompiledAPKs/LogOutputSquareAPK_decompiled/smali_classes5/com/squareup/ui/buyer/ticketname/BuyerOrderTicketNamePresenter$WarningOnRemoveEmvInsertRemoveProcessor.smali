.class Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$WarningOnRemoveEmvInsertRemoveProcessor;
.super Ljava/lang/Object;
.source "BuyerOrderTicketNamePresenter.java"

# interfaces
.implements Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WarningOnRemoveEmvInsertRemoveProcessor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;)V
    .locals 0

    .line 290
    iput-object p1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$WarningOnRemoveEmvInsertRemoveProcessor;->this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$1;)V
    .locals 0

    .line 290
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$WarningOnRemoveEmvInsertRemoveProcessor;-><init>(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;)V

    return-void
.end method


# virtual methods
.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 296
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresenceRequired()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 299
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$WarningOnRemoveEmvInsertRemoveProcessor;->this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-static {p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->access$200(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 300
    iget-object p1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$WarningOnRemoveEmvInsertRemoveProcessor;->this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-static {p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->access$200(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object p1

    .line 301
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 302
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->cancelPayment()V

    .line 306
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$WarningOnRemoveEmvInsertRemoveProcessor;->this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-static {p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->access$300(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;)Lcom/squareup/payment/Transaction;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$WarningOnRemoveEmvInsertRemoveProcessor;->this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-static {p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->access$300(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;)Lcom/squareup/payment/Transaction;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p1

    .line 307
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->hasSmartCardTenderWithCaptureArgs()Z

    move-result p1

    if-nez p1, :cond_2

    .line 309
    iget-object p1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$WarningOnRemoveEmvInsertRemoveProcessor;->this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-static {p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->access$300(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;)Lcom/squareup/payment/Transaction;

    move-result-object p1

    const/4 v0, 0x0

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 313
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$WarningOnRemoveEmvInsertRemoveProcessor;->this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-static {p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->access$400(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;)Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->reset()V

    .line 314
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->GENERIC_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 316
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->emv_card_removed_title:I

    .line 317
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->emv_card_removed_msg:I

    .line 318
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 319
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 320
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$WarningOnRemoveEmvInsertRemoveProcessor;->this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-static {v0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->access$500(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;)Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method
