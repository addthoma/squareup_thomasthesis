.class Lcom/squareup/ui/buyer/BuyerSpots$BuyerRight;
.super Lcom/squareup/container/spot/Spot;
.source "BuyerSpots.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/BuyerSpots;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BuyerRight"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/BuyerSpots$BuyerRight;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/buyer/BuyerSpots$BuyerRight;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 48
    new-instance v0, Lcom/squareup/ui/buyer/BuyerSpots$BuyerRight;

    invoke-direct {v0}, Lcom/squareup/ui/buyer/BuyerSpots$BuyerRight;-><init>()V

    sput-object v0, Lcom/squareup/ui/buyer/BuyerSpots$BuyerRight;->INSTANCE:Lcom/squareup/ui/buyer/BuyerSpots$BuyerRight;

    .line 104
    sget-object v0, Lcom/squareup/ui/buyer/BuyerSpots$BuyerRight;->INSTANCE:Lcom/squareup/ui/buyer/BuyerSpots$BuyerRight;

    invoke-static {v0}, Lcom/squareup/ui/buyer/BuyerSpots$BuyerRight;->forSpotSingleton(Lcom/squareup/container/spot/Spot;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/BuyerSpots$BuyerRight;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 50
    invoke-direct {p0}, Lcom/squareup/container/spot/Spot;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/buyer/BuyerSpots$1;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerSpots$BuyerRight;-><init>()V

    return-void
.end method


# virtual methods
.method protected backwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .line 79
    invoke-static {}, Lcom/squareup/container/spot/ExchangeSet;->createSet()Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 80
    invoke-virtual {v0, p3}, Lcom/squareup/container/spot/ExchangeSet$Builder;->standardDuration(Landroid/view/View;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 81
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->slideInFromLeft()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->defaultExchange(Lcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    .line 82
    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->skipExchange(I)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 83
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/container/spot/ExchangeSet$Builder;->start(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method protected backwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .line 88
    invoke-static {}, Lcom/squareup/container/spot/ExchangeSet;->createSet()Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 89
    invoke-virtual {v0, p4}, Lcom/squareup/container/spot/ExchangeSet$Builder;->standardDuration(Landroid/view/View;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 90
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->slideOutToRight()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->defaultExchange(Lcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    .line 91
    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->skipExchange(I)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 92
    invoke-virtual {v0, p1, p2, p4, p3}, Lcom/squareup/container/spot/ExchangeSet$Builder;->start(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method public forceOutgoingViewOnTop(Lcom/squareup/container/spot/Spot$Direction;Landroid/view/View;Landroid/view/View;)Z
    .locals 0

    .line 101
    const-class p1, Lcom/squareup/ui/buyer/ForceOutgoingViewOnTop;

    invoke-static {p2, p1}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result p1

    return p1
.end method

.method protected forwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .line 55
    invoke-static {}, Lcom/squareup/container/spot/ExchangeSet;->createSet()Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 56
    invoke-virtual {v0, p3}, Lcom/squareup/container/spot/ExchangeSet$Builder;->standardDuration(Landroid/view/View;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 57
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->slideInFromRight()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->defaultExchange(Lcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    .line 58
    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->skipExchange(I)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 61
    invoke-static {p3}, Lcom/squareup/ui/buyer/BuyerSpots;->access$200(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    sget v1, Lcom/squareup/checkout/R$id;->merchant_image:I

    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->fadeIn()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchange(ILcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    .line 65
    :cond_0
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/container/spot/ExchangeSet$Builder;->start(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method protected forwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .line 70
    invoke-static {}, Lcom/squareup/container/spot/ExchangeSet;->createSet()Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 71
    invoke-virtual {v0, p4}, Lcom/squareup/container/spot/ExchangeSet$Builder;->standardDuration(Landroid/view/View;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 72
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->slideOutToLeft()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->defaultExchange(Lcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    .line 73
    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->skipExchange(I)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 74
    invoke-virtual {v0, p1, p2, p4, p3}, Lcom/squareup/container/spot/ExchangeSet$Builder;->start(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method public skipTemporaryBackground()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
