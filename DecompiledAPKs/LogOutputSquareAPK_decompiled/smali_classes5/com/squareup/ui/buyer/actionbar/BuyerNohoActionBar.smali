.class public final Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;
.super Landroid/widget/RelativeLayout;
.source "BuyerNohoActionBar.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBuyerNohoActionBar.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BuyerNohoActionBar.kt\ncom/squareup/ui/buyer/actionbar/BuyerNohoActionBar\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,209:1\n1261#2:210\n*E\n*S KotlinDebug\n*F\n+ 1 BuyerNohoActionBar.kt\ncom/squareup/ui/buyer/actionbar/BuyerNohoActionBar\n*L\n51#1:210\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\u0018\u00002\u00020\u0001B#\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0006\u0010.\u001a\u00020/J\u0006\u00100\u001a\u00020/J\u0006\u00101\u001a\u00020/J\u0006\u00102\u001a\u00020/J\u0006\u00103\u001a\u00020/J\u0010\u00104\u001a\u00020/2\u0008\u0008\u0001\u00105\u001a\u00020\u0007J\u001c\u00106\u001a\u00020/2\u0006\u00107\u001a\u0002082\u000c\u00109\u001a\u0008\u0012\u0004\u0012\u00020/0:J\u0010\u0010;\u001a\u00020/2\u0008\u0010\u001f\u001a\u0004\u0018\u00010<J\u0010\u0010=\u001a\u00020/2\u0008\u0010>\u001a\u0004\u0018\u00010<J\u000e\u0010?\u001a\u00020/2\u0006\u0010\'\u001a\u00020<J\u001c\u0010@\u001a\u00020/2\u0006\u0010>\u001a\u00020<2\u000c\u00109\u001a\u0008\u0012\u0004\u0012\u00020/0:J\u001c\u0010A\u001a\u00020/2\u0006\u0010>\u001a\u00020<2\u000c\u00109\u001a\u0008\u0012\u0004\u0012\u00020/0:J\u001c\u0010B\u001a\u00020/2\u0006\u00107\u001a\u0002082\u000c\u00109\u001a\u0008\u0012\u0004\u0012\u00020/0:J\u0006\u0010C\u001a\u00020/J\u0006\u0010D\u001a\u00020/J\u0006\u0010E\u001a\u00020/R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u000c\u0010\r\u001a\u0004\u0008\t\u0010\u000bR\u001b\u0010\u000e\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u000f\u0010\r\u001a\u0004\u0008\u000e\u0010\u000bR\u001b\u0010\u0010\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0011\u0010\r\u001a\u0004\u0008\u0010\u0010\u000bR\u001b\u0010\u0012\u001a\u00020\u00138BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0016\u0010\r\u001a\u0004\u0008\u0014\u0010\u0015R\u001b\u0010\u0017\u001a\u00020\u00188BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001b\u0010\r\u001a\u0004\u0008\u0019\u0010\u001aR\u001b\u0010\u001c\u001a\u00020\u00138BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001e\u0010\r\u001a\u0004\u0008\u001d\u0010\u0015R\u001b\u0010\u001f\u001a\u00020 8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008#\u0010\r\u001a\u0004\u0008!\u0010\"R\u001b\u0010$\u001a\u00020 8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008&\u0010\r\u001a\u0004\u0008%\u0010\"R\u001b\u0010\'\u001a\u00020 8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008)\u0010\r\u001a\u0004\u0008(\u0010\"R\u001b\u0010*\u001a\u00020\u00188BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008,\u0010\r\u001a\u0004\u0008+\u0010\u001aR\u000e\u0010-\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006F"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;",
        "Landroid/widget/RelativeLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "isLandscape",
        "",
        "()Z",
        "isLandscape$delegate",
        "Lkotlin/Lazy;",
        "isPhone",
        "isPhone$delegate",
        "isTablet10Inch",
        "isTablet10Inch$delegate",
        "leftButton",
        "Lcom/squareup/noho/NohoButton;",
        "getLeftButton",
        "()Lcom/squareup/noho/NohoButton;",
        "leftButton$delegate",
        "leftGlyphButton",
        "Lcom/squareup/glyph/SquareGlyphView;",
        "getLeftGlyphButton",
        "()Lcom/squareup/glyph/SquareGlyphView;",
        "leftGlyphButton$delegate",
        "rightButton",
        "getRightButton",
        "rightButton$delegate",
        "subtitle",
        "Landroid/widget/TextView;",
        "getSubtitle",
        "()Landroid/widget/TextView;",
        "subtitle$delegate",
        "ticketName",
        "getTicketName",
        "ticketName$delegate",
        "title",
        "getTitle",
        "title$delegate",
        "upGlyphButton",
        "getUpGlyphButton",
        "upGlyphButton$delegate",
        "useCondensed",
        "hideLeftGlyphButton",
        "",
        "hideRightButton",
        "hideSubtitle",
        "hideTitle",
        "hideUpGlyph",
        "setButtonBackground",
        "backgroundColor",
        "setLeftGlyphButton",
        "glyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "command",
        "Lkotlin/Function0;",
        "setSubtitle",
        "Lcom/squareup/util/ViewString;",
        "setTicketName",
        "text",
        "setTitle",
        "setUpLeftButton",
        "setUpRightButton",
        "setupUpGlyph",
        "showLeftGlyphButton",
        "showRightButton",
        "showUpGlyph",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final isLandscape$delegate:Lkotlin/Lazy;

.field private final isPhone$delegate:Lkotlin/Lazy;

.field private final isTablet10Inch$delegate:Lkotlin/Lazy;

.field private final leftButton$delegate:Lkotlin/Lazy;

.field private final leftGlyphButton$delegate:Lkotlin/Lazy;

.field private final rightButton$delegate:Lkotlin/Lazy;

.field private final subtitle$delegate:Lkotlin/Lazy;

.field private final ticketName$delegate:Lkotlin/Lazy;

.field private final title$delegate:Lkotlin/Lazy;

.field private final upGlyphButton$delegate:Lkotlin/Lazy;

.field private useCondensed:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    sget-object v0, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-virtual {v0, p1}, Lcom/squareup/util/VectorFriendly;->ensureContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    .line 51
    invoke-direct {p0, v0, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    new-instance p2, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$upGlyphButton$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$upGlyphButton$2;-><init>(Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p2}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->upGlyphButton$delegate:Lkotlin/Lazy;

    .line 57
    new-instance p2, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$leftButton$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$leftButton$2;-><init>(Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p2}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->leftButton$delegate:Lkotlin/Lazy;

    .line 58
    new-instance p2, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$leftGlyphButton$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$leftGlyphButton$2;-><init>(Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p2}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->leftGlyphButton$delegate:Lkotlin/Lazy;

    .line 63
    new-instance p2, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$rightButton$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$rightButton$2;-><init>(Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p2}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->rightButton$delegate:Lkotlin/Lazy;

    .line 64
    new-instance p2, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$ticketName$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$ticketName$2;-><init>(Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p2}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->ticketName$delegate:Lkotlin/Lazy;

    .line 65
    new-instance p2, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$title$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$title$2;-><init>(Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p2}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->title$delegate:Lkotlin/Lazy;

    .line 66
    new-instance p2, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$subtitle$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$subtitle$2;-><init>(Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p2}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->subtitle$delegate:Lkotlin/Lazy;

    .line 67
    new-instance p2, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$isLandscape$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$isLandscape$2;-><init>(Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p2}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->isLandscape$delegate:Lkotlin/Lazy;

    .line 72
    new-instance p2, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$isTablet10Inch$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$isTablet10Inch$2;-><init>(Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p2}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->isTablet10Inch$delegate:Lkotlin/Lazy;

    .line 77
    new-instance p2, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$isPhone$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$isPhone$2;-><init>(Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p2}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->isPhone$delegate:Lkotlin/Lazy;

    .line 83
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->isLandscape()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->isTablet10Inch()Z

    move-result p2

    if-nez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    iput-boolean p2, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->useCondensed:Z

    .line 86
    iget-boolean p2, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->useCondensed:Z

    if-eqz p2, :cond_1

    .line 87
    sget p2, Lcom/squareup/checkout/R$layout;->buyer_noho_action_bar_condensed:I

    move-object p3, p0

    check-cast p3, Landroid/view/ViewGroup;

    invoke-static {p1, p2, p3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    goto :goto_1

    .line 89
    :cond_1
    sget p2, Lcom/squareup/checkout/R$layout;->buyer_noho_action_bar:I

    move-object p3, p0

    check-cast p3, Landroid/view/ViewGroup;

    invoke-static {p1, p2, p3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    :goto_1
    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 50
    sget p3, Lcom/squareup/noho/R$attr;->nohoActionBarStyle:I

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final getLeftButton()Lcom/squareup/noho/NohoButton;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->leftButton$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    return-object v0
.end method

.method private final getLeftGlyphButton()Lcom/squareup/glyph/SquareGlyphView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->leftGlyphButton$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    return-object v0
.end method

.method private final getRightButton()Lcom/squareup/noho/NohoButton;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->rightButton$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    return-object v0
.end method

.method private final getSubtitle()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->subtitle$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getTicketName()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->ticketName$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getTitle()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->title$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getUpGlyphButton()Lcom/squareup/glyph/SquareGlyphView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->upGlyphButton$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    return-object v0
.end method

.method private final isLandscape()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->isLandscape$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private final isPhone()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->isPhone$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private final isTablet10Inch()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->isTablet10Inch$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final hideLeftGlyphButton()V
    .locals 2

    .line 151
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getLeftGlyphButton()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    return-void
.end method

.method public final hideRightButton()V
    .locals 2

    .line 134
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getRightButton()Lcom/squareup/noho/NohoButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    return-void
.end method

.method public final hideSubtitle()V
    .locals 2

    .line 121
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getSubtitle()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final hideTitle()V
    .locals 2

    .line 99
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getTitle()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final hideUpGlyph()V
    .locals 2

    .line 195
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getUpGlyphButton()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    return-void
.end method

.method public final setButtonBackground(I)V
    .locals 1

    .line 203
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getLeftButton()Lcom/squareup/noho/NohoButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoButton;->setBackgroundResource(I)V

    .line 204
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getLeftGlyphButton()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundResource(I)V

    .line 205
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getRightButton()Lcom/squareup/noho/NohoButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoButton;->setBackgroundResource(I)V

    .line 206
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getUpGlyphButton()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundResource(I)V

    return-void
.end method

.method public final setLeftGlyphButton(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "glyph"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "command"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->showLeftGlyphButton()V

    .line 146
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getLeftGlyphButton()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 147
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getLeftGlyphButton()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$setLeftGlyphButton$1;

    invoke-direct {v0, p2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$setLeftGlyphButton$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setSubtitle(Lcom/squareup/util/ViewString;)V
    .locals 3

    if-eqz p1, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 104
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getSubtitle()Landroid/widget/TextView;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->useCondensed:Z

    if-eqz v1, :cond_1

    .line 105
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    goto :goto_1

    :cond_1
    move-object v1, p1

    .line 104
    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getSubtitle()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 110
    iget-boolean p1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->useCondensed:Z

    if-nez p1, :cond_3

    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getTicketName()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 p1, 0x4

    goto :goto_3

    :cond_3
    :goto_2
    const/16 p1, 0x8

    goto :goto_3

    :cond_4
    const/4 p1, 0x0

    .line 109
    :goto_3
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final setTicketName(Lcom/squareup/util/ViewString;)V
    .locals 4

    .line 169
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getTicketName()Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "resources"

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v3}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_0

    :cond_0
    move-object v3, v2

    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getTicketName()Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v2

    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public final setTitle(Lcom/squareup/util/ViewString;)V
    .locals 3

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getTitle()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getTitle()Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public final setUpLeftButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ViewString;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "command"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getLeftButton()Lcom/squareup/noho/NohoButton;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 163
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getLeftButton()Lcom/squareup/noho/NohoButton;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 164
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->hideUpGlyph()V

    .line 165
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getLeftButton()Lcom/squareup/noho/NohoButton;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$setUpLeftButton$1;

    invoke-direct {v0, p2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$setUpLeftButton$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setUpRightButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ViewString;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "command"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getRightButton()Lcom/squareup/noho/NohoButton;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 129
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getRightButton()Lcom/squareup/noho/NohoButton;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 130
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getRightButton()Lcom/squareup/noho/NohoButton;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$setUpRightButton$1;

    invoke-direct {v0, p2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$setUpRightButton$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "glyph"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "command"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->showUpGlyph()V

    .line 178
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getUpGlyphButton()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 179
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getUpGlyphButton()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$setupUpGlyph$1;

    invoke-direct {v1, p2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$setupUpGlyph$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p2}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    sget-object p2, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ordinal()I

    move-result p1

    aget p1, p2, p1

    const/4 p2, 0x1

    if-eq p1, p2, :cond_1

    const/4 p2, 0x2

    if-eq p1, p2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 182
    :cond_0
    sget p1, Lcom/squareup/marin/R$string;->content_description_navigate_up_button:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_0

    .line 181
    :cond_1
    sget p1, Lcom/squareup/marin/R$string;->content_description_navigate_x_button:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :goto_0
    if-eqz p1, :cond_2

    .line 185
    move-object p2, p1

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    .line 186
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getUpGlyphButton()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object p2

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/glyph/SquareGlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_2
    return-void
.end method

.method public final showLeftGlyphButton()V
    .locals 2

    .line 155
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getLeftGlyphButton()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    return-void
.end method

.method public final showRightButton()V
    .locals 2

    .line 138
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getRightButton()Lcom/squareup/noho/NohoButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    return-void
.end method

.method public final showUpGlyph()V
    .locals 2

    .line 199
    invoke-direct {p0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->getUpGlyphButton()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    return-void
.end method
