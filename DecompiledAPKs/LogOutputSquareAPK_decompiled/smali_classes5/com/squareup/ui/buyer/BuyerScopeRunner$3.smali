.class Lcom/squareup/ui/buyer/BuyerScopeRunner$3;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "BuyerScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToFirstAddCardScreen(Lcom/squareup/payment/PaymentReceipt;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field final synthetic val$receipt:Lcom/squareup/payment/PaymentReceipt;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/PaymentReceipt;)V
    .locals 0

    .line 714
    iput-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner$3;->this$0:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner$3;->val$receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 4

    .line 716
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner$3;->val$receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    .line 717
    instance-of v1, v0, Lcom/squareup/payment/BillPayment;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/squareup/payment/BillPayment;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 718
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner$3;->this$0:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-static {v2}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->access$000(Lcom/squareup/ui/buyer/BuyerScopeRunner;)Lcom/squareup/ui/buyer/BuyerScope;

    move-result-object v3

    invoke-static {v3, v0, v1}, Lcom/squareup/ui/crm/flow/CrmScope;->firstPostTransactionAddCardScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/payment/BillPayment;)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->access$100(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method
