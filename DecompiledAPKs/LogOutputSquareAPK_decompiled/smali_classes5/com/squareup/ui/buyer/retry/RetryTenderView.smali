.class public Lcom/squareup/ui/buyer/retry/RetryTenderView;
.super Landroid/widget/ScrollView;
.source "RetryTenderView.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/retry/RetryTenderView$ParentComponent;
    }
.end annotation


# instance fields
.field apiTransactionState:Lcom/squareup/api/ApiTransactionState;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private glyph:Lcom/squareup/glyph/SquareGlyphView;

.field private glyphMessage:Lcom/squareup/widgets/MessageView;

.field private glyphTitle:Landroid/widget/TextView;

.field private offlineButton:Landroid/widget/TextView;

.field private offlineButtonMessage:Lcom/squareup/widgets/MessageView;

.field presenter:Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private retryButton:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    const-class p2, Lcom/squareup/ui/buyer/retry/RetryTenderView$ParentComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/buyer/retry/RetryTenderView$ParentComponent;

    invoke-interface {p2, p0}, Lcom/squareup/ui/buyer/retry/RetryTenderView$ParentComponent;->inject(Lcom/squareup/ui/buyer/retry/RetryTenderView;)V

    .line 54
    sget p2, Lcom/squareup/checkout/R$layout;->auth_glyph_view:I

    const/4 v0, 0x0

    .line 55
    invoke-static {p1, p2, v0}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 56
    iget-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 86
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->buyer_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 87
    sget v0, Lcom/squareup/checkout/R$id;->glyph_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->glyphTitle:Landroid/widget/TextView;

    .line 88
    sget v0, Lcom/squareup/checkout/R$id;->glyph_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->glyphMessage:Lcom/squareup/widgets/MessageView;

    .line 89
    sget v0, Lcom/squareup/checkout/R$id;->payment_processing_primary_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->retryButton:Landroid/widget/TextView;

    .line 90
    sget v0, Lcom/squareup/checkout/R$id;->payment_processing_secondary_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->offlineButton:Landroid/widget/TextView;

    .line 91
    sget v0, Lcom/squareup/checkout/R$id;->payment_processing_action_hint:I

    .line 92
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->offlineButtonMessage:Lcom/squareup/widgets/MessageView;

    .line 93
    sget v0, Lcom/squareup/checkout/R$id;->glyph_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 102
    sget-object p1, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_TWEEN_Y:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method hideOfflineButton()V
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->offlineButton:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method hideOfflineMessage()V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->offlineButtonMessage:Lcom/squareup/widgets/MessageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$RetryTenderView()Lkotlin/Unit;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->presenter:Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->onBackPressed()Z

    .line 64
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public onBackPressed()Z
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->presenter:Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->presenter:Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->dropView(Ljava/lang/Object;)V

    .line 98
    invoke-super {p0}, Landroid/widget/ScrollView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 60
    invoke-super {p0}, Landroid/widget/ScrollView;->onFinishInflate()V

    .line 61
    invoke-direct {p0}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->bindViews()V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v2, Lcom/squareup/ui/buyer/retry/-$$Lambda$RetryTenderView$8FXXJ_XrCwHSwwkRdUqwpoaSN2A;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/retry/-$$Lambda$RetryTenderView$8FXXJ_XrCwHSwwkRdUqwpoaSN2A;-><init>(Lcom/squareup/ui/buyer/retry/RetryTenderView;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->glyphTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->connection_error_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->glyphMessage:Lcom/squareup/widgets/MessageView;

    iget-object v1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->connection_error_message:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->glyphMessage:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->retryButton:Landroid/widget/TextView;

    new-instance v2, Lcom/squareup/ui/buyer/retry/RetryTenderView$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/retry/RetryTenderView$1;-><init>(Lcom/squareup/ui/buyer/retry/RetryTenderView;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->retryButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->retry:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->retryButton:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->offlineButton:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->enter_offline_mode:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    sget-object v0, Lcom/squareup/ui/main/PosContainer;->Companion:Lcom/squareup/ui/main/PosContainer$Companion;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/PosContainer$Companion;->getThemeCardBackgroundDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyer/retry/RetryTenderView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->presenter:Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setOfflineButtonToEnterOnClick()V
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->offlineButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/buyer/retry/RetryTenderView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/retry/RetryTenderView$2;-><init>(Lcom/squareup/ui/buyer/retry/RetryTenderView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOfflineButtonToShowQuickEnableCardOnClick()V
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->offlineButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/buyer/retry/RetryTenderView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/retry/RetryTenderView$3;-><init>(Lcom/squareup/ui/buyer/retry/RetryTenderView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 114
    :cond_0
    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v0, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    move-object p1, v0

    .line 115
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method

.method public setTotal(Ljava/lang/CharSequence;)V
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v1, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method

.method showOfflineButton(IZ)V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->offlineButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 120
    iget-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->offlineButton:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    iget-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->offlineButton:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method showOfflineMessage(I)V
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->offlineButtonMessage:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 126
    iget-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView;->offlineButtonMessage:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method
