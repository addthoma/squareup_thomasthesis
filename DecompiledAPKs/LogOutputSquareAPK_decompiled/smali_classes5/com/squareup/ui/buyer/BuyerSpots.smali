.class public Lcom/squareup/ui/buyer/BuyerSpots;
.super Ljava/lang/Object;
.source "BuyerSpots.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/BuyerSpots$BuyerTweenYSpot;,
        Lcom/squareup/ui/buyer/BuyerSpots$BuyerRight;
    }
.end annotation


# static fields
.field public static final BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

.field public static final BUYER_TWEEN_Y:Lcom/squareup/container/spot/Spot;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 30
    new-instance v0, Lcom/squareup/ui/buyer/BuyerSpots$BuyerRight;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/buyer/BuyerSpots$BuyerRight;-><init>(Lcom/squareup/ui/buyer/BuyerSpots$1;)V

    sput-object v0, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    .line 37
    new-instance v0, Lcom/squareup/ui/buyer/BuyerSpots$BuyerTweenYSpot;

    invoke-direct {v0, v1}, Lcom/squareup/ui/buyer/BuyerSpots$BuyerTweenYSpot;-><init>(Lcom/squareup/ui/buyer/BuyerSpots$1;)V

    sput-object v0, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_TWEEN_Y:Lcom/squareup/container/spot/Spot;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$200(Landroid/view/View;)Z
    .locals 0

    .line 25
    invoke-static {p0}, Lcom/squareup/ui/buyer/BuyerSpots;->isTablet(Landroid/view/View;)Z

    move-result p0

    return p0
.end method

.method private static isTablet(Landroid/view/View;)Z
    .locals 1

    .line 40
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v0, Lcom/squareup/utilities/ui/R$bool;->sq_is_tablet:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p0

    return p0
.end method
