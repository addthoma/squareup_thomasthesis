.class public final Lcom/squareup/ui/buyer/loyalty/RewardTierView;
.super Landroid/widget/LinearLayout;
.source "RewardTierView.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRewardTierView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RewardTierView.kt\ncom/squareup/ui/buyer/loyalty/RewardTierView\n*L\n1#1,32:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0018\u00002\u00020\u0001B\u0019\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\u0010\u001a\u00020\u00002\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0012J\u0010\u0010\u0014\u001a\u00020\u00152\u0008\u0008\u0001\u0010\u0016\u001a\u00020\u0017R\u001b\u0010\u0007\u001a\u00020\u00088BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u000b\u0010\u000c\u001a\u0004\u0008\t\u0010\nR\u001b\u0010\r\u001a\u00020\u00088BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u000f\u0010\u000c\u001a\u0004\u0008\u000e\u0010\n\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/loyalty/RewardTierView;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "pointsAmount",
        "Lcom/squareup/widgets/MessageView;",
        "getPointsAmount",
        "()Lcom/squareup/widgets/MessageView;",
        "pointsAmount$delegate",
        "Lkotlin/Lazy;",
        "tierDescription",
        "getTierDescription",
        "tierDescription$delegate",
        "setRewardData",
        "pointsText",
        "",
        "tierText",
        "setTextColor",
        "",
        "textColor",
        "",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final pointsAmount$delegate:Lkotlin/Lazy;

.field private final tierDescription$delegate:Lkotlin/Lazy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    new-instance p1, Lcom/squareup/ui/buyer/loyalty/RewardTierView$pointsAmount$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/buyer/loyalty/RewardTierView$pointsAmount$2;-><init>(Lcom/squareup/ui/buyer/loyalty/RewardTierView;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/RewardTierView;->pointsAmount$delegate:Lkotlin/Lazy;

    .line 15
    new-instance p1, Lcom/squareup/ui/buyer/loyalty/RewardTierView$tierDescription$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/buyer/loyalty/RewardTierView$tierDescription$2;-><init>(Lcom/squareup/ui/buyer/loyalty/RewardTierView;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/RewardTierView;->tierDescription$delegate:Lkotlin/Lazy;

    return-void
.end method

.method private final getPointsAmount()Lcom/squareup/widgets/MessageView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/RewardTierView;->pointsAmount$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    return-object v0
.end method

.method private final getTierDescription()Lcom/squareup/widgets/MessageView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/RewardTierView;->tierDescription$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    return-object v0
.end method


# virtual methods
.method public final setRewardData(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/RewardTierView;
    .locals 1

    const-string v0, "pointsText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tierText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/buyer/loyalty/RewardTierView;->getPointsAmount()Lcom/squareup/widgets/MessageView;

    move-result-object v0

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 21
    invoke-direct {p0}, Lcom/squareup/ui/buyer/loyalty/RewardTierView;->getTierDescription()Lcom/squareup/widgets/MessageView;

    move-result-object p1

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/buyer/loyalty/RewardTierView;->getTierDescription()Lcom/squareup/widgets/MessageView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/widgets/MessageView;->getTextView()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/TextView;->setSingleLine()V

    .line 23
    invoke-direct {p0}, Lcom/squareup/ui/buyer/loyalty/RewardTierView;->getTierDescription()Lcom/squareup/widgets/MessageView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/widgets/MessageView;->getTextView()Landroid/widget/TextView;

    move-result-object p1

    const-string/jumbo p2, "tierDescription.textView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object p2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    return-object p0
.end method

.method public final setTextColor(I)V
    .locals 1

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/buyer/loyalty/RewardTierView;->getPointsAmount()Lcom/squareup/widgets/MessageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setTextColor(I)V

    .line 29
    invoke-direct {p0}, Lcom/squareup/ui/buyer/loyalty/RewardTierView;->getTierDescription()Lcom/squareup/widgets/MessageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setTextColor(I)V

    return-void
.end method
