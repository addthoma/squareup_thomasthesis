.class public final Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;
.super Ljava/lang/Object;
.source "InvoicePaidScreenDataFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoicePaidScreenDataFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoicePaidScreenDataFactory.kt\ncom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,168:1\n1550#2,3:169\n*E\n*S KotlinDebug\n*F\n+ 1 InvoicePaidScreenDataFactory.kt\ncom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory\n*L\n141#1,3:169\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B=\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015J\u0006\u0010\u0016\u001a\u00020\u0015J\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001aJ\u0012\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u0006J\u001a\u0010\u001e\u001a\u00020\u001c2\u0008\u0010\u001f\u001a\u0004\u0018\u00010 2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u0006J\u0012\u0010!\u001a\u00020\"2\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u0002J\u000c\u0010#\u001a\u00020\"*\u00020$H\u0002J\u000c\u0010%\u001a\u00020\"*\u00020$H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u00020\u00118BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0013R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "cardReaderHub",
        "Lcom/squareup/cardreader/CardReaderHub;",
        "offlineMode",
        "Lcom/squareup/payment/OfflineModeMonitor;",
        "customerManagementSettings",
        "Lcom/squareup/crm/CustomerManagementSettings;",
        "(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/crm/CustomerManagementSettings;)V",
        "receipt",
        "Lcom/squareup/payment/PaymentReceipt;",
        "getReceipt",
        "()Lcom/squareup/payment/PaymentReceipt;",
        "actionBarSubtitle",
        "Lcom/squareup/util/ViewString$TextString;",
        "actionBarTitle",
        "addCardGlyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "payment",
        "Lcom/squareup/payment/Payment;",
        "glyphSubtitle",
        "",
        "remainingBalance",
        "glyphTitle",
        "smartCardReaderId",
        "Lcom/squareup/cardreader/CardReaderId;",
        "shouldRemoveCard",
        "",
        "cardAlreadyOnFile",
        "Lcom/squareup/payment/BillPayment;",
        "cardButtonEnabled",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

.field private final res:Lcom/squareup/util/Res;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/crm/CustomerManagementSettings;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderHub"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "offlineMode"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerManagementSettings"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p4, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iput-object p5, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

    iput-object p6, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    return-void
.end method

.method private final cardAlreadyOnFile(Lcom/squareup/payment/BillPayment;)Z
    .locals 6

    .line 141
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->hasCustomer()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->getCustomerInstrumentDetails()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Ljava/lang/Iterable;

    .line 169
    instance-of v3, v0, Ljava/util/Collection;

    if-eqz v3, :cond_0

    move-object v3, v0

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 170
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    .line 142
    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->getCard()Lcom/squareup/Card;

    move-result-object v4

    const-string v5, "card"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v4}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactoryKt;->sameCard(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;Lcom/squareup/Card;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_3

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    return v1
.end method

.method private final cardButtonEnabled(Lcom/squareup/payment/BillPayment;)Z
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    .line 135
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->hasMagStripeTenderInFlight()Z

    move-result v1

    .line 136
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->hasEmvTenderInFlight()Z

    move-result p1

    .line 134
    invoke-interface {v0, v1, p1}, Lcom/squareup/crm/CustomerManagementSettings;->showCardButtonEnabledForTenderType(ZZ)Z

    move-result p1

    return p1
.end method

.method private final getReceipt()Lcom/squareup/payment/PaymentReceipt;
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    const-string/jumbo v1, "transaction.requireReceiptForLastPayment()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final shouldRemoveCard(Lcom/squareup/cardreader/CardReaderId;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 151
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v1, p1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresent()Z

    move-result v0

    :cond_1
    return v0
.end method


# virtual methods
.method public final actionBarSubtitle()Lcom/squareup/util/ViewString$TextString;
    .locals 11

    .line 59
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->asReturnsChange()Lcom/squareup/payment/tender/BaseTender$ReturnsChange;

    move-result-object v0

    .line 60
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getRemainingBalance()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 61
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->res:Lcom/squareup/util/Res;

    invoke-virtual {v2, v3}, Lcom/squareup/payment/PaymentReceipt;->getRemainingBalanceText(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v2

    .line 62
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v3

    const-string v4, "receipt.payment"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/payment/Payment;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v3

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    if-eqz v0, :cond_1

    .line 66
    invoke-interface {v0}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v8

    iget-object v8, v8, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v8, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v10, v8, v6

    if-eqz v10, :cond_1

    .line 67
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->buyer_send_receipt_title_no_change_sub:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 68
    iget-object v2, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string/jumbo v2, "total"

    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    :cond_1
    if-eqz v1, :cond_2

    .line 73
    move-object v0, v2

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_1

    :cond_2
    if-eqz v3, :cond_3

    .line 78
    iget-object v0, v3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v2, v0, v6

    if-lez v2, :cond_3

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkout/R$string;->buyer_payment_note_tip:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v2

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/payment/Payment;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "amount"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string/jumbo v2, "tip"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v5

    :goto_1
    if-eqz v0, :cond_4

    .line 86
    new-instance v5, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v5, v0}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    :cond_4
    return-object v5
.end method

.method public final actionBarTitle()Lcom/squareup/util/ViewString$TextString;
    .locals 7

    .line 39
    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->asReturnsChange()Lcom/squareup/payment/tender/BaseTender$ReturnsChange;

    move-result-object v0

    .line 40
    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    if-nez v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v2

    const-string v3, "receipt.payment"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/payment/Payment;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    .line 46
    :cond_0
    invoke-interface {v0}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getChange()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v2, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-eqz v6, :cond_2

    .line 47
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/checkout/R$string;->buyer_send_receipt_title_cash_change_only:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 48
    iget-object v3, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getChange()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {v3, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v3, "amount"

    invoke-virtual {v2, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    .line 53
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->buyer_send_receipt_title_no_change:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    :goto_1
    const-string/jumbo v2, "when {\n          // Not \u2026itle_no_change)\n        }"

    .line 41
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {v1, v0}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    return-object v1
.end method

.method public final addCardGlyph(Lcom/squareup/payment/Payment;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 2

    const-string v0, "payment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 126
    :cond_0
    instance-of v0, p1, Lcom/squareup/payment/BillPayment;

    if-nez v0, :cond_1

    goto :goto_0

    .line 127
    :cond_1
    check-cast p1, Lcom/squareup/payment/BillPayment;

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->cardButtonEnabled(Lcom/squareup/payment/BillPayment;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    .line 128
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->cardAlreadyOnFile(Lcom/squareup/payment/BillPayment;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    .line 129
    :cond_3
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SAVE_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    return-object v1
.end method

.method public final glyphSubtitle(Lcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 2

    .line 108
    invoke-static {p1}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactoryKt;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/buyerflow/R$string;->invoice_paid_remaining_balance:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 111
    iget-object v1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v1, "amount"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 112
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 113
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final glyphTitle(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 0

    .line 93
    invoke-static {p2}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactoryKt;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result p2

    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->shouldRemoveCard(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/cardreader/R$string;->please_remove_card_title:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 99
    iget-object p1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/ui/buyerflow/R$string;->invoice_paid:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 101
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/ui/buyerflow/R$string;->invoice_paid_transaction_complete:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method
