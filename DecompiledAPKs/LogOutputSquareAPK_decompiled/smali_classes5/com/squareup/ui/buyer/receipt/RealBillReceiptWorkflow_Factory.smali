.class public final Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealBillReceiptWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final buyerFlowReceiptManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerLocaleOverrideProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final receiptWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;->receiptWorkflowProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p2, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;->buyerFlowReceiptManagerProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p3, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p4, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p5, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p6, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;->offlineModeProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p7, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;)",
            "Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;"
        }
    .end annotation

    .line 66
    new-instance v8, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/buyer/language/BuyerLocaleOverride;)Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;
    .locals 9

    .line 74
    new-instance v8, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;
    .locals 8

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;->receiptWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;->buyerFlowReceiptManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/log/CheckoutInformationEventLogger;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/crm/CustomerManagementSettings;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/loyalty/LoyaltySettings;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;->offlineModeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;->newInstance(Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/buyer/language/BuyerLocaleOverride;)Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow_Factory;->get()Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;

    move-result-object v0

    return-object v0
.end method
