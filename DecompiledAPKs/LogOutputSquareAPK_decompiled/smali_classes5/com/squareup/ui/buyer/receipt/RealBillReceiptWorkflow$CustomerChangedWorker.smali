.class final Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$CustomerChangedWorker;
.super Lcom/squareup/workflow/rx2/PublisherWorker;
.source "RealBillReceiptWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CustomerChangedWorker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/rx2/PublisherWorker<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0014\u0010\u0006\u001a\u00020\u00072\n\u0010\u0008\u001a\u0006\u0012\u0002\u0008\u00030\tH\u0016J\u0016\u0010\n\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u00020\u00020\u000bH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$CustomerChangedWorker;",
        "Lcom/squareup/workflow/rx2/PublisherWorker;",
        "",
        "receipt",
        "Lcom/squareup/payment/PaymentReceipt;",
        "(Lcom/squareup/payment/PaymentReceipt;)V",
        "doesSameWorkAs",
        "",
        "otherWorker",
        "Lcom/squareup/workflow/Worker;",
        "runPublisher",
        "Lio/reactivex/Flowable;",
        "kotlin.jvm.PlatformType",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final receipt:Lcom/squareup/payment/PaymentReceipt;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/PaymentReceipt;)V
    .locals 1

    const-string v0, "receipt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    invoke-direct {p0}, Lcom/squareup/workflow/rx2/PublisherWorker;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$CustomerChangedWorker;->receipt:Lcom/squareup/payment/PaymentReceipt;

    return-void
.end method


# virtual methods
.method public doesSameWorkAs(Lcom/squareup/workflow/Worker;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Worker<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "otherWorker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    instance-of p1, p1, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$CustomerChangedWorker;

    return p1
.end method

.method public runPublisher()Lio/reactivex/Flowable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Flowable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$CustomerChangedWorker;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->onCustomerChanged()Lrx/Observable;

    move-result-object v0

    const-string v1, "receipt.payment.onCustomerChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Flowable(Lrx/Observable;)Lio/reactivex/Flowable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic runPublisher()Lorg/reactivestreams/Publisher;
    .locals 1

    .line 216
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$CustomerChangedWorker;->runPublisher()Lio/reactivex/Flowable;

    move-result-object v0

    check-cast v0, Lorg/reactivestreams/Publisher;

    return-object v0
.end method
