.class public final Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "ReceiptBootstrapScreen.kt"

# interfaces
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002Bw\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00060\n\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0006\u0012\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u000f\u0012\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u000f\u0012\u0006\u0010\u0017\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u0008\u0010\u001e\u001a\u00020\u0004H\u0016R\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0019R\u000e\u0010\u0017\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00060\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "Lcom/squareup/ui/buyer/PaymentExempt;",
        "buyerScope",
        "Lcom/squareup/ui/buyer/BuyerScope;",
        "uniqueKey",
        "",
        "receipt",
        "Lcom/squareup/payment/PaymentReceipt;",
        "getRemainingBalanceText",
        "Lkotlin/Function1;",
        "Lcom/squareup/util/Res;",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "showLanguageSelection",
        "",
        "displayName",
        "mostRecentActiveCardReaderId",
        "Lcom/squareup/cardreader/CardReaderId;",
        "supportsSms",
        "autoReceiptCompleteTimeout",
        "",
        "showSmsMarketing",
        "businessName",
        "(Lcom/squareup/ui/buyer/BuyerScope;Ljava/lang/String;Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)V",
        "Ljava/lang/Long;",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final autoReceiptCompleteTimeout:Ljava/lang/Long;

.field private final businessName:Ljava/lang/String;

.field private final buyerScope:Lcom/squareup/ui/buyer/BuyerScope;

.field private final countryCode:Lcom/squareup/CountryCode;

.field private final displayName:Ljava/lang/String;

.field private final getRemainingBalanceText:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field private final receipt:Lcom/squareup/payment/PaymentReceipt;

.field private final showLanguageSelection:Z

.field private final showSmsMarketing:Z

.field private final supportsSms:Z

.field private final uniqueKey:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;Ljava/lang/String;Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/BuyerScope;",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/PaymentReceipt;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/CountryCode;",
            "Z",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/CardReaderId;",
            "Z",
            "Ljava/lang/Long;",
            "Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "buyerScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "uniqueKey"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "receipt"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getRemainingBalanceText"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "businessName"

    invoke-static {p12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->buyerScope:Lcom/squareup/ui/buyer/BuyerScope;

    iput-object p2, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->uniqueKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->receipt:Lcom/squareup/payment/PaymentReceipt;

    iput-object p4, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    iput-object p5, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->countryCode:Lcom/squareup/CountryCode;

    iput-boolean p6, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->showLanguageSelection:Z

    iput-object p7, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->displayName:Ljava/lang/String;

    iput-object p8, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iput-boolean p9, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->supportsSms:Z

    iput-object p10, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    iput-boolean p11, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->showSmsMarketing:Z

    iput-object p12, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->businessName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 13

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    sget-object v0, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;->Companion:Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;

    move-result-object p1

    .line 34
    new-instance v12, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;

    .line 35
    iget-object v1, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->uniqueKey:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->receipt:Lcom/squareup/payment/PaymentReceipt;

    iget-object v3, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    iget-object v4, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->countryCode:Lcom/squareup/CountryCode;

    iget-boolean v5, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->showLanguageSelection:Z

    .line 36
    iget-object v6, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->displayName:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iget-boolean v8, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->supportsSms:Z

    iget-object v9, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    .line 37
    iget-boolean v10, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->showSmsMarketing:Z

    iget-object v11, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->businessName:Ljava/lang/String;

    move-object v0, v12

    .line 34
    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;-><init>(Ljava/lang/String;Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)V

    check-cast v12, Lcom/squareup/ui/buyer/BuyerScopeInput;

    .line 33
    invoke-interface {p1, v12}, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;->start(Lcom/squareup/ui/buyer/BuyerScopeInput;)V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ui/buyer/BuyerScope;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->buyerScope:Lcom/squareup/ui/buyer/BuyerScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;->getParentKey()Lcom/squareup/ui/buyer/BuyerScope;

    move-result-object v0

    return-object v0
.end method
