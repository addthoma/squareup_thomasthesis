.class Lcom/squareup/ui/buyer/BuyerSpots$BuyerTweenYSpot;
.super Lcom/squareup/container/spot/Spot;
.source "BuyerSpots.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/BuyerSpots;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BuyerTweenYSpot"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/BuyerSpots$BuyerTweenYSpot;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/buyer/BuyerSpots$BuyerTweenYSpot;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 108
    new-instance v0, Lcom/squareup/ui/buyer/BuyerSpots$BuyerTweenYSpot;

    invoke-direct {v0}, Lcom/squareup/ui/buyer/BuyerSpots$BuyerTweenYSpot;-><init>()V

    sput-object v0, Lcom/squareup/ui/buyer/BuyerSpots$BuyerTweenYSpot;->INSTANCE:Lcom/squareup/ui/buyer/BuyerSpots$BuyerTweenYSpot;

    .line 154
    sget-object v0, Lcom/squareup/ui/buyer/BuyerSpots$BuyerTweenYSpot;->INSTANCE:Lcom/squareup/ui/buyer/BuyerSpots$BuyerTweenYSpot;

    invoke-static {v0}, Lcom/squareup/ui/buyer/BuyerSpots$BuyerTweenYSpot;->forSpotSingleton(Lcom/squareup/container/spot/Spot;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/BuyerSpots$BuyerTweenYSpot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 107
    invoke-direct {p0}, Lcom/squareup/container/spot/Spot;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/buyer/BuyerSpots$1;)V
    .locals 0

    .line 107
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerSpots$BuyerTweenYSpot;-><init>()V

    return-void
.end method


# virtual methods
.method protected backwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .line 132
    invoke-static {}, Lcom/squareup/container/spot/ExchangeSet;->createSet()Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 133
    invoke-virtual {v0, p4}, Lcom/squareup/container/spot/ExchangeSet$Builder;->standardDuration(Landroid/view/View;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 134
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->fadeIn()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->defaultExchange(Lcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    .line 135
    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->skipExchange(I)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$id;->buyer_floating_content:I

    .line 136
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->tweenYAndFadeIn()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchange(ILcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 137
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/container/spot/ExchangeSet$Builder;->start(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method protected backwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .line 142
    invoke-static {}, Lcom/squareup/container/spot/ExchangeSet;->createSet()Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 143
    invoke-virtual {v0, p4}, Lcom/squareup/container/spot/ExchangeSet$Builder;->standardDuration(Landroid/view/View;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 144
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->fadeOut()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->defaultExchange(Lcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    .line 145
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->hide()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchange(ILcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$id;->buyer_floating_content:I

    .line 146
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->tweenYAndFadeOut()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchange(ILcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 147
    invoke-virtual {v0, p1, p2, p4, p3}, Lcom/squareup/container/spot/ExchangeSet$Builder;->start(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method protected forwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .line 112
    invoke-static {}, Lcom/squareup/container/spot/ExchangeSet;->createSet()Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 113
    invoke-virtual {v0, p3}, Lcom/squareup/container/spot/ExchangeSet$Builder;->standardDuration(Landroid/view/View;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 114
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->fadeIn()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->defaultExchange(Lcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    .line 115
    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->skipExchange(I)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$id;->buyer_floating_content:I

    .line 116
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->tweenYAndFadeIn()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchange(ILcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 117
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/container/spot/ExchangeSet$Builder;->start(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method protected forwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .line 122
    invoke-static {}, Lcom/squareup/container/spot/ExchangeSet;->createSet()Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 123
    invoke-virtual {v0, p4}, Lcom/squareup/container/spot/ExchangeSet$Builder;->standardDuration(Landroid/view/View;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 124
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->fadeOut()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->defaultExchange(Lcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    .line 125
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->hide()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchange(ILcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$id;->buyer_floating_content:I

    .line 126
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->tweenYAndFadeOut()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchange(ILcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 127
    invoke-virtual {v0, p1, p2, p4, p3}, Lcom/squareup/container/spot/ExchangeSet$Builder;->start(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method public skipTemporaryBackground()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
