.class public final Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "SignLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->renderSignatureView(Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lcom/squareup/util/Res;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$2",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $submitSignature:Lkotlin/jvm/functions/Function0;

.field final synthetic this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0;",
            ")V"
        }
    .end annotation

    .line 153
    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$2;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    iput-object p2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$2;->$submitSignature:Lkotlin/jvm/functions/Function0;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 155
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$2;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    invoke-static {p1}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->access$getSignatureView$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)Lcom/squareup/signature/SignatureView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/signature/SignatureView;->clearBitmap()V

    .line 156
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$2;->$submitSignature:Lkotlin/jvm/functions/Function0;

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method
