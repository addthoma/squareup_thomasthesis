.class public final Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;
.super Ljava/lang/Object;
.source "SignScreenRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/signature/SignScreenRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerAmountTextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerLocaleOverrideProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tipDeterminerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p2, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p3, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p4, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->buyerAmountTextProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p5, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p6, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p7, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p8, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p9, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->moneyLocaleHelperProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p10, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->tipDeterminerFactoryProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p11, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;"
        }
    .end annotation

    .line 85
    new-instance v12, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/util/Device;Lcom/squareup/payment/Transaction;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lflow/Flow;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/buyer/signature/SignScreenRunner;
    .locals 13

    .line 93
    new-instance v12, Lcom/squareup/ui/buyer/signature/SignScreenRunner;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;-><init>(Lcom/squareup/util/Device;Lcom/squareup/payment/Transaction;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lflow/Flow;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/analytics/Analytics;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/signature/SignScreenRunner;
    .locals 12

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->buyerAmountTextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->moneyLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/money/MoneyLocaleHelper;

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->tipDeterminerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/payment/TipDeterminerFactory;

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/analytics/Analytics;

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->newInstance(Lcom/squareup/util/Device;Lcom/squareup/payment/Transaction;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lflow/Flow;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/buyer/signature/SignScreenRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/signature/SignScreenRunner_Factory;->get()Lcom/squareup/ui/buyer/signature/SignScreenRunner;

    move-result-object v0

    return-object v0
.end method
