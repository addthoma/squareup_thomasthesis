.class public final Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;
.super Ljava/lang/Object;
.source "SignLayoutRunner_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final agreementBuilderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/signature/AgreementBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final picassoCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Cache;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Cache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/signature/AgreementBuilder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;->picassoCacheProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;->agreementBuilderProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;->moneyLocaleHelperProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Cache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/signature/AgreementBuilder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/picasso/Cache;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/ui/buyer/signature/AgreementBuilder;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;
    .locals 7

    .line 58
    new-instance v6, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;-><init>(Lcom/squareup/picasso/Cache;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/ui/buyer/signature/AgreementBuilder;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;->picassoCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/picasso/Cache;

    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/log/OhSnapLogger;

    iget-object v2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;->agreementBuilderProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/buyer/signature/AgreementBuilder;

    iget-object v3, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;->moneyLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/money/MoneyLocaleHelper;

    iget-object v4, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;->newInstance(Lcom/squareup/picasso/Cache;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/ui/buyer/signature/AgreementBuilder;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner_Factory_Factory;->get()Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;

    move-result-object v0

    return-object v0
.end method
