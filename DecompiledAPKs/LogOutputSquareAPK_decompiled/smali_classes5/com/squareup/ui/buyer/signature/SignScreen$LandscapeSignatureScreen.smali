.class final Lcom/squareup/ui/buyer/signature/SignScreen$LandscapeSignatureScreen;
.super Lcom/squareup/ui/buyer/signature/SignScreen;
.source "SignScreen.java"

# interfaces
.implements Lcom/squareup/container/LocksOrientation;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/buyer/signature/SignScreen$Component;
.end annotation

.annotation runtime Lcom/squareup/ui/buyer/RequiresBuyerInteraction;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/signature/SignScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "LandscapeSignatureScreen"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/signature/SignScreen$LandscapeSignatureScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/ui/buyer/signature/-$$Lambda$SignScreen$LandscapeSignatureScreen$4kbUfpQVfNHpwFvACtbq4kVJKtQ;->INSTANCE:Lcom/squareup/ui/buyer/signature/-$$Lambda$SignScreen$LandscapeSignatureScreen$4kbUfpQVfNHpwFvACtbq4kVJKtQ;

    .line 75
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/signature/SignScreen$LandscapeSignatureScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;)V
    .locals 1

    const/4 v0, 0x0

    .line 63
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/buyer/signature/SignScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/ui/buyer/signature/SignScreen$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/ui/buyer/signature/SignScreen$1;)V
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/signature/SignScreen$LandscapeSignatureScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/signature/SignScreen$LandscapeSignatureScreen;
    .locals 1

    .line 76
    const-class v0, Lcom/squareup/ui/buyer/signature/SignScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/BuyerScope;

    .line 77
    new-instance v0, Lcom/squareup/ui/buyer/signature/SignScreen$LandscapeSignatureScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/signature/SignScreen$LandscapeSignatureScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0
.end method


# virtual methods
.method public getOrientationForPhone()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 67
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->SENSOR_LANDSCAPE:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public getOrientationForTablet()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 71
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method
