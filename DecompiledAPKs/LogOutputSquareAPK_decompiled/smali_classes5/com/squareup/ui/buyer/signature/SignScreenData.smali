.class public final Lcom/squareup/ui/buyer/signature/SignScreenData;
.super Ljava/lang/Object;
.source "SignScreenData.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;,
        Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;,
        Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;,
        Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;,
        Lcom/squareup/ui/buyer/signature/SignScreenData$TipOptionInfo;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0005%&\'()B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\tH\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u000bH\u00c6\u0003J;\u0010\u001c\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000bH\u00c6\u0001J\u0013\u0010\u001d\u001a\u00020\u001e2\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u00d6\u0003J\t\u0010!\u001a\u00020\"H\u00d6\u0001J\t\u0010#\u001a\u00020$H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/signature/SignScreenData;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "res",
        "Lcom/squareup/util/Res;",
        "headerInformation",
        "Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;",
        "signatureConfig",
        "Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;",
        "returnPolicy",
        "Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;",
        "tipConfig",
        "Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;",
        "(Lcom/squareup/util/Res;Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;)V",
        "getHeaderInformation",
        "()Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "getReturnPolicy",
        "()Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;",
        "getSignatureConfig",
        "()Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;",
        "getTipConfig",
        "()Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "HeaderInformation",
        "ReturnPolicy",
        "SignatureConfig",
        "TipConfig",
        "TipOptionInfo",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final headerInformation:Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;

.field private final res:Lcom/squareup/util/Res;

.field private final returnPolicy:Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;

.field private final signatureConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

.field private final tipConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;)V
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "headerInformation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureConfig"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "returnPolicy"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tipConfig"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->headerInformation:Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;

    iput-object p3, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->signatureConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    iput-object p4, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->returnPolicy:Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;

    iput-object p5, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->tipConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/buyer/signature/SignScreenData;Lcom/squareup/util/Res;Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;ILjava/lang/Object;)Lcom/squareup/ui/buyer/signature/SignScreenData;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->res:Lcom/squareup/util/Res;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->headerInformation:Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->signatureConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->returnPolicy:Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->tipConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/ui/buyer/signature/SignScreenData;->copy(Lcom/squareup/util/Res;Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;)Lcom/squareup/ui/buyer/signature/SignScreenData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/util/Res;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method public final component2()Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->headerInformation:Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;

    return-object v0
.end method

.method public final component3()Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->signatureConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    return-object v0
.end method

.method public final component4()Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->returnPolicy:Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;

    return-object v0
.end method

.method public final component5()Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->tipConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;

    return-object v0
.end method

.method public final copy(Lcom/squareup/util/Res;Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;)Lcom/squareup/ui/buyer/signature/SignScreenData;
    .locals 7

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "headerInformation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureConfig"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "returnPolicy"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tipConfig"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/buyer/signature/SignScreenData;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/buyer/signature/SignScreenData;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/buyer/signature/SignScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/buyer/signature/SignScreenData;

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->res:Lcom/squareup/util/Res;

    iget-object v1, p1, Lcom/squareup/ui/buyer/signature/SignScreenData;->res:Lcom/squareup/util/Res;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->headerInformation:Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;

    iget-object v1, p1, Lcom/squareup/ui/buyer/signature/SignScreenData;->headerInformation:Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->signatureConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    iget-object v1, p1, Lcom/squareup/ui/buyer/signature/SignScreenData;->signatureConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->returnPolicy:Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;

    iget-object v1, p1, Lcom/squareup/ui/buyer/signature/SignScreenData;->returnPolicy:Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->tipConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;

    iget-object p1, p1, Lcom/squareup/ui/buyer/signature/SignScreenData;->tipConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHeaderInformation()Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->headerInformation:Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;

    return-object v0
.end method

.method public final getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method public final getReturnPolicy()Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->returnPolicy:Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;

    return-object v0
.end method

.method public final getSignatureConfig()Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->signatureConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    return-object v0
.end method

.method public final getTipConfig()Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->tipConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->res:Lcom/squareup/util/Res;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->headerInformation:Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->signatureConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->returnPolicy:Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->tipConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SignScreenData(res="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", headerInformation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->headerInformation:Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", signatureConfig="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->signatureConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", returnPolicy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->returnPolicy:Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tipConfig="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenData;->tipConfig:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
