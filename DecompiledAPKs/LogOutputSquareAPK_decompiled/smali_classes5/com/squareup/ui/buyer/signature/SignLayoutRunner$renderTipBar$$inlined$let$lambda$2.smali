.class public final Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "SignLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->renderTipBar(Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;Lcom/squareup/util/Res;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$1$2",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $res$inlined:Lcom/squareup/util/Res;

.field final synthetic $tipConfig$inlined:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

.field final synthetic this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;Lcom/squareup/util/Res;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$2;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    iput-object p2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$2;->$tipConfig$inlined:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

    iput-object p3, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$2;->$res$inlined:Lcom/squareup/util/Res;

    .line 295
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 297
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$2;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    invoke-static {p1}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->access$showCustomTip(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)V

    .line 298
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$2;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    invoke-static {p1}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->access$getXButton$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)Lcom/squareup/glyph/SquareGlyphView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 299
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$2;->$tipConfig$inlined:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->getClearTip()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method
