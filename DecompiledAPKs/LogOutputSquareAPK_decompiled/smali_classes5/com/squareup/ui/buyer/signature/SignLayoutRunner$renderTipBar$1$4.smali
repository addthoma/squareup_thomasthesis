.class final Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$1$4;
.super Ljava/lang/Object;
.source "SignLayoutRunner.kt"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->renderTipBar(Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;Lcom/squareup/util/Res;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "view",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "focused",
        "",
        "onFocusChange"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$1$4;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$1$4;

    invoke-direct {v0}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$1$4;-><init>()V

    sput-object v0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$1$4;->INSTANCE:Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$1$4;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 0

    if-nez p2, :cond_0

    const-string/jumbo p2, "view"

    .line 319
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    :cond_0
    return-void
.end method
