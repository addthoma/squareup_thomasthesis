.class public final Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter_Factory;
.super Ljava/lang/Object;
.source "PostAuthCouponPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final buyerAmountTextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final formatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter_Factory;->formatterProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter_Factory;->buyerAmountTextProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;)",
            "Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter_Factory;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;
    .locals 1

    .line 54
    new-instance v0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/Transaction;

    iget-object v2, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter_Factory;->formatterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    iget-object v3, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter_Factory;->buyerAmountTextProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter_Factory;->newInstance(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter_Factory;->get()Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;

    move-result-object v0

    return-object v0
.end method
