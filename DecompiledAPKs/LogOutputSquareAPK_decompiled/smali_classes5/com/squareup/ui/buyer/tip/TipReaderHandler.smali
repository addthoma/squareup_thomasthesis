.class public final Lcom/squareup/ui/buyer/tip/TipReaderHandler;
.super Ljava/lang/Object;
.source "TipReaderHandler.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B?\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0006\u0010\u0015\u001a\u00020\u0016J\u0006\u0010\u0017\u001a\u00020\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/tip/TipReaderHandler;",
        "",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "emvDipScreenHandler",
        "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
        "defaultEmvCardInsertRemoveProcessor",
        "Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;",
        "cardReaderHub",
        "Lcom/squareup/cardreader/CardReaderHub;",
        "activeCardReader",
        "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
        "tenderInEdit",
        "Lcom/squareup/payment/TenderInEdit;",
        "readerIssueScreenRequestSink",
        "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
        "(Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)V",
        "cardReaderAttachListener",
        "Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;",
        "emvCardInsertRemoveProcessor",
        "Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;",
        "register",
        "",
        "unregister",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private cardReaderAttachListener:Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final defaultEmvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;

.field private emvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private final readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emvDipScreenHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultEmvCardInsertRemoveProcessor"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderHub"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activeCardReader"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderInEdit"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "readerIssueScreenRequestSink"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p2, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    iput-object p3, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->defaultEmvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;

    iput-object p4, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iput-object p5, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iput-object p6, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    iput-object p7, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    return-void
.end method

.method public static final synthetic access$getActiveCardReader$p(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)Lcom/squareup/cardreader/dipper/ActiveCardReader;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    return-object p0
.end method

.method public static final synthetic access$getDefaultEmvCardInsertRemoveProcessor$p(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->defaultEmvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;

    return-object p0
.end method

.method public static final synthetic access$getReaderIssueScreenRequestSink$p(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    return-object p0
.end method

.method public static final synthetic access$getTenderInEdit$p(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)Lcom/squareup/payment/TenderInEdit;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    return-object p0
.end method

.method public static final synthetic access$getTransaction$p(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method


# virtual methods
.method public final register()V
    .locals 3

    .line 48
    new-instance v0, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$1;-><init>(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)V

    check-cast v0, Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    iput-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->emvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    iget-object v1, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->emvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    if-nez v1, :cond_0

    const-string v2, "emvCardInsertRemoveProcessor"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->addEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    .line 59
    new-instance v0, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$2;-><init>(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)V

    check-cast v0, Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    iput-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->cardReaderAttachListener:Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->cardReaderAttachListener:Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    if-nez v1, :cond_1

    const-string v2, "cardReaderAttachListener"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    return-void
.end method

.method public final unregister()V
    .locals 3

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    iget-object v1, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->emvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    if-nez v1, :cond_0

    const-string v2, "emvCardInsertRemoveProcessor"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->removeEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)Z

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->cardReaderAttachListener:Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    if-nez v1, :cond_1

    const-string v2, "cardReaderAttachListener"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderHub;->removeCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    return-void
.end method
