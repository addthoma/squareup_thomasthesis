.class public Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$NoOpCofDippedCardInfoProcessor;
.super Ljava/lang/Object;
.source "CofDippedCardInfoProcessor.java"

# interfaces
.implements Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NoOpCofDippedCardInfoProcessor"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .line 112
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected call to CofDippedCardInfoProcessor method"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCardInserted()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 104
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected call to CofDippedCardInfoProcessor method"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onDipResult()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;",
            ">;"
        }
    .end annotation

    .line 100
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected call to CofDippedCardInfoProcessor method"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public processDip()V
    .locals 2

    .line 108
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected call to CofDippedCardInfoProcessor method"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 116
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected call to CofDippedCardInfoProcessor method"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
