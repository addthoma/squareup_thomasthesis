.class public interface abstract Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;
.super Ljava/lang/Object;
.source "CreateNoteScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/CreateNoteScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeCreateNoteScreen()V
.end method

.method public abstract closeCreateNoteScreen(Lcom/squareup/protos/client/rolodex/Contact;)V
.end method

.method public abstract getContactForCreateNoteScreen()Lcom/squareup/protos/client/rolodex/Contact;
.end method

.method public abstract getReminder()Lcom/squareup/protos/client/rolodex/Reminder;
.end method

.method public abstract showReminderScreen(Lcom/squareup/protos/client/rolodex/Reminder;)V
.end method
