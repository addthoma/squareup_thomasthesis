.class public interface abstract Lcom/squareup/ui/crm/cards/ReminderScreen$Runner;
.super Ljava/lang/Object;
.source "ReminderScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ReminderScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeReminderScreen()V
.end method

.method public abstract closeReminderScreen(Lcom/squareup/protos/client/rolodex/Reminder;)V
.end method

.method public abstract getReminder()Lcom/squareup/protos/client/rolodex/Reminder;
.end method
