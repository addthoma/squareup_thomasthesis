.class public Lcom/squareup/ui/crm/cards/AddCouponScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "AddCouponScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/AddCouponScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/AddCouponScreen$Component;,
        Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;,
        Lcom/squareup/ui/crm/cards/AddCouponScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/AddCouponScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 139
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCouponScreen$AoxSk8Y0TFwFH2XcAKigjPuOccA;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$AddCouponScreen$AoxSk8Y0TFwFH2XcAKigjPuOccA;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/AddCouponScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AddCouponScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/AddCouponScreen;
    .locals 1

    .line 140
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 141
    new-instance v0, Lcom/squareup/ui/crm/cards/AddCouponScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/AddCouponScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 135
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/AddCouponScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 58
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 54
    sget v0, Lcom/squareup/crmscreens/R$layout;->crm_add_coupon_view:I

    return v0
.end method
