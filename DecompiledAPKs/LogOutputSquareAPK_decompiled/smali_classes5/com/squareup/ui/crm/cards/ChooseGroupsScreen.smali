.class public Lcom/squareup/ui/crm/cards/ChooseGroupsScreen;
.super Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;
.source "ChooseGroupsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Component;,
        Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;,
        Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/ChooseGroupsScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 269
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseGroupsScreen$VfCrrDQlAmwj81WiP90rZ8IImAM;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseGroupsScreen$VfCrrDQlAmwj81WiP90rZ8IImAM;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/ChooseGroupsScreen;
    .locals 1

    .line 270
    const-class v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    .line 271
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    .line 272
    new-instance v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 265
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 266
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 46
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CHOOSE_GROUPS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 50
    sget v0, Lcom/squareup/crmupdatecustomer/R$layout;->crm_choose_groups_view:I

    return v0
.end method
