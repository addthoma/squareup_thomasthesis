.class public final synthetic Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$vFxPZPc49wHAWVt9Fe0i6qQYZX8;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$vFxPZPc49wHAWVt9Fe0i6qQYZX8;->f$0:Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$vFxPZPc49wHAWVt9Fe0i6qQYZX8;->f$0:Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;->showError(Z)V

    return-void
.end method
