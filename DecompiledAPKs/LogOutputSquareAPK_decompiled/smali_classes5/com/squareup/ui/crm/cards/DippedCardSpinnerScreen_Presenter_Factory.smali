.class public final Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "DippedCardSpinnerScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final activeCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final cofDippedCardInfoProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;->cofDippedCardInfoProcessorProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p5, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;)",
            "Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;"
        }
    .end annotation

    .line 48
    new-instance v6, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;
    .locals 7

    .line 54
    new-instance v6, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;-><init>(Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;
    .locals 5

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;->cofDippedCardInfoProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;->newInstance(Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen_Presenter_Factory;->get()Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
