.class public final synthetic Lcom/squareup/ui/crm/cards/-$$Lambda$Q1OWHH8J2eGDNEPohIiSMbya4g4;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Function;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$Q1OWHH8J2eGDNEPohIiSMbya4g4;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$Q1OWHH8J2eGDNEPohIiSMbya4g4;

    invoke-direct {v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$Q1OWHH8J2eGDNEPohIiSMbya4g4;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$Q1OWHH8J2eGDNEPohIiSMbya4g4;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$Q1OWHH8J2eGDNEPohIiSMbya4g4;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/protos/client/rolodex/Group;

    invoke-static {p1}, Lcom/squareup/crm/util/RolodexGroupHelper;->canBeSaved(Lcom/squareup/protos/client/rolodex/Group;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
