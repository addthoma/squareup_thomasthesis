.class Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;
.super Lmortar/Presenter;
.source "DeletingCustomersPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;",
        ">;"
    }
.end annotation


# static fields
.field static final AUTO_CLOSE_SECONDS:J = 0x3L

.field static final MIN_LATENCY_SECONDS:J = 0x1L


# instance fields
.field private final autoClose:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final controller:Lcom/squareup/ui/crm/cards/DeletingCustomersScreen$Controller;

.field private final holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final res:Lcom/squareup/util/Res;

.field private final response:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/DeleteContactsResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/DeletingCustomersScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexServiceHelper;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/payment/crm/HoldsCustomer;)V
    .locals 1
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 49
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 45
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->response:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->controller:Lcom/squareup/ui/crm/cards/DeletingCustomersScreen$Controller;

    .line 51
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->res:Lcom/squareup/util/Res;

    .line 52
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 53
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 54
    iput-object p6, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    .line 56
    new-instance p1, Lcom/squareup/util/RxWatchdog;

    invoke-direct {p1, p4, p5}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    return-void
.end method

.method private formatDoneTitle(I)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/applet/R$string;->crm_customers_deleted_one_format:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_customers_deleted_many_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "number"

    .line 114
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 115
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Ljava/lang/Long;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p0
.end method


# virtual methods
.method protected extractBundleService(Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;)Lmortar/bundler/BundleService;
    .locals 0

    .line 60
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->extractBundleService(Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$1$DeletingCustomersPresenter(Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;Lcom/squareup/protos/client/rolodex/DeleteContactsResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 85
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;->showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 86
    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/DeleteContactsResponse;->batch_action_status:Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;->total_success_count:Ljava/lang/Integer;

    .line 87
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    .line 86
    invoke-direct {p0, p2}, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->formatDoneTitle(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;->showText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    const/4 p2, 0x0

    invoke-interface {p1, p2, p2, p2}, Lcom/squareup/payment/crm/HoldsCustomer;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->controller:Lcom/squareup/ui/crm/cards/DeletingCustomersScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/DeletingCustomersScreen$Controller;->success()V

    return-void
.end method

.method public synthetic lambda$null$2$DeletingCustomersPresenter(Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 96
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;->showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 97
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/applet/R$string;->crm_customers_deleted_error:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;->showText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public synthetic lambda$null$3$DeletingCustomersPresenter(Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 82
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;->hideProgress()V

    .line 84
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$9DJwoYHC69V1j0UiFsA7Bko0cxQ;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$9DJwoYHC69V1j0UiFsA7Bko0cxQ;-><init>(Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;)V

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$4AL3lNkM5PHSYNvt5CQwkBBPzQs;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$4AL3lNkM5PHSYNvt5CQwkBBPzQs;-><init>(Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3

    invoke-virtual {p1, p2, v1, v2, v0}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method public synthetic lambda$null$5$DeletingCustomersPresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 106
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->controller:Lcom/squareup/ui/crm/cards/DeletingCustomersScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/DeletingCustomersScreen$Controller;->closeDeletingCustomersScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$4$DeletingCustomersPresenter(Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->response:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$zdeWqMeKc2ooj94-mbStW-zsYaU;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$zdeWqMeKc2ooj94-mbStW-zsYaU;-><init>(Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$DeletingCustomersPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {v0}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$u-zbmerc5mmMoajArehHt_C2JeA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$u-zbmerc5mmMoajArehHt_C2JeA;-><init>(Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;)V

    .line 106
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 5

    .line 64
    invoke-super {p0, p1}, Lmortar/Presenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->controller:Lcom/squareup/ui/crm/cards/DeletingCustomersScreen$Controller;

    .line 68
    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/DeletingCustomersScreen$Controller;->getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/crm/RolodexServiceHelper;->deleteContacts(Lcom/squareup/protos/client/rolodex/ContactSet;)Lio/reactivex/Single;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v3, 0x1

    .line 69
    invoke-static {v3, v4, v1, v2}, Lio/reactivex/Single;->timer(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$AWLkeCpWvMXIkzMotdH04hq56q8;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$AWLkeCpWvMXIkzMotdH04hq56q8;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Single;->zipWith(Lio/reactivex/SingleSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->response:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 70
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 67
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 74
    invoke-super {p0, p1}, Lmortar/Presenter;->onLoad(Landroid/os/Bundle;)V

    .line 75
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_deleting_customers:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;->showText(Ljava/lang/CharSequence;)V

    .line 80
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;->getView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$7XW45D-F0Xrmmp7bF6a5etf0olI;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$7XW45D-F0Xrmmp7bF6a5etf0olI;-><init>(Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 104
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/DeletingCustomersDialog;->getView()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$zdA_bpQJ-XqLvymg0Vslf__BZxs;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingCustomersPresenter$zdA_bpQJ-XqLvymg0Vslf__BZxs;-><init>(Lcom/squareup/ui/crm/cards/DeletingCustomersPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
