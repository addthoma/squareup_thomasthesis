.class Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "SendMessageScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/SendMessageScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/SendMessageView;",
        ">;"
    }
.end annotation


# static fields
.field private static final KEY_UNIQUE_KEY:Ljava/lang/String; = "uniqueKey"


# instance fields
.field private final addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final busy:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final dialogue:Lcom/squareup/crm/DialogueServiceHelper;

.field private final errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final onSendClicked:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;

.field private sendMessageDisposable:Lio/reactivex/disposables/Disposable;

.field private final shortMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private uniqueKey:Ljava/util/UUID;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/coupon/AddCouponState;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;Lcom/squareup/crm/DialogueServiceHelper;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/coupon/AddCouponState;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;",
            "Lcom/squareup/crm/DialogueServiceHelper;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 116
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const/4 v0, 0x0

    .line 107
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 108
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->onSendClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 110
    invoke-static {}, Lio/reactivex/disposables/Disposables;->empty()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->sendMessageDisposable:Lio/reactivex/disposables/Disposable;

    .line 117
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

    .line 118
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 119
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;

    .line 120
    iput-object p5, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    .line 121
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->dialogue:Lcom/squareup/crm/DialogueServiceHelper;

    .line 122
    iput-object p6, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    .line 123
    iput-object p7, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 124
    iput-object p8, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    const/4 p1, 0x1

    .line 126
    invoke-virtual {p5, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->setMaxMessages(I)V

    return-void
.end method

.method static synthetic lambda$null$2(Lcom/squareup/ui/crm/cards/SendMessageView;Ljava/lang/Boolean;)V
    .locals 1

    .line 167
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 168
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/SendMessageView;->setActionBarUpButtonEnabled(Z)V

    .line 169
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/SendMessageView;->setEnabled(Z)V

    .line 170
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/SendMessageView;->showProgress(Z)V

    return-void
.end method

.method static synthetic lambda$null$4(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 178
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$5(Lcom/squareup/ui/crm/cards/SendMessageView;Ljava/lang/Boolean;)V
    .locals 0

    .line 181
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 182
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/SendMessageView;->setActionBarPrimaryButtonEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$null$7(Lkotlin/Unit;Lcom/squareup/api/items/Discount;)Lcom/squareup/api/items/Discount;
    .locals 0

    return-object p1
.end method

.method private showErrorBar()V
    .locals 3

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmscreens/R$string;->crm_send_message_error:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v2, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$10$SendMessageScreen$Presenter(Lcom/squareup/ui/crm/cards/SendMessageView;Lcom/squareup/api/items/Discount;)V
    .locals 2

    .line 203
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 204
    sget-object v0, Lcom/squareup/ui/crm/coupon/AddCouponState;->NO_DISCOUNT:Lcom/squareup/api/items/Discount;

    if-eq p2, v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_add_coupon_amount:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p2, p2, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    .line 206
    invoke-static {p2}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-interface {v1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v1, "amount"

    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 207
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 208
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    .line 205
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/SendMessageView;->showCouponRow(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 210
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/SendMessageView;->showAddCouponButton()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$8$SendMessageScreen$Presenter(Lcom/squareup/ui/crm/cards/SendMessageView;Lcom/squareup/api/items/Discount;)V
    .locals 3

    .line 192
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_MESSAGING_CREATE_CONVERSATION_SEND:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/crm/events/CrmEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->CRM_MESSAGING_CREATE_CONVERSATION_SEND:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v1, v2}, Lcom/squareup/crm/events/CrmEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 196
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/SendMessageView;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->onSendPressed(Lcom/squareup/ui/crm/cards/SendMessageView;Ljava/lang/String;Lcom/squareup/api/items/Discount;)V

    return-void
.end method

.method public synthetic lambda$onLoad$0$SendMessageScreen$Presenter(Lcom/squareup/ui/crm/cards/SendMessageView;)V
    .locals 0

    .line 156
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 157
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;->closeSendMessageScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$SendMessageScreen$Presenter()V
    .locals 2

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->onSendClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onLoad$11$SendMessageScreen$Presenter(Lcom/squareup/ui/crm/cards/SendMessageView;)Lrx/Subscription;
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/coupon/AddCouponState;->discount()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$XQJko5lVO649ISDnNRMGA6LtAcM;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$XQJko5lVO649ISDnNRMGA6LtAcM;-><init>(Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;Lcom/squareup/ui/crm/cards/SendMessageView;)V

    .line 202
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$3$SendMessageScreen$Presenter(Lcom/squareup/ui/crm/cards/SendMessageView;)Lrx/Subscription;
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$J9QK6RXrJtpxr5fOMzKJhNsOJRg;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$J9QK6RXrJtpxr5fOMzKJhNsOJRg;-><init>(Lcom/squareup/ui/crm/cards/SendMessageView;)V

    .line 166
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$SendMessageScreen$Presenter(Lcom/squareup/ui/crm/cards/SendMessageView;)Lrx/Subscription;
    .locals 3

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 177
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/SendMessageView;->messageIsBlank()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$plkHWYYWpsRpHxmJyDbW019RADU;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$plkHWYYWpsRpHxmJyDbW019RADU;

    .line 175
    invoke-static {v0, v1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 179
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$iALq7Q-8shqNOXfOujYMIUMbMr8;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$iALq7Q-8shqNOXfOujYMIUMbMr8;-><init>(Lcom/squareup/ui/crm/cards/SendMessageView;)V

    .line 180
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$9$SendMessageScreen$Presenter(Lcom/squareup/ui/crm/cards/SendMessageView;)Lrx/Subscription;
    .locals 3

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->onSendClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

    .line 189
    invoke-virtual {v1}, Lcom/squareup/ui/crm/coupon/AddCouponState;->discount()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$8n8ahoocgi_AjDTyGEkVc8U7Mhg;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$8n8ahoocgi_AjDTyGEkVc8U7Mhg;

    .line 188
    invoke-virtual {v0, v1, v2}, Lcom/jakewharton/rxrelay/PublishRelay;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$bBq9l8lyOnE1Cew6VNOWct-aw1Y;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$bBq9l8lyOnE1Cew6VNOWct-aw1Y;-><init>(Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;Lcom/squareup/ui/crm/cards/SendMessageView;)V

    .line 191
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onSendPressed$12$SendMessageScreen$Presenter(Lcom/squareup/api/items/Discount;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/SingleSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 237
    instance-of v0, p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_1

    .line 238
    sget-object v0, Lcom/squareup/ui/crm/coupon/AddCouponState;->NO_DISCOUNT:Lcom/squareup/api/items/Discount;

    if-ne p1, v0, :cond_0

    .line 239
    new-instance p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    new-instance v0, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;-><init>()V

    .line 240
    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/dialogue/CreateConversationResponse;

    iget-object v1, v1, Lcom/squareup/protos/client/dialogue/CreateConversationResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;

    move-result-object v0

    .line 241
    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/dialogue/CreateConversationResponse;

    iget-object p2, p2, Lcom/squareup/protos/client/dialogue/CreateConversationResponse;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->conversation(Lcom/squareup/protos/client/dialogue/Conversation;)Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;

    move-result-object p2

    .line 242
    invoke-virtual {p2}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->build()Lcom/squareup/protos/client/dialogue/CreateCouponResponse;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;-><init>(Ljava/lang/Object;)V

    .line 239
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->dialogue:Lcom/squareup/crm/DialogueServiceHelper;

    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/dialogue/CreateConversationResponse;

    iget-object p2, p2, Lcom/squareup/protos/client/dialogue/CreateConversationResponse;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    iget-object p2, p2, Lcom/squareup/protos/client/dialogue/Conversation;->token:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->uniqueKey:Ljava/util/UUID;

    invoke-virtual {v0, p2, p1, v1}, Lcom/squareup/crm/DialogueServiceHelper;->createCoupon(Ljava/lang/String;Lcom/squareup/api/items/Discount;Ljava/util/UUID;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 248
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->showErrorBar()V

    .line 249
    invoke-static {}, Lio/reactivex/Single;->never()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onSendPressed$13$SendMessageScreen$Presenter(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 253
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onSendPressed$14$SendMessageScreen$Presenter(Lcom/squareup/ui/crm/cards/SendMessageView;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 256
    instance-of v0, p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 257
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 258
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_MESSAGING_SHOW_CONVERSATION_SEND:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 259
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/crm/events/CrmEvent;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_MESSAGING_SHOW_CONVERSATION_SEND:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1}, Lcom/squareup/crm/events/CrmEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;)V

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 260
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;

    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/dialogue/CreateCouponResponse;

    iget-object p2, p2, Lcom/squareup/protos/client/dialogue/CreateCouponResponse;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;->closeSendMessageScreen(Lcom/squareup/protos/client/dialogue/Conversation;)V

    goto :goto_0

    .line 262
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->showErrorBar()V

    :goto_0
    return-void
.end method

.method onAddCouponClicked()V
    .locals 2

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/cards/SendMessageView;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/SendMessageView;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;->showAddCouponScreen(Ljava/lang/String;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 138
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_MESSAGING_CREATE_CONVERSATION:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->sendMessageDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->sendMessageDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

    sget-object v1, Lcom/squareup/ui/crm/coupon/AddCouponState;->NO_DISCOUNT:Lcom/squareup/api/items/Discount;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/coupon/AddCouponState;->setDiscount(Lcom/squareup/api/items/Discount;)V

    .line 134
    invoke-super {p0}, Lmortar/ViewPresenter;->onExitScope()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 6

    .line 142
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 143
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/SendMessageView;

    .line 145
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->CRM_ALLOWS_COUPONS_IN_DIRECT_MESSAGE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 146
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/SendMessageView;->showAddCouponContainer()V

    .line 149
    :cond_0
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crmscreens/R$string;->crm_send_message_title:I

    .line 151
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;

    .line 153
    invoke-interface {v4}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;->getContactForSendMessageScreen()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/crm/util/RolodexContactHelper;->getNonEmptyDisplayName(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "name"

    .line 152
    invoke-virtual {v3, v5, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 154
    invoke-virtual {v3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v3

    .line 150
    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$4y3bZAC1o5O0Y2r9pOd7Aj_g8a0;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$4y3bZAC1o5O0Y2r9pOd7Aj_g8a0;-><init>(Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;Lcom/squareup/ui/crm/cards/SendMessageView;)V

    .line 155
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crmscreens/R$string;->crm_send_message_send:I

    .line 159
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$t7PlvmEmYoubPSvkfGm5YMCQGjI;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$t7PlvmEmYoubPSvkfGm5YMCQGjI;-><init>(Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;)V

    .line 160
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 161
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 149
    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/SendMessageView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 164
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$RSh1xpmgo8CY5-COY_ehY3y6UUA;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$RSh1xpmgo8CY5-COY_ehY3y6UUA;-><init>(Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;Lcom/squareup/ui/crm/cards/SendMessageView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 174
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$rRbbe42GN7RrhpXByV7IzhFtXhM;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$rRbbe42GN7RrhpXByV7IzhFtXhM;-><init>(Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;Lcom/squareup/ui/crm/cards/SendMessageView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 186
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$ve3FR0cIc4zReHIyvHaPpUl_ByQ;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$ve3FR0cIc4zReHIyvHaPpUl_ByQ;-><init>(Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;Lcom/squareup/ui/crm/cards/SendMessageView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 200
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$s1jpeAQEc7oLUrW3JKEWuUr7X5M;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$s1jpeAQEc7oLUrW3JKEWuUr7X5M;-><init>(Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;Lcom/squareup/ui/crm/cards/SendMessageView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    if-nez p1, :cond_1

    .line 215
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->uniqueKey:Ljava/util/UUID;

    .line 216
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/SendMessageView;->setInitialFocus()V

    .line 217
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;->getMessageForSendMessageScreen()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/SendMessageView;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "uniqueKey"

    .line 219
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->uniqueKey:Ljava/util/UUID;

    :goto_0
    return-void
.end method

.method onRemoveCouponClicked()V
    .locals 2

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

    sget-object v1, Lcom/squareup/ui/crm/coupon/AddCouponState;->NO_DISCOUNT:Lcom/squareup/api/items/Discount;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/coupon/AddCouponState;->setDiscount(Lcom/squareup/api/items/Discount;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->uniqueKey:Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "uniqueKey"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method onSendPressed(Lcom/squareup/ui/crm/cards/SendMessageView;Ljava/lang/String;Lcom/squareup/api/items/Discount;)V
    .locals 3

    .line 228
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;->getContactForSendMessageScreen()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    .line 232
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->sendMessageDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v1}, Lio/reactivex/disposables/Disposable;->isDisposed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 233
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->sendMessageDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v1}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 235
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->dialogue:Lcom/squareup/crm/DialogueServiceHelper;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->uniqueKey:Ljava/util/UUID;

    invoke-virtual {v1, v0, p2, v2}, Lcom/squareup/crm/DialogueServiceHelper;->createConversation(Ljava/lang/String;Ljava/lang/String;Ljava/util/UUID;)Lio/reactivex/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$2vQrsioCx0wTjtr8fNINMl6uQL4;

    invoke-direct {v0, p0, p3}, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$2vQrsioCx0wTjtr8fNINMl6uQL4;-><init>(Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;Lcom/squareup/api/items/Discount;)V

    .line 236
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p2

    .line 252
    invoke-virtual {p2}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p2

    new-instance p3, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$V5R8HYZikhbRuGAX_NTNOsAeZoc;

    invoke-direct {p3, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$V5R8HYZikhbRuGAX_NTNOsAeZoc;-><init>(Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;)V

    .line 253
    invoke-virtual {p2, p3}, Lio/reactivex/Observable;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p2

    new-instance p3, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$GRHEteLDr7BG7_2Zheo5mmfJTU4;

    invoke-direct {p3, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageScreen$Presenter$GRHEteLDr7BG7_2Zheo5mmfJTU4;-><init>(Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;Lcom/squareup/ui/crm/cards/SendMessageView;)V

    .line 254
    invoke-virtual {p2, p3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->sendMessageDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method
