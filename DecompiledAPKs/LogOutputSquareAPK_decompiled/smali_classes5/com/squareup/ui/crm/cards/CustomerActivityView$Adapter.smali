.class Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;
.super Landroid/widget/BaseAdapter;
.source "CustomerActivityView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/CustomerActivityView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Adapter"
.end annotation


# static fields
.field private static final ACTIVITY_ROW_TYPE:I = 0x0

.field private static final ROW_TYPE_COUNT:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/CustomerActivityView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/CustomerActivityView;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;->this$0:Lcom/squareup/ui/crm/cards/CustomerActivityView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method private bindActivityListRow(Lcom/squareup/ui/account/view/SmartLineRow;I)V
    .locals 3

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;->this$0:Lcom/squareup/ui/crm/cards/CustomerActivityView;

    iget-object v0, v0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->presenter:Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->getEvents()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/rolodex/Event;

    .line 166
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Event;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 167
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Event;->subtitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Event;->subtitle:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;->this$0:Lcom/squareup/ui/crm/cards/CustomerActivityView;

    iget-object v0, v0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->presenter:Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;

    iget-object v2, p2, Lcom/squareup/protos/client/rolodex/Event;->occurred_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->getDateString(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 170
    invoke-virtual {p1, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 171
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTag(Ljava/lang/Object;)V

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;->this$0:Lcom/squareup/ui/crm/cards/CustomerActivityView;

    iget-object v0, v0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->presenter:Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->isActivityClickable(Lcom/squareup/protos/client/rolodex/Event;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    invoke-virtual {p1, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setClickable(Z)V

    .line 174
    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 175
    new-instance v0, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter$1;-><init>(Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;Lcom/squareup/protos/client/rolodex/Event;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 181
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setClickable(Z)V

    .line 182
    sget-object p2, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    :goto_0
    return-void
.end method

.method private buildActivityListRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 1

    .line 161
    sget v0, Lcom/squareup/crmscreens/R$layout;->crm_activity_list_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    return-object p1
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;->this$0:Lcom/squareup/ui/crm/cards/CustomerActivityView;

    iget-object v0, v0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->presenter:Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->getEvents()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .line 144
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_0

    .line 148
    check-cast p2, Lcom/squareup/ui/account/view/SmartLineRow;

    goto :goto_0

    .line 150
    :cond_0
    invoke-direct {p0, p3}, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;->buildActivityListRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p2

    .line 151
    :goto_0
    invoke-direct {p0, p2, p1}, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;->bindActivityListRow(Lcom/squareup/ui/account/view/SmartLineRow;I)V

    return-object p2

    .line 156
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unexpected row type."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
