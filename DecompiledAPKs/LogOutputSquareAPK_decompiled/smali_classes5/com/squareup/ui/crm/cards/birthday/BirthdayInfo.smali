.class public Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;
.super Ljava/lang/Object;
.source "BirthdayInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final day:I

.field public final month:I

.field public final removeInsteadOfCancel:Z

.field public final year:I

.field public final yearIsKnown:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 52
    new-instance v0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo$1;

    invoke-direct {v0}, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIIZZ)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->year:I

    .line 17
    iput p2, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->month:I

    .line 18
    iput p3, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->day:I

    .line 19
    iput-boolean p4, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->removeInsteadOfCancel:Z

    .line 20
    iput-boolean p5, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->yearIsKnown:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 36
    instance-of v0, p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 37
    check-cast p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;

    .line 38
    iget v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->year:I

    iget v2, p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->year:I

    if-ne v0, v2, :cond_0

    iget v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->month:I

    iget v2, p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->month:I

    if-ne v0, v2, :cond_0

    iget v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->day:I

    iget v2, p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->day:I

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->removeInsteadOfCancel:Z

    iget-boolean v2, p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->removeInsteadOfCancel:Z

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->yearIsKnown:Z

    iget-boolean p1, p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->yearIsKnown:Z

    if-ne v0, p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    .line 49
    iget v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->year:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->month:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->day:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->removeInsteadOfCancel:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->yearIsKnown:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 28
    iget p2, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->year:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 29
    iget p2, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->month:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 30
    iget p2, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->day:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 31
    iget-boolean p2, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->removeInsteadOfCancel:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 32
    iget-boolean p2, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->yearIsKnown:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
