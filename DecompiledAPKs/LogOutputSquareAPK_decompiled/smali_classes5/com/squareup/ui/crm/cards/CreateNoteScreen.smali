.class public Lcom/squareup/ui/crm/cards/CreateNoteScreen;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "CreateNoteScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/CreateNoteScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/CreateNoteScreen$Component;,
        Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;,
        Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/CreateNoteScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 256
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$wxt7GQrtxKFRFLvCSVtpMGc3UcA;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$wxt7GQrtxKFRFLvCSVtpMGc3UcA;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/CreateNoteScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/CreateNoteScreen;
    .locals 1

    .line 257
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 258
    new-instance v0, Lcom/squareup/ui/crm/cards/CreateNoteScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/CreateNoteScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 252
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InCrmScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 253
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen;->crmPath:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 61
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CREATE_NOTE:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 69
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 65
    sget v0, Lcom/squareup/crmscreens/R$layout;->crm_create_note_view:I

    return v0
.end method
