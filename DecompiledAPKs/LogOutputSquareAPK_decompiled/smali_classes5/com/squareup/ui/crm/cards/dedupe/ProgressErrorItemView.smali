.class public Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;
.super Landroid/widget/LinearLayout;
.source "ProgressErrorItemView.java"


# instance fields
.field private final messageView:Lcom/squareup/widgets/MessageView;

.field private final progressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 17
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 18
    sget v0, Lcom/squareup/crm/R$layout;->crm_contact_list_bottom_row:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 20
    sget p1, Lcom/squareup/crm/R$id;->crm_contact_list_bottom_row_progress:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;->progressBar:Landroid/widget/ProgressBar;

    .line 21
    sget p1, Lcom/squareup/crm/R$id;->crm_contact_list_bottom_row_message:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;->messageView:Lcom/squareup/widgets/MessageView;

    return-void
.end method


# virtual methods
.method showError(Z)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;->messageView:Lcom/squareup/widgets/MessageView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showProgress(Z)V
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;->progressBar:Landroid/widget/ProgressBar;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
