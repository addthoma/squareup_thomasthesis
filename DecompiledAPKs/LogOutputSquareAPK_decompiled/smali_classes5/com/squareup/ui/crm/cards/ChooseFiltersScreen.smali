.class public Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "ChooseFiltersScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;,
        Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;,
        Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;",
            ">;"
        }
    .end annotation
.end field

.field static final MAX_FILTERS_ALLOWED:I = 0xa


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 204
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$iaKnq6pZzNJfdo4BqMleeSZnW3s;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$iaKnq6pZzNJfdo4BqMleeSZnW3s;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 184
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;
    .locals 1

    .line 205
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 206
    new-instance v0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 200
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 192
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CHOOSE_FILTERS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 196
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_choose_filters_card_view:I

    return v0
.end method
