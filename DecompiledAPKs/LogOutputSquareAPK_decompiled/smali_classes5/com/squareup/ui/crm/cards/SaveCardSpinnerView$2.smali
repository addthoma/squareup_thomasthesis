.class Lcom/squareup/ui/crm/cards/SaveCardSpinnerView$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "SaveCardSpinnerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->showDoneButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;)V
    .locals 0

    .line 94
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView$2;->this$0:Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView$2;->this$0:Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;

    iget-object p1, p1, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->presenter:Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->onDoneButtonClicked()V

    return-void
.end method
