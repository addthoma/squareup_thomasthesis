.class public Lcom/squareup/ui/crm/cards/ProfileAttachmentsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ProfileAttachmentsCoordinator.java"


# instance fields
.field private final runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 22
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsCoordinator;->runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 48
    invoke-virtual {p1}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItems()Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;->setAttachments(Ljava/util/List;)V

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method static synthetic lambda$null$2(Landroid/view/View;Landroid/widget/ProgressBar;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 55
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 56
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    const/16 p0, 0x8

    :goto_0
    invoke-virtual {p1, p0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    .line 27
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 28
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 30
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    .line 31
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v3, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_header:I

    .line 32
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 31
    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 33
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsCoordinator;->runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/crm/cards/-$$Lambda$2fQmgizA3CaGnhE3rNwEQd6JrW4;

    invoke-direct {v3, v2}, Lcom/squareup/ui/crm/cards/-$$Lambda$2fQmgizA3CaGnhE3rNwEQd6JrW4;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    const/4 v2, 0x1

    .line 34
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 35
    sget v2, Lcom/squareup/profileattachments/R$string;->upload:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsCoordinator;->runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$sp44wAFoicNRgMASOLA4BXaRjUI;

    invoke-direct {v2, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$sp44wAFoicNRgMASOLA4BXaRjUI;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 38
    sget v0, Lcom/squareup/profileattachments/R$id;->crm_profile_attachments_list:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 40
    sget v1, Lcom/squareup/profileattachments/R$id;->crm_profile_attachments_progress_bar:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 42
    new-instance v2, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsCoordinator;->runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-direct {v2, v3}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;)V

    .line 44
    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 46
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsCoordinator$YetZtHPo_8nnx0hA-chTRJNHs1M;

    invoke-direct {v0, p0, v2}, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsCoordinator$YetZtHPo_8nnx0hA-chTRJNHs1M;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsCoordinator;Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 52
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsCoordinator$ayoxNc_o4YoRm8tdULZdy8_1_do;

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsCoordinator$ayoxNc_o4YoRm8tdULZdy8_1_do;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsCoordinator;Landroid/view/View;Landroid/widget/ProgressBar;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$1$ProfileAttachmentsCoordinator(Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsCoordinator;->runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->getLoaderResults()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsCoordinator$mZW6oY-QKGFyduCwDe8mQu-DWzQ;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsCoordinator$mZW6oY-QKGFyduCwDe8mQu-DWzQ;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;)V

    .line 47
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$3$ProfileAttachmentsCoordinator(Landroid/view/View;Landroid/widget/ProgressBar;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsCoordinator;->runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->isBusy()Lio/reactivex/Observable;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsCoordinator$hou69xr5YVIdY-7tyTQBOOdOj6w;

    invoke-direct {v1, p1, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsCoordinator$hou69xr5YVIdY-7tyTQBOOdOj6w;-><init>(Landroid/view/View;Landroid/widget/ProgressBar;)V

    .line 54
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method
