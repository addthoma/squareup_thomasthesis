.class public interface abstract Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen$Controller;
.super Ljava/lang/Object;
.source "MergingDuplicatesScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation


# virtual methods
.method public abstract closeMergingDuplicatesScreen()V
.end method

.method public abstract getExpectedDuplicateCount()Ljava/lang/Integer;
.end method

.method public abstract success()V
.end method
