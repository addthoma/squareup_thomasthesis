.class public final synthetic Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$yspGnxRJqt-lWJffDAHNy3aHEAI;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;

.field private final synthetic f$1:Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;

.field private final synthetic f$2:Ljava/lang/String;

.field private final synthetic f$3:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final synthetic f$4:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;Ljava/lang/String;Lcom/squareup/marin/widgets/MarinActionBar;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$yspGnxRJqt-lWJffDAHNy3aHEAI;->f$0:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$yspGnxRJqt-lWJffDAHNy3aHEAI;->f$1:Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;

    iput-object p3, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$yspGnxRJqt-lWJffDAHNy3aHEAI;->f$2:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$yspGnxRJqt-lWJffDAHNy3aHEAI;->f$3:Lcom/squareup/marin/widgets/MarinActionBar;

    iput-object p5, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$yspGnxRJqt-lWJffDAHNy3aHEAI;->f$4:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 6

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$yspGnxRJqt-lWJffDAHNy3aHEAI;->f$0:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$yspGnxRJqt-lWJffDAHNy3aHEAI;->f$1:Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$yspGnxRJqt-lWJffDAHNy3aHEAI;->f$2:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$yspGnxRJqt-lWJffDAHNy3aHEAI;->f$3:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$yspGnxRJqt-lWJffDAHNy3aHEAI;->f$4:Ljava/util/concurrent/atomic/AtomicReference;

    move-object v5, p1

    check-cast v5, Ljava/util/List;

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->lambda$null$2$AddCustomersToGroupScreen$Presenter(Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;Ljava/lang/String;Lcom/squareup/marin/widgets/MarinActionBar;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/List;)V

    return-void
.end method
