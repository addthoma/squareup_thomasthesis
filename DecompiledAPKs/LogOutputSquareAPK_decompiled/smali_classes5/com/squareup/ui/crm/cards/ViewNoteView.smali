.class public Lcom/squareup/ui/crm/cards/ViewNoteView;
.super Landroid/widget/LinearLayout;
.source "ViewNoteView.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private creatorTimestamp:Lcom/squareup/widgets/MessageView;

.field private deleteNoteButton:Lcom/squareup/ui/ConfirmButton;

.field private noteEditText:Landroid/widget/EditText;

.field presenter:Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/view/View;

.field private reminder:Lcom/squareup/ui/account/view/SmartLineRow;

.field private final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    const-class p2, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Component;->inject(Lcom/squareup/ui/crm/cards/ViewNoteView;)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ViewNoteView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->shortAnimTimeMs:I

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 114
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 115
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_view_note_body:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->noteEditText:Landroid/widget/EditText;

    .line 116
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_view_note_creator_timestamp:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->creatorTimestamp:Lcom/squareup/widgets/MessageView;

    .line 117
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_delete_note_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->deleteNoteButton:Lcom/squareup/ui/ConfirmButton;

    .line 118
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->progressBar:Landroid/view/View;

    .line 119
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_view_note_reminder:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->reminder:Lcom/squareup/ui/account/view/SmartLineRow;

    return-void
.end method


# virtual methods
.method getNote()Ljava/lang/String;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->noteEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method noteIsBlank()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->noteEditText:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Views;->isBlank(Landroid/widget/TextView;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 54
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->presenter:Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->presenter:Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 60
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 47
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/ViewNoteView;->bindViews()V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->deleteNoteButton:Lcom/squareup/ui/ConfirmButton;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->presenter:Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$N65WW_bbOiaDteivTQedWI0uPs0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$N65WW_bbOiaDteivTQedWI0uPs0;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;)V

    invoke-virtual {v0, v2}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    return-void
.end method

.method onReminderClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->reminder:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method setActionBarPrimaryButtonEnabled(Z)V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method

.method setActionBarUpButtonEnabled(Z)V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->deleteNoteButton:Lcom/squareup/ui/ConfirmButton;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ConfirmButton;->setEnabled(Z)V

    return-void
.end method

.method showCreatorTimestamp(Ljava/lang/String;)V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->creatorTimestamp:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showNote(Ljava/lang/String;)V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->noteEditText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showProgress(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->progressBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 87
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->progressBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    :goto_0
    return-void
.end method

.method showReminder(Ljava/lang/String;)V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->reminder:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->reminder:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteView;->reminder:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    return-void
.end method
