.class Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;
.super Lmortar/ViewPresenter;
.source "CustomerLookupPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;",
        ">;"
    }
.end annotation


# instance fields
.field private final searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 15
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const-string v0, ""

    .line 12
    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-void
.end method


# virtual methods
.method onSearchTermChanged(Ljava/lang/String;)V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method searchTerm()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method setSearchTerm(Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;Ljava/lang/String;)V
    .locals 0

    .line 18
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
