.class Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "AllNotesScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->bindRow(Lcom/squareup/ui/crm/rows/NoteRow;Lcom/squareup/protos/client/rolodex/Note;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;

.field final synthetic val$note:Lcom/squareup/protos/client/rolodex/Note;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;Lcom/squareup/protos/client/rolodex/Note;)V
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter$1;->this$0:Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter$1;->val$note:Lcom/squareup/protos/client/rolodex/Note;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 109
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter$1;->this$0:Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->access$000(Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;)Lcom/squareup/ui/crm/cards/AllNotesScreen$Runner;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter$1;->val$note:Lcom/squareup/protos/client/rolodex/Note;

    invoke-interface {p1, v0}, Lcom/squareup/ui/crm/cards/AllNotesScreen$Runner;->showViewNoteScreen(Lcom/squareup/protos/client/rolodex/Note;)V

    return-void
.end method
