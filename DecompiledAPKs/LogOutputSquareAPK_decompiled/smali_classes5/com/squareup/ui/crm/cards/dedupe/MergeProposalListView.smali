.class public Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "MergeProposalListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$ItemDecoration;,
        Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$Component;,
        Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$SharedScope;
    }
.end annotation


# static fields
.field static final NEAR_END_OF_LIST:I = 0xa


# instance fields
.field private final adapter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter;

.field private lastVisibleItemPosition:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

.field presenter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private totalItemCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 62
    invoke-direct {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x0

    .line 27
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->lastVisibleItemPosition:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 28
    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->totalItemCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 63
    const-class v0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$Component;

    invoke-interface {v0, p0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$Component;->inject(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;)V

    .line 65
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1, p2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 66
    new-instance p1, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter;

    iget-object p2, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->presenter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;

    invoke-direct {p1, p2}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->adapter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;)Landroidx/recyclerview/widget/LinearLayoutManager;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->lastVisibleItemPosition:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method static synthetic lambda$onNearEndOfList$0(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 120
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    add-int/lit8 p1, p1, -0xa

    if-lt p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onNearEndOfList$1(Ljava/lang/Boolean;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 122
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$onNearEndOfList$2(Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 123
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method


# virtual methods
.method public isBusy()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->presenter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->isBusy()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method notifyItemRangeInserted(II)V
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->adapter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter;->notifyItemRangeInserted(II)V

    .line 107
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->totalItemCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object p2, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p2}, Landroidx/recyclerview/widget/LinearLayoutManager;->getItemCount()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method notifyItemRangeRemoved(II)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->adapter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter;->notifyItemRangeRemoved(II)V

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->totalItemCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object p2, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p2}, Landroidx/recyclerview/widget/LinearLayoutManager;->getItemCount()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 84
    invoke-super {p0}, Landroidx/recyclerview/widget/RecyclerView;->onAttachedToWindow()V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->presenter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->presenter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->dropView(Ljava/lang/Object;)V

    .line 90
    invoke-super {p0}, Landroidx/recyclerview/widget/RecyclerView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 70
    invoke-super {p0}, Landroidx/recyclerview/widget/RecyclerView;->onFinishInflate()V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->adapter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 74
    new-instance v0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$ItemDecoration;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$ItemDecoration;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;Landroid/content/res/Resources;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 76
    new-instance v0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$1;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    return-void
.end method

.method onNearEndOfList()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->lastVisibleItemPosition:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->totalItemCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v2, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListView$hxEEOXbuEEhQjV_-GR-Lj4c9iJo;->INSTANCE:Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListView$hxEEOXbuEEhQjV_-GR-Lj4c9iJo;

    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListView$WoTEAKScUlmcVx_MfISAdJQfisw;->INSTANCE:Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListView$WoTEAKScUlmcVx_MfISAdJQfisw;

    .line 122
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListView$pd2E7ygmFYCuAWka04gf2F9dbdw;->INSTANCE:Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListView$pd2E7ygmFYCuAWka04gf2F9dbdw;

    .line 123
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 94
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented. Set saveEnabled to false."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public uncheckedCount()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->presenter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->uncheckedCount()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
