.class public interface abstract Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;
.super Ljava/lang/Object;
.source "AllAppointmentsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeAllAppointmentsScreen()V
.end method

.method public abstract pastAppointments()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract showAppointmentDetail(Ljava/lang/String;Lorg/threeten/bp/Instant;)V
.end method

.method public abstract upcomingAppointments()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;",
            ">;>;"
        }
    .end annotation
.end method
