.class public Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;
.super Lcom/squareup/flowlegacy/DialogPopup;
.source "BirthdayPopup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/DialogPopup<",
        "Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;",
        "Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/flowlegacy/DialogPopup;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic lambda$createDialog$0(Lcom/squareup/mortar/PopupPresenter;Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 38
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->getBirthday()Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$createDialog$1(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;)V
    .locals 0

    .line 40
    invoke-virtual {p0}, Lcom/squareup/mortar/PopupPresenter;->dismiss()V

    return-void
.end method

.method static synthetic lambda$createDialog$2(Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 44
    iget-boolean p0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->removeInsteadOfCancel:Z

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    .line 45
    invoke-virtual {p1, p0}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    goto :goto_0

    .line 47
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/mortar/PopupPresenter;->dismiss()V

    :goto_0
    return-void
.end method


# virtual methods
.method protected bridge synthetic createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;->createDialog(Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method protected createDialog(Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;",
            "Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;",
            ">;)",
            "Landroid/app/Dialog;"
        }
    .end annotation

    .line 29
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPopup;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 30
    sget v0, Lcom/squareup/crmscreens/R$layout;->crm_birthday_popup:I

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 32
    sget v1, Lcom/squareup/crmscreens/R$id;->crm_birthday_picker:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;

    .line 33
    invoke-virtual {v1, p1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->setBirthday(Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;)V

    .line 35
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 36
    invoke-virtual {v2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/crm/cards/birthday/-$$Lambda$BirthdayPopup$mWUKX0uGs_C-75sR-tTiGpJxzxI;

    invoke-direct {v0, p3, v1}, Lcom/squareup/ui/crm/cards/birthday/-$$Lambda$BirthdayPopup$mWUKX0uGs_C-75sR-tTiGpJxzxI;-><init>(Lcom/squareup/mortar/PopupPresenter;Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)V

    const v1, 0x104000a

    .line 37
    invoke-virtual {p2, v1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p2

    const/4 v0, 0x1

    .line 39
    invoke-virtual {p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/crm/cards/birthday/-$$Lambda$BirthdayPopup$Bdvgj8DcM6HYzetghM-jz8CGS1c;

    invoke-direct {v0, p3}, Lcom/squareup/ui/crm/cards/birthday/-$$Lambda$BirthdayPopup$Bdvgj8DcM6HYzetghM-jz8CGS1c;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 40
    invoke-virtual {p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p2

    iget-boolean v0, p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->removeInsteadOfCancel:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/squareup/crmscreens/R$string;->remove:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    :goto_0
    new-instance v1, Lcom/squareup/ui/crm/cards/birthday/-$$Lambda$BirthdayPopup$qwL6az6_QUTkltUIEISHJiPWnJ8;

    invoke-direct {v1, p1, p3}, Lcom/squareup/ui/crm/cards/birthday/-$$Lambda$BirthdayPopup$qwL6az6_QUTkltUIEISHJiPWnJ8;-><init>(Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;Lcom/squareup/mortar/PopupPresenter;)V

    .line 41
    invoke-virtual {p2, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 50
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
