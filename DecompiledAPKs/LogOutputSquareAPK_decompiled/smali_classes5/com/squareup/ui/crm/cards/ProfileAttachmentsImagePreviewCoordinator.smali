.class public Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ProfileAttachmentsImagePreviewCoordinator.java"


# instance fields
.field private final runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 22
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewCoordinator;->runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/marin/widgets/MarinActionBar;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 33
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic lambda$null$3(Landroid/widget/ProgressBar;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 48
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {p0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    .line 27
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 29
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 31
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    .line 32
    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsImagePreviewCoordinator$JSBlDPlDFKmkutCs67P4dAg75jY;

    invoke-direct {v2, p0, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsImagePreviewCoordinator$JSBlDPlDFKmkutCs67P4dAg75jY;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewCoordinator;Lcom/squareup/marin/widgets/MarinActionBar;)V

    invoke-static {p1, v2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 34
    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsImagePreviewCoordinator$f-G_NTFXwo_72y4W9HVxHpidFDw;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsImagePreviewCoordinator$f-G_NTFXwo_72y4W9HVxHpidFDw;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewCoordinator;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    const/4 v2, 0x1

    .line 40
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 41
    sget v2, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_options:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewCoordinator;->runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$6oow_nT_AKZBrJRTOaB8xU8xfwY;

    invoke-direct {v2, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$6oow_nT_AKZBrJRTOaB8xU8xfwY;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 44
    sget v0, Lcom/squareup/profileattachments/R$id;->crm_profile_attachments_image_preview_progress_bar:I

    .line 45
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 47
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsImagePreviewCoordinator$cNo0SR0qswJSVbL6-kjIS-XGU7Q;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsImagePreviewCoordinator$cNo0SR0qswJSVbL6-kjIS-XGU7Q;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewCoordinator;Landroid/widget/ProgressBar;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 50
    sget v1, Lcom/squareup/profileattachments/R$id;->crm_profile_attachments_image_preview_image:I

    .line 51
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    .line 52
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewCoordinator;->runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-virtual {v1, p1, v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->loadImage(Landroid/widget/ImageView;Landroid/widget/ProgressBar;)V

    return-void
.end method

.method public synthetic lambda$attach$1$ProfileAttachmentsImagePreviewCoordinator(Lcom/squareup/marin/widgets/MarinActionBar;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewCoordinator;->runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->getFilename()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsImagePreviewCoordinator$DInx4XzRvkoIBguGPQk5fkt41A0;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsImagePreviewCoordinator$DInx4XzRvkoIBguGPQk5fkt41A0;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;)V

    .line 33
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$2$ProfileAttachmentsImagePreviewCoordinator(Landroid/view/View;)V
    .locals 0

    .line 37
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewCoordinator;->detach(Landroid/view/View;)V

    .line 38
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewCoordinator;->runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->closePreviewScreen()V

    return-void
.end method

.method public synthetic lambda$attach$4$ProfileAttachmentsImagePreviewCoordinator(Landroid/widget/ProgressBar;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewCoordinator;->runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->isBusy()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsImagePreviewCoordinator$2XQR0nOriYDvh6IJqEQuR9NsJA4;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsImagePreviewCoordinator$2XQR0nOriYDvh6IJqEQuR9NsJA4;-><init>(Landroid/widget/ProgressBar;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method
