.class public Lcom/squareup/ui/crm/cards/CustomerActivityView;
.super Landroid/widget/LinearLayout;
.source "CustomerActivityView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private activityList:Landroid/widget/ListView;

.field private final activityListAdapter:Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;

.field private final isAtEndOfList:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final onIsNearEndOfList:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/widget/ProgressBar;

.field private final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->isAtEndOfList:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 45
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->onIsNearEndOfList:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 47
    new-instance p2, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;-><init>(Lcom/squareup/ui/crm/cards/CustomerActivityView;)V

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->activityListAdapter:Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;

    .line 51
    const-class p2, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Component;->inject(Lcom/squareup/ui/crm/cards/CustomerActivityView;)V

    .line 52
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/CustomerActivityView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->shortAnimTimeMs:I

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 188
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 189
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_activity_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->progressBar:Landroid/widget/ProgressBar;

    .line 190
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_activity_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->activityList:Landroid/widget/ListView;

    return-void
.end method

.method static synthetic lambda$onNearEndOfList$0(Ljava/lang/Boolean;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 94
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$onNearEndOfList$1(Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 95
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method


# virtual methods
.method atEndOfList()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->isAtEndOfList:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 75
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->presenter:Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->activityList:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->presenter:Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 82
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 56
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 57
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/CustomerActivityView;->bindViews()V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->activityList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->activityListAdapter:Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method onNearEndOfList()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->onIsNearEndOfList:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 93
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityView$pXx68vIEwY4FHmSqyxz-7_CkuGg;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityView$pXx68vIEwY4FHmSqyxz-7_CkuGg;

    .line 94
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityView$-_u-znjaNixLIQ2mx1rCKiiPXsY;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityView$-_u-znjaNixLIQ2mx1rCKiiPXsY;

    .line 95
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    add-int/2addr p2, p3

    const/4 p1, 0x1

    sub-int/2addr p4, p1

    .line 67
    iget-object p3, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->isAtEndOfList:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v0, 0x0

    if-lt p2, p4, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 68
    iget-object p3, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->onIsNearEndOfList:Lcom/jakewharton/rxrelay2/PublishRelay;

    add-int/lit8 p4, p4, -0x32

    if-lt p2, p4, :cond_1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    return-void
.end method

.method refresh()V
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->activityListAdapter:Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/CustomerActivityView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method setActionBarUpButtonEnabled(Z)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    return-void
.end method

.method showProgress(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->progressBar:Landroid/widget/ProgressBar;

    iget v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->shortAnimTimeMs:I

    invoke-static {p1, v0, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;II)V

    goto :goto_0

    .line 102
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->progressBar:Landroid/widget/ProgressBar;

    iget v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    :goto_0
    return-void
.end method
