.class Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ChooseGroupsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ChooseGroupsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/ChooseGroupsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final allManualGroups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation
.end field

.field private final chosenGroupTokens:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

.field private hasGroupsError:Z

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;

.field private visualState:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 90
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->allManualGroups:Ljava/util/List;

    .line 79
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->chosenGroupTokens:Ljava/util/Set;

    const/4 v0, 0x0

    .line 80
    iput-boolean v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->hasGroupsError:Z

    .line 91
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;

    .line 92
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    .line 93
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 95
    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;->getContactForChooseGroupsScreen()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    .line 96
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact;->group:Ljava/util/List;

    if-eqz p2, :cond_0

    .line 97
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->group:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/rolodex/Group;

    .line 98
    iget-object p3, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->chosenGroupTokens:Ljava/util/Set;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-interface {p3, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method private getModifiedContact()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 5

    .line 217
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 218
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->allManualGroups:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Group;

    .line 219
    iget-object v3, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->chosenGroupTokens:Ljava/util/Set;

    iget-object v4, v2, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 220
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 224
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;->getContactForChooseGroupsScreen()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v1

    .line 225
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->group(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 226
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/squareup/ui/crm/cards/ChooseGroupsView;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 123
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 124
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->showProgress(Z)V

    return-void
.end method

.method private onGroupLoaderSuccess(Lcom/squareup/ui/crm/cards/ChooseGroupsView;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/cards/ChooseGroupsView;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;)V"
        }
    .end annotation

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->allManualGroups:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->allManualGroups:Ljava/util/List;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/protos/client/rolodex/GroupType;

    sget-object v2, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2, v1}, Lcom/squareup/crm/util/RolodexGroupHelper;->filterByTypeAndSort(Ljava/util/List;[Lcom/squareup/protos/client/rolodex/GroupType;)Ljava/util/List;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 180
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;

    invoke-interface {p2}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;->getJustSavedGroupForChooseGroupsScreen()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->chosenGroupTokens:Ljava/util/Set;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;->clearJustSavedGroupForChooseGroupsScreen()V

    :cond_0
    if-eqz p2, :cond_1

    .line 187
    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->setGroupTokenToScrollTo(Ljava/lang/String;)V

    .line 190
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->refresh()V

    .line 192
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->allManualGroups:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 193
    sget-object p2, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->SHOWING_NO_GROUPS_MESSAGE:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->update(Lcom/squareup/ui/crm/cards/ChooseGroupsView;Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;)V

    goto :goto_0

    .line 195
    :cond_2
    sget-object p2, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->SHOWING_SOME_GROUPS:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->update(Lcom/squareup/ui/crm/cards/ChooseGroupsView;Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;)V

    :goto_0
    return-void
.end method

.method private update(Lcom/squareup/ui/crm/cards/ChooseGroupsView;Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;)V
    .locals 4

    .line 230
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->visualState:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    .line 232
    sget-object v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$1;->$SwitchMap$com$squareup$ui$crm$cards$ChooseGroupsScreen$Presenter$VisualState:[I

    invoke-virtual {p2}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v3, 0x2

    if-eq v0, v3, :cond_1

    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    .line 245
    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->showGroups(Z)V

    .line 246
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmupdatecustomer/R$string;->crm_group_loading_error:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->setMessage(Ljava/lang/String;)V

    .line 247
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->showMessage(Z)V

    goto :goto_0

    .line 251
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected visual state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 239
    :cond_1
    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->showGroups(Z)V

    .line 240
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmupdatecustomer/R$string;->crm_no_groups_exist:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->setMessage(Ljava/lang/String;)V

    .line 241
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->showMessage(Z)V

    goto :goto_0

    .line 234
    :cond_2
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->showGroups(Z)V

    .line 235
    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->showMessage(Z)V

    .line 254
    :goto_0
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->setActionBarUpButtonEnabled(Z)V

    return-void
.end method


# virtual methods
.method getAllManualGroups()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->allManualGroups:Ljava/util/List;

    return-object v0
.end method

.method isChosen(Lcom/squareup/protos/client/rolodex/Group;)Z
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->chosenGroupTokens:Ljava/util/Set;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$null$2$ChooseGroupsScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseGroupsView;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 130
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 131
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->onGroupLoaderSuccess(Lcom/squareup/ui/crm/cards/ChooseGroupsView;Ljava/util/List;)V

    return-void
.end method

.method public synthetic lambda$null$4$ChooseGroupsScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseGroupsView;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 137
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 138
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->hasGroupsError:Z

    .line 139
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 140
    sget-object p2, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->SHOWING_ERROR_MESSAGE:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->update(Lcom/squareup/ui/crm/cards/ChooseGroupsView;Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onLoad$1$ChooseGroupsScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseGroupsView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexGroupLoader;->progress()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseGroupsScreen$Presenter$OKGCffYupmQt9Rgf_NzcJIHfMRE;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseGroupsScreen$Presenter$OKGCffYupmQt9Rgf_NzcJIHfMRE;-><init>(Lcom/squareup/ui/crm/cards/ChooseGroupsView;)V

    .line 122
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$3$ChooseGroupsScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseGroupsView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexGroupLoader;->success()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseGroupsScreen$Presenter$PndAEoUY3_0nz0EC6XRgn8HyVxs;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseGroupsScreen$Presenter$PndAEoUY3_0nz0EC6XRgn8HyVxs;-><init>(Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseGroupsView;)V

    .line 129
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$ChooseGroupsScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseGroupsView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexGroupLoader;->failure()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseGroupsScreen$Presenter$U5eJZnO25C9iVDHb8DXGZNPzasg;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseGroupsScreen$Presenter$U5eJZnO25C9iVDHb8DXGZNPzasg;-><init>(Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseGroupsView;)V

    .line 136
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method onCreateNewGroupPressed()V
    .locals 2

    .line 169
    iget-boolean v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->hasGroupsError:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->allManualGroups:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;

    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->getModifiedContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;->setContactForChooseGroupsScreen(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;->showCreateGroupScreen()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 105
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    invoke-interface {p1}, Lcom/squareup/crm/RolodexGroupLoader;->refresh()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 110
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 111
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/ChooseGroupsView;

    .line 113
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crmupdatecustomer/R$string;->crm_choose_groups_title:I

    .line 114
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$VpxeKjjdmXVJJD7F56U6B8n4BP0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$VpxeKjjdmXVJJD7F56U6B8n4BP0;-><init>(Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;)V

    .line 115
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 117
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 113
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 120
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseGroupsScreen$Presenter$B0-3xMKLTBqO02zblMxmOr_HQDA;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseGroupsScreen$Presenter$B0-3xMKLTBqO02zblMxmOr_HQDA;-><init>(Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseGroupsView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 127
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseGroupsScreen$Presenter$GQpVETW_ofzpOFVRV3u0eUpC5jA;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseGroupsScreen$Presenter$GQpVETW_ofzpOFVRV3u0eUpC5jA;-><init>(Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseGroupsView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 134
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseGroupsScreen$Presenter$SOSNtTN6WwzeN0viYG9aEnT2Xxs;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseGroupsScreen$Presenter$SOSNtTN6WwzeN0viYG9aEnT2Xxs;-><init>(Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseGroupsView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method onUpPressed()V
    .locals 3

    .line 200
    sget-object v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$1;->$SwitchMap$com$squareup$ui$crm$cards$ChooseGroupsScreen$Presenter$VisualState:[I

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->visualState:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;->cancelChooseGroupsScreen()V

    goto :goto_0

    .line 211
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected visual state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->visualState:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;

    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->getModifiedContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;->closeChooseGroupsScreen(Lcom/squareup/protos/client/rolodex/Contact;)V

    :goto_0
    return-void
.end method

.method toggleChosen(Lcom/squareup/protos/client/rolodex/Group;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 161
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->chosenGroupTokens:Ljava/util/Set;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-interface {p2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 163
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->chosenGroupTokens:Ljava/util/Set;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-interface {p2, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :goto_0
    return-void
.end method
