.class public Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "UpdateLoyaltyPhoneCoordinator.java"


# instance fields
.field private final errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

.field private final pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;

.field private final validPhone:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/loyalty/PointsTermsFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 30
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->validPhone:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->runner:Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->res:Lcom/squareup/util/Res;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    .line 41
    iput-object p5, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;)Lcom/squareup/text/PhoneNumberHelper;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->validPhone:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method static synthetic lambda$null$1(Landroid/widget/ProgressBar;Lcom/squareup/marin/widgets/MarinActionBar;Landroid/widget/EditText;Ljava/lang/Boolean;)V
    .locals 1

    .line 63
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 64
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {p2}, Landroid/widget/EditText;->length()I

    move-result p0

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {p1, p0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 6

    .line 45
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 47
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->loyalty_phone_number:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 48
    sget v1, Lcom/squareup/crmviewcustomer/R$id;->loyalty_phone_update_hint:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/MessageView;

    .line 49
    sget v2, Lcom/squareup/crmviewcustomer/R$id;->crm_progress_bar:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    .line 51
    iget-object v3, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v4, Lcom/squareup/crmviewcustomer/R$string;->crm_update_loyalty_phone_hint:I

    invoke-virtual {v3, v4}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    .line 54
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/crmviewcustomer/R$string;->crm_update_loyalty_phone_title:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 55
    iget-object v3, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->runner:Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v4, Lcom/squareup/ui/crm/cards/-$$Lambda$hDqo6Ere5e1K1s-lcAn0PAwtqlw;

    invoke-direct {v4, v3}, Lcom/squareup/ui/crm/cards/-$$Lambda$hDqo6Ere5e1K1s-lcAn0PAwtqlw;-><init>(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;)V

    invoke-virtual {v1, v4}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 56
    iget-object v3, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->save:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 57
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 58
    new-instance v3, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneCoordinator$_XU_W0wMhJDZ8UNbuwsLV9s1V0Y;

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneCoordinator$_XU_W0wMhJDZ8UNbuwsLV9s1V0Y;-><init>(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 61
    new-instance v3, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneCoordinator$91NK02kjZf43CR0rTDE2xzgmrZw;

    invoke-direct {v3, p0, v2, v1, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneCoordinator$91NK02kjZf43CR0rTDE2xzgmrZw;-><init>(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;Landroid/widget/ProgressBar;Lcom/squareup/marin/widgets/MarinActionBar;Landroid/widget/EditText;)V

    invoke-static {p1, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 67
    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneCoordinator$BC4l2Cloi5OYhW6ba-cH8fd9uFA;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneCoordinator$BC4l2Cloi5OYhW6ba-cH8fd9uFA;-><init>(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;Landroid/widget/EditText;)V

    invoke-static {p1, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 72
    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneCoordinator$JWzG67dnxtd32GhLL3QSqvtBtR8;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneCoordinator$JWzG67dnxtd32GhLL3QSqvtBtR8;-><init>(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;Landroid/widget/EditText;)V

    invoke-static {p1, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 77
    new-instance v2, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator$1;

    invoke-direct {v2, p0, v1}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator$1;-><init>(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;Lcom/squareup/marin/widgets/MarinActionBar;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 93
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneCoordinator$CBD7GhaUpgAN0dXep7eiPTNP9LQ;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneCoordinator$CBD7GhaUpgAN0dXep7eiPTNP9LQ;-><init>(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$UpdateLoyaltyPhoneCoordinator(Landroid/widget/EditText;)V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->runner:Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;->saveLoyaltyPhone(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$attach$2$UpdateLoyaltyPhoneCoordinator(Landroid/widget/ProgressBar;Lcom/squareup/marin/widgets/MarinActionBar;Landroid/widget/EditText;)Lrx/Subscription;
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->runner:Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;->saveLoyaltyPhoneInProgress()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneCoordinator$-kMf6lA3FVnlqA9KwR9nMpBWUuc;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneCoordinator$-kMf6lA3FVnlqA9KwR9nMpBWUuc;-><init>(Landroid/widget/ProgressBar;Lcom/squareup/marin/widgets/MarinActionBar;Landroid/widget/EditText;)V

    .line 62
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$4$UpdateLoyaltyPhoneCoordinator(Landroid/widget/EditText;)Lrx/Subscription;
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->runner:Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;->getLoyaltyAccountMapping()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneCoordinator$4ZpiEoqj6AURwf3VgCElG5ewKPk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneCoordinator$4ZpiEoqj6AURwf3VgCElG5ewKPk;-><init>(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;)V

    .line 69
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$RIdbgd86IiNF8SnGFsLvdBnnY5A;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$RIdbgd86IiNF8SnGFsLvdBnnY5A;-><init>(Landroid/widget/EditText;)V

    .line 70
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$5$UpdateLoyaltyPhoneCoordinator(Landroid/widget/EditText;)Lrx/Subscription;
    .locals 3

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->validPhone:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CQhAkwX-7A-TERGkZ7ChsOAjtnE;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CQhAkwX-7A-TERGkZ7ChsOAjtnE;-><init>(Lcom/squareup/text/PhoneNumberHelper;)V

    .line 74
    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$RIdbgd86IiNF8SnGFsLvdBnnY5A;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$RIdbgd86IiNF8SnGFsLvdBnnY5A;-><init>(Landroid/widget/EditText;)V

    .line 75
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$7$UpdateLoyaltyPhoneCoordinator()Lrx/Subscription;
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->runner:Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;->onUpdateLoyaltyError()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneCoordinator$YWvZisMsrGhcy0Z-ITSz9KjDZnQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateLoyaltyPhoneCoordinator$YWvZisMsrGhcy0Z-ITSz9KjDZnQ;-><init>(Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;)V

    .line 94
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$3$UpdateLoyaltyPhoneCoordinator(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Ljava/lang/String;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->raw_id:Ljava/lang/String;

    invoke-interface {v0, p1}, Lcom/squareup/text/PhoneNumberHelper;->formatPartial(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$6$UpdateLoyaltyPhoneCoordinator(Ljava/lang/Boolean;)V
    .locals 3

    .line 95
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const-string v0, ""

    if-eqz p1, :cond_0

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_update_loyalty_phone_error:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 98
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneCoordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->removeError(Ljava/lang/String;)Z

    :goto_0
    return-void
.end method
