.class Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ViewNoteScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ViewNoteScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/ViewNoteView;",
        ">;"
    }
.end annotation


# static fields
.field private static final KEY_UNIQUE_KEY:Ljava/lang/String; = "uniqueKey"


# instance fields
.field private final busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private deleteNoteDisposable:Lio/reactivex/disposables/Disposable;

.field private final errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final locale:Ljava/util/Locale;

.field private final noteDateFormatter:Ljava/text/DateFormat;

.field private final reminderDateFormatter:Ljava/text/DateFormat;

.field private final res:Lcom/squareup/util/Res;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private final runner:Lcom/squareup/ui/crm/cards/ViewNoteScreen$Runner;

.field private final savedNote:Lcom/squareup/protos/client/rolodex/Note;

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final timeFormatter:Ljava/text/DateFormat;

.field private uniqueKey:Ljava/util/UUID;

.field private updateNoteDisposable:Lio/reactivex/disposables/Disposable;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Runner;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/util/Res;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 106
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const/4 v0, 0x0

    .line 92
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 93
    invoke-static {}, Lio/reactivex/disposables/Disposables;->empty()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->deleteNoteDisposable:Lio/reactivex/disposables/Disposable;

    .line 94
    invoke-static {}, Lio/reactivex/disposables/Disposables;->empty()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->updateNoteDisposable:Lio/reactivex/disposables/Disposable;

    .line 107
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ViewNoteScreen$Runner;

    .line 108
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    .line 109
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    .line 110
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 111
    iput-object p5, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 112
    iput-object p6, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->noteDateFormatter:Ljava/text/DateFormat;

    .line 113
    iput-object p7, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->reminderDateFormatter:Ljava/text/DateFormat;

    .line 114
    iput-object p8, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->timeFormatter:Ljava/text/DateFormat;

    .line 115
    iput-object p9, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->locale:Ljava/util/Locale;

    .line 116
    iput-object p10, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    .line 118
    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Runner;->getNoteForViewNoteScreen()Lcom/squareup/protos/client/rolodex/Note;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->savedNote:Lcom/squareup/protos/client/rolodex/Note;

    const/4 p1, 0x1

    .line 120
    invoke-virtual {p2, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->setMaxMessages(I)V

    return-void
.end method

.method private formatCreatorTimestamp(Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
    .locals 4

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->locale:Ljava/util/Locale;

    invoke-static {p2, v0}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p2

    const-string/jumbo v0, "time"

    const-string v1, "date"

    if-eqz p1, :cond_0

    .line 242
    iget-object v2, p1, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    iget-object v2, v2, Lcom/squareup/protos/client/Employee;->read_only_full_name:Ljava/lang/String;

    .line 244
    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 245
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crmscreens/R$string;->crm_note_creator_timestamp:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object p1, p1, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    iget-object p1, p1, Lcom/squareup/protos/client/Employee;->read_only_full_name:Ljava/lang/String;

    const-string v3, "full_name"

    .line 246
    invoke-virtual {v2, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->noteDateFormatter:Ljava/text/DateFormat;

    .line 247
    invoke-virtual {v2, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->timeFormatter:Ljava/text/DateFormat;

    .line 248
    invoke-virtual {v1, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 249
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 251
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crm/R$string;->date_format:I

    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->noteDateFormatter:Ljava/text/DateFormat;

    .line 252
    invoke-virtual {v2, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->timeFormatter:Ljava/text/DateFormat;

    .line 253
    invoke-virtual {v1, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 254
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private formatReminderTimestamp(Lcom/squareup/protos/client/rolodex/Reminder;)Ljava/lang/String;
    .locals 3

    if-eqz p1, :cond_1

    .line 259
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Reminder;->scheduled_at:Lcom/squareup/protos/common/time/DateTime;

    if-nez v0, :cond_0

    goto :goto_0

    .line 262
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Reminder;->scheduled_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->locale:Ljava/util/Locale;

    invoke-static {p1, v0}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    .line 263
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/R$string;->date_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->reminderDateFormatter:Ljava/text/DateFormat;

    .line 264
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "date"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->timeFormatter:Ljava/text/DateFormat;

    .line 265
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v1, "time"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 266
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 260
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmscreens/R$string;->crm_reminder_none:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$null$3(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 160
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private showErrorBar()V
    .locals 3

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmscreens/R$string;->crm_note_deleting_error:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v2, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$1$ViewNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/ViewNoteView;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 144
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/ViewNoteView;->setActionBarUpButtonEnabled(Z)V

    .line 145
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/ViewNoteView;->setEnabled(Z)V

    .line 146
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/ViewNoteView;->showProgress(Z)V

    return-void
.end method

.method public synthetic lambda$null$10$ViewNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/ViewNoteView;Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 207
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 208
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ViewNoteScreen$Runner;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;->note:Lcom/squareup/protos/client/rolodex/Note;

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Runner;->closeViewNoteScreen(Lcom/squareup/protos/client/rolodex/Note;)V

    return-void
.end method

.method public synthetic lambda$null$11$ViewNoteScreen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 211
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 212
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_note_saving_error:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {p1, v1, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$null$15$ViewNoteScreen$Presenter(Lcom/squareup/protos/client/rolodex/DeleteNoteResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 231
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ViewNoteScreen$Runner;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Runner;->closeViewNoteScreen(Lcom/squareup/protos/client/rolodex/Note;)V

    return-void
.end method

.method public synthetic lambda$null$16$ViewNoteScreen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 234
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 235
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->showErrorBar()V

    return-void
.end method

.method public synthetic lambda$null$4$ViewNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/ViewNoteView;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 164
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/ViewNoteView;->setActionBarPrimaryButtonEnabled(Z)V

    return-void
.end method

.method public synthetic lambda$null$6$ViewNoteScreen$Presenter(Lkotlin/Unit;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 176
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ViewNoteScreen$Runner;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->savedNote:Lcom/squareup/protos/client/rolodex/Note;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Note;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    invoke-interface {p1, v0}, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Runner;->showReminderScreen(Lcom/squareup/protos/client/rolodex/Reminder;)V

    return-void
.end method

.method public synthetic lambda$onDeletePressed$13$ViewNoteScreen$Presenter(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 222
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 223
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onDeletePressed$14$ViewNoteScreen$Presenter()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onDeletePressed$17$ViewNoteScreen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 229
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$eNWFHBTmRLtImdzcZRTiSQYc-GE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$eNWFHBTmRLtImdzcZRTiSQYc-GE;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;)V

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$kk3s88f7cZDJuwUBwaDdNjlzDJI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$kk3s88f7cZDJuwUBwaDdNjlzDJI;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$onLoad$0$ViewNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/ViewNoteView;)V
    .locals 1

    .line 136
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ViewNoteView;->getNote()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->onSavePressed(Lcom/squareup/ui/crm/cards/ViewNoteView;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$onLoad$2$ViewNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/ViewNoteView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$3CJ2R-R_8trGb-16D8yOJNBZSaw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$3CJ2R-R_8trGb-16D8yOJNBZSaw;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/ViewNoteView;)V

    .line 142
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$ViewNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/ViewNoteView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 159
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ViewNoteView;->noteIsBlank()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$DUjBiFfQQVK0sLzYndO7V-avjJg;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$DUjBiFfQQVK0sLzYndO7V-avjJg;

    .line 157
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$7msxmSdZSWCl2m21JKMEWhdadIc;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$7msxmSdZSWCl2m21JKMEWhdadIc;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/ViewNoteView;)V

    .line 162
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$7$ViewNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/ViewNoteView;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 175
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ViewNoteView;->onReminderClicked()Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$OoIg_OW53Q-CVwA-_Z0HZqvK-Oc;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$OoIg_OW53Q-CVwA-_Z0HZqvK-Oc;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;)V

    .line 176
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onSavePressed$12$ViewNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/ViewNoteView;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 204
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$QBwz02CaeFWx8ayQA2ULoWjk-rw;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$QBwz02CaeFWx8ayQA2ULoWjk-rw;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/ViewNoteView;)V

    new-instance p1, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$gD0pi7DNTy1OWXjiH_vb79pvgNY;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$gD0pi7DNTy1OWXjiH_vb79pvgNY;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;)V

    invoke-virtual {p2, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$onSavePressed$8$ViewNoteScreen$Presenter(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 197
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 198
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onSavePressed$9$ViewNoteScreen$Presenter()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method onDeletePressed()V
    .locals 2

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->deleteNoteDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->savedNote:Lcom/squareup/protos/client/rolodex/Note;

    invoke-interface {v0, v1}, Lcom/squareup/crm/RolodexServiceHelper;->deleteNote(Lcom/squareup/protos/client/rolodex/Note;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$1uJ4e2lmgcF7rseJwn8QvTWGy6w;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$1uJ4e2lmgcF7rseJwn8QvTWGy6w;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;)V

    .line 221
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$H8D6_e2ch2bhrHev2438p5R1FGY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$H8D6_e2ch2bhrHev2438p5R1FGY;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;)V

    .line 225
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$6R8n6nKdZUmDdrsUFtA-pO7uLFg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$6R8n6nKdZUmDdrsUFtA-pO7uLFg;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;)V

    .line 229
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->deleteNoteDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->deleteNoteDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 125
    invoke-super {p0}, Lmortar/ViewPresenter;->onExitScope()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 129
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 130
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/ViewNoteView;

    .line 132
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crmscreens/R$string;->crm_view_note_title:I

    .line 133
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ViewNoteScreen$Runner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/crm/cards/-$$Lambda$563fANpfkU0jQEU4_INH7SN597Q;

    invoke-direct {v3, v2}, Lcom/squareup/ui/crm/cards/-$$Lambda$563fANpfkU0jQEU4_INH7SN597Q;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Runner;)V

    .line 134
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->save:I

    .line 135
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$DHDR0Yns5VvB7ldOtWJdmEQ73jo;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$DHDR0Yns5VvB7ldOtWJdmEQ73jo;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/ViewNoteView;)V

    .line 136
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 137
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 132
    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/ViewNoteView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 140
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$adbnHdJAryu1dwOnA6uqfxB4qcY;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$adbnHdJAryu1dwOnA6uqfxB4qcY;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/ViewNoteView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 156
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$HnZWvapp0Qz_6CaBHbjRhhIhjh8;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$HnZWvapp0Qz_6CaBHbjRhhIhjh8;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/ViewNoteView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 167
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->savedNote:Lcom/squareup/protos/client/rolodex/Note;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Note;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->savedNote:Lcom/squareup/protos/client/rolodex/Note;

    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/Note;->occurred_at:Lcom/squareup/protos/common/time/DateTime;

    .line 168
    invoke-direct {p0, v1, v2}, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->formatCreatorTimestamp(Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object v1

    .line 167
    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/ViewNoteView;->showCreatorTimestamp(Ljava/lang/String;)V

    .line 170
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->CRM_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->savedNote:Lcom/squareup/protos/client/rolodex/Note;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Note;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    invoke-direct {p0, v1}, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->formatReminderTimestamp(Lcom/squareup/protos/client/rolodex/Reminder;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/ViewNoteView;->showReminder(Ljava/lang/String;)V

    .line 174
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$7p2w42pcI0MKBESlXoU9h66duIM;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$7p2w42pcI0MKBESlXoU9h66duIM;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/ViewNoteView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    :cond_0
    if-nez p1, :cond_1

    .line 180
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->uniqueKey:Ljava/util/UUID;

    .line 181
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->savedNote:Lcom/squareup/protos/client/rolodex/Note;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Note;->body:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/ViewNoteView;->showNote(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "uniqueKey"

    .line 183
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->uniqueKey:Ljava/util/UUID;

    :goto_0
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->uniqueKey:Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "uniqueKey"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method onSavePressed(Lcom/squareup/ui/crm/cards/ViewNoteView;Ljava/lang/String;)V
    .locals 2

    .line 192
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->updateNoteDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->savedNote:Lcom/squareup/protos/client/rolodex/Note;

    invoke-interface {v0, v1, p2}, Lcom/squareup/crm/RolodexServiceHelper;->updateNote(Lcom/squareup/protos/client/rolodex/Note;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$66PnkzNgXmfNfguKtynv9ikhVzM;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$66PnkzNgXmfNfguKtynv9ikhVzM;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;)V

    .line 196
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$GWQWNDpH_GFwsnnzgF8RbgCsVa0;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$GWQWNDpH_GFwsnnzgF8RbgCsVa0;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;)V

    .line 200
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$RofMhbWnur0xBiJC4J8f1bs5JgM;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ViewNoteScreen$Presenter$RofMhbWnur0xBiJC4J8f1bs5JgM;-><init>(Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/ViewNoteView;)V

    .line 204
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ViewNoteScreen$Presenter;->updateNoteDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method
