.class public interface abstract Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;
.super Ljava/lang/Object;
.source "AdjustPointsScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH&J\u0008\u0010\r\u001a\u00020\u000eH&J\u0008\u0010\u000f\u001a\u00020\u0010H&J\u000e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u000bH&J\u0010\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0007H&J\u0008\u0010\u0014\u001a\u00020\u0010H&J\u0010\u0010\u0015\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\u0017H&J\u0008\u0010\u0018\u001a\u00020\u000eH&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;",
        "",
        "adjustmentType",
        "Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;",
        "getAdjustmentType",
        "()Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;",
        "loyaltyStatusPoints",
        "",
        "getLoyaltyStatusPoints",
        "()I",
        "adjustPointsLoadingState",
        "Lrx/Observable;",
        "Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;",
        "canGoNegative",
        "",
        "closeAdjustPointsScreen",
        "",
        "currentAdjustment",
        "enterAdjustment",
        "amount",
        "saveAdjustmentAmount",
        "setAdjustReason",
        "reason",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;",
        "shouldShowSmsDisclaimer",
        "crm-screens_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract adjustPointsLoadingState()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract canGoNegative()Z
.end method

.method public abstract closeAdjustPointsScreen()V
.end method

.method public abstract currentAdjustment()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract enterAdjustment(I)V
.end method

.method public abstract getAdjustmentType()Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;
.end method

.method public abstract getLoyaltyStatusPoints()I
.end method

.method public abstract saveAdjustmentAmount()V
.end method

.method public abstract setAdjustReason(Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;)V
.end method

.method public abstract shouldShowSmsDisclaimer()Z
.end method
