.class public final synthetic Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$-0fejLXYv3z36pENNpoK--uFe3k;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

.field private final synthetic f$1:Lcom/squareup/crm/RolodexContactLoader;

.field private final synthetic f$2:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

.field private final synthetic f$3:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$-0fejLXYv3z36pENNpoK--uFe3k;->f$0:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$-0fejLXYv3z36pENNpoK--uFe3k;->f$1:Lcom/squareup/crm/RolodexContactLoader;

    iput-object p3, p0, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$-0fejLXYv3z36pENNpoK--uFe3k;->f$2:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    iput-object p4, p0, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$-0fejLXYv3z36pENNpoK--uFe3k;->f$3:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$-0fejLXYv3z36pENNpoK--uFe3k;->f$0:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$-0fejLXYv3z36pENNpoK--uFe3k;->f$1:Lcom/squareup/crm/RolodexContactLoader;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$-0fejLXYv3z36pENNpoK--uFe3k;->f$2:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/contact/-$$Lambda$ContactListPresenterV2$-0fejLXYv3z36pENNpoK--uFe3k;->f$3:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->lambda$init$4$ContactListPresenterV2(Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;Ljava/util/concurrent/atomic/AtomicReference;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method
