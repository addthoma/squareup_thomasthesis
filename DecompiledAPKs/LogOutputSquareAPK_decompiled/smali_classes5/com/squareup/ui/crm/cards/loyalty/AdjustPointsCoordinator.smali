.class public final Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AdjustPointsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAdjustPointsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AdjustPointsCoordinator.kt\ncom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator\n*L\n1#1,265:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B/\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0008\u0010A\u001a\u00020BH\u0002J\u0010\u0010C\u001a\u00020D2\u0006\u0010@\u001a\u00020*H\u0016J\u0010\u0010E\u001a\u00020B2\u0006\u0010F\u001a\u00020GH\u0002J\u0010\u0010H\u001a\u00020I2\u0006\u0010J\u001a\u00020KH\u0002J\u000c\u0010L\u001a\u00020I*\u00020MH\u0002R#\u0010\r\u001a\n \u000f*\u0004\u0018\u00010\u000e0\u000e8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0012\u0010\u0013\u001a\u0004\u0008\u0010\u0010\u0011R\u001b\u0010\u0014\u001a\u00020\u00158BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0018\u0010\u0013\u001a\u0004\u0008\u0016\u0010\u0017R\u001b\u0010\u0019\u001a\u00020\u00158BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001b\u0010\u0013\u001a\u0004\u0008\u001a\u0010\u0017R\u001b\u0010\u001c\u001a\u00020\u00158BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001e\u0010\u0013\u001a\u0004\u0008\u001d\u0010\u0017R\u001b\u0010\u001f\u001a\u00020 8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008#\u0010\u0013\u001a\u0004\u0008!\u0010\"R\u001b\u0010$\u001a\u00020%8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008(\u0010\u0013\u001a\u0004\u0008&\u0010\'R\u001b\u0010)\u001a\u00020*8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008-\u0010\u0013\u001a\u0004\u0008+\u0010,R\u001b\u0010.\u001a\u00020/8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u00082\u0010\u0013\u001a\u0004\u00080\u00101R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u00103\u001a\u00020*8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u00085\u0010\u0013\u001a\u0004\u00084\u0010,R\u001b\u00106\u001a\u0002078BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008:\u0010\u0013\u001a\u0004\u00088\u00109R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010;\u001a\u00020<8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008?\u0010\u0013\u001a\u0004\u0008=\u0010>R\u000e\u0010@\u001a\u00020*X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006N"
    }
    d2 = {
        "Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "res",
        "Lcom/squareup/util/Res;",
        "errorsBar",
        "Lcom/squareup/ui/ErrorsBarPresenter;",
        "formatter",
        "Lcom/squareup/loyalty/PointsTermsFormatter;",
        "(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/util/Res;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/loyalty/PointsTermsFormatter;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "kotlin.jvm.PlatformType",
        "getActionBar",
        "()Lcom/squareup/marin/widgets/MarinActionBar;",
        "actionBar$delegate",
        "Lkotlin/Lazy;",
        "adjustPointsReason1",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "getAdjustPointsReason1",
        "()Lcom/squareup/noho/NohoCheckableRow;",
        "adjustPointsReason1$delegate",
        "adjustPointsReason2",
        "getAdjustPointsReason2",
        "adjustPointsReason2$delegate",
        "adjustPointsReason3",
        "getAdjustPointsReason3",
        "adjustPointsReason3$delegate",
        "adjustPointsReasonTitle",
        "Lcom/squareup/marketfont/MarketTextView;",
        "getAdjustPointsReasonTitle",
        "()Lcom/squareup/marketfont/MarketTextView;",
        "adjustPointsReasonTitle$delegate",
        "adjustmentAmount",
        "Lcom/squareup/noho/NohoEditRow;",
        "getAdjustmentAmount",
        "()Lcom/squareup/noho/NohoEditRow;",
        "adjustmentAmount$delegate",
        "content",
        "Landroid/view/View;",
        "getContent",
        "()Landroid/view/View;",
        "content$delegate",
        "currentBalanceTextView",
        "Landroid/widget/TextView;",
        "getCurrentBalanceTextView",
        "()Landroid/widget/TextView;",
        "currentBalanceTextView$delegate",
        "progressBar",
        "getProgressBar",
        "progressBar$delegate",
        "reasonCheckGroup",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "getReasonCheckGroup",
        "()Lcom/squareup/noho/NohoCheckableGroup;",
        "reasonCheckGroup$delegate",
        "smsDisclaimer",
        "Lcom/squareup/widgets/MessageView;",
        "getSmsDisclaimer",
        "()Lcom/squareup/widgets/MessageView;",
        "smsDisclaimer$delegate",
        "view",
        "amountLabel",
        "",
        "attach",
        "",
        "formatPoints",
        "points",
        "",
        "scrubbedAdjustmentValue",
        "",
        "editable",
        "Landroid/text/Editable;",
        "toStringRes",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;",
        "crm-screens_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar$delegate:Lkotlin/Lazy;

.field private final adjustPointsReason1$delegate:Lkotlin/Lazy;

.field private final adjustPointsReason2$delegate:Lkotlin/Lazy;

.field private final adjustPointsReason3$delegate:Lkotlin/Lazy;

.field private final adjustPointsReasonTitle$delegate:Lkotlin/Lazy;

.field private final adjustmentAmount$delegate:Lkotlin/Lazy;

.field private final content$delegate:Lkotlin/Lazy;

.field private final currentBalanceTextView$delegate:Lkotlin/Lazy;

.field private final errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final formatter:Lcom/squareup/loyalty/PointsTermsFormatter;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final progressBar$delegate:Lkotlin/Lazy;

.field private final reasonCheckGroup$delegate:Lkotlin/Lazy;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;

.field private final smsDisclaimer$delegate:Lkotlin/Lazy;

.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/util/Res;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/loyalty/PointsTermsFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltySettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorsBar"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    iput-object p3, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    iput-object p5, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->formatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    .line 57
    new-instance p1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$actionBar$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$actionBar$2;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->actionBar$delegate:Lkotlin/Lazy;

    .line 58
    new-instance p1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$progressBar$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$progressBar$2;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->progressBar$delegate:Lkotlin/Lazy;

    .line 59
    new-instance p1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$content$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$content$2;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->content$delegate:Lkotlin/Lazy;

    .line 60
    new-instance p1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$currentBalanceTextView$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$currentBalanceTextView$2;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->currentBalanceTextView$delegate:Lkotlin/Lazy;

    .line 63
    new-instance p1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$adjustmentAmount$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$adjustmentAmount$2;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->adjustmentAmount$delegate:Lkotlin/Lazy;

    .line 66
    new-instance p1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$adjustPointsReasonTitle$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$adjustPointsReasonTitle$2;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->adjustPointsReasonTitle$delegate:Lkotlin/Lazy;

    .line 69
    new-instance p1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$reasonCheckGroup$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$reasonCheckGroup$2;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->reasonCheckGroup$delegate:Lkotlin/Lazy;

    .line 72
    new-instance p1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$adjustPointsReason1$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$adjustPointsReason1$2;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->adjustPointsReason1$delegate:Lkotlin/Lazy;

    .line 75
    new-instance p1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$adjustPointsReason2$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$adjustPointsReason2$2;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->adjustPointsReason2$delegate:Lkotlin/Lazy;

    .line 78
    new-instance p1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$adjustPointsReason3$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$adjustPointsReason3$2;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->adjustPointsReason3$delegate:Lkotlin/Lazy;

    .line 81
    new-instance p1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$smsDisclaimer$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$smsDisclaimer$2;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->smsDisclaimer$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic access$formatPoints(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;J)Ljava/lang/String;
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->formatPoints(J)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAdjustmentAmount$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/noho/NohoEditRow;
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->getAdjustmentAmount()Lcom/squareup/noho/NohoEditRow;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getContent$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Landroid/view/View;
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->getContent()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getCurrentBalanceTextView$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Landroid/widget/TextView;
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->getCurrentBalanceTextView()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getErrorsBar$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/ui/ErrorsBarPresenter;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    return-object p0
.end method

.method public static final synthetic access$getFormatter$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/loyalty/PointsTermsFormatter;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->formatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    return-object p0
.end method

.method public static final synthetic access$getLoyaltySettings$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/loyalty/LoyaltySettings;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    return-object p0
.end method

.method public static final synthetic access$getProgressBar$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Landroid/view/View;
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->getProgressBar()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getReasonCheckGroup$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/noho/NohoCheckableGroup;
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->getReasonCheckGroup()Lcom/squareup/noho/NohoCheckableGroup;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$getView$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Landroid/view/View;
    .locals 1

    .line 48
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->view:Landroid/view/View;

    if-nez p0, :cond_0

    const-string/jumbo v0, "view"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$scrubbedAdjustmentValue(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;Landroid/text/Editable;)I
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->scrubbedAdjustmentValue(Landroid/text/Editable;)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$setView$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;Landroid/view/View;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->view:Landroid/view/View;

    return-void
.end method

.method private final amountLabel()Ljava/lang/String;
    .locals 2

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;->getAdjustmentType()Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;->ADD_POINTS:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;

    if-ne v0, v1, :cond_0

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->formatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_points_add:I

    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->formatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_points_remove:I

    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private final formatPoints(J)Ljava/lang/String;
    .locals 1

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->formatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(J)Ljava/lang/String;

    move-result-object p1

    const-string p2, "formatter.points(points)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->actionBar$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinActionBar;

    return-object v0
.end method

.method private final getAdjustPointsReason1()Lcom/squareup/noho/NohoCheckableRow;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->adjustPointsReason1$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    return-object v0
.end method

.method private final getAdjustPointsReason2()Lcom/squareup/noho/NohoCheckableRow;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->adjustPointsReason2$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    return-object v0
.end method

.method private final getAdjustPointsReason3()Lcom/squareup/noho/NohoCheckableRow;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->adjustPointsReason3$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    return-object v0
.end method

.method private final getAdjustPointsReasonTitle()Lcom/squareup/marketfont/MarketTextView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->adjustPointsReasonTitle$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    return-object v0
.end method

.method private final getAdjustmentAmount()Lcom/squareup/noho/NohoEditRow;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->adjustmentAmount$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditRow;

    return-object v0
.end method

.method private final getContent()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->content$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getCurrentBalanceTextView()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->currentBalanceTextView$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getProgressBar()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->progressBar$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getReasonCheckGroup()Lcom/squareup/noho/NohoCheckableGroup;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->reasonCheckGroup$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableGroup;

    return-object v0
.end method

.method private final getSmsDisclaimer()Lcom/squareup/widgets/MessageView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->smsDisclaimer$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    return-object v0
.end method

.method private final scrubbedAdjustmentValue(Landroid/text/Editable;)I
    .locals 0

    .line 253
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/util/Strings;->removeNonDigits(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method private final toStringRes(Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;)I
    .locals 1

    .line 257
    sget-object v0, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason$ComplimentaryPoints;->INSTANCE:Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason$ComplimentaryPoints;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget p1, Lcom/squareup/crmscreens/R$string;->crm_loyalty_adjust_status_add_reason_one:I

    goto :goto_0

    .line 258
    :cond_0
    sget-object v0, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason$MissedPointsInTransaction;->INSTANCE:Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason$MissedPointsInTransaction;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget p1, Lcom/squareup/crmscreens/R$string;->crm_loyalty_adjust_status_add_reason_two:I

    goto :goto_0

    .line 259
    :cond_1
    sget-object v0, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$PointsAccidentallyAwarded;->INSTANCE:Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$PointsAccidentallyAwarded;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget p1, Lcom/squareup/crmscreens/R$string;->crm_loyalty_adjust_status_remove_reason_one:I

    goto :goto_0

    .line 260
    :cond_2
    sget-object v0, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$TransactionRefunded;->INSTANCE:Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$TransactionRefunded;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget p1, Lcom/squareup/crmscreens/R$string;->crm_loyalty_adjust_status_remove_reason_two:I

    goto :goto_0

    .line 261
    :cond_3
    sget-object v0, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$SuspiciousActivity;->INSTANCE:Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$SuspiciousActivity;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    sget p1, Lcom/squareup/crmscreens/R$string;->crm_loyalty_adjust_status_remove_reason_three:I

    :goto_0
    return p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 13

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->view:Landroid/view/View;

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;->getLoyaltyStatusPoints()I

    move-result v0

    int-to-long v0, v0

    .line 88
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v2

    .line 89
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->formatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v5, Lcom/squareup/crmscreens/R$string;->crm_adjust_points_title:I

    invoke-virtual {v4, v5}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 90
    new-instance v3, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$$inlined$with$lambda$1;

    invoke-direct {v3, p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$$inlined$with$lambda$1;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 91
    iget-object v3, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->save:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    .line 92
    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 93
    new-instance v4, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$$inlined$with$lambda$2;

    invoke-direct {v4, p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$$inlined$with$lambda$2;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V

    check-cast v4, Ljava/lang/Runnable;

    invoke-virtual {v2, v4}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 95
    new-instance v2, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$2;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v2}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 103
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->getAdjustPointsReasonTitle()Lcom/squareup/marketfont/MarketTextView;

    move-result-object v2

    .line 97
    iget-object v4, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->formatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    .line 98
    iget-object v5, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;

    invoke-interface {v5}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;->getAdjustmentType()Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;

    move-result-object v5

    sget-object v6, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v5}, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;->ordinal()I

    move-result v5

    aget v5, v6, v5

    const/4 v6, 0x2

    const/4 v7, 0x1

    if-eq v5, v7, :cond_1

    if-ne v5, v6, :cond_0

    .line 100
    sget v5, Lcom/squareup/crmscreens/R$string;->crm_loyalty_adjust_status_remove_reason_title:I

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 99
    :cond_1
    sget v5, Lcom/squareup/crmscreens/R$string;->crm_loyalty_adjust_status_add_reason_title:I

    .line 97
    :goto_0
    invoke-virtual {v4, v5}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 103
    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "(this as java.lang.String).toUpperCase()"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;

    invoke-interface {v2}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;->getAdjustmentType()Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;

    move-result-object v2

    sget-object v4, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v2}, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;->ordinal()I

    move-result v2

    aget v2, v4, v2

    if-eq v2, v7, :cond_3

    if-ne v2, v6, :cond_2

    const/4 v2, 0x3

    new-array v2, v2, [Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;

    .line 111
    sget-object v4, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$PointsAccidentallyAwarded;->INSTANCE:Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$PointsAccidentallyAwarded;

    check-cast v4, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;

    aput-object v4, v2, v3

    .line 112
    sget-object v4, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$TransactionRefunded;->INSTANCE:Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$TransactionRefunded;

    check-cast v4, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;

    aput-object v4, v2, v7

    .line 113
    sget-object v4, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$SuspiciousActivity;->INSTANCE:Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$SuspiciousActivity;

    check-cast v4, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;

    aput-object v4, v2, v6

    .line 110
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->mutableListOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    goto :goto_1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_3
    new-array v2, v6, [Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason;

    .line 107
    sget-object v4, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason$ComplimentaryPoints;->INSTANCE:Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason$ComplimentaryPoints;

    check-cast v4, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason;

    aput-object v4, v2, v3

    .line 108
    sget-object v4, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason$MissedPointsInTransaction;->INSTANCE:Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason$MissedPointsInTransaction;

    check-cast v4, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason;

    aput-object v4, v2, v7

    .line 106
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->mutableListOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 117
    :goto_1
    iget-object v4, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;

    invoke-interface {v4}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;->canGoNegative()Z

    move-result v4

    const-wide/16 v8, 0x3e8

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;

    invoke-interface {v4}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;->getAdjustmentType()Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;

    move-result-object v4

    sget-object v5, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;->ADD_POINTS:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;

    if-ne v4, v5, :cond_4

    goto :goto_2

    .line 121
    :cond_4
    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    .line 125
    :cond_5
    :goto_2
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->getAdjustPointsReason1()Lcom/squareup/noho/NohoCheckableRow;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->formatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;

    invoke-direct {p0, v10}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->toStringRes(Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;)I

    move-result v10

    invoke-virtual {v5, v10}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 126
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->getAdjustPointsReason2()Lcom/squareup/noho/NohoCheckableRow;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->formatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;

    invoke-direct {p0, v10}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->toStringRes(Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;)I

    move-result v10

    invoke-virtual {v5, v10}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 127
    invoke-static {v2, v6}, Lkotlin/collections/CollectionsKt;->getOrNull(Ljava/util/List;I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;

    .line 128
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->getAdjustPointsReason3()Lcom/squareup/noho/NohoCheckableRow;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    if-eqz v4, :cond_6

    const/4 v3, 0x1

    :cond_6
    invoke-static {v5, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 129
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->getAdjustPointsReason3()Lcom/squareup/noho/NohoCheckableRow;

    move-result-object v3

    if-eqz v4, :cond_7

    iget-object v5, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->formatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    invoke-direct {p0, v4}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->toStringRes(Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;)I

    move-result v4

    invoke-virtual {v5, v4}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    :cond_7
    const/4 v4, 0x0

    :goto_3
    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 131
    new-instance v3, Lcom/squareup/text/QuantityScrubber;

    invoke-direct {v3, v8, v9}, Lcom/squareup/text/QuantityScrubber;-><init>(J)V

    .line 132
    invoke-virtual {v3, v7}, Lcom/squareup/text/QuantityScrubber;->setAllowZero(Z)V

    .line 134
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->getAdjustmentAmount()Lcom/squareup/noho/NohoEditRow;

    move-result-object v4

    .line 136
    new-instance v12, Lcom/squareup/noho/LabelPlugin;

    .line 137
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string/jumbo v5, "view.context"

    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->amountLabel()Ljava/lang/String;

    move-result-object v7

    .line 139
    sget v8, Lcom/squareup/noho/R$style;->TextAppearance_Widget_Noho_Edit_Label:I

    const/4 v9, 0x0

    const/16 v10, 0x8

    const/4 v11, 0x0

    move-object v5, v12

    .line 136
    invoke-direct/range {v5 .. v11}, Lcom/squareup/noho/LabelPlugin;-><init>(Landroid/content/Context;Ljava/lang/String;IFILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v12, Lcom/squareup/noho/NohoEditRow$Plugin;

    .line 135
    invoke-virtual {v4, v12}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 142
    new-instance v5, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$$inlined$with$lambda$3;

    invoke-direct {v5, p0, p1, v3}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$$inlined$with$lambda$3;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;Landroid/view/View;Lcom/squareup/text/QuantityScrubber;)V

    check-cast v5, Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v4, v5}, Lcom/squareup/noho/NohoEditRow;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 156
    new-instance v5, Lcom/squareup/text/ScrubbingTextWatcher;

    move-object v6, v3

    check-cast v6, Lcom/squareup/text/Scrubber;

    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->getAdjustmentAmount()Lcom/squareup/noho/NohoEditRow;

    move-result-object v7

    check-cast v7, Lcom/squareup/text/HasSelectableText;

    invoke-direct {v5, v6, v7}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    check-cast v5, Landroid/text/TextWatcher;

    invoke-virtual {v4, v5}, Lcom/squareup/noho/NohoEditRow;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 157
    new-instance v5, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$$inlined$with$lambda$4;

    invoke-direct {v5, p0, p1, v3}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$$inlined$with$lambda$4;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;Landroid/view/View;Lcom/squareup/text/QuantityScrubber;)V

    check-cast v5, Landroid/text/TextWatcher;

    invoke-virtual {v4, v5}, Lcom/squareup/noho/NohoEditRow;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 171
    invoke-virtual {v4}, Lcom/squareup/noho/NohoEditRow;->requestFocus()Z

    .line 174
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->getReasonCheckGroup()Lcom/squareup/noho/NohoCheckableGroup;

    move-result-object v3

    new-instance v4, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$5;

    invoke-direct {v4, p0, v2}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$5;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;Ljava/util/List;)V

    check-cast v4, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    invoke-virtual {v3, v4}, Lcom/squareup/noho/NohoCheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V

    .line 188
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;

    invoke-interface {v2}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;->currentAdjustment()Lrx/Observable;

    move-result-object v2

    .line 189
    new-instance v3, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;

    invoke-direct {v3, p0, v0, v1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;J)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, p1, v3}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;->adjustPointsLoadingState()Lrx/Observable;

    move-result-object v0

    .line 226
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    const-string v1, "runner.adjustPointsLoadi\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    new-instance v1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$7;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$7;-><init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    .line 241
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->getSmsDisclaimer()Lcom/squareup/widgets/MessageView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->runner:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;->shouldShowSmsDisclaimer()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void

    .line 103
    :cond_8
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
