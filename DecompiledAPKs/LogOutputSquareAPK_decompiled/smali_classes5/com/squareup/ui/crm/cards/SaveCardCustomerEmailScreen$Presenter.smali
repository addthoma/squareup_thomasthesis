.class public Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "SaveCardCustomerEmailScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Runner;

.field private final state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Runner;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 75
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Runner;

    .line 77
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 78
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 79
    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Runner;->getStateForSaveCardScreens()Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;)Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Runner;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Runner;

    return-object p0
.end method

.method static synthetic lambda$onLoad$2(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;)Lrx/Subscription;
    .locals 2

    .line 109
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->email()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/-$$Lambda$fJkbvbzrcAf0-2PSI7GNXvjnOjc;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$fJkbvbzrcAf0-2PSI7GNXvjnOjc;

    .line 110
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$vnyZnkft2fsGmSMAERUsVfwK0qY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$vnyZnkft2fsGmSMAERUsVfwK0qY;-><init>(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;)V

    .line 112
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$null$0$SaveCardCustomerEmailScreen$Presenter(Ljava/lang/String;)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    iput-object p1, v0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->email:Ljava/lang/String;

    return-void
.end method

.method public synthetic lambda$null$3$SaveCardCustomerEmailScreen$Presenter(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;Lkotlin/Unit;)V
    .locals 0

    .line 118
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 119
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->hideSoftKeyboard()V

    .line 120
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Runner;->showVerifyZipCodeOrSaveCardSpinnerScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$SaveCardCustomerEmailScreen$Presenter(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;)Lrx/Subscription;
    .locals 1

    .line 103
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->email()Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x1

    .line 104
    invoke-virtual {p1, v0}, Lrx/Observable;->skip(I)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardCustomerEmailScreen$Presenter$bIMpmSSNfGfdObYHnCHBAeQ6izE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardCustomerEmailScreen$Presenter$bIMpmSSNfGfdObYHnCHBAeQ6izE;-><init>(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;)V

    .line 105
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$4$SaveCardCustomerEmailScreen$Presenter(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;)Lrx/Subscription;
    .locals 2

    .line 116
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->onNextClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardCustomerEmailScreen$Presenter$9WrIJ0pPzOnZvUADC4tYW6Tak7M;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardCustomerEmailScreen$Presenter$9WrIJ0pPzOnZvUADC4tYW6Tak7M;-><init>(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;)V

    .line 117
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 83
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 85
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;

    .line 87
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v1, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter$1;-><init>(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->setUpButton(Lcom/squareup/glyph/GlyphTypeface$Glyph;Landroid/view/View$OnClickListener;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v0

    .line 94
    invoke-static {v0}, Lcom/squareup/address/CountryResources;->cardOnFileLinkEmailDisclaimer(Lcom/squareup/CountryCode;)I

    move-result v0

    .line 95
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 96
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getBusinessName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "merchant_name"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 95
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->setDisclaimerText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->email:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->setEmail(Ljava/lang/CharSequence;)V

    .line 102
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardCustomerEmailScreen$Presenter$Zbo9GvnIRLCMB-HxKpDXSAUAnMo;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardCustomerEmailScreen$Presenter$Zbo9GvnIRLCMB-HxKpDXSAUAnMo;-><init>(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 108
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardCustomerEmailScreen$Presenter$-3huWv6UpMbc1ZENLSdyPfXGtsE;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardCustomerEmailScreen$Presenter$-3huWv6UpMbc1ZENLSdyPfXGtsE;-><init>(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 115
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardCustomerEmailScreen$Presenter$yIXsSzIOhFZiWjYq9xXwYjjdfng;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardCustomerEmailScreen$Presenter$yIXsSzIOhFZiWjYq9xXwYjjdfng;-><init>(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
