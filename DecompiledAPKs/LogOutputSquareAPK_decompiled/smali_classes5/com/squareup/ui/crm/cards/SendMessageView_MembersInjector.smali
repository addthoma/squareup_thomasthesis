.class public final Lcom/squareup/ui/crm/cards/SendMessageView_MembersInjector;
.super Ljava/lang/Object;
.source "SendMessageView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/crm/cards/SendMessageView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/crm/cards/SendMessageView;",
            ">;"
        }
    .end annotation

    .line 21
    new-instance v0, Lcom/squareup/ui/crm/cards/SendMessageView_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/SendMessageView_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/crm/cards/SendMessageView;Ljava/lang/Object;)V
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->presenter:Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/crm/cards/SendMessageView;)V
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/SendMessageView_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/cards/SendMessageView;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/ui/crm/cards/SendMessageView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/SendMessageView_MembersInjector;->injectMembers(Lcom/squareup/ui/crm/cards/SendMessageView;)V

    return-void
.end method
