.class final Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;
.super Lkotlin/jvm/internal/Lambda;
.source "AdjustPointsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "adjustment",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $currentPoints:J

.field final synthetic this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;J)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    iput-wide p2, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->$currentPoints:J

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->invoke(I)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(I)V
    .locals 9

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getActionBar$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getReasonCheckGroup$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/noho/NohoCheckableGroup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoCheckableGroup;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getCurrentBalanceTextView$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Landroid/widget/TextView;

    move-result-object v0

    if-nez p1, :cond_1

    .line 194
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getFormatter$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/loyalty/PointsTermsFormatter;

    move-result-object p1

    iget-wide v1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->$currentPoints:J

    sget v3, Lcom/squareup/crmscreens/R$string;->crm_points_current_unadjusted_balance:I

    invoke-virtual {p1, v1, v2, v3}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(JI)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    goto/16 :goto_1

    .line 197
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getRunner$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;->getAdjustmentType()Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;->REMOVE_POINTS:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;

    const-string v3, "points_terminology"

    const-string/jumbo v4, "total"

    const-string v5, "current"

    if-ne v1, v2, :cond_2

    .line 198
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getRes$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/crmscreens/R$string;->crm_points_negative_adjusted_balance:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 199
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    iget-wide v6, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->$currentPoints:J

    invoke-static {v2, v6, v7}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$formatPoints(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;J)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v5, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 200
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    int-to-long v5, p1

    invoke-static {v2, v5, v6}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$formatPoints(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;J)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v2, "removed"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 201
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    iget-wide v7, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->$currentPoints:J

    sub-long/2addr v7, v5

    invoke-static {v1, v7, v8}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$formatPoints(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;J)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v4, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 204
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getLoyaltySettings$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/loyalty/LoyaltySettings;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/loyalty/LoyaltySettings;->pointsTerms()Lcom/squareup/loyalty/PointsTerms;

    move-result-object v1

    iget-wide v7, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->$currentPoints:J

    sub-long/2addr v7, v5

    long-to-int v2, v7

    invoke-virtual {v1, v2}, Lcom/squareup/loyalty/PointsTerms;->getTerm(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 202
    invoke-virtual {p1, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 206
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 207
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_1

    .line 211
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getRes$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/crmscreens/R$string;->crm_points_positive_adjusted_balance:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 212
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    iget-wide v6, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->$currentPoints:J

    invoke-static {v2, v6, v7}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$formatPoints(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;J)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v5, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 213
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    int-to-long v5, p1

    invoke-static {v2, v5, v6}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$formatPoints(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;J)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v2, "added"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 214
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    iget-wide v7, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->$currentPoints:J

    add-long/2addr v7, v5

    invoke-static {v1, v7, v8}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$formatPoints(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;J)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v4, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 217
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->this$0:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;->access$getLoyaltySettings$p(Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator;)Lcom/squareup/loyalty/LoyaltySettings;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/loyalty/LoyaltySettings;->pointsTerms()Lcom/squareup/loyalty/PointsTerms;

    move-result-object v1

    iget-wide v7, p0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsCoordinator$attach$6;->$currentPoints:J

    sub-long/2addr v7, v5

    long-to-int v2, v7

    invoke-virtual {v1, v2}, Lcom/squareup/loyalty/PointsTerms;->getTerm(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 215
    invoke-virtual {p1, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 219
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 220
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 192
    :goto_1
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
