.class public Lcom/squareup/ui/crm/cards/CreateNoteView;
.super Landroid/widget/LinearLayout;
.source "CreateNoteView.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private messageLeft:Lcom/squareup/widgets/MessageView;

.field private messageRight:Lcom/squareup/widgets/MessageView;

.field private noteEdit:Landroid/widget/EditText;

.field presenter:Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/view/View;

.field private reminder:Lcom/squareup/ui/account/view/SmartLineRow;

.field private final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    const-class p2, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Component;->inject(Lcom/squareup/ui/crm/cards/CreateNoteView;)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/CreateNoteView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->shortAnimTimeMs:I

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 119
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 120
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_create_note:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->noteEdit:Landroid/widget/EditText;

    .line 121
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_note_message_left:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->messageLeft:Lcom/squareup/widgets/MessageView;

    .line 122
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_note_message_right:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->messageRight:Lcom/squareup/widgets/MessageView;

    .line 123
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->progressBar:Landroid/view/View;

    .line 124
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_create_note_reminder:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->reminder:Lcom/squareup/ui/account/view/SmartLineRow;

    return-void
.end method


# virtual methods
.method getNote()Ljava/lang/String;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->noteEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$setInitialFocus$0$CreateNoteView()V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->noteEdit:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method noteIsBlank()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->noteEdit:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Views;->isBlank(Landroid/widget/TextView;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 52
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->presenter:Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->presenter:Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 58
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .line 47
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/CreateNoteView;->bindViews()V

    return-void
.end method

.method onReminderClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->reminder:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method setActionBarPrimaryButtonEnabled(Z)V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method

.method setActionBarUpButtonEnabled(Z)V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->noteEdit:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    return-void
.end method

.method setInitialFocus()V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->noteEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->noteEdit:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteView$3_ombkYOZ605YKFhdr9fel9ZxEc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteView$3_ombkYOZ605YKFhdr9fel9ZxEc;-><init>(Lcom/squareup/ui/crm/cards/CreateNoteView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method showMessageLeft(Ljava/lang/CharSequence;)V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->messageLeft:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showMessageRight(Ljava/lang/CharSequence;)V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->messageRight:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->messageRight:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showProgress(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->progressBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 75
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->progressBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    :goto_0
    return-void
.end method

.method showReminder(Ljava/lang/String;)V
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->reminder:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->reminder:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteView;->reminder:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    return-void
.end method
