.class public interface abstract Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;
.super Ljava/lang/Object;
.source "UpdateGroup2Screen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation


# virtual methods
.method public abstract commitUpdateGroup2Screen(Lcom/squareup/protos/client/rolodex/Group;)V
.end method

.method public abstract dismissUpdateGroup2Screen()V
.end method

.method public abstract getGroup()Lcom/squareup/protos/client/rolodex/Group;
.end method

.method public abstract setGroup(Lcom/squareup/protos/client/rolodex/Group;)V
.end method

.method public abstract showCreateFilterScreen()V
.end method

.method public abstract showUpdateFilterScreen(I)V
.end method
