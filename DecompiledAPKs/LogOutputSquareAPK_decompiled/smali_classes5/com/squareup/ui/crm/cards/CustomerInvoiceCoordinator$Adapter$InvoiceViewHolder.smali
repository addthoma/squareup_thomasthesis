.class Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;
.super Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$ViewHolder;
.source "CustomerInvoiceCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InvoiceViewHolder"
.end annotation


# instance fields
.field final row:Lcom/squareup/ui/account/view/SmartLineRow;

.field final synthetic this$1:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;Landroid/view/View;)V
    .locals 0

    .line 213
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->this$1:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;

    .line 214
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$ViewHolder;-><init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;Landroid/view/View;)V

    .line 215
    check-cast p2, Lcom/squareup/ui/account/view/SmartLineRow;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    return-void
.end method


# virtual methods
.method public bind(I)V
    .locals 5

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->this$1:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;

    iget-object v0, v0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;->this$0:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->access$200(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    .line 220
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 221
    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->forInvoiceDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/InvoiceDisplayState;

    move-result-object v0

    .line 223
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->this$1:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;

    iget-object v1, v1, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;->this$0:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->access$400(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_invoice_row:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->this$1:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;

    iget-object v2, v2, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;->this$0:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;

    iget-object v2, v2, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v3, v3, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    .line 224
    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "money"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    const-string v3, "number"

    .line 225
    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 226
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 229
    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/Invoice;->invoice_name:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 230
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v3, v3, Lcom/squareup/protos/client/invoice/Invoice;->invoice_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    .line 233
    :goto_0
    iget-object v3, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 234
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 235
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->getTitle()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(I)V

    .line 236
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->getDisplayColor()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleColor(I)V

    .line 237
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->this$1:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;

    iget-object v3, v3, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;->this$0:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;

    invoke-static {v3}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->access$400(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)Lcom/squareup/util/Res;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->this$1:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;

    iget-object v4, v4, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;->this$0:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;

    invoke-static {v4}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->access$500(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)Ljava/text/DateFormat;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/squareup/crm/DateTimeFormatHelper;->formatDateForInvoice(Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/util/Res;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 239
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setPreserveValueText(Z)V

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    sget-object v1, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTag(Ljava/lang/Object;)V

    return-void
.end method
