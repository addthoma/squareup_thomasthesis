.class Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "AllAppointmentsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;->bind(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;

.field final synthetic val$appointment:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;)V
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder$1;->this$2:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder$1;->val$appointment:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 140
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder$1;->this$2:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;

    iget-object p1, p1, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;->this$1:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;

    iget-object p1, p1, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;->this$0:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->access$200(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;)Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder$1;->val$appointment:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;

    iget-object v0, v0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;->appointmentId:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder$1;->val$appointment:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;

    iget-object v1, v1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;->occurrenceStart:Lorg/threeten/bp/Instant;

    invoke-interface {p1, v0, v1}, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;->showAppointmentDetail(Ljava/lang/String;Lorg/threeten/bp/Instant;)V

    return-void
.end method
