.class public Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ReviewCustomerForMergingCoordinator.java"


# instance fields
.field private personalInfoSection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

.field private final personalInformationViewDataRenderer:Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 30
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;->runner:Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;->res:Lcom/squareup/util/Res;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;->personalInformationViewDataRenderer:Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 59
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_personal_information_section:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;->personalInfoSection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    .line 37
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;->bindViews(Landroid/view/View;)V

    .line 41
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;->runner:Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;->getContactForReviewCustomerForMergingScreen()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    .line 44
    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;->res:Lcom/squareup/util/Res;

    .line 45
    invoke-static {v1, v4}, Lcom/squareup/crm/util/RolodexContactHelper;->getFullName(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;->runner:Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v4, Lcom/squareup/ui/crm/cards/-$$Lambda$smpsOYRsQICBHi_VJKR2HSaX7pU;

    invoke-direct {v4, v3}, Lcom/squareup/ui/crm/cards/-$$Lambda$smpsOYRsQICBHi_VJKR2HSaX7pU;-><init>(Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;)V

    .line 46
    invoke-virtual {v2, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crmviewcustomer/R$string;->crm_merge_label:I

    .line 47
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/crm/cards/-$$Lambda$ReviewCustomerForMergingCoordinator$B-2peBHiLEBOqBiOn-9-QiVgSh0;

    invoke-direct {v3, p0, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ReviewCustomerForMergingCoordinator$B-2peBHiLEBOqBiOn-9-QiVgSh0;-><init>(Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 48
    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 49
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v2

    .line 44
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;->runner:Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$smpsOYRsQICBHi_VJKR2HSaX7pU;

    invoke-direct {v2, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$smpsOYRsQICBHi_VJKR2HSaX7pU;-><init>(Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;)V

    invoke-static {p1, v2}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 53
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ReviewCustomerForMergingCoordinator$jFChlXIWMtb28DlbFyLoww2WmFg;

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ReviewCustomerForMergingCoordinator$jFChlXIWMtb28DlbFyLoww2WmFg;-><init>(Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;Lcom/squareup/protos/client/rolodex/Contact;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$ReviewCustomerForMergingCoordinator(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;->runner:Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;->mergeContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public synthetic lambda$attach$1$ReviewCustomerForMergingCoordinator(Lcom/squareup/protos/client/rolodex/Contact;)Lrx/Subscription;
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;->personalInformationViewDataRenderer:Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->toViewData(Lcom/squareup/protos/client/rolodex/Contact;Z)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingCoordinator;->personalInfoSection:Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$bu9eNKHeP4CMLkJ5YNeA6gYDR8Y;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$bu9eNKHeP4CMLkJ5YNeA6gYDR8Y;-><init>(Lcom/squareup/ui/crm/v2/profile/SimpleSectionView;)V

    .line 55
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method
