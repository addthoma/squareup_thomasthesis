.class Lcom/squareup/ui/crm/v2/profile/NotesSectionView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "NotesSectionView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->bindRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/Note;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/NotesSectionView;

.field final synthetic val$note:Lcom/squareup/protos/client/rolodex/Note;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/NotesSectionView;Lcom/squareup/protos/client/rolodex/Note;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView$1;->this$0:Lcom/squareup/ui/crm/v2/profile/NotesSectionView;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView$1;->val$note:Lcom/squareup/protos/client/rolodex/Note;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 103
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView$1;->this$0:Lcom/squareup/ui/crm/v2/profile/NotesSectionView;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->access$000(Lcom/squareup/ui/crm/v2/profile/NotesSectionView;)Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView$1;->val$note:Lcom/squareup/protos/client/rolodex/Note;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
