.class public Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "ChooseCustomer2ScreenV2.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Component;,
        Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;,
        Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 325
    sget-object v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$e_yHLegqXco2eUwxFQtn9mnOmn8;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$e_yHLegqXco2eUwxFQtn9mnOmn8;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/crm/flow/CrmScope;)V
    .locals 0

    .line 59
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2;
    .locals 1

    .line 326
    const-class v0, Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/flow/CrmScope;

    .line 327
    new-instance v0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 321
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InCrmScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 322
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2;->crmPath:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 63
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CHOOSE_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 71
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 67
    sget v0, Lcom/squareup/crmviewcustomer/R$layout;->crm_v2_choose_customer2_view:I

    return v0
.end method
