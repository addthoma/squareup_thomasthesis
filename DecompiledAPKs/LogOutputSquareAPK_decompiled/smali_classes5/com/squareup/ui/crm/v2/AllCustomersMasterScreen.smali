.class public final Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen;
.super Lcom/squareup/ui/crm/applet/InCustomersAppletScope;
.source "AllCustomersMasterScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/Master;
    applet = Lcom/squareup/ui/crm/applet/CustomersApplet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen;

    invoke-direct {v0}, Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen;

    .line 46
    sget-object v0, Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen;

    .line 47
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/ui/crm/applet/InCustomersAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_CONTACTS_LIST:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 30
    const-class v0, Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen$Component;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen$Component;->coordinator()Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 50
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_v2_all_customers_list:I

    return v0
.end method
