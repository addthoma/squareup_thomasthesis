.class Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "ChooseCustomer2ViewV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2$1;->this$0:Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2$1;->this$0:Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;

    iget-object v0, v0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->presenter:Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2$1;->this$0:Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->onSearchCustomerPressed(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method
