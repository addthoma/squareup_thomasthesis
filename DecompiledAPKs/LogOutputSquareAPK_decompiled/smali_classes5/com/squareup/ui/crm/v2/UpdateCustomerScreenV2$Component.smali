.class public interface abstract Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$Component;
.super Ljava/lang/Object;
.source "UpdateCustomerScreenV2.java"

# interfaces
.implements Lcom/squareup/ui/ErrorsBarView$Component;
.implements Lcom/squareup/ui/crm/v2/profile/ContactEditView$Component;
.implements Lcom/squareup/address/AddressLayout$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract coordinator()Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;
.end method
