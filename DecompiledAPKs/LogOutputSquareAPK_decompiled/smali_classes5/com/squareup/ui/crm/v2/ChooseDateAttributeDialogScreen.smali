.class public final Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen;
.super Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;
.source "ChooseDateAttributeDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Factory;,
        Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Component;,
        Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 108
    sget-object v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseDateAttributeDialogScreen$Nje5MWotvCStUVj918vbwPMmqt0;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseDateAttributeDialogScreen$Nje5MWotvCStUVj918vbwPMmqt0;

    .line 109
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen;
    .locals 1

    .line 110
    const-class v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    .line 111
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    .line 112
    new-instance v0, Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 104
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
