.class public final Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen$Factory;
.super Ljava/lang/Object;
.source "DeleteLoyaltyAccountConfirmationScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen$Runner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 50
    invoke-interface {p0}, Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen$Runner;->deleteLoyaltyAccount()V

    return-void
.end method

.method static synthetic lambda$create$1(Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen$Runner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 54
    invoke-interface {p0}, Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen$Runner;->cancelDeleteLoyaltyAccount()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 44
    const-class v0, Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen$Component;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen$Component;->runner()Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen$Runner;

    move-result-object v0

    .line 46
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/crmscreens/R$string;->crm_loyalty_delete_account_title:I

    .line 47
    invoke-virtual {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 48
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen$Runner;->getDeleteLoyaltyAccountConfirmationText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_delete:I

    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$DeleteLoyaltyAccountConfirmationScreen$Factory$gozzSQVGm_MfhwKcEzVY-7yQxOg;

    invoke-direct {v2, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$DeleteLoyaltyAccountConfirmationScreen$Factory$gozzSQVGm_MfhwKcEzVY-7yQxOg;-><init>(Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen$Runner;)V

    .line 49
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/marin/R$drawable;->marin_selector_red:I

    .line 51
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/marin/R$color;->marin_white:I

    .line 52
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$DeleteLoyaltyAccountConfirmationScreen$Factory$2y5dyzWSWQRRzVW0tYFyXVq03eM;

    invoke-direct {v2, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$DeleteLoyaltyAccountConfirmationScreen$Factory$2y5dyzWSWQRRzVW0tYFyXVq03eM;-><init>(Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen$Runner;)V

    .line 53
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 55
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 56
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 46
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
