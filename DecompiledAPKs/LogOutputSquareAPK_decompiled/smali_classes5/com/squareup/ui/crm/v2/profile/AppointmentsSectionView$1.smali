.class Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "AppointmentsSectionView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->setViewData(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;Lcom/squareup/ui/crm/flow/CrmScopeType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;

.field final synthetic val$appointment:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;)V
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView$1;->this$0:Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView$1;->val$appointment:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView$1;->this$0:Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;->access$000(Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView;)Lio/reactivex/subjects/PublishSubject;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AppointmentsSectionView$1;->val$appointment:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;

    invoke-virtual {p1, v0}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
