.class Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "AllCouponsAndRewardsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AllCouponsAndRewardsAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private allSubscriptions:Lrx/subscriptions/CompositeSubscription;

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 90
    new-instance p1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->allSubscriptions:Lrx/subscriptions/CompositeSubscription;

    .line 170
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->items:Ljava/util/List;

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;)Lrx/subscriptions/CompositeSubscription;
    .locals 0

    .line 88
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->allSubscriptions:Lrx/subscriptions/CompositeSubscription;

    return-object p0
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 88
    check-cast p1, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->onBindViewHolder(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;I)V
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->bindCoupon(Lcom/squareup/protos/client/coupons/Coupon;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 88
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;
    .locals 0

    .line 180
    sget p2, Lcom/squareup/crmscreens/R$layout;->crm_v2_view_all_coupons_and_rewards_row:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 181
    new-instance p2, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;-><init>(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;Lcom/squareup/ui/account/view/SmartLineRow;)V

    return-object p2
.end method

.method public onDetachedFromRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 0

    .line 195
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->onDetachedFromRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 196
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->allSubscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {p1}, Lrx/subscriptions/CompositeSubscription;->clear()V

    return-void
.end method

.method updateItems(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;)V"
        }
    .end annotation

    .line 173
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->items:Ljava/util/List;

    .line 174
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->notifyDataSetChanged()V

    return-void
.end method
