.class public final synthetic Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$Px5FFOJmgeC5ZNv26kIDOMS4ujo;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

.field private final synthetic f$1:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

.field private final synthetic f$2:Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$Px5FFOJmgeC5ZNv26kIDOMS4ujo;->f$0:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$Px5FFOJmgeC5ZNv26kIDOMS4ujo;->f$1:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$Px5FFOJmgeC5ZNv26kIDOMS4ujo;->f$2:Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$Px5FFOJmgeC5ZNv26kIDOMS4ujo;->f$0:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$Px5FFOJmgeC5ZNv26kIDOMS4ujo;->f$1:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$Px5FFOJmgeC5ZNv26kIDOMS4ujo;->f$2:Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->lambda$addAttributeRow$4$ContactEditView(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;Landroid/widget/CompoundButton;Z)V

    return-void
.end method
