.class public Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;
.super Ljava/lang/Object;
.source "CardOnFileSectionView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewData"
.end annotation


# instance fields
.field final headerActionButtonState:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

.field final lineData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;",
            ">;"
        }
    .end annotation
.end field

.field final visible:Z


# direct methods
.method public constructor <init>(ZLcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-boolean p1, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;->visible:Z

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;->headerActionButtonState:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;->lineData:Ljava/util/List;

    return-void
.end method
