.class Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;
.super Lmortar/ViewPresenter;
.source "RewardsSectionPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;",
        ">;"
    }
.end annotation


# static fields
.field public static final SHOW_VIEW_ALL_REWARDS_SIZE:I = 0x7


# instance fields
.field private final contact:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private final formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

.field private holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

.field private final locale:Ljava/util/Locale;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/util/Res;Ljava/util/Locale;Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 50
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 41
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    .line 52
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->res:Lcom/squareup/util/Res;

    .line 53
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->locale:Ljava/util/Locale;

    .line 54
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;)Lcom/squareup/checkout/HoldsCoupons;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    return-object p0
.end method

.method private bindRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/coupons/Coupon;)V
    .locals 5

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->formatName(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->locale:Ljava/util/Locale;

    invoke-static {v0, p2, v1}, Lcom/squareup/crm/DateTimeFormatHelper;->formatCouponDetails(Lcom/squareup/util/Res;Lcom/squareup/protos/client/coupons/Coupon;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 106
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 107
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    .line 108
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    goto :goto_0

    .line 110
    :cond_0
    invoke-virtual {p1, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 112
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->getStartGlyphView()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    .line 114
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/squareup/crm/R$drawable;->crm_payment_giftcard:I

    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 113
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 116
    invoke-virtual {p1, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgedIconBlockVisibility(I)V

    .line 117
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->getEndGlyph()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    .line 119
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/crm/R$drawable;->crm_action_arrow:I

    invoke-static {v1, v2, v4}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 118
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    if-eqz v0, :cond_1

    .line 123
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$RewardsSectionPresenter$P34XRPJkb0eoMP53hpjFVbVUICE;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$RewardsSectionPresenter$P34XRPJkb0eoMP53hpjFVbVUICE;-><init>(Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/ui/account/view/SmartLineRow;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    :cond_1
    return-void
.end method

.method private renderCouponsSection(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;)V"
        }
    .end annotation

    .line 88
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;

    .line 90
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->setVisible(ZZ)V

    .line 92
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 93
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v3, 0x7

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/4 v4, 0x0

    .line 95
    invoke-interface {p1, v4, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/client/coupons/Coupon;

    .line 96
    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->addCouponRow()Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v6

    invoke-direct {p0, v6, v5}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->bindRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/coupons/Coupon;)V

    goto :goto_0

    .line 99
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-le p1, v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v0, v2}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->setViewAllVisible(Z)V

    :cond_2
    return-void
.end method


# virtual methods
.method public synthetic lambda$bindRow$4$RewardsSectionPresenter(Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/ui/account/view/SmartLineRow;)Lrx/Subscription;
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    invoke-interface {v0}, Lcom/squareup/checkout/HoldsCoupons;->discountsChanged()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 125
    invoke-virtual {v0, v1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$RewardsSectionPresenter$Yi2HxJxGtOoFjn0fqfY2DYKhorM;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$RewardsSectionPresenter$Yi2HxJxGtOoFjn0fqfY2DYKhorM;-><init>(Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;Lcom/squareup/protos/client/coupons/Coupon;)V

    .line 126
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$RewardsSectionPresenter$lqt00v5LXGLvw2nKEsK0Twixu4A;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$RewardsSectionPresenter$lqt00v5LXGLvw2nKEsK0Twixu4A;-><init>(Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/coupons/Coupon;)V

    .line 128
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$0$RewardsSectionPresenter(Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;Lkotlin/Pair;)V
    .locals 2

    .line 67
    invoke-virtual {p2}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    .line 68
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->clearCouponRows()V

    .line 69
    sget-object v1, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter$3;->$SwitchMap$com$squareup$ui$crm$flow$AbstractCrmScopeRunner$ContactLoadingState:[I

    invoke-virtual {p2}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    invoke-virtual {p2}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->ordinal()I

    move-result p2

    aget p2, v1, p2

    const/4 v1, 0x1

    if-eq p2, v1, :cond_1

    const/4 v1, 0x2

    if-eq p2, v1, :cond_1

    const/4 v1, 0x3

    if-eq p2, v1, :cond_1

    const/4 p1, 0x4

    if-eq p2, p1, :cond_0

    goto :goto_0

    .line 77
    :cond_0
    iget-object p1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->coupon:Ljava/util/List;

    invoke-static {p1}, Lcom/squareup/util/SquareCollections;->emptyIfNull(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->renderCouponsSection(Ljava/util/List;)V

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    .line 74
    invoke-virtual {p1, p2, p2}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;->setVisible(ZZ)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$2$RewardsSectionPresenter(Lcom/squareup/protos/client/coupons/Coupon;Lkotlin/Unit;)Ljava/lang/Boolean;
    .locals 0

    .line 126
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    iget-object p1, p1, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    invoke-interface {p2, p1}, Lcom/squareup/checkout/HoldsCoupons;->isCouponAddedToCart(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$3$RewardsSectionPresenter(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/coupons/Coupon;Ljava/lang/Boolean;)V
    .locals 0

    .line 129
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    if-eqz p3, :cond_0

    .line 130
    sget p3, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-virtual {p1, p3}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleColor(I)V

    .line 132
    new-instance p3, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter$1;

    invoke-direct {p3, p0, p2}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter$1;-><init>(Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;Lcom/squareup/protos/client/coupons/Coupon;)V

    invoke-virtual {p1, p3}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 138
    :cond_0
    sget p3, Lcom/squareup/marin/R$color;->marin_black:I

    invoke-virtual {p1, p3}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleColor(I)V

    .line 140
    new-instance p3, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter$2;

    invoke-direct {p3, p0, p2}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter$2;-><init>(Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;Lcom/squareup/protos/client/coupons/Coupon;)V

    invoke-virtual {p1, p3}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onLoad$1$RewardsSectionPresenter(Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;)Lrx/Subscription;
    .locals 3

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->loyaltyStatus()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse$Builder;-><init>()V

    .line 64
    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    .line 65
    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->loyaltyLoadingState()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$_VPzt3MJSmjJvoYT4sBX2uSy9YU;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$_VPzt3MJSmjJvoYT4sBX2uSy9YU;

    .line 63
    invoke-static {v0, v1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$RewardsSectionPresenter$qNWL0WUWX8O8Wjadd-Xj_D3-gkQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$RewardsSectionPresenter$qNWL0WUWX8O8Wjadd-Xj_D3-gkQ;-><init>(Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;)V

    .line 66
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 58
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 59
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getHoldsCoupons()Lcom/squareup/checkout/HoldsCoupons;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    .line 60
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;

    .line 62
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$RewardsSectionPresenter$-3_d8FbXxg--WKkkAp1Uv44EDeQ;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$RewardsSectionPresenter$-3_d8FbXxg--WKkkAp1Uv44EDeQ;-><init>(Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;Lcom/squareup/ui/crm/v2/profile/RewardsSectionView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method setContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
