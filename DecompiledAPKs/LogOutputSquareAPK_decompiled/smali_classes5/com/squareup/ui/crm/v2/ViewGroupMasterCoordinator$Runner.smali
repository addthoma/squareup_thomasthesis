.class public interface abstract Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;
.super Ljava/lang/Object;
.source "ViewGroupMasterCoordinator.java"

# interfaces
.implements Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeViewGroupMasterScreen()V
.end method

.method public abstract editGroup()V
.end method

.method public abstract getGroupForViewGroupScreen()Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getGroupTokenForViewGroupScreen()Ljava/lang/String;
.end method

.method public abstract viewGroupHardwareBackButton()V
.end method
