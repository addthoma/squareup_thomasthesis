.class final Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$7;
.super Lkotlin/jvm/internal/Lambda;
.source "LoyaltySectionPresenter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "items",
        "",
        "Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$7;->this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$7;->invoke(Ljava/util/List;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;",
            ">;)V"
        }
    .end annotation

    .line 127
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->access$getREWARD_TIERS_MAX$cp()I

    move-result v1

    const/4 v2, 0x0

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 129
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$7;->this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->access$getView(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;

    move-result-object v1

    if-eqz v0, :cond_1

    .line 131
    invoke-static {}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->access$getREWARD_TIERS_MAX$cp()I

    move-result v3

    invoke-interface {p1, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    .line 129
    :cond_1
    invoke-virtual {v1, p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->setRewardTiersMultiRedemption(Ljava/util/List;)V

    .line 135
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$7;->this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->access$getView(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->setSeeAllRewardsVisibility(Z)V

    return-void
.end method
