.class public final Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen;
.super Lcom/squareup/ui/crm/applet/InCustomersAppletScope;
.source "ViewGroupMasterScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/Master;
    applet = Lcom/squareup/ui/crm/applet/CustomersApplet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final isSmartGroup:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 64
    sget-object v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterScreen$pNhbn8arX0sH-rLlvv0I24l7uzU;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterScreen$pNhbn8arX0sH-rLlvv0I24l7uzU;

    .line 65
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/ui/crm/applet/InCustomersAppletScope;-><init>()V

    .line 37
    iput-boolean p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen;->isSmartGroup:Z

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen;
    .locals 1

    .line 66
    new-instance v0, Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen;

    invoke-virtual {p0}, Landroid/os/Parcel;->readByte()B

    move-result p0

    invoke-static {p0}, Lcom/squareup/util/Booleans;->toBoolean(B)Z

    move-result p0

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen;-><init>(Z)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 60
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/applet/InCustomersAppletScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 61
    iget-boolean p2, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen;->isSmartGroup:Z

    invoke-static {p2}, Lcom/squareup/util/Booleans;->toByte(Z)B

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 41
    iget-boolean v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen;->isSmartGroup:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_GROUPS_SMART_GROUP:Lcom/squareup/analytics/RegisterViewName;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_GROUPS_MANUAL_GROUP:Lcom/squareup/analytics/RegisterViewName;

    :goto_0
    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 47
    const-class v0, Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen$Component;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen$Component;->coordinator()Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 69
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_v2_view_group:I

    return v0
.end method
