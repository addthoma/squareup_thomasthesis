.class Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;
.super Lmortar/ViewPresenter;
.source "ContactEditPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/v2/profile/ContactEditView;",
        ">;"
    }
.end annotation


# static fields
.field private static final DEFAULT_BIRTH_YEAR:I


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private analyticsDeduplicater:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private analyticsPathName:Ljava/lang/String;

.field private attributes:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
            ">;"
        }
    .end annotation
.end field

.field private final birthdayFormatter:Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;

.field final birthdayPopupPresenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;",
            "Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final contact:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatCustomAttr:Ljava/text/DateFormat;

.field private final features:Lcom/squareup/settings/server/Features;

.field private isTyping:Ljava/lang/Boolean;

.field private final locale:Ljava/util/Locale;

.field private final rolodexMerchantLoader:Lcom/squareup/crm/RolodexMerchantLoader;

.field private final sellerTyping:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

.field private final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 69
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x14

    sput v0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->DEFAULT_BIRTH_YEAR:I

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;Lcom/squareup/crm/RolodexMerchantLoader;Lcom/squareup/settings/server/Features;Ljava/util/Locale;Ljava/text/DateFormat;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/analytics/Analytics;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 106
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 79
    invoke-static {}, Lcom/squareup/crm/util/RolodexContactHelper;->newContactBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 80
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->sellerTyping:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v0, 0x0

    .line 82
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->isTyping:Ljava/lang/Boolean;

    .line 85
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->attributes:Ljava/util/LinkedHashMap;

    .line 89
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->analyticsDeduplicater:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 91
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$1;

    const-string v1, "BIRTHDAY"

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$1;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->birthdayPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    .line 107
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->birthdayFormatter:Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;

    .line 108
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->rolodexMerchantLoader:Lcom/squareup/crm/RolodexMerchantLoader;

    .line 109
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->features:Lcom/squareup/settings/server/Features;

    .line 110
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->locale:Ljava/util/Locale;

    .line 111
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->dateFormatCustomAttr:Ljava/text/DateFormat;

    .line 112
    iput-object p6, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 113
    iput-object p7, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;)V
    .locals 0

    .line 67
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->onBirthdayPicked(Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;)V
    .locals 0

    .line 67
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->onBirthdayRemoved()V

    return-void
.end method

.method static synthetic lambda$null$3(Lcom/squareup/protos/client/rolodex/Merchant;)Lcom/squareup/protos/client/rolodex/AttributeSchema;
    .locals 0

    .line 230
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Merchant;->attribute_schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;

    return-object p0
.end method

.method private logAnalyticsEvent(Ljava/lang/String;)V
    .locals 1

    .line 326
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->analyticsDeduplicater:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private onBirthdayPicked(Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;)V
    .locals 2

    .line 164
    new-instance v0, Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;-><init>()V

    iget v1, p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->day:I

    .line 165
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->day_of_month(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    move-result-object v0

    iget v1, p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->month:I

    add-int/lit8 v1, v1, 0x1

    .line 166
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->month_of_year(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    move-result-object v0

    iget-boolean v1, p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->yearIsKnown:Z

    if-eqz v1, :cond_0

    iget p1, p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;->year:I

    .line 167
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->year(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    move-result-object p1

    .line 168
    invoke-virtual {p1}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->build()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v1, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->withBirthday(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 171
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->birthdayFormatter:Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;->format(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->showBirthday(Ljava/lang/String;)V

    const-string p1, "Birthday"

    .line 172
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->logAnalyticsEvent(Ljava/lang/String;)V

    return-void
.end method

.method private onBirthdayRemoved()V
    .locals 3

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Contact;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/squareup/crm/util/RolodexContactHelper;->withBirthday(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 177
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->showBirthday(Ljava/lang/String;)V

    return-void
.end method

.method private subscribeFieldChangeToContact(Landroid/view/View;Lrx/Observable;Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;ZLjava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;",
            "Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 306
    new-instance v6, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$AbjBGKsilWGQvsv4zVM_SCOOr-o;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$AbjBGKsilWGQvsv4zVM_SCOOr-o;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;Lrx/Observable;Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;ZLjava/lang/String;)V

    invoke-static {p1, v6}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private updateView(Lcom/squareup/ui/crm/v2/profile/ContactEditView;)V
    .locals 13

    .line 185
    invoke-static {p1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetachNow(Landroid/view/View;)V

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Contact;

    .line 189
    invoke-static {v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getFirstName(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->setFirstName(Ljava/lang/String;)V

    .line 190
    invoke-static {v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getLastName(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->setLastName(Ljava/lang/String;)V

    .line 191
    invoke-static {v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getEmail(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->setEmail(Ljava/lang/String;)V

    .line 192
    invoke-static {v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getPhone(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->setPhone(Ljava/lang/String;)V

    .line 193
    invoke-static {v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getCompany(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->setCompany(Ljava/lang/String;)V

    .line 194
    invoke-static {v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getReferenceId(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->setReferenceId(Ljava/lang/String;)V

    .line 195
    invoke-static {v0}, Lcom/squareup/crm/util/RolodexContactHelper;->formatManualGroups(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->setGroups(Ljava/lang/String;)V

    .line 196
    invoke-static {v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getAddress(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/address/Address;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->setAddress(Lcom/squareup/address/Address;)V

    .line 197
    invoke-static {v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getNote(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->setNote(Ljava/lang/String;)V

    .line 198
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->birthdayFormatter:Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;

    invoke-static {v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getBirthday(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;->format(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->showBirthday(Ljava/lang/String;)V

    .line 201
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->firstName()Lrx/Observable;

    move-result-object v3

    sget-object v4, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$T1hmqWMBIiMZ9Fre84qvNhbqnLs;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$T1hmqWMBIiMZ9Fre84qvNhbqnLs;

    const/4 v5, 0x1

    const-string v6, "First Name"

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->subscribeFieldChangeToContact(Landroid/view/View;Lrx/Observable;Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;ZLjava/lang/String;)V

    .line 203
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->lastName()Lrx/Observable;

    move-result-object v9

    sget-object v10, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$uP21FVHlhI5yaRh5oI-peGmgxgw;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$uP21FVHlhI5yaRh5oI-peGmgxgw;

    const/4 v11, 0x1

    const-string v12, "Last Name"

    move-object v7, p0

    move-object v8, p1

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->subscribeFieldChangeToContact(Landroid/view/View;Lrx/Observable;Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;ZLjava/lang/String;)V

    .line 205
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->email()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$O_oZ2nqeCjZ8GjW-YRiXJxCvf2E;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$O_oZ2nqeCjZ8GjW-YRiXJxCvf2E;

    const/4 v4, 0x1

    const-string v5, "Email"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->subscribeFieldChangeToContact(Landroid/view/View;Lrx/Observable;Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;ZLjava/lang/String;)V

    .line 207
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->phone()Lrx/Observable;

    move-result-object v8

    sget-object v9, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$tihoGGFCUYQ4L_vh2hxZF1dYBeI;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$tihoGGFCUYQ4L_vh2hxZF1dYBeI;

    const/4 v10, 0x1

    const-string v11, "Phone"

    move-object v6, p0

    move-object v7, p1

    invoke-direct/range {v6 .. v11}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->subscribeFieldChangeToContact(Landroid/view/View;Lrx/Observable;Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;ZLjava/lang/String;)V

    .line 209
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->company()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$GSpTVP0l4NwDjiMu81Iag9sV4Aw;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$GSpTVP0l4NwDjiMu81Iag9sV4Aw;

    const-string v5, "Company"

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->subscribeFieldChangeToContact(Landroid/view/View;Lrx/Observable;Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;ZLjava/lang/String;)V

    .line 211
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->referenceId()Lrx/Observable;

    move-result-object v8

    sget-object v9, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$tzBywS6W-N_S6SV8l98GfOe8UEM;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$tzBywS6W-N_S6SV8l98GfOe8UEM;

    const-string v11, "Reference Id"

    invoke-direct/range {v6 .. v11}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->subscribeFieldChangeToContact(Landroid/view/View;Lrx/Observable;Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;ZLjava/lang/String;)V

    .line 213
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->note()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$7WQc9-6lEnNse3z_55A0SS0MZvI;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$7WQc9-6lEnNse3z_55A0SS0MZvI;

    const/4 v4, 0x0

    const-string v5, "Memo"

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->subscribeFieldChangeToContact(Landroid/view/View;Lrx/Observable;Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;ZLjava/lang/String;)V

    .line 217
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$mG2oQ_JVY9Lru5TrpOJLG-0z3wc;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$mG2oQ_JVY9Lru5TrpOJLG-0z3wc;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;Lcom/squareup/ui/crm/v2/profile/ContactEditView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_CUSTOM_ATTRIBUTES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$ZS5XsPpJb0iMliOG3Jgox8f75TM;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$ZS5XsPpJb0iMliOG3Jgox8f75TM;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;Lcom/squareup/ui/crm/v2/profile/ContactEditView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 274
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$5sPGXimS_xzzEXdVmLB40NT_1lc;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$5sPGXimS_xzzEXdVmLB40NT_1lc;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;Lcom/squareup/ui/crm/v2/profile/ContactEditView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 291
    :cond_0
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$MGfMHAS48jaA3kjLqZwTwxKcQBk;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$MGfMHAS48jaA3kjLqZwTwxKcQBk;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method contact()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method getFormattedDateAttribute(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
    .locals 2

    .line 317
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->dateFormatCustomAttr:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->locale:Ljava/util/Locale;

    invoke-static {p1, v1}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$1$ContactEditPresenter(Lcom/squareup/address/Address;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 220
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v1, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->withAddress(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/address/Address;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    const-string p1, "Address"

    .line 222
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->logAnalyticsEvent(Ljava/lang/String;)V

    .line 223
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->sellerTyping:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$10$ContactEditPresenter(Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 309
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 310
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz p2, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p4

    :cond_0
    invoke-interface {p1, v1, p4}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;->call(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 311
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->sellerTyping:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 p2, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 312
    invoke-direct {p0, p3}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->logAnalyticsEvent(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$null$4$ContactEditPresenter(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lkotlin/Pair;)V
    .locals 8

    .line 233
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 235
    invoke-virtual {p2}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/AttributeSchema;

    .line 236
    invoke-virtual {p2}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/rolodex/Contact;

    .line 241
    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Contact;->schema_version:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema;->version:Ljava/lang/String;

    iget-object v2, p2, Lcom/squareup/protos/client/rolodex/Contact;->schema_version:Ljava/lang/String;

    .line 242
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 243
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->rolodexMerchantLoader:Lcom/squareup/crm/RolodexMerchantLoader;

    invoke-interface {p1}, Lcom/squareup/crm/RolodexMerchantLoader;->refresh()V

    return-void

    .line 247
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->showAttributeRows()V

    .line 248
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->clearAttributeRows()V

    .line 249
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->attributes:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->clear()V

    .line 251
    invoke-static {p2}, Lcom/squareup/crm/util/RolodexContactHelper;->getAttributeMapping(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/util/Map;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 252
    :goto_0
    iget-object v4, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema;->attribute_definitions:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 253
    iget-object v4, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema;->attribute_definitions:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    .line 255
    iget-object v5, v4, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_1

    goto :goto_1

    .line 259
    :cond_1
    iget-object v5, v4, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    .line 260
    iget-object v6, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->attributes:Ljava/util/LinkedHashMap;

    iget-object v7, v4, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    invoke-virtual {v6, v7, v5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    invoke-virtual {p1, v4, v5, v3}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->addAttributeRow(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;I)V

    if-eqz v5, :cond_2

    add-int/lit8 v3, v3, 0x1

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 269
    :cond_3
    iget-object p1, p2, Lcom/squareup/protos/client/rolodex/Contact;->schema_version:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 270
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema;->version:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/squareup/crm/util/RolodexContactHelper;->withSchemaVersion(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_4
    return-void
.end method

.method public synthetic lambda$null$6$ContactEditPresenter(Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;)V
    .locals 3

    .line 277
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->attributes:Ljava/util/LinkedHashMap;

    iget-object v1, p1, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;->attributeKey:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;->attribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->attributes:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const/4 v1, 0x0

    .line 279
    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 281
    iget-object v1, p1, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;->attribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    if-eqz v1, :cond_0

    .line 282
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Custom Field:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;->attribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 283
    invoke-static {v2}, Lcom/squareup/crm/util/RolodexContactHelper;->getAnalyticsNameForAttributeType(Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;->attribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->name:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 282
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->logAnalyticsEvent(Ljava/lang/String;)V

    .line 286
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v1, v0}, Lcom/squareup/crm/util/RolodexContactHelper;->withAttributes(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$8$ContactEditPresenter(Ljava/lang/Boolean;)V
    .locals 1

    .line 293
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->isTyping:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 294
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->sellerCreatingCustomer()Z

    const/4 p1, 0x1

    .line 295
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->isTyping:Ljava/lang/Boolean;

    :cond_0
    return-void
.end method

.method public synthetic lambda$onLoad$0$ContactEditPresenter(Ljava/lang/String;)V
    .locals 3

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->analyticsPathName:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    invoke-static {v1, v2, p1}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createEditContactEvent(Ljava/lang/String;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public synthetic lambda$subscribeFieldChangeToContact$11$ContactEditPresenter(Lrx/Observable;Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;ZLjava/lang/String;)Lrx/Subscription;
    .locals 1

    .line 307
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$fFlzs1vcFVYEjTFjNNuH5h1MRlU;

    invoke-direct {v0, p0, p2, p3, p4}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$fFlzs1vcFVYEjTFjNNuH5h1MRlU;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;ZLjava/lang/String;)V

    .line 308
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$updateView$2$ContactEditPresenter(Lcom/squareup/ui/crm/v2/profile/ContactEditView;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 218
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->address()Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$Yn7peYLNOaqTKVEkqLMvl9nhpcY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$Yn7peYLNOaqTKVEkqLMvl9nhpcY;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;)V

    .line 219
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$updateView$5$ContactEditPresenter(Lcom/squareup/ui/crm/v2/profile/ContactEditView;)Lrx/Subscription;
    .locals 3

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->rolodexMerchantLoader:Lcom/squareup/crm/RolodexMerchantLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexMerchantLoader;->merchant()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/squareup/receiving/StandardReceiver;->filterSuccess()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$ZF4MYJHDXtmvECpIx5A7Pa-nU80;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$ZF4MYJHDXtmvECpIx5A7Pa-nU80;

    .line 230
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v2, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$Ikok0X8I1I0ZxJ1XsfZVgZRGhdM;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$Ikok0X8I1I0ZxJ1XsfZVgZRGhdM;

    .line 231
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$xDkfplYoASxid1a1a9xPwGimfhI;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$xDkfplYoASxid1a1a9xPwGimfhI;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;Lcom/squareup/ui/crm/v2/profile/ContactEditView;)V

    .line 232
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$updateView$7$ContactEditPresenter(Lcom/squareup/ui/crm/v2/profile/ContactEditView;)Lrx/Subscription;
    .locals 1

    .line 275
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->onAttributeChanged()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$ugxFPPzyxUzjhEkgDJmTV3H43cw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$ugxFPPzyxUzjhEkgDJmTV3H43cw;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;)V

    .line 276
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$updateView$9$ContactEditPresenter()Lrx/Subscription;
    .locals 2

    .line 292
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->sellerTyping:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$re5O0b_yPvV2qGqluhUZCkrM6oo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$re5O0b_yPvV2qGqluhUZCkrM6oo;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method onBirthdayClicked()V
    .locals 12

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->sellerTyping:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 144
    sget v0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->DEFAULT_BIRTH_YEAR:I

    .line 150
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v2}, Lcom/squareup/crm/util/RolodexContactHelper;->getBirthday(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    .line 151
    iget-object v4, v2, Lcom/squareup/protos/common/time/YearMonthDay;->day_of_month:Ljava/lang/Integer;

    if-eqz v4, :cond_2

    iget-object v4, v2, Lcom/squareup/protos/common/time/YearMonthDay;->month_of_year:Ljava/lang/Integer;

    if-eqz v4, :cond_2

    .line 152
    iget-object v0, v2, Lcom/squareup/protos/common/time/YearMonthDay;->year:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, v2, Lcom/squareup/protos/common/time/YearMonthDay;->year:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->DEFAULT_BIRTH_YEAR:I

    .line 153
    :goto_0
    iget-object v4, v2, Lcom/squareup/protos/common/time/YearMonthDay;->month_of_year:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sub-int/2addr v4, v1

    .line 154
    iget-object v5, v2, Lcom/squareup/protos/common/time/YearMonthDay;->day_of_month:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 156
    iget-object v2, v2, Lcom/squareup/protos/common/time/YearMonthDay;->year:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    const/4 v3, 0x1

    :cond_1
    move v7, v0

    move v11, v3

    move v8, v4

    move v9, v5

    const/4 v10, 0x1

    goto :goto_1

    :cond_2
    move v7, v0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 159
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->birthdayPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    new-instance v1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;

    move-object v6, v1

    invoke-direct/range {v6 .. v11}, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;-><init>(IIIZZ)V

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 117
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 118
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->updateView(Lcom/squareup/ui/crm/v2/profile/ContactEditView;)V

    .line 123
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->analyticsDeduplicater:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/PublishRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$FXCUdR9XsZhH9M2rgznkq4g8MRo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditPresenter$FXCUdR9XsZhH9M2rgznkq4g8MRo;-><init>(Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;)V

    .line 124
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method setAnalyticsPathType(Ljava/lang/String;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;)V
    .locals 0

    .line 321
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->analyticsPathName:Ljava/lang/String;

    .line 322
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    return-void
.end method

.method setContact(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 133
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p2, :cond_0

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 137
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;->updateView(Lcom/squareup/ui/crm/v2/profile/ContactEditView;)V

    :cond_0
    return-void
.end method
