.class public interface abstract Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;
.super Ljava/lang/Object;
.source "ViewCustomerCoordinator.java"

# interfaces
.implements Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionRunner;
.implements Lcom/squareup/ui/crm/v2/profile/InvoicesSectionRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract addThisCustomerToSale()V
.end method

.method public abstract canStartNewSaleWithCustomerFromApplet()Z
.end method

.method public abstract contactForViewCustomerScreen()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end method

.method public abstract contactLoadingState()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getHoldsCoupons()Lcom/squareup/checkout/HoldsCoupons;
.end method

.method public abstract getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;
.end method

.method public abstract goBackFromViewCustomer()V
.end method

.method public abstract isCustomerAddedToSale(Ljava/lang/String;)Z
.end method

.method public abstract isForTransferringLoyalty()Z
.end method

.method public abstract isViewCustomerReadOnly()Z
.end method

.method public abstract loyaltyLoadingState()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract loyaltyStatus()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract newSaleWithCustomerFromApplet()V
.end method

.method public abstract removeThisCustomer()V
.end method

.method public abstract rewardTierClicked(Lcom/squareup/protos/client/loyalty/RewardTier;)V
.end method

.method public abstract seeAllRewardTiersClicked()V
.end method

.method public abstract showAllCouponsAndRewardsScreen()V
.end method

.method public abstract showAllFrequentItemsScreen()V
.end method

.method public abstract showAllNotesScreen()V
.end method

.method public abstract showAllPastAppointmentsScreen()V
.end method

.method public abstract showAllUpcomingAppointmentsScreen()V
.end method

.method public abstract showAppointmentDetail(Ljava/lang/String;Lorg/threeten/bp/Instant;)V
.end method

.method public abstract showBillHistoryScreen(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract showConversationScreen(Ljava/lang/String;)V
.end method

.method public abstract showCreateAppointment()V
.end method

.method public abstract showCreateNoteScreen()V
.end method

.method public abstract showCustomerActivityScreen()V
.end method

.method public abstract showDeleteSingleCustomerScreen()V
.end method

.method public abstract showImagePreviewScreen(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Attachment;)V
.end method

.method public abstract showLoyaltySectionDropDown()V
.end method

.method public abstract showManageCouponsAndRewardsScreen()V
.end method

.method public abstract showMergeContacts()V
.end method

.method public abstract showOverflowBottomSheet(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Attachment;)V
.end method

.method public abstract showProfileAttachmentsScreen()V
.end method

.method public abstract showProfileAttachmentsSection()Z
.end method

.method public abstract showSendMessageScreen()V
.end method

.method public abstract showUpdateCustomerV2()V
.end method

.method public abstract showUpdateLoyaltyPhoneScreen()V
.end method

.method public abstract showUploadFileBottomSheet()V
.end method

.method public abstract showViewNoteScreen(Lcom/squareup/protos/client/rolodex/Note;)V
.end method

.method public abstract title()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract transferLoyaltyAccount()V
.end method

.method public abstract unredeemRewardTier(Lcom/squareup/protos/client/loyalty/RewardTier;)V
.end method
