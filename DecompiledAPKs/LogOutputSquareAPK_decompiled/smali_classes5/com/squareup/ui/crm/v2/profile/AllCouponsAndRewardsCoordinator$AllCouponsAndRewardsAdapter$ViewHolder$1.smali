.class Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "AllCouponsAndRewardsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;-><init>(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;Lcom/squareup/ui/account/view/SmartLineRow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;

.field final synthetic val$this$1:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;)V
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder$1;->this$2:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder$1;->val$this$1:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 118
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder$1;->this$2:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->access$000(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)Lcom/squareup/checkout/HoldsCoupons;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder$1;->this$2:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->access$100(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;)Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/checkout/HoldsCoupons;->isCouponAddedToCart(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 119
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder$1;->this$2:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->access$000(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)Lcom/squareup/checkout/HoldsCoupons;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder$1;->this$2:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->access$100(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;)Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/checkout/HoldsCoupons;->remove(Lcom/squareup/protos/client/coupons/Coupon;)V

    goto :goto_0

    .line 121
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder$1;->this$2:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->access$000(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)Lcom/squareup/checkout/HoldsCoupons;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder$1;->this$2:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->access$100(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;)Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/checkout/HoldsCoupons;->apply(Lcom/squareup/protos/client/coupons/Coupon;)V

    :goto_0
    return-void
.end method
