.class final Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RewardRecyclerViewHelper.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->invoke(ILjava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "newCount",
        "",
        "invoke",
        "com/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$createRecyclerFor$1$1$1$1$2",
        "com/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$bind$1$lambda$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $quantityApplied:I

.field final synthetic $row:Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;


# direct methods
.method constructor <init>(ILcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;)V
    .locals 0

    iput p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$2;->$quantityApplied:I

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$2;->$row:Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$2;->invoke(I)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(I)V
    .locals 1

    .line 94
    iget v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$2;->$quantityApplied:I

    if-ge p1, v0, :cond_0

    .line 95
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$2;->$row:Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;->getOnRewardRowRemoved()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$2;->$row:Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 97
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$2;->$row:Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;->getOnRewardRowRedeemed()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$2;->$row:Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method
