.class public Lcom/squareup/ui/crm/v2/profile/NotesSectionView$ViewData;
.super Ljava/lang/Object;
.source "NotesSectionView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/NotesSectionView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewData"
.end annotation


# instance fields
.field public final notes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Note;",
            ">;"
        }
    .end annotation
.end field

.field public final showAll:Z

.field public final showReminders:Z


# direct methods
.method public constructor <init>(ZZLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Note;",
            ">;)V"
        }
    .end annotation

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-boolean p1, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView$ViewData;->showAll:Z

    .line 94
    iput-boolean p2, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView$ViewData;->showReminders:Z

    .line 95
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView$ViewData;->notes:Ljava/util/List;

    return-void
.end method
