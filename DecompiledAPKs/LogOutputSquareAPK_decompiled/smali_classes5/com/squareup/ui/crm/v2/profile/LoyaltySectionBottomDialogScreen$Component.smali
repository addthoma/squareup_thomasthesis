.class public interface abstract Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Component;
.super Ljava/lang/Object;
.source "LoyaltySectionBottomDialogScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract pointsFormatter()Lcom/squareup/loyalty/PointsTermsFormatter;
.end method

.method public abstract runner()Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Runner;
.end method
