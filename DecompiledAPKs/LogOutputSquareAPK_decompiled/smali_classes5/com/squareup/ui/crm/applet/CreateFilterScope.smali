.class public Lcom/squareup/ui/crm/applet/CreateFilterScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "CreateFilterScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/applet/CreateFilterScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/applet/CreateFilterScope$Component;,
        Lcom/squareup/ui/crm/applet/CreateFilterScope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/applet/CreateFilterScope;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 58
    sget-object v0, Lcom/squareup/ui/crm/applet/-$$Lambda$CreateFilterScope$mvhPcmuJ9b3UVc-LnwAxpkvzSGY;->INSTANCE:Lcom/squareup/ui/crm/applet/-$$Lambda$CreateFilterScope$mvhPcmuJ9b3UVc-LnwAxpkvzSGY;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/applet/CreateFilterScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/crm/applet/CreateFilterScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/applet/CreateFilterScope;
    .locals 1

    .line 59
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 60
    new-instance v0, Lcom/squareup/ui/crm/applet/CreateFilterScope;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/applet/CreateFilterScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 54
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CreateFilterScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CreateFilterScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 49
    const-class v0, Lcom/squareup/ui/crm/applet/CreateFilterScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/applet/CreateFilterScope$Component;

    .line 50
    invoke-interface {v0}, Lcom/squareup/ui/crm/applet/CreateFilterScope$Component;->filterTemplateLoader()Lcom/squareup/crm/FilterTemplateLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
