.class public Lcom/squareup/ui/crm/applet/UpdateGroup2Scope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "UpdateGroup2Scope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/applet/UpdateGroup2Scope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/applet/UpdateGroup2Scope$Component;,
        Lcom/squareup/ui/crm/applet/UpdateGroup2Scope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/applet/UpdateGroup2Scope;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 71
    sget-object v0, Lcom/squareup/ui/crm/applet/-$$Lambda$UpdateGroup2Scope$u4HXxDZyQoe5y4D5AQwbsMG7nZo;->INSTANCE:Lcom/squareup/ui/crm/applet/-$$Lambda$UpdateGroup2Scope$u4HXxDZyQoe5y4D5AQwbsMG7nZo;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/applet/UpdateGroup2Scope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 58
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/squareup/ui/crm/applet/UpdateGroup2Scope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/applet/UpdateGroup2Scope;
    .locals 1

    .line 72
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 73
    new-instance v0, Lcom/squareup/ui/crm/applet/UpdateGroup2Scope;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/applet/UpdateGroup2Scope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 67
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/UpdateGroup2Scope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/UpdateGroup2Scope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 2

    .line 27
    const-class v0, Lcom/squareup/ui/crm/applet/UpdateGroup2Scope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/applet/UpdateGroup2Scope$Component;

    .line 29
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object p1

    .line 30
    invoke-interface {v0}, Lcom/squareup/ui/crm/applet/UpdateGroup2Scope$Component;->updateGroup2Flow()Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 31
    invoke-interface {v0}, Lcom/squareup/ui/crm/applet/UpdateGroup2Scope$Component;->createFilterFlow()Lcom/squareup/ui/crm/flow/CreateFilterFlow;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 32
    invoke-interface {v0}, Lcom/squareup/ui/crm/applet/UpdateGroup2Scope$Component;->updateFilterFlow()Lcom/squareup/ui/crm/flow/UpdateFilterFlow;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    return-void
.end method
