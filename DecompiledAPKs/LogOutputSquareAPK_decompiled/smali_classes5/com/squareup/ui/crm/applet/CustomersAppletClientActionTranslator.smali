.class public final Lcom/squareup/ui/crm/applet/CustomersAppletClientActionTranslator;
.super Ljava/lang/Object;
.source "CustomersAppletClientActionTranslator.kt"

# interfaces
.implements Lcom/squareup/clientactiontranslation/ClientActionTranslator;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0001\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/crm/applet/CustomersAppletClientActionTranslator;",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslator;",
        "()V",
        "translate",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;",
        "clientAction",
        "Lcom/squareup/protos/client/ClientAction;",
        "crm-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public translate(Lcom/squareup/protos/client/ClientAction;)Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;
    .locals 3

    const-string v0, "clientAction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    const-string v0, "square-register://customers"

    invoke-direct {p1, v0}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    goto :goto_0

    .line 13
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    const-string v1, "square-register://customers/navigate?navigationID=feedback"

    if-eqz v0, :cond_1

    new-instance p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    invoke-direct {p1, v1}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    goto :goto_0

    .line 16
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    .line 17
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&conversationToken="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    iget-object p1, p1, Lcom/squareup/protos/client/ClientAction;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    iget-object p1, p1, Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;->conversation_token:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 16
    invoke-direct {v0, p1}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;-><init>(Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    goto :goto_0

    .line 20
    :cond_2
    sget-object p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Unhandled;->INSTANCE:Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Unhandled;

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    :goto_0
    return-object p1
.end method
