.class public Lcom/squareup/ui/crm/applet/ConversationDetailScope;
.super Lcom/squareup/ui/crm/applet/InCustomersAppletScope;
.source "ConversationDetailScope.java"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/applet/ConversationDetailScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/applet/ConversationDetailScope$Component;,
        Lcom/squareup/ui/crm/applet/ConversationDetailScope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/applet/ConversationDetailScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/crm/applet/ConversationDetailScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/ui/crm/applet/ConversationDetailScope;

    invoke-direct {v0}, Lcom/squareup/ui/crm/applet/ConversationDetailScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/applet/ConversationDetailScope;->INSTANCE:Lcom/squareup/ui/crm/applet/ConversationDetailScope;

    .line 83
    sget-object v0, Lcom/squareup/ui/crm/applet/ConversationDetailScope;->INSTANCE:Lcom/squareup/ui/crm/applet/ConversationDetailScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/applet/ConversationDetailScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/ui/crm/applet/InCustomersAppletScope;-><init>()V

    return-void
.end method
