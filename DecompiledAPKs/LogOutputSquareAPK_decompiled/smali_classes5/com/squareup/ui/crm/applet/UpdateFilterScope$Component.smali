.class public interface abstract Lcom/squareup/ui/crm/applet/UpdateFilterScope$Component;
.super Ljava/lang/Object;
.source "UpdateFilterScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/crm/applet/UpdateFilterScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/UpdateFilterScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract editFilterCardScreen()Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;
.end method
