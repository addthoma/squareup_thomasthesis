.class public Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;
.super Ljava/lang/Object;
.source "CustomersAppletScopeRunner.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;
.implements Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;
.implements Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$Runner;
.implements Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;
.implements Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator$Runner;
.implements Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;
.implements Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;
.implements Lcom/squareup/ui/crm/cards/AddCouponScreen$Runner;


# static fields
.field private static final CONTACT_SEARCH_DELAY_MS:J = 0xc8L

.field private static final MERGE_PROPOSAL_PAGE_SIZE:I = 0x7

.field private static final NO_FILTERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field private static final NO_GROUP:Lcom/squareup/protos/client/rolodex/Group;


# instance fields
.field private final appliedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;>;"
        }
    .end annotation
.end field

.field private final chooseFiltersFlow:Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;

.field private final contactLoader:Lcom/squareup/crm/RolodexContactLoader;

.field private final conversationLoader:Lcom/squareup/crm/ConversationLoader;

.field private final createManualGroupOnError:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private currentScrollPositionAll:I

.field private currentScrollPositionGroup:I

.field private final customerConversationTokenHolder:Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;

.field private final customersApplet:Lcom/squareup/ui/crm/applet/CustomersApplet;

.field private final device:Lcom/squareup/util/Device;

.field private final flow:Lflow/Flow;

.field private final groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation
.end field

.field private final groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

.field private isFirstManualGroup:Z

.field private loadedContactMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private final mergeProposalLoader:Lcom/squareup/crm/MergeProposalLoader;

.field private multiSelectContactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

.field private final multiSelectMode:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/crm/v2/MultiSelectMode;",
            ">;"
        }
    .end annotation
.end field

.field private final multiSelectedCustomerCount:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final resolveDuplicatesFlow:Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;

.field private final rightPaneData:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final rightPaneDataRenderer:Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private final selectCustomersFlow:Lcom/squareup/ui/crm/flow/SelectCustomersFlow;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionDiscountAdapter:Lcom/squareup/payment/TransactionDiscountAdapter;

.field private final updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

.field private final updateGroup2Flow:Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;

.field private final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 109
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->NO_FILTERS:Ljava/util/List;

    .line 110
    new-instance v0, Lcom/squareup/protos/client/rolodex/Group$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Group$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Group$Builder;->build()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->NO_GROUP:Lcom/squareup/protos/client/rolodex/Group;

    return-void
.end method

.method public constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;Lcom/squareup/ui/crm/flow/SelectCustomersFlow;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/ConversationLoader;Lcom/squareup/crm/MergeProposalLoader;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/ui/crm/applet/CustomersApplet;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;Lcom/squareup/util/Device;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    move-object v1, p10

    move-object/from16 v2, p12

    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->rightPaneData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 139
    sget-object v3, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->NO_FILTERS:Ljava/util/List;

    invoke-static {v3}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->appliedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 140
    sget-object v3, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->NO_GROUP:Lcom/squareup/protos/client/rolodex/Group;

    invoke-static {v3}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 141
    sget-object v3, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-static {v3}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v3, 0x0

    .line 142
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v4

    iput-object v4, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectedCustomerCount:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 143
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v4

    iput-object v4, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->createManualGroupOnError:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v4, 0x0

    .line 144
    iput-object v4, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectContactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

    .line 145
    iput-object v4, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->loadedContactMap:Ljava/util/Map;

    .line 147
    iput v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->currentScrollPositionAll:I

    .line 153
    iput v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->currentScrollPositionGroup:I

    .line 154
    iput-boolean v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->isFirstManualGroup:Z

    move-object v3, p1

    .line 178
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    move-object v3, p2

    .line 179
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->res:Lcom/squareup/util/Res;

    move-object v3, p3

    .line 180
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    move-object v3, p4

    .line 181
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->transactionDiscountAdapter:Lcom/squareup/payment/TransactionDiscountAdapter;

    move-object v3, p5

    .line 182
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->updateGroup2Flow:Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;

    move-object v3, p6

    .line 183
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->resolveDuplicatesFlow:Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;

    move-object v3, p7

    .line 184
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->chooseFiltersFlow:Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;

    move-object v3, p8

    .line 185
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->selectCustomersFlow:Lcom/squareup/ui/crm/flow/SelectCustomersFlow;

    move-object v3, p9

    .line 186
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    .line 187
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    move-object/from16 v3, p11

    .line 188
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->conversationLoader:Lcom/squareup/crm/ConversationLoader;

    .line 189
    iput-object v2, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->mergeProposalLoader:Lcom/squareup/crm/MergeProposalLoader;

    move-object/from16 v3, p13

    .line 190
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    move-object/from16 v3, p14

    .line 191
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->customersApplet:Lcom/squareup/ui/crm/applet/CustomersApplet;

    move-object/from16 v3, p15

    .line 192
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-object/from16 v3, p16

    .line 193
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->customerConversationTokenHolder:Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;

    move-object/from16 v3, p17

    .line 194
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->device:Lcom/squareup/util/Device;

    move-object/from16 v3, p18

    .line 195
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    move-object/from16 v3, p19

    .line 196
    iput-object v3, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->rightPaneDataRenderer:Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;

    const-wide/16 v3, 0xc8

    .line 198
    invoke-virtual {p10, v3, v4}, Lcom/squareup/crm/RolodexContactLoader;->setSearchDelayMs(J)V

    const/4 v1, 0x7

    .line 200
    invoke-virtual {v2, v1}, Lcom/squareup/crm/MergeProposalLoader;->setDefaultPageSize(I)V

    return-void
.end method

.method private closeDetailScreen()V
    .locals 6

    .line 596
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    goto :goto_0

    .line 601
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    sget-object v2, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/squareup/ui/crm/v2/CustomersAppletDetailScreen;

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lflow/Direction;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method static synthetic lambda$null$0(Lflow/History;)Lcom/squareup/container/Command;
    .locals 3

    .line 238
    invoke-virtual {p0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lflow/History$Builder;->clear()Lflow/History$Builder;

    move-result-object v0

    .line 239
    invoke-virtual {p0}, Lflow/History;->framesFromBottom()Ljava/lang/Iterable;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 240
    instance-of v2, v1, Lcom/squareup/ui/crm/v2/CustomersAppletDetailScreen;

    if-eqz v2, :cond_0

    .line 241
    sget-object v1, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;

    invoke-virtual {v0, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    goto :goto_0

    .line 243
    :cond_0
    invoke-virtual {v0, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    goto :goto_0

    .line 246
    :cond_1
    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p0, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$4(Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;Lflow/History;)Lcom/squareup/container/Command;
    .locals 0

    .line 291
    iget-object p0, p0, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;->backOut:Lrx/functions/Func1;

    invoke-interface {p0, p1}, Lrx/functions/Func1;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lflow/History;

    invoke-virtual {p0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p0

    .line 293
    :goto_0
    invoke-virtual {p0}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen;

    if-nez p1, :cond_0

    .line 294
    invoke-virtual {p0}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    goto :goto_0

    .line 296
    :cond_0
    invoke-virtual {p0}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    .line 297
    invoke-virtual {p0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    sget-object p1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {p0, p1}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$10(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 331
    invoke-virtual {p0}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getUpdateCustomerResultKey()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    move-result-object p0

    sget-object v0, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->CUSTOMERS_APPLET:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$onEnterScope$11(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 332
    invoke-virtual {p0}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getType()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    move-result-object p0

    sget-object v0, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->CREATE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$onEnterScope$6(Lcom/squareup/ui/crm/v2/MultiSelectMode;)Ljava/lang/Boolean;
    .locals 1

    .line 312
    sget-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    if-eq p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$7(Lcom/squareup/protos/client/rolodex/Group;)Ljava/lang/Boolean;
    .locals 1

    .line 317
    sget-object v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->NO_GROUP:Lcom/squareup/protos/client/rolodex/Group;

    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/rolodex/Group;->equals(Ljava/lang/Object;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$9(Ljava/util/List;)Ljava/lang/Boolean;
    .locals 0

    .line 324
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$showContactDetail$13(Lflow/History$Builder;)Lflow/History$Builder;
    .locals 0

    return-object p0
.end method

.method private onViewCustomerDetailScreenBack()V
    .locals 2

    .line 585
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    if-eq v0, v1, :cond_0

    .line 586
    sget-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->setMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V

    goto :goto_0

    .line 589
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->closeDetailScreen()V

    :goto_0
    return-void
.end method

.method private showContactDetail(Lcom/squareup/protos/client/rolodex/Contact;Lkotlin/jvm/functions/Function1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Lkotlin/jvm/functions/Function1<",
            "Lflow/History$Builder;",
            "Lflow/History$Builder;",
            ">;)V"
        }
    .end annotation

    .line 401
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->device:Lcom/squareup/util/Device;

    .line 403
    invoke-interface {v1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lflow/Direction;->FORWARD:Lflow/Direction;

    goto :goto_0

    :cond_0
    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    :goto_0
    const/4 v2, 0x2

    new-array v2, v2, [Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 p2, 0x1

    new-instance v3, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$0xd9iW2bM18sURygO0gV4WuoyB8;

    invoke-direct {v3, p0, p1}, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$0xd9iW2bM18sURygO0gV4WuoyB8;-><init>(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;Lcom/squareup/protos/client/rolodex/Contact;)V

    aput-object v3, v2, p2

    .line 401
    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->editHistory(Lflow/Flow;Lflow/Direction;[Lkotlin/jvm/functions/Function1;)V

    return-void
.end method


# virtual methods
.method public allCustomersListHardwareBackButton()V
    .locals 2

    .line 621
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    if-eq v0, v1, :cond_0

    .line 622
    sget-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->setMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V

    goto :goto_0

    .line 624
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    :goto_0
    return-void
.end method

.method public appliedFilters()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;>;"
        }
    .end annotation

    .line 503
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->appliedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public cancelCreateManualGroup()V
    .locals 2

    .line 541
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/v2/CreateManualGroupScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeAddCouponScreen(Lcom/squareup/api/items/Discount;)V
    .locals 1

    .line 687
    iget-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/ui/crm/cards/AddCouponScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeConversationDetailScreen()V
    .locals 6

    .line 665
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->customerConversationTokenHolder:Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;->setConversationToken(Ljava/lang/String;)V

    .line 667
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 669
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    goto :goto_0

    .line 672
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    sget-object v2, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/squareup/ui/crm/v2/CustomersAppletDetailScreen;

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lflow/Direction;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method public closeMessageListScreen()V
    .locals 2

    .line 648
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->customerConversationTokenHolder:Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;->setConversationToken(Ljava/lang/String;)V

    .line 651
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public closeViewGroupMasterScreen()V
    .locals 3

    .line 524
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->NO_GROUP:Lcom/squareup/protos/client/rolodex/Group;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    const/4 v0, 0x0

    .line 525
    iput v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->currentScrollPositionGroup:I

    .line 530
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$dm8x3xn5Qhwgo3gczsuUO4QFDCw;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$dm8x3xn5Qhwgo3gczsuUO4QFDCw;-><init>(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;)V

    invoke-direct {v1, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public closeViewGroupsListScreen()V
    .locals 1

    .line 483
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public commitMultiSelectAction()V
    .locals 7

    .line 439
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectContactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->loadedContactMap:Ljava/util/Map;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 441
    iget-object v1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->selectCustomersFlow:Lcom/squareup/ui/crm/flow/SelectCustomersFlow;

    sget-object v2, Lcom/squareup/ui/crm/applet/SelectCustomersScope;->INSTANCE:Lcom/squareup/ui/crm/applet/SelectCustomersScope;

    sget-object v3, Lcom/squareup/ui/crm/applet/SelectCustomersScope;->OTHER_INSTANCE:Lcom/squareup/ui/crm/applet/SelectCustomersScope;

    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 442
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/crm/v2/MultiSelectMode;

    iget-object v5, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectContactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

    iget-object v6, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->loadedContactMap:Ljava/util/Map;

    .line 441
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->applyMultiSelectActionAndRedirect(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/v2/MultiSelectMode;Lcom/squareup/protos/client/rolodex/ContactSet;Ljava/util/Map;)V

    return-void
.end method

.method public deleteIndividualFilter(Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 2

    .line 477
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->appliedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 478
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 479
    iget-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->appliedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public editGroup()V
    .locals 3

    .line 516
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Group;

    sget-object v1, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->NO_GROUP:Lcom/squareup/protos/client/rolodex/Group;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Group;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 518
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->updateGroup2Flow:Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;

    new-instance v1, Lcom/squareup/ui/crm/applet/UpdateGroup2Scope;

    sget-object v2, Lcom/squareup/ui/crm/applet/CustomersAppletScope;->INSTANCE:Lcom/squareup/ui/crm/applet/CustomersAppletScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/applet/UpdateGroup2Scope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    iget-object v2, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 519
    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Group;

    .line 518
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Group;)V

    return-void
.end method

.method public getAppliedFilters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 499
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->appliedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getGroupForViewGroupScreen()Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation

    .line 507
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public getGroupTokenForViewGroupScreen()Ljava/lang/String;
    .locals 1

    .line 511
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Group;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    return-object v0
.end method

.method public getInitialScrollPosition()I
    .locals 2

    .line 460
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Group;

    sget-object v1, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->NO_GROUP:Lcom/squareup/protos/client/rolodex/Group;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Group;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->currentScrollPositionAll:I

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->currentScrollPositionGroup:I

    :goto_0
    return v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 348
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMultiSelectMode()Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/crm/v2/MultiSelectMode;",
            ">;"
        }
    .end annotation

    .line 456
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public bridge synthetic getMultiSelectMode()Lrx/Observable;
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->getMultiSelectMode()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    return-object v0
.end method

.method public gotoCreateManualGroupScreen(Z)V
    .locals 1

    .line 494
    iput-boolean p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->isFirstManualGroup:Z

    .line 495
    iget-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/crm/v2/CreateManualGroupScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/CreateManualGroupScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public isFirstGroup()Z
    .locals 1

    .line 575
    iget-boolean v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->isFirstManualGroup:Z

    return v0
.end method

.method public synthetic lambda$closeViewGroupMasterScreen$15$CustomersAppletScopeRunner(Lflow/History;)Lcom/squareup/container/Command;
    .locals 3

    .line 532
    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    .line 534
    const-class v1, Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-class v1, Lcom/squareup/ui/crm/v2/CustomersAppletDetailScreen;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lcom/squareup/container/Histories;->popWhile(Lflow/History$Builder;[Ljava/lang/Class;)Lflow/History$Builder;

    .line 535
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->device:Lcom/squareup/util/Device;

    .line 536
    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lflow/Direction;->BACKWARD:Lflow/Direction;

    goto :goto_0

    :cond_0
    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    .line 535
    :goto_0
    invoke-static {p1, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$16$CustomersAppletScopeRunner(Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 554
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->selectCustomersFlow:Lcom/squareup/ui/crm/flow/SelectCustomersFlow;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;->group:Lcom/squareup/protos/client/rolodex/Group;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->setGroupToAddTo(Lcom/squareup/protos/client/rolodex/Group;)V

    .line 555
    sget-object p1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->ADD_TO_NEWLY_CREATED_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->setMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V

    .line 556
    iget-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 557
    iget-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/squareup/ui/crm/v2/CreateManualGroupScreen;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/squareup/ui/crm/v2/ViewGroupsListScreen;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    .line 559
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->cancelCreateManualGroup()V

    .line 560
    invoke-virtual {p0}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->closeViewGroupsListScreen()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$17$CustomersAppletScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 563
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;

    if-eqz p1, :cond_0

    .line 564
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p2, p2, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 565
    iget-object p2, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->createManualGroupOnError:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 567
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->createManualGroupOnError:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object p2, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmscreens/R$string;->crm_group_saving_error:I

    .line 568
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 567
    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$2$CustomersAppletScopeRunner(Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;Lflow/History;)Lcom/squareup/container/Command;
    .locals 2

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p1, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;->group:Lcom/squareup/protos/client/rolodex/GroupV2;

    invoke-static {v1}, Lcom/squareup/crm/util/RolodexGroupHelper;->toGroup(Lcom/squareup/protos/client/rolodex/GroupV2;)Lcom/squareup/protos/client/rolodex/Group;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 267
    iget-object p1, p1, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;->backOut:Lrx/functions/Func1;

    invoke-interface {p1, p2}, Lrx/functions/Func1;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/History;

    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    .line 269
    sget-object p2, Lcom/squareup/ui/crm/v2/ViewGroupsListScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/ViewGroupsListScreen;

    invoke-virtual {p1, p2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 274
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    sget-object p2, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p1, p2}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$1$CustomersAppletScopeRunner(Lkotlin/Unit;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 221
    iget-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->ADD_TO_NEWLY_CREATED_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    const/4 v1, 0x0

    if-ne p1, v0, :cond_0

    .line 222
    iput-object v1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectContactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

    .line 223
    iget-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 224
    invoke-virtual {p0}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->viewGroupsList()V

    return-void

    .line 227
    :cond_0
    iput-object v1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectContactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

    .line 228
    iget-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 230
    iget-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    if-nez p1, :cond_1

    .line 237
    iget-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/container/CalculatedKey;

    sget-object v1, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$7YTWxcKoK4ur9PPEv9q-GQ0n_UE;->INSTANCE:Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$7YTWxcKoK4ur9PPEv9q-GQ0n_UE;

    invoke-direct {v0, v1}, Lcom/squareup/container/CalculatedKey;-><init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public synthetic lambda$onEnterScope$12$CustomersAppletScopeRunner(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 334
    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 335
    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getHistoryFunc()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->showContactDetail(Lcom/squareup/protos/client/rolodex/Contact;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    const/4 v2, 0x1

    new-array v2, v2, [Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getHistoryFunc()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->editHistory(Lflow/Flow;Lflow/Direction;[Lkotlin/jvm/functions/Function1;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$3$CustomersAppletScopeRunner(Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;)V
    .locals 4

    .line 254
    iget-object v0, p1, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;->filters:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->appliedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object p1, p1, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;->filters:Ljava/util/List;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_1

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->appliedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->NO_FILTERS:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$Z90FR8cr_2EtYJJhUxaw3-OAZu8;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$Z90FR8cr_2EtYJJhUxaw3-OAZu8;-><init>(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;)V

    const-string v3, "chooseFiltersFlow.onResult"

    invoke-direct {v1, v3, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 276
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen;

    iget-object p1, p1, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow$Result;->group:Lcom/squareup/protos/client/rolodex/GroupV2;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/GroupV2;->type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    sget-object v2, Lcom/squareup/protos/client/rolodex/GroupV2$Type;->SMART:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    if-ne p1, v2, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen;-><init>(Z)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method public synthetic lambda$onEnterScope$5$CustomersAppletScopeRunner(Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;)V
    .locals 3

    .line 284
    iget-object v0, p1, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;->newGroup:Lcom/squareup/protos/client/rolodex/Group;

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object p1, p1, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;->newGroup:Lcom/squareup/protos/client/rolodex/Group;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 286
    :cond_0
    iget-object v0, p1, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;->backOut:Lrx/functions/Func1;

    if-eqz v0, :cond_1

    .line 289
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$8v1pVbUHkq6Z6DC1e5i4U7-hP38;

    invoke-direct {v2, p1}, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$8v1pVbUHkq6Z6DC1e5i4U7-hP38;-><init>(Lcom/squareup/ui/crm/flow/UpdateGroup2Flow$Result;)V

    const-string/jumbo p1, "updateGroup2Flow.onResult"

    invoke-direct {v1, p1, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 300
    iget-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->NO_GROUP:Lcom/squareup/protos/client/rolodex/Group;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$8$CustomersAppletScopeRunner(Lcom/squareup/protos/client/rolodex/Group;)Ljava/lang/Integer;
    .locals 1

    .line 320
    sget-object v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->NO_GROUP:Lcom/squareup/protos/client/rolodex/Group;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/rolodex/Group;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 321
    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Group;

    invoke-static {p1}, Lcom/squareup/crm/util/RolodexGroupHelper;->getGroupMembershipCount(Lcom/squareup/protos/client/rolodex/Group;)I

    move-result p1

    .line 320
    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$saveManualGroup$18$CustomersAppletScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 552
    new-instance v0, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$9aYQZLiKKL_gn08_MjsL_gPyf4M;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$9aYQZLiKKL_gn08_MjsL_gPyf4M;-><init>(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;)V

    new-instance v1, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$05Tz2sntaUGXDfx8oVKPfgb5uJ0;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$05Tz2sntaUGXDfx8oVKPfgb5uJ0;-><init>(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$showContactDetail$14$CustomersAppletScopeRunner(Lcom/squareup/protos/client/rolodex/Contact;Lflow/History$Builder;)Lflow/History$Builder;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    .line 409
    const-class v1, Lcom/squareup/ui/crm/v2/CustomersAppletDetailScreen;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p2, v0}, Lcom/squareup/container/Histories;->popWhile(Lflow/History$Builder;[Ljava/lang/Class;)Lflow/History$Builder;

    .line 410
    sget-object v0, Lcom/squareup/ui/crm/applet/CustomersAppletScope;->INSTANCE:Lcom/squareup/ui/crm/applet/CustomersAppletScope;

    iget-object v1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v2, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->transactionDiscountAdapter:Lcom/squareup/payment/TransactionDiscountAdapter;

    invoke-static {v0, p1, v1, v2}, Lcom/squareup/ui/crm/flow/CrmScope;->newViewCustomerDetailScreenInAppletV2(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/Transaction;Lcom/squareup/checkout/HoldsCoupons;)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object p1

    invoke-virtual {p2, p1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    return-object p2
.end method

.method public noCustomerSelectedScreenData()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;",
            ">;"
        }
    .end annotation

    .line 640
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->rightPaneData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public onCreateGroupError()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 579
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->createManualGroupOnError:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 8

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->conversationLoader:Lcom/squareup/crm/ConversationLoader;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->mergeProposalLoader:Lcom/squareup/crm/MergeProposalLoader;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 209
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    .line 210
    iget-object v1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->resolveDuplicatesFlow:Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 211
    iget-object v1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->selectCustomersFlow:Lcom/squareup/ui/crm/flow/SelectCustomersFlow;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->maybeRegister(Lmortar/MortarScope;Lmortar/Scoped;)V

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/squareup/crm/RolodexGroupLoader;->setIncludeCounts(Z)V

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexGroupLoader;->refresh()V

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->selectCustomersFlow:Lcom/squareup/ui/crm/flow/SelectCustomersFlow;

    .line 219
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$fWpbOlOoDFKbJ1CTzc7pqCJhkqM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$fWpbOlOoDFKbJ1CTzc7pqCJhkqM;-><init>(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;)V

    .line 220
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 218
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->chooseFiltersFlow:Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;

    .line 252
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->onResult()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$NdcOhSL-1hBxfAXSlR0L-lNtlZk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$NdcOhSL-1hBxfAXSlR0L-lNtlZk;-><init>(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;)V

    .line 253
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 251
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 280
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->updateGroup2Flow:Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;

    .line 281
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;->onResult()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$6Norta5OS49Ltm1-bTOC77Y1MSs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$6Norta5OS49Ltm1-bTOC77Y1MSs;-><init>(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;)V

    .line 282
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 280
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 306
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    .line 308
    invoke-interface {v0}, Lcom/squareup/crm/RolodexGroupLoader;->allCustomersCount()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    const/4 v1, -0x1

    .line 309
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    .line 310
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v2, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$AbDxiFKAGG7c9jlW79FHoNoIxaM;->INSTANCE:Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$AbDxiFKAGG7c9jlW79FHoNoIxaM;

    .line 312
    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 313
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectedCustomerCount:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 315
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v4, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$lcMOQKzlnoshT0t6u3KOAcaNS5k;->INSTANCE:Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$lcMOQKzlnoshT0t6u3KOAcaNS5k;

    .line 317
    invoke-virtual {v0, v4}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 318
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v5, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$tqx4Gor4xVBAyqmLrhLtSAhIyNQ;

    invoke-direct {v5, p0}, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$tqx4Gor4xVBAyqmLrhLtSAhIyNQ;-><init>(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;)V

    .line 320
    invoke-virtual {v0, v5}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 322
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v5

    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->appliedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v6, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$_NuAJI5Pgc3AU94vcBCUyiLNY-I;->INSTANCE:Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$_NuAJI5Pgc3AU94vcBCUyiLNY-I;

    .line 324
    invoke-virtual {v0, v6}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 325
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v6

    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->rightPaneDataRenderer:Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v7, Lcom/squareup/ui/crm/applet/-$$Lambda$l4fq4To0xkmNiI3ytZHy7IS2K-E;

    invoke-direct {v7, v0}, Lcom/squareup/ui/crm/applet/-$$Lambda$l4fq4To0xkmNiI3ytZHy7IS2K-E;-><init>(Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;)V

    .line 307
    invoke-static/range {v1 .. v7}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func6;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->rightPaneData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 328
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 306
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 330
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    invoke-interface {v0}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;->results()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$_gT-JaageNMsdZMKjPey4wAEvzQ;->INSTANCE:Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$_gT-JaageNMsdZMKjPey4wAEvzQ;

    .line 331
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$pwX3_lZAt3QQHZYveW65kmtBh3A;->INSTANCE:Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$pwX3_lZAt3QQHZYveW65kmtBh3A;

    .line 332
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$R8Vgn3YelhvBI_dGvzznVOfVvWA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$R8Vgn3YelhvBI_dGvzznVOfVvWA;-><init>(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;)V

    .line 333
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 330
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 342
    iget-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->customersApplet:Lcom/squareup/ui/crm/applet/CustomersApplet;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/applet/CustomersApplet;->select()V

    .line 344
    iget-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->enteringCustomersApplet()Z

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 377
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->exitingCustomersApplet()Z

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->appliedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v2, "appliedFilters"

    invoke-static {v1, p1, v2}, Lcom/squareup/util/Protos;->loadProtos(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Group;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v2, "groupForViewGroupScreen"

    .line 355
    invoke-static {v1, p1, v2}, Lcom/squareup/util/Protos;->loadProto(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/wire/Message;

    move-result-object v1

    .line 354
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    const-string v0, "currentScrollPositionAll"

    .line 356
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->currentScrollPositionAll:I

    const-string v0, "currentScrollPositionGroup"

    .line 357
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->currentScrollPositionGroup:I

    .line 358
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectedCustomerCount:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "multiSelectedCustomerCount"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 359
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "multiSelectMode"

    .line 360
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 359
    invoke-static {p1}, Lcom/squareup/ui/crm/v2/MultiSelectMode;->valueOf(Ljava/lang/String;)Lcom/squareup/ui/crm/v2/MultiSelectMode;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onNoCustomerHardwareBackPressed()V
    .locals 2

    .line 608
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    if-eq v0, v1, :cond_0

    .line 609
    sget-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->setMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V

    goto :goto_0

    .line 614
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    :goto_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 364
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->appliedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Ljava/util/List;)[B

    move-result-object v0

    const-string v1, "appliedFilters"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 365
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 366
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Lcom/squareup/wire/Message;)[B

    move-result-object v0

    const-string v1, "groupForViewGroupScreen"

    .line 365
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 367
    iget v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->currentScrollPositionAll:I

    const-string v1, "currentScrollPositionAll"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 368
    iget v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->currentScrollPositionGroup:I

    const-string v1, "currentScrollPositionGroup"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 369
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectedCustomerCount:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v1, "multiSelectedCustomerCount"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 370
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/MultiSelectMode;->name()Ljava/lang/String;

    move-result-object v0

    const-string v1, "multiSelectMode"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public saveManualGroup(Ljava/lang/String;)V
    .locals 2

    .line 546
    new-instance v0, Lcom/squareup/protos/client/rolodex/Group$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Group$Builder;-><init>()V

    sget-object v1, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    .line 547
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Group$Builder;->group_type(Lcom/squareup/protos/client/rolodex/GroupType;)Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object v0

    .line 548
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/Group$Builder;->display_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object p1

    .line 549
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Group$Builder;->build()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object p1

    .line 551
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/squareup/crm/RolodexServiceHelper;->upsertManualGroup(Lcom/squareup/protos/client/rolodex/Group;Ljava/util/UUID;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$RqmEjDkPuG6Jk_0xwTzs-Do1hI8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$RqmEjDkPuG6Jk_0xwTzs-Do1hI8;-><init>(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;)V

    .line 552
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public saveScrollPosition(I)V
    .locals 2

    .line 465
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Group;

    sget-object v1, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->NO_GROUP:Lcom/squareup/protos/client/rolodex/Group;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Group;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    iput p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->currentScrollPositionAll:I

    goto :goto_0

    .line 468
    :cond_0
    iput p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->currentScrollPositionGroup:I

    :goto_0
    return-void
.end method

.method public setMultiSelectContactSet(Lkotlin/Pair;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/rolodex/ContactSet;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;)V"
        }
    .end annotation

    .line 447
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/ContactSet;

    iput-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectContactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

    .line 448
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->loadedContactMap:Ljava/util/Map;

    return-void
.end method

.method public setMultiSelectCount(I)V
    .locals 1

    .line 452
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectedCustomerCount:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V
    .locals 1

    .line 430
    sget-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    .line 431
    iput-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectContactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

    .line 432
    iput-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->loadedContactMap:Ljava/util/Map;

    .line 434
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public showAddCouponToConversation()V
    .locals 3

    .line 678
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/AddCouponScreen;

    sget-object v2, Lcom/squareup/ui/crm/applet/ConversationDetailScope;->INSTANCE:Lcom/squareup/ui/crm/applet/ConversationDetailScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/AddCouponScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showChooseFiltersScreen()V
    .locals 3

    .line 473
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->chooseFiltersFlow:Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;

    sget-object v1, Lcom/squareup/ui/crm/applet/ChooseFiltersScope;->INSTANCE:Lcom/squareup/ui/crm/applet/ChooseFiltersScope;

    iget-object v2, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->appliedFilters:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;->showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/util/List;)V

    return-void
.end method

.method public showContactDetail(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 396
    sget-object v0, Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$1_lHiDvbzJ8Z4YXFRzSVs9Do3io;->INSTANCE:Lcom/squareup/ui/crm/applet/-$$Lambda$CustomersAppletScopeRunner$1_lHiDvbzJ8Z4YXFRzSVs9Do3io;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->showContactDetail(Lcom/squareup/protos/client/rolodex/Contact;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public showContactFromConversation(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 4

    .line 682
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/crm/applet/CustomersAppletScope;->INSTANCE:Lcom/squareup/ui/crm/applet/CustomersAppletScope;

    iget-object v2, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v3, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->transactionDiscountAdapter:Lcom/squareup/payment/TransactionDiscountAdapter;

    invoke-static {v1, p1, v2, v3}, Lcom/squareup/ui/crm/flow/CrmScope;->newViewCustomerCardScreenInApplet(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/Transaction;Lcom/squareup/checkout/HoldsCoupons;)Lcom/squareup/ui/crm/flow/InCrmScope;

    move-result-object p1

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showConversation(Ljava/lang/String;)V
    .locals 5

    .line 655
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->customerConversationTokenHolder:Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;->setConversationToken(Ljava/lang/String;)V

    .line 656
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->device:Lcom/squareup/util/Device;

    .line 658
    invoke-interface {v1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lflow/Direction;->FORWARD:Lflow/Direction;

    goto :goto_0

    :cond_0
    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    :goto_0
    new-instance v2, Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2;

    invoke-direct {v2, p1}, Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Lcom/squareup/ui/crm/v2/CustomersAppletDetailScreen;

    aput-object v4, p1, v3

    .line 656
    invoke-static {v0, v1, v2, p1}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lflow/Direction;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method

.method public showCreateCustomerScreen(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 8

    .line 385
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    sget-object v2, Lcom/squareup/ui/crm/applet/CustomersAppletScope;->INSTANCE:Lcom/squareup/ui/crm/applet/CustomersAppletScope;

    sget-object v3, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->CUSTOMERS_APPLET:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    sget-object v4, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->CREATE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    sget-object v5, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;->REQUIRE_AT_LEAST_ONE_FIELD:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    .line 391
    invoke-static {p1, p2}, Lcom/squareup/crm/util/RolodexContactHelper;->newContactFromSearchTermOrGroup(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v7

    const/4 v6, 0x0

    .line 385
    invoke-interface/range {v1 .. v7}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;->getFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showGroup(Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 3

    .line 487
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupForViewGroupScreen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 490
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    sget-object v2, Lcom/squareup/protos/client/rolodex/GroupType;->AUDIENCE_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    if-ne p1, v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen;-><init>(Z)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showResolveDuplicatesScreen()V
    .locals 2

    .line 381
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->resolveDuplicatesFlow:Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;

    sget-object v1, Lcom/squareup/ui/crm/applet/CustomersAppletScope;->INSTANCE:Lcom/squareup/ui/crm/applet/CustomersAppletScope;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;->showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public viewFeedbackV2()V
    .locals 2

    .line 644
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/crm/v2/MessageListScreenV2;->INSTANCE:Lcom/squareup/ui/crm/v2/MessageListScreenV2;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public viewGroupHardwareBackButton()V
    .locals 2

    .line 631
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->multiSelectMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    if-eq v0, v1, :cond_0

    .line 632
    sget-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->setMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V

    goto :goto_0

    .line 634
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->closeViewGroupMasterScreen()V

    :goto_0
    return-void
.end method

.method public viewGroupsList()V
    .locals 4

    .line 418
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexGroupLoader;->refresh()V

    .line 420
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/crm/v2/ViewGroupsListScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/ViewGroupsListScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    sget-object v2, Lcom/squareup/ui/crm/v2/ViewGroupsListScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/ViewGroupsListScreen;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lflow/Direction;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method
