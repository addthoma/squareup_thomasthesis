.class public final enum Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;
.super Ljava/lang/Enum;
.source "ChooseCustomerFlow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/ChooseCustomerFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ChooseCustomerResultKey"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0008\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;",
        "",
        "(Ljava/lang/String;I)V",
        "EDIT_INVOICE",
        "EDIT_ESTIMATE",
        "ORDER_ENTRY",
        "CUSTOMERS_APPLET_MERGE_TWO",
        "APPOINTMENT",
        "TRANSFER_LOYALTY",
        "crm-choose-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

.field public static final enum APPOINTMENT:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

.field public static final enum CUSTOMERS_APPLET_MERGE_TWO:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

.field public static final enum EDIT_ESTIMATE:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

.field public static final enum EDIT_INVOICE:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

.field public static final enum ORDER_ENTRY:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

.field public static final enum TRANSFER_LOYALTY:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    const/4 v2, 0x0

    const-string v3, "EDIT_INVOICE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->EDIT_INVOICE:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    const/4 v2, 0x1

    const-string v3, "EDIT_ESTIMATE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->EDIT_ESTIMATE:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    const/4 v2, 0x2

    const-string v3, "ORDER_ENTRY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->ORDER_ENTRY:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    const/4 v2, 0x3

    const-string v3, "CUSTOMERS_APPLET_MERGE_TWO"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->CUSTOMERS_APPLET_MERGE_TWO:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    const/4 v2, 0x4

    const-string v3, "APPOINTMENT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->APPOINTMENT:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    const/4 v2, 0x5

    const-string v3, "TRANSFER_LOYALTY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->TRANSFER_LOYALTY:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->$VALUES:[Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;
    .locals 1

    const-class v0, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;
    .locals 1

    sget-object v0, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->$VALUES:[Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    invoke-virtual {v0}, [Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    return-object v0
.end method
