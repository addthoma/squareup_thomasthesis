.class public Lcom/squareup/ui/crm/ChooseCustomerFlow;
.super Ljava/lang/Object;
.source "ChooseCustomerFlow.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;,
        Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;,
        Lcom/squareup/ui/crm/ChooseCustomerFlow$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\u0008\u0017\u0018\u0000 \u000f2\u00020\u0001:\u0003\u000e\u000f\u0010B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u0005H\u0000\u00a2\u0006\u0002\u0008\rR\u001a\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00050\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/crm/ChooseCustomerFlow;",
        "",
        "()V",
        "results",
        "Lrx/Observable;",
        "Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;",
        "getResults",
        "()Lrx/Observable;",
        "resultsRelay",
        "Lcom/jakewharton/rxrelay/PublishRelay;",
        "sendResult",
        "",
        "result",
        "sendResult$crm_choose_customer_release",
        "ChooseCustomerResultKey",
        "Companion",
        "Result",
        "crm-choose-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/crm/ChooseCustomerFlow$Companion;


# instance fields
.field private final resultsRelay:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/crm/ChooseCustomerFlow;->Companion:Lcom/squareup/ui/crm/ChooseCustomerFlow$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    const-string v1, "PublishRelay.create()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow;->resultsRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    return-void
.end method

.method public static final start(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;ILcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;ZZ)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 8
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/crm/ChooseCustomerFlow;->Companion:Lcom/squareup/ui/crm/ChooseCustomerFlow$Companion;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Companion;->start(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;ILcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;ZZ)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getResults()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow;->resultsRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public final sendResult$crm_choose_customer_release(Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;)V
    .locals 1

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow;->resultsRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
