.class final Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$5;
.super Lkotlin/jvm/internal/Lambda;
.source "ChooseCustomerCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/util/List<",
        "Lcom/squareup/ui/crm/BaseCustomerListItem;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0012\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00060\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "Lcom/squareup/ui/crm/BaseCustomerListItem;",
        "kotlin.jvm.PlatformType",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/ChooseCustomerCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$5;->this$0:Lcom/squareup/ui/crm/ChooseCustomerCoordinator;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$5;->invoke(Ljava/util/List;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/BaseCustomerListItem;",
            ">;)V"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$5;->this$0:Lcom/squareup/ui/crm/ChooseCustomerCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->access$getAdapter$p(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)Lcom/squareup/ui/crm/SimpleCustomerListAdapter;

    move-result-object v0

    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/SimpleCustomerListAdapter;->setItems(Ljava/util/List;)V

    return-void
.end method
