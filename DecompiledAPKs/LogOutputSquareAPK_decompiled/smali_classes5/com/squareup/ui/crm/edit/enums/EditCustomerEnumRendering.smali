.class public final Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;
.super Ljava/lang/Object;
.source "EditCustomerEnumRendering.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B1\u0012\u000e\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0002\u0010\u000bJ\u0011\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000f\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u00c6\u0003J\u000f\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u00c6\u0003J;\u0010\u0015\u001a\u00020\u00002\u0010\u0008\u0002\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00032\u000e\u0008\u0002\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u000e\u0008\u0002\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0004H\u00d6\u0001R\u0017\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0019\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "title",
        "Lcom/squareup/resources/TextModel;",
        "",
        "options",
        "",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;",
        "onClose",
        "Lkotlin/Function0;",
        "",
        "(Lcom/squareup/resources/TextModel;Ljava/util/List;Lkotlin/jvm/functions/Function0;)V",
        "getOnClose",
        "()Lkotlin/jvm/functions/Function0;",
        "getOptions",
        "()Ljava/util/List;",
        "getTitle",
        "()Lcom/squareup/resources/TextModel;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final onClose:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;",
            ">;"
        }
    .end annotation
.end field

.field private final title:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/resources/TextModel;Ljava/util/List;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "options"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClose"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->title:Lcom/squareup/resources/TextModel;

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->options:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->onClose:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;Lcom/squareup/resources/TextModel;Ljava/util/List;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->title:Lcom/squareup/resources/TextModel;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->options:Ljava/util/List;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->onClose:Lkotlin/jvm/functions/Function0;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->copy(Lcom/squareup/resources/TextModel;Ljava/util/List;Lkotlin/jvm/functions/Function0;)Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->title:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->options:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->onClose:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final copy(Lcom/squareup/resources/TextModel;Ljava/util/List;Lkotlin/jvm/functions/Function0;)Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;"
        }
    .end annotation

    const-string v0, "options"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClose"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;-><init>(Lcom/squareup/resources/TextModel;Ljava/util/List;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->title:Lcom/squareup/resources/TextModel;

    iget-object v1, p1, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->title:Lcom/squareup/resources/TextModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->options:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->options:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->onClose:Lkotlin/jvm/functions/Function0;

    iget-object p1, p1, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->onClose:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getOnClose()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->onClose:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;",
            ">;"
        }
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->options:Ljava/util/List;

    return-object v0
.end method

.method public final getTitle()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 7
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->title:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->title:Lcom/squareup/resources/TextModel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->options:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->onClose:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EditCustomerEnumRendering(title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->title:Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onClose="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->onClose:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
