.class public final Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/cycler/StandardRowSpec$Creator<",
        "TI;TS;TV;>;",
        "Landroid/content/Context;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$create$1\n+ 2 EditCustomerLayoutRunner.kt\ncom/squareup/ui/crm/edit/EditCustomerLayoutRunner\n+ 3 NohoInputBox.kt\ncom/squareup/noho/NohoInputBox\n+ 4 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator\n*L\n1#1,87:1\n100#2,12:88\n112#2,6:101\n118#2:108\n126#2:111\n172#3:100\n173#3:107\n64#4,2:109\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00072\u0006\u0010\u0008\u001a\u00020\tH\n\u00a2\u0006\u0002\u0008\n\u00a8\u0006\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "Lcom/squareup/cycler/StandardRowSpec$Creator;",
        "context",
        "Landroid/content/Context;",
        "invoke",
        "com/squareup/cycler/StandardRowSpec$create$1",
        "com/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$create$1",
        "com/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$row$lambda$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $layoutId:I

.field final synthetic this$0:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;


# direct methods
.method public constructor <init>(ILcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;)V
    .locals 0

    iput p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1;->$layoutId:I

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/cycler/StandardRowSpec$Creator;

    check-cast p2, Landroid/content/Context;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "TI;TS;TV;>;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget v1, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1;->$layoutId:I

    const/4 v2, 0x0

    invoke-static {p2, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p1, p2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->setView(Landroid/view/View;)V

    .line 88
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/view/ViewGroup;

    sget v1, Lcom/squareup/crmeditcustomer/impl/R$id;->crm_email_input_box:I

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/noho/NohoInputBox;

    .line 89
    sget-object v1, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$recycler$1$4$1$1;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$recycler$1$4$1$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoInputBox;->validation(Lkotlin/jvm/functions/Function1;)V

    .line 90
    new-instance v1, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1$1;

    invoke-direct {v1, p2}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1$1;-><init>(Lcom/squareup/noho/NohoInputBox;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoInputBox;->error(Lkotlin/jvm/functions/Function2;)V

    .line 100
    invoke-virtual {p2}, Lcom/squareup/noho/NohoInputBox;->getEdit()Lcom/squareup/noho/NohoEditRow;

    move-result-object v1

    const/4 v2, 0x1

    .line 101
    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoEditRow;->setSingleLine(Z)V

    const v2, 0x80021

    .line 102
    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoEditRow;->setInputType(I)V

    .line 105
    new-instance v2, Lcom/squareup/noho/ClearPlugin;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xe

    const/4 v9, 0x0

    move-object v3, v2

    invoke-direct/range {v3 .. v9}, Lcom/squareup/noho/ClearPlugin;-><init>(Landroid/content/Context;ILjava/util/Set;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v2, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 109
    new-instance v0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1$2;

    invoke-direct {v0, p1, p2, p0}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1$2;-><init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Lcom/squareup/noho/NohoInputBox;Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->bind(Lkotlin/jvm/functions/Function2;)V

    return-void

    .line 37
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type V"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
