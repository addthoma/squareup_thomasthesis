.class final Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$$inlined$also$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEditCustomerWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->render(Lcom/squareup/ui/crm/edit/EditCustomerProps;Lcom/squareup/ui/crm/edit/EditCustomerState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/protos/client/rolodex/Group;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/crm/edit/EditCustomerState;",
        "+",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/crm/edit/EditCustomerState;",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        "groups",
        "",
        "Lcom/squareup/protos/client/rolodex/Group;",
        "invoke",
        "com/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$4$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context$inlined:Lcom/squareup/workflow/RenderContext;

.field final synthetic $state$inlined:Lcom/squareup/ui/crm/edit/EditCustomerState;

.field final synthetic this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/crm/edit/EditCustomerState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$$inlined$also$lambda$1;->this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$$inlined$also$lambda$1;->$context$inlined:Lcom/squareup/workflow/RenderContext;

    iput-object p3, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$$inlined$also$lambda$1;->$state$inlined:Lcom/squareup/ui/crm/edit/EditCustomerState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/util/List;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/crm/edit/EditCustomerState;",
            "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "groups"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$$inlined$also$lambda$1;->this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;

    .line 171
    iget-object v1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$$inlined$also$lambda$1;->$state$inlined:Lcom/squareup/ui/crm/edit/EditCustomerState;

    check-cast v1, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$LoadingGroups;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$LoadingGroups;->getAttribute()Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    move-result-object v1

    .line 169
    invoke-static {v0, v1, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->access$showGroupsEditorAction(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;Ljava/util/List;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 85
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$$inlined$also$lambda$1;->invoke(Ljava/util/List;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
