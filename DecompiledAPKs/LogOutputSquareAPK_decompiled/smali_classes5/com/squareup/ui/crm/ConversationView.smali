.class public Lcom/squareup/ui/crm/ConversationView;
.super Landroid/widget/LinearLayout;
.source "ConversationView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/ConversationView$Component;,
        Lcom/squareup/ui/crm/ConversationView$SharedScope;
    }
.end annotation


# instance fields
.field private addCouponButton:Landroid/widget/ImageView;

.field private addCouponRow:Lcom/squareup/ui/crm/rows/AddCouponRow;

.field private billHistoryButton:Lcom/squareup/marketfont/MarketButton;

.field private messageEdit:Landroid/widget/EditText;

.field private onAddCouponClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onSendMessageClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/crm/ConversationPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/view/View;

.field private rows:Landroid/widget/LinearLayout;

.field private sendMessageButton:Landroid/widget/Button;

.field private final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 62
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    const-class p2, Lcom/squareup/ui/crm/ConversationView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/crm/ConversationView$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/crm/ConversationView$Component;->inject(Lcom/squareup/ui/crm/ConversationView;)V

    .line 64
    sget p2, Lcom/squareup/crmscreens/R$layout;->crm_conversation_view:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/ConversationView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 65
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/ConversationView;->setOrientation(I)V

    .line 67
    invoke-virtual {p0}, Lcom/squareup/ui/crm/ConversationView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/crm/ConversationView;->shortAnimTimeMs:I

    return-void
.end method


# virtual methods
.method addCouponRow()Lcom/squareup/ui/crm/rows/ConversationCouponRightRow;
    .locals 2

    .line 158
    new-instance v0, Lcom/squareup/ui/crm/rows/ConversationCouponRightRow;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/ConversationView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/rows/ConversationCouponRightRow;-><init>(Landroid/content/Context;)V

    .line 159
    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method addLeftRow()Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;
    .locals 2

    .line 146
    new-instance v0, Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/ConversationView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/rows/ConversationMessageLeftRow;-><init>(Landroid/content/Context;)V

    .line 147
    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method addRightRow()Lcom/squareup/ui/crm/rows/ConversationMessageRightRow;
    .locals 2

    .line 152
    new-instance v0, Lcom/squareup/ui/crm/rows/ConversationMessageRightRow;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/ConversationView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/rows/ConversationMessageRightRow;-><init>(Landroid/content/Context;)V

    .line 153
    iget-object v1, p0, Lcom/squareup/ui/crm/ConversationView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method public busy()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->presenter:Lcom/squareup/ui/crm/ConversationPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/ConversationPresenter;->busy()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method clearMessage()V
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->messageEdit:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method clearRows()V
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method public conversation()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/dialogue/Conversation;",
            ">;"
        }
    .end annotation

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->presenter:Lcom/squareup/ui/crm/ConversationPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/ConversationPresenter;->conversation()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method enableMessageEdit(Z)V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->messageEdit:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    return-void
.end method

.method enableSendMessageButton(Z)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->sendMessageButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method getMessage()Ljava/lang/String;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->messageEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hideCouponRow()V
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->addCouponRow:Lcom/squareup/ui/crm/rows/AddCouponRow;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/rows/AddCouponRow;->setVisibility(I)V

    return-void
.end method

.method messageIsBlank()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->messageEdit:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->isBlank(Landroid/widget/TextView;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onAddCouponClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->onAddCouponClicked:Lrx/Observable;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 86
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->presenter:Lcom/squareup/ui/crm/ConversationPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/ConversationPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method onBillHistoryButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->billHistoryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onBillHistoryClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;"
        }
    .end annotation

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->presenter:Lcom/squareup/ui/crm/ConversationPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/ConversationPresenter;->onBillHistoryClicked(Lcom/squareup/ui/crm/ConversationView;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->presenter:Lcom/squareup/ui/crm/ConversationPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/ConversationPresenter;->dropView(Ljava/lang/Object;)V

    .line 92
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 71
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 73
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_conversation_rows:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->rows:Landroid/widget/LinearLayout;

    .line 74
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_message_input:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->messageEdit:Landroid/widget/EditText;

    .line 75
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_send_message_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->sendMessageButton:Landroid/widget/Button;

    .line 76
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->progressBar:Landroid/view/View;

    .line 77
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_add_coupon_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->addCouponButton:Landroid/widget/ImageView;

    .line 78
    sget v0, Lcom/squareup/crmscreens/R$id;->add_coupon_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/rows/AddCouponRow;

    iput-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->addCouponRow:Lcom/squareup/ui/crm/rows/AddCouponRow;

    .line 79
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_bill_history_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->billHistoryButton:Lcom/squareup/marketfont/MarketButton;

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->sendMessageButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->onSendMessageClicked:Lrx/Observable;

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->addCouponButton:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->onAddCouponClicked:Lrx/Observable;

    return-void
.end method

.method public onRemoveCouponClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->addCouponRow:Lcom/squareup/ui/crm/rows/AddCouponRow;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/rows/AddCouponRow;->onCloseClicked()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 97
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented. Set saveEnabled to false."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method onSendMessageClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->onSendMessageClicked:Lrx/Observable;

    return-object v0
.end method

.method public setConversationToken(Ljava/lang/String;)V
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->presenter:Lcom/squareup/ui/crm/ConversationPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/ConversationPresenter;->setConversationToken(Ljava/lang/String;)V

    return-void
.end method

.method public showAddCouponButton()V
    .locals 2

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->addCouponButton:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method showBillHistoryButton(Ljava/lang/String;Z)V
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->billHistoryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    .line 131
    iget-object p1, p0, Lcom/squareup/ui/crm/ConversationView;->billHistoryButton:Lcom/squareup/marketfont/MarketButton;

    iget p2, p0, Lcom/squareup/ui/crm/ConversationView;->shortAnimTimeMs:I

    int-to-long v0, p2

    invoke-static {p1, v0, v1}, Lcom/squareup/register/widgets/Animations;->expandVertically(Landroid/view/View;J)V

    goto :goto_0

    .line 133
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/ConversationView;->billHistoryButton:Lcom/squareup/marketfont/MarketButton;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public showCouponRow(Ljava/lang/CharSequence;)V
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationView;->addCouponRow:Lcom/squareup/ui/crm/rows/AddCouponRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/rows/AddCouponRow;->setTitle(Ljava/lang/CharSequence;)V

    .line 173
    iget-object p1, p0, Lcom/squareup/ui/crm/ConversationView;->addCouponRow:Lcom/squareup/ui/crm/rows/AddCouponRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/rows/AddCouponRow;->setVisibility(I)V

    return-void
.end method

.method showProgress(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 165
    iget-object p1, p0, Lcom/squareup/ui/crm/ConversationView;->progressBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/crm/ConversationView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 167
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/ConversationView;->progressBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/crm/ConversationView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    :goto_0
    return-void
.end method
