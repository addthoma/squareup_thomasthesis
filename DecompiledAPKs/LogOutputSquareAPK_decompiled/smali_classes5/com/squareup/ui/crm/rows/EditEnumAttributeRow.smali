.class public Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;
.super Lcom/squareup/ui/account/view/SmartLineRow;
.source "EditEnumAttributeRow.java"

# interfaces
.implements Lcom/squareup/ui/crm/rows/HasAttribute;


# instance fields
.field private attribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

.field private onClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private formatEnumValues(Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 46
    invoke-virtual {p0}, Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/utilities/R$string;->list_pattern_long_nonfinal_separator:I

    invoke-static {v0, v1}, Lcom/squareup/util/ListPhrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/util/ListPhrase;

    move-result-object v0

    .line 48
    invoke-virtual {v0, p1}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 49
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    return-object p1
.end method


# virtual methods
.method public getAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;->attribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    return-object v0
.end method

.method public onClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;->onClicked:Lrx/Observable;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 27
    invoke-super {p0}, Lcom/squareup/ui/account/view/SmartLineRow;->onFinishInflate()V

    .line 28
    invoke-static {p0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;->onClicked:Lrx/Observable;

    return-void
.end method

.method public showAttribute(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V
    .locals 3

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;->attribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    .line 34
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;->setTitleText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 35
    :goto_0
    invoke-virtual {p0, v2}, Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;->setValueVisible(Z)V

    if-eqz p2, :cond_1

    .line 36
    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->enum_values:Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;->formatEnumValues(Ljava/util/List;)Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 37
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_2

    goto :goto_2

    :cond_2
    const/16 v1, 0x8

    :goto_2
    invoke-virtual {p0, v1}, Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;->setVisibility(I)V

    .line 38
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_read_only:Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    xor-int/2addr p2, v0

    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;->setEnabled(Z)V

    .line 39
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_read_only:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_3

    sget-object p1, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    goto :goto_3

    :cond_3
    sget-object p1, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    .line 42
    :goto_3
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    return-void
.end method
