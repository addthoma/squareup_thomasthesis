.class public final enum Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;
.super Ljava/lang/Enum;
.source "ProfileLineRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0007\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;",
        "",
        "(Ljava/lang/String;I)V",
        "TEXT",
        "TEXT_LINK",
        "PHONE",
        "EMAIL",
        "MAP",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

.field public static final enum EMAIL:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

.field public static final enum MAP:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

.field public static final enum PHONE:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

.field public static final enum TEXT:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

.field public static final enum TEXT_LINK:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    new-instance v1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    const/4 v2, 0x0

    const-string v3, "TEXT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->TEXT:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    const/4 v2, 0x1

    const-string v3, "TEXT_LINK"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->TEXT_LINK:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    const/4 v2, 0x2

    const-string v3, "PHONE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->PHONE:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    const/4 v2, 0x3

    const-string v3, "EMAIL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->EMAIL:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    const/4 v2, 0x4

    const-string v3, "MAP"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->MAP:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->$VALUES:[Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 81
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;
    .locals 1

    const-class v0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;
    .locals 1

    sget-object v0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->$VALUES:[Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    invoke-virtual {v0}, [Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    return-object v0
.end method
