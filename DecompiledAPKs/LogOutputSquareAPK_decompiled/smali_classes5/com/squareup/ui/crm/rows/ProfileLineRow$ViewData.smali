.class public final Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;
.super Ljava/lang/Object;
.source "ProfileLineRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/rows/ProfileLineRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001\u0017B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0006H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
        "",
        "title",
        "",
        "info",
        "type",
        "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;",
        "(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V",
        "getInfo",
        "()Ljava/lang/String;",
        "getTitle",
        "getType",
        "()Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "Type",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final info:Ljava/lang/String;

.field private final title:Ljava/lang/String;

.field private final type:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V
    .locals 1

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "info"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->info:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->type:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;ILjava/lang/Object;)Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->title:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->info:Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->type:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->copy(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->info:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->type:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;
    .locals 1

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "info"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->info:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->info:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->type:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    iget-object p1, p1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->type:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getInfo()Ljava/lang/String;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->info:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->type:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->title:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->info:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->type:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ViewData(title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->info:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->type:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
