.class public Lcom/squareup/ui/crm/rows/ContactListBottomRow;
.super Landroid/widget/LinearLayout;
.source "ContactListBottomRow.java"


# instance fields
.field private message:Lcom/squareup/widgets/MessageView;

.field private progress:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .line 31
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 33
    sget v0, Lcom/squareup/crm/R$id;->crm_contact_list_bottom_row_progress:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/ContactListBottomRow;->progress:Landroid/widget/ProgressBar;

    .line 34
    sget v0, Lcom/squareup/crm/R$id;->crm_contact_list_bottom_row_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/ContactListBottomRow;->message:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public showMessage(Z)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ContactListBottomRow;->message:Lcom/squareup/widgets/MessageView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public showProgress(Z)V
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ContactListBottomRow;->progress:Landroid/widget/ProgressBar;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
