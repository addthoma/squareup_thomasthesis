.class public Lcom/squareup/ui/crm/ChooseCustomerScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "ChooseCustomerScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/container/MaybePersistent;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/ChooseCustomerScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/ChooseCustomerScreen$Component;,
        Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;
    }
.end annotation


# instance fields
.field public final parentPath:Lcom/squareup/ui/crm/ChooseCustomerScope;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/ChooseCustomerScope;)V
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScreen;->parentPath:Lcom/squareup/ui/crm/ChooseCustomerScope;

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 87
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CHOOSE_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/squareup/ui/main/RegisterTreeKey;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerScreen;->parentPath:Lcom/squareup/ui/crm/ChooseCustomerScope;

    iget-object v1, v1, Lcom/squareup/ui/crm/ChooseCustomerScope;->chooseCustomerResultKey:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScreen;->parentPath:Lcom/squareup/ui/crm/ChooseCustomerScope;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScreen;->parentPath:Lcom/squareup/ui/crm/ChooseCustomerScope;

    iget-boolean v0, v0, Lcom/squareup/ui/crm/ChooseCustomerScope;->isFirstCardScreen:Z

    if-eqz v0, :cond_1

    .line 100
    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getDevice(Landroid/content/Context;)Lcom/squareup/util/Device;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    :goto_0
    return-object p1
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 105
    const-class v0, Lcom/squareup/ui/crm/ChooseCustomerScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/ChooseCustomerScreen$Component;

    .line 106
    invoke-interface {p1}, Lcom/squareup/ui/crm/ChooseCustomerScreen$Component;->coordinator()Lcom/squareup/ui/crm/ChooseCustomerCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 91
    sget v0, Lcom/squareup/crmchoosecustomer/R$layout;->crm_choose_customer_screen:I

    return v0
.end method
