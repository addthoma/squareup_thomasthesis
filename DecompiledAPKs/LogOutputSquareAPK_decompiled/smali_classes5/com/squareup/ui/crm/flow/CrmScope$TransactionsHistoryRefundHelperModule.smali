.class public abstract Lcom/squareup/ui/crm/flow/CrmScope$TransactionsHistoryRefundHelperModule;
.super Ljava/lang/Object;
.source "CrmScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/CrmScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "TransactionsHistoryRefundHelperModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideTransactionsHistoryRefundHelper(Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper;)Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
