.class public Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;
.super Lcom/squareup/ui/crm/flow/CrmScope$BaseRunner;
.source "ReaderSdkCrmRunner.java"


# static fields
.field public static final CONTACT:Ljava/lang/String; = "contact"


# instance fields
.field private contact:Lcom/squareup/protos/client/rolodex/Contact;

.field private final controller:Lcom/squareup/api/ApiAddCardOnFileController;

.field private final workflow:Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;Lcom/squareup/api/ApiAddCardOnFileController;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/CrmScope$BaseRunner;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;->workflow:Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;->controller:Lcom/squareup/api/ApiAddCardOnFileController;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$ReaderSdkCrmRunner(Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;)V
    .locals 3

    .line 43
    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;->getState()Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getServerCardInfo()Lcom/squareup/protos/client/instruments/InstrumentSummary;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/instruments/InstrumentSummary;->card:Lcom/squareup/protos/client/instruments/CardSummary;

    iget-object v0, v0, Lcom/squareup/protos/client/instruments/CardSummary;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    .line 42
    invoke-static {v0}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->builderFrom(Lcom/squareup/protos/client/bills/CardTender$Card;)Lcom/squareup/sdk/reader/checkout/Card$Builder;

    move-result-object v0

    .line 45
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;->getState()Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/Card;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 46
    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;->getState()Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/Card;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 47
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    .line 45
    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/sdk/reader/checkout/Card$Builder;->cardholderName(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/Card$Builder;

    move-result-object v1

    .line 48
    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;->getState()Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getServerCardInfo()Lcom/squareup/protos/client/instruments/InstrumentSummary;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/instruments/InstrumentSummary;->instrument_token:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/sdk/reader/checkout/Card$Builder;->id(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/Card$Builder;

    move-result-object v1

    .line 51
    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;->getState()Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getServerCardInfo()Lcom/squareup/protos/client/instruments/InstrumentSummary;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/instruments/InstrumentSummary;->card:Lcom/squareup/protos/client/instruments/CardSummary;

    iget-object v2, v2, Lcom/squareup/protos/client/instruments/CardSummary;->expiration_date:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->month:Ljava/lang/String;

    .line 50
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 49
    invoke-virtual {v1, v2}, Lcom/squareup/sdk/reader/checkout/Card$Builder;->expirationMonth(I)Lcom/squareup/sdk/reader/checkout/Card$Builder;

    move-result-object v1

    .line 54
    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;->getState()Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getServerCardInfo()Lcom/squareup/protos/client/instruments/InstrumentSummary;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/instruments/InstrumentSummary;->card:Lcom/squareup/protos/client/instruments/CardSummary;

    iget-object p1, p1, Lcom/squareup/protos/client/instruments/CardSummary;->expiration_date:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->year:Ljava/lang/String;

    .line 53
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    .line 52
    invoke-virtual {v1, p1}, Lcom/squareup/sdk/reader/checkout/Card$Builder;->expirationYear(I)Lcom/squareup/sdk/reader/checkout/Card$Builder;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;->controller:Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-virtual {v0}, Lcom/squareup/sdk/reader/checkout/Card$Builder;->build()Lcom/squareup/sdk/reader/checkout/Card;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/api/ApiAddCardOnFileController;->onSuccess(Lcom/squareup/sdk/reader/checkout/Card;)V

    return-void

    :catch_0
    move-exception p1

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Server gave us bad expiration data"

    invoke-direct {v0, v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public synthetic lambda$onEnterScope$1$ReaderSdkCrmRunner(Lkotlin/Unit;)V
    .locals 0

    .line 65
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;->controller:Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-virtual {p1}, Lcom/squareup/api/ApiAddCardOnFileController;->onCancel()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 35
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/CrmScope;

    .line 36
    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 37
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;->workflow:Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;->workflow:Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    .line 40
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onResult()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ReaderSdkCrmRunner$I6LYQLps88tptMHE0wTwlzh_bpI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ReaderSdkCrmRunner$I6LYQLps88tptMHE0wTwlzh_bpI;-><init>(Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;)V

    .line 41
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 39
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;->workflow:Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    .line 63
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onCancel()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ReaderSdkCrmRunner$8UhSPLlFVTgzpFYc_Hxjr-VqsFc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ReaderSdkCrmRunner$8UhSPLlFVTgzpFYc_Hxjr-VqsFc;-><init>(Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;)V

    .line 64
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 62
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public startSaveCardWorkflow()V
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;->workflow:Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->showFirstScreen(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method
