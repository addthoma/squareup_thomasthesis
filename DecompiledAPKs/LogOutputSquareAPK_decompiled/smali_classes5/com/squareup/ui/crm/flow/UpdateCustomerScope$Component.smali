.class public interface abstract Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Component;
.super Ljava/lang/Object;
.source "UpdateCustomerScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/UpdateCustomerScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract chooseDateAttributeDialogScreen()Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Component;
.end method

.method public abstract chooseEnumAttributeScreenV2()Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Component;
.end method

.method public abstract chooseGroupsScreen()Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Component;
.end method

.method public abstract createGroupScreen()Lcom/squareup/ui/crm/cards/CreateGroupScreen$Component;
.end method

.method public abstract inject(Lcom/squareup/ui/permissions/SystemPermissionDialog;)V
.end method

.method public abstract pickAddressBookContactScreen()Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Component;
.end method

.method public abstract runner()Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;
.end method

.method public abstract systemPermissionsPresenter()Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;
.end method

.method public abstract updateCustomerScreenV2()Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$Component;
.end method
