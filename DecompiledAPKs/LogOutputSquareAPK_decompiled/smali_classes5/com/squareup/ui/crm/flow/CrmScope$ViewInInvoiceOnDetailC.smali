.class public interface abstract Lcom/squareup/ui/crm/flow/CrmScope$ViewInInvoiceOnDetailC;
.super Ljava/lang/Object;
.source "CrmScope.java"

# interfaces
.implements Lcom/squareup/ui/crm/flow/CrmScope$CustomerProfileComponent;


# annotations
.annotation runtime Lcom/squareup/crm/RolodexAttachmentLoader$SharedScope;
.end annotation

.annotation runtime Lcom/squareup/crm/RolodexEventLoader$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/CrmScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ViewInInvoiceOnDetailC"
.end annotation


# virtual methods
.method public abstract profileAttachmentsScope()Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;
.end method
