.class Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$5;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "AbstractCrmScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->showUpdateCustomerV2()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V
    .locals 0

    .line 1316
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$5;->this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 9

    .line 1318
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$5;->this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$5;->this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;

    iget-object v2, v1, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$5;->this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;

    iget-object v3, v1, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v4, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->VIEW_CUSTOMER:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    sget-object v5, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->EDIT:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    sget-object v6, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;->REQUIRE_AT_LEAST_ONE_FIELD:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$5;->this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;

    iget-object v1, v1, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v7, v1, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$5;->this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;

    iget-object v1, v1, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 1325
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/protos/client/rolodex/Contact;

    .line 1319
    invoke-interface/range {v2 .. v8}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;->getFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    .line 1318
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
