.class public abstract Lcom/squareup/ui/crm/flow/CrmScope$BaseAddCustomerToSaleModule;
.super Ljava/lang/Object;
.source "CrmScope.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/ui/crm/flow/CrmScope$BillHistoryModule;,
        Lcom/squareup/ui/crm/flow/UpdateCustomerModule;,
        Lcom/squareup/ui/crm/edit/EditCustomerModule;,
        Lcom/squareup/ui/crm/flow/CrmScope$TransactionsHistoryRefundHelperModule;,
        Lcom/squareup/crm/RolodexLoaders$AttachmentLoaderModule;,
        Lcom/squareup/crm/RolodexLoaders$EventLoaderModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/CrmScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BaseAddCustomerToSaleModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract mergingCustomersScreen(Lcom/squareup/ui/crm/flow/MergeCustomersFlow;)Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideAddCouponRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/AddCouponScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideAdjustPointsScreenRunner(Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;)Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideAllAppointmentsScreenRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideAllCouponsAndRewardsScreenRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideAllFrequentItemsScreenRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideAllNoteScreenRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/AllNotesScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideBaseRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/flow/CrmScope$BaseRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideConfirmSendLoyaltyStatusDialogScreenRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideConversationRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/ConversationCardScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideCreateNoteScreenRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideCustomerActivityScreenRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideCustomerInvoiceScreenRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideCustomerSaveCardScreenRunner(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideDeleteCustomerRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideDeleteLoyaltyAccountRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideDeletingLoyaltyAccountRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideDippedCardSpinnerScreenRunner(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideExpiringPointsRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideInvoiceDetailScopeRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/invoices/ui/CrmInvoiceListRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideLoyaltySectionDropDownBottomDialogRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideManageCouponsAndRewardsScreenRunner(Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;)Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideReminderScreenRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/ReminderScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideRemovingCardOnFileScreenRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSaveCardCustomerEmailScreenRunner(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSaveCardSpinnerScreenRunner(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSaveCardVerifyPostalCodeRunner(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSeeAllRewardTiersScreenRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSendMessageScreenRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTransferringLoyaltyAccountRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideUpdateLoyaltyConflictRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideUpdateLoyaltyPhoneRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideViewCustomerScreenRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideViewLoyaltyBalanceScreenRunner(Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;)Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideViewNotesScreenRunner(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Lcom/squareup/ui/crm/cards/ViewNoteScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideVoidingCouponsScreenRunner(Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;)Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract reviewCustomerForMergingScreen(Lcom/squareup/ui/crm/flow/MergeCustomersFlow;)Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
