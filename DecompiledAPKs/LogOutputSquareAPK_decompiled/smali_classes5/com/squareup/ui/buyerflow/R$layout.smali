.class public final Lcom/squareup/ui/buyerflow/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyerflow/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final auth_spinner_view:I = 0x7f0d006c

.field public static final buyer_flow_action_bar:I = 0x7f0d00a2

.field public static final buyer_flow_action_bar_new_sale:I = 0x7f0d00a3

.field public static final buyer_flow_action_bar_new_sale_portrait:I = 0x7f0d00a4

.field public static final buyer_order_ticket_name_view:I = 0x7f0d00a9

.field public static final buyer_order_ticket_name_view_contents:I = 0x7f0d00aa

.field public static final coupon_container:I = 0x7f0d0109

.field public static final email_collection_view:I = 0x7f0d0241

.field public static final email_collection_view_all_done_contents:I = 0x7f0d0242

.field public static final email_collection_view_enrollment_contents:I = 0x7f0d0243

.field public static final emv_applications_options_container:I = 0x7f0d0252

.field public static final emv_approved_view:I = 0x7f0d0253

.field public static final emv_choose_option_view:I = 0x7f0d0254

.field public static final emv_option_item:I = 0x7f0d0255

.field public static final emv_progress_view:I = 0x7f0d0256

.field public static final invoice_paid_view:I = 0x7f0d02f2

.field public static final invoice_sent_save_view:I = 0x7f0d02f9

.field public static final loyalty_reward_tier:I = 0x7f0d0340

.field public static final loyalty_view:I = 0x7f0d0344

.field public static final loyalty_view_all_done_contents:I = 0x7f0d0345

.field public static final loyalty_view_cash_app_banner:I = 0x7f0d0346

.field public static final loyalty_view_enrollment_contents:I = 0x7f0d0347

.field public static final loyalty_view_reward_status_contents:I = 0x7f0d0348

.field public static final loyalty_view_reward_tiers:I = 0x7f0d0349

.field public static final partial_auth_warning_view:I = 0x7f0d0422

.field public static final pay_contactless_view:I = 0x7f0d042b

.field public static final payment_error_view:I = 0x7f0d0435

.field public static final post_auth_coupon_view:I = 0x7f0d044c

.field public static final receipt_view_legacy:I = 0x7f0d0472

.field public static final refund_policy_popup_view:I = 0x7f0d047a

.field public static final retry_tender_view:I = 0x7f0d0483

.field public static final sign_view:I = 0x7f0d04d1

.field public static final signature_scaling_tip_option_item:I = 0x7f0d04d2

.field public static final signature_tip_option_item:I = 0x7f0d04d4

.field public static final store_and_forward_quick_enable_view:I = 0x7f0d0507

.field public static final tablet_tip_button_contents:I = 0x7f0d0511

.field public static final tip_view_custom_stub:I = 0x7f0d0555


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
