.class public Lcom/squareup/ui/InfoBarView;
.super Landroid/widget/RelativeLayout;
.source "InfoBarView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/InfoBarView$ButtonLocation;
    }
.end annotation


# instance fields
.field private final button:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/squareup/ui/InfoBarView$ButtonLocation;)V
    .locals 1

    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, p1, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    invoke-static {p4}, Lcom/squareup/ui/InfoBarView$ButtonLocation;->access$000(Lcom/squareup/ui/InfoBarView$ButtonLocation;)I

    move-result v0

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/InfoBarView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 38
    invoke-virtual {p0}, Lcom/squareup/ui/InfoBarView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 39
    sget v0, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 40
    invoke-virtual {p0, p1}, Lcom/squareup/ui/InfoBarView;->setMinimumHeight(I)V

    .line 42
    sget p1, Lcom/squareup/widgets/pos/R$id;->info_bar_message:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 43
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    sget p1, Lcom/squareup/widgets/pos/R$id;->info_bar_button:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/InfoBarView;->button:Landroid/widget/TextView;

    .line 48
    iget-object p1, p0, Lcom/squareup/ui/InfoBarView;->button:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingTop()I

    move-result p1

    iget-object p2, p0, Lcom/squareup/ui/InfoBarView;->button:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result p2

    const/4 v0, 0x0

    invoke-virtual {p0, p3, p1, v0, p2}, Lcom/squareup/ui/InfoBarView;->setPadding(IIII)V

    .line 49
    sget-object p1, Lcom/squareup/ui/InfoBarView$ButtonLocation;->RIGHT_ALIGNED:Lcom/squareup/ui/InfoBarView$ButtonLocation;

    if-ne p4, p1, :cond_0

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/InfoBarView;->button:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingTop()I

    move-result p2

    iget-object p4, p0, Lcom/squareup/ui/InfoBarView;->button:Landroid/widget/TextView;

    invoke-virtual {p4}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result p4

    invoke-virtual {p1, p3, p2, p3, p4}, Landroid/widget/TextView;->setPadding(IIII)V

    :cond_0
    return-void
.end method


# virtual methods
.method public showButton(ILandroid/view/View$OnClickListener;)V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/InfoBarView;->button:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 57
    iget-object p1, p0, Lcom/squareup/ui/InfoBarView;->button:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    iget-object p1, p0, Lcom/squareup/ui/InfoBarView;->button:Landroid/widget/TextView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method
