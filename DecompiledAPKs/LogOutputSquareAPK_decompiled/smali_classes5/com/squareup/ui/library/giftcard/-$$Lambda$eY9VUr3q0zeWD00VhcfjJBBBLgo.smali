.class public final synthetic Lcom/squareup/ui/library/giftcard/-$$Lambda$eY9VUr3q0zeWD00VhcfjJBBBLgo;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/library/giftcard/-$$Lambda$eY9VUr3q0zeWD00VhcfjJBBBLgo;->f$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/-$$Lambda$eY9VUr3q0zeWD00VhcfjJBBBLgo;->f$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    check-cast p1, Lcom/squareup/x2/X2SwipedGiftCard;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->onX2SwipedGiftCard(Lcom/squareup/x2/X2SwipedGiftCard;)V

    return-void
.end method
