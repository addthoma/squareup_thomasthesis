.class public final Lcom/squareup/ui/library/DiscountEntryScreenRunner_Factory;
.super Ljava/lang/Object;
.source "DiscountEntryScreenRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/library/DiscountEntryScreenRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/library/DiscountEntryScreenRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/library/DiscountEntryScreenRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/ui/library/DiscountEntryScreenRunner_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/ui/library/DiscountEntryScreenRunner_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/library/DiscountEntryScreenRunner_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lflow/Flow;)Lcom/squareup/ui/library/DiscountEntryScreenRunner;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/ui/library/DiscountEntryScreenRunner;

    invoke-direct {v0, p0}, Lcom/squareup/ui/library/DiscountEntryScreenRunner;-><init>(Lflow/Flow;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/library/DiscountEntryScreenRunner;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryScreenRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    invoke-static {v0}, Lcom/squareup/ui/library/DiscountEntryScreenRunner_Factory;->newInstance(Lflow/Flow;)Lcom/squareup/ui/library/DiscountEntryScreenRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/library/DiscountEntryScreenRunner_Factory;->get()Lcom/squareup/ui/library/DiscountEntryScreenRunner;

    move-result-object v0

    return-object v0
.end method
