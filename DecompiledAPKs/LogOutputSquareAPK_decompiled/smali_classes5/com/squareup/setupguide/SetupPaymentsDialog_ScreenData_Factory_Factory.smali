.class public final Lcom/squareup/setupguide/SetupPaymentsDialog_ScreenData_Factory_Factory;
.super Ljava/lang/Object;
.source "SetupPaymentsDialog_ScreenData_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/setupguide/SetupPaymentsDialog_ScreenData_Factory_Factory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/setupguide/SetupPaymentsDialog_ScreenData_Factory_Factory;
    .locals 1

    .line 17
    invoke-static {}, Lcom/squareup/setupguide/SetupPaymentsDialog_ScreenData_Factory_Factory$InstanceHolder;->access$000()Lcom/squareup/setupguide/SetupPaymentsDialog_ScreenData_Factory_Factory;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance()Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;

    invoke-direct {v0}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;-><init>()V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;
    .locals 1

    .line 13
    invoke-static {}, Lcom/squareup/setupguide/SetupPaymentsDialog_ScreenData_Factory_Factory;->newInstance()Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupPaymentsDialog_ScreenData_Factory_Factory;->get()Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;

    move-result-object v0

    return-object v0
.end method
