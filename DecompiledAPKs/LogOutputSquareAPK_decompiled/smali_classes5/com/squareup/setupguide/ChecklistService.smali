.class public interface abstract Lcom/squareup/setupguide/ChecklistService;
.super Ljava/lang/Object;
.source "ChecklistService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020\nH\'\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/setupguide/ChecklistService;",
        "",
        "completeActionItem",
        "Lcom/squareup/setupguide/CompleteActionItemStandardResponse;",
        "Lcom/squareup/protos/checklist/service/CompleteActionItemResponse;",
        "request",
        "Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;",
        "getChecklist",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/checklist/service/GetChecklistResponse;",
        "Lcom/squareup/protos/checklist/service/GetChecklistRequest;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract completeActionItem(Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;)Lcom/squareup/setupguide/CompleteActionItemStandardResponse;
    .param p1    # Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;",
            ")",
            "Lcom/squareup/setupguide/CompleteActionItemStandardResponse<",
            "Lcom/squareup/protos/checklist/service/CompleteActionItemResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.checklist.service.ChecklistService/CompleteActionItem"
    .end annotation
.end method

.method public abstract getChecklist(Lcom/squareup/protos/checklist/service/GetChecklistRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/checklist/service/GetChecklistRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/checklist/service/GetChecklistRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/checklist/service/GetChecklistResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.checklist.service.ChecklistService/GetChecklist"
    .end annotation
.end method
