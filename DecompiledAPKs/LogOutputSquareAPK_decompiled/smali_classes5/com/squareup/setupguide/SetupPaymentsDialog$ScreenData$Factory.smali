.class public final Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;
.super Ljava/lang/Object;
.source "SetupPaymentsDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0001J\u000e\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0001J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0001J\u000e\u0010\u0008\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0001\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;",
        "",
        "()V",
        "forSetupPaymentsPromptWhenSendingEstimate",
        "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;",
        "key",
        "forSetupPaymentsPromptWhenSendingInvoice",
        "forSetupPaymentsPromptWithNoIDVSendingEnabled",
        "forSetupPaymentsReminder",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final forSetupPaymentsPromptWhenSendingEstimate(Ljava/lang/Object;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;
    .locals 8

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    new-instance v0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;

    .line 95
    sget v3, Lcom/squareup/setupguide/R$string;->onboarding_modal_idv_estimate_error_title:I

    .line 96
    sget v1, Lcom/squareup/setupguide/R$string;->onboarding_modal_idv_estimate_error_message:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 97
    sget v5, Lcom/squareup/setupguide/R$string;->onboarding_modal_button_setup_payments:I

    .line 98
    sget v1, Lcom/squareup/setupguide/R$string;->onboarding_modal_button_later:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p1

    .line 93
    invoke-direct/range {v1 .. v7}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;-><init>(Ljava/lang/Object;ILjava/lang/Integer;ILjava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public final forSetupPaymentsPromptWhenSendingInvoice(Ljava/lang/Object;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;
    .locals 8

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    new-instance v0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;

    .line 73
    sget v3, Lcom/squareup/setupguide/R$string;->onboarding_modal_idv_invoice_error_title:I

    .line 74
    sget v1, Lcom/squareup/setupguide/R$string;->onboarding_modal_idv_invoice_error_message:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 75
    sget v5, Lcom/squareup/setupguide/R$string;->onboarding_modal_button_setup_payments:I

    .line 76
    sget v1, Lcom/squareup/setupguide/R$string;->onboarding_modal_button_later:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p1

    .line 71
    invoke-direct/range {v1 .. v7}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;-><init>(Ljava/lang/Object;ILjava/lang/Integer;ILjava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public final forSetupPaymentsPromptWithNoIDVSendingEnabled(Ljava/lang/Object;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;
    .locals 8

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    new-instance v0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;

    .line 86
    sget v3, Lcom/squareup/setupguide/R$string;->onboarding_modal_send_with_no_idv_title:I

    .line 87
    sget v1, Lcom/squareup/setupguide/R$string;->onboarding_modal_send_with_no_idv_message:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 88
    sget v5, Lcom/squareup/setupguide/R$string;->onboarding_modal_button_setup_payments:I

    .line 89
    sget v1, Lcom/squareup/setupguide/R$string;->send_anyways:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p1

    .line 84
    invoke-direct/range {v1 .. v7}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;-><init>(Ljava/lang/Object;ILjava/lang/Integer;ILjava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public final forSetupPaymentsReminder(Ljava/lang/Object;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;
    .locals 8

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    new-instance v0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;

    .line 60
    sget v3, Lcom/squareup/setupguide/R$string;->onboarding_modal_idv_invoice_reminder_title:I

    .line 61
    sget v1, Lcom/squareup/setupguide/R$string;->onboarding_modal_idv_invoice_reminder_message:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 62
    sget v5, Lcom/squareup/setupguide/R$string;->onboarding_modal_button_setup_payments:I

    .line 63
    sget v1, Lcom/squareup/setupguide/R$string;->onboarding_modal_button_later:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p1

    .line 58
    invoke-direct/range {v1 .. v7}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;-><init>(Ljava/lang/Object;ILjava/lang/Integer;ILjava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method
