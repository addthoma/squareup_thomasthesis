.class Lcom/squareup/shared/pricing/engine/rules/Collator$Event;
.super Ljava/lang/Object;
.source "Collator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/pricing/engine/rules/Collator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Event"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;,
        Lcom/squareup/shared/pricing/engine/rules/Collator$Event$Modifier;
    }
.end annotation


# instance fields
.field final position:Ljava/math/BigDecimal;

.field final rulesIn:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final rulesOut:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/math/BigDecimal;)V
    .locals 1

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 94
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 97
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->position:Ljava/math/BigDecimal;

    .line 98
    new-instance p1, Ljava/util/TreeMap;

    invoke-direct {p1}, Ljava/util/TreeMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->rulesIn:Ljava/util/Map;

    .line 99
    new-instance p1, Ljava/util/TreeMap;

    invoke-direct {p1}, Ljava/util/TreeMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->rulesOut:Ljava/util/Map;

    return-void

    .line 95
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "position may not be null or negative"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static final synthetic lambda$log$0$Collator$Event()Ljava/lang/Integer;
    .locals 1

    const/4 v0, 0x0

    .line 107
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private log(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 106
    new-instance v0, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;

    invoke-direct {v0, p1, p2}, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    sget-object p1, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$$Lambda$0;->$instance:Lcom/squareup/shared/pricing/engine/util/MapUtils$Defer;

    invoke-static {p3, v0, p1}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->getOrDefault(Ljava/util/Map;Ljava/lang/Object;Lcom/squareup/shared/pricing/engine/util/MapUtils$Defer;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p3, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method logIn(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->rulesIn:Ljava/util/Map;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->log(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method logOut(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->rulesOut:Ljava/util/Map;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->log(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method
