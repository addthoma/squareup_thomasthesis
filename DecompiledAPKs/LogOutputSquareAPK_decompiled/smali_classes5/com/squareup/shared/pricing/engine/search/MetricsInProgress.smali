.class public Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;
.super Ljava/lang/Object;
.source "MetricsInProgress.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;,
        Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ProductSetVisitor;
    }
.end annotation


# instance fields
.field private automatic:Z

.field private final clock:Lcom/squareup/shared/catalog/logging/Clock;

.field private clockState:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

.field private countAmountDiscountRulesApplied:I

.field private countCategoryRulesApplied:I

.field private countComboRulesApplied:I

.field private countComplexRulesApplied:I

.field private countItemRulesApplied:I

.field private countItemizationsInCart:I

.field private countPercentDiscountRulesApplied:I

.field private countQuantityInCart:Ljava/math/BigDecimal;

.field private countRecurringRulesApplied:I

.field private countRulesApplied:I

.field private countRulesLoaded:I

.field private countTimeRestrictedRulesApplied:I

.field private countVolumeRulesApplied:I

.field private currency:Ljava/util/Currency;

.field private largestRivalSet:I

.field private loaderDuration:J

.field private loaderStartMs:J

.field private passcodeUsed:Z

.field private ruleEvaluations:J

.field private searchDuration:J

.field private searchStartMs:J

.field private totalAmountDiscounted:J

.field private totalDuration:J

.field private totalStartMs:J


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/logging/Clock;)V
    .locals 3

    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    .line 170
    sget-object p1, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->NOT_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clockState:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    const/4 p1, 0x0

    .line 171
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->currency:Ljava/util/Currency;

    const-wide/16 v0, 0x0

    .line 173
    iput-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->searchDuration:J

    .line 174
    iput-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->loaderDuration:J

    .line 175
    iput-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->totalDuration:J

    const/4 p1, 0x0

    .line 176
    iput p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countRulesLoaded:I

    .line 177
    iput p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->largestRivalSet:I

    .line 178
    iput-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->ruleEvaluations:J

    .line 179
    iput p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countItemizationsInCart:I

    .line 180
    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    iput-object v2, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countQuantityInCart:Ljava/math/BigDecimal;

    .line 181
    iput p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countRulesApplied:I

    .line 182
    iput p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countPercentDiscountRulesApplied:I

    .line 183
    iput p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countAmountDiscountRulesApplied:I

    .line 184
    iput-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->totalAmountDiscounted:J

    .line 185
    iput p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countCategoryRulesApplied:I

    .line 186
    iput p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countItemRulesApplied:I

    .line 187
    iput p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countVolumeRulesApplied:I

    .line 188
    iput p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countComboRulesApplied:I

    .line 189
    iput p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countTimeRestrictedRulesApplied:I

    .line 190
    iput p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countRecurringRulesApplied:I

    .line 191
    iput p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countComplexRulesApplied:I

    const/4 v0, 0x1

    .line 192
    iput-boolean v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->automatic:Z

    .line 193
    iput-boolean p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->passcodeUsed:Z

    return-void
.end method

.method static final synthetic lambda$recordRuleApplied$0$MetricsInProgress(Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;)V
    .locals 2

    .line 305
    invoke-interface {p2}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->isGreedy()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 306
    invoke-virtual {p0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 308
    :cond_0
    invoke-interface {p2}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->hasProductsAll()Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-interface {p2}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->getProductsAll()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    if-le p0, v1, :cond_1

    .line 309
    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :cond_1
    return-void
.end method

.method private visitAllProductSets(Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ProductSetVisitor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;",
            ">;",
            "Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ProductSetVisitor;",
            ")V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 354
    invoke-interface {p3, p1}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ProductSetVisitor;->visit(Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public commit()Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;
    .locals 30

    move-object/from16 v0, p0

    .line 359
    iget-object v1, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clockState:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    sget-object v2, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->TOTAL_FINISHED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    if-ne v1, v2, :cond_0

    .line 363
    new-instance v1, Lcom/squareup/shared/pricing/models/MonetaryAmount;

    move-object/from16 v20, v1

    iget-wide v2, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->totalAmountDiscounted:J

    iget-object v4, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->currency:Ljava/util/Currency;

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/shared/pricing/models/MonetaryAmount;-><init>(JLjava/util/Currency;)V

    .line 365
    new-instance v1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;

    move-object v3, v1

    iget-wide v4, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->searchDuration:J

    iget-wide v6, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->loaderDuration:J

    iget-wide v8, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->totalDuration:J

    iget v10, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countRulesLoaded:I

    iget v11, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->largestRivalSet:I

    iget-wide v12, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->ruleEvaluations:J

    iget v14, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countItemizationsInCart:I

    iget-object v2, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countQuantityInCart:Ljava/math/BigDecimal;

    .line 373
    invoke-virtual {v2}, Ljava/math/BigDecimal;->intValue()I

    move-result v15

    iget-object v2, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countQuantityInCart:Ljava/math/BigDecimal;

    move-object/from16 v16, v2

    iget v2, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countRulesApplied:I

    move/from16 v17, v2

    iget v2, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countPercentDiscountRulesApplied:I

    move/from16 v18, v2

    iget v2, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countAmountDiscountRulesApplied:I

    move/from16 v19, v2

    iget v2, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countCategoryRulesApplied:I

    move/from16 v21, v2

    iget v2, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countItemRulesApplied:I

    move/from16 v22, v2

    iget v2, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countVolumeRulesApplied:I

    move/from16 v23, v2

    iget v2, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countComboRulesApplied:I

    move/from16 v24, v2

    iget v2, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countTimeRestrictedRulesApplied:I

    move/from16 v25, v2

    iget v2, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countRecurringRulesApplied:I

    move/from16 v26, v2

    iget v2, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countComplexRulesApplied:I

    move/from16 v27, v2

    iget-boolean v2, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->automatic:Z

    move/from16 v28, v2

    iget-boolean v2, v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->passcodeUsed:Z

    move/from16 v29, v2

    invoke-direct/range {v3 .. v29}, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;-><init>(JJJIIJIILjava/math/BigDecimal;IIILcom/squareup/shared/pricing/models/MonetaryAmount;IIIIIIIZZ)V

    return-object v1

    .line 360
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Must start and stop clocks to get durations"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public recordDiscountImproved(J)Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;
    .locals 2

    .line 281
    iget-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->totalAmountDiscounted:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->totalAmountDiscounted:J

    return-object p0
.end method

.method public recordItemizations(Ljava/util/List;)Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;",
            ">;)",
            "Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;"
        }
    .end annotation

    .line 267
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countItemizationsInCart:I

    .line 268
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    iput-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countQuantityInCart:Ljava/math/BigDecimal;

    .line 269
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    .line 270
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countQuantityInCart:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countQuantityInCart:Ljava/math/BigDecimal;

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public recordRuleApplied(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;",
            ">;)",
            "Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;"
        }
    .end annotation

    .line 290
    iget v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countRulesApplied:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countRulesApplied:I

    .line 292
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getDiscountId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 293
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getDiscountId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;

    .line 294
    invoke-interface {p2}, Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;->isPercentageDiscount()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 295
    iget p2, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countPercentDiscountRulesApplied:I

    add-int/2addr p2, v1

    iput p2, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countPercentDiscountRulesApplied:I

    goto :goto_0

    .line 298
    :cond_0
    iget p2, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countAmountDiscountRulesApplied:I

    add-int/2addr p2, v1

    iput p2, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countAmountDiscountRulesApplied:I

    .line 302
    :cond_1
    :goto_0
    new-instance p2, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-direct {p2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 303
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 304
    new-instance v3, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$$Lambda$0;

    invoke-direct {v3, p2, v2}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$$Lambda$0;-><init>(Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 312
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getMatchProductSetId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;

    invoke-direct {p0, v4, p3, v3}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->visitAllProductSets(Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ProductSetVisitor;)V

    .line 313
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getApplyOrMatchProductSetId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 314
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getApplyOrMatchProductSetId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getMatchProductSetId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 315
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getApplyOrMatchProductSetId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;

    invoke-direct {p0, v4, p3, v3}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->visitAllProductSets(Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ProductSetVisitor;)V

    .line 317
    :cond_2
    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p2

    if-eqz p2, :cond_3

    .line 318
    iget p2, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countVolumeRulesApplied:I

    add-int/2addr p2, v1

    iput p2, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countVolumeRulesApplied:I

    .line 320
    :cond_3
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p2

    if-eqz p2, :cond_4

    .line 321
    iget p2, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countComboRulesApplied:I

    add-int/2addr p2, v1

    iput p2, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countComboRulesApplied:I

    .line 324
    :cond_4
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getValidFrom()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_5

    .line 325
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getValidUntil()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_5

    .line 326
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getValidity()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_6

    .line 327
    :cond_5
    iget p2, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countTimeRestrictedRulesApplied:I

    add-int/2addr p2, v1

    iput p2, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countTimeRestrictedRulesApplied:I

    .line 331
    :cond_6
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getValidity()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_7
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_9

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 334
    invoke-interface {p4, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_8

    goto :goto_1

    .line 337
    :cond_8
    invoke-interface {p4, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;

    .line 338
    invoke-interface {p2}, Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;->getRRule()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_7

    const/4 v0, 0x1

    goto :goto_1

    :cond_9
    if-eqz v0, :cond_a

    .line 343
    iget p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countRecurringRulesApplied:I

    add-int/2addr p1, v1

    iput p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countRecurringRulesApplied:I

    :cond_a
    return-object p0
.end method

.method public recordRuleEvaluation()Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;
    .locals 4

    .line 276
    iget-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->ruleEvaluations:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->ruleEvaluations:J

    return-object p0
.end method

.method public recordRuleSet(Lcom/squareup/shared/pricing/engine/rules/RuleSet;)Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;
    .locals 0

    .line 262
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/rules/RuleSet;->getRules()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->countRulesLoaded:I

    return-object p0
.end method

.method public setCurrency(Ljava/util/Currency;)Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->currency:Ljava/util/Currency;

    return-object p0
.end method

.method public startClock()Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clockState:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    sget-object v1, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->NOT_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    if-ne v0, v1, :cond_0

    .line 206
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/logging/Clock;->getUptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->totalStartMs:J

    .line 207
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->TOTAL_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    iput-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clockState:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    return-object p0

    .line 203
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can only start if clock is not yet started"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public startLoader()Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;
    .locals 2

    .line 212
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clockState:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    sget-object v1, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->TOTAL_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    if-ne v0, v1, :cond_0

    .line 216
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/logging/Clock;->getUptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->loaderStartMs:J

    .line 217
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->LOADER_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    iput-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clockState:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    return-object p0

    .line 213
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can only startLoader in TOTAL_STARTED"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public startSearch()Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;
    .locals 2

    .line 232
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clockState:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    sget-object v1, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->LOADER_FINISHED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    if-ne v0, v1, :cond_0

    .line 236
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/logging/Clock;->getUptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->searchStartMs:J

    .line 237
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->SEARCH_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    iput-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clockState:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    return-object p0

    .line 233
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can only startSearch in LOADER_FINISHED"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public stopClock()Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;
    .locals 4

    .line 252
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clockState:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    sget-object v1, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->SEARCH_FINISHED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    if-ne v0, v1, :cond_0

    .line 256
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/logging/Clock;->getUptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->totalStartMs:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->totalDuration:J

    .line 257
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->TOTAL_FINISHED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    iput-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clockState:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    return-object p0

    .line 253
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can only stopClock in SEARCH_FINISHED"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public stopLoader()Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;
    .locals 4

    .line 222
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clockState:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    sget-object v1, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->LOADER_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    if-ne v0, v1, :cond_0

    .line 226
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/logging/Clock;->getUptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->loaderStartMs:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->loaderDuration:J

    .line 227
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->LOADER_FINISHED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    iput-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clockState:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    return-object p0

    .line 223
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can only stopLoader in LOADER_STARTED"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public stopSearch()Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;
    .locals 4

    .line 242
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clockState:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    sget-object v1, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->SEARCH_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    if-ne v0, v1, :cond_0

    .line 246
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/logging/Clock;->getUptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->searchStartMs:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->searchDuration:J

    .line 247
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->SEARCH_FINISHED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    iput-object v0, p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->clockState:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    return-object p0

    .line 243
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can only stopSearch in SEARCH_FINISHED"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
