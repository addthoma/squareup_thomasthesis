.class public Lcom/squareup/shared/pricing/engine/catalog/client/TimePeriodWrapper;
.super Ljava/lang/Object;
.source "TimePeriodWrapper.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;


# instance fields
.field private final catalogTimePeriod:Lcom/squareup/shared/catalog/models/CatalogTimePeriod;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogTimePeriod;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/catalog/client/TimePeriodWrapper;->catalogTimePeriod:Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    return-void
.end method


# virtual methods
.method public activeAt(Ljava/util/Date;Ljava/util/TimeZone;)Z
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/TimePeriodWrapper;->catalogTimePeriod:Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->activeAt(Ljava/util/Date;Ljava/util/TimeZone;)Z

    move-result p1

    return p1
.end method

.method public getRRule()Ljava/lang/String;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/TimePeriodWrapper;->catalogTimePeriod:Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getRRule()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public recurrenceRuleValueKey()Ljava/lang/String;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/TimePeriodWrapper;->catalogTimePeriod:Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->recurrenceRuleValueKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
