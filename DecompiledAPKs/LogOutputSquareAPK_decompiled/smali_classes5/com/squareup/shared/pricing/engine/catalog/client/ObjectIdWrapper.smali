.class public Lcom/squareup/shared/pricing/engine/catalog/client/ObjectIdWrapper;
.super Ljava/lang/Object;
.source "ObjectIdWrapper.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade;


# instance fields
.field private final syncObjectId:Lcom/squareup/api/sync/ObjectId;


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectId;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    invoke-static {p1}, Lcom/squareup/shared/pricing/engine/catalog/client/ObjectIdWrapper;->validate(Lcom/squareup/api/sync/ObjectId;)V

    .line 12
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ObjectIdWrapper;->syncObjectId:Lcom/squareup/api/sync/ObjectId;

    return-void
.end method

.method private static validate(Lcom/squareup/api/sync/ObjectId;)V
    .locals 1

    if-eqz p0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    if-eqz v0, :cond_0

    .line 30
    iget-object p0, p0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    iget-object p0, p0, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    invoke-virtual {p0}, Lcom/squareup/api/items/Type;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;->valueOf(Ljava/lang/String;)Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;

    return-void

    .line 27
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "invalid sync ObjectId"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/ObjectIdWrapper;->syncObjectId:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade$Type;
    .locals 2

    .line 22
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not allowed to use ObjectId::getType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
