.class public Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;
.super Ljava/lang/Object;
.source "ItemizationDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field addedAt:Ljava/util/Date;

.field backingType:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

.field blacklistedDiscounts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field categoryID:Ljava/lang/String;

.field clientServerIds:Lcom/squareup/shared/pricing/models/ClientServerIds;

.field existingAutoDiscounts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field fractional:Z

.field itemID:Ljava/lang/String;

.field quantity:Ljava/math/BigDecimal;

.field unitPrice:Lcom/squareup/shared/pricing/models/MonetaryAmount;

.field variationID:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;->ITEMS_SERVICE_ITEM_VARIATION:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    iput-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->backingType:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    return-void
.end method


# virtual methods
.method public addedAt(Ljava/util/Date;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->addedAt:Ljava/util/Date;

    return-object p0
.end method

.method public backingType(Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->backingType:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    return-object p0
.end method

.method public blacklistedDiscounts(Ljava/util/Set;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;"
        }
    .end annotation

    .line 211
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->blacklistedDiscounts:Ljava/util/Set;

    return-object p0
.end method

.method public build()Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;
    .locals 15

    .line 221
    new-instance v14, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->quantity:Ljava/math/BigDecimal;

    iget-object v2, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->clientServerIds:Lcom/squareup/shared/pricing/models/ClientServerIds;

    iget-object v3, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->variationID:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->itemID:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->categoryID:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->unitPrice:Lcom/squareup/shared/pricing/models/MonetaryAmount;

    iget-object v7, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->addedAt:Ljava/util/Date;

    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    iget-boolean v9, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->fractional:Z

    iget-object v10, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->existingAutoDiscounts:Ljava/util/Set;

    iget-object v11, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->blacklistedDiscounts:Ljava/util/Set;

    iget-object v12, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->backingType:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    const/4 v13, 0x0

    move-object v0, v14

    invoke-direct/range {v0 .. v13}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;-><init>(Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/models/ClientServerIds;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/shared/pricing/models/MonetaryAmount;Ljava/util/Date;Ljava/util/Set;ZLjava/util/Set;Ljava/util/Set;Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$1;)V

    return-object v14
.end method

.method public categoryID(Ljava/lang/String;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->categoryID:Ljava/lang/String;

    return-object p0
.end method

.method public clientServerIds(Lcom/squareup/shared/pricing/models/ClientServerIds;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->clientServerIds:Lcom/squareup/shared/pricing/models/ClientServerIds;

    return-object p0
.end method

.method public existingAutoDiscounts(Ljava/util/Set;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;"
        }
    .end annotation

    .line 206
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->existingAutoDiscounts:Ljava/util/Set;

    return-object p0
.end method

.method public fractional(Z)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;
    .locals 0

    .line 201
    iput-boolean p1, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->fractional:Z

    return-object p0
.end method

.method public itemID(Ljava/lang/String;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->itemID:Ljava/lang/String;

    return-object p0
.end method

.method public quantity(Ljava/math/BigDecimal;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->quantity:Ljava/math/BigDecimal;

    return-object p0
.end method

.method public unitPrice(Lcom/squareup/shared/pricing/models/MonetaryAmount;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->unitPrice:Lcom/squareup/shared/pricing/models/MonetaryAmount;

    return-object p0
.end method

.method public variationID(Ljava/lang/String;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->variationID:Ljava/lang/String;

    return-object p0
.end method
