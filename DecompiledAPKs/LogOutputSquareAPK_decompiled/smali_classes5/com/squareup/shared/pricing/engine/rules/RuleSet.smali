.class public interface abstract Lcom/squareup/shared/pricing/engine/rules/RuleSet;
.super Ljava/lang/Object;
.source "RuleSet.java"


# virtual methods
.method public abstract getEnd()Ljava/util/Date;
.end method

.method public abstract getRules()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getStart()Ljava/util/Date;
.end method
