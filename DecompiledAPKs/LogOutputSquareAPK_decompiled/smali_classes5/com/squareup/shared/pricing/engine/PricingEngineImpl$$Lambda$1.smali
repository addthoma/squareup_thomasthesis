.class final synthetic Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/shared/pricing/engine/catalog/CatalogTaskFacade;


# instance fields
.field private final arg$1:Lcom/squareup/shared/pricing/engine/PricingEngineImpl;

.field private final arg$2:Ljava/util/List;

.field private final arg$3:Ljava/util/Set;

.field private final arg$4:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

.field private final arg$5:Ljava/util/TimeZone;

.field private final arg$6:Z


# direct methods
.method constructor <init>(Lcom/squareup/shared/pricing/engine/PricingEngineImpl;Ljava/util/List;Ljava/util/Set;Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;Ljava/util/TimeZone;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$1;->arg$1:Lcom/squareup/shared/pricing/engine/PricingEngineImpl;

    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$1;->arg$2:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$1;->arg$3:Ljava/util/Set;

    iput-object p4, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$1;->arg$4:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    iput-object p5, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$1;->arg$5:Ljava/util/TimeZone;

    iput-boolean p6, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$1;->arg$6:Z

    return-void
.end method


# virtual methods
.method public perform(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;)Ljava/lang/Object;
    .locals 7

    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$1;->arg$1:Lcom/squareup/shared/pricing/engine/PricingEngineImpl;

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$1;->arg$2:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$1;->arg$3:Ljava/util/Set;

    iget-object v3, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$1;->arg$4:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    iget-object v4, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$1;->arg$5:Ljava/util/TimeZone;

    iget-boolean v5, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$1;->arg$6:Z

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->lambda$applyRules$1$PricingEngineImpl(Ljava/util/List;Ljava/util/Set;Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;Ljava/util/TimeZone;ZLcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;)Lcom/squareup/shared/pricing/engine/PricingEngineResult;

    move-result-object p1

    return-object p1
.end method
