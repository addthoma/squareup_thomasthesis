.class public Lcom/squareup/shared/pricing/engine/search/SearchBuilder;
.super Ljava/lang/Object;
.source "SearchBuilder.java"


# instance fields
.field private final discounts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;",
            ">;"
        }
    .end annotation
.end field

.field private final periods:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;",
            ">;"
        }
    .end annotation
.end field

.field private final productSets:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;",
            ">;"
        }
    .end annotation
.end field

.field private final ruleSet:Lcom/squareup/shared/pricing/engine/rules/RuleSet;

.field private final timePeriodVerificationCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/rules/RuleSet;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;",
            ">;",
            "Lcom/squareup/shared/pricing/engine/rules/RuleSet;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->productSets:Ljava/util/Map;

    .line 39
    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->periods:Ljava/util/Map;

    .line 40
    iput-object p3, p0, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->discounts:Ljava/util/Map;

    .line 41
    iput-object p4, p0, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->ruleSet:Lcom/squareup/shared/pricing/engine/rules/RuleSet;

    .line 42
    iput-object p5, p0, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->timePeriodVerificationCache:Ljava/util/Map;

    return-void
.end method

.method private linkJunction(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/search/JunctionSet;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/search/Rollup;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Lcom/squareup/shared/pricing/engine/search/Source;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;",
            ">;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;",
            ")V"
        }
    .end annotation

    .line 226
    invoke-interface {p5}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 231
    :cond_0
    invoke-interface {p5}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->getFlagAllProducts()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 233
    new-instance p2, Lcom/squareup/shared/pricing/engine/search/DisjunctiveSet;

    invoke-interface {p5}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->getId()Ljava/lang/String;

    move-result-object p4

    .line 234
    invoke-interface {p5}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->getEffectiveMin()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-interface {p5}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->getEffectiveMax()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-direct {p2, p4, v0, v1}, Lcom/squareup/shared/pricing/engine/search/DisjunctiveSet;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)V

    .line 235
    invoke-interface {p3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_6

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/shared/pricing/engine/search/Source;

    .line 236
    invoke-virtual {p2, p4}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->addChild(Lcom/squareup/shared/pricing/engine/search/Provider;)V

    goto :goto_0

    .line 240
    :cond_1
    invoke-interface {p5}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->hasProductsAll()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 241
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/ConjunctiveSet;

    invoke-interface {p5}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->getId()Ljava/lang/String;

    move-result-object v1

    .line 242
    invoke-interface {p5}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->getEffectiveMin()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-interface {p5}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->getEffectiveMax()Ljava/math/BigDecimal;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/shared/pricing/engine/search/ConjunctiveSet;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)V

    goto :goto_1

    .line 243
    :cond_2
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/DisjunctiveSet;

    invoke-interface {p5}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->getId()Ljava/lang/String;

    move-result-object v1

    .line 244
    invoke-interface {p5}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->getEffectiveMin()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-interface {p5}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->getEffectiveMax()Ljava/math/BigDecimal;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/shared/pricing/engine/search/DisjunctiveSet;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)V

    .line 245
    :goto_1
    invoke-interface {p5}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->getProducts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade;

    .line 246
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p4, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 247
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;

    move-object v4, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v4 .. v9}, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->linkJunction(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;)V

    .line 248
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/Provider;

    invoke-virtual {v0, v2}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->addChild(Lcom/squareup/shared/pricing/engine/search/Provider;)V

    goto :goto_2

    .line 249
    :cond_4
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 250
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/Provider;

    invoke-virtual {v0, v2}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->addChild(Lcom/squareup/shared/pricing/engine/search/Provider;)V

    goto :goto_2

    :cond_5
    move-object p2, v0

    .line 254
    :cond_6
    invoke-interface {p5}, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;->getId()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private linkRollups(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/search/Rollup;",
            ">;",
            "Ljava/util/Map<",
            "TT;+",
            "Lcom/squareup/shared/pricing/engine/search/Provider;",
            ">;",
            "Ljava/util/Map<",
            "TT;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 206
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 207
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 208
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v2, Lcom/squareup/shared/pricing/engine/search/Rollup;

    invoke-direct {v2}, Lcom/squareup/shared/pricing/engine/search/Rollup;-><init>()V

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/search/Rollup;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/pricing/engine/search/Provider;

    invoke-virtual {v1, v0}, Lcom/squareup/shared/pricing/engine/search/Rollup;->addChild(Lcom/squareup/shared/pricing/engine/search/Provider;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private ruleActive(Ljava/util/List;Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;Ljava/util/Map;Ljava/util/TimeZone;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;",
            ">;",
            "Ljava/util/TimeZone;",
            ")Z"
        }
    .end annotation

    const/4 v0, 0x1

    if-eqz p1, :cond_5

    .line 168
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_2

    .line 172
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 176
    invoke-interface {p3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 180
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 181
    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;

    invoke-interface {v3}, Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;->recurrenceRuleValueKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getAddedAt()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 184
    iget-object v3, p0, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->timePeriodVerificationCache:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 185
    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->timePeriodVerificationCache:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    goto :goto_1

    .line 187
    :cond_3
    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;

    invoke-virtual {p2}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getAddedAt()Ljava/util/Date;

    move-result-object v3

    invoke-interface {v1, v3, p4}, Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;->activeAt(Ljava/util/Date;Ljava/util/TimeZone;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 188
    iget-object v3, p0, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->timePeriodVerificationCache:Ljava/util/Map;

    invoke-interface {v3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    return v0

    :cond_4
    const/4 p1, 0x0

    return p1

    :cond_5
    :goto_2
    return v0
.end method


# virtual methods
.method public build(Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;Ljava/util/List;Ljava/util/TimeZone;Lcom/squareup/shared/catalog/logging/Clock;Ljava/util/Map;)Lcom/squareup/shared/pricing/engine/search/Search;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;",
            ">;",
            "Ljava/util/TimeZone;",
            "Lcom/squareup/shared/catalog/logging/Clock;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            ">;>;)",
            "Lcom/squareup/shared/pricing/engine/search/Search;"
        }
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v7, p5

    .line 56
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 57
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 63
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 64
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 66
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    .line 68
    invoke-virtual {v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getBackingType()Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    move-result-object v5

    sget-object v8, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;->ITEMS_SERVICE_ITEM_VARIATION:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    if-eq v5, v8, :cond_1

    goto :goto_0

    .line 71
    :cond_1
    invoke-virtual {v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getClientServerIds()Lcom/squareup/shared/pricing/models/ClientServerIds;

    move-result-object v5

    .line 72
    new-instance v8, Lcom/squareup/shared/pricing/engine/search/Source;

    invoke-direct {v8, v4}, Lcom/squareup/shared/pricing/engine/search/Source;-><init>(Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;)V

    invoke-interface {v10, v5, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    invoke-virtual {v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getVariationID()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v0, v5, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-virtual {v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getVariationID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getItemID()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v5, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    invoke-virtual {v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getCategoryID()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 76
    invoke-virtual {v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getItemID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getCategoryID()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 80
    :cond_2
    invoke-direct {v6, v12, v10, v0}, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->linkRollups(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    .line 81
    invoke-direct {v6, v12, v12, v1}, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->linkRollups(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    .line 82
    invoke-direct {v6, v12, v12, v2}, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->linkRollups(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    .line 87
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 88
    iget-object v0, v6, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->productSets:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 89
    iget-object v4, v6, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->productSets:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;

    move-object/from16 v0, p0

    move-object v1, v11

    move-object v2, v12

    move-object v3, v10

    invoke-direct/range {v0 .. v5}, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->linkJunction(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;)V

    goto :goto_1

    .line 92
    :cond_3
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 93
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 95
    iget-object v0, v6, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->ruleSet:Lcom/squareup/shared/pricing/engine/rules/RuleSet;

    invoke-interface {v0}, Lcom/squareup/shared/pricing/engine/rules/RuleSet;->getRules()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    .line 96
    invoke-interface {v14}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getDiscountId()Ljava/lang/String;

    move-result-object v15

    .line 100
    iget-object v0, v6, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->discounts:Ljava/util/Map;

    invoke-interface {v0, v15}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_2

    .line 104
    :cond_4
    iget-object v0, v6, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->discounts:Ljava/util/Map;

    invoke-interface {v0, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;

    .line 108
    invoke-interface {v5}, Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;->getAmount()Lcom/squareup/shared/pricing/models/MonetaryAmount;

    move-result-object v0

    if-nez v0, :cond_5

    invoke-interface {v5}, Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;->getPercentage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    goto :goto_2

    .line 112
    :cond_5
    invoke-interface {v14}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getMatchProductSetId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    .line 113
    invoke-interface {v14}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getApplyProductSetId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    .line 114
    invoke-interface {v14}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getApplyProductSetId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    move-object v4, v0

    goto :goto_3

    :cond_6
    move-object v4, v1

    .line 115
    :goto_3
    invoke-interface {v14}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getExcludeProductSetId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 116
    invoke-interface {v14}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getExcludeProductSetId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    move-object v3, v0

    goto :goto_4

    :cond_7
    move-object v3, v1

    .line 118
    :goto_4
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/SearchBuilder$1;->$SwitchMap$com$squareup$shared$pricing$engine$catalog$models$PricingRuleFacade$DiscountTargetScope:[I

    invoke-interface {v14}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getDiscountTargetScope()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/16 v16, 0x0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_9

    const/4 v3, 0x2

    if-ne v0, v3, :cond_8

    .line 125
    invoke-interface {v14}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lcom/squareup/shared/pricing/engine/search/WholePurchaseRuleNode;

    new-instance v4, Ljava/util/HashSet;

    move-object/from16 v1, p2

    invoke-direct {v4, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-direct {v3, v14, v5, v2, v4}, Lcom/squareup/shared/pricing/engine/search/WholePurchaseRuleNode;-><init>(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;Lcom/squareup/shared/pricing/engine/search/JunctionSet;Ljava/util/Set;)V

    invoke-interface {v9, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v18, v13

    const/16 v17, 0x1

    move-object v13, v5

    goto :goto_5

    .line 129
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 130
    invoke-interface {v14}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getDiscountTargetScope()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    move-result-object v2

    aput-object v2, v1, v16

    const-string v2, "Unknown discount target scope %s"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    move-object/from16 v1, p2

    const/16 v17, 0x1

    .line 120
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;

    move-object/from16 p4, v0

    move-object v1, v14

    move-object/from16 v18, v13

    move-object v13, v5

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;-><init>(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Lcom/squareup/shared/pricing/engine/search/JunctionSet;Lcom/squareup/shared/pricing/engine/search/JunctionSet;Lcom/squareup/shared/pricing/engine/search/JunctionSet;Ljava/util/Map;)V

    .line 121
    invoke-interface {v14}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p4

    invoke-interface {v9, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    :goto_5
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_a
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    .line 135
    invoke-interface {v7, v15}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 136
    invoke-interface {v7, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getClientServerIds()Lcom/squareup/shared/pricing/models/ClientServerIds;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v2, 0x1

    goto :goto_7

    :cond_b
    const/4 v2, 0x0

    .line 138
    :goto_7
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getBlacklistedDiscounts()Ljava/util/Set;

    move-result-object v3

    if-eqz v3, :cond_c

    .line 140
    invoke-interface {v3, v15}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    or-int/2addr v2, v3

    .line 143
    :cond_c
    invoke-interface {v14}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getValidity()Ljava/util/List;

    move-result-object v3

    iget-object v4, v6, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->periods:Ljava/util/Map;

    move-object/from16 v5, p3

    invoke-direct {v6, v3, v1, v4, v5}, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->ruleActive(Ljava/util/List;Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;Ljava/util/Map;Ljava/util/TimeZone;)Z

    move-result v3

    .line 146
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getUnitPrice()Lcom/squareup/shared/pricing/models/MonetaryAmount;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/shared/pricing/models/MonetaryAmount;->getAmount()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v13, v4, v5}, Lcom/squareup/shared/pricing/engine/search/helpers/DiscountHelper;->getDiscountValueForAmount(Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;J)J

    move-result-wide v4

    .line 148
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getClientServerIds()Lcom/squareup/shared/pricing/models/ClientServerIds;

    move-result-object v1

    invoke-interface {v10, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/search/Source;

    if-eqz v1, :cond_a

    if-eqz v3, :cond_d

    if-nez v2, :cond_d

    const/4 v2, 0x1

    goto :goto_8

    :cond_d
    const/4 v2, 0x0

    .line 150
    :goto_8
    invoke-virtual {v1, v14, v4, v5, v2}, Lcom/squareup/shared/pricing/engine/search/Source;->registerRule(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;JZ)V

    goto :goto_6

    :cond_e
    move-object/from16 v13, v18

    goto/16 :goto_2

    .line 155
    :cond_f
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/Search;

    iget-object v13, v6, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->productSets:Ljava/util/Map;

    iget-object v14, v6, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->periods:Ljava/util/Map;

    iget-object v15, v6, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->discounts:Ljava/util/Map;

    move-object v7, v0

    move-object/from16 v8, p1

    invoke-direct/range {v7 .. v15}, Lcom/squareup/shared/pricing/engine/search/Search;-><init>(Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    return-object v0
.end method
