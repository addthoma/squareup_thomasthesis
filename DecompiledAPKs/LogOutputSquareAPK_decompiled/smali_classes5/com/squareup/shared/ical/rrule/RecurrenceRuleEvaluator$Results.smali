.class public Lcom/squareup/shared/ical/rrule/RecurrenceRuleEvaluator$Results;
.super Ljava/lang/Object;
.source "RecurrenceRuleEvaluator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/ical/rrule/RecurrenceRuleEvaluator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Results"
.end annotation


# instance fields
.field private evaluatedAllInstances:Z

.field private instants:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/Date;",
            ">;Z)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleEvaluator$Results;->instants:Ljava/util/List;

    .line 29
    iput-boolean p2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleEvaluator$Results;->evaluatedAllInstances:Z

    return-void
.end method


# virtual methods
.method public getEvaluatedAllInstants()Z
    .locals 1

    .line 39
    iget-boolean v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleEvaluator$Results;->evaluatedAllInstances:Z

    return v0
.end method

.method public getInstants()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleEvaluator$Results;->instants:Ljava/util/List;

    return-object v0
.end method
