.class public Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
.super Ljava/lang/Object;
.source "RecurrenceRuleImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private byDay:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;",
            ">;"
        }
    .end annotation
.end field

.field private byMonth:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private byMonthDay:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private bySetPos:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private byWeekNo:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private byYearDay:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private count:Ljava/lang/Integer;

.field private dtStart:Ljava/util/Date;

.field private exDates:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

.field private interval:I

.field private timeZone:Ljava/util/TimeZone;

.field private until:Ljava/util/Date;

.field private wkst:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 129
    iput v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->interval:I

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;
    .locals 17

    move-object/from16 v0, p0

    .line 210
    iget-object v1, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->dtStart:Ljava/util/Date;

    if-eqz v1, :cond_4

    .line 213
    iget-object v1, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->timeZone:Ljava/util/TimeZone;

    if-eqz v1, :cond_3

    .line 216
    iget-object v1, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->wkst:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    if-nez v1, :cond_0

    .line 217
    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->MO:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    iput-object v1, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->wkst:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    .line 219
    :cond_0
    iget-object v1, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->exDates:Ljava/util/Set;

    if-nez v1, :cond_1

    .line 220
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->exDates:Ljava/util/Set;

    .line 222
    :cond_1
    iget-object v7, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    if-eqz v7, :cond_2

    .line 225
    new-instance v1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;

    iget-object v3, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->dtStart:Ljava/util/Date;

    iget-object v4, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->timeZone:Ljava/util/TimeZone;

    iget-object v5, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->wkst:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    iget-object v6, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->exDates:Ljava/util/Set;

    iget v8, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->interval:I

    iget-object v9, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->count:Ljava/lang/Integer;

    iget-object v10, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->until:Ljava/util/Date;

    iget-object v11, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->byMonth:Ljava/util/Set;

    iget-object v12, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->byWeekNo:Ljava/util/Set;

    iget-object v13, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->byYearDay:Ljava/util/Set;

    iget-object v14, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->byMonthDay:Ljava/util/Set;

    iget-object v15, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->byDay:Ljava/util/Set;

    iget-object v2, v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->bySetPos:Ljava/util/Set;

    move-object/from16 v16, v2

    move-object v2, v1

    invoke-direct/range {v2 .. v16}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;-><init>(Ljava/util/Date;Ljava/util/TimeZone;Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;Ljava/util/Set;Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;ILjava/lang/Integer;Ljava/util/Date;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    return-object v1

    .line 223
    :cond_2
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Frequency must be supplied."

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 214
    :cond_3
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Time zone must be supplied."

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 211
    :cond_4
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "DTStart must be supplied."

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setByDay(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;",
            ">;)",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;"
        }
    .end annotation

    .line 195
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->byDay:Ljava/util/Set;

    return-object p0
.end method

.method public setByMonth(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;"
        }
    .end annotation

    .line 175
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->byMonth:Ljava/util/Set;

    return-object p0
.end method

.method public setByMonthDay(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;"
        }
    .end annotation

    .line 190
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->byMonthDay:Ljava/util/Set;

    return-object p0
.end method

.method public setBySetPos(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;"
        }
    .end annotation

    .line 200
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->bySetPos:Ljava/util/Set;

    return-object p0
.end method

.method public setByWeekNo(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;"
        }
    .end annotation

    .line 180
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->byWeekNo:Ljava/util/Set;

    return-object p0
.end method

.method public setByYearDay(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;"
        }
    .end annotation

    .line 185
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->byYearDay:Ljava/util/Set;

    return-object p0
.end method

.method public setCount(Ljava/lang/Integer;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->count:Ljava/lang/Integer;

    return-object p0
.end method

.method public setDtStart(Ljava/util/Date;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->dtStart:Ljava/util/Date;

    return-object p0
.end method

.method public setExDates(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/util/Date;",
            ">;)",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;"
        }
    .end annotation

    .line 155
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->exDates:Ljava/util/Set;

    return-object p0
.end method

.method public setFrequency(Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->frequency:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    return-object p0
.end method

.method public setInterval(I)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
    .locals 0

    .line 165
    iput p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->interval:I

    return-object p0
.end method

.method public setTimeZone(Ljava/util/TimeZone;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->timeZone:Ljava/util/TimeZone;

    return-object p0
.end method

.method public setUntil(Ljava/util/Date;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->until:Ljava/util/Date;

    return-object p0
.end method

.method public setWkst(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->wkst:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    return-object p0
.end method
