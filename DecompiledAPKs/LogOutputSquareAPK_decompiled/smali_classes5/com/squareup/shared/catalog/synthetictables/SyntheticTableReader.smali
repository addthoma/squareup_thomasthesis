.class public interface abstract Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;
.super Ljava/lang/Object;
.source "SyntheticTableReader.java"


# virtual methods
.method public abstract onRegistered(Lcom/squareup/shared/sql/DatabaseHelper;)V
.end method

.method public abstract sourceTables()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;"
        }
    .end annotation
.end method
