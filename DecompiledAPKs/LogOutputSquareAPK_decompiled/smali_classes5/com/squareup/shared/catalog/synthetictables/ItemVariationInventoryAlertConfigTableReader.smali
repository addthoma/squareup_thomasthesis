.class public Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader;
.super Ljava/lang/Object;
.source "ItemVariationInventoryAlertConfigTableReader.java"

# interfaces
.implements Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;
    }
.end annotation


# instance fields
.field private helper:Lcom/squareup/shared/sql/DatabaseHelper;

.field private final sourceTables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader;->sourceTables:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public allVariationInventoryAlertConfigs(J)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfig;",
            ">;"
        }
    .end annotation

    move-object/from16 v1, p0

    .line 76
    iget-object v0, v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 77
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-wide/from16 v3, p1

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    .line 80
    sget-object v2, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;->ALL_VARIATION_ALERT_CONFIGS_UPDATED_AFTER_TIMESTAMP:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;

    .line 81
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v4}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object v2

    .line 83
    :try_start_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 84
    :goto_0
    invoke-interface {v2}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 85
    invoke-interface {v2, v5}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 86
    invoke-interface {v2, v3}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v4, 0x2

    .line 87
    invoke-interface {v2, v4}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v4, 0x3

    .line 88
    invoke-interface {v2, v4}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result v4

    if-ne v4, v3, :cond_0

    const/4 v10, 0x1

    goto :goto_1

    :cond_0
    const/4 v10, 0x0

    :goto_1
    const/4 v4, 0x4

    .line 89
    invoke-interface {v2, v4}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result v4

    if-ne v4, v3, :cond_1

    const/4 v11, 0x1

    goto :goto_2

    :cond_1
    const/4 v11, 0x0

    :goto_2
    const/4 v4, 0x5

    .line 90
    invoke-interface {v2, v4}, Lcom/squareup/shared/sql/SQLCursor;->getLong(I)J

    move-result-wide v12

    const/4 v4, 0x6

    .line 91
    invoke-interface {v2, v4}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result v4

    if-ne v4, v3, :cond_2

    const/4 v14, 0x1

    goto :goto_3

    :cond_2
    const/4 v14, 0x0

    :goto_3
    const/4 v4, 0x7

    .line 92
    invoke-interface {v2, v4}, Lcom/squareup/shared/sql/SQLCursor;->getLong(I)J

    move-result-wide v15

    .line 93
    new-instance v4, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfig;

    move-object v6, v4

    invoke-direct/range {v6 .. v16}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfig;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZJZJ)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 101
    :cond_3
    invoke-interface {v2}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw v0
.end method

.method public onRegistered(Lcom/squareup/shared/sql/DatabaseHelper;)V
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    return-void
.end method

.method public sourceTables()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;"
        }
    .end annotation

    .line 70
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTableReader;->sourceTables:Ljava/util/List;

    return-object v0
.end method
