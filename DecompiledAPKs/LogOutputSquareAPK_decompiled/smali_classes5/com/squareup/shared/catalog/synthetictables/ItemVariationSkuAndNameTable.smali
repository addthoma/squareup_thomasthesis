.class public Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable;
.super Ljava/lang/Object;
.source "ItemVariationSkuAndNameTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;
    }
.end annotation


# static fields
.field static final COLUMN_ITEM_ID:Ljava/lang/String; = "item_id"

.field static final COLUMN_SKU:Ljava/lang/String; = "sku"

.field static final COLUMN_VARIATION_ID:Ljava/lang/String; = "variation_id"

.field static final COLUMN_VARIATION_SEARCH_WORDS:Ljava/lang/String; = "variation_search_words"

.field static final NAME:Ljava/lang/String; = "variation_sku_name"

.field private static final VERSION:J = 0x2L


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private deleteByVariationId(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V
    .locals 1

    .line 152
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->DELETE_BY_VARIATION_ID:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 153
    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 154
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method private upsert(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V
    .locals 2

    .line 142
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->searchKeywords()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/StringUtils;->joinWordsWithLeadingWhitespace(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 143
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->UPSERT_VARIATION:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 144
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getSku()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 145
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getItemId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 146
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 147
    invoke-virtual {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 148
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method


# virtual methods
.method public applyDeletes(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/DeletedCatalogObjects;)V
    .locals 1

    .line 134
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedVariationItemIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    iget-object p2, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedVariationItemIds:Ljava/util/Map;

    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 136
    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable;->deleteByVariationId(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public applyUpdates(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/UpdatedCatalogObjects;ZZ)V
    .locals 0

    .line 122
    iget-object p2, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedVariationsByItemId:Ljava/util/Map;

    .line 124
    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result p3

    if-nez p3, :cond_1

    .line 125
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/List;

    .line 126
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_0

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 127
    invoke-direct {p0, p1, p4}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable;->upsert(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public create(Lcom/squareup/shared/sql/SQLDatabase;)V
    .locals 1

    .line 114
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->CREATE:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 115
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->CREATE_ITEM_ID_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 116
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->CREATE_SKU_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 117
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->CREATE_VARIATION_NAME_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public tableName()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "variation_sku_name"

    return-object v0
.end method

.method public tableVersion()J
    .locals 2

    const-wide/16 v0, 0x2

    return-wide v0
.end method
