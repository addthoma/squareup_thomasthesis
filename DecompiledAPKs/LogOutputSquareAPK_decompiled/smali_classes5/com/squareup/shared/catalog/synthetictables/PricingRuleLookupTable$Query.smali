.class public final enum Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;
.super Ljava/lang/Enum;
.source "PricingRuleLookupTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/HasQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;",
        ">;",
        "Lcom/squareup/shared/catalog/HasQuery;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

.field public static final enum CREATE:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

.field public static final enum CREATE_ENDS_AT_INDEX:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

.field public static final enum CREATE_STARTS_AT_INDEX:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

.field public static final enum DELETE_BY_PRICING_RULE_ID:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

.field public static final enum INSERT_OR_REPLACE:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;


# instance fields
.field private final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 51
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    const-string v1, "CREATE TABLE {table} ({pricing_rule_id} TEXT NOT NULL, {starts_at} INTEGER, {ends_at} INTEGER, PRIMARY KEY ({pricing_rule_id}))"

    .line 52
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "pricing_rule_lookup"

    const-string v3, "table"

    .line 54
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v4, "pricing_rule_id"

    .line 55
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v5, "starts_at"

    .line 56
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v6, "ends_at"

    .line 57
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 58
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 59
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x0

    const-string v8, "CREATE"

    invoke-direct {v0, v8, v7, v1}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->CREATE:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    .line 61
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    const-string v1, "CREATE INDEX {idx} ON {table} ({starts_at})"

    .line 62
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v8, "idx"

    const-string v9, "index_pricing_rule_lookup_on_starts_at"

    .line 63
    invoke-virtual {v1, v8, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 64
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 65
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 66
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 67
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v9, 0x1

    const-string v10, "CREATE_STARTS_AT_INDEX"

    invoke-direct {v0, v10, v9, v1}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->CREATE_STARTS_AT_INDEX:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    .line 69
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    const-string v1, "CREATE INDEX {idx} ON {table} ({ends_at})"

    .line 70
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v10, "index_pricing_rule_lookup_on_ends_at"

    .line 71
    invoke-virtual {v1, v8, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 72
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 73
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 74
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 75
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v8, 0x2

    const-string v10, "CREATE_ENDS_AT_INDEX"

    invoke-direct {v0, v10, v8, v1}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->CREATE_ENDS_AT_INDEX:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    .line 77
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    const-string v1, "INSERT OR REPLACE INTO {table} ({pricing_rule_id}, {starts_at}, {ends_at}) VALUES (?, ?, ?)"

    .line 78
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 79
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 80
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 81
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 82
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 83
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 84
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x3

    const-string v6, "INSERT_OR_REPLACE"

    invoke-direct {v0, v6, v5, v1}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->INSERT_OR_REPLACE:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    .line 86
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    const-string v1, "DELETE FROM {table} WHERE {pricing_rule_id} = ?"

    .line 87
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 88
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 89
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 90
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 91
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    const-string v3, "DELETE_BY_PRICING_RULE_ID"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->DELETE_BY_PRICING_RULE_ID:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    .line 50
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->CREATE:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->CREATE_STARTS_AT_INDEX:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->CREATE_ENDS_AT_INDEX:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->INSERT_OR_REPLACE:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->DELETE_BY_PRICING_RULE_ID:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 95
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 96
    iput-object p3, p0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->query:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;
    .locals 1

    .line 50
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;
    .locals 1

    .line 50
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    return-object v0
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->query:Ljava/lang/String;

    return-object v0
.end method
