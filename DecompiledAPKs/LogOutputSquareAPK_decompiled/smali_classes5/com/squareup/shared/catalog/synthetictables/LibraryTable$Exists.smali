.class Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Exists;
.super Ljava/lang/Object;
.source "LibraryTable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/synthetictables/LibraryTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Exists"
.end annotation


# instance fields
.field public final imageId:Ljava/lang/String;

.field public final imageUrl:Ljava/lang/String;

.field public final name:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 904
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905
    iput-object p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Exists;->imageId:Ljava/lang/String;

    .line 906
    iput-object p2, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Exists;->imageUrl:Ljava/lang/String;

    .line 907
    iput-object p3, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTable$Exists;->name:Ljava/lang/String;

    return-void
.end method
