.class public interface abstract Lcom/squareup/shared/catalog/CatalogStore;
.super Ljava/lang/Object;
.source "CatalogStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/CatalogStore$Factory;,
        Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;
    }
.end annotation


# virtual methods
.method public abstract applyCogsVersionBatch(Ljava/util/List;Ljava/util/List;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;Z)V"
        }
    .end annotation
.end method

.method public abstract applyConnectV2Batch(Ljava/util/List;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract beginVersionSync(JLjava/util/List;Ljava/util/List;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;)Z"
        }
    .end annotation
.end method

.method public abstract close()V
.end method

.method public abstract count(Lcom/squareup/shared/catalog/models/CatalogObjectType;)I
.end method

.method public abstract countItemsWithTypes(Ljava/util/List;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)I"
        }
    .end annotation
.end method

.method public abstract countRelatedItems(Lcom/squareup/shared/catalog/ManyToMany$Lookup;Ljava/util/List;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/ManyToMany$Lookup<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "*>;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)I"
        }
    .end annotation
.end method

.method public abstract deletePendingWriteRequest(J)V
.end method

.method public abstract endVersionSync(JZLjava/util/Date;)V
.end method

.method public abstract findReferences(Lcom/squareup/api/items/Type;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/Type;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findReferrers(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract readAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Lcom/squareup/shared/sql/SQLCursor;
.end method

.method public abstract readAllConnectV2Objects(Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;)Lcom/squareup/shared/sql/SQLCursor;
.end method

.method public abstract readAllSyncedConnectV2ObjectTypes()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end method

.method public abstract readAndParseAll(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            ")",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract readAndParseAllConnectV2Objects(Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">(",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;",
            ")",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract readById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract readByIdOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract readByIds(Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract readByTokenOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract readByTokens(Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract readClientStateSeq()J
.end method

.method public abstract readCogsAppliedServerVersion()J
.end method

.method public abstract readConnectV2ObjectById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract readConnectV2ObjectsByIds(Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract readIdsForAllObjectsOfType(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract readLastSyncTimestamp()Ljava/util/Date;
.end method

.method public abstract readPendingWriteRequests()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/PendingWriteRequest;",
            ">;"
        }
    .end annotation
.end method

.method public abstract readSessionId()Ljava/lang/Long;
.end method

.method public abstract readVersionSyncIncomplete()Z
.end method

.method public abstract rebuildSyntheticTables(Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;)V
.end method

.method public abstract requiresSyntheticTableRebuild()Z
.end method

.method public abstract resolve(Lcom/squareup/shared/catalog/ManyToMany$Lookup;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/ManyToMany$Lookup<",
            "TT;*>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract resolve(Lcom/squareup/shared/catalog/ManyToMany$Lookup;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/ManyToMany$Lookup<",
            "TT;*>;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract resolveIdsForRelationship(Lcom/squareup/shared/catalog/ManyToMany$Lookup;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/ManyToMany$Lookup<",
            "TT;*>;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract resolveMemberships(Lcom/squareup/shared/catalog/ManyToMany$Lookup;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/ManyToMany$Lookup<",
            "TT;*>;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract resolveOuter(Lcom/squareup/shared/catalog/ManyToMany$Lookup;)Lcom/squareup/shared/catalog/TypedCursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;J:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/ManyToMany$Lookup<",
            "TT;TJ;>;)",
            "Lcom/squareup/shared/catalog/TypedCursor<",
            "Lcom/squareup/shared/catalog/Related<",
            "TT;TJ;>;>;"
        }
    .end annotation
.end method

.method public abstract resolveRelatedObjectIds(Lcom/squareup/shared/catalog/ManyToMany$Lookup;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/ManyToMany$Lookup<",
            "TT;*>;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract writeAndEnqueue(Ljava/util/Collection;Ljava/util/Collection;Lcom/squareup/api/rpc/Request;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Lcom/squareup/api/rpc/Request;",
            ")V"
        }
    .end annotation
.end method

.method public abstract writeClientStateSeq(J)V
.end method

.method public abstract writeConnectV2Objects(Ljava/util/Collection;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;",
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract writeSessionId(J)V
.end method
