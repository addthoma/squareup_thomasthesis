.class Lcom/squareup/shared/catalog/sync/CatalogSync$4;
.super Ljava/lang/Object;
.source "CatalogSync.java"

# interfaces
.implements Lcom/squareup/shared/catalog/sync/SyncCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/catalog/sync/CatalogSync;->foregroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;JLcom/squareup/shared/catalog/sync/SyncCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/sync/SyncCallback<",
        "Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

.field final synthetic val$callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

.field final synthetic val$itemBatchSize:J

.field final synthetic val$maxAge:Lcom/squareup/shared/catalog/utils/ElapsedTime;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/utils/ElapsedTime;J)V
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$4;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$4;->val$callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    iput-object p3, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$4;->val$maxAge:Lcom/squareup/shared/catalog/utils/ElapsedTime;

    iput-wide p4, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$4;->val$itemBatchSize:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;",
            ">;)V"
        }
    .end annotation

    .line 197
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;

    if-nez p1, :cond_0

    .line 199
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$4;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$200(Lcom/squareup/shared/catalog/sync/CatalogSync;)Ljava/util/concurrent/Executor;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$4;->val$callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncSucceed(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$4;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$4;->val$maxAge:Lcom/squareup/shared/catalog/utils/ElapsedTime;

    invoke-static {v0, v1, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$600(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$4;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    iget-wide v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$4;->val$itemBatchSize:J

    iget-object v3, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$4;->val$callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {v0, p1, v1, v2, v3}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$700(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;JLcom/squareup/shared/catalog/sync/SyncCallback;)V

    goto :goto_0

    .line 205
    :cond_1
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$4;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$200(Lcom/squareup/shared/catalog/sync/CatalogSync;)Ljava/util/concurrent/Executor;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$4;->val$callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncSucceed(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    :try_end_0
    .catch Lcom/squareup/shared/catalog/CatalogException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 208
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$4;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$200(Lcom/squareup/shared/catalog/sync/CatalogSync;)Ljava/util/concurrent/Executor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$4;->val$callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {v0, v1, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 213
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " => "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$4;->val$callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
