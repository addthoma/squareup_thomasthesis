.class Lcom/squareup/shared/catalog/sync/CatalogSync$6;
.super Ljava/lang/Object;
.source "CatalogSync.java"

# interfaces
.implements Lcom/squareup/shared/catalog/sync/SyncCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/catalog/sync/CatalogSync;->shouldForegroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/CatalogCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/sync/SyncCallback<",
        "Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

.field final synthetic val$callback:Lcom/squareup/shared/catalog/CatalogCallback;

.field final synthetic val$maxAge:Lcom/squareup/shared/catalog/utils/ElapsedTime;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$6;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$6;->val$maxAge:Lcom/squareup/shared/catalog/utils/ElapsedTime;

    iput-object p3, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$6;->val$callback:Lcom/squareup/shared/catalog/CatalogCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    .line 231
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;

    if-eqz p1, :cond_1

    .line 232
    iget-boolean v1, p1, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->requiresSyntheticTableRebuild:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$6;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$6;->val$maxAge:Lcom/squareup/shared/catalog/utils/ElapsedTime;

    .line 234
    invoke-static {v1, v2, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$600(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 235
    :goto_1
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$6;->val$callback:Lcom/squareup/shared/catalog/CatalogCallback;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/CatalogResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/CatalogResult;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/shared/catalog/CatalogCallback;->call(Lcom/squareup/shared/catalog/CatalogResult;)V
    :try_end_0
    .catch Lcom/squareup/shared/catalog/CatalogException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 237
    :catch_0
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$6;->val$callback:Lcom/squareup/shared/catalog/CatalogCallback;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/CatalogResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/CatalogResult;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/CatalogCallback;->call(Lcom/squareup/shared/catalog/CatalogResult;)V

    :goto_2
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " => "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$6;->val$callback:Lcom/squareup/shared/catalog/CatalogCallback;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
