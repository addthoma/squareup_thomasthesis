.class Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback$1;
.super Lcom/squareup/shared/catalog/sync/SyncTask;
.source "CatalogSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/sync/SyncTask<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback;

.field final synthetic val$sessionId:J


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback;J)V
    .locals 0

    .line 650
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback$1;->this$1:Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback;

    iput-wide p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback$1;->val$sessionId:J

    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/SyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public perform(Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 652
    iget-wide v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback$1;->val$sessionId:J

    invoke-virtual {p1, v0, v1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->writeSessionId(J)V

    const/4 p1, 0x0

    return-object p1
.end method
