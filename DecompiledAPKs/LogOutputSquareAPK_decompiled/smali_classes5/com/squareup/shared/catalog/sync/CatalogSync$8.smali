.class Lcom/squareup/shared/catalog/sync/CatalogSync$8;
.super Ljava/lang/Object;
.source "CatalogSync.java"

# interfaces
.implements Lcom/squareup/shared/catalog/sync/SyncCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/catalog/sync/CatalogSync;->sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/sync/SyncCallback<",
        "Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

.field final synthetic val$callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

.field final synthetic val$syncOnlyIfPendingWrites:Z


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSync;ZLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 0

    .line 304
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$8;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    iput-boolean p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$8;->val$syncOnlyIfPendingWrites:Z

    iput-object p3, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$8;->val$callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;",
            ">;)V"
        }
    .end annotation

    .line 307
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;

    if-eqz p1, :cond_2

    .line 308
    iget-boolean v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$8;->val$syncOnlyIfPendingWrites:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->writeRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$8;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$1200(Lcom/squareup/shared/catalog/sync/CatalogSync;)Lcom/squareup/shared/catalog/CatalogStoreProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->isCloseEnqueued()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 324
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$8;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$200(Lcom/squareup/shared/catalog/sync/CatalogSync;)Ljava/util/concurrent/Executor;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$8;->val$callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncSucceed(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void

    .line 327
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$8;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    const-wide/16 v1, 0x0

    iget-object v3, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$8;->val$callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {v0, p1, v1, v2, v3}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$700(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;JLcom/squareup/shared/catalog/sync/SyncCallback;)V

    goto :goto_1

    .line 311
    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$8;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$200(Lcom/squareup/shared/catalog/sync/CatalogSync;)Ljava/util/concurrent/Executor;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$8;->val$callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncSucceed(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    .line 329
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$8;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/CatalogSync;->access$200(Lcom/squareup/shared/catalog/sync/CatalogSync;)Ljava/util/concurrent/Executor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$8;->val$callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {v0, v1, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 334
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " => "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$8;->val$callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
