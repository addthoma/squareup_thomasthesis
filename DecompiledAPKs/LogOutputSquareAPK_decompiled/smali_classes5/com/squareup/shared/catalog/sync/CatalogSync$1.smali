.class Lcom/squareup/shared/catalog/sync/CatalogSync$1;
.super Lcom/squareup/shared/catalog/sync/SyncTask;
.source "CatalogSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/catalog/sync/CatalogSync;->resumeLock(Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/sync/SyncTask<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

.field final synthetic val$lock:Lcom/squareup/shared/catalog/sync/CatalogSyncLock;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$1;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$1;->val$lock:Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/SyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public perform(Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 141
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$1;->val$lock:Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->lock(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method
