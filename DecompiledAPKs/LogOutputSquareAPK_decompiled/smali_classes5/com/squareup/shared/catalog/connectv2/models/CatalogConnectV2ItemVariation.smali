.class public final Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;
.super Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;
.source "CatalogConnectV2ItemVariation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation$Builder;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V
    .locals 1

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    .line 27
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    const-string v0, "Item variation data"

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public getAllItemOptionValues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation;",
            ">;"
        }
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->item_option_values:Ljava/util/List;

    return-object v0
.end method

.method public getMeasurementUnitId()Ljava/lang/String;
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->measurement_unit_id:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPriceDescriptionForLocation(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v0, "Location Id"

    .line 55
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 57
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->location_overrides:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;

    .line 58
    iget-object v2, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->location_id:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 59
    iget-object p1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_description:Ljava/lang/String;

    if-nez p1, :cond_1

    goto :goto_0

    .line 63
    :cond_1
    iget-object p1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_description:Ljava/lang/String;

    return-object p1

    .line 67
    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->price_description:Ljava/lang/String;

    return-object p1
.end method

.method public getPriceForLocation(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/Money;
    .locals 3

    const-string v0, "Location Id"

    .line 39
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 41
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->location_overrides:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;

    .line 42
    iget-object v2, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->location_id:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 43
    iget-object p1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;->FIXED_PRICING:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    if-ne p1, v0, :cond_1

    iget-object p1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-nez p1, :cond_1

    goto :goto_0

    .line 47
    :cond_1
    iget-object p1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p1

    .line 51
    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p1
.end method

.method public getSku()Ljava/lang/String;
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->sku:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public isVariablePricing()Z
    .locals 3

    .line 80
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;->VARIABLE_PRICING:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    sget-object v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->DEFAULT_PRICING_TYPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    .line 81
    invoke-static {v1, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 80
    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
