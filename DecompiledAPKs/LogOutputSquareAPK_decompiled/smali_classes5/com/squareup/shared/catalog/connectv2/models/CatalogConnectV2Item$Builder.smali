.class public Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;
.super Ljava/lang/Object;
.source "CatalogConnectV2Item.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

.field private item:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;-><init>()V

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    .line 52
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->item:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item;)V
    .locals 1

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iget-object v0, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    .line 57
    iget-object p1, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->item:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    return-void
.end method


# virtual methods
.method public addOption(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;
    .locals 1

    .line 75
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object p1

    .line 76
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem$Builder;-><init>()V

    .line 77
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem$Builder;->item_option_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem;

    move-result-object p1

    .line 79
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->item:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->item_options:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->item:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->item_options:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public addOrReplaceVariation(Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;
    .locals 3

    .line 96
    iget-object p1, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    const/4 v0, 0x0

    .line 99
    :goto_0
    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->item:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 100
    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->item:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->variations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->id:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->item:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->variations:Ljava/util/List;

    invoke-interface {v1, v0, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->item:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->variations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item;
    .locals 2

    .line 131
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->item:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object v0

    .line 133
    new-instance v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item;

    invoke-direct {v1, v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v1
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->item:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public removeAllOptions()Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->item:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->item_options:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-object p0
.end method

.method public removeOption(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;
    .locals 1

    .line 87
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object p1

    .line 88
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem$Builder;-><init>()V

    .line 89
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem$Builder;->item_option_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem;

    move-result-object p1

    .line 91
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->item:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->item_options:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public removeVariationWithId(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;
    .locals 2

    .line 113
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->item:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 114
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    .line 116
    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->id:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    :cond_1
    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;->item:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem$Builder;

    return-object p0
.end method
