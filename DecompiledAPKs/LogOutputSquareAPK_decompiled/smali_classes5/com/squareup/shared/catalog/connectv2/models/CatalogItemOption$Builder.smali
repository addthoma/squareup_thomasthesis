.class public Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;
.super Ljava/lang/Object;
.source "CatalogItemOption.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

.field private itemOption:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;-><init>()V

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_OPTION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 59
    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    .line 60
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->itemOption:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)V
    .locals 1

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iget-object v0, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    .line 66
    iget-object p1, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->itemOption:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    return-void
.end method

.method public static fromByteArray([B)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 140
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;

    invoke-virtual {v1, p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->parse([B)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)V

    return-object v0
.end method


# virtual methods
.method public addValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->itemOption:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;->values:Ljava/util/List;

    .line 115
    iget-object p1, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->itemOption:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_option_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object v0

    .line 132
    new-instance v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    invoke-direct {v1, v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v1
.end method

.method public getAllValues()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;",
            ">;"
        }
    .end annotation

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 107
    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->itemOption:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;->values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    .line 108
    new-instance v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    invoke-direct {v3, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->itemOption:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;->display_name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->itemOption:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getShowColors()Z
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->itemOption:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;->show_colors:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;->DEFAULT_SHOW_COLORS:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public removeAllValues()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->itemOption:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;->values:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-object p0
.end method

.method public removeValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->itemOption:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;->values:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public setDisplayName(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->itemOption:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/StringUtils;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;->display_name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->itemOption:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    return-object p0
.end method

.method public setShowColors(Z)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->itemOption:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;->show_colors(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption$Builder;

    return-object p0
.end method

.method public toByteArray()[B
    .locals 1

    .line 136
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method
