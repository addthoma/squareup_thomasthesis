.class public Lcom/squareup/shared/catalog/CatalogKeyManifest;
.super Lcom/squareup/shared/i18n/KeyManifest;
.source "CatalogKeyManifest.java"


# static fields
.field public static final ALL_ITEMS:Lcom/squareup/shared/i18n/Key;

.field public static final APPLY_ITEMS:Lcom/squareup/shared/i18n/Key;

.field public static final APPLY_ITEMS_EXACT_MANY:Lcom/squareup/shared/i18n/Key;

.field public static final APPLY_ITEMS_EXACT_ONE:Lcom/squareup/shared/i18n/Key;

.field public static final APPLY_ITEMS_RANGE:Lcom/squareup/shared/i18n/Key;

.field public static final BUY_ITEMS_ANY:Lcom/squareup/shared/i18n/Key;

.field public static final BUY_ITEMS_EXACT_MANY:Lcom/squareup/shared/i18n/Key;

.field public static final BUY_ITEMS_EXACT_ONE:Lcom/squareup/shared/i18n/Key;

.field public static final BUY_ITEMS_MIN_MANY:Lcom/squareup/shared/i18n/Key;

.field public static final BUY_ITEMS_MIN_ONE:Lcom/squareup/shared/i18n/Key;

.field public static final CATEGORIES_MANY:Lcom/squareup/shared/i18n/Key;

.field public static final CATEGORY_ONE:Lcom/squareup/shared/i18n/Key;

.field public static final ENDED_AT_LABEL:Lcom/squareup/shared/i18n/Key;

.field public static final ENDS_AT_LABEL:Lcom/squareup/shared/i18n/Key;

.field public static final FRIDAYS:Lcom/squareup/shared/i18n/Key;

.field public static final ITEMS_PLURAL:Lcom/squareup/shared/i18n/Key;

.field public static final ITEM_ONE:Lcom/squareup/shared/i18n/Key;

.field public static final LIST_THREE:Lcom/squareup/shared/i18n/Key;

.field public static final LIST_TWO:Lcom/squareup/shared/i18n/Key;

.field public static final MONDAYS:Lcom/squareup/shared/i18n/Key;

.field public static final NAME:Lcom/squareup/shared/i18n/Key;

.field public static final PRICING_RULE_DESCRIPTION_LONG:Lcom/squareup/shared/i18n/Key;

.field public static final PRICING_RULE_DESCRIPTION_MEDIUM_WITHOUT_ITEMS:Lcom/squareup/shared/i18n/Key;

.field public static final PRICING_RULE_DESCRIPTION_MEDIUM_WITH_ITEMS:Lcom/squareup/shared/i18n/Key;

.field public static final RULES_MANY:Lcom/squareup/shared/i18n/Key;

.field public static final SATURDAYS:Lcom/squareup/shared/i18n/Key;

.field public static final SCHEDULE:Lcom/squareup/shared/i18n/Key;

.field public static final SCHEDULE_DAYS_OF_WEEK_DAILY:Lcom/squareup/shared/i18n/Key;

.field public static final SCHEDULE_DAYS_OF_WEEK_MANY:Lcom/squareup/shared/i18n/Key;

.field public static final SCHEDULE_DAYS_OF_WEEK_WEEKDAYS:Lcom/squareup/shared/i18n/Key;

.field public static final SCHEDULE_MANY:Lcom/squareup/shared/i18n/Key;

.field public static final SCHEDULE_TIME_RANGE:Lcom/squareup/shared/i18n/Key;

.field public static final SCHEDULE_TIME_RANGE_NEXT_DAY:Lcom/squareup/shared/i18n/Key;

.field public static final SPECIFIED_PRODUCTS:Lcom/squareup/shared/i18n/Key;

.field public static final SPECIFIED_TIMES:Lcom/squareup/shared/i18n/Key;

.field public static final STARTED_AT_LABEL:Lcom/squareup/shared/i18n/Key;

.field public static final STARTS_AT_LABEL:Lcom/squareup/shared/i18n/Key;

.field public static final SUNDAYS:Lcom/squareup/shared/i18n/Key;

.field public static final THURSDAYS:Lcom/squareup/shared/i18n/Key;

.field public static final TUESDAYS:Lcom/squareup/shared/i18n/Key;

.field public static final VARIATIONS_PLURAL:Lcom/squareup/shared/i18n/Key;

.field public static final VARIATION_ONE:Lcom/squareup/shared/i18n/Key;

.field public static final WEDNESDAYS:Lcom/squareup/shared/i18n/Key;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 10
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/String;

    const-string v3, "all_items"

    const-string v4, "AllItems"

    invoke-direct {v0, v3, v4, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->ALL_ITEMS:Lcom/squareup/shared/i18n/Key;

    .line 14
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v2, v1, [Ljava/lang/String;

    const-string v3, "apply_items"

    const-string v4, "ApplyItems"

    invoke-direct {v0, v3, v4, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->APPLY_ITEMS:Lcom/squareup/shared/i18n/Key;

    .line 18
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v2, v1, [Ljava/lang/String;

    const-string v3, "apply_items_exact_one"

    const-string v4, "ApplyItemsExactOne"

    invoke-direct {v0, v3, v4, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->APPLY_ITEMS_EXACT_ONE:Lcom/squareup/shared/i18n/Key;

    .line 22
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    const-string v2, "num_items"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v3

    const-string v4, "apply_items_exact_many"

    const-string v5, "ApplyItemsExactMany"

    invoke-direct {v0, v4, v5, v3}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->APPLY_ITEMS_EXACT_MANY:Lcom/squareup/shared/i18n/Key;

    .line 26
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    const-string v3, "min_num_items"

    const-string v4, "max_num_items"

    filled-new-array {v3, v4}, [Ljava/lang/String;

    move-result-object v3

    const-string v4, "apply_items_range"

    const-string v5, "ApplyItemsRange"

    invoke-direct {v0, v4, v5, v3}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->APPLY_ITEMS_RANGE:Lcom/squareup/shared/i18n/Key;

    .line 29
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v3, v1, [Ljava/lang/String;

    const-string v4, "buy_items_any"

    const-string v5, "BuyItemsAny"

    invoke-direct {v0, v4, v5, v3}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->BUY_ITEMS_ANY:Lcom/squareup/shared/i18n/Key;

    .line 32
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v3, v1, [Ljava/lang/String;

    const-string v4, "buy_items_exact_one"

    const-string v5, "BuyItemsExactOne"

    invoke-direct {v0, v4, v5, v3}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->BUY_ITEMS_EXACT_ONE:Lcom/squareup/shared/i18n/Key;

    .line 35
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v3

    const-string v4, "buy_items_exact_many"

    const-string v5, "BuyItemsExactMany"

    invoke-direct {v0, v4, v5, v3}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->BUY_ITEMS_EXACT_MANY:Lcom/squareup/shared/i18n/Key;

    .line 38
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v3, v1, [Ljava/lang/String;

    const-string v4, "buy_items_min_one"

    const-string v5, "BuyItemsMinOne"

    invoke-direct {v0, v4, v5, v3}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->BUY_ITEMS_MIN_ONE:Lcom/squareup/shared/i18n/Key;

    .line 41
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v3

    const-string v4, "buy_items_min_many"

    const-string v5, "BuyItemsMinMany"

    invoke-direct {v0, v4, v5, v3}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->BUY_ITEMS_MIN_MANY:Lcom/squareup/shared/i18n/Key;

    .line 44
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    const-string v3, "num_categories"

    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object v3

    const-string v4, "categories_many"

    const-string v5, "CategoriesMany"

    invoke-direct {v0, v4, v5, v3}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->CATEGORIES_MANY:Lcom/squareup/shared/i18n/Key;

    .line 47
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v3, v1, [Ljava/lang/String;

    const-string v4, "category_one"

    const-string v5, "CategoryOne"

    invoke-direct {v0, v4, v5, v3}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->CATEGORY_ONE:Lcom/squareup/shared/i18n/Key;

    .line 50
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    const-string v3, "end_time"

    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object v4

    const-string v5, "ended_at_label"

    const-string v6, "EndedAtLabel"

    invoke-direct {v0, v5, v6, v4}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->ENDED_AT_LABEL:Lcom/squareup/shared/i18n/Key;

    .line 53
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object v4

    const-string v5, "ends_at_label"

    const-string v6, "EndsAtLabel"

    invoke-direct {v0, v5, v6, v4}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->ENDS_AT_LABEL:Lcom/squareup/shared/i18n/Key;

    .line 56
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v4, v1, [Ljava/lang/String;

    const-string v5, "fridays"

    const-string v6, "Fridays"

    invoke-direct {v0, v5, v6, v4}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->FRIDAYS:Lcom/squareup/shared/i18n/Key;

    .line 59
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v4, v1, [Ljava/lang/String;

    const-string v5, "item_one"

    const-string v6, "ItemOne"

    invoke-direct {v0, v5, v6, v4}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->ITEM_ONE:Lcom/squareup/shared/i18n/Key;

    .line 62
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    const-string v4, "items_plural"

    const-string v5, "ItemsPlural"

    invoke-direct {v0, v4, v5, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->ITEMS_PLURAL:Lcom/squareup/shared/i18n/Key;

    .line 65
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    const-string v2, "b"

    const-string v4, "a"

    const-string v5, "c"

    filled-new-array {v4, v2, v5}, [Ljava/lang/String;

    move-result-object v5

    const-string v6, "list_three"

    const-string v7, "ListThree"

    invoke-direct {v0, v6, v7, v5}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->LIST_THREE:Lcom/squareup/shared/i18n/Key;

    .line 68
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    filled-new-array {v4, v2}, [Ljava/lang/String;

    move-result-object v2

    const-string v4, "list_two"

    const-string v5, "ListTwo"

    invoke-direct {v0, v4, v5, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->LIST_TWO:Lcom/squareup/shared/i18n/Key;

    .line 71
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v2, v1, [Ljava/lang/String;

    const-string v4, "mondays"

    const-string v5, "Mondays"

    invoke-direct {v0, v4, v5, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->MONDAYS:Lcom/squareup/shared/i18n/Key;

    .line 73
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v2, v1, [Ljava/lang/String;

    const-string v4, "name"

    const-string v5, "Name"

    invoke-direct {v0, v4, v5, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->NAME:Lcom/squareup/shared/i18n/Key;

    .line 76
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    const-string v2, "items_description"

    const-string v4, "date_range"

    const-string v5, "schedules"

    filled-new-array {v5, v4, v2}, [Ljava/lang/String;

    move-result-object v6

    const-string v7, "pricing_rule_description_long"

    const-string v8, "PricingRuleDescriptionLong"

    invoke-direct {v0, v7, v8, v6}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->PRICING_RULE_DESCRIPTION_LONG:Lcom/squareup/shared/i18n/Key;

    .line 80
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    const-string v6, "schedules_or_date_range"

    filled-new-array {v6, v2}, [Ljava/lang/String;

    move-result-object v2

    const-string v6, "pricing_rule_description_medium_with_items"

    const-string v7, "PricingRuleDescriptionMediumWithItems"

    invoke-direct {v0, v6, v7, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->PRICING_RULE_DESCRIPTION_MEDIUM_WITH_ITEMS:Lcom/squareup/shared/i18n/Key;

    .line 84
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    filled-new-array {v5, v4}, [Ljava/lang/String;

    move-result-object v2

    const-string v4, "pricing_rule_description_medium_without_items"

    const-string v5, "PricingRuleDescriptionMediumWithoutItems"

    invoke-direct {v0, v4, v5, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->PRICING_RULE_DESCRIPTION_MEDIUM_WITHOUT_ITEMS:Lcom/squareup/shared/i18n/Key;

    .line 88
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    const-string v2, "num_rules"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    const-string v4, "rules_many"

    const-string v5, "RulesMany"

    invoke-direct {v0, v4, v5, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->RULES_MANY:Lcom/squareup/shared/i18n/Key;

    .line 91
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v2, v1, [Ljava/lang/String;

    const-string v4, "saturdays"

    const-string v5, "Saturdays"

    invoke-direct {v0, v4, v5, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SATURDAYS:Lcom/squareup/shared/i18n/Key;

    .line 94
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    const-string v2, "days_of_the_week"

    const-string/jumbo v4, "time_range"

    filled-new-array {v2, v4}, [Ljava/lang/String;

    move-result-object v2

    const-string v4, "schedule"

    const-string v5, "Schedule"

    invoke-direct {v0, v4, v5, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SCHEDULE:Lcom/squareup/shared/i18n/Key;

    .line 97
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v2, v1, [Ljava/lang/String;

    const-string v4, "schedule_days_of_the_week_daily"

    const-string v5, "ScheduleDaysOfTheWeekDaily"

    invoke-direct {v0, v4, v5, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SCHEDULE_DAYS_OF_WEEK_DAILY:Lcom/squareup/shared/i18n/Key;

    .line 100
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    const-string v2, "num_days"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    const-string v4, "schedule_days_of_the_week_many"

    const-string v5, "ScheduleDaysOfTheWeekMany"

    invoke-direct {v0, v4, v5, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SCHEDULE_DAYS_OF_WEEK_MANY:Lcom/squareup/shared/i18n/Key;

    .line 103
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v2, v1, [Ljava/lang/String;

    const-string v4, "schedule_days_of_the_week_weekdays"

    const-string v5, "ScheduleDaysOfTheWeekWeekdays"

    invoke-direct {v0, v4, v5, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SCHEDULE_DAYS_OF_WEEK_WEEKDAYS:Lcom/squareup/shared/i18n/Key;

    .line 106
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    const-string v2, "num_schedules"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    const-string v4, "schedule_many"

    const-string v5, "ScheduleMany"

    invoke-direct {v0, v4, v5, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SCHEDULE_MANY:Lcom/squareup/shared/i18n/Key;

    .line 109
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    const-string v2, "start_time"

    filled-new-array {v2, v3}, [Ljava/lang/String;

    move-result-object v4

    const-string v5, "schedule_time_range"

    const-string v6, "ScheduleTimeRange"

    invoke-direct {v0, v5, v6, v4}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SCHEDULE_TIME_RANGE:Lcom/squareup/shared/i18n/Key;

    .line 112
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    filled-new-array {v2, v3}, [Ljava/lang/String;

    move-result-object v3

    const-string v4, "schedule_time_range_next_day"

    const-string v5, "ScheduleTimeRangeNextDay"

    invoke-direct {v0, v4, v5, v3}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SCHEDULE_TIME_RANGE_NEXT_DAY:Lcom/squareup/shared/i18n/Key;

    .line 115
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v3, v1, [Ljava/lang/String;

    const-string v4, "specified_products"

    const-string v5, "SpecifiedProducts"

    invoke-direct {v0, v4, v5, v3}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SPECIFIED_PRODUCTS:Lcom/squareup/shared/i18n/Key;

    .line 118
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v3, v1, [Ljava/lang/String;

    const-string v4, "specified_times"

    const-string v5, "SpecifiedTimes"

    invoke-direct {v0, v4, v5, v3}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SPECIFIED_TIMES:Lcom/squareup/shared/i18n/Key;

    .line 121
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v3

    const-string v4, "started_at_label"

    const-string v5, "StartedAtLabel"

    invoke-direct {v0, v4, v5, v3}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->STARTED_AT_LABEL:Lcom/squareup/shared/i18n/Key;

    .line 124
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    const-string v3, "starts_at_label"

    const-string v4, "StartsAtLabel"

    invoke-direct {v0, v3, v4, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->STARTS_AT_LABEL:Lcom/squareup/shared/i18n/Key;

    .line 127
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v2, v1, [Ljava/lang/String;

    const-string v3, "sundays"

    const-string v4, "Sundays"

    invoke-direct {v0, v3, v4, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SUNDAYS:Lcom/squareup/shared/i18n/Key;

    .line 130
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v2, v1, [Ljava/lang/String;

    const-string/jumbo v3, "thursdays"

    const-string v4, "Thursdays"

    invoke-direct {v0, v3, v4, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->THURSDAYS:Lcom/squareup/shared/i18n/Key;

    .line 133
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v2, v1, [Ljava/lang/String;

    const-string/jumbo v3, "tuesdays"

    const-string v4, "Tuesdays"

    invoke-direct {v0, v3, v4, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->TUESDAYS:Lcom/squareup/shared/i18n/Key;

    .line 136
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v2, v1, [Ljava/lang/String;

    const-string/jumbo v3, "variation_one"

    const-string v4, "VariationOne"

    invoke-direct {v0, v3, v4, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->VARIATION_ONE:Lcom/squareup/shared/i18n/Key;

    .line 139
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    const-string v2, "num_variations"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "variations_plural"

    const-string v4, "VariationPlural"

    invoke-direct {v0, v3, v4, v2}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->VARIATIONS_PLURAL:Lcom/squareup/shared/i18n/Key;

    .line 142
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "wednesdays"

    const-string v3, "Wednesdays"

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->WEDNESDAYS:Lcom/squareup/shared/i18n/Key;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/squareup/shared/i18n/KeyManifest;-><init>()V

    return-void
.end method
