.class public final Lcom/squareup/shared/catalog/ManyToMany;
.super Ljava/lang/Object;
.source "ManyToMany.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/ManyToMany$Lookup;
    }
.end annotation


# static fields
.field public static final FEE_ITEMS:Lcom/squareup/shared/catalog/ManyToMany;

.field public static final ITEM_FEES:Lcom/squareup/shared/catalog/ManyToMany;

.field public static final ITEM_ITEM_MODIFIER_LISTS:Lcom/squareup/shared/catalog/ManyToMany;

.field public static final ITEM_MODIFIER_LIST_ITEMS:Lcom/squareup/shared/catalog/ManyToMany;


# instance fields
.field private final fromRef:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field private final toRef:Lcom/squareup/shared/catalog/models/CatalogRelation;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 16
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_LIST_MEMBERSHIP_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_LIST_MEMBERSHIP_LIST:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 17
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/ManyToMany;->manyToMany(Lcom/squareup/shared/catalog/models/CatalogRelation;Lcom/squareup/shared/catalog/models/CatalogRelation;)Lcom/squareup/shared/catalog/ManyToMany;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/ManyToMany;->ITEM_ITEM_MODIFIER_LISTS:Lcom/squareup/shared/catalog/ManyToMany;

    .line 19
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_LIST_MEMBERSHIP_LIST:Lcom/squareup/shared/catalog/models/CatalogRelation;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_LIST_MEMBERSHIP_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 20
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/ManyToMany;->manyToMany(Lcom/squareup/shared/catalog/models/CatalogRelation;Lcom/squareup/shared/catalog/models/CatalogRelation;)Lcom/squareup/shared/catalog/ManyToMany;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/ManyToMany;->ITEM_MODIFIER_LIST_ITEMS:Lcom/squareup/shared/catalog/ManyToMany;

    .line 22
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_ITEM_FEE_MEMBERSHIP_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_ITEM_FEE_MEMBERSHIP_TAX:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 23
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/ManyToMany;->manyToMany(Lcom/squareup/shared/catalog/models/CatalogRelation;Lcom/squareup/shared/catalog/models/CatalogRelation;)Lcom/squareup/shared/catalog/ManyToMany;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/ManyToMany;->ITEM_FEES:Lcom/squareup/shared/catalog/ManyToMany;

    .line 25
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_ITEM_FEE_MEMBERSHIP_TAX:Lcom/squareup/shared/catalog/models/CatalogRelation;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_ITEM_FEE_MEMBERSHIP_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 26
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/ManyToMany;->manyToMany(Lcom/squareup/shared/catalog/models/CatalogRelation;Lcom/squareup/shared/catalog/models/CatalogRelation;)Lcom/squareup/shared/catalog/ManyToMany;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/ManyToMany;->FEE_ITEMS:Lcom/squareup/shared/catalog/ManyToMany;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/shared/catalog/models/CatalogRelation;Lcom/squareup/shared/catalog/models/CatalogRelation;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/shared/catalog/ManyToMany;->fromRef:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 33
    iput-object p2, p0, Lcom/squareup/shared/catalog/ManyToMany;->toRef:Lcom/squareup/shared/catalog/models/CatalogRelation;

    return-void
.end method

.method public static manyToMany(Lcom/squareup/shared/catalog/models/CatalogRelation;Lcom/squareup/shared/catalog/models/CatalogRelation;)Lcom/squareup/shared/catalog/ManyToMany;
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/shared/catalog/ManyToMany;

    invoke-direct {v0, p0, p1}, Lcom/squareup/shared/catalog/ManyToMany;-><init>(Lcom/squareup/shared/catalog/models/CatalogRelation;Lcom/squareup/shared/catalog/models/CatalogRelation;)V

    return-object v0
.end method


# virtual methods
.method public createLookup(Ljava/lang/String;)Lcom/squareup/shared/catalog/ManyToMany$Lookup;
    .locals 4

    .line 41
    new-instance v0, Lcom/squareup/shared/catalog/ManyToMany$Lookup;

    iget-object v1, p0, Lcom/squareup/shared/catalog/ManyToMany;->fromRef:Lcom/squareup/shared/catalog/models/CatalogRelation;

    iget-object v2, p0, Lcom/squareup/shared/catalog/ManyToMany;->toRef:Lcom/squareup/shared/catalog/models/CatalogRelation;

    const/4 v3, 0x0

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/squareup/shared/catalog/ManyToMany$Lookup;-><init>(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/lang/String;Lcom/squareup/shared/catalog/models/CatalogRelation;Lcom/squareup/shared/catalog/ManyToMany$1;)V

    return-object v0
.end method
