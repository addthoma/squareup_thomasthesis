.class public interface abstract Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;
.super Ljava/lang/Object;
.source "CatalogUpdateDispatcher.java"


# virtual methods
.method public abstract cleanUp()V
.end method

.method public abstract dispatchLocalEdits()V
.end method

.method public abstract dispatchObjectsToBeChanged(Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
            "*>;>;)V"
        }
    .end annotation
.end method

.method public abstract dispatchSyncUpdates()V
.end method

.method public abstract updateCogsObjects(Ljava/util/Collection;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;)V"
        }
    .end annotation
.end method

.method public abstract updateConnectV2Objects(Ljava/util/Collection;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
            "*>;>;)V"
        }
    .end annotation
.end method
