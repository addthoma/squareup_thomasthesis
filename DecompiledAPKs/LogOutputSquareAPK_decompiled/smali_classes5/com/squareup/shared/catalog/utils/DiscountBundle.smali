.class public Lcom/squareup/shared/catalog/utils/DiscountBundle;
.super Ljava/lang/Object;
.source "DiscountBundle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;
    }
.end annotation


# instance fields
.field private final discount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

.field private final localizer:Lcom/squareup/shared/i18n/Localizer;

.field private final rules:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/utils/InflatedPricingRule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;Ljava/util/Map;Lcom/squareup/shared/i18n/Localizer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/utils/InflatedPricingRule;",
            ">;",
            "Lcom/squareup/shared/i18n/Localizer;",
            ")V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->discount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 44
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1, p2}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->rules:Ljava/util/Map;

    .line 45
    iput-object p3, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->localizer:Lcom/squareup/shared/i18n/Localizer;

    return-void
.end method


# virtual methods
.method public applyDescription()Ljava/lang/String;
    .locals 2

    .line 208
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->rules:Ljava/util/Map;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->rules:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 212
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->localizer:Lcom/squareup/shared/i18n/Localizer;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->applyDescription(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public description()Ljava/lang/String;
    .locals 6

    .line 181
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->localizer:Lcom/squareup/shared/i18n/Localizer;

    const/4 v1, 0x3

    new-array v2, v1, [Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    sget-object v3, Lcom/squareup/shared/i18n/LocalizedListHelper;->AS_IS:Lcom/squareup/shared/i18n/Key;

    const/4 v4, 0x1

    .line 182
    invoke-static {v4, v3}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->exactly(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    move-result-object v3

    const/4 v5, 0x0

    aput-object v3, v2, v5

    sget-object v3, Lcom/squareup/shared/catalog/CatalogKeyManifest;->LIST_TWO:Lcom/squareup/shared/i18n/Key;

    const/4 v5, 0x2

    .line 183
    invoke-static {v5, v3}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->exactly(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    move-result-object v3

    aput-object v3, v2, v4

    sget-object v3, Lcom/squareup/shared/catalog/CatalogKeyManifest;->RULES_MANY:Lcom/squareup/shared/i18n/Key;

    .line 184
    invoke-static {v1, v3}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->orMore(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    move-result-object v1

    aput-object v1, v2, v5

    .line 181
    invoke-static {v0, v2}, Lcom/squareup/shared/i18n/LocalizedListHelper;->list(Lcom/squareup/shared/i18n/Localizer;[Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)Lcom/squareup/shared/i18n/LocalizedListHelper;

    move-result-object v0

    .line 185
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->rules:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 186
    iget-object v3, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->localizer:Lcom/squareup/shared/i18n/Localizer;

    invoke-virtual {v2, v3}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->localizedDescription(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/shared/i18n/LocalizedListHelper;->put(Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedListHelper;

    goto :goto_0

    .line 188
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/shared/i18n/LocalizedListHelper;->format()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDiscount()Lcom/squareup/shared/catalog/models/CatalogDiscount;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->discount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    return-object v0
.end method

.method public getFlattenedExcludeProducts()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 161
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 162
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->rules:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 163
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getFlattenedExcludeProducts()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getFlattenedMatchProducts()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 153
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 154
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->rules:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 155
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getFlattenedMatchProducts()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getFlattenedMatchProductsWithoutExcludes()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 169
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 170
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->rules:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 171
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getFlattenedMatchProductsWithoutExcludes()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getRules()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/utils/InflatedPricingRule;",
            ">;"
        }
    .end annotation

    .line 149
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->rules:Ljava/util/Map;

    return-object v0
.end method

.method public hasComplexProducts()Z
    .locals 3

    .line 242
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->rules:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    return v1

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->rules:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 247
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->hasComplexProducts()Z

    move-result v2

    if-eqz v2, :cond_1

    return v1

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public hasComplexSchedule()Z
    .locals 2

    .line 258
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->rules:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 259
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->hasComplexSchedule()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public matchDescription()Ljava/lang/String;
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->rules:Ljava/util/Map;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->rules:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 200
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->localizer:Lcom/squareup/shared/i18n/Localizer;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchDescription(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public matchesAllProducts()Z
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->rules:Ljava/util/Map;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->rules:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 223
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchesAllProducts()Z

    move-result v0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public schedule()Lcom/squareup/shared/catalog/utils/WeeklySchedule;
    .locals 3

    .line 231
    new-instance v0, Lcom/squareup/shared/catalog/utils/WeeklySchedule;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/utils/WeeklySchedule;-><init>()V

    .line 232
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/DiscountBundle;->rules:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 233
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->schedule()Lcom/squareup/shared/catalog/utils/WeeklySchedule;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/shared/catalog/utils/WeeklySchedule;->addWeeklySchedule(Lcom/squareup/shared/catalog/utils/WeeklySchedule;)V

    goto :goto_0

    :cond_0
    return-object v0
.end method
