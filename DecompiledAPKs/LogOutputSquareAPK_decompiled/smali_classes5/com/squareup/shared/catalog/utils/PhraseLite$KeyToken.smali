.class Lcom/squareup/shared/catalog/utils/PhraseLite$KeyToken;
.super Lcom/squareup/shared/catalog/utils/PhraseLite$Token;
.source "PhraseLite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/utils/PhraseLite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "KeyToken"
.end annotation


# instance fields
.field private final key:Ljava/lang/String;

.field private value:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;Ljava/lang/String;)V
    .locals 0

    .line 314
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/utils/PhraseLite$Token;-><init>(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;)V

    .line 315
    iput-object p2, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$KeyToken;->key:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method expand(Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    .line 319
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$KeyToken;->key:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    iput-object p2, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$KeyToken;->value:Ljava/lang/CharSequence;

    .line 321
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/utils/PhraseLite$KeyToken;->getFormattedStart()I

    move-result p2

    .line 323
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$KeyToken;->key:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, p2

    add-int/lit8 v0, v0, 0x2

    .line 324
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$KeyToken;->value:Ljava/lang/CharSequence;

    invoke-virtual {p1, p2, v0, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;->replace(IILjava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;

    return-void
.end method

.method getFormattedLength()I
    .locals 1

    .line 330
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$KeyToken;->value:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    return v0
.end method
