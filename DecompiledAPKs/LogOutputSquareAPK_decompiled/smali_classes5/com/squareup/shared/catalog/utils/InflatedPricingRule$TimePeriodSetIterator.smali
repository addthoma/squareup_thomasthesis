.class Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;
.super Ljava/lang/Object;
.source "InflatedPricingRule.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/utils/InflatedPricingRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TimePeriodSetIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;",
        ">;"
    }
.end annotation


# instance fields
.field private iterators:[Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/Iterator<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private nextPeriods:[Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;

.field private ruleValidFrom:Ljava/util/Date;

.field private ruleValidUntil:Ljava/util/Date;


# direct methods
.method constructor <init>(Ljava/util/TimeZone;Lcom/squareup/shared/catalog/utils/InflatedPricingRule;)V
    .locals 4

    .line 682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 683
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getPeriods()Ljava/util/Collection;

    move-result-object v0

    .line 684
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    new-array v1, v1, [Ljava/util/Iterator;

    iput-object v1, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->iterators:[Ljava/util/Iterator;

    .line 685
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    new-array v1, v1, [Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;

    iput-object v1, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->nextPeriods:[Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;

    .line 686
    invoke-static {p2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->access$000(Lcom/squareup/shared/catalog/utils/InflatedPricingRule;)Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->validFrom(Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->ruleValidFrom:Ljava/util/Date;

    .line 687
    invoke-static {p2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->access$000(Lcom/squareup/shared/catalog/utils/InflatedPricingRule;)Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->validUntil(Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->ruleValidUntil:Ljava/util/Date;

    .line 690
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    .line 691
    iget-object v2, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->iterators:[Ljava/util/Iterator;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getDates(Ljava/util/TimeZone;)Ljava/util/Iterator;

    move-result-object v3

    aput-object v3, v2, v0

    .line 692
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getDurationMillis()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->pullNext(IJ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private pullNext(IJ)V
    .locals 5

    .line 726
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->iterators:[Ljava/util/Iterator;

    aget-object v0, v0, p1

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    .line 727
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->iterators:[Ljava/util/Iterator;

    aget-object v0, v0, p1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    .line 728
    new-instance v2, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    add-long/2addr v3, p2

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    .line 729
    iget-object v3, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->ruleValidUntil:Ljava/util/Date;

    if-eqz v3, :cond_2

    invoke-virtual {v3, v2}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 730
    iget-object p2, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->ruleValidUntil:Ljava/util/Date;

    invoke-virtual {p2, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 732
    iget-object p2, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->nextPeriods:[Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;

    new-instance p3, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;

    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->ruleValidUntil:Ljava/util/Date;

    .line 733
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    invoke-direct {p3, v0, v1, v2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;-><init>(Ljava/util/Date;J)V

    aput-object p3, p2, p1

    return-void

    .line 737
    :cond_1
    iget-object p2, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->nextPeriods:[Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;

    aput-object v1, p2, p1

    return-void

    .line 741
    :cond_2
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->ruleValidFrom:Ljava/util/Date;

    if-eqz v1, :cond_5

    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    .line 746
    :cond_3
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->ruleValidFrom:Ljava/util/Date;

    if-eqz v0, :cond_4

    invoke-virtual {v2, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 748
    :cond_4
    iget-object p2, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->nextPeriods:[Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;

    new-instance p3, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;

    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->ruleValidFrom:Ljava/util/Date;

    .line 749
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    iget-object v3, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->ruleValidFrom:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    invoke-direct {p3, v0, v1, v2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;-><init>(Ljava/util/Date;J)V

    aput-object p3, p2, p1

    return-void

    .line 743
    :cond_5
    :goto_0
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->nextPeriods:[Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;

    new-instance v2, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;

    invoke-direct {v2, v0, p2, p3}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;-><init>(Ljava/util/Date;J)V

    aput-object v2, v1, p1

    return-void

    .line 753
    :cond_6
    iget-object p2, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->nextPeriods:[Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;

    aput-object v1, p2, p1

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 5

    .line 698
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->nextPeriods:[Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public next()Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 708
    :goto_0
    iget-object v2, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->nextPeriods:[Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;

    array-length v3, v2

    if-ge v1, v3, :cond_2

    .line 709
    aget-object v3, v2, v1

    if-eqz v3, :cond_1

    if-nez v0, :cond_0

    .line 711
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 712
    :cond_0
    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;->getStartsAt()Ljava/util/Date;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->nextPeriods:[Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;->getStartsAt()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 713
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    .line 720
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v1, v2, v1

    .line 721
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;->getDuration()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->pullNext(IJ)V

    return-object v1

    .line 718
    :cond_3
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .line 676
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;->next()Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;

    move-result-object v0

    return-object v0
.end method
