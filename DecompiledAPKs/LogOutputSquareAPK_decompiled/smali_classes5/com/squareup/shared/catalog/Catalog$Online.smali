.class public interface abstract Lcom/squareup/shared/catalog/Catalog$Online;
.super Ljava/lang/Object;
.source "Catalog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/Catalog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Online"
.end annotation


# virtual methods
.method public abstract batchRetrieveCatalogConnectV2Objects(Ljava/util/List;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract batchUpsertCatalogConnectV2Objects(Ljava/util/List;Ljava/lang/String;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract countVariationsWithAssignedUnit(Ljava/lang/String;I)Lcom/squareup/shared/catalog/sync/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract deleteCatalogConnectV2Object(Ljava/lang/String;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end method

.method public abstract retrieveCatalogConnectV2Object(Ljava/lang/String;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;"
        }
    .end annotation
.end method
