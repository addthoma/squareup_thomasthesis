.class public final enum Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;
.super Ljava/lang/Enum;
.source "PendingWriteRequestsTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/HasQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/PendingWriteRequestsTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;",
        ">;",
        "Lcom/squareup/shared/catalog/HasQuery;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

.field public static final enum CREATE:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

.field public static final enum DELETE:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

.field public static final enum ENQUEUE:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

.field public static final enum HAS_PUT_REQUESTS:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

.field public static final enum READ_ALL:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;


# instance fields
.field private final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 35
    new-instance v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    const-string v1, "CREATE TABLE {table} ({id} INTEGER PRIMARY KEY, {request} BLOB)"

    .line 36
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "pending_write_requests"

    const-string v3, "table"

    .line 38
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v4, "id"

    .line 39
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v5, "request"

    .line 40
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 41
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 42
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x0

    const-string v7, "CREATE"

    invoke-direct {v0, v7, v6, v1}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->CREATE:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    .line 44
    new-instance v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    const-string v1, "DELETE FROM {table} WHERE {id} = ?"

    .line 45
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 46
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 47
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 48
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 49
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x1

    const-string v8, "DELETE"

    invoke-direct {v0, v8, v7, v1}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->DELETE:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    .line 51
    new-instance v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    const-string v1, "INSERT OR ABORT INTO {table} ({id}, {request}) VALUES (?, ?)"

    .line 52
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 53
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 54
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 55
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 56
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 57
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v8, 0x2

    const-string v9, "ENQUEUE"

    invoke-direct {v0, v9, v8, v1}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->ENQUEUE:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    .line 59
    new-instance v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    const-string v1, "SELECT COUNT(*) FROM {table}"

    .line 60
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 61
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 62
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 63
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v9, 0x3

    const-string v10, "HAS_PUT_REQUESTS"

    invoke-direct {v0, v10, v9, v1}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->HAS_PUT_REQUESTS:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    .line 65
    new-instance v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    const-string v1, "SELECT {id}, {request} FROM {table} ORDER BY {id} ASC"

    .line 66
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 67
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 68
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 69
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 70
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 71
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    const-string v3, "READ_ALL"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->READ_ALL:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    .line 34
    sget-object v1, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->CREATE:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->DELETE:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->ENQUEUE:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->HAS_PUT_REQUESTS:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->READ_ALL:Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 76
    iput-object p3, p0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->query:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;
    .locals 1

    .line 34
    const-class v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;

    return-object v0
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/shared/catalog/PendingWriteRequestsTable$Query;->query:Ljava/lang/String;

    return-object v0
.end method
