.class public Lcom/squareup/shared/catalog/MetadataTable;
.super Ljava/lang/Object;
.source "MetadataTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/MetadataTable$Query;
    }
.end annotation


# static fields
.field public static final COLUMN_CLIENT_STATE_SEQ:Ljava/lang/String; = "seq"

.field public static final COLUMN_COGS_APPLIED_SERVER_VERSION:Ljava/lang/String; = "cogs_server_version"

.field public static final COLUMN_LAST_SYNC_TIMESTAMP:Ljava/lang/String; = "last_sync_timestamp"

.field public static final COLUMN_SESSION_ID:Ljava/lang/String; = "session_id"

.field public static final COLUMN_VERSION_SYNC_INCOMPLETE:Ljava/lang/String; = "version_sync_incomplete"

.field private static INSTANCE:Lcom/squareup/shared/catalog/MetadataTable; = null

.field public static final NAME:Ljava/lang/String; = "catalog_metadata"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static instance()Lcom/squareup/shared/catalog/MetadataTable;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/shared/catalog/MetadataTable;->INSTANCE:Lcom/squareup/shared/catalog/MetadataTable;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/squareup/shared/catalog/MetadataTable;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/MetadataTable;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/MetadataTable;->INSTANCE:Lcom/squareup/shared/catalog/MetadataTable;

    .line 37
    :cond_0
    sget-object v0, Lcom/squareup/shared/catalog/MetadataTable;->INSTANCE:Lcom/squareup/shared/catalog/MetadataTable;

    return-object v0
.end method

.method private readJavaLong(Ljava/lang/String;Lcom/squareup/shared/sql/SQLDatabase;)Ljava/lang/Long;
    .locals 3

    .line 157
    sget-object v0, Lcom/squareup/shared/catalog/MetadataTable$Query;->SELECT_COLUMN:Lcom/squareup/shared/catalog/MetadataTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/MetadataTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    .line 158
    invoke-interface {p2, p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    .line 160
    :try_start_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->moveToFirst()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 161
    invoke-interface {p1, v2}, Lcom/squareup/shared/sql/SQLCursor;->isNull(I)Z

    move-result p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_0

    .line 168
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object v0

    .line 164
    :cond_0
    :try_start_1
    new-instance p2, Ljava/lang/Long;

    invoke-interface {p1, v2}, Lcom/squareup/shared/sql/SQLCursor;->getLong(I)J

    move-result-wide v0

    invoke-direct {p2, v0, v1}, Ljava/lang/Long;-><init>(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 168
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object p2

    :cond_1
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object v0

    :catchall_0
    move-exception p2

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw p2
.end method

.method private readLong(Ljava/lang/String;Lcom/squareup/shared/sql/SQLDatabase;)J
    .locals 0

    .line 148
    invoke-direct {p0, p1, p2}, Lcom/squareup/shared/catalog/MetadataTable;->readJavaLong(Ljava/lang/String;Lcom/squareup/shared/sql/SQLDatabase;)Ljava/lang/Long;

    move-result-object p1

    if-nez p1, :cond_0

    const-wide/16 p1, 0x0

    return-wide p1

    .line 152
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    return-wide p1
.end method

.method private readString(Ljava/lang/String;Lcom/squareup/shared/sql/SQLDatabase;)Ljava/lang/String;
    .locals 3

    .line 180
    sget-object v0, Lcom/squareup/shared/catalog/MetadataTable$Query;->SELECT_COLUMN:Lcom/squareup/shared/catalog/MetadataTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/MetadataTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    .line 181
    invoke-interface {p2, p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    .line 183
    :try_start_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->moveToFirst()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 184
    invoke-interface {p1, v2}, Lcom/squareup/shared/sql/SQLCursor;->isNull(I)Z

    move-result p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_0

    .line 191
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object v0

    .line 187
    :cond_0
    :try_start_1
    invoke-interface {p1, v2}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 191
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object p2

    :cond_1
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object v0

    :catchall_0
    move-exception p2

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw p2
.end method

.method private writeLong(JLjava/lang/String;Lcom/squareup/shared/sql/SQLDatabase;)V
    .locals 3

    .line 141
    sget-object v0, Lcom/squareup/shared/catalog/MetadataTable$Query;->UPDATE_COLUMN:Lcom/squareup/shared/catalog/MetadataTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/MetadataTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 142
    invoke-static {p4, p3}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p3

    .line 143
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 144
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method private writeString(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/shared/sql/SQLDatabase;)V
    .locals 3

    .line 173
    sget-object v0, Lcom/squareup/shared/catalog/MetadataTable$Query;->UPDATE_COLUMN:Lcom/squareup/shared/catalog/MetadataTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/MetadataTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 174
    invoke-static {p3, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p2

    .line 175
    invoke-virtual {p2, p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 176
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method


# virtual methods
.method public create(Lcom/squareup/shared/sql/SQLDatabase;)V
    .locals 1

    .line 92
    sget-object v0, Lcom/squareup/shared/catalog/MetadataTable$Query;->CREATE:Lcom/squareup/shared/catalog/MetadataTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/MetadataTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 93
    sget-object v0, Lcom/squareup/shared/catalog/MetadataTable$Query;->INSERT_INITIAL_ROW:Lcom/squareup/shared/catalog/MetadataTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/MetadataTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public selectClientStateSeq(Lcom/squareup/shared/sql/SQLDatabase;)J
    .locals 2

    const-string v0, "seq"

    .line 137
    invoke-direct {p0, v0, p1}, Lcom/squareup/shared/catalog/MetadataTable;->readLong(Ljava/lang/String;Lcom/squareup/shared/sql/SQLDatabase;)J

    move-result-wide v0

    return-wide v0
.end method

.method public selectCogsAppliedServerVersion(Lcom/squareup/shared/sql/SQLDatabase;)J
    .locals 2

    const-string v0, "cogs_server_version"

    .line 109
    invoke-direct {p0, v0, p1}, Lcom/squareup/shared/catalog/MetadataTable;->readLong(Ljava/lang/String;Lcom/squareup/shared/sql/SQLDatabase;)J

    move-result-wide v0

    return-wide v0
.end method

.method public selectSessionId(Lcom/squareup/shared/sql/SQLDatabase;)Ljava/lang/Long;
    .locals 1

    const-string v0, "session_id"

    .line 101
    invoke-direct {p0, v0, p1}, Lcom/squareup/shared/catalog/MetadataTable;->readJavaLong(Ljava/lang/String;Lcom/squareup/shared/sql/SQLDatabase;)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method

.method public selectSyncIncomplete(Lcom/squareup/shared/sql/SQLDatabase;)Z
    .locals 4

    const-string/jumbo v0, "version_sync_incomplete"

    .line 117
    invoke-direct {p0, v0, p1}, Lcom/squareup/shared/catalog/MetadataTable;->readLong(Ljava/lang/String;Lcom/squareup/shared/sql/SQLDatabase;)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public selectSyncTimestamp(Lcom/squareup/shared/sql/SQLDatabase;)Ljava/util/Date;
    .locals 1

    const-string v0, "last_sync_timestamp"

    .line 127
    invoke-direct {p0, v0, p1}, Lcom/squareup/shared/catalog/MetadataTable;->readString(Ljava/lang/String;Lcom/squareup/shared/sql/SQLDatabase;)Ljava/lang/String;

    move-result-object p1

    .line 128
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/TimeUtils;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    return-object p1
.end method

.method public updateClientStateSeq(JLcom/squareup/shared/sql/SQLDatabase;)V
    .locals 1

    const-string v0, "seq"

    .line 133
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/squareup/shared/catalog/MetadataTable;->writeLong(JLjava/lang/String;Lcom/squareup/shared/sql/SQLDatabase;)V

    return-void
.end method

.method public updateCogsAppliedServerVersion(JLcom/squareup/shared/sql/SQLDatabase;)V
    .locals 1

    const-string v0, "cogs_server_version"

    .line 105
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/squareup/shared/catalog/MetadataTable;->writeLong(JLjava/lang/String;Lcom/squareup/shared/sql/SQLDatabase;)V

    return-void
.end method

.method public updateSessionId(JLcom/squareup/shared/sql/SQLDatabase;)V
    .locals 1

    const-string v0, "session_id"

    .line 97
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/squareup/shared/catalog/MetadataTable;->writeLong(JLjava/lang/String;Lcom/squareup/shared/sql/SQLDatabase;)V

    return-void
.end method

.method public updateSyncIncomplete(ZLcom/squareup/shared/sql/SQLDatabase;)V
    .locals 2

    int-to-long v0, p1

    const-string/jumbo p1, "version_sync_incomplete"

    .line 113
    invoke-direct {p0, v0, v1, p1, p2}, Lcom/squareup/shared/catalog/MetadataTable;->writeLong(JLjava/lang/String;Lcom/squareup/shared/sql/SQLDatabase;)V

    return-void
.end method

.method public updateSyncTimestamp(Ljava/util/Date;Lcom/squareup/shared/sql/SQLDatabase;)V
    .locals 1

    .line 122
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/TimeUtils;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "last_sync_timestamp"

    .line 123
    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/shared/catalog/MetadataTable;->writeString(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/shared/sql/SQLDatabase;)V

    return-void
.end method
