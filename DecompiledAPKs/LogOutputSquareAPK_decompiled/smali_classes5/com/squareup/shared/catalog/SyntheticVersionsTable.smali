.class public Lcom/squareup/shared/catalog/SyntheticVersionsTable;
.super Ljava/lang/Object;
.source "SyntheticVersionsTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;
    }
.end annotation


# static fields
.field public static final COLUMN_TABLE_NAME:Ljava/lang/String; = "table_name"

.field public static final COLUMN_TABLE_VERSION:Ljava/lang/String; = "table_version"

.field private static INSTANCE:Lcom/squareup/shared/catalog/SyntheticVersionsTable; = null

.field public static final NAME:Ljava/lang/String; = "synthetic_table_versions"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static instance()Lcom/squareup/shared/catalog/SyntheticVersionsTable;
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable;->INSTANCE:Lcom/squareup/shared/catalog/SyntheticVersionsTable;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/SyntheticVersionsTable;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable;->INSTANCE:Lcom/squareup/shared/catalog/SyntheticVersionsTable;

    .line 32
    :cond_0
    sget-object v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable;->INSTANCE:Lcom/squareup/shared/catalog/SyntheticVersionsTable;

    return-object v0
.end method


# virtual methods
.method public clearAllAndWriteVersions(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;)V"
        }
    .end annotation

    .line 100
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->beginTransaction()V

    .line 102
    :try_start_0
    sget-object v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->DELETE_FROM_TABLE:Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 103
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;

    .line 104
    sget-object v1, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->WRITE_VERSIONS:Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v1

    .line 105
    invoke-interface {v0}, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;->tableName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v1

    .line 106
    invoke-interface {v0}, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;->tableVersion()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    goto :goto_0

    .line 109
    :cond_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p2

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    throw p2
.end method

.method public create(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;)V"
        }
    .end annotation

    .line 77
    sget-object v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->CREATE:Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0, p1, p2}, Lcom/squareup/shared/catalog/SyntheticVersionsTable;->clearAllAndWriteVersions(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Set;)V

    return-void
.end method

.method public readAllVersions(Lcom/squareup/shared/sql/SQLDatabase;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 83
    sget-object v0, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->READ_VERSIONS:Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/SyntheticVersionsTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    .line 85
    :try_start_0
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 86
    :goto_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 87
    invoke-interface {p1, v1}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    .line 88
    invoke-interface {p1, v2}, Lcom/squareup/shared/sql/SQLCursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 89
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    .line 94
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_1
    return-object v0

    :catchall_0
    move-exception v0

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    :cond_2
    throw v0
.end method
