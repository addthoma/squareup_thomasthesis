.class public final Lcom/squareup/shared/catalog/models/CatalogVoidReason;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogVoidReason.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/VoidReason;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method

.method public static createForTest(Ljava/lang/String;Ljava/lang/String;I)Lcom/squareup/shared/catalog/models/CatalogVoidReason;
    .locals 2

    .line 14
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->VOID_REASON:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 15
    new-instance v0, Lcom/squareup/api/items/VoidReason$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/VoidReason$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    .line 16
    invoke-virtual {v0, v1}, Lcom/squareup/api/items/VoidReason$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/VoidReason$Builder;

    move-result-object v0

    .line 17
    invoke-virtual {v0, p1}, Lcom/squareup/api/items/VoidReason$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/VoidReason$Builder;

    move-result-object p1

    .line 18
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/api/items/VoidReason$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/VoidReason$Builder;

    move-result-object p1

    .line 19
    invoke-virtual {p1}, Lcom/squareup/api/items/VoidReason$Builder;->build()Lcom/squareup/api/items/VoidReason;

    move-result-object p1

    .line 15
    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->void_reason(Lcom/squareup/api/items/VoidReason;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 20
    new-instance p1, Lcom/squareup/shared/catalog/models/CatalogVoidReason;

    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/squareup/shared/catalog/models/CatalogVoidReason;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object p1
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 2

    .line 28
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogVoidReason;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/VoidReason;

    iget-object v0, v0, Lcom/squareup/api/items/VoidReason;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getOrdinal()I
    .locals 2

    .line 32
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogVoidReason;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/VoidReason;

    iget-object v0, v0, Lcom/squareup/api/items/VoidReason;->ordinal:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/VoidReason;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
