.class public final Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;
.super Ljava/lang/Object;
.source "CatalogDiscount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogDiscount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final discount:Lcom/squareup/api/items/Discount$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 180
    new-instance v0, Lcom/squareup/api/items/Discount$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Discount$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Discount$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->discount:Lcom/squareup/api/items/Discount$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/models/CatalogDiscount;
    .locals 2

    .line 231
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->discount:Lcom/squareup/api/items/Discount$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/Discount$Builder;->build()Lcom/squareup/api/items/Discount;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->discount(Lcom/squareup/api/items/Discount;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 232
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;
    .locals 1

    .line 201
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->discount:Lcom/squareup/api/items/Discount$Builder;

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toDineroOrNull(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Discount$Builder;->amount(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/Discount$Builder;

    return-object p0
.end method

.method public setApplicationMethod(Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;
    .locals 1

    .line 216
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->discount:Lcom/squareup/api/items/Discount$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Discount$Builder;->application_method(Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/api/items/Discount$Builder;

    return-object p0
.end method

.method public setCompOrdinal(I)Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->discount:Lcom/squareup/api/items/Discount$Builder;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Discount$Builder;->comp_ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/Discount$Builder;

    return-object p0
.end method

.method public setDiscountType(Lcom/squareup/api/items/Discount$DiscountType;)Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->discount:Lcom/squareup/api/items/Discount$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Discount$Builder;->discount_type(Lcom/squareup/api/items/Discount$DiscountType;)Lcom/squareup/api/items/Discount$Builder;

    return-object p0
.end method

.method public setIdForTest(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;
    .locals 2

    .line 184
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    new-instance v1, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v1}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectId;

    .line 185
    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId;->newBuilder()Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v0

    iput-object v0, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    .line 186
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->discount:Lcom/squareup/api/items/Discount$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Discount$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;

    return-object p0
.end method

.method public setMaximumAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->discount:Lcom/squareup/api/items/Discount$Builder;

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toDineroOrNull(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Discount$Builder;->maximum_amount(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/Discount$Builder;

    return-object p0
.end method

.method public setModifyTaxBasis(Lcom/squareup/api/items/Discount$ModifyTaxBasis;)Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;
    .locals 1

    .line 221
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->discount:Lcom/squareup/api/items/Discount$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Discount$Builder;->modify_tax_basis(Lcom/squareup/api/items/Discount$ModifyTaxBasis;)Lcom/squareup/api/items/Discount$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->discount:Lcom/squareup/api/items/Discount$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Discount$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;

    return-object p0
.end method

.method public setPercentage(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;
    .locals 1

    .line 196
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->discount:Lcom/squareup/api/items/Discount$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Discount$Builder;->percentage(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;

    return-object p0
.end method
