.class public final Lcom/squareup/shared/catalog/models/CatalogDiningOption;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogDiningOption.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/DiningOption;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method

.method public static createForTest(Ljava/lang/String;Ljava/lang/String;I)Lcom/squareup/shared/catalog/models/CatalogDiningOption;
    .locals 2

    .line 15
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DINING_OPTION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 16
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogDiningOption;

    new-instance v1, Lcom/squareup/api/items/DiningOption$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/DiningOption$Builder;-><init>()V

    .line 17
    invoke-virtual {v1, p1}, Lcom/squareup/api/items/DiningOption$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/DiningOption$Builder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/squareup/api/items/DiningOption$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/DiningOption$Builder;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/api/items/DiningOption$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/DiningOption$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/items/DiningOption$Builder;->build()Lcom/squareup/api/items/DiningOption;

    move-result-object p1

    .line 16
    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->dining_option(Lcom/squareup/api/items/DiningOption;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 18
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 40
    instance-of v0, p1, Lcom/squareup/shared/catalog/models/CatalogDiningOption;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 43
    :cond_0
    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogDiningOption;

    .line 44
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 46
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->getOrdinal()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->getOrdinal()I

    move-result p1

    if-ne v0, p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public getMerchantCatalogObjectToken()Ljava/lang/String;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/DiningOption;

    iget-object v0, v0, Lcom/squareup/api/items/DiningOption;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 28
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/DiningOption;

    iget-object v0, v0, Lcom/squareup/api/items/DiningOption;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v0, v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 32
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/DiningOption;

    iget-object v0, v0, Lcom/squareup/api/items/DiningOption;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getOrdinal()I
    .locals 2

    .line 36
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/DiningOption;

    iget-object v0, v0, Lcom/squareup/api/items/DiningOption;->ordinal:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/DiningOption;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 50
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->getOrdinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/ObjectUtils;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
