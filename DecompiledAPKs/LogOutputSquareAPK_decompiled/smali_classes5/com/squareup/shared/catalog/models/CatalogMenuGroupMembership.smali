.class public final Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogMenuGroupMembership.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/MenuGroupMembership;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method


# virtual methods
.method public getMemberCatalogObjectType()Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership;->getMemberType()Lcom/squareup/api/items/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeFromProtoType(Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v0

    return-object v0
.end method

.method public getMemberId()Ljava/lang/String;
    .locals 2

    .line 36
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/MenuGroupMembership;

    .line 37
    iget-object v1, v0, Lcom/squareup/api/items/MenuGroupMembership;->member:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/api/items/MenuGroupMembership;->member:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/MenuGroupMembership;->member:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getMemberType()Lcom/squareup/api/items/Type;
    .locals 1

    .line 42
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/MenuGroupMembership;

    .line 43
    iget-object v0, v0, Lcom/squareup/api/items/MenuGroupMembership;->member:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    return-object v0
.end method

.method public getParentMenuGroupId()Ljava/lang/String;
    .locals 2

    .line 30
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/MenuGroupMembership;

    .line 31
    iget-object v1, v0, Lcom/squareup/api/items/MenuGroupMembership;->parent_menu_group:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/api/items/MenuGroupMembership;->parent_menu_group:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/MenuGroupMembership;->parent_menu_group:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getReferentTypes()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;"
        }
    .end annotation

    .line 68
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership;->getMemberType()Lcom/squareup/api/items/Type;

    move-result-object v0

    .line 69
    sget-object v1, Lcom/squareup/api/items/Type;->TAG:Lcom/squareup/api/items/Type;

    if-ne v0, v1, :cond_0

    .line 70
    sget-object v0, Lcom/squareup/api/items/Type;->TAG:Lcom/squareup/api/items/Type;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [Lcom/squareup/api/items/Type;

    const/4 v2, 0x0

    .line 72
    sget-object v3, Lcom/squareup/api/items/Type;->TAG:Lcom/squareup/api/items/Type;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRelations()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 51
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 54
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership;->getMemberType()Lcom/squareup/api/items/Type;

    move-result-object v1

    .line 55
    sget-object v2, Lcom/squareup/api/items/Type;->TAG:Lcom/squareup/api/items/Type;

    if-ne v1, v2, :cond_0

    .line 56
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_MENU_GROUP_MEMBERSHIP_TAG:Lcom/squareup/shared/catalog/models/CatalogRelation;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership;->getParentMenuGroupId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership;->getMemberId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 57
    :cond_0
    sget-object v2, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    if-ne v1, v2, :cond_1

    .line 58
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_MENU_GROUP_MEMBERSHIP_TAG:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership;->getParentMenuGroupId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_MENU_GROUP_MEMBERSHIP_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership;->getMemberId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    :goto_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0

    .line 61
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Invalid MenuGroupMembership member type"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
