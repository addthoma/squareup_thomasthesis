.class public final Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;
.super Ljava/lang/Object;
.source "CatalogProductSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogProductSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final productSet:Lcom/squareup/api/items/ProductSet$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PRODUCT_SET:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 140
    new-instance v0, Lcom/squareup/api/items/ProductSet$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ProductSet$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ProductSet$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ProductSet$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogProductSet;)V
    .locals 0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 145
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object p1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->product_set:Lcom/squareup/api/items/ProductSet;

    invoke-virtual {p1}, Lcom/squareup/api/items/ProductSet;->newBuilder()Lcom/squareup/api/items/ProductSet$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/models/CatalogProductSet;
    .locals 2

    .line 276
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/ProductSet$Builder;->build()Lcom/squareup/api/items/ProductSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->product_set(Lcom/squareup/api/items/ProductSet;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 277
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogProductSet;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 245
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet$Builder;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getProductsAll()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 265
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet$Builder;->products_all:Ljava/util/List;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getProductsAny()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 261
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet$Builder;->products_any:Ljava/util/List;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getQuantityExact()Ljava/lang/Integer;
    .locals 2

    .line 249
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet$Builder;->quantity_exact:Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getQuantityMax()Ljava/lang/Integer;
    .locals 2

    .line 257
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet$Builder;->quantity_max:Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getQuantityMin()Ljava/lang/Integer;
    .locals 2

    .line 253
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/ProductSet$Builder;->quantity_min:Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public setAllProducts(Z)Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, v0, Lcom/squareup/api/items/ProductSet$Builder;->all_products:Ljava/lang/Boolean;

    return-object p0
.end method

.method public setAppliesToCustomAmount(Z)Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, v0, Lcom/squareup/api/items/ProductSet$Builder;->custom_amounts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public setIdForTest(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;
    .locals 2

    .line 269
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    new-instance v1, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v1}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectId;

    .line 270
    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId;->newBuilder()Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v0

    iput-object v0, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    .line 271
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ProductSet$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ProductSet$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ProductSet$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/ProductSet$Builder;

    return-object p0
.end method

.method public setProductsAll(Ljava/util/List;)Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 229
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 230
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->getProductsAny()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->getProductsAny()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "Product set can not have both productsAny and productsAll"

    .line 228
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 232
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    iput-object p1, v0, Lcom/squareup/api/items/ProductSet$Builder;->products_all:Ljava/util/List;

    return-object p0
.end method

.method public setProductsAny(Ljava/util/List;)Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 216
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 217
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->getProductsAll()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->getProductsAll()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "Product set can not have both productsAny and productsAll"

    .line 215
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 219
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    iput-object p1, v0, Lcom/squareup/api/items/ProductSet$Builder;->products_any:Ljava/util/List;

    return-object p0
.end method

.method public setQuantityExact(Ljava/lang/Integer;)Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_1

    .line 168
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->getQuantityMin()Ljava/lang/Integer;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    const-string v3, "Product set can not have both quantityMin and quantityExact"

    invoke-static {v2, v3}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    if-eqz p1, :cond_3

    .line 170
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->getQuantityMax()Ljava/lang/Integer;

    move-result-object v2

    if-nez v2, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v2, 0x1

    :goto_3
    const-string v3, "Product set can not have both quantityMax and quantityExact"

    invoke-static {v2, v3}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    if-eqz p1, :cond_4

    .line 172
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_5

    :cond_4
    const/4 v0, 0x1

    :cond_5
    const-string v1, "quantityExact must be greater than 0"

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 174
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ProductSet$Builder;->quantity_exact(Ljava/lang/Integer;)Lcom/squareup/api/items/ProductSet$Builder;

    return-object p0
.end method

.method public setQuantityMax(Ljava/lang/Integer;)Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_1

    .line 202
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->getQuantityExact()Ljava/lang/Integer;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    const-string v3, "Product set can not have both quantityMax and quantityExact"

    invoke-static {v2, v3}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    if-eqz p1, :cond_2

    .line 204
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    const-string v1, "quantityMax must be greater than 0"

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 206
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ProductSet$Builder;->quantity_max(Ljava/lang/Integer;)Lcom/squareup/api/items/ProductSet$Builder;

    return-object p0
.end method

.method public setQuantityMin(Ljava/lang/Integer;)Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_1

    .line 185
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->getQuantityExact()Ljava/lang/Integer;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    const-string v3, "Product set can not have both quantityMin and quantityExact"

    invoke-static {v2, v3}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    if-eqz p1, :cond_3

    .line 187
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v2, 0x1

    :goto_3
    const-string v3, "quantityMin must be greater than 0"

    invoke-static {v2, v3}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 189
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->getQuantityMax()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_4

    if-eqz p1, :cond_4

    .line 190
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->getQuantityMax()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-le v2, v3, :cond_5

    :cond_4
    const/4 v0, 0x1

    :cond_5
    const-string v1, "quantityMax must be greater than quantityMin"

    .line 189
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->checkState(ZLjava/lang/Object;)V

    .line 191
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogProductSet$Builder;->productSet:Lcom/squareup/api/items/ProductSet$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ProductSet$Builder;->quantity_min(Ljava/lang/Integer;)Lcom/squareup/api/items/ProductSet$Builder;

    return-object p0
.end method
