.class public Lcom/squareup/shared/catalog/models/CatalogFloorPlanTile;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogFloorPlanTile.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/FloorPlanTile;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method


# virtual methods
.method public getFloorPlan()Lcom/squareup/api/sync/ObjectId;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFloorPlanTile;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/FloorPlanTile;

    iget-object v0, v0, Lcom/squareup/api/items/FloorPlanTile;->floor_plan:Lcom/squareup/api/sync/ObjectId;

    return-object v0
.end method

.method public getHeight()I
    .locals 2

    .line 39
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFloorPlanTile;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/FloorPlanTile;

    iget-object v0, v0, Lcom/squareup/api/items/FloorPlanTile;->height:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/FloorPlanTile;->DEFAULT_HEIGHT:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getObject()Lcom/squareup/api/sync/ObjectId;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFloorPlanTile;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/FloorPlanTile;

    iget-object v0, v0, Lcom/squareup/api/items/FloorPlanTile;->object:Lcom/squareup/api/sync/ObjectId;

    return-object v0
.end method

.method public getObjectType()Lcom/squareup/api/items/Type;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFloorPlanTile;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/FloorPlanTile;

    iget-object v0, v0, Lcom/squareup/api/items/FloorPlanTile;->object:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    return-object v0
.end method

.method public getPosition()Lcom/squareup/api/items/Point;
    .locals 1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFloorPlanTile;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/FloorPlanTile;

    iget-object v0, v0, Lcom/squareup/api/items/FloorPlanTile;->position:Lcom/squareup/api/items/Point;

    return-object v0
.end method

.method public getReferentTypes()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/api/items/Type;

    .line 64
    sget-object v1, Lcom/squareup/api/items/Type;->FLOOR_PLAN:Lcom/squareup/api/items/Type;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFloorPlanTile;->getObjectType()Lcom/squareup/api/items/Type;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRelations()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 55
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 57
    new-instance v1, Lcom/squareup/shared/catalog/models/CatalogRelation;

    sget-object v2, Lcom/squareup/api/items/Type;->FLOOR_PLAN_TILE:Lcom/squareup/api/items/Type;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFloorPlanTile;->getObjectType()Lcom/squareup/api/items/Type;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/shared/catalog/models/CatalogRelation;-><init>(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)V

    .line 58
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFloorPlanTile;->getObject()Lcom/squareup/api/sync/ObjectId;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_FLOOR_PLAN_TILE_FLOOR_PLAN:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFloorPlanTile;->getFloorPlan()Lcom/squareup/api/sync/ObjectId;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getRotationAngle()I
    .locals 2

    .line 51
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFloorPlanTile;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/FloorPlanTile;

    iget-object v0, v0, Lcom/squareup/api/items/FloorPlanTile;->rotation_angle:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/FloorPlanTile;->DEFAULT_ROTATION_ANGLE:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getWidth()I
    .locals 2

    .line 43
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogFloorPlanTile;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/FloorPlanTile;

    iget-object v0, v0, Lcom/squareup/api/items/FloorPlanTile;->width:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/FloorPlanTile;->DEFAULT_WIDTH:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
