.class public Lcom/squareup/shared/catalog/CatalogObjectCursor;
.super Lcom/squareup/shared/catalog/TypedCursor;
.source "CatalogObjectCursor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/shared/catalog/models/CatalogObject;",
        ">",
        "Lcom/squareup/shared/catalog/TypedCursor<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final objectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;


# direct methods
.method constructor <init>(Ljava/lang/Class;Lcom/squareup/shared/sql/SQLCursor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Lcom/squareup/shared/sql/SQLCursor;",
            ")V"
        }
    .end annotation

    .line 27
    invoke-direct {p0, p2}, Lcom/squareup/shared/catalog/TypedCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;)V

    .line 28
    invoke-static {p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeFromObject(Ljava/lang/Class;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/CatalogObjectCursor;->objectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    return-void
.end method

.method public static from(Ljava/lang/Class;Lcom/squareup/shared/sql/SQLCursor;)Lcom/squareup/shared/catalog/CatalogObjectCursor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Lcom/squareup/shared/sql/SQLCursor;",
            ")",
            "Lcom/squareup/shared/catalog/CatalogObjectCursor<",
            "TT;>;"
        }
    .end annotation

    .line 21
    new-instance v0, Lcom/squareup/shared/catalog/CatalogObjectCursor;

    invoke-direct {v0, p0, p1}, Lcom/squareup/shared/catalog/CatalogObjectCursor;-><init>(Ljava/lang/Class;Lcom/squareup/shared/sql/SQLCursor;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 32
    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/CatalogObjectCursor;->getBlob(I)[B

    move-result-object v0

    .line 35
    :try_start_0
    sget-object v1, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v1, v0}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectWrapper;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    iget-object v1, p0, Lcom/squareup/shared/catalog/CatalogObjectCursor;->objectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    .line 37
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/CatalogObjectCursor;->get()Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v0

    return-object v0
.end method
