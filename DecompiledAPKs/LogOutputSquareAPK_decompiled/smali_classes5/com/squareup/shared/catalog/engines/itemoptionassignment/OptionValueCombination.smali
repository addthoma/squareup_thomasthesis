.class public interface abstract Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;
.super Ljava/lang/Object;
.source "OptionValueCombination.java"


# virtual methods
.method public abstract copyOptionValueIdPairs()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getName(Ljava/util/Map;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method public abstract optionValueCount()I
.end method
