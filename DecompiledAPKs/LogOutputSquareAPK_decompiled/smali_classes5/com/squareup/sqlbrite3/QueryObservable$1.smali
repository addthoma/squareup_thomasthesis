.class final Lcom/squareup/sqlbrite3/QueryObservable$1;
.super Ljava/lang/Object;
.source "QueryObservable.java"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqlbrite3/QueryObservable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Lio/reactivex/Observable<",
        "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
        ">;",
        "Lcom/squareup/sqlbrite3/QueryObservable;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lio/reactivex/Observable;)Lcom/squareup/sqlbrite3/QueryObservable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;)",
            "Lcom/squareup/sqlbrite3/QueryObservable;"
        }
    .end annotation

    .line 20
    new-instance v0, Lcom/squareup/sqlbrite3/QueryObservable;

    invoke-direct {v0, p1}, Lcom/squareup/sqlbrite3/QueryObservable;-><init>(Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 18
    check-cast p1, Lio/reactivex/Observable;

    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/QueryObservable$1;->apply(Lio/reactivex/Observable;)Lcom/squareup/sqlbrite3/QueryObservable;

    move-result-object p1

    return-object p1
.end method
