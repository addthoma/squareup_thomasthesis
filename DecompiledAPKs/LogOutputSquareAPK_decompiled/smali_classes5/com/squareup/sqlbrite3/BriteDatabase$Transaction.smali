.class public interface abstract Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;
.super Ljava/lang/Object;
.source "BriteDatabase.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqlbrite3/BriteDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Transaction"
.end annotation


# virtual methods
.method public abstract close()V
.end method

.method public abstract end()V
.end method

.method public abstract markSuccessful()V
.end method

.method public abstract yieldIfContendedSafely()Z
.end method

.method public abstract yieldIfContendedSafely(JLjava/util/concurrent/TimeUnit;)Z
.end method
