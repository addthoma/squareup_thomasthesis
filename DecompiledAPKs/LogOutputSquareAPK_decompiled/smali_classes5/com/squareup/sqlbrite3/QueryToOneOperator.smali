.class final Lcom/squareup/sqlbrite3/QueryToOneOperator;
.super Ljava/lang/Object;
.source "QueryToOneOperator.java"

# interfaces
.implements Lio/reactivex/ObservableOperator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sqlbrite3/QueryToOneOperator$MappingObserver;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOperator<",
        "TT;",
        "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
        ">;"
    }
.end annotation


# instance fields
.field private final defaultValue:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mapper:Lio/reactivex/functions/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/functions/Function;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;TT;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/sqlbrite3/QueryToOneOperator;->mapper:Lio/reactivex/functions/Function;

    .line 34
    iput-object p2, p0, Lcom/squareup/sqlbrite3/QueryToOneOperator;->defaultValue:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public apply(Lio/reactivex/Observer;)Lio/reactivex/Observer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer<",
            "-TT;>;)",
            "Lio/reactivex/Observer<",
            "-",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/sqlbrite3/QueryToOneOperator$MappingObserver;

    iget-object v1, p0, Lcom/squareup/sqlbrite3/QueryToOneOperator;->mapper:Lio/reactivex/functions/Function;

    iget-object v2, p0, Lcom/squareup/sqlbrite3/QueryToOneOperator;->defaultValue:Ljava/lang/Object;

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/sqlbrite3/QueryToOneOperator$MappingObserver;-><init>(Lio/reactivex/Observer;Lio/reactivex/functions/Function;Ljava/lang/Object;)V

    return-object v0
.end method
