.class public final Lcom/squareup/time/RealCurrentTime;
.super Ljava/lang/Object;
.source "RealCurrentTime.kt"

# interfaces
.implements Lcom/squareup/time/CurrentTime;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/time/RealCurrentTime$RealClock;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u0007B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/time/RealCurrentTime;",
        "Lcom/squareup/time/CurrentTime;",
        "currentTimeZone",
        "Lcom/squareup/time/CurrentTimeZone;",
        "(Lcom/squareup/time/CurrentTimeZone;)V",
        "clock",
        "Lorg/threeten/bp/Clock;",
        "RealClock",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentTimeZone:Lcom/squareup/time/CurrentTimeZone;


# direct methods
.method public constructor <init>(Lcom/squareup/time/CurrentTimeZone;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "currentTimeZone"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/time/RealCurrentTime;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    return-void
.end method

.method public static final synthetic access$getCurrentTimeZone$p(Lcom/squareup/time/RealCurrentTime;)Lcom/squareup/time/CurrentTimeZone;
    .locals 0

    .line 8
    iget-object p0, p0, Lcom/squareup/time/RealCurrentTime;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    return-object p0
.end method


# virtual methods
.method public clock()Lorg/threeten/bp/Clock;
    .locals 2

    .line 12
    new-instance v0, Lcom/squareup/time/RealCurrentTime$RealClock;

    new-instance v1, Lcom/squareup/time/RealCurrentTime$clock$1;

    invoke-direct {v1, p0}, Lcom/squareup/time/RealCurrentTime$clock$1;-><init>(Lcom/squareup/time/RealCurrentTime;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-direct {v0, v1}, Lcom/squareup/time/RealCurrentTime$RealClock;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lorg/threeten/bp/Clock;

    return-object v0
.end method

.method public instant()Lorg/threeten/bp/Instant;
    .locals 1

    .line 8
    invoke-static {p0}, Lcom/squareup/time/CurrentTime$DefaultImpls;->instant(Lcom/squareup/time/CurrentTime;)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0
.end method

.method public localDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 8
    invoke-static {p0}, Lcom/squareup/time/CurrentTime$DefaultImpls;->localDate(Lcom/squareup/time/CurrentTime;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public localDateTime()Lorg/threeten/bp/LocalDateTime;
    .locals 1

    .line 8
    invoke-static {p0}, Lcom/squareup/time/CurrentTime$DefaultImpls;->localDateTime(Lcom/squareup/time/CurrentTime;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    return-object v0
.end method

.method public localTime()Lorg/threeten/bp/LocalTime;
    .locals 1

    .line 8
    invoke-static {p0}, Lcom/squareup/time/CurrentTime$DefaultImpls;->localTime(Lcom/squareup/time/CurrentTime;)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public zonedDateTime()Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    .line 8
    invoke-static {p0}, Lcom/squareup/time/CurrentTime$DefaultImpls;->zonedDateTime(Lcom/squareup/time/CurrentTime;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method
