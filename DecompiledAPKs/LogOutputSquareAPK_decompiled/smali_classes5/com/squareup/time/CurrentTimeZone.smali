.class public interface abstract Lcom/squareup/time/CurrentTimeZone;
.super Ljava/lang/Object;
.source "CurrentTimeZone.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0004H&R\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/time/CurrentTimeZone;",
        "",
        "zoneId",
        "Lio/reactivex/Observable;",
        "Lorg/threeten/bp/ZoneId;",
        "getZoneId",
        "()Lio/reactivex/Observable;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getZoneId()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lorg/threeten/bp/ZoneId;",
            ">;"
        }
    .end annotation
.end method

.method public abstract zoneId()Lorg/threeten/bp/ZoneId;
.end method
