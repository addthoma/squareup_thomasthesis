.class public final Lcom/squareup/time/AvailableTimeZones;
.super Ljava/lang/Object;
.source "AvailableTimeZones.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/time/AvailableTimeZones$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAvailableTimeZones.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AvailableTimeZones.kt\ncom/squareup/time/AvailableTimeZones\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,38:1\n310#2,7:39\n*E\n*S KotlinDebug\n*F\n+ 1 AvailableTimeZones.kt\ncom/squareup/time/AvailableTimeZones\n*L\n20#1,7:39\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0018\u0000 \u000e2\u00020\u0001:\u0001\u000eB\u0013\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000e\u0010\u0008\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\nR\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/time/AvailableTimeZones;",
        "",
        "allTimeZones",
        "",
        "Lcom/squareup/time/TimeZoneDescriptor;",
        "(Ljava/util/List;)V",
        "getAllTimeZones",
        "()Ljava/util/List;",
        "descriptor",
        "zoneId",
        "Lorg/threeten/bp/ZoneId;",
        "getCurrentTimeZoneIndex",
        "",
        "currentTimeZone",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/time/AvailableTimeZones$Companion;

.field public static final UNKNOWN_NAME:I = -0x1


# instance fields
.field private final allTimeZones:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/time/TimeZoneDescriptor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/time/AvailableTimeZones$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/time/AvailableTimeZones$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/time/AvailableTimeZones;->Companion:Lcom/squareup/time/AvailableTimeZones$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/time/TimeZoneDescriptor;",
            ">;)V"
        }
    .end annotation

    const-string v0, "allTimeZones"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/time/AvailableTimeZones;->allTimeZones:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final descriptor(Lorg/threeten/bp/ZoneId;)Lcom/squareup/time/TimeZoneDescriptor;
    .locals 3

    const-string/jumbo v0, "zoneId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/squareup/time/AvailableTimeZones;->allTimeZones:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/time/TimeZoneDescriptor;

    invoke-virtual {v2}, Lcom/squareup/time/TimeZoneDescriptor;->getTimeZone()Lorg/threeten/bp/ZoneId;

    move-result-object v2

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    check-cast v1, Lcom/squareup/time/TimeZoneDescriptor;

    if-eqz v1, :cond_2

    goto :goto_1

    .line 25
    :cond_2
    new-instance v1, Lcom/squareup/time/TimeZoneDescriptor;

    const/4 v0, -0x1

    invoke-direct {v1, p1, v0}, Lcom/squareup/time/TimeZoneDescriptor;-><init>(Lorg/threeten/bp/ZoneId;I)V

    :goto_1
    return-object v1
.end method

.method public final getAllTimeZones()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/time/TimeZoneDescriptor;",
            ">;"
        }
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/squareup/time/AvailableTimeZones;->allTimeZones:Ljava/util/List;

    return-object v0
.end method

.method public final getCurrentTimeZoneIndex(Lorg/threeten/bp/ZoneId;)I
    .locals 3

    const-string v0, "currentTimeZone"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lcom/squareup/time/AvailableTimeZones;->allTimeZones:Ljava/util/List;

    .line 40
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 41
    check-cast v2, Lcom/squareup/time/TimeZoneDescriptor;

    .line 20
    invoke-virtual {v2}, Lcom/squareup/time/TimeZoneDescriptor;->getTimeZone()Lorg/threeten/bp/ZoneId;

    move-result-object v2

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    :goto_1
    return v1
.end method
