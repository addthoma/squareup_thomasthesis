.class public final Lcom/squareup/sqldelight/internal/MathKt;
.super Ljava/lang/Object;
.source "Math.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMath.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Math.kt\ncom/squareup/sqldelight/internal/MathKt\n*L\n1#1,22:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0001H\u0000\u00a8\u0006\u0004"
    }
    d2 = {
        "presizeArguments",
        "",
        "count",
        "offset",
        "runtime"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final presizeArguments(II)I
    .locals 9

    const/high16 v0, 0x41200000    # 10.0f

    float-to-double v0, v0

    const/4 v2, 0x0

    int-to-double v3, v2

    .line 14
    invoke-static {v0, v1, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    double-to-float v3, v3

    invoke-static {v3}, Lkotlin/math/MathKt;->roundToInt(F)I

    move-result v3

    const/4 v4, 0x0

    :goto_0
    add-int v5, p1, p0

    if-le v5, v3, :cond_0

    add-int/lit8 v4, v4, 0x1

    int-to-double v6, v4

    .line 18
    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    double-to-float v6, v6

    invoke-static {v6}, Lkotlin/math/MathKt;->roundToInt(F)I

    move-result v6

    add-int/lit8 v7, v4, 0x1

    .line 19
    invoke-static {v6, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v6, p1}, Ljava/lang/Math;->min(II)I

    move-result v8

    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    sub-int/2addr v5, v3

    mul-int v7, v7, v5

    add-int/2addr v2, v7

    move v3, v6

    goto :goto_0

    :cond_0
    add-int/lit8 v2, v2, 0x2

    add-int/lit8 p0, p0, -0x1

    mul-int/lit8 p0, p0, 0x2

    add-int/2addr v2, p0

    return v2
.end method
