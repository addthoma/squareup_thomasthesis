.class public interface abstract Lcom/squareup/statusbar/event/StatusBarEventManager;
.super Ljava/lang/Object;
.source "StatusBarEventManager.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/statusbar/event/StatusBarEventManager$ChargerType;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$ConfigureDeviceButtonState;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$EmployeeManagementButtonState;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationButtonState;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$SoftwareUpdateState;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$LowBatteryDialogStatus;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\r\u0008f\u0018\u00002\u00020\u0001:\tCDEFGHIJKJ\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\u0005H\u0016J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\u0005H\u0016J\u000e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cH\u0016J\u0008\u0010\u000e\u001a\u00020\u0003H\u0016J\u0018\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00032\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\rH\u0016J\u0010\u0010\u001e\u001a\u00020\u00032\u0006\u0010\u001f\u001a\u00020\rH\u0016J\u0010\u0010 \u001a\u00020\u00032\u0006\u0010!\u001a\u00020\"H\u0016J\u0010\u0010#\u001a\u00020\u00032\u0006\u0010$\u001a\u00020\rH\u0016J\u0010\u0010%\u001a\u00020\u00032\u0006\u0010&\u001a\u00020\rH\u0016J\u0010\u0010\'\u001a\u00020\u00032\u0006\u0010(\u001a\u00020)H\u0016J\u0010\u0010*\u001a\u00020\u00032\u0006\u0010+\u001a\u00020,H\u0016J\u0010\u0010-\u001a\u00020\u00032\u0006\u0010.\u001a\u00020/H\u0016J\u0018\u00100\u001a\u00020\u00032\u0006\u00101\u001a\u00020\r2\u0006\u00102\u001a\u00020\rH&J\u0008\u00103\u001a\u00020\u0003H\u0016J\u0010\u00104\u001a\u00020\u00032\u0006\u00105\u001a\u00020\rH\u0016J\u0008\u00106\u001a\u00020\u0003H&J\u0010\u00107\u001a\u00020\u00032\u0006\u00108\u001a\u000209H&J\u0008\u0010:\u001a\u00020\u0003H&J\u0010\u0010;\u001a\u00020\u00032\u0006\u0010<\u001a\u00020\rH&J\u0010\u0010=\u001a\u00020\u00032\u0006\u0010>\u001a\u00020?H\u0016J\u000e\u0010@\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u000cH&J\u0008\u0010A\u001a\u00020\u0003H\u0016J\u0008\u0010B\u001a\u00020\u0003H\u0016\u00a8\u0006L"
    }
    d2 = {
        "Lcom/squareup/statusbar/event/StatusBarEventManager;",
        "Lmortar/Scoped;",
        "acceptViewClickEvent",
        "",
        "action",
        "",
        "enterFullscreenMode",
        "until",
        "Lio/reactivex/Completable;",
        "sessionKey",
        "exitFullscreenMode",
        "inFullscreenMode",
        "Lio/reactivex/Observable;",
        "",
        "launchSignInOrRegister",
        "onBadgesChanged",
        "applet",
        "badgeCount",
        "",
        "onBranStatusChanged",
        "branStatus",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus;",
        "onChargerTypeChanged",
        "chargerType",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$ChargerType;",
        "onConfigureDeviceButtonStateChanged",
        "configureDeviceButtonState",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$ConfigureDeviceButtonState;",
        "onCriticalSoftwareUpdateAvailable",
        "criticalUpdateAvailable",
        "onEditModeChanged",
        "inEditMode",
        "onEmployeeManagementButtonStateChanged",
        "employeeManagementButtonState",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$EmployeeManagementButtonState;",
        "onInternalPrinterOutOfPaper",
        "outOfPaper",
        "onInternalPrinterUnrecoverableError",
        "unrecoverableError",
        "onLowBatteryStatusChanged",
        "lowBatteryDialogStatus",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$LowBatteryDialogStatus;",
        "onNotificationButtonStateChanged",
        "notificationButtonState",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationButtonState;",
        "onNotificationCenterStateChanged",
        "notificationCenterState",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState;",
        "onOfflineStateChanged",
        "isInOfflineMode",
        "isInternetStateConnected",
        "onOobFinished",
        "onPrinterDoorStateChanged",
        "open",
        "onSignedOut",
        "onSoftwareUpdateStateChanged",
        "softwareUpdateState",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$SoftwareUpdateState;",
        "onStatusBarAttachChanged",
        "onStatusBarAvailable",
        "available",
        "onSwitcherStateChanged",
        "switcherState",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState;",
        "onViewClicked",
        "showBfdUpdateStatusDialog",
        "showNotificationListDialog",
        "BranStatus",
        "ChargerType",
        "ConfigureDeviceButtonState",
        "EmployeeManagementButtonState",
        "LowBatteryDialogStatus",
        "NotificationButtonState",
        "NotificationCenterState",
        "SoftwareUpdateState",
        "SwitcherState",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract acceptViewClickEvent(Ljava/lang/String;)V
.end method

.method public abstract enterFullscreenMode(Lio/reactivex/Completable;)V
.end method

.method public abstract enterFullscreenMode(Ljava/lang/String;)V
.end method

.method public abstract exitFullscreenMode(Ljava/lang/String;)V
.end method

.method public abstract inFullscreenMode()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract launchSignInOrRegister()V
.end method

.method public abstract onBadgesChanged(Ljava/lang/String;I)V
.end method

.method public abstract onBranStatusChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus;)V
.end method

.method public abstract onChargerTypeChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$ChargerType;)V
.end method

.method public abstract onConfigureDeviceButtonStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$ConfigureDeviceButtonState;)V
.end method

.method public abstract onCriticalSoftwareUpdateAvailable(Z)V
.end method

.method public abstract onEditModeChanged(Z)V
.end method

.method public abstract onEmployeeManagementButtonStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$EmployeeManagementButtonState;)V
.end method

.method public abstract onInternalPrinterOutOfPaper(Z)V
.end method

.method public abstract onInternalPrinterUnrecoverableError(Z)V
.end method

.method public abstract onLowBatteryStatusChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$LowBatteryDialogStatus;)V
.end method

.method public abstract onNotificationButtonStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationButtonState;)V
.end method

.method public abstract onNotificationCenterStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState;)V
.end method

.method public abstract onOfflineStateChanged(ZZ)V
.end method

.method public abstract onOobFinished()V
.end method

.method public abstract onPrinterDoorStateChanged(Z)V
.end method

.method public abstract onSignedOut()V
.end method

.method public abstract onSoftwareUpdateStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$SoftwareUpdateState;)V
.end method

.method public abstract onStatusBarAttachChanged()V
.end method

.method public abstract onStatusBarAvailable(Z)V
.end method

.method public abstract onSwitcherStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState;)V
.end method

.method public abstract onViewClicked()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract showBfdUpdateStatusDialog()V
.end method

.method public abstract showNotificationListDialog()V
.end method
