.class public final Lcom/squareup/statusbar/event/NoStatusBarAppModule_ProvideStatusBarEventManagerFactory;
.super Ljava/lang/Object;
.source "NoStatusBarAppModule_ProvideStatusBarEventManagerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/statusbar/event/NoStatusBarAppModule_ProvideStatusBarEventManagerFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/statusbar/event/StatusBarEventManager;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/statusbar/event/NoStatusBarAppModule_ProvideStatusBarEventManagerFactory;
    .locals 1

    .line 22
    invoke-static {}, Lcom/squareup/statusbar/event/NoStatusBarAppModule_ProvideStatusBarEventManagerFactory$InstanceHolder;->access$000()Lcom/squareup/statusbar/event/NoStatusBarAppModule_ProvideStatusBarEventManagerFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideStatusBarEventManager()Lcom/squareup/statusbar/event/StatusBarEventManager;
    .locals 2

    .line 26
    invoke-static {}, Lcom/squareup/statusbar/event/NoStatusBarAppModule;->provideStatusBarEventManager()Lcom/squareup/statusbar/event/StatusBarEventManager;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/statusbar/event/StatusBarEventManager;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/statusbar/event/StatusBarEventManager;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/statusbar/event/NoStatusBarAppModule_ProvideStatusBarEventManagerFactory;->provideStatusBarEventManager()Lcom/squareup/statusbar/event/StatusBarEventManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/statusbar/event/NoStatusBarAppModule_ProvideStatusBarEventManagerFactory;->get()Lcom/squareup/statusbar/event/StatusBarEventManager;

    move-result-object v0

    return-object v0
.end method
