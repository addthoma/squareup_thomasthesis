.class public final Lcom/squareup/statusbar/event/EmptySwitcherAppletsModule_ProvideSwitcherAppletsFactory;
.super Ljava/lang/Object;
.source "EmptySwitcherAppletsModule_ProvideSwitcherAppletsFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/statusbar/event/EmptySwitcherAppletsModule_ProvideSwitcherAppletsFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/statusbar/event/SwitcherApplets;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/statusbar/event/EmptySwitcherAppletsModule_ProvideSwitcherAppletsFactory;
    .locals 1

    .line 22
    invoke-static {}, Lcom/squareup/statusbar/event/EmptySwitcherAppletsModule_ProvideSwitcherAppletsFactory$InstanceHolder;->access$000()Lcom/squareup/statusbar/event/EmptySwitcherAppletsModule_ProvideSwitcherAppletsFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideSwitcherApplets()Lcom/squareup/statusbar/event/SwitcherApplets;
    .locals 2

    .line 26
    invoke-static {}, Lcom/squareup/statusbar/event/EmptySwitcherAppletsModule;->provideSwitcherApplets()Lcom/squareup/statusbar/event/SwitcherApplets;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/statusbar/event/SwitcherApplets;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/statusbar/event/SwitcherApplets;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/statusbar/event/EmptySwitcherAppletsModule_ProvideSwitcherAppletsFactory;->provideSwitcherApplets()Lcom/squareup/statusbar/event/SwitcherApplets;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/statusbar/event/EmptySwitcherAppletsModule_ProvideSwitcherAppletsFactory;->get()Lcom/squareup/statusbar/event/SwitcherApplets;

    move-result-object v0

    return-object v0
.end method
