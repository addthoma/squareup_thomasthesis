.class public interface abstract Lcom/squareup/server/referral/ReferralService;
.super Ljava/lang/Object;
.source "ReferralService.java"


# virtual methods
.method public abstract referral(Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/onboard/signup-token"
    .end annotation
.end method
