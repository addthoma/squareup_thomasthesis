.class public final Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedProtoRestAdapterFactory;
.super Ljava/lang/Object;
.source "RestAdapterCommonModule_ProvideUnauthenticatedProtoRestAdapterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lretrofit/RestAdapter;",
        ">;"
    }
.end annotation


# instance fields
.field private final clientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit/client/Client;",
            ">;"
        }
    .end annotation
.end field

.field private final executorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final serverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lretrofit/client/Client;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedProtoRestAdapterFactory;->serverProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedProtoRestAdapterFactory;->clientProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedProtoRestAdapterFactory;->executorProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedProtoRestAdapterFactory;->mainThreadProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedProtoRestAdapterFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lretrofit/client/Client;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)",
            "Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedProtoRestAdapterFactory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedProtoRestAdapterFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedProtoRestAdapterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideUnauthenticatedProtoRestAdapter(Lcom/squareup/http/Server;Lretrofit/client/Client;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;)Lretrofit/RestAdapter;
    .locals 0

    .line 52
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/server/RestAdapterCommonModule;->provideUnauthenticatedProtoRestAdapter(Lcom/squareup/http/Server;Lretrofit/client/Client;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;)Lretrofit/RestAdapter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lretrofit/RestAdapter;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedProtoRestAdapterFactory;->get()Lretrofit/RestAdapter;

    move-result-object v0

    return-object v0
.end method

.method public get()Lretrofit/RestAdapter;
    .locals 4

    .line 41
    iget-object v0, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedProtoRestAdapterFactory;->serverProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/http/Server;

    iget-object v1, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedProtoRestAdapterFactory;->clientProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lretrofit/client/Client;

    iget-object v2, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedProtoRestAdapterFactory;->executorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedProtoRestAdapterFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/thread/executor/MainThread;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedProtoRestAdapterFactory;->provideUnauthenticatedProtoRestAdapter(Lcom/squareup/http/Server;Lretrofit/client/Client;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;)Lretrofit/RestAdapter;

    move-result-object v0

    return-object v0
.end method
