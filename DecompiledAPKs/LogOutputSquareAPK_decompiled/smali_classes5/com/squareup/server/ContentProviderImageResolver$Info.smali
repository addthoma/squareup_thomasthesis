.class Lcom/squareup/server/ContentProviderImageResolver$Info;
.super Ljava/lang/Object;
.source "ContentProviderImageResolver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/ContentProviderImageResolver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Info"
.end annotation


# instance fields
.field final length:J

.field final mimeType:Ljava/lang/String;

.field final name:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0

    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    iput-object p1, p0, Lcom/squareup/server/ContentProviderImageResolver$Info;->name:Ljava/lang/String;

    .line 186
    iput-object p2, p0, Lcom/squareup/server/ContentProviderImageResolver$Info;->mimeType:Ljava/lang/String;

    .line 187
    iput-wide p3, p0, Lcom/squareup/server/ContentProviderImageResolver$Info;->length:J

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;JLcom/squareup/server/ContentProviderImageResolver$1;)V
    .locals 0

    .line 179
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/server/ContentProviderImageResolver$Info;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method
