.class public Lcom/squareup/server/activation/QuizQuestion;
.super Ljava/lang/Object;
.source "QuizQuestion.java"


# instance fields
.field public final answers:[Ljava/lang/String;

.field public final id:Ljava/lang/String;

.field public final text:Ljava/lang/String;


# direct methods
.method public varargs constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/server/activation/QuizQuestion;->id:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lcom/squareup/server/activation/QuizQuestion;->text:Ljava/lang/String;

    .line 14
    iput-object p3, p0, Lcom/squareup/server/activation/QuizQuestion;->answers:[Ljava/lang/String;

    return-void
.end method
