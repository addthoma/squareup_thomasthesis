.class public Lcom/squareup/server/activation/PurchaseConfirmation;
.super Ljava/lang/Object;
.source "PurchaseConfirmation.java"


# instance fields
.field public final state:Ljava/lang/String;

.field public final token:Ljava/lang/String;

.field public final user_email:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/squareup/server/activation/PurchaseConfirmation;->state:Ljava/lang/String;

    .line 10
    iput-object p2, p0, Lcom/squareup/server/activation/PurchaseConfirmation;->token:Ljava/lang/String;

    .line 11
    iput-object p3, p0, Lcom/squareup/server/activation/PurchaseConfirmation;->user_email:Ljava/lang/String;

    return-void
.end method
