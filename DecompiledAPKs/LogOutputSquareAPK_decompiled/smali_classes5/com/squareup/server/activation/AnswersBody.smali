.class public Lcom/squareup/server/activation/AnswersBody;
.super Ljava/lang/Object;
.source "AnswersBody.java"


# instance fields
.field public final answers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/activation/QuizAnswer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/activation/QuizAnswer;",
            ">;)V"
        }
    .end annotation

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/squareup/server/activation/AnswersBody;->answers:Ljava/util/List;

    return-void
.end method
