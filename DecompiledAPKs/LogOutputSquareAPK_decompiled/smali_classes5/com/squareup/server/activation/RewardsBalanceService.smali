.class public interface abstract Lcom/squareup/server/activation/RewardsBalanceService;
.super Ljava/lang/Object;
.source "RewardsBalanceService.java"


# virtual methods
.method public abstract getBalance(Lcom/squareup/protos/client/rewards/GetMerchantBalanceRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/rewards/GetMerchantBalanceRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rewards/GetMerchantBalanceRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/onboard/get-merchant-balance"
    .end annotation
.end method
