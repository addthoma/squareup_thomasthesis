.class public final Lcom/squareup/server/CancelableSquareCallback;
.super Lcom/squareup/server/SquareCallback;
.source "CancelableSquareCallback.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/server/SquareCallback<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use RxJava directly instead."
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0003\n\u0000\u0008\u0007\u0018\u0000*\u0004\u0008\u0000\u0010\u00012\u0008\u0012\u0004\u0012\u0002H\u00010\u0002B\u0013\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0002\u00a2\u0006\u0002\u0010\u0004J\u0015\u0010\u000b\u001a\u00020\t2\u0006\u0010\u000c\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\rJ\u000e\u0010\u000e\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\u0010J\u001d\u0010\u0011\u001a\u00020\t2\u0006\u0010\u000c\u001a\u00028\u00002\u0006\u0010\u0012\u001a\u00020\u0013H\u0016\u00a2\u0006\u0002\u0010\u0014J\u0008\u0010\u0015\u001a\u00020\tH\u0002J\u0008\u0010\u0016\u001a\u00020\tH\u0016J\u0006\u0010\u0017\u001a\u00020\u0018J\u0010\u0010\u0019\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0008\u0010\u001a\u001a\u00020\tH\u0016J\u0012\u0010\u001b\u001a\u00020\t2\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0002X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0007\u001a\u0010\u0012\u000c\u0012\n \n*\u0004\u0018\u00010\t0\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/server/CancelableSquareCallback;",
        "T",
        "Lcom/squareup/server/SquareCallback;",
        "callback",
        "(Lcom/squareup/server/SquareCallback;)V",
        "completed",
        "",
        "completionSubject",
        "Lio/reactivex/subjects/PublishSubject;",
        "",
        "kotlin.jvm.PlatformType",
        "call",
        "response",
        "(Ljava/lang/Object;)V",
        "cancelWhenScopeExits",
        "scope",
        "Lmortar/MortarScope;",
        "clientError",
        "statusCode",
        "",
        "(Ljava/lang/Object;I)V",
        "complete",
        "networkError",
        "observeCompletion",
        "Lio/reactivex/Completable;",
        "serverError",
        "sessionExpired",
        "unexpectedError",
        "t",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final callback:Lcom/squareup/server/SquareCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/server/SquareCallback<",
            "TT;>;"
        }
    .end annotation
.end field

.field private completed:Z

.field private final completionSubject:Lio/reactivex/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/PublishSubject<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/server/SquareCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "callback"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Lcom/squareup/server/SquareCallback;-><init>()V

    iput-object p1, p0, Lcom/squareup/server/CancelableSquareCallback;->callback:Lcom/squareup/server/SquareCallback;

    .line 14
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object p1

    const-string v0, "PublishSubject.create<Unit>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/server/CancelableSquareCallback;->completionSubject:Lio/reactivex/subjects/PublishSubject;

    return-void
.end method

.method public static final synthetic access$complete(Lcom/squareup/server/CancelableSquareCallback;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/server/CancelableSquareCallback;->complete()V

    return-void
.end method

.method private final complete()V
    .locals 1

    .line 66
    iget-boolean v0, p0, Lcom/squareup/server/CancelableSquareCallback;->completed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 67
    iput-boolean v0, p0, Lcom/squareup/server/CancelableSquareCallback;->completed:Z

    .line 68
    iget-object v0, p0, Lcom/squareup/server/CancelableSquareCallback;->completionSubject:Lio/reactivex/subjects/PublishSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/PublishSubject;->onComplete()V

    :cond_0
    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 17
    iget-boolean v0, p0, Lcom/squareup/server/CancelableSquareCallback;->completed:Z

    if-nez v0, :cond_0

    .line 18
    iget-object v0, p0, Lcom/squareup/server/CancelableSquareCallback;->callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, p1}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    .line 19
    invoke-direct {p0}, Lcom/squareup/server/CancelableSquareCallback;->complete()V

    :cond_0
    return-void
.end method

.method public final cancelWhenScopeExits(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-static {p1}, Lcom/squareup/mortar/MortarScopes;->completeOnExit(Lmortar/MortarScope;)Lio/reactivex/Completable;

    move-result-object p1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/server/CancelableSquareCallback;->observeCompletion()Lio/reactivex/Completable;

    move-result-object v0

    check-cast v0, Lio/reactivex/CompletableSource;

    invoke-virtual {p1, v0}, Lio/reactivex/Completable;->ambWith(Lio/reactivex/CompletableSource;)Lio/reactivex/Completable;

    move-result-object p1

    .line 78
    new-instance v0, Lcom/squareup/server/CancelableSquareCallback$cancelWhenScopeExits$1;

    invoke-direct {v0, p0}, Lcom/squareup/server/CancelableSquareCallback$cancelWhenScopeExits$1;-><init>(Lcom/squareup/server/CancelableSquareCallback;)V

    check-cast v0, Lio/reactivex/functions/Action;

    invoke-virtual {p1, v0}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public clientError(Ljava/lang/Object;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation

    .line 34
    iget-boolean v0, p0, Lcom/squareup/server/CancelableSquareCallback;->completed:Z

    if-nez v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/squareup/server/CancelableSquareCallback;->callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/server/SquareCallback;->clientError(Ljava/lang/Object;I)V

    .line 36
    invoke-direct {p0}, Lcom/squareup/server/CancelableSquareCallback;->complete()V

    :cond_0
    return-void
.end method

.method public networkError()V
    .locals 1

    .line 48
    iget-boolean v0, p0, Lcom/squareup/server/CancelableSquareCallback;->completed:Z

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/squareup/server/CancelableSquareCallback;->callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0}, Lcom/squareup/server/SquareCallback;->networkError()V

    .line 50
    invoke-direct {p0}, Lcom/squareup/server/CancelableSquareCallback;->complete()V

    :cond_0
    return-void
.end method

.method public final observeCompletion()Lio/reactivex/Completable;
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/squareup/server/CancelableSquareCallback;->completionSubject:Lio/reactivex/subjects/PublishSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/PublishSubject;->ignoreElements()Lio/reactivex/Completable;

    move-result-object v0

    const-string v1, "completionSubject.ignoreElements()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public serverError(I)V
    .locals 1

    .line 41
    iget-boolean v0, p0, Lcom/squareup/server/CancelableSquareCallback;->completed:Z

    if-nez v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/squareup/server/CancelableSquareCallback;->callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, p1}, Lcom/squareup/server/SquareCallback;->serverError(I)V

    .line 43
    invoke-direct {p0}, Lcom/squareup/server/CancelableSquareCallback;->complete()V

    :cond_0
    return-void
.end method

.method public sessionExpired()V
    .locals 1

    .line 24
    iget-boolean v0, p0, Lcom/squareup/server/CancelableSquareCallback;->completed:Z

    if-nez v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/squareup/server/CancelableSquareCallback;->callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0}, Lcom/squareup/server/SquareCallback;->sessionExpired()V

    .line 26
    invoke-direct {p0}, Lcom/squareup/server/CancelableSquareCallback;->complete()V

    :cond_0
    return-void
.end method

.method public unexpectedError(Ljava/lang/Throwable;)V
    .locals 1

    .line 55
    iget-boolean v0, p0, Lcom/squareup/server/CancelableSquareCallback;->completed:Z

    if-nez v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/squareup/server/CancelableSquareCallback;->callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, p1}, Lcom/squareup/server/SquareCallback;->unexpectedError(Ljava/lang/Throwable;)V

    .line 57
    invoke-direct {p0}, Lcom/squareup/server/CancelableSquareCallback;->complete()V

    goto :goto_0

    .line 59
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/server/SquareCallback;->unexpectedError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
