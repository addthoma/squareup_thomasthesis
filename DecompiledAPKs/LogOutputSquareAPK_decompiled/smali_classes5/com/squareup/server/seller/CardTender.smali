.class public Lcom/squareup/server/seller/CardTender;
.super Ljava/lang/Object;
.source "CardTender.java"


# instance fields
.field public final keyed_card:Lcom/squareup/server/seller/KeyedCard;

.field public final swiped_card:Lcom/squareup/server/seller/SwipedCard;


# direct methods
.method public constructor <init>(Lcom/squareup/server/seller/KeyedCard;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/server/seller/CardTender;->keyed_card:Lcom/squareup/server/seller/KeyedCard;

    const/4 p1, 0x0

    .line 17
    iput-object p1, p0, Lcom/squareup/server/seller/CardTender;->swiped_card:Lcom/squareup/server/seller/SwipedCard;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/seller/SwipedCard;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/server/seller/CardTender;->swiped_card:Lcom/squareup/server/seller/SwipedCard;

    const/4 p1, 0x0

    .line 22
    iput-object p1, p0, Lcom/squareup/server/seller/CardTender;->keyed_card:Lcom/squareup/server/seller/KeyedCard;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 26
    instance-of v1, p1, Lcom/squareup/server/seller/CardTender;

    if-nez v1, :cond_0

    goto :goto_0

    .line 27
    :cond_0
    check-cast p1, Lcom/squareup/server/seller/CardTender;

    .line 28
    iget-object v1, p0, Lcom/squareup/server/seller/CardTender;->keyed_card:Lcom/squareup/server/seller/KeyedCard;

    iget-object v2, p1, Lcom/squareup/server/seller/CardTender;->keyed_card:Lcom/squareup/server/seller/KeyedCard;

    invoke-static {v1, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/server/seller/CardTender;->swiped_card:Lcom/squareup/server/seller/SwipedCard;

    iget-object p1, p1, Lcom/squareup/server/seller/CardTender;->swiped_card:Lcom/squareup/server/seller/SwipedCard;

    invoke-static {v1, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 32
    iget-object v1, p0, Lcom/squareup/server/seller/CardTender;->keyed_card:Lcom/squareup/server/seller/KeyedCard;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/seller/CardTender;->swiped_card:Lcom/squareup/server/seller/SwipedCard;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
