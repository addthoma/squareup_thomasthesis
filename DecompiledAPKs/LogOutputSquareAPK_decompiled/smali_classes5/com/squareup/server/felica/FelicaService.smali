.class public interface abstract Lcom/squareup/server/felica/FelicaService;
.super Ljava/lang/Object;
.source "FelicaService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/felica/FelicaService$ProxyMessageStandardResponse;,
        Lcom/squareup/server/felica/FelicaService$SetTransactionStatusStandardResponse;,
        Lcom/squareup/server/felica/FelicaService$DefaultImpls;,
        Lcom/squareup/server/felica/FelicaService$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008f\u0018\u0000 \u000f2\u00020\u0001:\u0003\u000f\u0010\u0011J-\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u00072\n\u0008\u0003\u0010\u0008\u001a\u0004\u0018\u00010\tH\'\u00a2\u0006\u0002\u0010\nJ-\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0001\u0010\u0004\u001a\u00020\r2\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u00072\n\u0008\u0003\u0010\u0008\u001a\u0004\u0018\u00010\tH\'\u00a2\u0006\u0002\u0010\u000e\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/server/felica/FelicaService;",
        "",
        "proxyMessage",
        "Lcom/squareup/server/felica/FelicaService$ProxyMessageStandardResponse;",
        "request",
        "Lcom/squareup/protos/client/felica/ProxyMessageRequest;",
        "transactionId",
        "",
        "felicaCertEnv",
        "",
        "(Lcom/squareup/protos/client/felica/ProxyMessageRequest;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/squareup/server/felica/FelicaService$ProxyMessageStandardResponse;",
        "setTransactionStatus",
        "Lcom/squareup/server/felica/FelicaService$SetTransactionStatusStandardResponse;",
        "Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;",
        "(Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/squareup/server/felica/FelicaService$SetTransactionStatusStandardResponse;",
        "Companion",
        "ProxyMessageStandardResponse",
        "SetTransactionStatusStandardResponse",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/server/felica/FelicaService$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/server/felica/FelicaService$Companion;->$$INSTANCE:Lcom/squareup/server/felica/FelicaService$Companion;

    sput-object v0, Lcom/squareup/server/felica/FelicaService;->Companion:Lcom/squareup/server/felica/FelicaService$Companion;

    return-void
.end method


# virtual methods
.method public abstract proxyMessage(Lcom/squareup/protos/client/felica/ProxyMessageRequest;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/squareup/server/felica/FelicaService$ProxyMessageStandardResponse;
    .param p1    # Lcom/squareup/protos/client/felica/ProxyMessageRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "X-Affinity-Id"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lretrofit2/http/Header;
            value = "X-Enable-Felica-Certification-Environment"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/felica-proxy/proxy-message"
    .end annotation
.end method

.method public abstract setTransactionStatus(Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/squareup/server/felica/FelicaService$SetTransactionStatusStandardResponse;
    .param p1    # Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "X-Affinity-Id"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lretrofit2/http/Header;
            value = "X-Enable-Felica-Certification-Environment"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/felica-proxy/set-transaction-status"
    .end annotation
.end method
