.class public final Lcom/squareup/server/felica/FelicaService$Companion;
.super Ljava/lang/Object;
.source "FelicaService.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/felica/FelicaService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/server/felica/FelicaService$Companion;",
        "",
        "()V",
        "X_AFFINITY_HEADER",
        "",
        "X_FELICA_CERT_ENV_HEADER",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/server/felica/FelicaService$Companion;

.field private static final X_AFFINITY_HEADER:Ljava/lang/String; = "X-Affinity-Id"

.field private static final X_FELICA_CERT_ENV_HEADER:Ljava/lang/String; = "X-Enable-Felica-Certification-Environment"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/server/felica/FelicaService$Companion;

    invoke-direct {v0}, Lcom/squareup/server/felica/FelicaService$Companion;-><init>()V

    sput-object v0, Lcom/squareup/server/felica/FelicaService$Companion;->$$INSTANCE:Lcom/squareup/server/felica/FelicaService$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
