.class public final Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;
.super Ljava/lang/Object;
.source "RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lretrofit/RestAdapter;",
        ">;"
    }
.end annotation


# instance fields
.field private final clientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit/client/Client;",
            ">;"
        }
    .end annotation
.end field

.field private final executorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final serverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lretrofit/client/Client;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;->serverProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;->clientProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;->executorProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;->gsonProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lretrofit/client/Client;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)",
            "Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;"
        }
    .end annotation

    .line 53
    new-instance v6, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static provideUnauthenticatedGsonRestAdapter(Lcom/squareup/http/Server;Lretrofit/client/Client;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/google/gson/Gson;)Lretrofit/RestAdapter;
    .locals 0

    .line 58
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/server/RestAdapterCommonModule;->provideUnauthenticatedGsonRestAdapter(Lcom/squareup/http/Server;Lretrofit/client/Client;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/google/gson/Gson;)Lretrofit/RestAdapter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lretrofit/RestAdapter;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;->get()Lretrofit/RestAdapter;

    move-result-object v0

    return-object v0
.end method

.method public get()Lretrofit/RestAdapter;
    .locals 5

    .line 46
    iget-object v0, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;->serverProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/http/Server;

    iget-object v1, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;->clientProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lretrofit/client/Client;

    iget-object v2, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;->executorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/thread/executor/MainThread;

    iget-object v4, p0, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/gson/Gson;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/server/RestAdapterCommonModule_ProvideUnauthenticatedGsonRestAdapterFactory;->provideUnauthenticatedGsonRestAdapter(Lcom/squareup/http/Server;Lretrofit/client/Client;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/google/gson/Gson;)Lretrofit/RestAdapter;

    move-result-object v0

    return-object v0
.end method
