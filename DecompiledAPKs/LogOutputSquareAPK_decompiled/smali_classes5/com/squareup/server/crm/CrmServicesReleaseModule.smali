.class public abstract Lcom/squareup/server/crm/CrmServicesReleaseModule;
.super Ljava/lang/Object;
.source "CrmServicesReleaseModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/server/crm/CrmServicesMainModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004H\'J\u0012\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0007H\'\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/server/crm/CrmServicesReleaseModule;",
        "",
        "()V",
        "bindDialogueService",
        "Lcom/squareup/server/crm/DialogueService;",
        "realService",
        "bindRolodexService",
        "Lcom/squareup/server/crm/RolodexService;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindDialogueService(Lcom/squareup/server/crm/DialogueService;)Lcom/squareup/server/crm/DialogueService;
    .param p1    # Lcom/squareup/server/crm/DialogueService;
        .annotation runtime Lcom/squareup/api/RealService;
        .end annotation
    .end param
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindRolodexService(Lcom/squareup/server/crm/RolodexService;)Lcom/squareup/server/crm/RolodexService;
    .param p1    # Lcom/squareup/server/crm/RolodexService;
        .annotation runtime Lcom/squareup/api/RealService;
        .end annotation
    .end param
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
