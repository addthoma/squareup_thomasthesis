.class public interface abstract Lcom/squareup/server/crm/DialogueService;
.super Ljava/lang/Object;
.source "DialogueService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\tH\'J\u0018\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u000cH\'J\u0018\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e2\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0010H\'J\u0018\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0013H\'\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/server/crm/DialogueService;",
        "",
        "createComment",
        "Lcom/squareup/server/StatusResponse;",
        "Lcom/squareup/protos/client/dialogue/CreateCommentResponse;",
        "request",
        "Lcom/squareup/protos/client/dialogue/CreateCommentRequest;",
        "createConversation",
        "Lcom/squareup/protos/client/dialogue/CreateConversationResponse;",
        "Lcom/squareup/protos/client/dialogue/CreateConversationRequest;",
        "createCoupon",
        "Lcom/squareup/protos/client/dialogue/CreateCouponResponse;",
        "Lcom/squareup/protos/client/dialogue/CreateCouponRequest;",
        "getConversation",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/client/dialogue/GetConversationResponse;",
        "Lcom/squareup/protos/client/dialogue/GetConversationRequest;",
        "listConversations",
        "Lcom/squareup/protos/client/dialogue/ListConversationsResponse;",
        "Lcom/squareup/protos/client/dialogue/ListConversationsRequest;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract createComment(Lcom/squareup/protos/client/dialogue/CreateCommentRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/dialogue/CreateCommentRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/dialogue/CreateCommentRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/dialogue/CreateCommentResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/dialogue/create-comment"
    .end annotation
.end method

.method public abstract createConversation(Lcom/squareup/protos/client/dialogue/CreateConversationRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/dialogue/CreateConversationRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/dialogue/CreateConversationRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/dialogue/CreateConversationResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/dialogue/create-conversation"
    .end annotation
.end method

.method public abstract createCoupon(Lcom/squareup/protos/client/dialogue/CreateCouponRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/dialogue/CreateCouponRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/dialogue/CreateCouponRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/dialogue/CreateCouponResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/dialogue/create-coupon"
    .end annotation
.end method

.method public abstract getConversation(Lcom/squareup/protos/client/dialogue/GetConversationRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/dialogue/GetConversationRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/dialogue/GetConversationRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/dialogue/GetConversationResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/dialogue/get-conversation"
    .end annotation
.end method

.method public abstract listConversations(Lcom/squareup/protos/client/dialogue/ListConversationsRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/dialogue/ListConversationsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/dialogue/ListConversationsRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/dialogue/ListConversationsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/dialogue/list-conversations"
    .end annotation
.end method
