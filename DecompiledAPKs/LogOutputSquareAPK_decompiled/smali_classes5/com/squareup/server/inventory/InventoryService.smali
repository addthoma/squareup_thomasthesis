.class public interface abstract Lcom/squareup/server/inventory/InventoryService;
.super Ljava/lang/Object;
.source "InventoryService.java"


# virtual methods
.method public abstract adjust(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/AdjustVariationInventoryRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/AdjustVariationInventoryRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/AdjustVariationInventoryResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/api/v3/itemsfe/adjust-variation-inventory"
    .end annotation
.end method

.method public abstract batchAdjust(Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/api/v3/itemsfe/batch-adjust-variation-inventory"
    .end annotation
.end method

.method public abstract get(Lcom/squareup/protos/client/GetVariationInventoryRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/GetVariationInventoryRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/GetVariationInventoryRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/GetVariationInventoryResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/api/v3/itemsfe/get-variation-inventory"
    .end annotation
.end method
