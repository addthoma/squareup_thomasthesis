.class public final Lcom/squareup/server/account/PreferenceUtils;
.super Ljava/lang/Object;
.source "PreferenceUtils.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/server/account/PreferenceUtils;",
        "",
        "()V",
        "EMPTY_PREFERENCES",
        "Lcom/squareup/server/account/protos/Preferences;",
        "services-data_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final EMPTY_PREFERENCES:Lcom/squareup/server/account/protos/Preferences;

.field public static final INSTANCE:Lcom/squareup/server/account/PreferenceUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 12
    new-instance v0, Lcom/squareup/server/account/PreferenceUtils;

    invoke-direct {v0}, Lcom/squareup/server/account/PreferenceUtils;-><init>()V

    sput-object v0, Lcom/squareup/server/account/PreferenceUtils;->INSTANCE:Lcom/squareup/server/account/PreferenceUtils;

    .line 14
    new-instance v0, Lcom/squareup/server/account/protos/Preferences$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Preferences$Builder;-><init>()V

    .line 15
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/Preferences$Builder;->build()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    .line 16
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/Preferences;->populateDefaults()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    const-string v1, "Preferences.Builder()\n  \u2026      .populateDefaults()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/server/account/PreferenceUtils;->EMPTY_PREFERENCES:Lcom/squareup/server/account/protos/Preferences;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
