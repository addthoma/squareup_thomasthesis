.class public Lcom/squareup/server/account/MerchantProfileResponse;
.super Lcom/squareup/server/SimpleResponse;
.source "MerchantProfileResponse.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/MerchantProfileResponse$Entity;,
        Lcom/squareup/server/account/MerchantProfileResponse$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008\u0016\u0018\u0000 \u00072\u00020\u0001:\u0002\u0007\u0008B\u001b\u0008\u0007\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/server/account/MerchantProfileResponse;",
        "Lcom/squareup/server/SimpleResponse;",
        "entity",
        "Lcom/squareup/server/account/MerchantProfileResponse$Entity;",
        "success",
        "",
        "(Lcom/squareup/server/account/MerchantProfileResponse$Entity;Z)V",
        "Companion",
        "Entity",
        "services-data_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/server/account/MerchantProfileResponse$Companion;


# instance fields
.field public final entity:Lcom/squareup/server/account/MerchantProfileResponse$Entity;

.field public final transient success:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/server/account/MerchantProfileResponse$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/server/account/MerchantProfileResponse$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/server/account/MerchantProfileResponse;->Companion:Lcom/squareup/server/account/MerchantProfileResponse$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {p0, v0, v1, v2, v0}, Lcom/squareup/server/account/MerchantProfileResponse;-><init>(Lcom/squareup/server/account/MerchantProfileResponse$Entity;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/MerchantProfileResponse$Entity;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/squareup/server/account/MerchantProfileResponse;-><init>(Lcom/squareup/server/account/MerchantProfileResponse$Entity;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/MerchantProfileResponse$Entity;Z)V
    .locals 1

    const-string v0, "entity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0, p2}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    iput-object p1, p0, Lcom/squareup/server/account/MerchantProfileResponse;->entity:Lcom/squareup/server/account/MerchantProfileResponse$Entity;

    iput-boolean p2, p0, Lcom/squareup/server/account/MerchantProfileResponse;->success:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/server/account/MerchantProfileResponse$Entity;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 27

    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_0

    .line 11
    new-instance v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;

    move-object v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const v25, 0x7fffff

    const/16 v26, 0x0

    invoke-direct/range {v1 .. v26}, Lcom/squareup/server/account/MerchantProfileResponse$Entity;-><init>(ZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;[Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    :cond_0
    move-object/from16 v0, p1

    :goto_0
    and-int/lit8 v1, p3, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    const/4 v2, 0x1

    move-object/from16 v1, p0

    goto :goto_1

    :cond_1
    move-object/from16 v1, p0

    move/from16 v2, p2

    .line 12
    :goto_1
    invoke-direct {v1, v0, v2}, Lcom/squareup/server/account/MerchantProfileResponse;-><init>(Lcom/squareup/server/account/MerchantProfileResponse$Entity;Z)V

    return-void
.end method

.method public static final failure()Lcom/squareup/server/account/MerchantProfileResponse;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/server/account/MerchantProfileResponse;->Companion:Lcom/squareup/server/account/MerchantProfileResponse$Companion;

    invoke-virtual {v0}, Lcom/squareup/server/account/MerchantProfileResponse$Companion;->failure()Lcom/squareup/server/account/MerchantProfileResponse;

    move-result-object v0

    return-object v0
.end method
