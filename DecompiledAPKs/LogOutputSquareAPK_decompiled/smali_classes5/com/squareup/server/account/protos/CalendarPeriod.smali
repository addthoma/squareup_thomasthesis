.class public final Lcom/squareup/server/account/protos/CalendarPeriod;
.super Lcom/squareup/wire/AndroidMessage;
.source "CalendarPeriod.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/CalendarPeriod$ProtoAdapter_CalendarPeriod;,
        Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;,
        Lcom/squareup/server/account/protos/CalendarPeriod$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/CalendarPeriod;",
        "Lcom/squareup/server/account/protos/CalendarPeriod$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/CalendarPeriod;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/CalendarPeriod;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/CalendarPeriod;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/CalendarPeriod;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/server/account/protos/CalendarPeriod$ProtoAdapter_CalendarPeriod;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/CalendarPeriod$ProtoAdapter_CalendarPeriod;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/CalendarPeriod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/server/account/protos/CalendarPeriod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/CalendarPeriod;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 37
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, v0}, Lcom/squareup/server/account/protos/CalendarPeriod;-><init>(Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;)V
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/server/account/protos/CalendarPeriod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p1}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/CalendarPeriod$Builder;)Lcom/squareup/server/account/protos/CalendarPeriod$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 83
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/CalendarPeriod;->newBuilder()Lcom/squareup/server/account/protos/CalendarPeriod$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 54
    :cond_0
    instance-of v0, p1, Lcom/squareup/server/account/protos/CalendarPeriod;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 55
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/CalendarPeriod;

    .line 56
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/CalendarPeriod;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/CalendarPeriod;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 61
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/CalendarPeriod;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/CalendarPeriod$Builder;
    .locals 2

    .line 46
    new-instance v0, Lcom/squareup/server/account/protos/CalendarPeriod$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/CalendarPeriod$Builder;-><init>()V

    .line 47
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/CalendarPeriod;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/CalendarPeriod$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/CalendarPeriod;->newBuilder()Lcom/squareup/server/account/protos/CalendarPeriod$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/CalendarPeriod;)Lcom/squareup/server/account/protos/CalendarPeriod;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/server/account/protos/CalendarPeriod;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/CalendarPeriod;->overlay(Lcom/squareup/server/account/protos/CalendarPeriod;)Lcom/squareup/server/account/protos/CalendarPeriod;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/CalendarPeriod;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/CalendarPeriod;->populateDefaults()Lcom/squareup/server/account/protos/CalendarPeriod;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CalendarPeriod{"

    .line 67
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
