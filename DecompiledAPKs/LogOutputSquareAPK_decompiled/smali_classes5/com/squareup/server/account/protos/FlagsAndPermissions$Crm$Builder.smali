.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allows_coupons_in_direct_message:Ljava/lang/Boolean;

.field public buyer_email_collection:Ljava/lang/Boolean;

.field public can_configure_profiles_android:Ljava/lang/Boolean;

.field public can_delete_groups:Ljava/lang/Boolean;

.field public disable_buyer_profile:Ljava/lang/Boolean;

.field public manage_loyalty_in_directory:Ljava/lang/Boolean;

.field public merge_in_register:Ljava/lang/Boolean;

.field public use_adhoc_filters:Ljava/lang/Boolean;

.field public use_custom_attributes_extra_default_fields:Ljava/lang/Boolean;

.field public use_filter_location:Ljava/lang/Boolean;

.field public use_filter_manual_group:Ljava/lang/Boolean;

.field public use_filter_visit_frequency:Ljava/lang/Boolean;

.field public use_new_groups_and_filtering:Ljava/lang/Boolean;

.field public use_reminders:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public allows_coupons_in_direct_message(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    .locals 0

    .line 5714
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->allows_coupons_in_direct_message:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;
    .locals 18

    move-object/from16 v0, p0

    .line 5753
    new-instance v17, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    iget-object v2, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->buyer_email_collection:Ljava/lang/Boolean;

    iget-object v3, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->merge_in_register:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_new_groups_and_filtering:Ljava/lang/Boolean;

    iget-object v5, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_adhoc_filters:Ljava/lang/Boolean;

    iget-object v6, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->can_delete_groups:Ljava/lang/Boolean;

    iget-object v7, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_filter_manual_group:Ljava/lang/Boolean;

    iget-object v8, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_filter_location:Ljava/lang/Boolean;

    iget-object v9, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_filter_visit_frequency:Ljava/lang/Boolean;

    iget-object v10, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_reminders:Ljava/lang/Boolean;

    iget-object v11, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->allows_coupons_in_direct_message:Ljava/lang/Boolean;

    iget-object v12, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->manage_loyalty_in_directory:Ljava/lang/Boolean;

    iget-object v13, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_custom_attributes_extra_default_fields:Ljava/lang/Boolean;

    iget-object v14, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->disable_buyer_profile:Ljava/lang/Boolean;

    iget-object v15, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->can_configure_profiles_android:Ljava/lang/Boolean;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object/from16 v1, v17

    invoke-direct/range {v1 .. v16}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v17
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 5606
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object v0

    return-object v0
.end method

.method public buyer_email_collection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    .locals 0

    .line 5642
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->buyer_email_collection:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_configure_profiles_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    .locals 0

    .line 5747
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->can_configure_profiles_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_delete_groups(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    .locals 0

    .line 5674
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->can_delete_groups:Ljava/lang/Boolean;

    return-object p0
.end method

.method public disable_buyer_profile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    .locals 0

    .line 5739
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->disable_buyer_profile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public manage_loyalty_in_directory(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    .locals 0

    .line 5722
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->manage_loyalty_in_directory:Ljava/lang/Boolean;

    return-object p0
.end method

.method public merge_in_register(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    .locals 0

    .line 5650
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->merge_in_register:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_adhoc_filters(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    .locals 0

    .line 5666
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_adhoc_filters:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_custom_attributes_extra_default_fields(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    .locals 0

    .line 5731
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_custom_attributes_extra_default_fields:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_filter_location(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    .locals 0

    .line 5690
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_filter_location:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_filter_manual_group(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    .locals 0

    .line 5682
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_filter_manual_group:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_filter_visit_frequency(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    .locals 0

    .line 5698
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_filter_visit_frequency:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_new_groups_and_filtering(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    .locals 0

    .line 5658
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_new_groups_and_filtering:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_reminders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    .locals 0

    .line 5706
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_reminders:Ljava/lang/Boolean;

    return-object p0
.end method
