.class final Lcom/squareup/server/account/protos/FlagsAndPermissions$ProtoAdapter_FlagsAndPermissions;
.super Lcom/squareup/wire/ProtoAdapter;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FlagsAndPermissions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 19732
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/FlagsAndPermissions;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 20057
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;-><init>()V

    .line 20058
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 20059
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 20224
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 20222
    :pswitch_1
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->auth(Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto :goto_0

    .line 20221
    :pswitch_2
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->posfeatures(Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto :goto_0

    .line 20220
    :pswitch_3
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->marketing(Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto :goto_0

    .line 20219
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->display_learn_more_r4(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto :goto_0

    .line 20218
    :pswitch_5
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->capital(Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto :goto_0

    .line 20217
    :pswitch_6
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->printers(Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto :goto_0

    .line 20216
    :pswitch_7
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->ecom(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto :goto_0

    .line 20215
    :pswitch_8
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->payments(Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto :goto_0

    .line 20214
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->has_bazaar_online_store(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto :goto_0

    .line 20213
    :pswitch_a
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->orderhub(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20212
    :pswitch_b
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->prices(Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20211
    :pswitch_c
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->retailer(Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20210
    :pswitch_d
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->spoc(Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20209
    :pswitch_e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->order_hub_sync_period_without_notifications(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20208
    :pswitch_f
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->order_hub_sync_period_with_notifications(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20207
    :pswitch_10
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->discount(Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20206
    :pswitch_11
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->appointments(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20205
    :pswitch_12
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->restaurants(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20204
    :pswitch_13
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->uses_device_profile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20203
    :pswitch_14
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employeejobs(Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20202
    :pswitch_15
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_use_android_ecr(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20201
    :pswitch_16
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->squaredevice(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20200
    :pswitch_17
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->items(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20199
    :pswitch_18
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->billing(Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20198
    :pswitch_19
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20197
    :pswitch_1a
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->readerfw(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20196
    :pswitch_1b
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->fees(Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20195
    :pswitch_1c
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bizbank(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20194
    :pswitch_1d
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bfd(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20193
    :pswitch_1e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_inventory_plus(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20192
    :pswitch_1f
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->using_time_tracking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20191
    :pswitch_20
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->devplatmobile(Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20190
    :pswitch_21
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->pos(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20189
    :pswitch_22
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->timecards(Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20188
    :pswitch_23
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->login_type(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20187
    :pswitch_24
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->terminal(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20186
    :pswitch_25
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_emv(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20185
    :pswitch_26
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employee(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20184
    :pswitch_27
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->paymentflow(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20183
    :pswitch_28
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->giftcard(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20182
    :pswitch_29
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->collect_tip_before_authorization_required(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20181
    :pswitch_2a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->collect_tip_before_authorization_preferred(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20180
    :pswitch_2b
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->deposits(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20179
    :pswitch_2c
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->invoices(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20178
    :pswitch_2d
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->regex(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20177
    :pswitch_2e
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->reports(Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20176
    :pswitch_2f
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->crm(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20175
    :pswitch_30
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->onboard(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20174
    :pswitch_31
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_pre_tax_tipping_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20173
    :pswitch_32
    sget-object v3, Lcom/squareup/server/account/protos/Cardreaders;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/Cardreaders;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->cardreaders(Lcom/squareup/server/account/protos/Cardreaders;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20172
    :pswitch_33
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->loyalty(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20171
    :pswitch_34
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_custom_attributes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20170
    :pswitch_35
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->catalog(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20169
    :pswitch_36
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->merge_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20168
    :pswitch_37
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->allow_payment_for_invoice_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20167
    :pswitch_38
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_no_contactless_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20166
    :pswitch_39
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_contactless_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20165
    :pswitch_3a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->protect_edit_tax(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20164
    :pswitch_3b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_on_file_in_android_register_on_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20163
    :pswitch_3c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_invoice_applet_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20162
    :pswitch_3d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_in_app_timecards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20161
    :pswitch_3e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_a10(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20160
    :pswitch_3f
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employee_cancel_transaction(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20159
    :pswitch_40
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_notes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20158
    :pswitch_41
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_messaging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20157
    :pswitch_42
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_directory_with_invoices(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20156
    :pswitch_43
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->accept_third_party_gift_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20155
    :pswitch_44
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->transfer_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20154
    :pswitch_45
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->require_secure_session_for_r6_swipe(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20153
    :pswitch_46
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_allow_swipe_for_chip_cards_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20152
    :pswitch_47
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->zero_amount_contactless_arqc_experiment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20151
    :pswitch_48
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_bulk_delete_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20150
    :pswitch_49
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->activation_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20149
    :pswitch_4a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support_retail_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20148
    :pswitch_4b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support_restaurant_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20147
    :pswitch_4c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->placeholder(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20146
    :pswitch_4d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->cardholder_name_on_dip_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20145
    :pswitch_4e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_filtering_for_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20144
    :pswitch_4f
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_on_file_in_android_register(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20143
    :pswitch_50
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_zero_amount_tender(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20142
    :pswitch_51
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_void_comp_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20141
    :pswitch_52
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_v3_catalog(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20140
    :pswitch_53
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_split_tender(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20139
    :pswitch_54
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_split_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20138
    :pswitch_55
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_safetynet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20137
    :pswitch_56
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r6(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20136
    :pswitch_57
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r12_pairing_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20135
    :pswitch_58
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r12(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20134
    :pswitch_59
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_reports_charts_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20133
    :pswitch_5a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_predefined_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20132
    :pswitch_5b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_paper_signature_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20131
    :pswitch_5c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_open_tickets_mobile_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20130
    :pswitch_5d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20129
    :pswitch_5e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices_with_modifiers(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20128
    :pswitch_5f
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices_with_discounts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20127
    :pswitch_60
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20126
    :pswitch_61
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_magstripe_only_readers(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20125
    :pswitch_62
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_gift_cards_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20124
    :pswitch_63
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_enforce_location_fix(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20123
    :pswitch_64
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_timecards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20122
    :pswitch_65
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_filtering_for_paper_signature_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20121
    :pswitch_66
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_dining_options(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20120
    :pswitch_67
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_in_android_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20119
    :pswitch_68
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20118
    :pswitch_69
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_applet_in_android_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20117
    :pswitch_6a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_applet_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20116
    :pswitch_6b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_activity_list_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20115
    :pswitch_6c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_conditional_taxes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20114
    :pswitch_6d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_associated_coupons(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20113
    :pswitch_6e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_australian_gross_sales(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20112
    :pswitch_6f
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_employee_management(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20111
    :pswitch_70
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_barcode_scanning(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20110
    :pswitch_71
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_printer_stations(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20109
    :pswitch_72
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->upload_support_ledger(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20108
    :pswitch_73
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->swipe_to_create_open_ticket_with_name(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20107
    :pswitch_74
    iget-object v3, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->supported_card_brands_online:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 20106
    :pswitch_75
    iget-object v3, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->supported_card_brands_offline:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 20105
    :pswitch_76
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->sms(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20104
    :pswitch_77
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_items_library_after_login(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20103
    :pswitch_78
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_o1_jp_deprecation_warning_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20102
    :pswitch_79
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_inclusive_taxes_in_cart(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20101
    :pswitch_7a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_feature_tour(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20100
    :pswitch_7b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->skip_signatures_for_small_payments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20099
    :pswitch_7c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->skip_modifier_detail_screen_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20098
    :pswitch_7d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->seller_agreement_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20097
    :pswitch_7e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_payroll_register_learn_more_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20096
    :pswitch_7f
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_payroll_register_feature_tour(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20095
    :pswitch_80
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_advanced_modifiers(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20089
    :pswitch_81
    :try_start_0
    sget-object v4, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    invoke-virtual {v0, v4}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->rounding_type(Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v4

    .line 20091
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 20086
    :pswitch_82
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->requires_track_2_if_not_amex(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20085
    :pswitch_83
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->reduce_printing_waste_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20084
    :pswitch_84
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->privacy_policy_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20083
    :pswitch_85
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->payment_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20082
    :pswitch_86
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->opt_in_to_store_and_forward_payments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20081
    :pswitch_87
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->opt_in_to_open_tickets_with_beta_ui_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20080
    :pswitch_88
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->items_batch_size(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20079
    :pswitch_89
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->invoice_tipping(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20078
    :pswitch_8a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->hide_modifiers_on_receipts_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20077
    :pswitch_8b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->help_center_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20076
    :pswitch_8c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_register_api(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20075
    :pswitch_8d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->email_transaction_ledger(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20074
    :pswitch_8e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->eligible_for_third_party_card_processing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20073
    :pswitch_8f
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->eligible_for_square_card_processing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20072
    :pswitch_90
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_mobile_reports_applet_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20071
    :pswitch_91
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_item_editing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20070
    :pswitch_92
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->contact_support_twitter(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20069
    :pswitch_93
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->contact_support_phone_number(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20068
    :pswitch_94
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_transfer_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20067
    :pswitch_95
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_skip_receipt_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20066
    :pswitch_96
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_refer_friend_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20065
    :pswitch_97
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_publish_merchant_card(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20064
    :pswitch_98
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->print_logo_on_paper_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20063
    :pswitch_99
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_onboard(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20062
    :pswitch_9a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_enable_tipping(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20061
    :pswitch_9b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->auto_capture_payments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    goto/16 :goto_0

    .line 20228
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 20229
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9b
        :pswitch_0
        :pswitch_9a
        :pswitch_0
        :pswitch_99
        :pswitch_98
        :pswitch_97
        :pswitch_96
        :pswitch_95
        :pswitch_94
        :pswitch_0
        :pswitch_0
        :pswitch_93
        :pswitch_92
        :pswitch_91
        :pswitch_90
        :pswitch_8f
        :pswitch_8e
        :pswitch_8d
        :pswitch_8c
        :pswitch_8b
        :pswitch_8a
        :pswitch_89
        :pswitch_88
        :pswitch_0
        :pswitch_87
        :pswitch_86
        :pswitch_85
        :pswitch_84
        :pswitch_83
        :pswitch_82
        :pswitch_81
        :pswitch_80
        :pswitch_7f
        :pswitch_7e
        :pswitch_7d
        :pswitch_7c
        :pswitch_7b
        :pswitch_7a
        :pswitch_79
        :pswitch_78
        :pswitch_77
        :pswitch_0
        :pswitch_76
        :pswitch_75
        :pswitch_74
        :pswitch_73
        :pswitch_72
        :pswitch_71
        :pswitch_70
        :pswitch_6f
        :pswitch_6e
        :pswitch_0
        :pswitch_0
        :pswitch_6d
        :pswitch_6c
        :pswitch_6b
        :pswitch_6a
        :pswitch_69
        :pswitch_68
        :pswitch_67
        :pswitch_0
        :pswitch_66
        :pswitch_0
        :pswitch_65
        :pswitch_64
        :pswitch_63
        :pswitch_62
        :pswitch_0
        :pswitch_61
        :pswitch_60
        :pswitch_5f
        :pswitch_5e
        :pswitch_0
        :pswitch_5d
        :pswitch_5c
        :pswitch_5b
        :pswitch_5a
        :pswitch_59
        :pswitch_58
        :pswitch_57
        :pswitch_56
        :pswitch_55
        :pswitch_54
        :pswitch_53
        :pswitch_0
        :pswitch_52
        :pswitch_51
        :pswitch_50
        :pswitch_4f
        :pswitch_0
        :pswitch_0
        :pswitch_4e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4d
        :pswitch_4c
        :pswitch_4b
        :pswitch_4a
        :pswitch_0
        :pswitch_49
        :pswitch_48
        :pswitch_47
        :pswitch_46
        :pswitch_45
        :pswitch_44
        :pswitch_43
        :pswitch_42
        :pswitch_41
        :pswitch_40
        :pswitch_3f
        :pswitch_0
        :pswitch_3e
        :pswitch_3d
        :pswitch_0
        :pswitch_0
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_0
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_0
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_0
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_0
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_11
        :pswitch_10
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 19730
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ProtoAdapter_FlagsAndPermissions;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/FlagsAndPermissions;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/FlagsAndPermissions;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 19897
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->accept_third_party_gift_cards:Ljava/lang/Boolean;

    const/16 v2, 0x6c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19898
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->activation_url:Ljava/lang/String;

    const/16 v2, 0x66

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19899
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->allow_payment_for_invoice_android:Ljava/lang/Boolean;

    const/16 v2, 0x7c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19900
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auto_capture_payments:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19901
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

    const/16 v2, 0x69

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19902
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

    const/16 v2, 0x67

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19903
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_enable_tipping:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19904
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_onboard:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19905
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->print_logo_on_paper_android:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19906
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_publish_merchant_card:Ljava/lang/Boolean;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19907
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_refer_friend_in_app:Ljava/lang/Boolean;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19908
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_skip_receipt_screen:Ljava/lang/Boolean;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19909
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_transfer_open_tickets_android:Ljava/lang/Boolean;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19910
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_use_android_ecr:Ljava/lang/Boolean;

    const/16 v2, 0xa1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19911
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardholder_name_on_dip_android:Ljava/lang/Boolean;

    const/16 v2, 0x61

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19912
    sget-object v0, Lcom/squareup/server/account/protos/Cardreaders;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    const/16 v2, 0x82

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19913
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

    const/16 v2, 0x8a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19914
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_required:Ljava/lang/Boolean;

    const/16 v2, 0x8b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19915
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_phone_number:Ljava/lang/String;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19916
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_twitter:Ljava/lang/String;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19917
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_contactless_cards:Ljava/lang/Boolean;

    const/16 v2, 0x7a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19918
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_no_contactless_cards:Ljava/lang/Boolean;

    const/16 v2, 0x7b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19919
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_emv:Ljava/lang/Boolean;

    const/16 v2, 0x8f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19920
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_item_editing:Ljava/lang/Boolean;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19921
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_mobile_reports_applet_android:Ljava/lang/Boolean;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19922
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19923
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_third_party_card_processing:Ljava/lang/Boolean;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19924
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->email_transaction_ledger:Ljava/lang/Boolean;

    const/16 v2, 0x13

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19925
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_register_api:Ljava/lang/Boolean;

    const/16 v2, 0x14

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19926
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->help_center_url:Ljava/lang/String;

    const/16 v2, 0x15

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19927
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

    const/16 v2, 0x16

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19928
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoice_tipping:Ljava/lang/Boolean;

    const/16 v2, 0x17

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19929
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items_batch_size:Ljava/lang/Long;

    const/16 v2, 0x18

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19930
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->login_type:Ljava/lang/String;

    const/16 v2, 0x92

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19931
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->merge_open_tickets_android:Ljava/lang/Boolean;

    const/16 v2, 0x7d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19932
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

    const/16 v2, 0x1a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19933
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

    const/16 v2, 0x1b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19934
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payment_cards:Ljava/lang/Boolean;

    const/16 v2, 0x1c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19935
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->protect_edit_tax:Ljava/lang/Boolean;

    const/16 v2, 0x78

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19936
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->privacy_policy_url:Ljava/lang/String;

    const/16 v2, 0x1d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19937
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reduce_printing_waste_android:Ljava/lang/Boolean;

    const/16 v2, 0x1e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19938
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

    const/16 v2, 0x6a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19939
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requires_track_2_if_not_amex:Ljava/lang/Boolean;

    const/16 v2, 0x1f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19940
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    const/16 v2, 0x20

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19941
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_advanced_modifiers:Ljava/lang/Boolean;

    const/16 v2, 0x21

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19942
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_feature_tour:Ljava/lang/Boolean;

    const/16 v2, 0x22

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19943
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_learn_more_android:Ljava/lang/Boolean;

    const/16 v2, 0x23

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19944
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->seller_agreement_url:Ljava/lang/String;

    const/16 v2, 0x24

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19945
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

    const/16 v2, 0x25

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19946
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_signatures_for_small_payments:Ljava/lang/Boolean;

    const/16 v2, 0x26

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19947
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_feature_tour:Ljava/lang/Boolean;

    const/16 v2, 0x27

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19948
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

    const/16 v2, 0x28

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19949
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

    const/16 v2, 0x29

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19950
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_items_library_after_login:Ljava/lang/Boolean;

    const/16 v2, 0x2a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19951
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->sms:Ljava/lang/Boolean;

    const/16 v2, 0x2c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19952
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->placeholder:Ljava/lang/Boolean;

    const/16 v2, 0x62

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19953
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_restaurant_items:Ljava/lang/Boolean;

    const/16 v2, 0x63

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19954
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_retail_items:Ljava/lang/Boolean;

    const/16 v2, 0x64

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19955
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_offline:Ljava/util/List;

    const/16 v2, 0x2d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19956
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_online:Ljava/util/List;

    const/16 v2, 0x2e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19957
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    const/16 v2, 0x2f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19958
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->transfer_open_tickets_android:Ljava/lang/Boolean;

    const/16 v2, 0x6b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19959
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->upload_support_ledger:Ljava/lang/Boolean;

    const/16 v2, 0x30

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19960
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_a10:Ljava/lang/Boolean;

    const/16 v2, 0x72

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19961
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_printer_stations:Ljava/lang/Boolean;

    const/16 v2, 0x31

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19962
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_barcode_scanning:Ljava/lang/Boolean;

    const/16 v2, 0x32

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19963
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_employee_management:Ljava/lang/Boolean;

    const/16 v2, 0x33

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19964
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_australian_gross_sales:Ljava/lang/Boolean;

    const/16 v2, 0x34

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19965
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_associated_coupons:Ljava/lang/Boolean;

    const/16 v2, 0x37

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19966
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register:Ljava/lang/Boolean;

    const/16 v2, 0x5a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19967
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

    const/16 v2, 0x77

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19968
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_conditional_taxes:Ljava/lang/Boolean;

    const/16 v2, 0x38

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19969
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_custom_attributes:Ljava/lang/Boolean;

    const/16 v2, 0x80

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19970
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_messaging:Ljava/lang/Boolean;

    const/16 v2, 0x6e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19971
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_notes:Ljava/lang/Boolean;

    const/16 v2, 0x6f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19972
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_directory_with_invoices:Ljava/lang/Boolean;

    const/16 v2, 0x6d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19973
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

    const/16 v2, 0x39

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19974
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android:Ljava/lang/Boolean;

    const/16 v2, 0x3a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19975
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

    const/16 v2, 0x3b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19976
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android:Ljava/lang/Boolean;

    const/16 v2, 0x3c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19977
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android_mobile:Ljava/lang/Boolean;

    const/16 v2, 0x3d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19978
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_dining_options:Ljava/lang/Boolean;

    const/16 v2, 0x3f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19979
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee_cancel_transaction:Ljava/lang/Boolean;

    const/16 v2, 0x70

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19980
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

    const/16 v2, 0x5d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19981
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

    const/16 v2, 0x41

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19982
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_timecards:Ljava/lang/Boolean;

    const/16 v2, 0x42

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19983
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_enforce_location_fix:Ljava/lang/Boolean;

    const/16 v2, 0x43

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19984
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_gift_cards_v2:Ljava/lang/Boolean;

    const/16 v2, 0x44

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19985
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_in_app_timecards:Ljava/lang/Boolean;

    const/16 v2, 0x73

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19986
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_inventory_plus:Ljava/lang/Boolean;

    const/16 v2, 0x97

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19987
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_invoice_applet_in_android:Ljava/lang/Boolean;

    const/16 v2, 0x76

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19988
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_magstripe_only_readers:Ljava/lang/Boolean;

    const/16 v2, 0x46

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19989
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices:Ljava/lang/Boolean;

    const/16 v2, 0x47

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19990
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

    const/16 v2, 0x48

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19991
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

    const/16 v2, 0x49

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19992
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_android:Ljava/lang/Boolean;

    const/16 v2, 0x4b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19993
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_mobile_android:Ljava/lang/Boolean;

    const/16 v2, 0x4c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19994
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_paper_signature_android:Ljava/lang/Boolean;

    const/16 v2, 0x4d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19995
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    const/16 v2, 0x83

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19996
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_predefined_tickets_android:Ljava/lang/Boolean;

    const/16 v2, 0x4e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19997
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_reports_charts_android:Ljava/lang/Boolean;

    const/16 v2, 0x4f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19998
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12:Ljava/lang/Boolean;

    const/16 v2, 0x50

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 19999
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12_pairing_v2:Ljava/lang/Boolean;

    const/16 v2, 0x51

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20000
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r6:Ljava/lang/Boolean;

    const/16 v2, 0x52

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20001
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_safetynet:Ljava/lang/Boolean;

    const/16 v2, 0x53

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20002
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tickets_android:Ljava/lang/Boolean;

    const/16 v2, 0x54

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20003
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tender:Ljava/lang/Boolean;

    const/16 v2, 0x55

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20004
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_v3_catalog:Ljava/lang/Boolean;

    const/16 v2, 0x57

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20005
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_void_comp_android:Ljava/lang/Boolean;

    const/16 v2, 0x58

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20006
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_zero_amount_tender:Ljava/lang/Boolean;

    const/16 v2, 0x59

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20007
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->uses_device_profile:Ljava/lang/Boolean;

    const/16 v2, 0xa3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20008
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->using_time_tracking:Ljava/lang/Boolean;

    const/16 v2, 0x96

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20009
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;

    const/16 v2, 0x68

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20010
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_with_notifications:Ljava/lang/Long;

    const/16 v2, 0xae

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20011
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_without_notifications:Ljava/lang/Long;

    const/16 v2, 0xaf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20012
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->has_bazaar_online_store:Ljava/lang/Boolean;

    const/16 v2, 0xba

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20013
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->display_learn_more_r4:Ljava/lang/Boolean;

    const/16 v2, 0xbf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20014
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    const/16 v2, 0x7f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20015
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    const/16 v2, 0x81

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20016
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    const/16 v2, 0x84

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20017
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    const/16 v2, 0x85

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20018
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    const/16 v2, 0x87

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20019
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    const/16 v2, 0x86

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20020
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    const/16 v2, 0x88

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20021
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    const/16 v2, 0x89

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20022
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    const/16 v2, 0x8c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20023
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    const/16 v2, 0x8d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20024
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    const/16 v2, 0x94

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20025
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    const/16 v2, 0xc1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20026
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    const/16 v2, 0x8e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20027
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    const/16 v2, 0x91

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20028
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    const/16 v2, 0x93

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20029
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    const/16 v2, 0x95

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20030
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    const/16 v2, 0x98

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20031
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    const/16 v2, 0x99

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20032
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    const/16 v2, 0x9a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20033
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    const/16 v2, 0x9c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20034
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    const/16 v2, 0x9d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20035
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    const/16 v2, 0x9e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20036
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    const/16 v2, 0x9f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20037
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    const/16 v2, 0xa0

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20038
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    const/16 v2, 0xa2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20039
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    const/16 v2, 0xa4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20040
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    const/16 v2, 0xa8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20041
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    const/16 v2, 0xa9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20042
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    const/16 v2, 0xb0

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20043
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    const/16 v2, 0xb1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20044
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    const/16 v2, 0xb2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20045
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    const/16 v2, 0xb5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20046
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    const/16 v2, 0xbb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20047
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    const/16 v2, 0xbc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20048
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    const/16 v2, 0xbd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20049
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    const/16 v2, 0xbe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20050
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    const/16 v2, 0xc0

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20051
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    const/16 v2, 0xc2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 20052
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 19730
    check-cast p2, Lcom/squareup/server/account/protos/FlagsAndPermissions;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ProtoAdapter_FlagsAndPermissions;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/FlagsAndPermissions;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/FlagsAndPermissions;)I
    .locals 4

    .line 19737
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->accept_third_party_gift_cards:Ljava/lang/Boolean;

    const/16 v2, 0x6c

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->activation_url:Ljava/lang/String;

    const/16 v3, 0x66

    .line 19738
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->allow_payment_for_invoice_android:Ljava/lang/Boolean;

    const/16 v3, 0x7c

    .line 19739
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auto_capture_payments:Ljava/lang/Boolean;

    const/4 v3, 0x1

    .line 19740
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

    const/16 v3, 0x69

    .line 19741
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

    const/16 v3, 0x67

    .line 19742
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_enable_tipping:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 19743
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_onboard:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 19744
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->print_logo_on_paper_android:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 19745
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_publish_merchant_card:Ljava/lang/Boolean;

    const/4 v3, 0x7

    .line 19746
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_refer_friend_in_app:Ljava/lang/Boolean;

    const/16 v3, 0x8

    .line 19747
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_skip_receipt_screen:Ljava/lang/Boolean;

    const/16 v3, 0x9

    .line 19748
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_transfer_open_tickets_android:Ljava/lang/Boolean;

    const/16 v3, 0xa

    .line 19749
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_use_android_ecr:Ljava/lang/Boolean;

    const/16 v3, 0xa1

    .line 19750
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardholder_name_on_dip_android:Ljava/lang/Boolean;

    const/16 v3, 0x61

    .line 19751
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/Cardreaders;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    const/16 v3, 0x82

    .line 19752
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

    const/16 v3, 0x8a

    .line 19753
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_required:Ljava/lang/Boolean;

    const/16 v3, 0x8b

    .line 19754
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_phone_number:Ljava/lang/String;

    const/16 v3, 0xd

    .line 19755
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_twitter:Ljava/lang/String;

    const/16 v3, 0xe

    .line 19756
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_contactless_cards:Ljava/lang/Boolean;

    const/16 v3, 0x7a

    .line 19757
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_no_contactless_cards:Ljava/lang/Boolean;

    const/16 v3, 0x7b

    .line 19758
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_emv:Ljava/lang/Boolean;

    const/16 v3, 0x8f

    .line 19759
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_item_editing:Ljava/lang/Boolean;

    const/16 v3, 0xf

    .line 19760
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_mobile_reports_applet_android:Ljava/lang/Boolean;

    const/16 v3, 0x10

    .line 19761
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    const/16 v3, 0x11

    .line 19762
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_third_party_card_processing:Ljava/lang/Boolean;

    const/16 v3, 0x12

    .line 19763
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->email_transaction_ledger:Ljava/lang/Boolean;

    const/16 v3, 0x13

    .line 19764
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_register_api:Ljava/lang/Boolean;

    const/16 v3, 0x14

    .line 19765
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->help_center_url:Ljava/lang/String;

    const/16 v3, 0x15

    .line 19766
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

    const/16 v3, 0x16

    .line 19767
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoice_tipping:Ljava/lang/Boolean;

    const/16 v3, 0x17

    .line 19768
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items_batch_size:Ljava/lang/Long;

    const/16 v3, 0x18

    .line 19769
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->login_type:Ljava/lang/String;

    const/16 v3, 0x92

    .line 19770
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->merge_open_tickets_android:Ljava/lang/Boolean;

    const/16 v3, 0x7d

    .line 19771
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

    const/16 v3, 0x1a

    .line 19772
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

    const/16 v3, 0x1b

    .line 19773
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payment_cards:Ljava/lang/Boolean;

    const/16 v3, 0x1c

    .line 19774
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->protect_edit_tax:Ljava/lang/Boolean;

    const/16 v3, 0x78

    .line 19775
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->privacy_policy_url:Ljava/lang/String;

    const/16 v3, 0x1d

    .line 19776
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reduce_printing_waste_android:Ljava/lang/Boolean;

    const/16 v3, 0x1e

    .line 19777
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

    const/16 v3, 0x6a

    .line 19778
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requires_track_2_if_not_amex:Ljava/lang/Boolean;

    const/16 v3, 0x1f

    .line 19779
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    const/16 v3, 0x20

    .line 19780
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_advanced_modifiers:Ljava/lang/Boolean;

    const/16 v3, 0x21

    .line 19781
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_feature_tour:Ljava/lang/Boolean;

    const/16 v3, 0x22

    .line 19782
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_learn_more_android:Ljava/lang/Boolean;

    const/16 v3, 0x23

    .line 19783
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->seller_agreement_url:Ljava/lang/String;

    const/16 v3, 0x24

    .line 19784
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

    const/16 v3, 0x25

    .line 19785
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_signatures_for_small_payments:Ljava/lang/Boolean;

    const/16 v3, 0x26

    .line 19786
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_feature_tour:Ljava/lang/Boolean;

    const/16 v3, 0x27

    .line 19787
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

    const/16 v3, 0x28

    .line 19788
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

    const/16 v3, 0x29

    .line 19789
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_items_library_after_login:Ljava/lang/Boolean;

    const/16 v3, 0x2a

    .line 19790
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->sms:Ljava/lang/Boolean;

    const/16 v3, 0x2c

    .line 19791
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->placeholder:Ljava/lang/Boolean;

    const/16 v3, 0x62

    .line 19792
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_restaurant_items:Ljava/lang/Boolean;

    const/16 v3, 0x63

    .line 19793
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_retail_items:Ljava/lang/Boolean;

    const/16 v3, 0x64

    .line 19794
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 19795
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_offline:Ljava/util/List;

    const/16 v3, 0x2d

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 19796
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_online:Ljava/util/List;

    const/16 v3, 0x2e

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    const/16 v3, 0x2f

    .line 19797
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->transfer_open_tickets_android:Ljava/lang/Boolean;

    const/16 v3, 0x6b

    .line 19798
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->upload_support_ledger:Ljava/lang/Boolean;

    const/16 v3, 0x30

    .line 19799
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_a10:Ljava/lang/Boolean;

    const/16 v3, 0x72

    .line 19800
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_printer_stations:Ljava/lang/Boolean;

    const/16 v3, 0x31

    .line 19801
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_barcode_scanning:Ljava/lang/Boolean;

    const/16 v3, 0x32

    .line 19802
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_employee_management:Ljava/lang/Boolean;

    const/16 v3, 0x33

    .line 19803
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_australian_gross_sales:Ljava/lang/Boolean;

    const/16 v3, 0x34

    .line 19804
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_associated_coupons:Ljava/lang/Boolean;

    const/16 v3, 0x37

    .line 19805
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register:Ljava/lang/Boolean;

    const/16 v3, 0x5a

    .line 19806
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

    const/16 v3, 0x77

    .line 19807
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_conditional_taxes:Ljava/lang/Boolean;

    const/16 v3, 0x38

    .line 19808
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_custom_attributes:Ljava/lang/Boolean;

    const/16 v3, 0x80

    .line 19809
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_messaging:Ljava/lang/Boolean;

    const/16 v3, 0x6e

    .line 19810
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_notes:Ljava/lang/Boolean;

    const/16 v3, 0x6f

    .line 19811
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_directory_with_invoices:Ljava/lang/Boolean;

    const/16 v3, 0x6d

    .line 19812
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

    const/16 v3, 0x39

    .line 19813
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android:Ljava/lang/Boolean;

    const/16 v3, 0x3a

    .line 19814
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

    const/16 v3, 0x3b

    .line 19815
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android:Ljava/lang/Boolean;

    const/16 v3, 0x3c

    .line 19816
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android_mobile:Ljava/lang/Boolean;

    const/16 v3, 0x3d

    .line 19817
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_dining_options:Ljava/lang/Boolean;

    const/16 v3, 0x3f

    .line 19818
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee_cancel_transaction:Ljava/lang/Boolean;

    const/16 v3, 0x70

    .line 19819
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

    const/16 v3, 0x5d

    .line 19820
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

    const/16 v3, 0x41

    .line 19821
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_timecards:Ljava/lang/Boolean;

    const/16 v3, 0x42

    .line 19822
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_enforce_location_fix:Ljava/lang/Boolean;

    const/16 v3, 0x43

    .line 19823
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_gift_cards_v2:Ljava/lang/Boolean;

    const/16 v3, 0x44

    .line 19824
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_in_app_timecards:Ljava/lang/Boolean;

    const/16 v3, 0x73

    .line 19825
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_inventory_plus:Ljava/lang/Boolean;

    const/16 v3, 0x97

    .line 19826
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_invoice_applet_in_android:Ljava/lang/Boolean;

    const/16 v3, 0x76

    .line 19827
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_magstripe_only_readers:Ljava/lang/Boolean;

    const/16 v3, 0x46

    .line 19828
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices:Ljava/lang/Boolean;

    const/16 v3, 0x47

    .line 19829
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

    const/16 v3, 0x48

    .line 19830
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

    const/16 v3, 0x49

    .line 19831
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_android:Ljava/lang/Boolean;

    const/16 v3, 0x4b

    .line 19832
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_mobile_android:Ljava/lang/Boolean;

    const/16 v3, 0x4c

    .line 19833
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_paper_signature_android:Ljava/lang/Boolean;

    const/16 v3, 0x4d

    .line 19834
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    const/16 v3, 0x83

    .line 19835
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_predefined_tickets_android:Ljava/lang/Boolean;

    const/16 v3, 0x4e

    .line 19836
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_reports_charts_android:Ljava/lang/Boolean;

    const/16 v3, 0x4f

    .line 19837
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12:Ljava/lang/Boolean;

    const/16 v3, 0x50

    .line 19838
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12_pairing_v2:Ljava/lang/Boolean;

    const/16 v3, 0x51

    .line 19839
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r6:Ljava/lang/Boolean;

    const/16 v3, 0x52

    .line 19840
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_safetynet:Ljava/lang/Boolean;

    const/16 v3, 0x53

    .line 19841
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tickets_android:Ljava/lang/Boolean;

    const/16 v3, 0x54

    .line 19842
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tender:Ljava/lang/Boolean;

    const/16 v3, 0x55

    .line 19843
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_v3_catalog:Ljava/lang/Boolean;

    const/16 v3, 0x57

    .line 19844
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_void_comp_android:Ljava/lang/Boolean;

    const/16 v3, 0x58

    .line 19845
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_zero_amount_tender:Ljava/lang/Boolean;

    const/16 v3, 0x59

    .line 19846
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->uses_device_profile:Ljava/lang/Boolean;

    const/16 v3, 0xa3

    .line 19847
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->using_time_tracking:Ljava/lang/Boolean;

    const/16 v3, 0x96

    .line 19848
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;

    const/16 v3, 0x68

    .line 19849
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_with_notifications:Ljava/lang/Long;

    const/16 v3, 0xae

    .line 19850
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_without_notifications:Ljava/lang/Long;

    const/16 v3, 0xaf

    .line 19851
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->has_bazaar_online_store:Ljava/lang/Boolean;

    const/16 v3, 0xba

    .line 19852
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->display_learn_more_r4:Ljava/lang/Boolean;

    const/16 v3, 0xbf

    .line 19853
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    const/16 v3, 0x7f

    .line 19854
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    const/16 v3, 0x81

    .line 19855
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    const/16 v3, 0x84

    .line 19856
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    const/16 v3, 0x85

    .line 19857
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    const/16 v3, 0x87

    .line 19858
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    const/16 v3, 0x86

    .line 19859
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    const/16 v3, 0x88

    .line 19860
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    const/16 v3, 0x89

    .line 19861
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    const/16 v3, 0x8c

    .line 19862
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    const/16 v3, 0x8d

    .line 19863
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    const/16 v3, 0x94

    .line 19864
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    const/16 v3, 0xc1

    .line 19865
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    const/16 v3, 0x8e

    .line 19866
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    const/16 v3, 0x91

    .line 19867
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    const/16 v3, 0x93

    .line 19868
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    const/16 v3, 0x95

    .line 19869
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    const/16 v3, 0x98

    .line 19870
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    const/16 v3, 0x99

    .line 19871
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    const/16 v3, 0x9a

    .line 19872
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    const/16 v3, 0x9c

    .line 19873
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    const/16 v3, 0x9d

    .line 19874
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    const/16 v3, 0x9e

    .line 19875
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    const/16 v3, 0x9f

    .line 19876
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    const/16 v3, 0xa0

    .line 19877
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    const/16 v3, 0xa2

    .line 19878
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    const/16 v3, 0xa4

    .line 19879
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    const/16 v3, 0xa8

    .line 19880
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    const/16 v3, 0xa9

    .line 19881
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    const/16 v3, 0xb0

    .line 19882
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    const/16 v3, 0xb1

    .line 19883
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    const/16 v3, 0xb2

    .line 19884
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    const/16 v3, 0xb5

    .line 19885
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    const/16 v3, 0xbb

    .line 19886
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    const/16 v3, 0xbc

    .line 19887
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    const/16 v3, 0xbd

    .line 19888
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    const/16 v3, 0xbe

    .line 19889
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    const/16 v3, 0xc0

    .line 19890
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    const/16 v3, 0xc2

    .line 19891
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19892
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 19730
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ProtoAdapter_FlagsAndPermissions;->encodedSize(Lcom/squareup/server/account/protos/FlagsAndPermissions;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/FlagsAndPermissions;)Lcom/squareup/server/account/protos/FlagsAndPermissions;
    .locals 2

    .line 20234
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object p1

    .line 20235
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/server/account/protos/Cardreaders;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/Cardreaders;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    .line 20236
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    .line 20237
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    .line 20238
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    .line 20239
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    .line 20240
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    .line 20241
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    .line 20242
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    .line 20243
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    .line 20244
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    .line 20245
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    .line 20246
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    .line 20247
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    .line 20248
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    .line 20249
    :cond_d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    .line 20250
    :cond_e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    if-eqz v0, :cond_f

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    .line 20251
    :cond_f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    if-eqz v0, :cond_10

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    .line 20252
    :cond_10
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    if-eqz v0, :cond_11

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    .line 20253
    :cond_11
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    if-eqz v0, :cond_12

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    .line 20254
    :cond_12
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    if-eqz v0, :cond_13

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    .line 20255
    :cond_13
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    if-eqz v0, :cond_14

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    .line 20256
    :cond_14
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    if-eqz v0, :cond_15

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    .line 20257
    :cond_15
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    if-eqz v0, :cond_16

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    .line 20258
    :cond_16
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    if-eqz v0, :cond_17

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    .line 20259
    :cond_17
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    if-eqz v0, :cond_18

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    .line 20260
    :cond_18
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    if-eqz v0, :cond_19

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    .line 20261
    :cond_19
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    if-eqz v0, :cond_1a

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    .line 20262
    :cond_1a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    if-eqz v0, :cond_1b

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    .line 20263
    :cond_1b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    if-eqz v0, :cond_1c

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    .line 20264
    :cond_1c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    if-eqz v0, :cond_1d

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    .line 20265
    :cond_1d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    if-eqz v0, :cond_1e

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    .line 20266
    :cond_1e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    if-eqz v0, :cond_1f

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    .line 20267
    :cond_1f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    if-eqz v0, :cond_20

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    .line 20268
    :cond_20
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    if-eqz v0, :cond_21

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    .line 20269
    :cond_21
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    if-eqz v0, :cond_22

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    .line 20270
    :cond_22
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    if-eqz v0, :cond_23

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    .line 20271
    :cond_23
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    if-eqz v0, :cond_24

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    .line 20272
    :cond_24
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    if-eqz v0, :cond_25

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    .line 20273
    :cond_25
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    if-eqz v0, :cond_26

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    iput-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    .line 20274
    :cond_26
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 20275
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19730
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ProtoAdapter_FlagsAndPermissions;->redact(Lcom/squareup/server/account/protos/FlagsAndPermissions;)Lcom/squareup/server/account/protos/FlagsAndPermissions;

    move-result-object p1

    return-object p1
.end method
