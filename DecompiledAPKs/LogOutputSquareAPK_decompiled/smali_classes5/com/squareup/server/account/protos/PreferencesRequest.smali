.class public final Lcom/squareup/server/account/protos/PreferencesRequest;
.super Lcom/squareup/wire/AndroidMessage;
.source "PreferencesRequest.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/PreferencesRequest$ProtoAdapter_PreferencesRequest;,
        Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/PreferencesRequest;",
        "Lcom/squareup/server/account/protos/PreferencesRequest$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/PreferencesRequest;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/PreferencesRequest;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALLOW_INSTANT_DEPOSIT:Ljava/lang/Boolean;

.field public static final DEFAULT_CASH_MANAGEMENT_EMAIL_REPORT:Ljava/lang/Boolean;

.field public static final DEFAULT_CASH_MANAGEMENT_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_CASH_MANAGEMENT_PRINT_REPORT:Ljava/lang/Boolean;

.field public static final DEFAULT_CASH_MANAGEMENT_REPORT_RECIPIENT_EMAIL:Ljava/lang/String; = ""

.field public static final DEFAULT_CUSTOMER_MANAGEMENT_COLLECT_AFTER_CHECKOUT:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOMER_MANAGEMENT_COLLECT_BEFORE_CHECKOUT:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOMER_MANAGEMENT_SHOW_EMAIL_COLLECTION_SCREEN:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOMER_MANAGEMENT_SHOW_SAVE_CARD_BUTTON_AFTER_CHECKOUT:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOMER_MANAGEMENT_USE_CARD_ON_FILE:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOM_TENDER_OPTIONS_PROTO:Ljava/lang/String; = ""

.field public static final DEFAULT_EMPLOYEE_MANAGEMENT_ENABLED_FOR_ACCOUNT:Ljava/lang/Boolean;

.field public static final DEFAULT_EMPLOYEE_MANAGEMENT_ENABLED_FOR_DEVICE:Ljava/lang/Boolean;

.field public static final DEFAULT_EMPLOYEE_MANAGEMENT_INACTIVITY_TIMEOUT:Ljava/lang/String; = ""

.field public static final DEFAULT_EMPLOYEE_MANAGEMENT_TRACKING_LEVEL:Ljava/lang/String; = ""

.field public static final DEFAULT_FOR_PAPER_SIGNATURE_ALWAYS_PRINT_CUSTOMER_COPY:Ljava/lang/Boolean;

.field public static final DEFAULT_FOR_PAPER_SIGNATURE_PRINT_ADDITIONAL_AUTH_SLIP:Ljava/lang/Boolean;

.field public static final DEFAULT_FOR_PAPER_SIGNATURE_USE_QUICK_TIP_RECEIPT:Ljava/lang/Boolean;

.field public static final DEFAULT_OPEN_TICKETS_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_REQUIRES_EMPLOYEE_LOGOUT_AFTER_COMPLETED_TRANSACTION:Ljava/lang/Boolean;

.field public static final DEFAULT_SKIP_SIGNATURE:Ljava/lang/Boolean;

.field public static final DEFAULT_SKIP_SIGNATURE_ALWAYS:Ljava/lang/Boolean;

.field public static final DEFAULT_STORE_AND_FORWARD_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_TERMINAL_CONNECTED_MODE_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_TIPPING_CALCULATION_PHASE:Ljava/lang/String; = ""

.field public static final DEFAULT_TIPPING_CUSTOM_PERCENTAGES:Ljava/lang/String; = ""

.field public static final DEFAULT_TIPPING_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_TIPPING_USE_CUSTOM_PERCENTAGES:Ljava/lang/Boolean;

.field public static final DEFAULT_TIPPING_USE_MANUAL_TIP_ENTRY:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_ALLOW_SWIPE_FOR_CHIP_CARDS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CURATED_IMAGE_FOR_RECEIPT:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_OPEN_TICKETS_MENU_AS_HOME_SCREEN:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_PAPER_SIGNATURE:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_PREDEFINED_TICKETS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_SEPARATE_TIPPING_SCREEN:Ljava/lang/Boolean;

.field public static final DEFAULT_USING_TIME_TRACKING:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final allow_instant_deposit:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x26
    .end annotation
.end field

.field public final cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x1f
    .end annotation
.end field

.field public final cash_management_email_report:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1d
    .end annotation
.end field

.field public final cash_management_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1c
    .end annotation
.end field

.field public final cash_management_print_report:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1e
    .end annotation
.end field

.field public final cash_management_report_recipient_email:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x20
    .end annotation
.end field

.field public final custom_tender_options_proto:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x18
    .end annotation
.end field

.field public final customer_management_collect_after_checkout:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x22
    .end annotation
.end field

.field public final customer_management_collect_before_checkout:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x21
    .end annotation
.end field

.field public final customer_management_show_email_collection_screen:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x25
    .end annotation
.end field

.field public final customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x24
    .end annotation
.end field

.field public final customer_management_use_card_on_file:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x23
    .end annotation
.end field

.field public final employee_management_enabled_for_account:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x11
    .end annotation
.end field

.field public final employee_management_enabled_for_device:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x12
    .end annotation
.end field

.field public final employee_management_inactivity_timeout:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x13
    .end annotation
.end field

.field public final employee_management_tracking_level:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x14
    .end annotation
.end field

.field public final for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xe
    .end annotation
.end field

.field public final for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x10
    .end annotation
.end field

.field public final for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xf
    .end annotation
.end field

.field public final open_tickets_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x28
    .end annotation
.end field

.field public final skip_signature:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final skip_signature_always:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x19
    .end annotation
.end field

.field public final store_and_forward_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final terminal_connected_mode_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x27
    .end annotation
.end field

.field public final tipping_calculation_phase:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x16
    .end annotation
.end field

.field public final tipping_custom_percentages:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final tipping_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final tipping_use_custom_percentages:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final tipping_use_manual_tip_entry:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x15
    .end annotation
.end field

.field public final use_curated_image_for_receipt:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1a
    .end annotation
.end field

.field public final use_paper_signature:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xd
    .end annotation
.end field

.field public final use_predefined_tickets:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1b
    .end annotation
.end field

.field public final use_separate_tipping_screen:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final using_time_tracking:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x17
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/server/account/protos/PreferencesRequest$ProtoAdapter_PreferencesRequest;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/PreferencesRequest$ProtoAdapter_PreferencesRequest;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 35
    sget-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 39
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_TIPPING_ENABLED:Ljava/lang/Boolean;

    .line 41
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_TIPPING_USE_CUSTOM_PERCENTAGES:Ljava/lang/Boolean;

    .line 43
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_TIPPING_USE_MANUAL_TIP_ENTRY:Ljava/lang/Boolean;

    .line 47
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_SKIP_SIGNATURE:Ljava/lang/Boolean;

    .line 49
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_STORE_AND_FORWARD_ENABLED:Ljava/lang/Boolean;

    .line 51
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_USE_CURATED_IMAGE_FOR_RECEIPT:Ljava/lang/Boolean;

    .line 53
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_USE_SEPARATE_TIPPING_SCREEN:Ljava/lang/Boolean;

    .line 55
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_OPEN_TICKETS_ENABLED:Ljava/lang/Boolean;

    .line 57
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_USE_PAPER_SIGNATURE:Ljava/lang/Boolean;

    .line 59
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_FOR_PAPER_SIGNATURE_ALWAYS_PRINT_CUSTOMER_COPY:Ljava/lang/Boolean;

    .line 61
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_FOR_PAPER_SIGNATURE_USE_QUICK_TIP_RECEIPT:Ljava/lang/Boolean;

    .line 63
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_FOR_PAPER_SIGNATURE_PRINT_ADDITIONAL_AUTH_SLIP:Ljava/lang/Boolean;

    .line 65
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_EMPLOYEE_MANAGEMENT_ENABLED_FOR_ACCOUNT:Ljava/lang/Boolean;

    .line 67
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_EMPLOYEE_MANAGEMENT_ENABLED_FOR_DEVICE:Ljava/lang/Boolean;

    .line 73
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_USE_ALLOW_SWIPE_FOR_CHIP_CARDS:Ljava/lang/Boolean;

    .line 77
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_USING_TIME_TRACKING:Ljava/lang/Boolean;

    .line 81
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_SKIP_SIGNATURE_ALWAYS:Ljava/lang/Boolean;

    .line 83
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_USE_OPEN_TICKETS_MENU_AS_HOME_SCREEN:Ljava/lang/Boolean;

    .line 85
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_USE_PREDEFINED_TICKETS:Ljava/lang/Boolean;

    .line 87
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_CASH_MANAGEMENT_ENABLED:Ljava/lang/Boolean;

    .line 89
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_CASH_MANAGEMENT_EMAIL_REPORT:Ljava/lang/Boolean;

    .line 91
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_CASH_MANAGEMENT_PRINT_REPORT:Ljava/lang/Boolean;

    .line 95
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_CUSTOMER_MANAGEMENT_COLLECT_BEFORE_CHECKOUT:Ljava/lang/Boolean;

    .line 97
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_CUSTOMER_MANAGEMENT_COLLECT_AFTER_CHECKOUT:Ljava/lang/Boolean;

    .line 99
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_CUSTOMER_MANAGEMENT_USE_CARD_ON_FILE:Ljava/lang/Boolean;

    .line 101
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_CUSTOMER_MANAGEMENT_SHOW_SAVE_CARD_BUTTON_AFTER_CHECKOUT:Ljava/lang/Boolean;

    .line 103
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_CUSTOMER_MANAGEMENT_SHOW_EMAIL_COLLECTION_SCREEN:Ljava/lang/Boolean;

    .line 105
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_ALLOW_INSTANT_DEPOSIT:Ljava/lang/Boolean;

    .line 107
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_TERMINAL_CONNECTED_MODE_ENABLED:Ljava/lang/Boolean;

    .line 109
    sput-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->DEFAULT_REQUIRES_EMPLOYEE_LOGOUT_AFTER_COMPLETED_TRANSACTION:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;Lokio/ByteString;)V
    .locals 1

    .line 382
    sget-object v0, Lcom/squareup/server/account/protos/PreferencesRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 383
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_enabled:Ljava/lang/Boolean;

    .line 384
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    .line 385
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    .line 386
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_custom_percentages:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_custom_percentages:Ljava/lang/String;

    .line 387
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->skip_signature:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature:Ljava/lang/Boolean;

    .line 388
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->store_and_forward_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_enabled:Ljava/lang/Boolean;

    .line 389
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    .line 390
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    .line 391
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_separate_tipping_screen:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_separate_tipping_screen:Ljava/lang/Boolean;

    .line 392
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->open_tickets_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->open_tickets_enabled:Ljava/lang/Boolean;

    .line 393
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_paper_signature:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_paper_signature:Ljava/lang/Boolean;

    .line 394
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    .line 395
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    .line 396
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    .line 397
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    .line 398
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    .line 399
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_inactivity_timeout:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_inactivity_timeout:Ljava/lang/String;

    .line 400
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_tracking_level:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_tracking_level:Ljava/lang/String;

    .line 401
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    .line 402
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_calculation_phase:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_calculation_phase:Ljava/lang/String;

    .line 403
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->using_time_tracking:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->using_time_tracking:Ljava/lang/Boolean;

    .line 404
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->custom_tender_options_proto:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->custom_tender_options_proto:Ljava/lang/String;

    .line 405
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->skip_signature_always:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature_always:Ljava/lang/Boolean;

    .line 406
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    .line 407
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_predefined_tickets:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_predefined_tickets:Ljava/lang/Boolean;

    .line 408
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_enabled:Ljava/lang/Boolean;

    .line 409
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_email_report:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_email_report:Ljava/lang/Boolean;

    .line 410
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_print_report:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_print_report:Ljava/lang/Boolean;

    .line 411
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    .line 412
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_report_recipient_email:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_report_recipient_email:Ljava/lang/String;

    .line 413
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    .line 414
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    .line 415
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    .line 416
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    .line 417
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    .line 418
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->allow_instant_deposit:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->allow_instant_deposit:Ljava/lang/Boolean;

    .line 419
    iget-object p2, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    .line 420
    iget-object p1, p1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 657
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/PreferencesRequest;->newBuilder()Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 471
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 472
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/PreferencesRequest;

    .line 473
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/PreferencesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/PreferencesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_enabled:Ljava/lang/Boolean;

    .line 474
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    .line 475
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    .line 476
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_custom_percentages:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_custom_percentages:Ljava/lang/String;

    .line 477
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature:Ljava/lang/Boolean;

    .line 478
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_enabled:Ljava/lang/Boolean;

    .line 479
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    .line 480
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    .line 481
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_separate_tipping_screen:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_separate_tipping_screen:Ljava/lang/Boolean;

    .line 482
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->open_tickets_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->open_tickets_enabled:Ljava/lang/Boolean;

    .line 483
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_paper_signature:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_paper_signature:Ljava/lang/Boolean;

    .line 484
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    .line 485
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    .line 486
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    .line 487
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    .line 488
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    .line 489
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_inactivity_timeout:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_inactivity_timeout:Ljava/lang/String;

    .line 490
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_tracking_level:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_tracking_level:Ljava/lang/String;

    .line 491
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    .line 492
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_calculation_phase:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_calculation_phase:Ljava/lang/String;

    .line 493
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->using_time_tracking:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->using_time_tracking:Ljava/lang/Boolean;

    .line 494
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->custom_tender_options_proto:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->custom_tender_options_proto:Ljava/lang/String;

    .line 495
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature_always:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature_always:Ljava/lang/Boolean;

    .line 496
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    .line 497
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_predefined_tickets:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_predefined_tickets:Ljava/lang/Boolean;

    .line 498
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_enabled:Ljava/lang/Boolean;

    .line 499
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_email_report:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_email_report:Ljava/lang/Boolean;

    .line 500
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_print_report:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_print_report:Ljava/lang/Boolean;

    .line 501
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    .line 502
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_report_recipient_email:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_report_recipient_email:Ljava/lang/String;

    .line 503
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    .line 504
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    .line 505
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    .line 506
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    .line 507
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    .line 508
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->allow_instant_deposit:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->allow_instant_deposit:Ljava/lang/Boolean;

    .line 509
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    .line 510
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    .line 511
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 516
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_26

    .line 518
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/PreferencesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 519
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_enabled:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 520
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 521
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 522
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_custom_percentages:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 523
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 524
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 525
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 526
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 527
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_separate_tipping_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 528
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->open_tickets_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 529
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_paper_signature:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 530
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 531
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 532
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 533
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 534
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 535
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_inactivity_timeout:Ljava/lang/String;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 536
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_tracking_level:Ljava/lang/String;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 537
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 538
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_calculation_phase:Ljava/lang/String;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 539
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->using_time_tracking:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 540
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->custom_tender_options_proto:Ljava/lang/String;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 541
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature_always:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 542
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 543
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_predefined_tickets:Ljava/lang/Boolean;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 544
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 545
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_email_report:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_1a
    const/4 v1, 0x0

    :goto_1a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 546
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_print_report:Ljava/lang/Boolean;

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1b

    :cond_1b
    const/4 v1, 0x0

    :goto_1b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 547
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1c

    :cond_1c
    const/4 v1, 0x0

    :goto_1c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 548
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_report_recipient_email:Ljava/lang/String;

    if-eqz v1, :cond_1d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1d

    :cond_1d
    const/4 v1, 0x0

    :goto_1d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 549
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_1e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1e

    :cond_1e
    const/4 v1, 0x0

    :goto_1e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 550
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_1f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1f

    :cond_1f
    const/4 v1, 0x0

    :goto_1f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 551
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    if-eqz v1, :cond_20

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_20

    :cond_20
    const/4 v1, 0x0

    :goto_20
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 552
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_21

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_21

    :cond_21
    const/4 v1, 0x0

    :goto_21
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 553
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_22

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_22

    :cond_22
    const/4 v1, 0x0

    :goto_22
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 554
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->allow_instant_deposit:Ljava/lang/Boolean;

    if-eqz v1, :cond_23

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_23

    :cond_23
    const/4 v1, 0x0

    :goto_23
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 555
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_24

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_24

    :cond_24
    const/4 v1, 0x0

    :goto_24
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 556
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    if-eqz v1, :cond_25

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_25
    add-int/2addr v0, v2

    .line 557
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_26
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 2

    .line 425
    new-instance v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;-><init>()V

    .line 426
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_enabled:Ljava/lang/Boolean;

    .line 427
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    .line 428
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    .line 429
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_custom_percentages:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_custom_percentages:Ljava/lang/String;

    .line 430
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->skip_signature:Ljava/lang/Boolean;

    .line 431
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->store_and_forward_enabled:Ljava/lang/Boolean;

    .line 432
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    .line 433
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    .line 434
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_separate_tipping_screen:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_separate_tipping_screen:Ljava/lang/Boolean;

    .line 435
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->open_tickets_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->open_tickets_enabled:Ljava/lang/Boolean;

    .line 436
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_paper_signature:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_paper_signature:Ljava/lang/Boolean;

    .line 437
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    .line 438
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    .line 439
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    .line 440
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    .line 441
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    .line 442
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_inactivity_timeout:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_inactivity_timeout:Ljava/lang/String;

    .line 443
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_tracking_level:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_tracking_level:Ljava/lang/String;

    .line 444
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    .line 445
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_calculation_phase:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_calculation_phase:Ljava/lang/String;

    .line 446
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->using_time_tracking:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->using_time_tracking:Ljava/lang/Boolean;

    .line 447
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->custom_tender_options_proto:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->custom_tender_options_proto:Ljava/lang/String;

    .line 448
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature_always:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->skip_signature_always:Ljava/lang/Boolean;

    .line 449
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    .line 450
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_predefined_tickets:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_predefined_tickets:Ljava/lang/Boolean;

    .line 451
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_enabled:Ljava/lang/Boolean;

    .line 452
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_email_report:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_email_report:Ljava/lang/Boolean;

    .line 453
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_print_report:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_print_report:Ljava/lang/Boolean;

    .line 454
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    .line 455
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_report_recipient_email:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_report_recipient_email:Ljava/lang/String;

    .line 456
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    .line 457
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    .line 458
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    .line 459
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    .line 460
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    .line 461
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->allow_instant_deposit:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->allow_instant_deposit:Ljava/lang/Boolean;

    .line 462
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    .line 463
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    .line 464
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/PreferencesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/PreferencesRequest;->newBuilder()Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/PreferencesRequest;)Lcom/squareup/server/account/protos/PreferencesRequest;
    .locals 2

    .line 615
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_enabled:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 616
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_use_custom_percentages(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 617
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_use_manual_tip_entry(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 618
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_custom_percentages:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_custom_percentages:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_custom_percentages(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 619
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->skip_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 620
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->store_and_forward_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 621
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->store_and_forward_single_transaction_limit(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 622
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_curated_image_for_receipt(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 623
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_separate_tipping_screen:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_separate_tipping_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_separate_tipping_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 624
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->open_tickets_enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->open_tickets_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->open_tickets_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 625
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_paper_signature:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_paper_signature:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_paper_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 626
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->for_paper_signature_always_print_customer_copy(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 627
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->for_paper_signature_use_quick_tip_receipt(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 628
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->for_paper_signature_print_additional_auth_slip(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 629
    :cond_d
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_enabled_for_account(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 630
    :cond_e
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_enabled_for_device(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 631
    :cond_f
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_inactivity_timeout:Ljava/lang/String;

    if-eqz v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_inactivity_timeout:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_inactivity_timeout(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 632
    :cond_10
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_tracking_level:Ljava/lang/String;

    if-eqz v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_tracking_level:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_tracking_level(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 633
    :cond_11
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_allow_swipe_for_chip_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 634
    :cond_12
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_calculation_phase:Ljava/lang/String;

    if-eqz v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_calculation_phase:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_calculation_phase(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 635
    :cond_13
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->using_time_tracking:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->using_time_tracking:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->using_time_tracking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 636
    :cond_14
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->custom_tender_options_proto:Ljava/lang/String;

    if-eqz v0, :cond_15

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->custom_tender_options_proto:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->custom_tender_options_proto(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 637
    :cond_15
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature_always:Ljava/lang/Boolean;

    if-eqz v0, :cond_16

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature_always:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->skip_signature_always(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 638
    :cond_16
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    if-eqz v0, :cond_17

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_open_tickets_menu_as_home_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 639
    :cond_17
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_predefined_tickets:Ljava/lang/Boolean;

    if-eqz v0, :cond_18

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->use_predefined_tickets:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_predefined_tickets(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 640
    :cond_18
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_19

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 641
    :cond_19
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_email_report:Ljava/lang/Boolean;

    if-eqz v0, :cond_1a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_email_report:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_email_report(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 642
    :cond_1a
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_print_report:Ljava/lang/Boolean;

    if-eqz v0, :cond_1b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_print_report:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_print_report(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 643
    :cond_1b
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_default_starting_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 644
    :cond_1c
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_report_recipient_email:Ljava/lang/String;

    if-eqz v0, :cond_1d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_report_recipient_email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_report_recipient_email(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 645
    :cond_1d
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    if-eqz v0, :cond_1e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_collect_before_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 646
    :cond_1e
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    if-eqz v0, :cond_1f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_collect_after_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 647
    :cond_1f
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    if-eqz v0, :cond_20

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_use_card_on_file(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 648
    :cond_20
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    if-eqz v0, :cond_21

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_show_save_card_button_after_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 649
    :cond_21
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    if-eqz v0, :cond_22

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_show_email_collection_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 650
    :cond_22
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->allow_instant_deposit:Ljava/lang/Boolean;

    if-eqz v0, :cond_23

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->allow_instant_deposit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->allow_instant_deposit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 651
    :cond_23
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_24

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->terminal_connected_mode_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    .line 652
    :cond_24
    iget-object v0, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    if-eqz v0, :cond_25

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->requireBuilder(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->requires_employee_logout_after_completed_transaction(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object v1

    :cond_25
    if-nez v1, :cond_26

    move-object p1, p0

    goto :goto_0

    .line 653
    :cond_26
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->build()Lcom/squareup/server/account/protos/PreferencesRequest;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/server/account/protos/PreferencesRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/PreferencesRequest;->overlay(Lcom/squareup/server/account/protos/PreferencesRequest;)Lcom/squareup/server/account/protos/PreferencesRequest;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/PreferencesRequest;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/PreferencesRequest;->populateDefaults()Lcom/squareup/server/account/protos/PreferencesRequest;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 564
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 565
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", tipping_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 566
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", tipping_use_custom_percentages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 567
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", tipping_use_manual_tip_entry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 568
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_custom_percentages:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", tipping_custom_percentages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_custom_percentages:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 569
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", skip_signature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 570
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", store_and_forward_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 571
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", store_and_forward_single_transaction_limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 572
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", use_curated_image_for_receipt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 573
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_separate_tipping_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", use_separate_tipping_screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_separate_tipping_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 574
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->open_tickets_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", open_tickets_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->open_tickets_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 575
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_paper_signature:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", use_paper_signature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_paper_signature:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 576
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", for_paper_signature_always_print_customer_copy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 577
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", for_paper_signature_use_quick_tip_receipt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 578
    :cond_c
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", for_paper_signature_print_additional_auth_slip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 579
    :cond_d
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    const-string v1, ", employee_management_enabled_for_account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 580
    :cond_e
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    const-string v1, ", employee_management_enabled_for_device="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 581
    :cond_f
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_inactivity_timeout:Ljava/lang/String;

    if-eqz v1, :cond_10

    const-string v1, ", employee_management_inactivity_timeout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_inactivity_timeout:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 582
    :cond_10
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_tracking_level:Ljava/lang/String;

    if-eqz v1, :cond_11

    const-string v1, ", employee_management_tracking_level="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_tracking_level:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 583
    :cond_11
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    const-string v1, ", use_allow_swipe_for_chip_cards="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 584
    :cond_12
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_calculation_phase:Ljava/lang/String;

    if-eqz v1, :cond_13

    const-string v1, ", tipping_calculation_phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_calculation_phase:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 585
    :cond_13
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->using_time_tracking:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    const-string v1, ", using_time_tracking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->using_time_tracking:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 586
    :cond_14
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->custom_tender_options_proto:Ljava/lang/String;

    if-eqz v1, :cond_15

    const-string v1, ", custom_tender_options_proto="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->custom_tender_options_proto:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 587
    :cond_15
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature_always:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    const-string v1, ", skip_signature_always="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature_always:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 588
    :cond_16
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    const-string v1, ", use_open_tickets_menu_as_home_screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 589
    :cond_17
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_predefined_tickets:Ljava/lang/Boolean;

    if-eqz v1, :cond_18

    const-string v1, ", use_predefined_tickets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_predefined_tickets:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 590
    :cond_18
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    const-string v1, ", cash_management_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 591
    :cond_19
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_email_report:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    const-string v1, ", cash_management_email_report="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_email_report:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 592
    :cond_1a
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_print_report:Ljava/lang/Boolean;

    if-eqz v1, :cond_1b

    const-string v1, ", cash_management_print_report="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_print_report:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 593
    :cond_1b
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1c

    const-string v1, ", cash_management_default_starting_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 594
    :cond_1c
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_report_recipient_email:Ljava/lang/String;

    if-eqz v1, :cond_1d

    const-string v1, ", cash_management_report_recipient_email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_report_recipient_email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 595
    :cond_1d
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_1e

    const-string v1, ", customer_management_collect_before_checkout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 596
    :cond_1e
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_1f

    const-string v1, ", customer_management_collect_after_checkout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 597
    :cond_1f
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    if-eqz v1, :cond_20

    const-string v1, ", customer_management_use_card_on_file="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 598
    :cond_20
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_21

    const-string v1, ", customer_management_show_save_card_button_after_checkout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 599
    :cond_21
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_22

    const-string v1, ", customer_management_show_email_collection_screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 600
    :cond_22
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->allow_instant_deposit:Ljava/lang/Boolean;

    if-eqz v1, :cond_23

    const-string v1, ", allow_instant_deposit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->allow_instant_deposit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 601
    :cond_23
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_24

    const-string v1, ", terminal_connected_mode_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 602
    :cond_24
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    if-eqz v1, :cond_25

    const-string v1, ", requires_employee_logout_after_completed_transaction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_25
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PreferencesRequest{"

    .line 603
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
