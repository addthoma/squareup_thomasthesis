.class public final Lcom/squareup/server/account/protos/ProcessingFees$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ProcessingFees.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/ProcessingFees;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/ProcessingFees;",
        "Lcom/squareup/server/account/protos/ProcessingFees$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cnp:Lcom/squareup/server/account/protos/ProcessingFee;

.field public cof:Lcom/squareup/server/account/protos/ProcessingFee;

.field public cp:Lcom/squareup/server/account/protos/ProcessingFee;

.field public interac:Lcom/squareup/server/account/protos/ProcessingFee;

.field public invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

.field public jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 208
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/ProcessingFees;
    .locals 9

    .line 243
    new-instance v8, Lcom/squareup/server/account/protos/ProcessingFees;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    iget-object v2, p0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    iget-object v3, p0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    iget-object v4, p0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    iget-object v5, p0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    iget-object v6, p0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/server/account/protos/ProcessingFees;-><init>(Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 195
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->build()Lcom/squareup/server/account/protos/ProcessingFees;

    move-result-object v0

    return-object v0
.end method

.method public cnp(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;
    .locals 0

    .line 212
    iput-object p1, p0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    return-object p0
.end method

.method public cof(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    return-object p0
.end method

.method public cp(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;
    .locals 0

    .line 217
    iput-object p1, p0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    return-object p0
.end method

.method public interac(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    return-object p0
.end method

.method public invoice_web_form(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    return-object p0
.end method

.method public jcb_cp(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    return-object p0
.end method
