.class public final Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StoreAndForwardKey.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/StoreAndForwardKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/StoreAndForwardKey;",
        "Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bletchley_key_id:Ljava/lang/String;

.field public expiration:Ljava/lang/String;

.field public raw_certificate:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 143
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bletchley_key_id(Ljava/lang/String;)Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->bletchley_key_id:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/StoreAndForwardKey;
    .locals 5

    .line 163
    new-instance v0, Lcom/squareup/server/account/protos/StoreAndForwardKey;

    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->bletchley_key_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->expiration:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->raw_certificate:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/server/account/protos/StoreAndForwardKey;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 136
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->build()Lcom/squareup/server/account/protos/StoreAndForwardKey;

    move-result-object v0

    return-object v0
.end method

.method public expiration(Ljava/lang/String;)Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->expiration:Ljava/lang/String;

    return-object p0
.end method

.method public raw_certificate(Ljava/lang/String;)Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;
    .locals 0

    .line 157
    iput-object p1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->raw_certificate:Ljava/lang/String;

    return-object p0
.end method
