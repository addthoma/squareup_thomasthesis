.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Crm"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$ProtoAdapter_Crm;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALLOWS_COUPONS_IN_DIRECT_MESSAGE:Ljava/lang/Boolean;

.field public static final DEFAULT_BUYER_EMAIL_COLLECTION:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_CONFIGURE_PROFILES_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_DELETE_GROUPS:Ljava/lang/Boolean;

.field public static final DEFAULT_DISABLE_BUYER_PROFILE:Ljava/lang/Boolean;

.field public static final DEFAULT_MANAGE_LOYALTY_IN_DIRECTORY:Ljava/lang/Boolean;

.field public static final DEFAULT_MERGE_IN_REGISTER:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_ADHOC_FILTERS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CUSTOM_ATTRIBUTES_EXTRA_DEFAULT_FIELDS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_FILTER_LOCATION:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_FILTER_MANUAL_GROUP:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_FILTER_VISIT_FREQUENCY:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_NEW_GROUPS_AND_FILTERING:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_REMINDERS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final allows_coupons_in_direct_message:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xf
    .end annotation
.end field

.field public final buyer_email_collection:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final can_configure_profiles_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x16
    .end annotation
.end field

.field public final can_delete_groups:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final disable_buyer_profile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x15
    .end annotation
.end field

.field public final manage_loyalty_in_directory:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x11
    .end annotation
.end field

.field public final merge_in_register:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final use_adhoc_filters:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final use_custom_attributes_extra_default_fields:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x14
    .end annotation
.end field

.field public final use_filter_location:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final use_filter_manual_group:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final use_filter_visit_frequency:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final use_new_groups_and_filtering:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final use_reminders:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5264
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$ProtoAdapter_Crm;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$ProtoAdapter_Crm;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 5266
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 5270
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_BUYER_EMAIL_COLLECTION:Ljava/lang/Boolean;

    .line 5272
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_MERGE_IN_REGISTER:Ljava/lang/Boolean;

    .line 5274
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_USE_NEW_GROUPS_AND_FILTERING:Ljava/lang/Boolean;

    .line 5276
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_USE_ADHOC_FILTERS:Ljava/lang/Boolean;

    .line 5278
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_CAN_DELETE_GROUPS:Ljava/lang/Boolean;

    .line 5280
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_USE_FILTER_MANUAL_GROUP:Ljava/lang/Boolean;

    .line 5282
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_USE_FILTER_LOCATION:Ljava/lang/Boolean;

    .line 5284
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_USE_FILTER_VISIT_FREQUENCY:Ljava/lang/Boolean;

    .line 5286
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_USE_REMINDERS:Ljava/lang/Boolean;

    .line 5288
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_ALLOWS_COUPONS_IN_DIRECT_MESSAGE:Ljava/lang/Boolean;

    .line 5290
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_MANAGE_LOYALTY_IN_DIRECTORY:Ljava/lang/Boolean;

    .line 5292
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_USE_CUSTOM_ATTRIBUTES_EXTRA_DEFAULT_FIELDS:Ljava/lang/Boolean;

    .line 5294
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_DISABLE_BUYER_PROFILE:Ljava/lang/Boolean;

    .line 5296
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_CAN_CONFIGURE_PROFILES_ANDROID:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 16

    .line 5446
    sget-object v15, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 3

    move-object v0, p0

    .line 5458
    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p15

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 5459
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->buyer_email_collection:Ljava/lang/Boolean;

    move-object v1, p2

    .line 5460
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->merge_in_register:Ljava/lang/Boolean;

    move-object v1, p3

    .line 5461
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_new_groups_and_filtering:Ljava/lang/Boolean;

    move-object v1, p4

    .line 5462
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_adhoc_filters:Ljava/lang/Boolean;

    move-object v1, p5

    .line 5463
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_delete_groups:Ljava/lang/Boolean;

    move-object v1, p6

    .line 5464
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_manual_group:Ljava/lang/Boolean;

    move-object v1, p7

    .line 5465
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_location:Ljava/lang/Boolean;

    move-object v1, p8

    .line 5466
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_visit_frequency:Ljava/lang/Boolean;

    move-object v1, p9

    .line 5467
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_reminders:Ljava/lang/Boolean;

    move-object v1, p10

    .line 5468
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->allows_coupons_in_direct_message:Ljava/lang/Boolean;

    move-object v1, p11

    .line 5469
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->manage_loyalty_in_directory:Ljava/lang/Boolean;

    move-object v1, p12

    .line 5470
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_custom_attributes_extra_default_fields:Ljava/lang/Boolean;

    move-object/from16 v1, p13

    .line 5471
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->disable_buyer_profile:Ljava/lang/Boolean;

    move-object/from16 v1, p14

    .line 5472
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_configure_profiles_android:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 5603
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 5499
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 5500
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    .line 5501
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->buyer_email_collection:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->buyer_email_collection:Ljava/lang/Boolean;

    .line 5502
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->merge_in_register:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->merge_in_register:Ljava/lang/Boolean;

    .line 5503
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_new_groups_and_filtering:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_new_groups_and_filtering:Ljava/lang/Boolean;

    .line 5504
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_adhoc_filters:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_adhoc_filters:Ljava/lang/Boolean;

    .line 5505
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_delete_groups:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_delete_groups:Ljava/lang/Boolean;

    .line 5506
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_manual_group:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_manual_group:Ljava/lang/Boolean;

    .line 5507
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_location:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_location:Ljava/lang/Boolean;

    .line 5508
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_visit_frequency:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_visit_frequency:Ljava/lang/Boolean;

    .line 5509
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_reminders:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_reminders:Ljava/lang/Boolean;

    .line 5510
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->allows_coupons_in_direct_message:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->allows_coupons_in_direct_message:Ljava/lang/Boolean;

    .line 5511
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->manage_loyalty_in_directory:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->manage_loyalty_in_directory:Ljava/lang/Boolean;

    .line 5512
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_custom_attributes_extra_default_fields:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_custom_attributes_extra_default_fields:Ljava/lang/Boolean;

    .line 5513
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->disable_buyer_profile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->disable_buyer_profile:Ljava/lang/Boolean;

    .line 5514
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_configure_profiles_android:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_configure_profiles_android:Ljava/lang/Boolean;

    .line 5515
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 5520
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_e

    .line 5522
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 5523
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->buyer_email_collection:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5524
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->merge_in_register:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5525
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_new_groups_and_filtering:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5526
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_adhoc_filters:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5527
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_delete_groups:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5528
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_manual_group:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5529
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_location:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5530
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_visit_frequency:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5531
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_reminders:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5532
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->allows_coupons_in_direct_message:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5533
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->manage_loyalty_in_directory:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5534
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_custom_attributes_extra_default_fields:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5535
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->disable_buyer_profile:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5536
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_configure_profiles_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_d
    add-int/2addr v0, v2

    .line 5537
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_e
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;
    .locals 2

    .line 5477
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;-><init>()V

    .line 5478
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->buyer_email_collection:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->buyer_email_collection:Ljava/lang/Boolean;

    .line 5479
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->merge_in_register:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->merge_in_register:Ljava/lang/Boolean;

    .line 5480
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_new_groups_and_filtering:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_new_groups_and_filtering:Ljava/lang/Boolean;

    .line 5481
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_adhoc_filters:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_adhoc_filters:Ljava/lang/Boolean;

    .line 5482
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_delete_groups:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->can_delete_groups:Ljava/lang/Boolean;

    .line 5483
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_manual_group:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_filter_manual_group:Ljava/lang/Boolean;

    .line 5484
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_location:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_filter_location:Ljava/lang/Boolean;

    .line 5485
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_visit_frequency:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_filter_visit_frequency:Ljava/lang/Boolean;

    .line 5486
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_reminders:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_reminders:Ljava/lang/Boolean;

    .line 5487
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->allows_coupons_in_direct_message:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->allows_coupons_in_direct_message:Ljava/lang/Boolean;

    .line 5488
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->manage_loyalty_in_directory:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->manage_loyalty_in_directory:Ljava/lang/Boolean;

    .line 5489
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_custom_attributes_extra_default_fields:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_custom_attributes_extra_default_fields:Ljava/lang/Boolean;

    .line 5490
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->disable_buyer_profile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->disable_buyer_profile:Ljava/lang/Boolean;

    .line 5491
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_configure_profiles_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->can_configure_profiles_android:Ljava/lang/Boolean;

    .line 5492
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 5263
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;
    .locals 2

    .line 5585
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->buyer_email_collection:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->buyer_email_collection:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->buyer_email_collection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5586
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->merge_in_register:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->merge_in_register:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->merge_in_register(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5587
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_new_groups_and_filtering:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_new_groups_and_filtering:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_new_groups_and_filtering(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5588
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_adhoc_filters:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_adhoc_filters:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_adhoc_filters(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5589
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_delete_groups:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_delete_groups:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->can_delete_groups(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5590
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_manual_group:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_manual_group:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_filter_manual_group(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5591
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_location:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_location:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_filter_location(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5592
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_visit_frequency:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_visit_frequency:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_filter_visit_frequency(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5593
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_reminders:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_reminders:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_reminders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5594
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->allows_coupons_in_direct_message:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->allows_coupons_in_direct_message:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->allows_coupons_in_direct_message(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5595
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->manage_loyalty_in_directory:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->manage_loyalty_in_directory:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->manage_loyalty_in_directory(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5596
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_custom_attributes_extra_default_fields:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_custom_attributes_extra_default_fields:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_custom_attributes_extra_default_fields(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5597
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->disable_buyer_profile:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->disable_buyer_profile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->disable_buyer_profile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5598
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_configure_profiles_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_configure_profiles_android:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->can_configure_profiles_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    :cond_d
    if-nez v1, :cond_e

    move-object p1, p0

    goto :goto_0

    .line 5599
    :cond_e
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 5263
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;
    .locals 2

    .line 5565
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->buyer_email_collection:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_BUYER_EMAIL_COLLECTION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->buyer_email_collection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5566
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->merge_in_register:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_MERGE_IN_REGISTER:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->merge_in_register(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5567
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_new_groups_and_filtering:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_USE_NEW_GROUPS_AND_FILTERING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_new_groups_and_filtering(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5568
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_adhoc_filters:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_USE_ADHOC_FILTERS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_adhoc_filters(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5569
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_delete_groups:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_CAN_DELETE_GROUPS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->can_delete_groups(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5570
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_manual_group:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_USE_FILTER_MANUAL_GROUP:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_filter_manual_group(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5571
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_location:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_USE_FILTER_LOCATION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_filter_location(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5572
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_visit_frequency:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_USE_FILTER_VISIT_FREQUENCY:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_filter_visit_frequency(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5573
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_reminders:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_USE_REMINDERS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_reminders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5574
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->allows_coupons_in_direct_message:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_ALLOWS_COUPONS_IN_DIRECT_MESSAGE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->allows_coupons_in_direct_message(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5575
    :cond_9
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->manage_loyalty_in_directory:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_MANAGE_LOYALTY_IN_DIRECTORY:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->manage_loyalty_in_directory(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5576
    :cond_a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_custom_attributes_extra_default_fields:Ljava/lang/Boolean;

    if-nez v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_USE_CUSTOM_ATTRIBUTES_EXTRA_DEFAULT_FIELDS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->use_custom_attributes_extra_default_fields(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5577
    :cond_b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->disable_buyer_profile:Ljava/lang/Boolean;

    if-nez v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_DISABLE_BUYER_PROFILE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->disable_buyer_profile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    .line 5578
    :cond_c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_configure_profiles_android:Ljava/lang/Boolean;

    if-nez v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->DEFAULT_CAN_CONFIGURE_PROFILES_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->can_configure_profiles_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;

    move-result-object v1

    :cond_d
    if-nez v1, :cond_e

    move-object v0, p0

    goto :goto_0

    .line 5579
    :cond_e
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 5263
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 5544
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 5545
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->buyer_email_collection:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", buyer_email_collection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->buyer_email_collection:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5546
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->merge_in_register:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", merge_in_register="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->merge_in_register:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5547
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_new_groups_and_filtering:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", use_new_groups_and_filtering="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_new_groups_and_filtering:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5548
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_adhoc_filters:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", use_adhoc_filters="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_adhoc_filters:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5549
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_delete_groups:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", can_delete_groups="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_delete_groups:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5550
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_manual_group:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", use_filter_manual_group="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_manual_group:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5551
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_location:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", use_filter_location="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_location:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5552
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_visit_frequency:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", use_filter_visit_frequency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_filter_visit_frequency:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5553
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_reminders:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", use_reminders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_reminders:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5554
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->allows_coupons_in_direct_message:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", allows_coupons_in_direct_message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->allows_coupons_in_direct_message:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5555
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->manage_loyalty_in_directory:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", manage_loyalty_in_directory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->manage_loyalty_in_directory:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5556
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_custom_attributes_extra_default_fields:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", use_custom_attributes_extra_default_fields="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->use_custom_attributes_extra_default_fields:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5557
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->disable_buyer_profile:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", disable_buyer_profile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->disable_buyer_profile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5558
    :cond_c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_configure_profiles_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", can_configure_profiles_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->can_configure_profiles_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_d
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Crm{"

    .line 5559
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
