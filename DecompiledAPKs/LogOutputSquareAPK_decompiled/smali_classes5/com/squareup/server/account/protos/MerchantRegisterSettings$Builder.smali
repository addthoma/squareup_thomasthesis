.class public final Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MerchantRegisterSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/MerchantRegisterSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/MerchantRegisterSettings;",
        "Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public default_register_permissions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public legacy_guest_mode_expiration:Ljava/lang/String;

.field public show_share_team_passcode_warning:Ljava/lang/Boolean;

.field public team_passcode:Ljava/lang/String;

.field public team_role_token:Ljava/lang/String;

.field public use_team_permissions:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 203
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 204
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->default_register_permissions:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/MerchantRegisterSettings;
    .locals 9

    .line 243
    new-instance v8, Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->default_register_permissions:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->use_team_permissions:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->team_role_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->team_passcode:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->legacy_guest_mode_expiration:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->show_share_team_passcode_warning:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;-><init>(Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 190
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->build()Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    move-result-object v0

    return-object v0
.end method

.method public default_register_permissions(Ljava/util/List;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;"
        }
    .end annotation

    .line 208
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 209
    iput-object p1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->default_register_permissions:Ljava/util/List;

    return-object p0
.end method

.method public legacy_guest_mode_expiration(Ljava/lang/String;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->legacy_guest_mode_expiration:Ljava/lang/String;

    return-object p0
.end method

.method public show_share_team_passcode_warning(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->show_share_team_passcode_warning:Ljava/lang/Boolean;

    return-object p0
.end method

.method public team_passcode(Ljava/lang/String;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;
    .locals 0

    .line 224
    iput-object p1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->team_passcode:Ljava/lang/String;

    return-object p0
.end method

.method public team_role_token(Ljava/lang/String;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->team_role_token:Ljava/lang/String;

    return-object p0
.end method

.method public use_team_permissions(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->use_team_permissions:Ljava/lang/Boolean;

    return-object p0
.end method
