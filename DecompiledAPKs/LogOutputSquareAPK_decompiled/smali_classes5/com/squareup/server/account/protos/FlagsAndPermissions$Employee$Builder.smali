.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public time_tracking_device_level:Ljava/lang/Boolean;

.field public updated_guest_access:Ljava/lang/Boolean;

.field public use_reports_granular_permissions:Ljava/lang/Boolean;

.field public use_settings_granular_permissions:Ljava/lang/Boolean;

.field public use_view_detailed_sales_reports_permission:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11930
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;
    .locals 8

    .line 11964
    new-instance v7, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->updated_guest_access:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->time_tracking_device_level:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->use_reports_granular_permissions:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->use_settings_granular_permissions:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->use_view_detailed_sales_reports_permission:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 11919
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    move-result-object v0

    return-object v0
.end method

.method public time_tracking_device_level(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;
    .locals 0

    .line 11942
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->time_tracking_device_level:Ljava/lang/Boolean;

    return-object p0
.end method

.method public updated_guest_access(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;
    .locals 0

    .line 11937
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->updated_guest_access:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_reports_granular_permissions(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;
    .locals 0

    .line 11947
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->use_reports_granular_permissions:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_settings_granular_permissions(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;
    .locals 0

    .line 11952
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->use_settings_granular_permissions:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_view_detailed_sales_reports_permission(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;
    .locals 0

    .line 11958
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->use_view_detailed_sales_reports_permission:Ljava/lang/Boolean;

    return-object p0
.end method
