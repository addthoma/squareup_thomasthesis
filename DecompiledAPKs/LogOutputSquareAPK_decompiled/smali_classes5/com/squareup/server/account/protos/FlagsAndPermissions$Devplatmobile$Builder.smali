.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public use_paysdk:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13074
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;
    .locals 3

    .line 13087
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;->use_paysdk:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 13071
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    move-result-object v0

    return-object v0
.end method

.method public use_paysdk(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;
    .locals 0

    .line 13081
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile$Builder;->use_paysdk:Ljava/lang/Boolean;

    return-object p0
.end method
