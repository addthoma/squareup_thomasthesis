.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Items"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$ProtoAdapter_Items;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CAN_CREATE_EDIT_SERVICES:Ljava/lang/Boolean;

.field public static final DEFAULT_HAS_EXTRA_CREATE_EDIT_SERVICE_OPTIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_LIBRARY_LIST_SHOW_SERVICES:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOULD_SHOW_DISPLAY_PRICE:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_PHASE_THREE_FIELDS:Ljava/lang/Boolean;

.field public static final DEFAULT_SUPPORT_APPOINTMENT_ITEMS_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_ITEMIZED_PAYMENTS_TUTORIAL:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_ITEMS_TUTORIAL:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_POS_INTENT_PAYMENTS_TUTORIAL:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final can_create_edit_services:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final has_extra_create_edit_service_options:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final library_list_show_services:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final should_show_display_price:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final show_phase_three_fields:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final support_appointment_items_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final use_itemized_payments_tutorial:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final use_items_tutorial:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final use_pos_intent_payments_tutorial:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14768
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$ProtoAdapter_Items;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$ProtoAdapter_Items;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 14770
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 14774
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_USE_ITEMS_TUTORIAL:Ljava/lang/Boolean;

    .line 14776
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_LIBRARY_LIST_SHOW_SERVICES:Ljava/lang/Boolean;

    .line 14778
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_SUPPORT_APPOINTMENT_ITEMS_V2:Ljava/lang/Boolean;

    .line 14780
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_CAN_CREATE_EDIT_SERVICES:Ljava/lang/Boolean;

    .line 14782
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_HAS_EXTRA_CREATE_EDIT_SERVICE_OPTIONS:Ljava/lang/Boolean;

    .line 14784
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_SHOULD_SHOW_DISPLAY_PRICE:Ljava/lang/Boolean;

    .line 14786
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_SHOW_PHASE_THREE_FIELDS:Ljava/lang/Boolean;

    .line 14788
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_USE_ITEMIZED_PAYMENTS_TUTORIAL:Ljava/lang/Boolean;

    .line 14790
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_USE_POS_INTENT_PAYMENTS_TUTORIAL:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 11

    .line 14869
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 14878
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 14879
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_items_tutorial:Ljava/lang/Boolean;

    .line 14880
    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->library_list_show_services:Ljava/lang/Boolean;

    .line 14881
    iput-object p3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->support_appointment_items_v2:Ljava/lang/Boolean;

    .line 14882
    iput-object p4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->can_create_edit_services:Ljava/lang/Boolean;

    .line 14883
    iput-object p5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->has_extra_create_edit_service_options:Ljava/lang/Boolean;

    .line 14884
    iput-object p6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->should_show_display_price:Ljava/lang/Boolean;

    .line 14885
    iput-object p7, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->show_phase_three_fields:Ljava/lang/Boolean;

    .line 14886
    iput-object p8, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_itemized_payments_tutorial:Ljava/lang/Boolean;

    .line 14887
    iput-object p9, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_pos_intent_payments_tutorial:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 14988
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 14909
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 14910
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    .line 14911
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_items_tutorial:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_items_tutorial:Ljava/lang/Boolean;

    .line 14912
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->library_list_show_services:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->library_list_show_services:Ljava/lang/Boolean;

    .line 14913
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->support_appointment_items_v2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->support_appointment_items_v2:Ljava/lang/Boolean;

    .line 14914
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->can_create_edit_services:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->can_create_edit_services:Ljava/lang/Boolean;

    .line 14915
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->has_extra_create_edit_service_options:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->has_extra_create_edit_service_options:Ljava/lang/Boolean;

    .line 14916
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->should_show_display_price:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->should_show_display_price:Ljava/lang/Boolean;

    .line 14917
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->show_phase_three_fields:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->show_phase_three_fields:Ljava/lang/Boolean;

    .line 14918
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_itemized_payments_tutorial:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_itemized_payments_tutorial:Ljava/lang/Boolean;

    .line 14919
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_pos_intent_payments_tutorial:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_pos_intent_payments_tutorial:Ljava/lang/Boolean;

    .line 14920
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 14925
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_9

    .line 14927
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 14928
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_items_tutorial:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14929
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->library_list_show_services:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14930
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->support_appointment_items_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14931
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->can_create_edit_services:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14932
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->has_extra_create_edit_service_options:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14933
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->should_show_display_price:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14934
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->show_phase_three_fields:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14935
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_itemized_payments_tutorial:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14936
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_pos_intent_payments_tutorial:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 14937
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;
    .locals 2

    .line 14892
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;-><init>()V

    .line 14893
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_items_tutorial:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->use_items_tutorial:Ljava/lang/Boolean;

    .line 14894
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->library_list_show_services:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->library_list_show_services:Ljava/lang/Boolean;

    .line 14895
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->support_appointment_items_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->support_appointment_items_v2:Ljava/lang/Boolean;

    .line 14896
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->can_create_edit_services:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->can_create_edit_services:Ljava/lang/Boolean;

    .line 14897
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->has_extra_create_edit_service_options:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->has_extra_create_edit_service_options:Ljava/lang/Boolean;

    .line 14898
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->should_show_display_price:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->should_show_display_price:Ljava/lang/Boolean;

    .line 14899
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->show_phase_three_fields:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->show_phase_three_fields:Ljava/lang/Boolean;

    .line 14900
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_itemized_payments_tutorial:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->use_itemized_payments_tutorial:Ljava/lang/Boolean;

    .line 14901
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_pos_intent_payments_tutorial:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->use_pos_intent_payments_tutorial:Ljava/lang/Boolean;

    .line 14902
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 14767
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;
    .locals 2

    .line 14975
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_items_tutorial:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_items_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->use_items_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    .line 14976
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->library_list_show_services:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->library_list_show_services:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->library_list_show_services(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    .line 14977
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->support_appointment_items_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->support_appointment_items_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->support_appointment_items_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    .line 14978
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->can_create_edit_services:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->can_create_edit_services:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->can_create_edit_services(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    .line 14979
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->has_extra_create_edit_service_options:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->has_extra_create_edit_service_options:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->has_extra_create_edit_service_options(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    .line 14980
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->should_show_display_price:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->should_show_display_price:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->should_show_display_price(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    .line 14981
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->show_phase_three_fields:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->show_phase_three_fields:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->show_phase_three_fields(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    .line 14982
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_itemized_payments_tutorial:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_itemized_payments_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->use_itemized_payments_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    .line 14983
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_pos_intent_payments_tutorial:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_pos_intent_payments_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->use_pos_intent_payments_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    :cond_8
    if-nez v1, :cond_9

    move-object p1, p0

    goto :goto_0

    .line 14984
    :cond_9
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 14767
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;
    .locals 2

    .line 14960
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_items_tutorial:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_USE_ITEMS_TUTORIAL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->use_items_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    .line 14961
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->library_list_show_services:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_LIBRARY_LIST_SHOW_SERVICES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->library_list_show_services(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    .line 14962
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->support_appointment_items_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_SUPPORT_APPOINTMENT_ITEMS_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->support_appointment_items_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    .line 14963
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->can_create_edit_services:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_CAN_CREATE_EDIT_SERVICES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->can_create_edit_services(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    .line 14964
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->has_extra_create_edit_service_options:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_HAS_EXTRA_CREATE_EDIT_SERVICE_OPTIONS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->has_extra_create_edit_service_options(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    .line 14965
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->should_show_display_price:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_SHOULD_SHOW_DISPLAY_PRICE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->should_show_display_price(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    .line 14966
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->show_phase_three_fields:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_SHOW_PHASE_THREE_FIELDS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->show_phase_three_fields(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    .line 14967
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_itemized_payments_tutorial:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_USE_ITEMIZED_PAYMENTS_TUTORIAL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->use_itemized_payments_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    .line 14968
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_pos_intent_payments_tutorial:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->DEFAULT_USE_POS_INTENT_PAYMENTS_TUTORIAL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->use_pos_intent_payments_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;

    move-result-object v1

    :cond_8
    if-nez v1, :cond_9

    move-object v0, p0

    goto :goto_0

    .line 14969
    :cond_9
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 14767
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 14944
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14945
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_items_tutorial:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", use_items_tutorial="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_items_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14946
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->library_list_show_services:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", library_list_show_services="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->library_list_show_services:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14947
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->support_appointment_items_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", support_appointment_items_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->support_appointment_items_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14948
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->can_create_edit_services:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", can_create_edit_services="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->can_create_edit_services:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14949
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->has_extra_create_edit_service_options:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", has_extra_create_edit_service_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->has_extra_create_edit_service_options:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14950
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->should_show_display_price:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", should_show_display_price="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->should_show_display_price:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14951
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->show_phase_three_fields:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", show_phase_three_fields="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->show_phase_three_fields:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14952
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_itemized_payments_tutorial:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", use_itemized_payments_tutorial="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_itemized_payments_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14953
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_pos_intent_payments_tutorial:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", use_pos_intent_payments_tutorial="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->use_pos_intent_payments_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Items{"

    .line 14954
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
