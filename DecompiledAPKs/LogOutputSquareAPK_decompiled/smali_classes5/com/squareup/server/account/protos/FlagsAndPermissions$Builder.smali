.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public accept_third_party_gift_cards:Ljava/lang/Boolean;

.field public activation_url:Ljava/lang/String;

.field public allow_payment_for_invoice_android:Ljava/lang/Boolean;

.field public appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

.field public auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

.field public auto_capture_payments:Ljava/lang/Boolean;

.field public bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

.field public billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

.field public bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

.field public can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

.field public can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

.field public can_enable_tipping:Ljava/lang/Boolean;

.field public can_onboard:Ljava/lang/Boolean;

.field public can_publish_merchant_card:Ljava/lang/Boolean;

.field public can_refer_friend_in_app:Ljava/lang/Boolean;

.field public can_skip_receipt_screen:Ljava/lang/Boolean;

.field public can_transfer_open_tickets_android:Ljava/lang/Boolean;

.field public can_use_android_ecr:Ljava/lang/Boolean;

.field public capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

.field public cardholder_name_on_dip_android:Ljava/lang/Boolean;

.field public cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

.field public catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

.field public collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

.field public collect_tip_before_authorization_required:Ljava/lang/Boolean;

.field public contact_support_phone_number:Ljava/lang/String;

.field public contact_support_twitter:Ljava/lang/String;

.field public country_prefers_contactless_cards:Ljava/lang/Boolean;

.field public country_prefers_emv:Ljava/lang/Boolean;

.field public country_prefers_no_contactless_cards:Ljava/lang/Boolean;

.field public crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

.field public deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

.field public devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

.field public disable_item_editing:Ljava/lang/Boolean;

.field public disable_mobile_reports_applet_android:Ljava/lang/Boolean;

.field public disable_register_api:Ljava/lang/Boolean;

.field public discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

.field public display_learn_more_r4:Ljava/lang/Boolean;

.field public ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

.field public eligible_for_square_card_processing:Ljava/lang/Boolean;

.field public eligible_for_third_party_card_processing:Ljava/lang/Boolean;

.field public email_transaction_ledger:Ljava/lang/Boolean;

.field public employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

.field public employee_cancel_transaction:Ljava/lang/Boolean;

.field public employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

.field public fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

.field public giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

.field public has_bazaar_online_store:Ljava/lang/Boolean;

.field public help_center_url:Ljava/lang/String;

.field public hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

.field public invoice_tipping:Ljava/lang/Boolean;

.field public invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

.field public items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

.field public items_batch_size:Ljava/lang/Long;

.field public login_type:Ljava/lang/String;

.field public loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

.field public marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

.field public merge_open_tickets_android:Ljava/lang/Boolean;

.field public onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

.field public opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

.field public opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

.field public order_hub_sync_period_with_notifications:Ljava/lang/Long;

.field public order_hub_sync_period_without_notifications:Ljava/lang/Long;

.field public orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

.field public payment_cards:Ljava/lang/Boolean;

.field public paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

.field public payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

.field public placeholder:Ljava/lang/Boolean;

.field public pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

.field public posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

.field public prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

.field public print_logo_on_paper_android:Ljava/lang/Boolean;

.field public printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

.field public privacy_policy_url:Ljava/lang/String;

.field public protect_edit_tax:Ljava/lang/Boolean;

.field public readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

.field public reduce_printing_waste_android:Ljava/lang/Boolean;

.field public regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

.field public reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

.field public require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

.field public requires_track_2_if_not_amex:Ljava/lang/Boolean;

.field public restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

.field public retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

.field public rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

.field public see_advanced_modifiers:Ljava/lang/Boolean;

.field public see_payroll_register_feature_tour:Ljava/lang/Boolean;

.field public see_payroll_register_learn_more_android:Ljava/lang/Boolean;

.field public seller_agreement_url:Ljava/lang/String;

.field public show_feature_tour:Ljava/lang/Boolean;

.field public show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

.field public show_items_library_after_login:Ljava/lang/Boolean;

.field public show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

.field public skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

.field public skip_signatures_for_small_payments:Ljava/lang/Boolean;

.field public sms:Ljava/lang/Boolean;

.field public spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

.field public squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

.field public support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

.field public support_restaurant_items:Ljava/lang/Boolean;

.field public support_retail_items:Ljava/lang/Boolean;

.field public supported_card_brands_offline:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public supported_card_brands_online:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

.field public terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

.field public timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

.field public transfer_open_tickets_android:Ljava/lang/Boolean;

.field public upload_support_ledger:Ljava/lang/Boolean;

.field public use_a10:Ljava/lang/Boolean;

.field public use_android_barcode_scanning:Ljava/lang/Boolean;

.field public use_android_employee_management:Ljava/lang/Boolean;

.field public use_android_printer_stations:Ljava/lang/Boolean;

.field public use_australian_gross_sales:Ljava/lang/Boolean;

.field public use_card_associated_coupons:Ljava/lang/Boolean;

.field public use_card_on_file_in_android_register:Ljava/lang/Boolean;

.field public use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

.field public use_conditional_taxes:Ljava/lang/Boolean;

.field public use_crm_custom_attributes:Ljava/lang/Boolean;

.field public use_crm_messaging:Ljava/lang/Boolean;

.field public use_crm_notes:Ljava/lang/Boolean;

.field public use_customer_directory_with_invoices:Ljava/lang/Boolean;

.field public use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

.field public use_customer_list_applet_in_android:Ljava/lang/Boolean;

.field public use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

.field public use_customer_list_in_android:Ljava/lang/Boolean;

.field public use_customer_list_in_android_mobile:Ljava/lang/Boolean;

.field public use_dining_options:Ljava/lang/Boolean;

.field public use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

.field public use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

.field public use_employee_timecards:Ljava/lang/Boolean;

.field public use_enforce_location_fix:Ljava/lang/Boolean;

.field public use_gift_cards_v2:Ljava/lang/Boolean;

.field public use_in_app_timecards:Ljava/lang/Boolean;

.field public use_inventory_plus:Ljava/lang/Boolean;

.field public use_invoice_applet_in_android:Ljava/lang/Boolean;

.field public use_magstripe_only_readers:Ljava/lang/Boolean;

.field public use_mobile_invoices:Ljava/lang/Boolean;

.field public use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

.field public use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

.field public use_open_tickets_android:Ljava/lang/Boolean;

.field public use_open_tickets_mobile_android:Ljava/lang/Boolean;

.field public use_paper_signature_android:Ljava/lang/Boolean;

.field public use_pre_tax_tipping_android:Ljava/lang/Boolean;

.field public use_predefined_tickets_android:Ljava/lang/Boolean;

.field public use_r12:Ljava/lang/Boolean;

.field public use_r12_pairing_v2:Ljava/lang/Boolean;

.field public use_r6:Ljava/lang/Boolean;

.field public use_reports_charts_android:Ljava/lang/Boolean;

.field public use_safetynet:Ljava/lang/Boolean;

.field public use_split_tender:Ljava/lang/Boolean;

.field public use_split_tickets_android:Ljava/lang/Boolean;

.field public use_v3_catalog:Ljava/lang/Boolean;

.field public use_void_comp_android:Ljava/lang/Boolean;

.field public use_zero_amount_tender:Ljava/lang/Boolean;

.field public uses_device_profile:Ljava/lang/Boolean;

.field public using_time_tracking:Ljava/lang/Boolean;

.field public zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 2949
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 2950
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->supported_card_brands_offline:Ljava/util/List;

    .line 2951
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->supported_card_brands_online:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public accept_third_party_gift_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 2958
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->accept_third_party_gift_cards:Ljava/lang/Boolean;

    return-object p0
.end method

.method public activation_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 2966
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->activation_url:Ljava/lang/String;

    return-object p0
.end method

.method public allow_payment_for_invoice_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 2971
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->allow_payment_for_invoice_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public appointments(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3733
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    return-object p0
.end method

.method public auth(Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3788
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    return-object p0
.end method

.method public auto_capture_payments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 2979
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->auto_capture_payments:Ljava/lang/Boolean;

    return-object p0
.end method

.method public bfd(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3683
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    return-object p0
.end method

.method public billing(Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3708
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    return-object p0
.end method

.method public bizbank(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3688
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions;
    .locals 2

    .line 3794
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;-><init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2638
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions;

    move-result-object v0

    return-object v0
.end method

.method public can_allow_swipe_for_chip_cards_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 2985
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_bulk_delete_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 2991
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_enable_tipping(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 2996
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_enable_tipping:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_onboard(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3001
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_onboard:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_publish_merchant_card(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3011
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_publish_merchant_card:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_refer_friend_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3016
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_refer_friend_in_app:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_skip_receipt_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3021
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_skip_receipt_screen:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_transfer_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 3030
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_transfer_open_tickets_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_android_ecr(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3035
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_use_android_ecr:Ljava/lang/Boolean;

    return-object p0
.end method

.method public capital(Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3778
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    return-object p0
.end method

.method public cardholder_name_on_dip_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 3041
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->cardholder_name_on_dip_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public cardreaders(Lcom/squareup/server/account/protos/Cardreaders;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3046
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    return-object p0
.end method

.method public catalog(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3603
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    return-object p0
.end method

.method public collect_tip_before_authorization_preferred(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3052
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

    return-object p0
.end method

.method public collect_tip_before_authorization_required(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3058
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->collect_tip_before_authorization_required:Ljava/lang/Boolean;

    return-object p0
.end method

.method public contact_support_phone_number(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3066
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->contact_support_phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public contact_support_twitter(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3071
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->contact_support_twitter:Ljava/lang/String;

    return-object p0
.end method

.method public country_prefers_contactless_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3076
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_contactless_cards:Ljava/lang/Boolean;

    return-object p0
.end method

.method public country_prefers_emv(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3087
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_emv:Ljava/lang/Boolean;

    return-object p0
.end method

.method public country_prefers_no_contactless_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3082
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_no_contactless_cards:Ljava/lang/Boolean;

    return-object p0
.end method

.method public crm(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3618
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    return-object p0
.end method

.method public deposits(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3638
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    return-object p0
.end method

.method public devplatmobile(Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3678
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    return-object p0
.end method

.method public disable_item_editing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3092
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_item_editing:Ljava/lang/Boolean;

    return-object p0
.end method

.method public disable_mobile_reports_applet_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3098
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_mobile_reports_applet_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public disable_register_api(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3120
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_register_api:Ljava/lang/Boolean;

    return-object p0
.end method

.method public discount(Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3738
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    return-object p0
.end method

.method public display_learn_more_r4(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3598
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->display_learn_more_r4:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ecom(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3768
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    return-object p0
.end method

.method public eligible_for_square_card_processing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3104
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    return-object p0
.end method

.method public eligible_for_third_party_card_processing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3110
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->eligible_for_third_party_card_processing:Ljava/lang/Boolean;

    return-object p0
.end method

.method public email_transaction_ledger(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3115
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->email_transaction_ledger:Ljava/lang/Boolean;

    return-object p0
.end method

.method public employee(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3663
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    return-object p0
.end method

.method public employee_cancel_transaction(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3417
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employee_cancel_transaction:Ljava/lang/Boolean;

    return-object p0
.end method

.method public employeejobs(Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3723
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    return-object p0
.end method

.method public fees(Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3693
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    return-object p0
.end method

.method public giftcard(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3643
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    return-object p0
.end method

.method public has_bazaar_online_store(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3593
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->has_bazaar_online_store:Ljava/lang/Boolean;

    return-object p0
.end method

.method public help_center_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3125
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->help_center_url:Ljava/lang/String;

    return-object p0
.end method

.method public hide_modifiers_on_receipts_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3131
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public invoice_tipping(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3136
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->invoice_tipping:Ljava/lang/Boolean;

    return-object p0
.end method

.method public invoices(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3633
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    return-object p0
.end method

.method public items(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3713
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    return-object p0
.end method

.method public items_batch_size(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3141
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->items_batch_size:Ljava/lang/Long;

    return-object p0
.end method

.method public login_type(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3149
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->login_type:Ljava/lang/String;

    return-object p0
.end method

.method public loyalty(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3608
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    return-object p0
.end method

.method public marketing(Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3783
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    return-object p0
.end method

.method public merge_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3154
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->merge_open_tickets_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public onboard(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3613
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    return-object p0
.end method

.method public opt_in_to_open_tickets_with_beta_ui_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3160
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public opt_in_to_store_and_forward_payments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3166
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

    return-object p0
.end method

.method public order_hub_sync_period_with_notifications(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3582
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->order_hub_sync_period_with_notifications:Ljava/lang/Long;

    return-object p0
.end method

.method public order_hub_sync_period_without_notifications(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3588
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->order_hub_sync_period_without_notifications:Ljava/lang/Long;

    return-object p0
.end method

.method public orderhub(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3758
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    return-object p0
.end method

.method public payment_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3171
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->payment_cards:Ljava/lang/Boolean;

    return-object p0
.end method

.method public paymentflow(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3648
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    return-object p0
.end method

.method public payments(Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3763
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    return-object p0
.end method

.method public placeholder(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3271
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->placeholder:Ljava/lang/Boolean;

    return-object p0
.end method

.method public pos(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3653
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    return-object p0
.end method

.method public posfeatures(Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3658
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    return-object p0
.end method

.method public prices(Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3753
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    return-object p0
.end method

.method public print_logo_on_paper_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3006
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->print_logo_on_paper_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public printers(Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3773
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    return-object p0
.end method

.method public privacy_policy_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3181
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->privacy_policy_url:Ljava/lang/String;

    return-object p0
.end method

.method public protect_edit_tax(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3176
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->protect_edit_tax:Ljava/lang/Boolean;

    return-object p0
.end method

.method public readerfw(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3698
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    return-object p0
.end method

.method public reduce_printing_waste_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3186
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->reduce_printing_waste_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public regex(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3623
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    return-object p0
.end method

.method public reports(Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3628
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    return-object p0
.end method

.method public require_secure_session_for_r6_swipe(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3192
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

    return-object p0
.end method

.method public requires_track_2_if_not_amex(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3197
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->requires_track_2_if_not_amex:Ljava/lang/Boolean;

    return-object p0
.end method

.method public restaurants(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3728
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    return-object p0
.end method

.method public retailer(Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3748
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    return-object p0
.end method

.method public rounding_type(Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3205
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    return-object p0
.end method

.method public see_advanced_modifiers(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3210
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_advanced_modifiers:Ljava/lang/Boolean;

    return-object p0
.end method

.method public see_payroll_register_feature_tour(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3215
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_payroll_register_feature_tour:Ljava/lang/Boolean;

    return-object p0
.end method

.method public see_payroll_register_learn_more_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3221
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_payroll_register_learn_more_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public seller_agreement_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3226
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->seller_agreement_url:Ljava/lang/String;

    return-object p0
.end method

.method public show_feature_tour(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3242
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_feature_tour:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_inclusive_taxes_in_cart(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3247
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_items_library_after_login(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3258
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_items_library_after_login:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_o1_jp_deprecation_warning_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3253
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

    return-object p0
.end method

.method public skip_modifier_detail_screen_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3232
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public skip_signatures_for_small_payments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3237
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->skip_signatures_for_small_payments:Ljava/lang/Boolean;

    return-object p0
.end method

.method public sms(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3263
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->sms:Ljava/lang/Boolean;

    return-object p0
.end method

.method public spoc(Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3743
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    return-object p0
.end method

.method public squaredevice(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3718
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    return-object p0
.end method

.method public support(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3703
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    return-object p0
.end method

.method public support_restaurant_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3276
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support_restaurant_items:Ljava/lang/Boolean;

    return-object p0
.end method

.method public support_retail_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3281
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support_retail_items:Ljava/lang/Boolean;

    return-object p0
.end method

.method public supported_card_brands_offline(Ljava/util/List;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;"
        }
    .end annotation

    .line 3286
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 3287
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->supported_card_brands_offline:Ljava/util/List;

    return-object p0
.end method

.method public supported_card_brands_online(Ljava/util/List;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;"
        }
    .end annotation

    .line 3292
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 3293
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->supported_card_brands_online:Ljava/util/List;

    return-object p0
.end method

.method public swipe_to_create_open_ticket_with_name(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3299
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    return-object p0
.end method

.method public terminal(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3668
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    return-object p0
.end method

.method public timecards(Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3673
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    return-object p0
.end method

.method public transfer_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3304
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->transfer_open_tickets_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public upload_support_ledger(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3309
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->upload_support_ledger:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_a10(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3314
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_a10:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_android_barcode_scanning(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3324
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_barcode_scanning:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_android_employee_management(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3329
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_employee_management:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_android_printer_stations(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3319
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_printer_stations:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_australian_gross_sales(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 3335
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_australian_gross_sales:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_card_associated_coupons(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3340
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_associated_coupons:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_card_on_file_in_android_register(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3346
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_on_file_in_android_register:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_card_on_file_in_android_register_on_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3352
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_conditional_taxes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3357
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_conditional_taxes:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_crm_custom_attributes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3362
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_custom_attributes:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_crm_messaging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3367
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_messaging:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_crm_notes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3372
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_notes:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_customer_directory_with_invoices(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3378
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_directory_with_invoices:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_customer_list_activity_list_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3384
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_customer_list_applet_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3390
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_applet_in_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_customer_list_applet_in_android_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3396
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_customer_list_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3401
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_in_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_customer_list_in_android_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3407
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_in_android_mobile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_dining_options(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3412
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_dining_options:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_employee_filtering_for_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3423
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_employee_filtering_for_paper_signature_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3429
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_employee_timecards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3434
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_timecards:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_enforce_location_fix(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3444
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_enforce_location_fix:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_gift_cards_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3449
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_gift_cards_v2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_in_app_timecards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3454
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_in_app_timecards:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_inventory_plus(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3459
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_inventory_plus:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_invoice_applet_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3464
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_invoice_applet_in_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_magstripe_only_readers(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3469
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_magstripe_only_readers:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_mobile_invoices(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3474
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_mobile_invoices_with_discounts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3479
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_mobile_invoices_with_modifiers(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3484
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3489
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_open_tickets_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_open_tickets_mobile_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3494
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_open_tickets_mobile_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_paper_signature_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3499
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_paper_signature_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_pre_tax_tipping_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 3505
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_predefined_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3510
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_predefined_tickets_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_r12(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3520
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r12:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_r12_pairing_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3525
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r12_pairing_v2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_r6(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3530
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r6:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_reports_charts_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3515
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_reports_charts_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_safetynet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3535
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_safetynet:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_split_tender(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3545
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_split_tender:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_split_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3540
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_split_tickets_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_v3_catalog(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3550
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_v3_catalog:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_void_comp_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3555
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_void_comp_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_zero_amount_tender(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3560
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_zero_amount_tender:Ljava/lang/Boolean;

    return-object p0
.end method

.method public uses_device_profile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3565
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->uses_device_profile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public using_time_tracking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3570
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->using_time_tracking:Ljava/lang/Boolean;

    return-object p0
.end method

.method public zero_amount_contactless_arqc_experiment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    .line 3576
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;

    return-object p0
.end method
