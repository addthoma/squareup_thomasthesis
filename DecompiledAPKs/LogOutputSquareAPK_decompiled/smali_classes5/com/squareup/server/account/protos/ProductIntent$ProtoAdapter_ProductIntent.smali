.class final Lcom/squareup/server/account/protos/ProductIntent$ProtoAdapter_ProductIntent;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ProductIntent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/ProductIntent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ProductIntent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/ProductIntent;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 123
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/ProductIntent;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/ProductIntent;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 140
    new-instance v0, Lcom/squareup/server/account/protos/ProductIntent$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/ProductIntent$Builder;-><init>()V

    .line 141
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 142
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 146
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 144
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/ProductIntent$Builder;->best_available(Ljava/lang/String;)Lcom/squareup/server/account/protos/ProductIntent$Builder;

    goto :goto_0

    .line 150
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/ProductIntent$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 151
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/ProductIntent$Builder;->build()Lcom/squareup/server/account/protos/ProductIntent;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 121
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/ProductIntent$ProtoAdapter_ProductIntent;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/ProductIntent;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/ProductIntent;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 134
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/ProductIntent;->best_available:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 135
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/ProductIntent;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 121
    check-cast p2, Lcom/squareup/server/account/protos/ProductIntent;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/ProductIntent$ProtoAdapter_ProductIntent;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/ProductIntent;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/ProductIntent;)I
    .locals 3

    .line 128
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/ProductIntent;->best_available:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 129
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ProductIntent;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 121
    check-cast p1, Lcom/squareup/server/account/protos/ProductIntent;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/ProductIntent$ProtoAdapter_ProductIntent;->encodedSize(Lcom/squareup/server/account/protos/ProductIntent;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/ProductIntent;)Lcom/squareup/server/account/protos/ProductIntent;
    .locals 0

    .line 156
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ProductIntent;->newBuilder()Lcom/squareup/server/account/protos/ProductIntent$Builder;

    move-result-object p1

    .line 157
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ProductIntent$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 158
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ProductIntent$Builder;->build()Lcom/squareup/server/account/protos/ProductIntent;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 121
    check-cast p1, Lcom/squareup/server/account/protos/ProductIntent;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/ProductIntent$ProtoAdapter_ProductIntent;->redact(Lcom/squareup/server/account/protos/ProductIntent;)Lcom/squareup/server/account/protos/ProductIntent;

    move-result-object p1

    return-object p1
.end method
