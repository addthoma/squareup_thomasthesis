.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Bizbank"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$ProtoAdapter_Bizbank;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BALANCE_APPLET_MASTER_SWITCH:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_MANAGE_SQUARE_CARD_MASTER_SWITCH:Ljava/lang/Boolean;

.field public static final DEFAULT_COLLECT_MOBILE_NUMBER_AFTER_CARD_ORDERING_FLOW:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_ACTIVE_CARD_FREEZE_TOGGLE:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_ADD_TO_WALLET:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_CARD_ACTIVATION_BILLING_ADDRESS:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_NOTIFICATION_PREFERENCES:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_SERVER_STAMPS_FOR_CARD_CUSTOMIZATION:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_SQUARE_CARD_PAN:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_SQUARE_CARD_UPSELL_IN_DEPOSITS:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_SQUARE_CARD_UPSELL_IN_INSTANT_DEPOSITS:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_UNIFIED_ACTIVITY:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_BALANCE_APPLET:Ljava/lang/Boolean;

.field public static final DEFAULT_VIEW_DASHBOARD_RESET_PIN:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final balance_applet_master_switch:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final can_manage_square_card_master_switch:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final collect_mobile_number_after_card_ordering_flow:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final show_active_card_freeze_toggle:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final show_add_to_wallet:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final show_card_activation_billing_address:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final show_notification_preferences:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xd
    .end annotation
.end field

.field public final show_server_stamps_for_card_customization:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final show_square_card_pan:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xe
    .end annotation
.end field

.field public final show_square_card_upsell_in_deposits:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final show_square_card_upsell_in_instant_deposits:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final show_unified_activity:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final use_balance_applet:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final view_dashboard_reset_pin:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13271
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$ProtoAdapter_Bizbank;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$ProtoAdapter_Bizbank;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 13273
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 13277
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_USE_BALANCE_APPLET:Ljava/lang/Boolean;

    .line 13279
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_BALANCE_APPLET_MASTER_SWITCH:Ljava/lang/Boolean;

    .line 13281
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_CAN_MANAGE_SQUARE_CARD_MASTER_SWITCH:Ljava/lang/Boolean;

    .line 13283
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_ACTIVE_CARD_FREEZE_TOGGLE:Ljava/lang/Boolean;

    .line 13285
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_CARD_ACTIVATION_BILLING_ADDRESS:Ljava/lang/Boolean;

    .line 13287
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_ADD_TO_WALLET:Ljava/lang/Boolean;

    .line 13289
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_SERVER_STAMPS_FOR_CARD_CUSTOMIZATION:Ljava/lang/Boolean;

    .line 13291
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_SQUARE_CARD_UPSELL_IN_DEPOSITS:Ljava/lang/Boolean;

    .line 13293
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_VIEW_DASHBOARD_RESET_PIN:Ljava/lang/Boolean;

    .line 13295
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_COLLECT_MOBILE_NUMBER_AFTER_CARD_ORDERING_FLOW:Ljava/lang/Boolean;

    .line 13297
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_SQUARE_CARD_UPSELL_IN_INSTANT_DEPOSITS:Ljava/lang/Boolean;

    .line 13299
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_UNIFIED_ACTIVITY:Ljava/lang/Boolean;

    .line 13301
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_NOTIFICATION_PREFERENCES:Ljava/lang/Boolean;

    .line 13303
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_SQUARE_CARD_PAN:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 16

    .line 13466
    sget-object v15, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 3

    move-object v0, p0

    .line 13482
    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p15

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 13483
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->use_balance_applet:Ljava/lang/Boolean;

    move-object v1, p2

    .line 13484
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->balance_applet_master_switch:Ljava/lang/Boolean;

    move-object v1, p3

    .line 13485
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->can_manage_square_card_master_switch:Ljava/lang/Boolean;

    move-object v1, p4

    .line 13486
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_active_card_freeze_toggle:Ljava/lang/Boolean;

    move-object v1, p5

    .line 13487
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_card_activation_billing_address:Ljava/lang/Boolean;

    move-object v1, p6

    .line 13488
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_add_to_wallet:Ljava/lang/Boolean;

    move-object v1, p7

    .line 13489
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_server_stamps_for_card_customization:Ljava/lang/Boolean;

    move-object v1, p8

    .line 13490
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_deposits:Ljava/lang/Boolean;

    move-object v1, p9

    .line 13491
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->view_dashboard_reset_pin:Ljava/lang/Boolean;

    move-object v1, p10

    .line 13492
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->collect_mobile_number_after_card_ordering_flow:Ljava/lang/Boolean;

    move-object v1, p11

    .line 13493
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_instant_deposits:Ljava/lang/Boolean;

    move-object v1, p12

    .line 13494
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_unified_activity:Ljava/lang/Boolean;

    move-object/from16 v1, p13

    .line 13495
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_notification_preferences:Ljava/lang/Boolean;

    move-object/from16 v1, p14

    .line 13496
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_pan:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 13627
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 13523
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 13524
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    .line 13525
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->use_balance_applet:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->use_balance_applet:Ljava/lang/Boolean;

    .line 13526
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->balance_applet_master_switch:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->balance_applet_master_switch:Ljava/lang/Boolean;

    .line 13527
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->can_manage_square_card_master_switch:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->can_manage_square_card_master_switch:Ljava/lang/Boolean;

    .line 13528
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_active_card_freeze_toggle:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_active_card_freeze_toggle:Ljava/lang/Boolean;

    .line 13529
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_card_activation_billing_address:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_card_activation_billing_address:Ljava/lang/Boolean;

    .line 13530
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_add_to_wallet:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_add_to_wallet:Ljava/lang/Boolean;

    .line 13531
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_server_stamps_for_card_customization:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_server_stamps_for_card_customization:Ljava/lang/Boolean;

    .line 13532
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_deposits:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_deposits:Ljava/lang/Boolean;

    .line 13533
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->view_dashboard_reset_pin:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->view_dashboard_reset_pin:Ljava/lang/Boolean;

    .line 13534
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->collect_mobile_number_after_card_ordering_flow:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->collect_mobile_number_after_card_ordering_flow:Ljava/lang/Boolean;

    .line 13535
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_instant_deposits:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_instant_deposits:Ljava/lang/Boolean;

    .line 13536
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_unified_activity:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_unified_activity:Ljava/lang/Boolean;

    .line 13537
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_notification_preferences:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_notification_preferences:Ljava/lang/Boolean;

    .line 13538
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_pan:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_pan:Ljava/lang/Boolean;

    .line 13539
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 13544
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_e

    .line 13546
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 13547
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->use_balance_applet:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13548
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->balance_applet_master_switch:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13549
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->can_manage_square_card_master_switch:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13550
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_active_card_freeze_toggle:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13551
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_card_activation_billing_address:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13552
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_add_to_wallet:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13553
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_server_stamps_for_card_customization:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13554
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_deposits:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13555
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->view_dashboard_reset_pin:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13556
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->collect_mobile_number_after_card_ordering_flow:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13557
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_instant_deposits:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13558
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_unified_activity:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13559
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_notification_preferences:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13560
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_pan:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_d
    add-int/2addr v0, v2

    .line 13561
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_e
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;
    .locals 2

    .line 13501
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;-><init>()V

    .line 13502
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->use_balance_applet:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->use_balance_applet:Ljava/lang/Boolean;

    .line 13503
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->balance_applet_master_switch:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->balance_applet_master_switch:Ljava/lang/Boolean;

    .line 13504
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->can_manage_square_card_master_switch:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->can_manage_square_card_master_switch:Ljava/lang/Boolean;

    .line 13505
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_active_card_freeze_toggle:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_active_card_freeze_toggle:Ljava/lang/Boolean;

    .line 13506
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_card_activation_billing_address:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_card_activation_billing_address:Ljava/lang/Boolean;

    .line 13507
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_add_to_wallet:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_add_to_wallet:Ljava/lang/Boolean;

    .line 13508
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_server_stamps_for_card_customization:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_server_stamps_for_card_customization:Ljava/lang/Boolean;

    .line 13509
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_deposits:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_square_card_upsell_in_deposits:Ljava/lang/Boolean;

    .line 13510
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->view_dashboard_reset_pin:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->view_dashboard_reset_pin:Ljava/lang/Boolean;

    .line 13511
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->collect_mobile_number_after_card_ordering_flow:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->collect_mobile_number_after_card_ordering_flow:Ljava/lang/Boolean;

    .line 13512
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_instant_deposits:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_square_card_upsell_in_instant_deposits:Ljava/lang/Boolean;

    .line 13513
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_unified_activity:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_unified_activity:Ljava/lang/Boolean;

    .line 13514
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_notification_preferences:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_notification_preferences:Ljava/lang/Boolean;

    .line 13515
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_pan:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_square_card_pan:Ljava/lang/Boolean;

    .line 13516
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 13270
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;
    .locals 2

    .line 13609
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->use_balance_applet:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->use_balance_applet:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->use_balance_applet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13610
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->balance_applet_master_switch:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->balance_applet_master_switch:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->balance_applet_master_switch(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13611
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->can_manage_square_card_master_switch:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->can_manage_square_card_master_switch:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->can_manage_square_card_master_switch(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13612
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_active_card_freeze_toggle:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_active_card_freeze_toggle:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_active_card_freeze_toggle(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13613
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_card_activation_billing_address:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_card_activation_billing_address:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_card_activation_billing_address(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13614
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_add_to_wallet:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_add_to_wallet:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_add_to_wallet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13615
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_server_stamps_for_card_customization:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_server_stamps_for_card_customization:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_server_stamps_for_card_customization(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13616
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_deposits:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_deposits:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_square_card_upsell_in_deposits(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13617
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->view_dashboard_reset_pin:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->view_dashboard_reset_pin:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->view_dashboard_reset_pin(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13618
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->collect_mobile_number_after_card_ordering_flow:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->collect_mobile_number_after_card_ordering_flow:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->collect_mobile_number_after_card_ordering_flow(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13619
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_instant_deposits:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_instant_deposits:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_square_card_upsell_in_instant_deposits(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13620
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_unified_activity:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_unified_activity:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_unified_activity(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13621
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_notification_preferences:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_notification_preferences:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_notification_preferences(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13622
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_pan:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_pan:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_square_card_pan(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    :cond_d
    if-nez v1, :cond_e

    move-object p1, p0

    goto :goto_0

    .line 13623
    :cond_e
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 13270
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;
    .locals 2

    .line 13589
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->use_balance_applet:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_USE_BALANCE_APPLET:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->use_balance_applet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13590
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->balance_applet_master_switch:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_BALANCE_APPLET_MASTER_SWITCH:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->balance_applet_master_switch(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13591
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->can_manage_square_card_master_switch:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_CAN_MANAGE_SQUARE_CARD_MASTER_SWITCH:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->can_manage_square_card_master_switch(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13592
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_active_card_freeze_toggle:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_ACTIVE_CARD_FREEZE_TOGGLE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_active_card_freeze_toggle(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13593
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_card_activation_billing_address:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_CARD_ACTIVATION_BILLING_ADDRESS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_card_activation_billing_address(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13594
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_add_to_wallet:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_ADD_TO_WALLET:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_add_to_wallet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13595
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_server_stamps_for_card_customization:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_SERVER_STAMPS_FOR_CARD_CUSTOMIZATION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_server_stamps_for_card_customization(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13596
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_deposits:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_SQUARE_CARD_UPSELL_IN_DEPOSITS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_square_card_upsell_in_deposits(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13597
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->view_dashboard_reset_pin:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_VIEW_DASHBOARD_RESET_PIN:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->view_dashboard_reset_pin(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13598
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->collect_mobile_number_after_card_ordering_flow:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_COLLECT_MOBILE_NUMBER_AFTER_CARD_ORDERING_FLOW:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->collect_mobile_number_after_card_ordering_flow(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13599
    :cond_9
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_instant_deposits:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_SQUARE_CARD_UPSELL_IN_INSTANT_DEPOSITS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_square_card_upsell_in_instant_deposits(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13600
    :cond_a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_unified_activity:Ljava/lang/Boolean;

    if-nez v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_UNIFIED_ACTIVITY:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_unified_activity(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13601
    :cond_b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_notification_preferences:Ljava/lang/Boolean;

    if-nez v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_NOTIFICATION_PREFERENCES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_notification_preferences(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    .line 13602
    :cond_c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_pan:Ljava/lang/Boolean;

    if-nez v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->DEFAULT_SHOW_SQUARE_CARD_PAN:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->show_square_card_pan(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;

    move-result-object v1

    :cond_d
    if-nez v1, :cond_e

    move-object v0, p0

    goto :goto_0

    .line 13603
    :cond_e
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 13270
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 13568
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 13569
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->use_balance_applet:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", use_balance_applet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->use_balance_applet:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13570
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->balance_applet_master_switch:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", balance_applet_master_switch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->balance_applet_master_switch:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13571
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->can_manage_square_card_master_switch:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", can_manage_square_card_master_switch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->can_manage_square_card_master_switch:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13572
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_active_card_freeze_toggle:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", show_active_card_freeze_toggle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_active_card_freeze_toggle:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13573
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_card_activation_billing_address:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", show_card_activation_billing_address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_card_activation_billing_address:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13574
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_add_to_wallet:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", show_add_to_wallet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_add_to_wallet:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13575
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_server_stamps_for_card_customization:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", show_server_stamps_for_card_customization="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_server_stamps_for_card_customization:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13576
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_deposits:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", show_square_card_upsell_in_deposits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_deposits:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13577
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->view_dashboard_reset_pin:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", view_dashboard_reset_pin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->view_dashboard_reset_pin:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13578
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->collect_mobile_number_after_card_ordering_flow:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", collect_mobile_number_after_card_ordering_flow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->collect_mobile_number_after_card_ordering_flow:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13579
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_instant_deposits:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", show_square_card_upsell_in_instant_deposits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_upsell_in_instant_deposits:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13580
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_unified_activity:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", show_unified_activity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_unified_activity:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13581
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_notification_preferences:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", show_notification_preferences="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_notification_preferences:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13582
    :cond_c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_pan:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", show_square_card_pan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->show_square_card_pan:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_d
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Bizbank{"

    .line 13583
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
