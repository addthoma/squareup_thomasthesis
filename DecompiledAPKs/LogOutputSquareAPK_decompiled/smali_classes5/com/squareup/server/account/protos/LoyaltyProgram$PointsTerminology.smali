.class public final Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;
.super Lcom/squareup/wire/AndroidMessage;
.source "LoyaltyProgram.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/LoyaltyProgram;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PointsTerminology"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$ProtoAdapter_PointsTerminology;,
        Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;",
        "Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ONE:Ljava/lang/String; = ""

.field public static final DEFAULT_OTHER:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final one:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final other:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 430
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$ProtoAdapter_PointsTerminology;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$ProtoAdapter_PointsTerminology;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 432
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 461
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 466
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 467
    iput-object p1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->one:Ljava/lang/String;

    .line 468
    iput-object p2, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->other:Ljava/lang/String;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 525
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->newBuilder()Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 483
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 484
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    .line 485
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->one:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->one:Ljava/lang/String;

    .line 486
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->other:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->other:Ljava/lang/String;

    .line 487
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 492
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_2

    .line 494
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 495
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->one:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 496
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->other:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 497
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;
    .locals 2

    .line 473
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;-><init>()V

    .line 474
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->one:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->one:Ljava/lang/String;

    .line 475
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->other:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->other:Ljava/lang/String;

    .line 476
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 429
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->newBuilder()Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;)Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;
    .locals 2

    .line 519
    iget-object v0, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->one:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->one:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->one(Ljava/lang/String;)Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;

    move-result-object v1

    .line 520
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->other:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->requireBuilder(Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;)Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->other:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->other(Ljava/lang/String;)Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object p1, p0

    goto :goto_0

    .line 521
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->build()Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 429
    check-cast p1, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->overlay(Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;)Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 429
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->populateDefaults()Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 504
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 505
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->one:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", one="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->one:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 506
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->other:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", other="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->other:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PointsTerminology{"

    .line 507
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
