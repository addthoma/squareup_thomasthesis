.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public can_display_support_messaging:Ljava/lang/Boolean;

.field public can_display_support_messaging_android_tablet:Ljava/lang/Boolean;

.field public hide_announcements_section:Ljava/lang/Boolean;

.field public streaming_ledger_upload:Ljava/lang/Boolean;

.field public upload_ledger_and_diagnostics:Ljava/lang/Boolean;

.field public use_jedi_workflow:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14507
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;
    .locals 9

    .line 14561
    new-instance v8, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->upload_ledger_and_diagnostics:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->streaming_ledger_upload:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->can_display_support_messaging:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->can_display_support_messaging_android_tablet:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->hide_announcements_section:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->use_jedi_workflow:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 14494
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    move-result-object v0

    return-object v0
.end method

.method public can_display_support_messaging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;
    .locals 0

    .line 14530
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->can_display_support_messaging:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_display_support_messaging_android_tablet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;
    .locals 0

    .line 14539
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->can_display_support_messaging_android_tablet:Ljava/lang/Boolean;

    return-object p0
.end method

.method public hide_announcements_section(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;
    .locals 0

    .line 14547
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->hide_announcements_section:Ljava/lang/Boolean;

    return-object p0
.end method

.method public streaming_ledger_upload(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;
    .locals 0

    .line 14522
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->streaming_ledger_upload:Ljava/lang/Boolean;

    return-object p0
.end method

.method public upload_ledger_and_diagnostics(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;
    .locals 0

    .line 14514
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->upload_ledger_and_diagnostics:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_jedi_workflow(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;
    .locals 0

    .line 14555
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->use_jedi_workflow:Ljava/lang/Boolean;

    return-object p0
.end method
