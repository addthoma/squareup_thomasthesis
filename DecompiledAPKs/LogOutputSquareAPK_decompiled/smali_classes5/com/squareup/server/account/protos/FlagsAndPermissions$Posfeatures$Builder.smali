.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public show_discard_on_error_on_jailscreen:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11706
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;
    .locals 3

    .line 11720
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;->show_discard_on_error_on_jailscreen:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 11703
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    move-result-object v0

    return-object v0
.end method

.method public show_discard_on_error_on_jailscreen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;
    .locals 0

    .line 11714
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures$Builder;->show_discard_on_error_on_jailscreen:Ljava/lang/Boolean;

    return-object p0
.end method
