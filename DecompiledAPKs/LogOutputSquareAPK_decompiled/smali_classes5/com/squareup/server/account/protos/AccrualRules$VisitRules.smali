.class public final Lcom/squareup/server/account/protos/AccrualRules$VisitRules;
.super Lcom/squareup/wire/AndroidMessage;
.source "AccrualRules.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/AccrualRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VisitRules"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/AccrualRules$VisitRules$ProtoAdapter_VisitRules;,
        Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/AccrualRules$VisitRules;",
        "Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/AccrualRules$VisitRules;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/AccrualRules$VisitRules;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/AccrualRules$VisitRules;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/AccrualRules$VisitRules;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_INCLUDE_TAX:Ljava/lang/Boolean;

.field public static final DEFAULT_POINTS:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final include_tax:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final minimum_spend:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final points:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 432
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules$ProtoAdapter_VisitRules;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules$ProtoAdapter_VisitRules;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 434
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->CREATOR:Landroid/os/Parcelable$Creator;

    const-wide/16 v0, 0x0

    .line 438
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->DEFAULT_POINTS:Ljava/lang/Long;

    const/4 v0, 0x0

    .line 440
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->DEFAULT_INCLUDE_TAX:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lcom/squareup/protos/common/Money;Ljava/lang/Boolean;)V
    .locals 1

    .line 475
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/Money;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lcom/squareup/protos/common/Money;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 480
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 481
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->points:Ljava/lang/Long;

    .line 482
    iput-object p2, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->minimum_spend:Lcom/squareup/protos/common/Money;

    .line 483
    iput-object p3, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->include_tax:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 547
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->newBuilder()Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 499
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 500
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    .line 501
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->points:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->points:Ljava/lang/Long;

    .line 502
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->minimum_spend:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->minimum_spend:Lcom/squareup/protos/common/Money;

    .line 503
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->include_tax:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->include_tax:Ljava/lang/Boolean;

    .line 504
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 509
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_3

    .line 511
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 512
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->points:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 513
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->minimum_spend:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 514
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->include_tax:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 515
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;
    .locals 2

    .line 488
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;-><init>()V

    .line 489
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->points:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;->points:Ljava/lang/Long;

    .line 490
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->minimum_spend:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;->minimum_spend:Lcom/squareup/protos/common/Money;

    .line 491
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->include_tax:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;->include_tax:Ljava/lang/Boolean;

    .line 492
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 431
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->newBuilder()Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/AccrualRules$VisitRules;)Lcom/squareup/server/account/protos/AccrualRules$VisitRules;
    .locals 2

    .line 540
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->points:Ljava/lang/Long;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->points:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;->points(Ljava/lang/Long;)Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;

    move-result-object v1

    .line 541
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->minimum_spend:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->minimum_spend:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;->minimum_spend(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;

    move-result-object v1

    .line 542
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->include_tax:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->include_tax:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;->include_tax(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    move-object p1, p0

    goto :goto_0

    .line 543
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 431
    check-cast p1, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->overlay(Lcom/squareup/server/account/protos/AccrualRules$VisitRules;)Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/AccrualRules$VisitRules;
    .locals 2

    .line 532
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->points:Ljava/lang/Long;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->DEFAULT_POINTS:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;->points(Ljava/lang/Long;)Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;

    move-result-object v1

    .line 533
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->include_tax:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->DEFAULT_INCLUDE_TAX:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;->include_tax(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object v0, p0

    goto :goto_0

    .line 534
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 431
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->populateDefaults()Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 522
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 523
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->points:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", points="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->points:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 524
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->minimum_spend:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", minimum_spend="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->minimum_spend:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 525
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->include_tax:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", include_tax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->include_tax:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "VisitRules{"

    .line 526
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
