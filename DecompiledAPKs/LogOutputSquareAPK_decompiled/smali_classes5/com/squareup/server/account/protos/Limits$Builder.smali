.class public final Lcom/squareup/server/account/protos/Limits$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Limits.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/Limits;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/Limits;",
        "Lcom/squareup/server/account/protos/Limits$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public emoney_transaction_min_cents:Ljava/lang/Long;

.field public gift_card_max_activation_amount_cents:Ljava/lang/Long;

.field public gift_card_max_total_purchase_amount_cents:Ljava/lang/Long;

.field public skip_receipt_max_cents:Ljava/lang/Long;

.field public suica_transaction_max_cents:Ljava/lang/Long;

.field public transaction_max_cents:Ljava/lang/Long;

.field public transaction_min_cents:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 236
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/Limits;
    .locals 10

    .line 287
    new-instance v9, Lcom/squareup/server/account/protos/Limits;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits$Builder;->transaction_min_cents:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/server/account/protos/Limits$Builder;->transaction_max_cents:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/server/account/protos/Limits$Builder;->skip_receipt_max_cents:Ljava/lang/Long;

    iget-object v4, p0, Lcom/squareup/server/account/protos/Limits$Builder;->gift_card_max_activation_amount_cents:Ljava/lang/Long;

    iget-object v5, p0, Lcom/squareup/server/account/protos/Limits$Builder;->gift_card_max_total_purchase_amount_cents:Ljava/lang/Long;

    iget-object v6, p0, Lcom/squareup/server/account/protos/Limits$Builder;->emoney_transaction_min_cents:Ljava/lang/Long;

    iget-object v7, p0, Lcom/squareup/server/account/protos/Limits$Builder;->suica_transaction_max_cents:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/account/protos/Limits;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 221
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Limits$Builder;->build()Lcom/squareup/server/account/protos/Limits;

    move-result-object v0

    return-object v0
.end method

.method public emoney_transaction_min_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;
    .locals 0

    .line 276
    iput-object p1, p0, Lcom/squareup/server/account/protos/Limits$Builder;->emoney_transaction_min_cents:Ljava/lang/Long;

    return-object p0
.end method

.method public gift_card_max_activation_amount_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;
    .locals 0

    .line 265
    iput-object p1, p0, Lcom/squareup/server/account/protos/Limits$Builder;->gift_card_max_activation_amount_cents:Ljava/lang/Long;

    return-object p0
.end method

.method public gift_card_max_total_purchase_amount_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/squareup/server/account/protos/Limits$Builder;->gift_card_max_total_purchase_amount_cents:Ljava/lang/Long;

    return-object p0
.end method

.method public skip_receipt_max_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;
    .locals 0

    .line 259
    iput-object p1, p0, Lcom/squareup/server/account/protos/Limits$Builder;->skip_receipt_max_cents:Ljava/lang/Long;

    return-object p0
.end method

.method public suica_transaction_max_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;
    .locals 0

    .line 281
    iput-object p1, p0, Lcom/squareup/server/account/protos/Limits$Builder;->suica_transaction_max_cents:Ljava/lang/Long;

    return-object p0
.end method

.method public transaction_max_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;
    .locals 0

    .line 251
    iput-object p1, p0, Lcom/squareup/server/account/protos/Limits$Builder;->transaction_max_cents:Ljava/lang/Long;

    return-object p0
.end method

.method public transaction_min_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/server/account/protos/Limits$Builder;->transaction_min_cents:Ljava/lang/Long;

    return-object p0
.end method
