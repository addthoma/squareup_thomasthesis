.class public final Lcom/squareup/server/account/protos/ProcessingFee$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ProcessingFee.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/ProcessingFee;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/ProcessingFee;",
        "Lcom/squareup/server/account/protos/ProcessingFee$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public discount_basis_points:Ljava/lang/Integer;

.field public interchange_cents:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 129
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/ProcessingFee;
    .locals 4

    .line 144
    new-instance v0, Lcom/squareup/server/account/protos/ProcessingFee;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->discount_basis_points:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->interchange_cents:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/ProcessingFee;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 124
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->build()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v0

    return-object v0
.end method

.method public discount_basis_points(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/ProcessingFee$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->discount_basis_points:Ljava/lang/Integer;

    return-object p0
.end method

.method public interchange_cents(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/ProcessingFee$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->interchange_cents:Ljava/lang/Integer;

    return-object p0
.end method
