.class public final Lcom/squareup/server/account/protos/InstantDeposits;
.super Lcom/squareup/wire/AndroidMessage;
.source "InstantDeposits.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/InstantDeposits$ProtoAdapter_InstantDeposits;,
        Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;,
        Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;,
        Lcom/squareup/server/account/protos/InstantDeposits$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/InstantDeposits;",
        "Lcom/squareup/server/account/protos/InstantDeposits$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/InstantDeposits;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/InstantDeposits;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/InstantDeposits;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/InstantDeposits;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.InstantDeposits$FeeStructure#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.InstantDeposits$LinkedCard#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.InstantDeposits$FeeStructure#ADAPTER"
        tag = 0x7
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$ProtoAdapter_InstantDeposits;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/InstantDeposits$ProtoAdapter_InstantDeposits;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 35
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;)V
    .locals 1

    .line 68
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/server/account/protos/InstantDeposits;-><init>(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;Lokio/ByteString;)V
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 75
    iput-object p1, p0, Lcom/squareup/server/account/protos/InstantDeposits;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    .line 76
    iput-object p2, p0, Lcom/squareup/server/account/protos/InstantDeposits;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    .line 77
    iput-object p3, p0, Lcom/squareup/server/account/protos/InstantDeposits;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 143
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits;->newBuilder()Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 93
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/InstantDeposits;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 94
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/InstantDeposits;

    .line 95
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/InstantDeposits;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    iget-object v3, p1, Lcom/squareup/server/account/protos/InstantDeposits;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    .line 96
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    iget-object v3, p1, Lcom/squareup/server/account/protos/InstantDeposits;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    .line 97
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    iget-object p1, p1, Lcom/squareup/server/account/protos/InstantDeposits;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    .line 98
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 103
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_3

    .line 105
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 106
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 107
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 109
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/InstantDeposits$Builder;
    .locals 2

    .line 82
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;-><init>()V

    .line 83
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    iput-object v1, v0, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    .line 84
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    iput-object v1, v0, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    .line 85
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    iput-object v1, v0, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    .line 86
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits;->newBuilder()Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/InstantDeposits;)Lcom/squareup/server/account/protos/InstantDeposits;
    .locals 2

    .line 136
    iget-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/InstantDeposits;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->linked_card(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    move-result-object v1

    .line 137
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/InstantDeposits;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->instant_deposit_fee(Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    move-result-object v1

    .line 138
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/InstantDeposits;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->same_day_deposit_fee(Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    move-object p1, p0

    goto :goto_0

    .line 139
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/server/account/protos/InstantDeposits;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/InstantDeposits;->overlay(Lcom/squareup/server/account/protos/InstantDeposits;)Lcom/squareup/server/account/protos/InstantDeposits;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/InstantDeposits;
    .locals 3

    .line 126
    iget-object v0, p0, Lcom/squareup/server/account/protos/InstantDeposits;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->populateDefaults()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    move-result-object v0

    .line 128
    iget-object v2, p0, Lcom/squareup/server/account/protos/InstantDeposits;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    if-eq v0, v2, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->linked_card(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object v0, p0

    goto :goto_0

    .line 130
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits;->populateDefaults()Lcom/squareup/server/account/protos/InstantDeposits;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 117
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    if-eqz v1, :cond_0

    const-string v1, ", linked_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    if-eqz v1, :cond_1

    const-string v1, ", instant_deposit_fee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 119
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    if-eqz v1, :cond_2

    const-string v1, ", same_day_deposit_fee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InstantDeposits{"

    .line 120
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
