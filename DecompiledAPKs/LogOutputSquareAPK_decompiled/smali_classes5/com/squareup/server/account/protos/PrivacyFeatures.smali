.class public final Lcom/squareup/server/account/protos/PrivacyFeatures;
.super Lcom/squareup/wire/AndroidMessage;
.source "PrivacyFeatures.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/PrivacyFeatures$ProtoAdapter_PrivacyFeatures;,
        Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/PrivacyFeatures;",
        "Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/PrivacyFeatures;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/PrivacyFeatures;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/PrivacyFeatures;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/PrivacyFeatures;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_OPTED_OUT_OF_TRACKING:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final opted_out_of_tracking:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/server/account/protos/PrivacyFeatures$ProtoAdapter_PrivacyFeatures;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/PrivacyFeatures$ProtoAdapter_PrivacyFeatures;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/PrivacyFeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 31
    sget-object v0, Lcom/squareup/server/account/protos/PrivacyFeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/PrivacyFeatures;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 35
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/PrivacyFeatures;->DEFAULT_OPTED_OUT_OF_TRACKING:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 1

    .line 45
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/server/account/protos/PrivacyFeatures;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 49
    sget-object v0, Lcom/squareup/server/account/protos/PrivacyFeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 50
    iput-object p1, p0, Lcom/squareup/server/account/protos/PrivacyFeatures;->opted_out_of_tracking:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;)Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/PrivacyFeatures;->newBuilder()Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 64
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/PrivacyFeatures;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 65
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/PrivacyFeatures;

    .line 66
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/PrivacyFeatures;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/PrivacyFeatures;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/PrivacyFeatures;->opted_out_of_tracking:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/PrivacyFeatures;->opted_out_of_tracking:Ljava/lang/Boolean;

    .line 67
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 72
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_1

    .line 74
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/PrivacyFeatures;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 75
    iget-object v1, p0, Lcom/squareup/server/account/protos/PrivacyFeatures;->opted_out_of_tracking:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 76
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;
    .locals 2

    .line 55
    new-instance v0, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;-><init>()V

    .line 56
    iget-object v1, p0, Lcom/squareup/server/account/protos/PrivacyFeatures;->opted_out_of_tracking:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;->opted_out_of_tracking:Ljava/lang/Boolean;

    .line 57
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/PrivacyFeatures;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/PrivacyFeatures;->newBuilder()Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/PrivacyFeatures;)Lcom/squareup/server/account/protos/PrivacyFeatures;
    .locals 2

    .line 98
    iget-object v0, p1, Lcom/squareup/server/account/protos/PrivacyFeatures;->opted_out_of_tracking:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PrivacyFeatures;->requireBuilder(Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;)Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/PrivacyFeatures;->opted_out_of_tracking:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;->opted_out_of_tracking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object p1, p0

    goto :goto_0

    .line 99
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;->build()Lcom/squareup/server/account/protos/PrivacyFeatures;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/server/account/protos/PrivacyFeatures;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/PrivacyFeatures;->overlay(Lcom/squareup/server/account/protos/PrivacyFeatures;)Lcom/squareup/server/account/protos/PrivacyFeatures;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/PrivacyFeatures;
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/server/account/protos/PrivacyFeatures;->opted_out_of_tracking:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/PrivacyFeatures;->requireBuilder(Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;)Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/PrivacyFeatures;->DEFAULT_OPTED_OUT_OF_TRACKING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;->opted_out_of_tracking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object v0, p0

    goto :goto_0

    .line 92
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;->build()Lcom/squareup/server/account/protos/PrivacyFeatures;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/PrivacyFeatures;->populateDefaults()Lcom/squareup/server/account/protos/PrivacyFeatures;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 84
    iget-object v1, p0, Lcom/squareup/server/account/protos/PrivacyFeatures;->opted_out_of_tracking:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", opted_out_of_tracking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PrivacyFeatures;->opted_out_of_tracking:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PrivacyFeatures{"

    .line 85
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
