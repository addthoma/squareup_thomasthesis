.class public final Lcom/squareup/server/account/protos/User;
.super Lcom/squareup/wire/AndroidMessage;
.source "User.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/User$ProtoAdapter_User;,
        Lcom/squareup/server/account/protos/User$MerchantKey;,
        Lcom/squareup/server/account/protos/User$MerchantProfile;,
        Lcom/squareup/server/account/protos/User$ReceiptAddress;,
        Lcom/squareup/server/account/protos/User$SquareLocale;,
        Lcom/squareup/server/account/protos/User$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/User;",
        "Lcom/squareup/server/account/protos/User$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/User;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/User;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/User;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/User;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BUSINESS_ABN:Ljava/lang/String; = ""

.field public static final DEFAULT_EMAIL:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_IMPERSONATING:Ljava/lang/Boolean;

.field public static final DEFAULT_MCC:Ljava/lang/Integer;

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PHONE:Ljava/lang/String; = ""

.field public static final DEFAULT_PROFILE_IMAGE_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_SUBUNIT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_TWITTER:Ljava/lang/String; = ""

.field public static final DEFAULT_WEBSITE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final business_abn:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation
.end field

.field public final email:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final impersonating:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x10
    .end annotation
.end field

.field public final locale:Lcom/squareup/server/account/protos/User$SquareLocale;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.User$SquareLocale#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final mcc:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x8
    .end annotation
.end field

.field public final merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.User$MerchantKey#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.User$MerchantProfile#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x11
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final phone:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x5
    .end annotation
.end field

.field public final profile_image_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xe
    .end annotation
.end field

.field public final receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.User$ReceiptAddress#ADAPTER"
        redacted = true
        tag = 0xa
    .end annotation
.end field

.field public final subunit_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xf
    .end annotation
.end field

.field public final twitter:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final website:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 33
    new-instance v0, Lcom/squareup/server/account/protos/User$ProtoAdapter_User;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/User$ProtoAdapter_User;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/User;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 35
    sget-object v0, Lcom/squareup/server/account/protos/User;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/User;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 53
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/server/account/protos/User;->DEFAULT_MCC:Ljava/lang/Integer;

    .line 61
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/User;->DEFAULT_IMPERSONATING:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/User$Builder;Lokio/ByteString;)V
    .locals 1

    .line 197
    sget-object v0, Lcom/squareup/server/account/protos/User;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 198
    iget-object p2, p1, Lcom/squareup/server/account/protos/User$Builder;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/User;->id:Ljava/lang/String;

    .line 199
    iget-object p2, p1, Lcom/squareup/server/account/protos/User$Builder;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/User;->name:Ljava/lang/String;

    .line 200
    iget-object p2, p1, Lcom/squareup/server/account/protos/User$Builder;->email:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/User;->email:Ljava/lang/String;

    .line 201
    iget-object p2, p1, Lcom/squareup/server/account/protos/User$Builder;->twitter:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/User;->twitter:Ljava/lang/String;

    .line 202
    iget-object p2, p1, Lcom/squareup/server/account/protos/User$Builder;->phone:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/User;->phone:Ljava/lang/String;

    .line 203
    iget-object p2, p1, Lcom/squareup/server/account/protos/User$Builder;->website:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/User;->website:Ljava/lang/String;

    .line 204
    iget-object p2, p1, Lcom/squareup/server/account/protos/User$Builder;->subunit_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/User;->subunit_name:Ljava/lang/String;

    .line 205
    iget-object p2, p1, Lcom/squareup/server/account/protos/User$Builder;->mcc:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/server/account/protos/User;->mcc:Ljava/lang/Integer;

    .line 206
    iget-object p2, p1, Lcom/squareup/server/account/protos/User$Builder;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    iput-object p2, p0, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    .line 207
    iget-object p2, p1, Lcom/squareup/server/account/protos/User$Builder;->receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

    iput-object p2, p0, Lcom/squareup/server/account/protos/User;->receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

    .line 208
    iget-object p2, p1, Lcom/squareup/server/account/protos/User$Builder;->merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

    iput-object p2, p0, Lcom/squareup/server/account/protos/User;->merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

    .line 209
    iget-object p2, p1, Lcom/squareup/server/account/protos/User$Builder;->business_abn:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/User;->business_abn:Ljava/lang/String;

    .line 210
    iget-object p2, p1, Lcom/squareup/server/account/protos/User$Builder;->merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;

    iput-object p2, p0, Lcom/squareup/server/account/protos/User;->merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;

    .line 211
    iget-object p2, p1, Lcom/squareup/server/account/protos/User$Builder;->profile_image_url:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/User;->profile_image_url:Ljava/lang/String;

    .line 212
    iget-object p2, p1, Lcom/squareup/server/account/protos/User$Builder;->token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/User;->token:Ljava/lang/String;

    .line 213
    iget-object p2, p1, Lcom/squareup/server/account/protos/User$Builder;->impersonating:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/User;->impersonating:Ljava/lang/Boolean;

    .line 214
    iget-object p1, p1, Lcom/squareup/server/account/protos/User$Builder;->merchant_token:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/server/account/protos/User;->merchant_token:Ljava/lang/String;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 364
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User;->newBuilder()Lcom/squareup/server/account/protos/User$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 244
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/User;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 245
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/User;

    .line 246
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->id:Ljava/lang/String;

    .line 247
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->name:Ljava/lang/String;

    .line 248
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->email:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->email:Ljava/lang/String;

    .line 249
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->twitter:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->twitter:Ljava/lang/String;

    .line 250
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->phone:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->phone:Ljava/lang/String;

    .line 251
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->website:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->website:Ljava/lang/String;

    .line 252
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->subunit_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->subunit_name:Ljava/lang/String;

    .line 253
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->mcc:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->mcc:Ljava/lang/Integer;

    .line 254
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    .line 255
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

    .line 256
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

    .line 257
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->business_abn:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->business_abn:Ljava/lang/String;

    .line 258
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;

    .line 259
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->profile_image_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->profile_image_url:Ljava/lang/String;

    .line 260
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->token:Ljava/lang/String;

    .line 261
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->impersonating:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->impersonating:Ljava/lang/Boolean;

    .line 262
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->merchant_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/account/protos/User;->merchant_token:Ljava/lang/String;

    .line 263
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 268
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_11

    .line 270
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 271
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 272
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 273
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->email:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 274
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->twitter:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 275
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->phone:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 276
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->website:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 277
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->subunit_name:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 278
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->mcc:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 279
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/User$SquareLocale;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 280
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 281
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/User$MerchantProfile;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 282
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->business_abn:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 283
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/User$MerchantKey;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 284
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->profile_image_url:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 285
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->token:Ljava/lang/String;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 286
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->impersonating:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 287
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_10
    add-int/2addr v0, v2

    .line 288
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_11
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/User$Builder;
    .locals 2

    .line 219
    new-instance v0, Lcom/squareup/server/account/protos/User$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/User$Builder;-><init>()V

    .line 220
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->id:Ljava/lang/String;

    .line 221
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->name:Ljava/lang/String;

    .line 222
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->email:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->email:Ljava/lang/String;

    .line 223
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->twitter:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->twitter:Ljava/lang/String;

    .line 224
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->phone:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->phone:Ljava/lang/String;

    .line 225
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->website:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->website:Ljava/lang/String;

    .line 226
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->subunit_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->subunit_name:Ljava/lang/String;

    .line 227
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->mcc:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->mcc:Ljava/lang/Integer;

    .line 228
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    .line 229
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

    .line 230
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

    .line 231
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->business_abn:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->business_abn:Ljava/lang/String;

    .line 232
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;

    .line 233
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->profile_image_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->profile_image_url:Ljava/lang/String;

    .line 234
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->token:Ljava/lang/String;

    .line 235
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->impersonating:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->impersonating:Ljava/lang/Boolean;

    .line 236
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$Builder;->merchant_token:Ljava/lang/String;

    .line 237
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User;->newBuilder()Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/User;)Lcom/squareup/server/account/protos/User;
    .locals 2

    .line 343
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->id:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->id(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 344
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->name(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 345
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->email:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User;->email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->email(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 346
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->twitter:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User;->twitter:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->twitter(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 347
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->phone:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User;->phone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->phone(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 348
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->website:Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User;->website:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->website(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 349
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->subunit_name:Ljava/lang/String;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User;->subunit_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->subunit_name(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 350
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->mcc:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User;->mcc:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->mcc(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 351
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->locale(Lcom/squareup/server/account/protos/User$SquareLocale;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 352
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User;->receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->receipt_address(Lcom/squareup/server/account/protos/User$ReceiptAddress;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 353
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User;->merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->merchant_profile(Lcom/squareup/server/account/protos/User$MerchantProfile;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 354
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->business_abn:Ljava/lang/String;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User;->business_abn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->business_abn(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 355
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User;->merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->merchant_key(Lcom/squareup/server/account/protos/User$MerchantKey;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 356
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->profile_image_url:Ljava/lang/String;

    if-eqz v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User;->profile_image_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->profile_image_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 357
    :cond_d
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->token:Ljava/lang/String;

    if-eqz v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->token(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 358
    :cond_e
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->impersonating:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User;->impersonating:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->impersonating(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 359
    :cond_f
    iget-object v0, p1, Lcom/squareup/server/account/protos/User;->merchant_token:Ljava/lang/String;

    if-eqz v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/User;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/User$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    :cond_10
    if-nez v1, :cond_11

    move-object p1, p0

    goto :goto_0

    .line 360
    :cond_11
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/User$Builder;->build()Lcom/squareup/server/account/protos/User;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/server/account/protos/User;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User;->overlay(Lcom/squareup/server/account/protos/User;)Lcom/squareup/server/account/protos/User;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/User;
    .locals 3

    .line 319
    iget-object v0, p0, Lcom/squareup/server/account/protos/User;->mcc:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/User;->DEFAULT_MCC:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->mcc(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    if-eqz v0, :cond_1

    .line 321
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/User$SquareLocale;->populateDefaults()Lcom/squareup/server/account/protos/User$SquareLocale;

    move-result-object v0

    .line 322
    iget-object v2, p0, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    if-eq v0, v2, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/User$Builder;->locale(Lcom/squareup/server/account/protos/User$SquareLocale;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 324
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/User;->receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

    if-eqz v0, :cond_2

    .line 325
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->populateDefaults()Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object v0

    .line 326
    iget-object v2, p0, Lcom/squareup/server/account/protos/User;->receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

    if-eq v0, v2, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/User$Builder;->receipt_address(Lcom/squareup/server/account/protos/User$ReceiptAddress;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 328
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/User;->merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

    if-eqz v0, :cond_3

    .line 329
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/User$MerchantProfile;->populateDefaults()Lcom/squareup/server/account/protos/User$MerchantProfile;

    move-result-object v0

    .line 330
    iget-object v2, p0, Lcom/squareup/server/account/protos/User;->merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

    if-eq v0, v2, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/User$Builder;->merchant_profile(Lcom/squareup/server/account/protos/User$MerchantProfile;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 332
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/User;->merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;

    if-eqz v0, :cond_4

    .line 333
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/User$MerchantKey;->populateDefaults()Lcom/squareup/server/account/protos/User$MerchantKey;

    move-result-object v0

    .line 334
    iget-object v2, p0, Lcom/squareup/server/account/protos/User;->merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;

    if-eq v0, v2, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/User$Builder;->merchant_key(Lcom/squareup/server/account/protos/User$MerchantKey;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 336
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/User;->impersonating:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User;->requireBuilder(Lcom/squareup/server/account/protos/User$Builder;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/User;->DEFAULT_IMPERSONATING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$Builder;->impersonating(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    :cond_5
    if-nez v1, :cond_6

    move-object v0, p0

    goto :goto_0

    .line 337
    :cond_6
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/User$Builder;->build()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User;->populateDefaults()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 295
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 296
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->email:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", email=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->twitter:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", twitter=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->phone:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", phone=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->website:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", website=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->subunit_name:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", subunit_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->subunit_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->mcc:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    const-string v1, ", mcc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->mcc:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 304
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    if-eqz v1, :cond_8

    const-string v1, ", locale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 305
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->receipt_address:Lcom/squareup/server/account/protos/User$ReceiptAddress;

    if-eqz v1, :cond_9

    const-string v1, ", receipt_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

    if-eqz v1, :cond_a

    const-string v1, ", merchant_profile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->merchant_profile:Lcom/squareup/server/account/protos/User$MerchantProfile;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 307
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->business_abn:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", business_abn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->business_abn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;

    if-eqz v1, :cond_c

    const-string v1, ", merchant_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->merchant_key:Lcom/squareup/server/account/protos/User$MerchantKey;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 309
    :cond_c
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->profile_image_url:Ljava/lang/String;

    if-eqz v1, :cond_d

    const-string v1, ", profile_image_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->profile_image_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    :cond_d
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->token:Ljava/lang/String;

    if-eqz v1, :cond_e

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    :cond_e
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->impersonating:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    const-string v1, ", impersonating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->impersonating:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 312
    :cond_f
    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_10

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_10
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "User{"

    .line 313
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
