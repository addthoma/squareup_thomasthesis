.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PaymentFlow"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$ProtoAdapter_PaymentFlow;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALLOW_CNP_SIGNATURES:Ljava/lang/Boolean;

.field public static final DEFAULT_ALLOW_COF_SIGNATURES:Ljava/lang/Boolean;

.field public static final DEFAULT_ALWAYS_SHOW_ITEMIZED_CART:Ljava/lang/Boolean;

.field public static final DEFAULT_ANDROID_USE_PAYMENT_CONFIG:Ljava/lang/Boolean;

.field public static final DEFAULT_BUYER_CHECKOUT:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_ALWAYS_SKIP_SIGNATURES:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_SPLIT_EMONEY:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_BUYER_LANGUAGE_SELECTION:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_CASH_QR_CODES:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_CASH_WORKFLOW_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_CTO_WORKFLOW_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_INSTALLMENTS:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_JP_FORMAL_PRINTED_RECEIPTS:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_PAYMENT_METHOD_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_SELECT_METHOD_WORKFLOW_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_SELLER_CASH_RECEIVED_WORKFLOW_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_SEPARATE_LOCAL_PAYMENTS_QUEUE_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_SPLIT_TENDER_WORKFLOW_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_TENDER_IN_EDIT_REFACTOR:Ljava/lang/Boolean;

.field public static final DEFAULT_DO_NOT_USER_TASK_QUEUE_FOR_PAYMENTS_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_EMONEY:Ljava/lang/Boolean;

.field public static final DEFAULT_EVEN_SPLITS_ANDROID_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_ORDERS_INTEGRATION_PROCESS_CNP_VIA_ORDERS:Ljava/lang/Boolean;

.field public static final DEFAULT_PORTRAIT_SIGNATURE:Ljava/lang/Boolean;

.field public static final DEFAULT_PREFER_SQLITE_TASKS_QUEUE:Ljava/lang/Boolean;

.field public static final DEFAULT_PRINT_TAX_PERCENTAGE:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_ACCIDENTAL_CASH_OTHER_MODAL:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_NOHO_RECEIPT_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_AUTH_WORKFLOW_ANDROID:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final allow_cnp_signatures:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final allow_cof_signatures:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final always_show_itemized_cart:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x17
    .end annotation
.end field

.field public final android_use_payment_config:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1b
    .end annotation
.end field

.field public final buyer_checkout:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final can_always_skip_signatures:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final can_split_emoney:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1a
    .end annotation
.end field

.field public final can_use_buyer_language_selection:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final can_use_cash_qr_codes:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x19
    .end annotation
.end field

.field public final can_use_cash_workflow_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x18
    .end annotation
.end field

.field public final can_use_cto_workflow_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x14
    .end annotation
.end field

.field public final can_use_installments:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x11
    .end annotation
.end field

.field public final can_use_jp_formal_printed_receipts:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final can_use_payment_method_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final can_use_select_method_workflow_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1d
    .end annotation
.end field

.field public final can_use_seller_cash_received_workflow_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x12
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final can_use_separate_local_payments_queue_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x15
    .end annotation
.end field

.field public final can_use_split_tender_workflow_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x13
    .end annotation
.end field

.field public final can_use_tender_in_edit_refactor:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final do_not_user_task_queue_for_payments_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x16
    .end annotation
.end field

.field public final emoney:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xd
    .end annotation
.end field

.field public final even_splits_android_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final orders_integration_process_cnp_via_orders:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1c
    .end annotation
.end field

.field public final portrait_signature:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final prefer_sqlite_tasks_queue:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xe
    .end annotation
.end field

.field public final print_tax_percentage:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final show_accidental_cash_other_modal:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final show_noho_receipt_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xf
    .end annotation
.end field

.field public final use_auth_workflow_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x10
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10165
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$ProtoAdapter_PaymentFlow;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$ProtoAdapter_PaymentFlow;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 10167
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x1

    .line 10171
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_ALLOW_CNP_SIGNATURES:Ljava/lang/Boolean;

    .line 10173
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_ALLOW_COF_SIGNATURES:Ljava/lang/Boolean;

    const/4 v0, 0x0

    .line 10175
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_PAYMENT_METHOD_V2:Ljava/lang/Boolean;

    .line 10177
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_JP_FORMAL_PRINTED_RECEIPTS:Ljava/lang/Boolean;

    .line 10179
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_PRINT_TAX_PERCENTAGE:Ljava/lang/Boolean;

    .line 10181
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_PORTRAIT_SIGNATURE:Ljava/lang/Boolean;

    .line 10183
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_BUYER_CHECKOUT:Ljava/lang/Boolean;

    .line 10185
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_EVEN_SPLITS_ANDROID_V2:Ljava/lang/Boolean;

    .line 10187
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_BUYER_LANGUAGE_SELECTION:Ljava/lang/Boolean;

    .line 10189
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_ALWAYS_SKIP_SIGNATURES:Ljava/lang/Boolean;

    .line 10191
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_TENDER_IN_EDIT_REFACTOR:Ljava/lang/Boolean;

    .line 10193
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_SHOW_ACCIDENTAL_CASH_OTHER_MODAL:Ljava/lang/Boolean;

    .line 10195
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_EMONEY:Ljava/lang/Boolean;

    .line 10197
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_PREFER_SQLITE_TASKS_QUEUE:Ljava/lang/Boolean;

    .line 10199
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_SHOW_NOHO_RECEIPT_ANDROID:Ljava/lang/Boolean;

    .line 10201
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_USE_AUTH_WORKFLOW_ANDROID:Ljava/lang/Boolean;

    .line 10203
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_INSTALLMENTS:Ljava/lang/Boolean;

    .line 10205
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_SELLER_CASH_RECEIVED_WORKFLOW_ANDROID:Ljava/lang/Boolean;

    .line 10207
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_SPLIT_TENDER_WORKFLOW_ANDROID:Ljava/lang/Boolean;

    .line 10209
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_CTO_WORKFLOW_ANDROID:Ljava/lang/Boolean;

    .line 10211
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_SEPARATE_LOCAL_PAYMENTS_QUEUE_ANDROID:Ljava/lang/Boolean;

    .line 10213
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_DO_NOT_USER_TASK_QUEUE_FOR_PAYMENTS_ANDROID:Ljava/lang/Boolean;

    .line 10215
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_ALWAYS_SHOW_ITEMIZED_CART:Ljava/lang/Boolean;

    .line 10217
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_CASH_WORKFLOW_ANDROID:Ljava/lang/Boolean;

    .line 10219
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_CASH_QR_CODES:Ljava/lang/Boolean;

    .line 10221
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_SPLIT_EMONEY:Ljava/lang/Boolean;

    .line 10223
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_ANDROID_USE_PAYMENT_CONFIG:Ljava/lang/Boolean;

    .line 10225
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_ORDERS_INTEGRATION_PROCESS_CNP_VIA_ORDERS:Ljava/lang/Boolean;

    .line 10227
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_SELECT_METHOD_WORKFLOW_ANDROID:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;Lokio/ByteString;)V
    .locals 1

    .line 10436
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 10437
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->allow_cnp_signatures:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cnp_signatures:Ljava/lang/Boolean;

    .line 10438
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->allow_cof_signatures:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cof_signatures:Ljava/lang/Boolean;

    .line 10439
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_payment_method_v2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_payment_method_v2:Ljava/lang/Boolean;

    .line 10440
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_jp_formal_printed_receipts:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_jp_formal_printed_receipts:Ljava/lang/Boolean;

    .line 10441
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->print_tax_percentage:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->print_tax_percentage:Ljava/lang/Boolean;

    .line 10442
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->portrait_signature:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->portrait_signature:Ljava/lang/Boolean;

    .line 10443
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->buyer_checkout:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->buyer_checkout:Ljava/lang/Boolean;

    .line 10444
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->even_splits_android_v2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->even_splits_android_v2:Ljava/lang/Boolean;

    .line 10445
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_buyer_language_selection:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_buyer_language_selection:Ljava/lang/Boolean;

    .line 10446
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_always_skip_signatures:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_always_skip_signatures:Ljava/lang/Boolean;

    .line 10447
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_tender_in_edit_refactor:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_tender_in_edit_refactor:Ljava/lang/Boolean;

    .line 10448
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->show_accidental_cash_other_modal:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_accidental_cash_other_modal:Ljava/lang/Boolean;

    .line 10449
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->emoney:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->emoney:Ljava/lang/Boolean;

    .line 10450
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->prefer_sqlite_tasks_queue:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->prefer_sqlite_tasks_queue:Ljava/lang/Boolean;

    .line 10451
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->show_noho_receipt_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_noho_receipt_android:Ljava/lang/Boolean;

    .line 10452
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->use_auth_workflow_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->use_auth_workflow_android:Ljava/lang/Boolean;

    .line 10453
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_installments:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_installments:Ljava/lang/Boolean;

    .line 10454
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_seller_cash_received_workflow_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_seller_cash_received_workflow_android:Ljava/lang/Boolean;

    .line 10455
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_split_tender_workflow_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_split_tender_workflow_android:Ljava/lang/Boolean;

    .line 10456
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_cto_workflow_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cto_workflow_android:Ljava/lang/Boolean;

    .line 10457
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_separate_local_payments_queue_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_separate_local_payments_queue_android:Ljava/lang/Boolean;

    .line 10458
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->do_not_user_task_queue_for_payments_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->do_not_user_task_queue_for_payments_android:Ljava/lang/Boolean;

    .line 10459
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->always_show_itemized_cart:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->always_show_itemized_cart:Ljava/lang/Boolean;

    .line 10460
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_cash_workflow_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_workflow_android:Ljava/lang/Boolean;

    .line 10461
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_cash_qr_codes:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_qr_codes:Ljava/lang/Boolean;

    .line 10462
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_split_emoney:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_split_emoney:Ljava/lang/Boolean;

    .line 10463
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->android_use_payment_config:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->android_use_payment_config:Ljava/lang/Boolean;

    .line 10464
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->orders_integration_process_cnp_via_orders:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->orders_integration_process_cnp_via_orders:Ljava/lang/Boolean;

    .line 10465
    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_select_method_workflow_android:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_select_method_workflow_android:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 10686
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 10507
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 10508
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    .line 10509
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cnp_signatures:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cnp_signatures:Ljava/lang/Boolean;

    .line 10510
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cof_signatures:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cof_signatures:Ljava/lang/Boolean;

    .line 10511
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_payment_method_v2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_payment_method_v2:Ljava/lang/Boolean;

    .line 10512
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_jp_formal_printed_receipts:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_jp_formal_printed_receipts:Ljava/lang/Boolean;

    .line 10513
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->print_tax_percentage:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->print_tax_percentage:Ljava/lang/Boolean;

    .line 10514
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->portrait_signature:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->portrait_signature:Ljava/lang/Boolean;

    .line 10515
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->buyer_checkout:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->buyer_checkout:Ljava/lang/Boolean;

    .line 10516
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->even_splits_android_v2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->even_splits_android_v2:Ljava/lang/Boolean;

    .line 10517
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_buyer_language_selection:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_buyer_language_selection:Ljava/lang/Boolean;

    .line 10518
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_always_skip_signatures:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_always_skip_signatures:Ljava/lang/Boolean;

    .line 10519
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_tender_in_edit_refactor:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_tender_in_edit_refactor:Ljava/lang/Boolean;

    .line 10520
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_accidental_cash_other_modal:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_accidental_cash_other_modal:Ljava/lang/Boolean;

    .line 10521
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->emoney:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->emoney:Ljava/lang/Boolean;

    .line 10522
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->prefer_sqlite_tasks_queue:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->prefer_sqlite_tasks_queue:Ljava/lang/Boolean;

    .line 10523
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_noho_receipt_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_noho_receipt_android:Ljava/lang/Boolean;

    .line 10524
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->use_auth_workflow_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->use_auth_workflow_android:Ljava/lang/Boolean;

    .line 10525
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_installments:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_installments:Ljava/lang/Boolean;

    .line 10526
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_seller_cash_received_workflow_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_seller_cash_received_workflow_android:Ljava/lang/Boolean;

    .line 10527
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_split_tender_workflow_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_split_tender_workflow_android:Ljava/lang/Boolean;

    .line 10528
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cto_workflow_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cto_workflow_android:Ljava/lang/Boolean;

    .line 10529
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_separate_local_payments_queue_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_separate_local_payments_queue_android:Ljava/lang/Boolean;

    .line 10530
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->do_not_user_task_queue_for_payments_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->do_not_user_task_queue_for_payments_android:Ljava/lang/Boolean;

    .line 10531
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->always_show_itemized_cart:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->always_show_itemized_cart:Ljava/lang/Boolean;

    .line 10532
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_workflow_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_workflow_android:Ljava/lang/Boolean;

    .line 10533
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_qr_codes:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_qr_codes:Ljava/lang/Boolean;

    .line 10534
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_split_emoney:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_split_emoney:Ljava/lang/Boolean;

    .line 10535
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->android_use_payment_config:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->android_use_payment_config:Ljava/lang/Boolean;

    .line 10536
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->orders_integration_process_cnp_via_orders:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->orders_integration_process_cnp_via_orders:Ljava/lang/Boolean;

    .line 10537
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_select_method_workflow_android:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_select_method_workflow_android:Ljava/lang/Boolean;

    .line 10538
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 10543
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_1d

    .line 10545
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 10546
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cnp_signatures:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10547
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cof_signatures:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10548
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_payment_method_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10549
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_jp_formal_printed_receipts:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10550
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->print_tax_percentage:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10551
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->portrait_signature:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10552
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->buyer_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10553
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->even_splits_android_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10554
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_buyer_language_selection:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10555
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_always_skip_signatures:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10556
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_tender_in_edit_refactor:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10557
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_accidental_cash_other_modal:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10558
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->emoney:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10559
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->prefer_sqlite_tasks_queue:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10560
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_noho_receipt_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10561
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->use_auth_workflow_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10562
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_installments:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10563
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_seller_cash_received_workflow_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10564
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_split_tender_workflow_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10565
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cto_workflow_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10566
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_separate_local_payments_queue_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10567
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->do_not_user_task_queue_for_payments_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10568
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->always_show_itemized_cart:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10569
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_workflow_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10570
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_qr_codes:Ljava/lang/Boolean;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10571
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_split_emoney:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10572
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->android_use_payment_config:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_1a
    const/4 v1, 0x0

    :goto_1a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10573
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->orders_integration_process_cnp_via_orders:Ljava/lang/Boolean;

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1b

    :cond_1b
    const/4 v1, 0x0

    :goto_1b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10574
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_select_method_workflow_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_1c
    add-int/2addr v0, v2

    .line 10575
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_1d
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 2

    .line 10470
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;-><init>()V

    .line 10471
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cnp_signatures:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->allow_cnp_signatures:Ljava/lang/Boolean;

    .line 10472
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cof_signatures:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->allow_cof_signatures:Ljava/lang/Boolean;

    .line 10473
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_payment_method_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_payment_method_v2:Ljava/lang/Boolean;

    .line 10474
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_jp_formal_printed_receipts:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_jp_formal_printed_receipts:Ljava/lang/Boolean;

    .line 10475
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->print_tax_percentage:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->print_tax_percentage:Ljava/lang/Boolean;

    .line 10476
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->portrait_signature:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->portrait_signature:Ljava/lang/Boolean;

    .line 10477
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->buyer_checkout:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->buyer_checkout:Ljava/lang/Boolean;

    .line 10478
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->even_splits_android_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->even_splits_android_v2:Ljava/lang/Boolean;

    .line 10479
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_buyer_language_selection:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_buyer_language_selection:Ljava/lang/Boolean;

    .line 10480
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_always_skip_signatures:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_always_skip_signatures:Ljava/lang/Boolean;

    .line 10481
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_tender_in_edit_refactor:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_tender_in_edit_refactor:Ljava/lang/Boolean;

    .line 10482
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_accidental_cash_other_modal:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->show_accidental_cash_other_modal:Ljava/lang/Boolean;

    .line 10483
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->emoney:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->emoney:Ljava/lang/Boolean;

    .line 10484
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->prefer_sqlite_tasks_queue:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->prefer_sqlite_tasks_queue:Ljava/lang/Boolean;

    .line 10485
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_noho_receipt_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->show_noho_receipt_android:Ljava/lang/Boolean;

    .line 10486
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->use_auth_workflow_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->use_auth_workflow_android:Ljava/lang/Boolean;

    .line 10487
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_installments:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_installments:Ljava/lang/Boolean;

    .line 10488
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_seller_cash_received_workflow_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_seller_cash_received_workflow_android:Ljava/lang/Boolean;

    .line 10489
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_split_tender_workflow_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_split_tender_workflow_android:Ljava/lang/Boolean;

    .line 10490
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cto_workflow_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_cto_workflow_android:Ljava/lang/Boolean;

    .line 10491
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_separate_local_payments_queue_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_separate_local_payments_queue_android:Ljava/lang/Boolean;

    .line 10492
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->do_not_user_task_queue_for_payments_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->do_not_user_task_queue_for_payments_android:Ljava/lang/Boolean;

    .line 10493
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->always_show_itemized_cart:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->always_show_itemized_cart:Ljava/lang/Boolean;

    .line 10494
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_workflow_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_cash_workflow_android:Ljava/lang/Boolean;

    .line 10495
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_qr_codes:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_cash_qr_codes:Ljava/lang/Boolean;

    .line 10496
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_split_emoney:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_split_emoney:Ljava/lang/Boolean;

    .line 10497
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->android_use_payment_config:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->android_use_payment_config:Ljava/lang/Boolean;

    .line 10498
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->orders_integration_process_cnp_via_orders:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->orders_integration_process_cnp_via_orders:Ljava/lang/Boolean;

    .line 10499
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_select_method_workflow_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_select_method_workflow_android:Ljava/lang/Boolean;

    .line 10500
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 10164
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;
    .locals 2

    .line 10653
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cnp_signatures:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cnp_signatures:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->allow_cnp_signatures(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10654
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cof_signatures:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cof_signatures:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->allow_cof_signatures(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10655
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_payment_method_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_payment_method_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_payment_method_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10656
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_jp_formal_printed_receipts:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_jp_formal_printed_receipts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_jp_formal_printed_receipts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10657
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->print_tax_percentage:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->print_tax_percentage:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->print_tax_percentage(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10658
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->portrait_signature:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->portrait_signature:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->portrait_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10659
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->buyer_checkout:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->buyer_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->buyer_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10660
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->even_splits_android_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->even_splits_android_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->even_splits_android_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10661
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_buyer_language_selection:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_buyer_language_selection:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_buyer_language_selection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10662
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_always_skip_signatures:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_always_skip_signatures:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_always_skip_signatures(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10663
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_tender_in_edit_refactor:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_tender_in_edit_refactor:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_tender_in_edit_refactor(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10664
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_accidental_cash_other_modal:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_accidental_cash_other_modal:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->show_accidental_cash_other_modal(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10665
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->emoney:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->emoney:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->emoney(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10666
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->prefer_sqlite_tasks_queue:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->prefer_sqlite_tasks_queue:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->prefer_sqlite_tasks_queue(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10667
    :cond_d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_noho_receipt_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_noho_receipt_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->show_noho_receipt_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10668
    :cond_e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->use_auth_workflow_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->use_auth_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->use_auth_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10669
    :cond_f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_installments:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_installments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_installments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10670
    :cond_10
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_seller_cash_received_workflow_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_seller_cash_received_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_seller_cash_received_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10671
    :cond_11
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_split_tender_workflow_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_split_tender_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_split_tender_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10672
    :cond_12
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cto_workflow_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cto_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_cto_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10673
    :cond_13
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_separate_local_payments_queue_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_separate_local_payments_queue_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_separate_local_payments_queue_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10674
    :cond_14
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->do_not_user_task_queue_for_payments_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_15

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->do_not_user_task_queue_for_payments_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->do_not_user_task_queue_for_payments_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10675
    :cond_15
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->always_show_itemized_cart:Ljava/lang/Boolean;

    if-eqz v0, :cond_16

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->always_show_itemized_cart:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->always_show_itemized_cart(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10676
    :cond_16
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_workflow_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_17

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_cash_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10677
    :cond_17
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_qr_codes:Ljava/lang/Boolean;

    if-eqz v0, :cond_18

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_qr_codes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_cash_qr_codes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10678
    :cond_18
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_split_emoney:Ljava/lang/Boolean;

    if-eqz v0, :cond_19

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_split_emoney:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_split_emoney(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10679
    :cond_19
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->android_use_payment_config:Ljava/lang/Boolean;

    if-eqz v0, :cond_1a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->android_use_payment_config:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->android_use_payment_config(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10680
    :cond_1a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->orders_integration_process_cnp_via_orders:Ljava/lang/Boolean;

    if-eqz v0, :cond_1b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->orders_integration_process_cnp_via_orders:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->orders_integration_process_cnp_via_orders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10681
    :cond_1b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_select_method_workflow_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_1c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_select_method_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_select_method_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    :cond_1c
    if-nez v1, :cond_1d

    move-object p1, p0

    goto :goto_0

    .line 10682
    :cond_1d
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 10164
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;
    .locals 2

    .line 10618
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cnp_signatures:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_ALLOW_CNP_SIGNATURES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->allow_cnp_signatures(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10619
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cof_signatures:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_ALLOW_COF_SIGNATURES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->allow_cof_signatures(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10620
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_payment_method_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_PAYMENT_METHOD_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_payment_method_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10621
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_jp_formal_printed_receipts:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_JP_FORMAL_PRINTED_RECEIPTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_jp_formal_printed_receipts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10622
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->print_tax_percentage:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_PRINT_TAX_PERCENTAGE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->print_tax_percentage(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10623
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->portrait_signature:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_PORTRAIT_SIGNATURE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->portrait_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10624
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->buyer_checkout:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_BUYER_CHECKOUT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->buyer_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10625
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->even_splits_android_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_EVEN_SPLITS_ANDROID_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->even_splits_android_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10626
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_buyer_language_selection:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_BUYER_LANGUAGE_SELECTION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_buyer_language_selection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10627
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_always_skip_signatures:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_ALWAYS_SKIP_SIGNATURES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_always_skip_signatures(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10628
    :cond_9
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_tender_in_edit_refactor:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_TENDER_IN_EDIT_REFACTOR:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_tender_in_edit_refactor(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10629
    :cond_a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_accidental_cash_other_modal:Ljava/lang/Boolean;

    if-nez v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_SHOW_ACCIDENTAL_CASH_OTHER_MODAL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->show_accidental_cash_other_modal(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10630
    :cond_b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->emoney:Ljava/lang/Boolean;

    if-nez v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_EMONEY:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->emoney(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10631
    :cond_c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->prefer_sqlite_tasks_queue:Ljava/lang/Boolean;

    if-nez v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_PREFER_SQLITE_TASKS_QUEUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->prefer_sqlite_tasks_queue(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10632
    :cond_d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_noho_receipt_android:Ljava/lang/Boolean;

    if-nez v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_SHOW_NOHO_RECEIPT_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->show_noho_receipt_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10633
    :cond_e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->use_auth_workflow_android:Ljava/lang/Boolean;

    if-nez v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_USE_AUTH_WORKFLOW_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->use_auth_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10634
    :cond_f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_installments:Ljava/lang/Boolean;

    if-nez v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_INSTALLMENTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_installments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10635
    :cond_10
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_seller_cash_received_workflow_android:Ljava/lang/Boolean;

    if-nez v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_SELLER_CASH_RECEIVED_WORKFLOW_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_seller_cash_received_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10636
    :cond_11
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_split_tender_workflow_android:Ljava/lang/Boolean;

    if-nez v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_SPLIT_TENDER_WORKFLOW_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_split_tender_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10637
    :cond_12
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cto_workflow_android:Ljava/lang/Boolean;

    if-nez v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_CTO_WORKFLOW_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_cto_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10638
    :cond_13
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_separate_local_payments_queue_android:Ljava/lang/Boolean;

    if-nez v0, :cond_14

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_SEPARATE_LOCAL_PAYMENTS_QUEUE_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_separate_local_payments_queue_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10639
    :cond_14
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->do_not_user_task_queue_for_payments_android:Ljava/lang/Boolean;

    if-nez v0, :cond_15

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_DO_NOT_USER_TASK_QUEUE_FOR_PAYMENTS_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->do_not_user_task_queue_for_payments_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10640
    :cond_15
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->always_show_itemized_cart:Ljava/lang/Boolean;

    if-nez v0, :cond_16

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_ALWAYS_SHOW_ITEMIZED_CART:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->always_show_itemized_cart(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10641
    :cond_16
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_workflow_android:Ljava/lang/Boolean;

    if-nez v0, :cond_17

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_CASH_WORKFLOW_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_cash_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10642
    :cond_17
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_qr_codes:Ljava/lang/Boolean;

    if-nez v0, :cond_18

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_CASH_QR_CODES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_cash_qr_codes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10643
    :cond_18
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_split_emoney:Ljava/lang/Boolean;

    if-nez v0, :cond_19

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_SPLIT_EMONEY:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_split_emoney(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10644
    :cond_19
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->android_use_payment_config:Ljava/lang/Boolean;

    if-nez v0, :cond_1a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_ANDROID_USE_PAYMENT_CONFIG:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->android_use_payment_config(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10645
    :cond_1a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->orders_integration_process_cnp_via_orders:Ljava/lang/Boolean;

    if-nez v0, :cond_1b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_ORDERS_INTEGRATION_PROCESS_CNP_VIA_ORDERS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->orders_integration_process_cnp_via_orders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    .line 10646
    :cond_1b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_select_method_workflow_android:Ljava/lang/Boolean;

    if-nez v0, :cond_1c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->DEFAULT_CAN_USE_SELECT_METHOD_WORKFLOW_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_select_method_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;

    move-result-object v1

    :cond_1c
    if-nez v1, :cond_1d

    move-object v0, p0

    goto :goto_0

    .line 10647
    :cond_1d
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 10164
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 10582
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 10583
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cnp_signatures:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", allow_cnp_signatures="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cnp_signatures:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10584
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cof_signatures:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", allow_cof_signatures="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->allow_cof_signatures:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10585
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_payment_method_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", can_use_payment_method_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_payment_method_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10586
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_jp_formal_printed_receipts:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", can_use_jp_formal_printed_receipts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_jp_formal_printed_receipts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10587
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->print_tax_percentage:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", print_tax_percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->print_tax_percentage:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10588
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->portrait_signature:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", portrait_signature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->portrait_signature:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10589
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->buyer_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", buyer_checkout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->buyer_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10590
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->even_splits_android_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", even_splits_android_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->even_splits_android_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10591
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_buyer_language_selection:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", can_use_buyer_language_selection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_buyer_language_selection:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10592
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_always_skip_signatures:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", can_always_skip_signatures="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_always_skip_signatures:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10593
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_tender_in_edit_refactor:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", can_use_tender_in_edit_refactor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_tender_in_edit_refactor:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10594
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_accidental_cash_other_modal:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", show_accidental_cash_other_modal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_accidental_cash_other_modal:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10595
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->emoney:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", emoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->emoney:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10596
    :cond_c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->prefer_sqlite_tasks_queue:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", prefer_sqlite_tasks_queue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->prefer_sqlite_tasks_queue:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10597
    :cond_d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_noho_receipt_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    const-string v1, ", show_noho_receipt_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->show_noho_receipt_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10598
    :cond_e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->use_auth_workflow_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    const-string v1, ", use_auth_workflow_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->use_auth_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10599
    :cond_f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_installments:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    const-string v1, ", can_use_installments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_installments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10600
    :cond_10
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_seller_cash_received_workflow_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    const-string v1, ", can_use_seller_cash_received_workflow_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_seller_cash_received_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10601
    :cond_11
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_split_tender_workflow_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    const-string v1, ", can_use_split_tender_workflow_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_split_tender_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10602
    :cond_12
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cto_workflow_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    const-string v1, ", can_use_cto_workflow_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cto_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10603
    :cond_13
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_separate_local_payments_queue_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    const-string v1, ", can_use_separate_local_payments_queue_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_separate_local_payments_queue_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10604
    :cond_14
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->do_not_user_task_queue_for_payments_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_15

    const-string v1, ", do_not_user_task_queue_for_payments_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->do_not_user_task_queue_for_payments_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10605
    :cond_15
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->always_show_itemized_cart:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    const-string v1, ", always_show_itemized_cart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->always_show_itemized_cart:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10606
    :cond_16
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_workflow_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    const-string v1, ", can_use_cash_workflow_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10607
    :cond_17
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_qr_codes:Ljava/lang/Boolean;

    if-eqz v1, :cond_18

    const-string v1, ", can_use_cash_qr_codes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_cash_qr_codes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10608
    :cond_18
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_split_emoney:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    const-string v1, ", can_split_emoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_split_emoney:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10609
    :cond_19
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->android_use_payment_config:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    const-string v1, ", android_use_payment_config="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->android_use_payment_config:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10610
    :cond_1a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->orders_integration_process_cnp_via_orders:Ljava/lang/Boolean;

    if-eqz v1, :cond_1b

    const-string v1, ", orders_integration_process_cnp_via_orders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->orders_integration_process_cnp_via_orders:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10611
    :cond_1b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_select_method_workflow_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_1c

    const-string v1, ", can_use_select_method_workflow_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->can_use_select_method_workflow_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PaymentFlow{"

    .line 10612
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
