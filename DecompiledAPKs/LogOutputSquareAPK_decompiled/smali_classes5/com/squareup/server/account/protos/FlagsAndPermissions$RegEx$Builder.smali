.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bundle_logging:Ljava/lang/Boolean;

.field public buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

.field public can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

.field public can_print_single_ticket_per_item:Ljava/lang/Boolean;

.field public checkout_applet_v2_android:Ljava/lang/Boolean;

.field public delete_session_token_plaintext:Ljava/lang/Boolean;

.field public dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

.field public enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

.field public enhanced_transaction_search_android:Ljava/lang/Boolean;

.field public firmware_update_jail_t2:Ljava/lang/Boolean;

.field public firmware_update_t2:Ljava/lang/Boolean;

.field public hardware_secure_touch:Ljava/lang/Boolean;

.field public hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

.field public hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

.field public ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

.field public intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

.field public loyalty_checkout_x2:Ljava/lang/Boolean;

.field public order_entry_library_create_new_item:Ljava/lang/Boolean;

.field public order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

.field public order_entry_library_item_suggestions:Ljava/lang/Boolean;

.field public print_itemized_discounts:Ljava/lang/Boolean;

.field public printer_debugging:Ljava/lang/Boolean;

.field public receipts_print_disposition:Ljava/lang/Boolean;

.field public resilient_bus:Ljava/lang/Boolean;

.field public restart_app_after_crash:Ljava/lang/Boolean;

.field public separated_printouts:Ljava/lang/Boolean;

.field public show_fee_breakdown_table:Ljava/lang/Boolean;

.field public skyhook_integration_v2:Ljava/lang/Boolean;

.field public skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

.field public skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

.field public skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

.field public skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

.field public upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

.field public upload_x2_wifi_events:Ljava/lang/Boolean;

.field public use_api_url_list_android:Ljava/lang/Boolean;

.field public use_clock_skew_lockout_android:Ljava/lang/Boolean;

.field public use_device_settings_android:Ljava/lang/Boolean;

.field public use_feature_tour_x2:Ljava/lang/Boolean;

.field public use_jedi_help_applet:Ljava/lang/Boolean;

.field public use_jedi_help_applet_t2:Ljava/lang/Boolean;

.field public use_jedi_help_applet_x2:Ljava/lang/Boolean;

.field public use_order_entry_screen_v2_android:Ljava/lang/Boolean;

.field public use_persistent_bundle_android:Ljava/lang/Boolean;

.field public use_pre_tax_tipping_android:Ljava/lang/Boolean;

.field public use_predefined_tickets_t2:Ljava/lang/Boolean;

.field public use_session_token_encryption_android:Ljava/lang/Boolean;

.field public use_session_token_encryption_android_v2:Ljava/lang/Boolean;

.field public wait_for_bran_x2:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6897
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;
    .locals 2

    .line 7296
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;-><init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 6800
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object v0

    return-object v0
.end method

.method public bundle_logging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 6904
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->bundle_logging:Ljava/lang/Boolean;

    return-object p0
.end method

.method public buyer_checkout_display_transaction_type(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7170
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_close_remote_cash_drawers_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7109
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_print_single_ticket_per_item(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7229
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->can_print_single_ticket_per_item:Ljava/lang/Boolean;

    return-object p0
.end method

.method public checkout_applet_v2_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7290
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->checkout_applet_v2_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public delete_session_token_plaintext(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7255
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->delete_session_token_plaintext:Ljava/lang/Boolean;

    return-object p0
.end method

.method public dip_tap_to_create_open_ticket_with_name(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 6970
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    return-object p0
.end method

.method public enable_verbose_login_response_cache_logging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7264
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

    return-object p0
.end method

.method public enhanced_transaction_search_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7247
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->enhanced_transaction_search_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public firmware_update_jail_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7092
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->firmware_update_jail_t2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public firmware_update_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7060
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->firmware_update_t2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public hardware_secure_touch(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7100
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch:Ljava/lang/Boolean;

    return-object p0
.end method

.method public hardware_secure_touch_accessibility_mode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7238
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

    return-object p0
.end method

.method public hardware_secure_touch_high_contrast_mode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7282
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ignore_pii_accessibility_scrubber_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 6921
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public intercept_magswipe_events_during_printing_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7036
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public loyalty_checkout_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 6937
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->loyalty_checkout_x2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public order_entry_library_create_new_item(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7187
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_create_new_item:Ljava/lang/Boolean;

    return-object p0
.end method

.method public order_entry_library_hide_giftcards_rewards_if_no_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7204
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

    return-object p0
.end method

.method public order_entry_library_item_suggestions(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7221
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_item_suggestions:Ljava/lang/Boolean;

    return-object p0
.end method

.method public print_itemized_discounts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7003
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->print_itemized_discounts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public printer_debugging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7019
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->printer_debugging:Ljava/lang/Boolean;

    return-object p0
.end method

.method public receipts_print_disposition(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7178
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->receipts_print_disposition:Ljava/lang/Boolean;

    return-object p0
.end method

.method public resilient_bus(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7084
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->resilient_bus:Ljava/lang/Boolean;

    return-object p0
.end method

.method public restart_app_after_crash(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7027
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->restart_app_after_crash:Ljava/lang/Boolean;

    return-object p0
.end method

.method public separated_printouts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7068
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->separated_printouts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_fee_breakdown_table(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7195
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->show_fee_breakdown_table:Ljava/lang/Boolean;

    return-object p0
.end method

.method public skyhook_integration_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7125
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_integration_v2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public skyhook_t2_use_3_hour_check_interval(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7143
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

    return-object p0
.end method

.method public skyhook_t2_use_6_hour_cache_lifespan(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7152
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

    return-object p0
.end method

.method public skyhook_v2_only_use_ip_location_with_no_wps(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7161
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

    return-object p0
.end method

.method public skyhook_x2_use_24_hour_cache_lifespan(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7134
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

    return-object p0
.end method

.method public upload_ledger_and_diagnostics_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 6995
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public upload_x2_wifi_events(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 6929
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->upload_x2_wifi_events:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_api_url_list_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 6915
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_api_url_list_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_clock_skew_lockout_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 6945
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_clock_skew_lockout_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_device_settings_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7044
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_device_settings_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_feature_tour_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7011
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_feature_tour_x2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_jedi_help_applet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 6953
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_jedi_help_applet_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7052
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet_t2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_jedi_help_applet_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 6987
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet_x2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_order_entry_screen_v2_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7212
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_order_entry_screen_v2_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_persistent_bundle_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 6961
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_persistent_bundle_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_pre_tax_tipping_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 6910
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_predefined_tickets_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7076
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_predefined_tickets_t2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_session_token_encryption_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 6979
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_session_token_encryption_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_session_token_encryption_android_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7273
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_session_token_encryption_android_v2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public wait_for_bran_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    .line 7117
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->wait_for_bran_x2:Ljava/lang/Boolean;

    return-object p0
.end method
