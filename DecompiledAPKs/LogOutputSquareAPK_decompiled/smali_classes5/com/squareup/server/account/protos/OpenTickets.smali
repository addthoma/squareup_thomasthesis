.class public final Lcom/squareup/server/account/protos/OpenTickets;
.super Lcom/squareup/wire/AndroidMessage;
.source "OpenTickets.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/OpenTickets$ProtoAdapter_OpenTickets;,
        Lcom/squareup/server/account/protos/OpenTickets$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/OpenTickets;",
        "Lcom/squareup/server/account/protos/OpenTickets$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/OpenTickets;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/OpenTickets;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/OpenTickets;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/OpenTickets;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_OPEN_TICKETS_MAX:Ljava/lang/Integer;

.field public static final DEFAULT_TICKET_GROUPS_MAX:Ljava/lang/Integer;

.field public static final DEFAULT_TICKET_TEMPLATES_PER_GROUP_MAX:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final open_tickets_max:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field

.field public final ticket_groups_max:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final ticket_templates_per_group_max:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/server/account/protos/OpenTickets$ProtoAdapter_OpenTickets;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/OpenTickets$ProtoAdapter_OpenTickets;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/OpenTickets;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 33
    sget-object v0, Lcom/squareup/server/account/protos/OpenTickets;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/OpenTickets;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 37
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/OpenTickets;->DEFAULT_OPEN_TICKETS_MAX:Ljava/lang/Integer;

    .line 39
    sput-object v0, Lcom/squareup/server/account/protos/OpenTickets;->DEFAULT_TICKET_GROUPS_MAX:Ljava/lang/Integer;

    .line 41
    sput-object v0, Lcom/squareup/server/account/protos/OpenTickets;->DEFAULT_TICKET_TEMPLATES_PER_GROUP_MAX:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1

    .line 66
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/server/account/protos/OpenTickets;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 71
    sget-object v0, Lcom/squareup/server/account/protos/OpenTickets;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 72
    iput-object p1, p0, Lcom/squareup/server/account/protos/OpenTickets;->open_tickets_max:Ljava/lang/Integer;

    .line 73
    iput-object p2, p0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_groups_max:Ljava/lang/Integer;

    .line 74
    iput-object p3, p0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_templates_per_group_max:Ljava/lang/Integer;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/OpenTickets$Builder;)Lcom/squareup/server/account/protos/OpenTickets$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/OpenTickets;->newBuilder()Lcom/squareup/server/account/protos/OpenTickets$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 90
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/OpenTickets;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 91
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/OpenTickets;

    .line 92
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/OpenTickets;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/OpenTickets;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/OpenTickets;->open_tickets_max:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/server/account/protos/OpenTickets;->open_tickets_max:Ljava/lang/Integer;

    .line 93
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_groups_max:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/server/account/protos/OpenTickets;->ticket_groups_max:Ljava/lang/Integer;

    .line 94
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_templates_per_group_max:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/server/account/protos/OpenTickets;->ticket_templates_per_group_max:Ljava/lang/Integer;

    .line 95
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 100
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_3

    .line 102
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/OpenTickets;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 103
    iget-object v1, p0, Lcom/squareup/server/account/protos/OpenTickets;->open_tickets_max:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 104
    iget-object v1, p0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_groups_max:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 105
    iget-object v1, p0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_templates_per_group_max:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 106
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/OpenTickets$Builder;
    .locals 2

    .line 79
    new-instance v0, Lcom/squareup/server/account/protos/OpenTickets$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/OpenTickets$Builder;-><init>()V

    .line 80
    iget-object v1, p0, Lcom/squareup/server/account/protos/OpenTickets;->open_tickets_max:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/server/account/protos/OpenTickets$Builder;->open_tickets_max:Ljava/lang/Integer;

    .line 81
    iget-object v1, p0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_groups_max:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/server/account/protos/OpenTickets$Builder;->ticket_groups_max:Ljava/lang/Integer;

    .line 82
    iget-object v1, p0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_templates_per_group_max:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/server/account/protos/OpenTickets$Builder;->ticket_templates_per_group_max:Ljava/lang/Integer;

    .line 83
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/OpenTickets;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/OpenTickets$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/OpenTickets;->newBuilder()Lcom/squareup/server/account/protos/OpenTickets$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/OpenTickets;)Lcom/squareup/server/account/protos/OpenTickets;
    .locals 2

    .line 132
    iget-object v0, p1, Lcom/squareup/server/account/protos/OpenTickets;->open_tickets_max:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/OpenTickets;->requireBuilder(Lcom/squareup/server/account/protos/OpenTickets$Builder;)Lcom/squareup/server/account/protos/OpenTickets$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/OpenTickets;->open_tickets_max:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/OpenTickets$Builder;->open_tickets_max(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/OpenTickets$Builder;

    move-result-object v1

    .line 133
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/OpenTickets;->ticket_groups_max:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/OpenTickets;->requireBuilder(Lcom/squareup/server/account/protos/OpenTickets$Builder;)Lcom/squareup/server/account/protos/OpenTickets$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/OpenTickets;->ticket_groups_max:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/OpenTickets$Builder;->ticket_groups_max(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/OpenTickets$Builder;

    move-result-object v1

    .line 134
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/OpenTickets;->ticket_templates_per_group_max:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/OpenTickets;->requireBuilder(Lcom/squareup/server/account/protos/OpenTickets$Builder;)Lcom/squareup/server/account/protos/OpenTickets$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/OpenTickets;->ticket_templates_per_group_max:Ljava/lang/Integer;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/OpenTickets$Builder;->ticket_templates_per_group_max(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/OpenTickets$Builder;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    move-object p1, p0

    goto :goto_0

    .line 135
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/OpenTickets$Builder;->build()Lcom/squareup/server/account/protos/OpenTickets;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/server/account/protos/OpenTickets;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/OpenTickets;->overlay(Lcom/squareup/server/account/protos/OpenTickets;)Lcom/squareup/server/account/protos/OpenTickets;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/OpenTickets;
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/server/account/protos/OpenTickets;->open_tickets_max:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/OpenTickets;->requireBuilder(Lcom/squareup/server/account/protos/OpenTickets$Builder;)Lcom/squareup/server/account/protos/OpenTickets$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/OpenTickets;->DEFAULT_OPEN_TICKETS_MAX:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/OpenTickets$Builder;->open_tickets_max(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/OpenTickets$Builder;

    move-result-object v1

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_groups_max:Ljava/lang/Integer;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/OpenTickets;->requireBuilder(Lcom/squareup/server/account/protos/OpenTickets$Builder;)Lcom/squareup/server/account/protos/OpenTickets$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/OpenTickets;->DEFAULT_TICKET_GROUPS_MAX:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/OpenTickets$Builder;->ticket_groups_max(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/OpenTickets$Builder;

    move-result-object v1

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_templates_per_group_max:Ljava/lang/Integer;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/OpenTickets;->requireBuilder(Lcom/squareup/server/account/protos/OpenTickets$Builder;)Lcom/squareup/server/account/protos/OpenTickets$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/OpenTickets;->DEFAULT_TICKET_TEMPLATES_PER_GROUP_MAX:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/OpenTickets$Builder;->ticket_templates_per_group_max(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/OpenTickets$Builder;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    move-object v0, p0

    goto :goto_0

    .line 126
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/OpenTickets$Builder;->build()Lcom/squareup/server/account/protos/OpenTickets;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/OpenTickets;->populateDefaults()Lcom/squareup/server/account/protos/OpenTickets;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    iget-object v1, p0, Lcom/squareup/server/account/protos/OpenTickets;->open_tickets_max:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", open_tickets_max="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/OpenTickets;->open_tickets_max:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 115
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_groups_max:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", ticket_groups_max="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_groups_max:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 116
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_templates_per_group_max:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", ticket_templates_per_group_max="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/OpenTickets;->ticket_templates_per_group_max:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "OpenTickets{"

    .line 117
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
