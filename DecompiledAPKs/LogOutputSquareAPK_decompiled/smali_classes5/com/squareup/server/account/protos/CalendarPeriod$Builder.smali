.class public final Lcom/squareup/server/account/protos/CalendarPeriod$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CalendarPeriod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/CalendarPeriod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/CalendarPeriod;",
        "Lcom/squareup/server/account/protos/CalendarPeriod$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 87
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/CalendarPeriod;
    .locals 2

    .line 92
    new-instance v0, Lcom/squareup/server/account/protos/CalendarPeriod;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/server/account/protos/CalendarPeriod;-><init>(Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 86
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/CalendarPeriod$Builder;->build()Lcom/squareup/server/account/protos/CalendarPeriod;

    move-result-object v0

    return-object v0
.end method
