.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public can_show_notification_center:Ljava/lang/Boolean;

.field public can_show_sales_report_v2:Ljava/lang/Boolean;

.field public can_show_sales_report_v2_feature_carousel:Ljava/lang/Boolean;

.field public can_show_settings_v2:Ljava/lang/Boolean;

.field public can_use_ble_scales:Ljava/lang/Boolean;

.field public can_use_cash_management:Ljava/lang/Boolean;

.field public can_use_forced_offline_mode:Ljava/lang/Boolean;

.field public can_use_scales:Ljava/lang/Boolean;

.field public can_use_spos_order_hub:Ljava/lang/Boolean;

.field public enable_felica_certification_environment:Ljava/lang/Boolean;

.field public sanitize_eventstream_coordinates:Ljava/lang/Boolean;

.field public setup_guide:Ljava/lang/Boolean;

.field public use_device_level_paper_signature:Ljava/lang/Boolean;

.field public use_open_tickets_v2:Ljava/lang/Boolean;

.field public use_open_tickets_v2_name_and_notes:Ljava/lang/Boolean;

.field public use_open_tickets_v2_search_sort_filter:Ljava/lang/Boolean;

.field public use_predefined_tickets_v2:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11418
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;
    .locals 2

    .line 11527
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;-><init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 11383
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object v0

    return-object v0
.end method

.method public can_show_notification_center(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11433
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_notification_center:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_show_sales_report_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11457
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_sales_report_v2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_show_sales_report_v2_feature_carousel(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11463
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_sales_report_v2_feature_carousel:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_show_settings_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11521
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_show_settings_v2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_ble_scales(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11478
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_ble_scales:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_cash_management(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11511
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_cash_management:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_forced_offline_mode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11425
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_forced_offline_mode:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_scales(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11473
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_scales:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_spos_order_hub(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11441
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->can_use_spos_order_hub:Ljava/lang/Boolean;

    return-object p0
.end method

.method public enable_felica_certification_environment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11484
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->enable_felica_certification_environment:Ljava/lang/Boolean;

    return-object p0
.end method

.method public sanitize_eventstream_coordinates(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11516
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->sanitize_eventstream_coordinates:Ljava/lang/Boolean;

    return-object p0
.end method

.method public setup_guide(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11468
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->setup_guide:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_device_level_paper_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11449
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_device_level_paper_signature:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_open_tickets_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11489
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_open_tickets_v2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_open_tickets_v2_name_and_notes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11506
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_open_tickets_v2_name_and_notes:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_open_tickets_v2_search_sort_filter(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11500
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_open_tickets_v2_search_sort_filter:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_predefined_tickets_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;
    .locals 0

    .line 11494
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos$Builder;->use_predefined_tickets_v2:Ljava/lang/Boolean;

    return-object p0
.end method
