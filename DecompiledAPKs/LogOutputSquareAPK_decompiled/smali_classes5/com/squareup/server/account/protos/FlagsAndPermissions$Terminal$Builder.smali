.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public app_can_order_r12:Ljava/lang/Boolean;

.field public app_can_order_r4:Ljava/lang/Boolean;

.field public app_can_order_r41:Ljava/lang/Boolean;

.field public app_can_show_account_freeze:Ljava/lang/Boolean;

.field public app_can_show_account_freeze_banner:Ljava/lang/Boolean;

.field public app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

.field public app_can_show_cnp_fees:Ljava/lang/Boolean;

.field public app_can_show_cp_pricing_change:Ljava/lang/Boolean;

.field public app_can_show_disputes:Ljava/lang/Boolean;

.field public app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;

.field public app_can_show_order_reader_menu:Ljava/lang/Boolean;

.field public app_collect_cnp_postal_code:Ljava/lang/Boolean;

.field public app_collect_cof_postal_code:Ljava/lang/Boolean;

.field public app_connected_terminal:Ljava/lang/Boolean;

.field public app_dip_cof_post_payment:Ljava/lang/Boolean;

.field public app_fee_tutorial:Ljava/lang/Boolean;

.field public app_first_payment_tutorial_v2:Ljava/lang/Boolean;

.field public app_terminal_api_spm:Ljava/lang/Boolean;

.field public app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

.field public can_show_fees:Ljava/lang/Boolean;

.field public dip_card_for_customer_cof:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12493
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public app_can_order_r12(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12527
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r12:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_can_order_r4(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12532
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r4:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_can_order_r41(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12542
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r41:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_can_show_account_freeze(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12600
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_can_show_account_freeze_banner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12618
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze_banner:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_can_show_account_freeze_notifications(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12609
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_can_show_cnp_fees(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12497
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_cnp_fees:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_can_show_cp_pricing_change(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12626
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_cp_pricing_change:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_can_show_disputes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12560
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_disputes:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_can_show_disputes_challenge_flow(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12570
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_can_show_order_reader_menu(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12522
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_order_reader_menu:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_collect_cnp_postal_code(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12591
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_collect_cnp_postal_code:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_collect_cof_postal_code(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12583
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_collect_cof_postal_code:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_connected_terminal(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12575
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_connected_terminal:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_dip_cof_post_payment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12517
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_dip_cof_post_payment:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_fee_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12502
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_fee_tutorial:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_first_payment_tutorial_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12552
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_first_payment_tutorial_v2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_terminal_api_spm(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12634
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_terminal_api_spm:Ljava/lang/Boolean;

    return-object p0
.end method

.method public app_x2_dip_card_for_customer_cof(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12547
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;
    .locals 2

    .line 12640
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;-><init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 12450
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object v0

    return-object v0
.end method

.method public can_show_fees(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12507
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->can_show_fees:Ljava/lang/Boolean;

    return-object p0
.end method

.method public dip_card_for_customer_cof(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    .line 12512
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->dip_card_for_customer_cof:Ljava/lang/Boolean;

    return-object p0
.end method
