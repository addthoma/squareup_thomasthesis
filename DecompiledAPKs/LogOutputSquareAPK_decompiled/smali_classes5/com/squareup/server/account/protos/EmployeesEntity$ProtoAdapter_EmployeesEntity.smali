.class final Lcom/squareup/server/account/protos/EmployeesEntity$ProtoAdapter_EmployeesEntity;
.super Lcom/squareup/wire/ProtoAdapter;
.source "EmployeesEntity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/EmployeesEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_EmployeesEntity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/EmployeesEntity;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 768
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/EmployeesEntity;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/EmployeesEntity;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 809
    new-instance v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;-><init>()V

    .line 810
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 811
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 827
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 825
    :pswitch_0
    iget-object v3, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->employee_role_tokens:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 824
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_owner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    goto :goto_0

    .line 823
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_account_owner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    goto :goto_0

    .line 822
    :pswitch_3
    sget-object v3, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->current_timecard(Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    goto :goto_0

    .line 821
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_track_time(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    goto :goto_0

    .line 820
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_access_register_with_passcode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    goto :goto_0

    .line 819
    :pswitch_6
    iget-object v3, v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->permissions:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 818
    :pswitch_7
    sget-object v3, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->passcode_only_credential(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    goto :goto_0

    .line 817
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->active(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    goto :goto_0

    .line 816
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->token(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    goto/16 :goto_0

    .line 815
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->employee_number(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    goto/16 :goto_0

    .line 814
    :pswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->last_name(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    goto/16 :goto_0

    .line 813
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->first_name(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    goto/16 :goto_0

    .line 831
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 832
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 766
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/EmployeesEntity$ProtoAdapter_EmployeesEntity;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/EmployeesEntity;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 791
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/EmployeesEntity;->first_name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 792
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/EmployeesEntity;->last_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 793
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_number:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 794
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/EmployeesEntity;->token:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 795
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/EmployeesEntity;->active:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 796
    sget-object v0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 797
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/EmployeesEntity;->permissions:Ljava/util/List;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 798
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/EmployeesEntity;->can_access_register_with_passcode:Ljava/lang/Boolean;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 799
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/EmployeesEntity;->can_track_time:Ljava/lang/Boolean;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 800
    sget-object v0, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 801
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/EmployeesEntity;->is_account_owner:Ljava/lang/Boolean;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 802
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/EmployeesEntity;->is_owner:Ljava/lang/Boolean;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 803
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_role_tokens:Ljava/util/List;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 804
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/EmployeesEntity;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 766
    check-cast p2, Lcom/squareup/server/account/protos/EmployeesEntity;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/EmployeesEntity$ProtoAdapter_EmployeesEntity;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/EmployeesEntity;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/EmployeesEntity;)I
    .locals 4

    .line 773
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->first_name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->last_name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 774
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_number:Ljava/lang/String;

    const/4 v3, 0x3

    .line 775
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->token:Ljava/lang/String;

    const/4 v3, 0x4

    .line 776
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->active:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 777
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    const/4 v3, 0x6

    .line 778
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 779
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->permissions:Ljava/util/List;

    const/4 v3, 0x7

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->can_access_register_with_passcode:Ljava/lang/Boolean;

    const/16 v3, 0x8

    .line 780
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->can_track_time:Ljava/lang/Boolean;

    const/16 v3, 0x9

    .line 781
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    const/16 v3, 0xa

    .line 782
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->is_account_owner:Ljava/lang/Boolean;

    const/16 v3, 0xb

    .line 783
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->is_owner:Ljava/lang/Boolean;

    const/16 v3, 0xc

    .line 784
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 785
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_role_tokens:Ljava/util/List;

    const/16 v3, 0xd

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 786
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/EmployeesEntity;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 766
    check-cast p1, Lcom/squareup/server/account/protos/EmployeesEntity;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/EmployeesEntity$ProtoAdapter_EmployeesEntity;->encodedSize(Lcom/squareup/server/account/protos/EmployeesEntity;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/server/account/protos/EmployeesEntity;
    .locals 2

    .line 837
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/EmployeesEntity;->newBuilder()Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 838
    iput-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->first_name:Ljava/lang/String;

    .line 839
    iput-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->last_name:Ljava/lang/String;

    .line 840
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    iput-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    .line 841
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    iput-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    .line 842
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 843
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 766
    check-cast p1, Lcom/squareup/server/account/protos/EmployeesEntity;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/EmployeesEntity$ProtoAdapter_EmployeesEntity;->redact(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object p1

    return-object p1
.end method
