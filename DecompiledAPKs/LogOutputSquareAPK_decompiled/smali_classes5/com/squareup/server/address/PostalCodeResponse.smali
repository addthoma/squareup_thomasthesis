.class public Lcom/squareup/server/address/PostalCodeResponse;
.super Lcom/squareup/server/SimpleResponse;
.source "PostalCodeResponse.java"


# instance fields
.field private final results:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/address/PostalLocation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 16
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/server/address/PostalCodeResponse;-><init>(Ljava/util/List;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/address/PostalLocation;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    .line 21
    invoke-direct {p0, v0}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    .line 22
    iput-object p1, p0, Lcom/squareup/server/address/PostalCodeResponse;->results:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getAllResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/address/PostalLocation;",
            ">;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/server/address/PostalCodeResponse;->results:Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getDefaultCityIndex()I
    .locals 3

    .line 39
    invoke-virtual {p0}, Lcom/squareup/server/address/PostalCodeResponse;->getAllResults()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    .line 40
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 41
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/address/PostalLocation;

    .line 42
    iget-boolean v2, v2, Lcom/squareup/server/address/PostalLocation;->default_city:Z

    if-eqz v2, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public isSuccessful()Z
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/server/address/PostalCodeResponse;->results:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
