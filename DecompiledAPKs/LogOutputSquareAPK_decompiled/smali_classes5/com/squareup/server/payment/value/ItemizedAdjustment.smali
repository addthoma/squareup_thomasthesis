.class public Lcom/squareup/server/payment/value/ItemizedAdjustment;
.super Ljava/lang/Object;
.source "ItemizedAdjustment.java"


# instance fields
.field private final applied:Lcom/squareup/protos/common/Money;

.field private final id:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/server/payment/value/ItemizedAdjustment;->id:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/squareup/server/payment/value/ItemizedAdjustment;->applied:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 40
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 42
    :cond_1
    check-cast p1, Lcom/squareup/server/payment/value/ItemizedAdjustment;

    .line 44
    iget-object v2, p0, Lcom/squareup/server/payment/value/ItemizedAdjustment;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/value/ItemizedAdjustment;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/value/ItemizedAdjustment;->applied:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/server/payment/value/ItemizedAdjustment;->applied:Lcom/squareup/protos/common/Money;

    invoke-static {v2, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public getApplied()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/server/payment/value/ItemizedAdjustment;->applied:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/server/payment/value/ItemizedAdjustment;->id:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 49
    iget-object v1, p0, Lcom/squareup/server/payment/value/ItemizedAdjustment;->id:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/value/ItemizedAdjustment;->applied:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ItemizedAdjustment{applied_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/payment/value/ItemizedAdjustment;->applied:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/payment/value/ItemizedAdjustment;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
