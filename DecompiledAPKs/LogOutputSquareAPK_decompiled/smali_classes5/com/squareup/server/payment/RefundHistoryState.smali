.class public final enum Lcom/squareup/server/payment/RefundHistoryState;
.super Ljava/lang/Enum;
.source "RefundHistoryState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/payment/RefundHistoryState$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/server/payment/RefundHistoryState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u000c\u0008\u0086\u0001\u0018\u0000 \u000c2\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000b\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/server/payment/RefundHistoryState;",
        "",
        "(Ljava/lang/String;I)V",
        "UNKNOWN",
        "NEW",
        "PENDING",
        "REQUESTED",
        "COMPLETED",
        "APPROVED",
        "SUCCESS",
        "REJECTED",
        "FAILED",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/server/payment/RefundHistoryState;

.field public static final enum APPROVED:Lcom/squareup/server/payment/RefundHistoryState;

.field public static final enum COMPLETED:Lcom/squareup/server/payment/RefundHistoryState;

.field public static final Companion:Lcom/squareup/server/payment/RefundHistoryState$Companion;

.field public static final enum FAILED:Lcom/squareup/server/payment/RefundHistoryState;

.field public static final enum NEW:Lcom/squareup/server/payment/RefundHistoryState;

.field public static final enum PENDING:Lcom/squareup/server/payment/RefundHistoryState;

.field public static final enum REJECTED:Lcom/squareup/server/payment/RefundHistoryState;

.field public static final enum REQUESTED:Lcom/squareup/server/payment/RefundHistoryState;

.field public static final enum SUCCESS:Lcom/squareup/server/payment/RefundHistoryState;

.field public static final enum UNKNOWN:Lcom/squareup/server/payment/RefundHistoryState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/server/payment/RefundHistoryState;

    new-instance v1, Lcom/squareup/server/payment/RefundHistoryState;

    const/4 v2, 0x0

    const-string v3, "UNKNOWN"

    invoke-direct {v1, v3, v2}, Lcom/squareup/server/payment/RefundHistoryState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/server/payment/RefundHistoryState;->UNKNOWN:Lcom/squareup/server/payment/RefundHistoryState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/server/payment/RefundHistoryState;

    const/4 v2, 0x1

    const-string v3, "NEW"

    invoke-direct {v1, v3, v2}, Lcom/squareup/server/payment/RefundHistoryState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/server/payment/RefundHistoryState;->NEW:Lcom/squareup/server/payment/RefundHistoryState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/server/payment/RefundHistoryState;

    const/4 v2, 0x2

    const-string v3, "PENDING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/server/payment/RefundHistoryState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/server/payment/RefundHistoryState;->PENDING:Lcom/squareup/server/payment/RefundHistoryState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/server/payment/RefundHistoryState;

    const/4 v2, 0x3

    const-string v3, "REQUESTED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/server/payment/RefundHistoryState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/server/payment/RefundHistoryState;->REQUESTED:Lcom/squareup/server/payment/RefundHistoryState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/server/payment/RefundHistoryState;

    const/4 v2, 0x4

    const-string v3, "COMPLETED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/server/payment/RefundHistoryState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/server/payment/RefundHistoryState;->COMPLETED:Lcom/squareup/server/payment/RefundHistoryState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/server/payment/RefundHistoryState;

    const/4 v2, 0x5

    const-string v3, "APPROVED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/server/payment/RefundHistoryState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/server/payment/RefundHistoryState;->APPROVED:Lcom/squareup/server/payment/RefundHistoryState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/server/payment/RefundHistoryState;

    const/4 v2, 0x6

    const-string v3, "SUCCESS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/server/payment/RefundHistoryState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/server/payment/RefundHistoryState;->SUCCESS:Lcom/squareup/server/payment/RefundHistoryState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/server/payment/RefundHistoryState;

    const/4 v2, 0x7

    const-string v3, "REJECTED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/server/payment/RefundHistoryState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/server/payment/RefundHistoryState;->REJECTED:Lcom/squareup/server/payment/RefundHistoryState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/server/payment/RefundHistoryState;

    const/16 v2, 0x8

    const-string v3, "FAILED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/server/payment/RefundHistoryState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/server/payment/RefundHistoryState;->FAILED:Lcom/squareup/server/payment/RefundHistoryState;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/server/payment/RefundHistoryState;->$VALUES:[Lcom/squareup/server/payment/RefundHistoryState;

    new-instance v0, Lcom/squareup/server/payment/RefundHistoryState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/server/payment/RefundHistoryState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/server/payment/RefundHistoryState;->Companion:Lcom/squareup/server/payment/RefundHistoryState$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/server/payment/RefundHistoryState;
    .locals 1

    const-class v0, Lcom/squareup/server/payment/RefundHistoryState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/payment/RefundHistoryState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/server/payment/RefundHistoryState;
    .locals 1

    sget-object v0, Lcom/squareup/server/payment/RefundHistoryState;->$VALUES:[Lcom/squareup/server/payment/RefundHistoryState;

    invoke-virtual {v0}, [Lcom/squareup/server/payment/RefundHistoryState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/server/payment/RefundHistoryState;

    return-object v0
.end method
