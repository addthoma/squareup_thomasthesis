.class public Lcom/squareup/server/payment/CaptureMessage;
.super Ljava/lang/Object;
.source "CaptureMessage.java"


# instance fields
.field public final amount_cents:J

.field public final authorization_id:Ljava/lang/String;

.field public final capture_type:Lcom/squareup/server/payment/CaptureType;

.field public final currency_code:Ljava/lang/String;

.field public final customer_presence:Lcom/squareup/server/payment/CustomerPresence;

.field public final line_items:Lcom/squareup/server/payment/ItemizationsMessage;

.field public final tax_cents:J

.field public final timestamp:Ljava/lang/String;

.field public final tip_amount_cents:J


# direct methods
.method public constructor <init>(JLjava/lang/String;Lcom/squareup/server/payment/CaptureType;Ljava/lang/String;Lcom/squareup/server/payment/CustomerPresence;Lcom/squareup/server/payment/ItemizationsMessage;Ljava/lang/String;JJ)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-wide p1, p0, Lcom/squareup/server/payment/CaptureMessage;->amount_cents:J

    .line 19
    iput-object p3, p0, Lcom/squareup/server/payment/CaptureMessage;->authorization_id:Ljava/lang/String;

    .line 20
    iput-object p4, p0, Lcom/squareup/server/payment/CaptureMessage;->capture_type:Lcom/squareup/server/payment/CaptureType;

    .line 21
    iput-object p5, p0, Lcom/squareup/server/payment/CaptureMessage;->currency_code:Ljava/lang/String;

    .line 22
    iput-object p6, p0, Lcom/squareup/server/payment/CaptureMessage;->customer_presence:Lcom/squareup/server/payment/CustomerPresence;

    .line 23
    iput-object p7, p0, Lcom/squareup/server/payment/CaptureMessage;->line_items:Lcom/squareup/server/payment/ItemizationsMessage;

    .line 24
    iput-object p8, p0, Lcom/squareup/server/payment/CaptureMessage;->timestamp:Ljava/lang/String;

    .line 25
    iput-wide p9, p0, Lcom/squareup/server/payment/CaptureMessage;->tax_cents:J

    .line 26
    iput-wide p11, p0, Lcom/squareup/server/payment/CaptureMessage;->tip_amount_cents:J

    return-void
.end method
