.class public interface abstract Lcom/squareup/server/reporting/TransferReportsService;
.super Ljava/lang/Object;
.source "TransferReportsService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\tH\'J\u0018\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u000cH\'J\u0018\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u000fH\'\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/server/reporting/TransferReportsService;",
        "",
        "getPendingSettlements",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;",
        "request",
        "Lcom/squareup/protos/client/settlements/PendingSettlementsRequest;",
        "getSettledBillEntries",
        "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;",
        "Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest;",
        "getSettlementReport",
        "Lcom/squareup/protos/client/settlements/SettlementReportResponse;",
        "Lcom/squareup/protos/client/settlements/SettlementReportRequest;",
        "getSettlementReportLite",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getPendingSettlements(Lcom/squareup/protos/client/settlements/PendingSettlementsRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/settlements/PendingSettlementsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/settlements/PendingSettlementsRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/settlements/pending-settlements"
    .end annotation
.end method

.method public abstract getSettledBillEntries(Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/settlements/settled-bill-entries"
    .end annotation
.end method

.method public abstract getSettlementReport(Lcom/squareup/protos/client/settlements/SettlementReportRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/settlements/SettlementReportRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/settlements/SettlementReportRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/settlements/SettlementReportResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/settlements/settlement-report"
    .end annotation
.end method

.method public abstract getSettlementReportLite(Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/settlements/settlement-report-lite"
    .end annotation
.end method
