.class public final enum Lcom/squareup/server/onboard/Image$Size;
.super Ljava/lang/Enum;
.source "PanelComponents.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/onboard/Image;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Size"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/server/onboard/Image$Size;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0007\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/server/onboard/Image$Size;",
        "",
        "(Ljava/lang/String;I)V",
        "SMALL",
        "MEDIUM",
        "LARGE",
        "ORIGINAL",
        "UNKNOWN",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/server/onboard/Image$Size;

.field public static final enum LARGE:Lcom/squareup/server/onboard/Image$Size;

.field public static final enum MEDIUM:Lcom/squareup/server/onboard/Image$Size;

.field public static final enum ORIGINAL:Lcom/squareup/server/onboard/Image$Size;

.field public static final enum SMALL:Lcom/squareup/server/onboard/Image$Size;

.field public static final enum UNKNOWN:Lcom/squareup/server/onboard/Image$Size;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/server/onboard/Image$Size;

    new-instance v1, Lcom/squareup/server/onboard/Image$Size;

    const/4 v2, 0x0

    const-string v3, "SMALL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/server/onboard/Image$Size;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/server/onboard/Image$Size;->SMALL:Lcom/squareup/server/onboard/Image$Size;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/server/onboard/Image$Size;

    const/4 v2, 0x1

    const-string v3, "MEDIUM"

    invoke-direct {v1, v3, v2}, Lcom/squareup/server/onboard/Image$Size;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/server/onboard/Image$Size;->MEDIUM:Lcom/squareup/server/onboard/Image$Size;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/server/onboard/Image$Size;

    const/4 v2, 0x2

    const-string v3, "LARGE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/server/onboard/Image$Size;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/server/onboard/Image$Size;->LARGE:Lcom/squareup/server/onboard/Image$Size;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/server/onboard/Image$Size;

    const/4 v2, 0x3

    const-string v3, "ORIGINAL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/server/onboard/Image$Size;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/server/onboard/Image$Size;->ORIGINAL:Lcom/squareup/server/onboard/Image$Size;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/server/onboard/Image$Size;

    const/4 v2, 0x4

    const-string v3, "UNKNOWN"

    invoke-direct {v1, v3, v2}, Lcom/squareup/server/onboard/Image$Size;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/server/onboard/Image$Size;->UNKNOWN:Lcom/squareup/server/onboard/Image$Size;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/server/onboard/Image$Size;->$VALUES:[Lcom/squareup/server/onboard/Image$Size;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 119
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/server/onboard/Image$Size;
    .locals 1

    const-class v0, Lcom/squareup/server/onboard/Image$Size;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/onboard/Image$Size;

    return-object p0
.end method

.method public static values()[Lcom/squareup/server/onboard/Image$Size;
    .locals 1

    sget-object v0, Lcom/squareup/server/onboard/Image$Size;->$VALUES:[Lcom/squareup/server/onboard/Image$Size;

    invoke-virtual {v0}, [Lcom/squareup/server/onboard/Image$Size;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/server/onboard/Image$Size;

    return-object v0
.end method
