.class public interface abstract Lcom/squareup/server/ReportCoredumpService;
.super Ljava/lang/Object;
.source "ReportCoredumpService.java"


# virtual methods
.method public abstract send(Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/tarkin/ReportCoredumpResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/reader/report-coredump"
    .end annotation
.end method
