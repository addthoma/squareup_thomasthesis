.class public interface abstract Lcom/squareup/server/catalog/CatalogConnectV2Service;
.super Ljava/lang/Object;
.source "CatalogConnectV2Service.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/catalog/CatalogConnectV2Service$SearchCatalogObjectsStandardResponse;,
        Lcom/squareup/server/catalog/CatalogConnectV2Service$UpsertCatalogObjectStandardResponse;,
        Lcom/squareup/server/catalog/CatalogConnectV2Service$BatchUpsertCatalogObjectStandardResponse;,
        Lcom/squareup/server/catalog/CatalogConnectV2Service$DeleteCatalogObjectStandardResponse;,
        Lcom/squareup/server/catalog/CatalogConnectV2Service$BatchRetrieveCatalogObjectsStandardResponse;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008f\u0018\u00002\u00020\u0001:\u0005\u0013\u0014\u0015\u0016\u0017J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005H\'J\u0012\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0008H\'J\u0012\u0010\t\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u000cH\'J\u0012\u0010\r\u001a\u00020\u000e2\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u000fH\'J\u0012\u0010\u0010\u001a\u00020\u00112\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0012H\'\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/server/catalog/CatalogConnectV2Service;",
        "",
        "batchRetrieve",
        "Lcom/squareup/server/catalog/CatalogConnectV2Service$BatchRetrieveCatalogObjectsStandardResponse;",
        "request",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;",
        "batchUpsert",
        "Lcom/squareup/server/catalog/CatalogConnectV2Service$BatchUpsertCatalogObjectStandardResponse;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;",
        "delete",
        "Lcom/squareup/server/catalog/CatalogConnectV2Service$DeleteCatalogObjectStandardResponse;",
        "objectId",
        "",
        "search",
        "Lcom/squareup/server/catalog/CatalogConnectV2Service$SearchCatalogObjectsStandardResponse;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;",
        "upsert",
        "Lcom/squareup/server/catalog/CatalogConnectV2Service$UpsertCatalogObjectStandardResponse;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;",
        "BatchRetrieveCatalogObjectsStandardResponse",
        "BatchUpsertCatalogObjectStandardResponse",
        "DeleteCatalogObjectStandardResponse",
        "SearchCatalogObjectsStandardResponse",
        "UpsertCatalogObjectStandardResponse",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract batchRetrieve(Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;)Lcom/squareup/server/catalog/CatalogConnectV2Service$BatchRetrieveCatalogObjectsStandardResponse;
    .param p1    # Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/v2/catalog/frontend/batch-retrieve"
    .end annotation
.end method

.method public abstract batchUpsert(Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;)Lcom/squareup/server/catalog/CatalogConnectV2Service$BatchUpsertCatalogObjectStandardResponse;
    .param p1    # Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/v2/catalog/frontend/batch-upsert"
    .end annotation
.end method

.method public abstract delete(Ljava/lang/String;)Lcom/squareup/server/catalog/CatalogConnectV2Service$DeleteCatalogObjectStandardResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Path;
            value = "object_id"
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/DELETE;
        value = "/v2/catalog/frontend/object/{object_id}"
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/x-protobuf"
        }
    .end annotation
.end method

.method public abstract search(Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;)Lcom/squareup/server/catalog/CatalogConnectV2Service$SearchCatalogObjectsStandardResponse;
    .param p1    # Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/v2/catalog/frontend/search"
    .end annotation
.end method

.method public abstract upsert(Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;)Lcom/squareup/server/catalog/CatalogConnectV2Service$UpsertCatalogObjectStandardResponse;
    .param p1    # Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/v2/catalog/frontend/object"
    .end annotation
.end method
