.class public interface abstract Lcom/squareup/server/employees/TimecardsService;
.super Ljava/lang/Object;
.source "TimecardsService.java"


# virtual methods
.method public abstract getEmployeeJobInfos(Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/employeejobs/get-employee-job-infos"
    .end annotation
.end method

.method public abstract getOpenTimecardAndBreakResponse(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/timecards/get-open-timecard-and-break"
    .end annotation
.end method

.method public abstract listTimecardBreakDefinitions(Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/timecards/list-timecard-break-definitions"
    .end annotation
.end method

.method public abstract startBreak(Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/timecards/start-timecard-break"
    .end annotation
.end method

.method public abstract startTimecard(Lcom/squareup/protos/client/timecards/StartTimecardRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/timecards/StartTimecardRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/timecards/StartTimecardRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/timecards/StartTimecardResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/timecards/start-timecard"
    .end annotation
.end method

.method public abstract stopBreak(Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/timecards/stop-timecard-break"
    .end annotation
.end method

.method public abstract stopTimecard(Lcom/squareup/protos/client/timecards/StopTimecardRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/timecards/StopTimecardRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/timecards/StopTimecardRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/timecards/StopTimecardResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/timecards/stop-timecard"
    .end annotation
.end method

.method public abstract switchJobs(Lcom/squareup/protos/client/timecards/SwitchJobsRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/timecards/SwitchJobsRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/timecards/SwitchJobsRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/timecards/SwitchJobsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/timecards/switch-jobs"
    .end annotation
.end method

.method public abstract updateNotes(Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/timecards/UpdateTimecardNotesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/timecards/update-timecard-notes"
    .end annotation
.end method
