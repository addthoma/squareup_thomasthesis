.class public Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;
.super Ljava/lang/Object;
.source "ClockInOutResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/employees/ClockInOutResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TimeCard"
.end annotation


# instance fields
.field public final clockin_time:Ljava/lang/String;

.field public final clockout_time:Ljava/lang/String;

.field public final id:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;->id:Ljava/lang/String;

    .line 23
    iput-object p2, p0, Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;->clockin_time:Ljava/lang/String;

    .line 24
    iput-object p3, p0, Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;->clockout_time:Ljava/lang/String;

    return-void
.end method
