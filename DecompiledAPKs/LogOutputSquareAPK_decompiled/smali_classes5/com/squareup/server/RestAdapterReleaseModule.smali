.class public Lcom/squareup/server/RestAdapterReleaseModule;
.super Ljava/lang/Object;
.source "RestAdapterReleaseModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCogsServiceCreator(Lretrofit/RestAdapter;)Lcom/squareup/api/ServiceCreator;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/server/RestServiceCreator;

    invoke-direct {v0, p0}, Lcom/squareup/server/RestServiceCreator;-><init>(Lretrofit/RestAdapter;)V

    return-object v0
.end method

.method static provideGsonServiceCreator(Lretrofit/RestAdapter;)Lcom/squareup/api/ServiceCreator;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 17
    new-instance v0, Lcom/squareup/server/RestServiceCreator;

    invoke-direct {v0, p0}, Lcom/squareup/server/RestServiceCreator;-><init>(Lretrofit/RestAdapter;)V

    return-object v0
.end method

.method static provideProtoServiceCreator(Lretrofit/RestAdapter;)Lcom/squareup/api/ServiceCreator;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/server/RestServiceCreator;

    invoke-direct {v0, p0}, Lcom/squareup/server/RestServiceCreator;-><init>(Lretrofit/RestAdapter;)V

    return-object v0
.end method

.method static provideUnauthenticatedGsonServiceCreator(Lretrofit/RestAdapter;)Lcom/squareup/api/ServiceCreator;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 23
    new-instance v0, Lcom/squareup/server/RestServiceCreator;

    invoke-direct {v0, p0}, Lcom/squareup/server/RestServiceCreator;-><init>(Lretrofit/RestAdapter;)V

    return-object v0
.end method

.method static provideUnauthenticatedProtoServiceCreator(Lretrofit/RestAdapter;)Lcom/squareup/api/ServiceCreator;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/server/RestServiceCreator;

    invoke-direct {v0, p0}, Lcom/squareup/server/RestServiceCreator;-><init>(Lretrofit/RestAdapter;)V

    return-object v0
.end method
