.class public abstract Lcom/squareup/server/SquareCallback;
.super Ljava/lang/Object;
.source "SquareCallback.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lretrofit/Callback<",
        "TT;>;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+TT;>;)V"
        }
    .end annotation

    .line 79
    new-instance v0, Lcom/squareup/server/-$$Lambda$0YbinRpObDiY-kFvkoFSZUY7e7g;

    invoke-direct {v0, p0}, Lcom/squareup/server/-$$Lambda$0YbinRpObDiY-kFvkoFSZUY7e7g;-><init>(Lcom/squareup/server/SquareCallback;)V

    new-instance v1, Lcom/squareup/server/-$$Lambda$SquareCallback$_dz7kdVgCMYFkGEiNB-__7azy0Q;

    invoke-direct {v1, p0}, Lcom/squareup/server/-$$Lambda$SquareCallback$_dz7kdVgCMYFkGEiNB-__7azy0Q;-><init>(Lcom/squareup/server/SquareCallback;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public abstract call(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public abstract clientError(Ljava/lang/Object;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation
.end method

.method public final failure(Lretrofit/RetrofitError;)V
    .locals 3

    .line 33
    :try_start_0
    invoke-static {p1}, Lcom/squareup/receiving/ReceivedResponseRetrofit1;->fromRetrofitError1(Lretrofit/RetrofitError;)Lcom/squareup/receiving/ReceivedResponse$Error;

    move-result-object p1

    .line 34
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_0

    .line 35
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    .line 36
    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getResponse()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getStatusCode()I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lcom/squareup/server/SquareCallback;->clientError(Ljava/lang/Object;I)V

    goto :goto_0

    .line 37
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz v0, :cond_1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/server/SquareCallback;->sessionExpired()V

    goto :goto_0

    .line 39
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_2

    .line 40
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->getStatusCode()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/server/SquareCallback;->serverError(I)V

    goto :goto_0

    .line 41
    :cond_2
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_3

    .line 42
    invoke-virtual {p0}, Lcom/squareup/server/SquareCallback;->networkError()V

    goto :goto_0

    .line 44
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected response "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    .line 47
    invoke-virtual {p0, p1}, Lcom/squareup/server/SquareCallback;->unexpectedError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$accept$0$SquareCallback(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 82
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 83
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_0

    .line 84
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 85
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz v0, :cond_1

    .line 86
    invoke-virtual {p0}, Lcom/squareup/server/SquareCallback;->sessionExpired()V

    goto :goto_0

    .line 87
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_2

    .line 88
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    .line 89
    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getResponse()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getStatusCode()I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lcom/squareup/server/SquareCallback;->clientError(Ljava/lang/Object;I)V

    goto :goto_0

    .line 90
    :cond_2
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_3

    .line 91
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->getStatusCode()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/server/SquareCallback;->serverError(I)V

    goto :goto_0

    .line 92
    :cond_3
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_4

    .line 93
    invoke-virtual {p0}, Lcom/squareup/server/SquareCallback;->networkError()V

    :goto_0
    return-void

    .line 95
    :cond_4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Missing implementation for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract networkError()V
.end method

.method public abstract serverError(I)V
.end method

.method public abstract sessionExpired()V
.end method

.method public final success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lretrofit/client/Response;",
            ")V"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 25
    invoke-virtual {p2}, Lretrofit/client/Response;->getStatus()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/server/SquareCallback;->serverError(I)V

    goto :goto_0

    .line 27
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public unexpectedError(Ljava/lang/Throwable;)V
    .locals 2

    .line 71
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected callback error"

    invoke-direct {v0, v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method
