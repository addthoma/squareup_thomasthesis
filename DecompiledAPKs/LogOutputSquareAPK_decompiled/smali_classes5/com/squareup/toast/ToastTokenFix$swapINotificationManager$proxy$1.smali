.class final Lcom/squareup/toast/ToastTokenFix$swapINotificationManager$proxy$1;
.super Ljava/lang/Object;
.source "ToastTokenFix.kt"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/toast/ToastTokenFix;->swapINotificationManager(Lcom/squareup/toast/ToastTokenFix$ToastTnField;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000e\u0010\u0003\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000e\u0010\u0004\u001a\n \u0002*\u0004\u0018\u00010\u00050\u00052,\u0010\u0006\u001a(\u0012\u000c\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001 \u0002*\u0014\u0012\u000e\u0008\u0001\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0018\u00010\u00070\u0007H\n\u00a2\u0006\u0004\u0008\u0008\u0010\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 0>",
        "method",
        "Ljava/lang/reflect/Method;",
        "args",
        "",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $enqueueToastMethod:Ljava/lang/reflect/Method;

.field final synthetic $fieldWrapper:Lcom/squareup/toast/ToastTokenFix$ToastTnField;

.field final synthetic $realService:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/reflect/Method;Lcom/squareup/toast/ToastTokenFix$ToastTnField;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/toast/ToastTokenFix$swapINotificationManager$proxy$1;->$enqueueToastMethod:Ljava/lang/reflect/Method;

    iput-object p2, p0, Lcom/squareup/toast/ToastTokenFix$swapINotificationManager$proxy$1;->$fieldWrapper:Lcom/squareup/toast/ToastTokenFix$ToastTnField;

    iput-object p3, p0, Lcom/squareup/toast/ToastTokenFix$swapINotificationManager$proxy$1;->$realService:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 138
    iget-object p1, p0, Lcom/squareup/toast/ToastTokenFix$swapINotificationManager$proxy$1;->$enqueueToastMethod:Ljava/lang/reflect/Method;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 139
    aget-object p1, p3, p1

    .line 140
    iget-object v0, p0, Lcom/squareup/toast/ToastTokenFix$swapINotificationManager$proxy$1;->$fieldWrapper:Lcom/squareup/toast/ToastTokenFix$ToastTnField;

    invoke-virtual {v0, p1}, Lcom/squareup/toast/ToastTokenFix$ToastTnField;->swallowBadTokenException(Ljava/lang/Object;)V

    :cond_0
    if-eqz p3, :cond_1

    .line 143
    iget-object p1, p0, Lcom/squareup/toast/ToastTokenFix$swapINotificationManager$proxy$1;->$realService:Ljava/lang/Object;

    array-length v0, p3

    invoke-static {p3, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p2, p1, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 145
    :cond_1
    iget-object p1, p0, Lcom/squareup/toast/ToastTokenFix$swapINotificationManager$proxy$1;->$realService:Ljava/lang/Object;

    const/4 p3, 0x0

    new-array p3, p3, [Ljava/lang/Object;

    invoke-virtual {p2, p1, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    :goto_0
    return-object p1
.end method
