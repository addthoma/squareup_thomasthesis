.class public final Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider_Factory;
.super Ljava/lang/Object;
.source "SposReceiptAutoCloseProvider_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;)",
            "Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider_Factory;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/api/ApiTransactionState;)Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider;

    invoke-direct {v0, p0}, Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider;-><init>(Lcom/squareup/api/ApiTransactionState;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ApiTransactionState;

    invoke-static {v0}, Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider_Factory;->newInstance(Lcom/squareup/api/ApiTransactionState;)Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider_Factory;->get()Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider;

    move-result-object v0

    return-object v0
.end method
