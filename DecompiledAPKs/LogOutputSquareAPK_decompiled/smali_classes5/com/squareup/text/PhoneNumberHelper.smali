.class public interface abstract Lcom/squareup/text/PhoneNumberHelper;
.super Ljava/lang/Object;
.source "PhoneNumberHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0003H&J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0003H&J\u0012\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0003H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/text/PhoneNumberHelper;",
        "",
        "format",
        "",
        "rawText",
        "formatPartial",
        "isValid",
        "",
        "input",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract format(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract formatPartial(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract isValid(Ljava/lang/String;)Z
.end method
