.class public Lcom/squareup/text/AutoFitTextWatcher;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "AutoFitTextWatcher.java"


# instance fields
.field private onPreDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

.field private final view:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/widget/TextView;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/text/AutoFitTextWatcher;->view:Landroid/widget/TextView;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/text/AutoFitTextWatcher;)Landroid/widget/TextView;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/squareup/text/AutoFitTextWatcher;->view:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$102(Lcom/squareup/text/AutoFitTextWatcher;Landroid/view/ViewTreeObserver$OnPreDrawListener;)Landroid/view/ViewTreeObserver$OnPreDrawListener;
    .locals 0

    .line 11
    iput-object p1, p0, Lcom/squareup/text/AutoFitTextWatcher;->onPreDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    return-object p1
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .line 21
    invoke-virtual {p0}, Lcom/squareup/text/AutoFitTextWatcher;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 25
    iget-object v0, p0, Lcom/squareup/text/AutoFitTextWatcher;->view:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 27
    iget-object v1, p0, Lcom/squareup/text/AutoFitTextWatcher;->onPreDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    if-eqz v1, :cond_0

    .line 28
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 31
    :cond_0
    new-instance v1, Lcom/squareup/text/AutoFitTextWatcher$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/text/AutoFitTextWatcher$1;-><init>(Lcom/squareup/text/AutoFitTextWatcher;Landroid/text/Editable;)V

    iput-object v1, p0, Lcom/squareup/text/AutoFitTextWatcher;->onPreDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 48
    iget-object p1, p0, Lcom/squareup/text/AutoFitTextWatcher;->onPreDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v0, p1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    return-void

    .line 54
    :cond_1
    iget-object v1, p0, Lcom/squareup/text/AutoFitTextWatcher;->view:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/squareup/text/AutoFitTextWatcher;->autoFitText(Landroid/graphics/Rect;Landroid/text/Editable;)F

    move-result p1

    invoke-virtual {v1, v2, p1}, Landroid/widget/TextView;->setTextSize(IF)V

    return-void
.end method

.method protected autoFitText(Landroid/graphics/Rect;Landroid/text/Editable;)F
    .locals 3

    .line 58
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    .line 59
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p2, "W"

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/squareup/text/AutoFitTextWatcher;->view:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "l"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, p2, p1, v1}, Lcom/squareup/text/Fonts;->autoFitText(Landroid/text/TextPaint;Ljava/lang/String;Landroid/graphics/Rect;F)V

    .line 66
    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextSize()F

    move-result p2

    .line 67
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iget p1, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, p1

    int-to-float p1, v1

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float p1, p1, v1

    .line 68
    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextSize()F

    move-result v0

    cmpl-float v0, v0, p1

    if-lez v0, :cond_1

    goto :goto_0

    :cond_1
    move p1, p2

    :goto_0
    return p1
.end method

.method public getBounds()Landroid/graphics/Rect;
    .locals 6

    .line 80
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/squareup/text/AutoFitTextWatcher;->view:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/text/AutoFitTextWatcher;->view:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/squareup/text/AutoFitTextWatcher;->view:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/text/AutoFitTextWatcher;->view:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/squareup/text/AutoFitTextWatcher;->view:Landroid/widget/TextView;

    .line 81
    invoke-virtual {v3}, Landroid/widget/TextView;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/squareup/text/AutoFitTextWatcher;->view:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/squareup/text/AutoFitTextWatcher;->view:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getBottom()I

    move-result v4

    iget-object v5, p0, Lcom/squareup/text/AutoFitTextWatcher;->view:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method protected getView()Landroid/widget/TextView;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/text/AutoFitTextWatcher;->view:Landroid/widget/TextView;

    return-object v0
.end method
