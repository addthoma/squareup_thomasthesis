.class public Lcom/squareup/text/NumberedListItemSpan;
.super Lcom/squareup/text/ListItemSpan;
.source "NumberedListItemSpan.java"


# instance fields
.field private final number:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .line 8
    invoke-direct {p0, p2}, Lcom/squareup/text/ListItemSpan;-><init>(I)V

    .line 9
    iput p1, p0, Lcom/squareup/text/NumberedListItemSpan;->number:I

    return-void
.end method


# virtual methods
.method protected getListMarker()Ljava/lang/String;
    .locals 2

    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/squareup/text/NumberedListItemSpan;->number:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
