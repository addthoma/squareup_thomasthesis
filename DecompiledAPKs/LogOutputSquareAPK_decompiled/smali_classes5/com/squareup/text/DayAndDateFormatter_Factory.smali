.class public final Lcom/squareup/text/DayAndDateFormatter_Factory;
.super Ljava/lang/Object;
.source "DayAndDateFormatter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/text/DayAndDateFormatter;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/text/DayAndDateFormatter_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/text/DayAndDateFormatter_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/text/DayAndDateFormatter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/text/DayAndDateFormatter_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/text/DayAndDateFormatter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;)",
            "Lcom/squareup/text/DayAndDateFormatter_Factory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/text/DayAndDateFormatter_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/text/DayAndDateFormatter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Clock;Ljavax/inject/Provider;Lcom/squareup/util/Res;Ljava/text/DateFormat;)Lcom/squareup/text/DayAndDateFormatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Clock;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Ljava/text/DateFormat;",
            ")",
            "Lcom/squareup/text/DayAndDateFormatter;"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/text/DayAndDateFormatter;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/text/DayAndDateFormatter;-><init>(Lcom/squareup/util/Clock;Ljavax/inject/Provider;Lcom/squareup/util/Res;Ljava/text/DateFormat;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/text/DayAndDateFormatter;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/text/DayAndDateFormatter_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Clock;

    iget-object v1, p0, Lcom/squareup/text/DayAndDateFormatter_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/text/DayAndDateFormatter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/text/DayAndDateFormatter_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/text/DateFormat;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/text/DayAndDateFormatter_Factory;->newInstance(Lcom/squareup/util/Clock;Ljavax/inject/Provider;Lcom/squareup/util/Res;Ljava/text/DateFormat;)Lcom/squareup/text/DayAndDateFormatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/text/DayAndDateFormatter_Factory;->get()Lcom/squareup/text/DayAndDateFormatter;

    move-result-object v0

    return-object v0
.end method
