.class public abstract Lcom/squareup/text/CompactFormatter;
.super Ljava/lang/Object;
.source "CompactFormatter.kt"

# interfaces
.implements Lcom/squareup/text/Formatter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/text/CompactFormatter$FormatterScale;,
        Lcom/squareup/text/CompactFormatter$GroupingSize;,
        Lcom/squareup/text/CompactFormatter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/text/Formatter<",
        "TT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCompactFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CompactFormatter.kt\ncom/squareup/text/CompactFormatter\n*L\n1#1,163:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0008&\u0018\u0000 \u001f*\u0004\u0008\u0000\u0010\u00012\u0008\u0012\u0004\u0012\u0002H\u00010\u0002:\u0003\u001f !B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0004J\u0015\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00028\u0000H\u0014\u00a2\u0006\u0002\u0010\u0016J\u0015\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00028\u0000H\u0004\u00a2\u0006\u0002\u0010\u0019J%\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00130\u001b*\u00028\u00002\u0006\u0010\u001c\u001a\u00020\u001dH\u0004\u00a2\u0006\u0002\u0010\u001eR\u0014\u0010\u0006\u001a\u00020\u00078BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u00020\u000b*\u00028\u0000X\u00a4\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/text/CompactFormatter;",
        "T",
        "Lcom/squareup/text/Formatter;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "groupingSize",
        "Lcom/squareup/text/CompactFormatter$GroupingSize;",
        "getGroupingSize",
        "()Lcom/squareup/text/CompactFormatter$GroupingSize;",
        "asBigDecimal",
        "Ljava/math/BigDecimal;",
        "getAsBigDecimal",
        "(Ljava/lang/Object;)Ljava/math/BigDecimal;",
        "abbreviate",
        "",
        "formattedString",
        "",
        "scale",
        "Lcom/squareup/text/CompactFormatter$FormatterScale;",
        "formatterScale",
        "value",
        "(Ljava/lang/Object;)Lcom/squareup/text/CompactFormatter$FormatterScale;",
        "needsTruncation",
        "",
        "(Ljava/lang/Object;)Z",
        "truncatedValueAndScale",
        "Lkotlin/Pair;",
        "numberOfDecimalPlacesWhenTruncated",
        "",
        "(Ljava/lang/Object;I)Lkotlin/Pair;",
        "Companion",
        "FormatterScale",
        "GroupingSize",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/text/CompactFormatter$Companion;

.field private static final UNTRUNCATED_MAXIMUM_VALUE_EXCLUSIVE:Ljava/math/BigDecimal;


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/text/CompactFormatter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/text/CompactFormatter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/text/CompactFormatter;->Companion:Lcom/squareup/text/CompactFormatter$Companion;

    .line 138
    new-instance v0, Ljava/math/BigDecimal;

    const-wide/16 v1, 0x2710

    invoke-direct {v0, v1, v2}, Ljava/math/BigDecimal;-><init>(J)V

    sput-object v0, Lcom/squareup/text/CompactFormatter;->UNTRUNCATED_MAXIMUM_VALUE_EXCLUSIVE:Ljava/math/BigDecimal;

    .line 143
    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;

    invoke-virtual {v0}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;->getMagnitude()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Lcom/squareup/text/CompactFormatter;->UNTRUNCATED_MAXIMUM_VALUE_EXCLUSIVE:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Check failed."

    if-eqz v0, :cond_2

    .line 144
    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$MILLION;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$MILLION;

    invoke-virtual {v0}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$MILLION;->getMagnitude()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v2, Lcom/squareup/text/CompactFormatter;->UNTRUNCATED_MAXIMUM_VALUE_EXCLUSIVE:Ljava/math/BigDecimal;

    invoke-static {v0, v2}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$BILLION;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$BILLION;

    invoke-virtual {v0}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$BILLION;->getMagnitude()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v2, Lcom/squareup/text/CompactFormatter;->UNTRUNCATED_MAXIMUM_VALUE_EXCLUSIVE:Ljava/math/BigDecimal;

    invoke-static {v0, v2}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 144
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 143
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/text/CompactFormatter;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$getUNTRUNCATED_MAXIMUM_VALUE_EXCLUSIVE$cp()Ljava/math/BigDecimal;
    .locals 1

    .line 23
    sget-object v0, Lcom/squareup/text/CompactFormatter;->UNTRUNCATED_MAXIMUM_VALUE_EXCLUSIVE:Ljava/math/BigDecimal;

    return-object v0
.end method

.method private final getGroupingSize()Lcom/squareup/text/CompactFormatter$GroupingSize;
    .locals 3

    .line 157
    iget-object v0, p0, Lcom/squareup/text/CompactFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/utilities/text/R$integer;->power_of_ten_for_compact_numbers:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 158
    sget-object v0, Lcom/squareup/text/CompactFormatter$GroupingSize$THOUSANDS;->INSTANCE:Lcom/squareup/text/CompactFormatter$GroupingSize$THOUSANDS;

    check-cast v0, Lcom/squareup/text/CompactFormatter$GroupingSize;

    goto :goto_0

    :cond_0
    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 159
    sget-object v0, Lcom/squareup/text/CompactFormatter$GroupingSize$MYRIADS;->INSTANCE:Lcom/squareup/text/CompactFormatter$GroupingSize$MYRIADS;

    check-cast v0, Lcom/squareup/text/CompactFormatter$GroupingSize;

    :goto_0
    return-object v0

    .line 160
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Only supports a grouping size of 3 or 4, was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method


# virtual methods
.method protected final abbreviate(Ljava/lang/String;Lcom/squareup/text/CompactFormatter$FormatterScale;)Ljava/lang/CharSequence;
    .locals 1

    const-string v0, "formattedString"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scale"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/text/CompactFormatter;->res:Lcom/squareup/util/Res;

    invoke-virtual {p2}, Lcom/squareup/text/CompactFormatter$FormatterScale;->getFormatPhraseId()I

    move-result p2

    invoke-interface {v0, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 38
    check-cast p1, Ljava/lang/CharSequence;

    const-string v0, "number"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 39
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "res.phrase(scale.formatP\u2026String)\n        .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected formatterScale(Ljava/lang/Object;)Lcom/squareup/text/CompactFormatter$FormatterScale;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/squareup/text/CompactFormatter$FormatterScale;"
        }
    .end annotation

    .line 57
    invoke-virtual {p0, p1}, Lcom/squareup/text/CompactFormatter;->getAsBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {p1}, Ljava/math/BigDecimal;->abs()Ljava/math/BigDecimal;

    move-result-object p1

    const-string/jumbo v0, "value.asBigDecimal.abs()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-direct {p0}, Lcom/squareup/text/CompactFormatter;->getGroupingSize()Lcom/squareup/text/CompactFormatter$GroupingSize;

    move-result-object v0

    .line 59
    sget-object v1, Lcom/squareup/text/CompactFormatter$GroupingSize$MYRIADS;->INSTANCE:Lcom/squareup/text/CompactFormatter$GroupingSize$MYRIADS;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 61
    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD_MYLLION;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD_MYLLION;

    invoke-virtual {v0}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD_MYLLION;->getMagnitude()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/BigDecimals;->greaterThanOrEqualTo(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD_MYLLION;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD_MYLLION;

    check-cast p1, Lcom/squareup/text/CompactFormatter$FormatterScale;

    goto :goto_0

    .line 62
    :cond_0
    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYLLION;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYLLION;

    invoke-virtual {v0}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYLLION;->getMagnitude()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/BigDecimals;->greaterThanOrEqualTo(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYLLION;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYLLION;

    check-cast p1, Lcom/squareup/text/CompactFormatter$FormatterScale;

    goto :goto_0

    .line 63
    :cond_1
    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;

    invoke-virtual {v0}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;->getMagnitude()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/BigDecimals;->greaterThanOrEqualTo(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;

    check-cast p1, Lcom/squareup/text/CompactFormatter$FormatterScale;

    goto :goto_0

    .line 64
    :cond_2
    sget-object p1, Lcom/squareup/text/CompactFormatter$FormatterScale$ONE;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$ONE;

    check-cast p1, Lcom/squareup/text/CompactFormatter$FormatterScale;

    goto :goto_0

    .line 66
    :cond_3
    sget-object v1, Lcom/squareup/text/CompactFormatter$GroupingSize$THOUSANDS;->INSTANCE:Lcom/squareup/text/CompactFormatter$GroupingSize$THOUSANDS;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 68
    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$BILLION;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$BILLION;

    invoke-virtual {v0}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$BILLION;->getMagnitude()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/BigDecimals;->greaterThanOrEqualTo(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object p1, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$BILLION;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$BILLION;

    check-cast p1, Lcom/squareup/text/CompactFormatter$FormatterScale;

    goto :goto_0

    .line 69
    :cond_4
    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$MILLION;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$MILLION;

    invoke-virtual {v0}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$MILLION;->getMagnitude()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/BigDecimals;->greaterThanOrEqualTo(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object p1, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$MILLION;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$MILLION;

    check-cast p1, Lcom/squareup/text/CompactFormatter$FormatterScale;

    goto :goto_0

    .line 70
    :cond_5
    sget-object v0, Lcom/squareup/text/CompactFormatter;->UNTRUNCATED_MAXIMUM_VALUE_EXCLUSIVE:Ljava/math/BigDecimal;

    invoke-static {p1, v0}, Lcom/squareup/util/BigDecimals;->greaterThanOrEqualTo(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result p1

    if-eqz p1, :cond_6

    sget-object p1, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$THOUSAND;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleThousands$THOUSAND;

    check-cast p1, Lcom/squareup/text/CompactFormatter$FormatterScale;

    goto :goto_0

    .line 71
    :cond_6
    sget-object p1, Lcom/squareup/text/CompactFormatter$FormatterScale$ONE;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$ONE;

    check-cast p1, Lcom/squareup/text/CompactFormatter$FormatterScale;

    :goto_0
    return-object p1

    .line 67
    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method protected abstract getAsBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/math/BigDecimal;"
        }
    .end annotation
.end method

.method protected final needsTruncation(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/text/CompactFormatter;->formatterScale(Ljava/lang/Object;)Lcom/squareup/text/CompactFormatter$FormatterScale;

    move-result-object p1

    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$ONE;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$ONE;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method protected final truncatedValueAndScale(Ljava/lang/Object;I)Lkotlin/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)",
            "Lkotlin/Pair<",
            "Ljava/math/BigDecimal;",
            "Lcom/squareup/text/CompactFormatter$FormatterScale;",
            ">;"
        }
    .end annotation

    .line 85
    invoke-virtual {p0, p1}, Lcom/squareup/text/CompactFormatter;->getAsBigDecimal(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 86
    invoke-virtual {p0, p1}, Lcom/squareup/text/CompactFormatter;->formatterScale(Ljava/lang/Object;)Lcom/squareup/text/CompactFormatter$FormatterScale;

    move-result-object p1

    .line 88
    sget-object v1, Lcom/squareup/text/CompactFormatter$FormatterScale$ONE;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$ONE;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    new-instance p1, Lkotlin/Pair;

    sget-object p2, Lcom/squareup/text/CompactFormatter$FormatterScale$ONE;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$ONE;

    invoke-direct {p1, v0, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1

    .line 94
    :cond_0
    new-instance v1, Lkotlin/Pair;

    .line 96
    invoke-virtual {p1}, Lcom/squareup/text/CompactFormatter$FormatterScale;->getMagnitude()Ljava/math/BigDecimal;

    move-result-object v2

    .line 98
    sget-object v3, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    .line 95
    invoke-virtual {v0, v2, p2, v3}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object p2

    .line 94
    invoke-direct {v1, p2, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method
