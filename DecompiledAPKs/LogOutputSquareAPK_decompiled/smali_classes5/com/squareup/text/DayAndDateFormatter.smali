.class public final Lcom/squareup/text/DayAndDateFormatter;
.super Ljava/lang/Object;
.source "DayAndDateFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDayAndDateFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DayAndDateFormatter.kt\ncom/squareup/text/DayAndDateFormatter\n*L\n1#1,112:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0008\u0001\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u000e\u0010\u000c\u001a\u00020\u00002\u0006\u0010\u000c\u001a\u00020\rJ\u0006\u0010\u0011\u001a\u00020\u0012J\u0010\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u000c\u001a\u00020\rH\u0002J\u0008\u0010\u0014\u001a\u00020\u0015H\u0002J\u0006\u0010\u0016\u001a\u00020\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/text/DayAndDateFormatter;",
        "",
        "clock",
        "Lcom/squareup/util/Clock;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "res",
        "Lcom/squareup/util/Res;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "(Lcom/squareup/util/Clock;Ljavax/inject/Provider;Lcom/squareup/util/Res;Ljava/text/DateFormat;)V",
        "date",
        "Ljava/util/Date;",
        "dayNameFormat",
        "inUpperCase",
        "",
        "format",
        "",
        "getDay",
        "resetFormatter",
        "",
        "toUpperCase",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private date:Ljava/util/Date;

.field private final dateFormat:Ljava/text/DateFormat;

.field private final dayNameFormat:Ljava/text/DateFormat;

.field private inUpperCase:Z

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;Ljavax/inject/Provider;Lcom/squareup/util/Res;Ljava/text/DateFormat;)V
    .locals 1
    .param p4    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/LongForm;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Clock;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Ljava/text/DateFormat;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "clock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormat"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/text/DayAndDateFormatter;->clock:Lcom/squareup/util/Clock;

    iput-object p2, p0, Lcom/squareup/text/DayAndDateFormatter;->localeProvider:Ljavax/inject/Provider;

    iput-object p3, p0, Lcom/squareup/text/DayAndDateFormatter;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/text/DayAndDateFormatter;->dateFormat:Ljava/text/DateFormat;

    .line 40
    new-instance p1, Ljava/text/SimpleDateFormat;

    iget-object p2, p0, Lcom/squareup/text/DayAndDateFormatter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {p2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Locale;

    const-string p3, "EEEE"

    invoke-direct {p1, p3, p2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    check-cast p1, Ljava/text/DateFormat;

    iput-object p1, p0, Lcom/squareup/text/DayAndDateFormatter;->dayNameFormat:Ljava/text/DateFormat;

    return-void
.end method

.method private final getDay(Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/text/DayAndDateFormatter;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/squareup/util/Times;->getRelativeDate(Ljava/util/Date;J)Lcom/squareup/util/Times$RelativeDate;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/text/DayAndDateFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/util/Times$RelativeDate;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 102
    :goto_0
    iget-object v0, p0, Lcom/squareup/text/DayAndDateFormatter;->dayNameFormat:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "dayNameFormat.format(date)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 101
    :cond_1
    iget-object p1, p0, Lcom/squareup/text/DayAndDateFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/utilities/R$string;->yesterday:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 100
    :cond_2
    iget-object p1, p0, Lcom/squareup/text/DayAndDateFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/utilities/R$string;->today:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method private final resetFormatter()V
    .locals 1

    const/4 v0, 0x0

    .line 106
    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/squareup/text/DayAndDateFormatter;->date:Ljava/util/Date;

    const/4 v0, 0x0

    .line 107
    iput-boolean v0, p0, Lcom/squareup/text/DayAndDateFormatter;->inUpperCase:Z

    return-void
.end method


# virtual methods
.method public final date(Ljava/util/Date;)Lcom/squareup/text/DayAndDateFormatter;
    .locals 1

    const-string v0, "date"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-static {p1}, Lcom/squareup/util/Dates;->copy(Ljava/util/Date;)Ljava/util/Date;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/text/DayAndDateFormatter;->date:Ljava/util/Date;

    return-object p0
.end method

.method public final format()Ljava/lang/String;
    .locals 3

    .line 57
    iget-object v0, p0, Lcom/squareup/text/DayAndDateFormatter;->date:Ljava/util/Date;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_4

    .line 59
    iget-object v0, p0, Lcom/squareup/text/DayAndDateFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/utilities/R$string;->day_and_date:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/squareup/text/DayAndDateFormatter;->date:Ljava/util/Date;

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-direct {p0, v1}, Lcom/squareup/text/DayAndDateFormatter;->getDay(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "day"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/squareup/text/DayAndDateFormatter;->dateFormat:Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/text/DayAndDateFormatter;->date:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "date"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 67
    iget-boolean v1, p0, Lcom/squareup/text/DayAndDateFormatter;->inUpperCase:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/squareup/text/DayAndDateFormatter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "localeProvider.get()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/util/Locale;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "(this as java.lang.String).toUpperCase(locale)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/squareup/text/DayAndDateFormatter;->resetFormatter()V

    return-object v0

    .line 57
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Date must be set with method date() before calling format()."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final toUpperCase()Lcom/squareup/text/DayAndDateFormatter;
    .locals 1

    const/4 v0, 0x1

    .line 94
    iput-boolean v0, p0, Lcom/squareup/text/DayAndDateFormatter;->inUpperCase:Z

    return-object p0
.end method
