.class public abstract Lcom/squareup/text/PostalScrubber;
.super Ljava/lang/Object;
.source "PostalScrubber.java"

# interfaces
.implements Lcom/squareup/text/Scrubber;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/text/PostalScrubber$UsaOrCanada;,
        Lcom/squareup/text/PostalScrubber$Canada;,
        Lcom/squareup/text/PostalScrubber$Usa;,
        Lcom/squareup/text/PostalScrubber$Result;
    }
.end annotation


# instance fields
.field private onInvalidContentListener:Lcom/squareup/text/OnInvalidContentListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static forCanada()Lcom/squareup/text/PostalScrubber;
    .locals 2

    .line 23
    new-instance v0, Lcom/squareup/text/PostalScrubber$Canada;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/text/PostalScrubber$Canada;-><init>(Lcom/squareup/text/PostalScrubber$1;)V

    return-object v0
.end method

.method public static forUs()Lcom/squareup/text/PostalScrubber;
    .locals 2

    .line 19
    new-instance v0, Lcom/squareup/text/PostalScrubber$Usa;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/text/PostalScrubber$Usa;-><init>(Lcom/squareup/text/PostalScrubber$1;)V

    return-object v0
.end method

.method public static forUsOrCanada()Lcom/squareup/text/PostalScrubber;
    .locals 2

    .line 27
    new-instance v0, Lcom/squareup/text/PostalScrubber$UsaOrCanada;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/text/PostalScrubber$UsaOrCanada;-><init>(Lcom/squareup/text/PostalScrubber$1;)V

    return-object v0
.end method


# virtual methods
.method public isValid(Ljava/lang/String;)Z
    .locals 0

    .line 44
    invoke-virtual {p0, p1}, Lcom/squareup/text/PostalScrubber;->scrub(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/text/PostalScrubber;->isValidFormattedPostalCode(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method protected abstract isValidFormattedPostalCode(Ljava/lang/String;)Z
.end method

.method protected abstract parsePostal(Ljava/lang/String;)Lcom/squareup/text/PostalScrubber$Result;
.end method

.method public final scrub(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 31
    invoke-virtual {p0, p1}, Lcom/squareup/text/PostalScrubber;->parsePostal(Ljava/lang/String;)Lcom/squareup/text/PostalScrubber$Result;

    move-result-object p1

    .line 32
    iget-boolean v0, p1, Lcom/squareup/text/PostalScrubber$Result;->valid:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/text/PostalScrubber;->onInvalidContentListener:Lcom/squareup/text/OnInvalidContentListener;

    if-eqz v0, :cond_0

    .line 33
    invoke-interface {v0}, Lcom/squareup/text/OnInvalidContentListener;->onInvalidContent()V

    .line 35
    :cond_0
    iget-object p1, p1, Lcom/squareup/text/PostalScrubber$Result;->scrubbed:Ljava/lang/String;

    return-object p1
.end method

.method public setOnInvalidContentListener(Lcom/squareup/text/OnInvalidContentListener;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/text/PostalScrubber;->onInvalidContentListener:Lcom/squareup/text/OnInvalidContentListener;

    return-void
.end method

.method public shorten(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/text/PostalScrubber;->scrub(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 40
    invoke-static {p1, p1}, Lcom/squareup/text/PostalCodes;->getShortZipCode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
