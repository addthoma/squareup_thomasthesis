.class public Lcom/squareup/text/RateFormatter;
.super Ljava/lang/Object;
.source "RateFormatter.java"


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V
    .locals 0
    .param p2    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/Cents;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/text/RateFormatter;->res:Lcom/squareup/util/Res;

    .line 21
    iput-object p2, p0, Lcom/squareup/text/RateFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 22
    iput-object p3, p0, Lcom/squareup/text/RateFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method public format(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Ljava/lang/CharSequence;
    .locals 2

    .line 26
    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    iget-object p1, p0, Lcom/squareup/text/RateFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 31
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/util/Percentage;->isZero()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33
    iget-object p2, p0, Lcom/squareup/text/RateFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 37
    :cond_1
    iget-object v0, p0, Lcom/squareup/text/RateFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/proto_utilities/R$string;->combined_rate_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/text/RateFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 38
    invoke-interface {v1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v1, "percentage"

    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/text/RateFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 39
    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "interchange"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 40
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method
