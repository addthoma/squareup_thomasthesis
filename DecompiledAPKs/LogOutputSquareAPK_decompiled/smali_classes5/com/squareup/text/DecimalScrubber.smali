.class public final Lcom/squareup/text/DecimalScrubber;
.super Ljava/lang/Object;
.source "DecimalScrubber.kt"

# interfaces
.implements Lcom/squareup/text/SelectableTextScrubber;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/text/DecimalScrubber$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDecimalScrubber.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DecimalScrubber.kt\ncom/squareup/text/DecimalScrubber\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n+ 3 _Strings.kt\nkotlin/text/StringsKt___StringsKt\n*L\n1#1,462:1\n11561#2,3:463\n995#3,3:466\n*E\n*S KotlinDebug\n*F\n+ 1 DecimalScrubber.kt\ncom/squareup/text/DecimalScrubber\n*L\n191#1,3:463\n384#1,3:466\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\t\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0011H\u0016J\u000c\u0010\u0014\u001a\u00020\u000f*\u00020\u0011H\u0002J\u000c\u0010\u0015\u001a\u00020\u0016*\u00020\rH\u0002J\u000c\u0010\u0017\u001a\u00020\u0016*\u00020\rH\u0002J\u000c\u0010\u0018\u001a\u00020\u0016*\u00020\u0006H\u0002J\u000c\u0010\u0018\u001a\u00020\u0016*\u00020\rH\u0002J\u000c\u0010\u0019\u001a\u00020\u0016*\u00020\u000bH\u0002J\u001c\u0010\u001a\u001a\u00020\u0016*\u00020\u00062\u0006\u0010\u001b\u001a\u00020\u000f2\u0006\u0010\u001c\u001a\u00020\u000fH\u0002J\u000c\u0010\u001d\u001a\u00020\u000f*\u00020\u0006H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\n \t*\u0004\u0018\u00010\u00080\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/text/DecimalScrubber;",
        "Lcom/squareup/text/SelectableTextScrubber;",
        "formatter",
        "Lcom/squareup/quantity/BigDecimalFormatter;",
        "(Lcom/squareup/quantity/BigDecimalFormatter;)V",
        "decimalSeparator",
        "",
        "decimalSeparatorPattern",
        "Ljava/util/regex/Pattern;",
        "kotlin.jvm.PlatformType",
        "maxIntValue",
        "",
        "maxValue",
        "",
        "precision",
        "",
        "scrub",
        "Lcom/squareup/text/SelectableTextScrubber$SelectableText;",
        "current",
        "proposed",
        "decimalSeparatorPosition",
        "hasMultipleDecimalSeparators",
        "",
        "hasValidDecimalSeparator",
        "isDecimalSeparator",
        "isEmpty",
        "isValidDecimalSeparator",
        "textLength",
        "positionInText",
        "toNumericalValue",
        "Companion",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/text/DecimalScrubber$Companion;

.field private static final EMPTY:J = -0x1L


# instance fields
.field private final decimalSeparator:C

.field private final decimalSeparatorPattern:Ljava/util/regex/Pattern;

.field private final formatter:Lcom/squareup/quantity/BigDecimalFormatter;

.field private final maxIntValue:J

.field private final maxValue:Ljava/lang/String;

.field private final precision:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/text/DecimalScrubber$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/text/DecimalScrubber$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/text/DecimalScrubber;->Companion:Lcom/squareup/text/DecimalScrubber$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/quantity/BigDecimalFormatter;)V
    .locals 4

    const-string v0, "formatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/text/DecimalScrubber;->formatter:Lcom/squareup/quantity/BigDecimalFormatter;

    .line 68
    iget-object p1, p0, Lcom/squareup/text/DecimalScrubber;->formatter:Lcom/squareup/quantity/BigDecimalFormatter;

    invoke-virtual {p1}, Lcom/squareup/quantity/BigDecimalFormatter;->getRoundingScale()I

    move-result p1

    iput p1, p0, Lcom/squareup/text/DecimalScrubber;->precision:I

    .line 69
    iget-object p1, p0, Lcom/squareup/text/DecimalScrubber;->formatter:Lcom/squareup/quantity/BigDecimalFormatter;

    invoke-virtual {p1}, Lcom/squareup/quantity/BigDecimalFormatter;->getDecimalSeparator()C

    move-result p1

    iput-char p1, p0, Lcom/squareup/text/DecimalScrubber;->decimalSeparator:C

    .line 70
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v0, 0x5c

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-char v0, p0, Lcom/squareup/text/DecimalScrubber;->decimalSeparator:C

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/text/DecimalScrubber;->decimalSeparatorPattern:Ljava/util/regex/Pattern;

    .line 71
    iget-object p1, p0, Lcom/squareup/text/DecimalScrubber;->formatter:Lcom/squareup/quantity/BigDecimalFormatter;

    new-instance v0, Ljava/math/BigDecimal;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "999999."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "9"

    check-cast v2, Ljava/lang/CharSequence;

    iget v3, p0, Lcom/squareup/text/DecimalScrubber;->precision:I

    invoke-static {v2, v3}, Lkotlin/text/StringsKt;->repeat(Ljava/lang/CharSequence;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/quantity/BigDecimalFormatter;->format(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/text/DecimalScrubber;->maxValue:Ljava/lang/String;

    const/4 p1, 0x0

    .line 72
    invoke-static {p1}, Lcom/squareup/money/WholeUnitAmountScrubber;->maxValueFor(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/text/DecimalScrubber;->maxIntValue:J

    return-void
.end method

.method private final decimalSeparatorPosition(Lcom/squareup/text/SelectableTextScrubber$SelectableText;)I
    .locals 8

    .line 391
    iget-object v0, p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    const-string/jumbo v1, "text"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v0

    check-cast v2, Ljava/lang/CharSequence;

    iget-char v3, p0, Lcom/squareup/text/DecimalScrubber;->decimalSeparator:C

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lkotlin/text/StringsKt;->indexOf$default(Ljava/lang/CharSequence;CIZILjava/lang/Object;)I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 395
    iget-object p1, p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p1

    check-cast v2, Ljava/lang/CharSequence;

    const/16 v3, 0x2e

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lkotlin/text/StringsKt;->indexOf$default(Ljava/lang/CharSequence;CIZILjava/lang/Object;)I

    move-result v0

    :cond_0
    return v0
.end method

.method private final hasMultipleDecimalSeparators(Ljava/lang/String;)Z
    .locals 4

    .line 384
    check-cast p1, Ljava/lang/CharSequence;

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 467
    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 384
    invoke-direct {p0, v3}, Lcom/squareup/text/DecimalScrubber;->isDecimalSeparator(C)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v2, v2, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    if-le v2, p1, :cond_2

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method private final hasValidDecimalSeparator(Ljava/lang/String;)Z
    .locals 6

    .line 303
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    iget-char v1, p0, Lcom/squareup/text/DecimalScrubber;->decimalSeparator:C

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lkotlin/text/StringsKt;->indexOf$default(Ljava/lang/CharSequence;CIZILjava/lang/Object;)I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 305
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    add-int/2addr v0, v1

    invoke-direct {p0, v2, p1, v0}, Lcom/squareup/text/DecimalScrubber;->isValidDecimalSeparator(CII)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private final isDecimalSeparator(C)Z
    .locals 1

    const/16 v0, 0x2e

    if-eq p1, v0, :cond_1

    .line 295
    iget-char v0, p0, Lcom/squareup/text/DecimalScrubber;->decimalSeparator:C

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private final isDecimalSeparator(Ljava/lang/String;)Z
    .locals 3

    .line 297
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/text/DecimalScrubber;->isDecimalSeparator(C)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private final isEmpty(J)Z
    .locals 3

    const-wide/16 v0, -0x1

    cmp-long v2, p1, v0

    if-nez v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final isValidDecimalSeparator(CII)Z
    .locals 2

    .line 377
    invoke-direct {p0, p1}, Lcom/squareup/text/DecimalScrubber;->isDecimalSeparator(C)Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    .line 378
    iget p1, p0, Lcom/squareup/text/DecimalScrubber;->precision:I

    add-int/lit8 v1, p1, -0x1

    add-int/2addr p1, v0

    sub-int/2addr p2, p3

    if-le v1, p2, :cond_0

    goto :goto_0

    :cond_0
    if-lt p1, p2, :cond_1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method private final toNumericalValue(C)I
    .locals 0

    add-int/lit8 p1, p1, -0x30

    return p1
.end method


# virtual methods
.method public scrub(Lcom/squareup/text/SelectableTextScrubber$SelectableText;Lcom/squareup/text/SelectableTextScrubber$SelectableText;)Lcom/squareup/text/SelectableTextScrubber$SelectableText;
    .locals 22

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const-string v3, "current"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "proposed"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-static/range {p1 .. p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    return-object v1

    .line 83
    :cond_0
    iget-object v3, v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    const-string v4, "proposed.text"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    const/4 v4, 0x0

    if-nez v3, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    const-string v6, ""

    if-eqz v3, :cond_2

    .line 84
    new-instance v1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {v1, v6}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    return-object v1

    .line 87
    :cond_2
    iget-object v3, v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    .line 88
    iget-object v7, v1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    .line 92
    iget v8, v0, Lcom/squareup/text/DecimalScrubber;->precision:I

    const-string v9, "null cannot be cast to non-null type java.lang.String"

    const/16 v10, 0x2e

    const-string v11, "proposedText"

    const-string v12, "currentText"

    if-lez v8, :cond_e

    .line 97
    invoke-static {v7, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v8, v7

    check-cast v8, Ljava/lang/CharSequence;

    invoke-interface {v8}, Ljava/lang/CharSequence;->length()I

    move-result v13

    if-lez v13, :cond_3

    const/4 v13, 0x1

    goto :goto_1

    :cond_3
    const/4 v13, 0x0

    :goto_1
    const/4 v14, 0x2

    if-eqz v13, :cond_4

    .line 98
    invoke-static {v3, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v13, v3

    check-cast v13, Ljava/lang/CharSequence;

    iget-char v15, v0, Lcom/squareup/text/DecimalScrubber;->decimalSeparator:C

    const/4 v5, 0x0

    invoke-static {v13, v15, v4, v14, v5}, Lkotlin/text/StringsKt;->contains$default(Ljava/lang/CharSequence;CZILjava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 99
    iget-object v5, v0, Lcom/squareup/text/DecimalScrubber;->decimalSeparatorPattern:Ljava/util/regex/Pattern;

    const-string v13, "decimalSeparatorPattern"

    invoke-static {v5, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v8, v5}, Lcom/squareup/util/Strings;->removePattern(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 100
    invoke-direct/range {p0 .. p1}, Lcom/squareup/text/DecimalScrubber;->decimalSeparatorPosition(Lcom/squareup/text/SelectableTextScrubber$SelectableText;)I

    move-result v1

    .line 101
    new-instance v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {v2, v7, v1, v1}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    return-object v2

    .line 109
    :cond_4
    invoke-static {v3, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v3}, Lcom/squareup/text/DecimalScrubber;->isDecimalSeparator(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 110
    new-instance v1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x30

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-char v3, v0, Lcom/squareup/text/DecimalScrubber;->decimalSeparator:C

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v3, "0"

    check-cast v3, Ljava/lang/CharSequence;

    iget v4, v0, Lcom/squareup/text/DecimalScrubber;->precision:I

    invoke-static {v3, v4}, Lkotlin/text/StringsKt;->repeat(Ljava/lang/CharSequence;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v14, v14}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    return-object v1

    .line 113
    :cond_5
    move-object v5, v3

    check-cast v5, Ljava/lang/CharSequence;

    invoke-static {v5}, Lkotlin/text/StringsKt;->first(Ljava/lang/CharSequence;)C

    move-result v13

    invoke-direct {v0, v13}, Lcom/squareup/text/DecimalScrubber;->isDecimalSeparator(C)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 114
    new-instance v2, Ljava/math/BigDecimal;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {v5}, Lcom/squareup/util/Strings;->removeNonDigits(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 117
    invoke-static {v2}, Lcom/squareup/util/BigDecimals;->isZero(Ljava/math/BigDecimal;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 118
    new-instance v1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {v1, v6}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    return-object v1

    .line 123
    :cond_6
    invoke-direct {v0, v3}, Lcom/squareup/text/DecimalScrubber;->hasMultipleDecimalSeparators(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    return-object v1

    .line 127
    :cond_7
    iget-object v1, v0, Lcom/squareup/text/DecimalScrubber;->formatter:Lcom/squareup/quantity/BigDecimalFormatter;

    invoke-virtual {v1, v2}, Lcom/squareup/quantity/BigDecimalFormatter;->format(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object v1

    .line 131
    invoke-interface {v8}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_8

    const/16 v16, 0x1

    goto :goto_2

    :cond_8
    const/16 v16, 0x0

    :goto_2
    if-nez v16, :cond_9

    .line 132
    new-instance v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {v2, v1, v4, v4}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    return-object v2

    .line 137
    :cond_9
    new-instance v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    .line 138
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    .line 137
    invoke-direct {v2, v1, v3, v4}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    return-object v2

    .line 143
    :cond_a
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_b

    invoke-static {v5}, Lkotlin/text/StringsKt;->first(Ljava/lang/CharSequence;)C

    move-result v13

    invoke-static {v13}, Ljava/lang/Character;->isDigit(C)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 144
    new-instance v1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    iget-object v2, v0, Lcom/squareup/text/DecimalScrubber;->formatter:Lcom/squareup/quantity/BigDecimalFormatter;

    new-instance v4, Ljava/math/BigDecimal;

    invoke-direct {v4, v3}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/squareup/quantity/BigDecimalFormatter;->format(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v14, v14}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    return-object v1

    .line 148
    :cond_b
    invoke-interface {v8}, Ljava/lang/CharSequence;->length()I

    move-result v8

    if-lez v8, :cond_c

    const/4 v8, 0x1

    goto :goto_3

    :cond_c
    const/4 v8, 0x0

    :goto_3
    if-eqz v8, :cond_e

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v13, v14

    if-le v8, v13, :cond_e

    .line 150
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v13

    sub-int/2addr v8, v13

    .line 151
    iget v13, v1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    add-int/2addr v8, v13

    .line 154
    invoke-virtual {v3, v13, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    const-string v15, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v14, v15}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v14, Ljava/lang/CharSequence;

    .line 155
    invoke-static {v14}, Lcom/squareup/util/Strings;->removeNonDigits(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v14

    .line 157
    check-cast v14, Ljava/lang/CharSequence;

    invoke-static {v5, v13, v8, v14}, Lkotlin/text/StringsKt;->replaceRange(Ljava/lang/CharSequence;IILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 161
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v5, :cond_d

    invoke-virtual {v5, v4, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v15}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    invoke-direct {v0, v8}, Lcom/squareup/text/DecimalScrubber;->hasValidDecimalSeparator(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_e

    .line 163
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v2, v3

    .line 166
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v3

    .line 167
    iget v1, v1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    add-int/2addr v1, v2

    .line 165
    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 170
    new-instance v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {v2, v8, v1, v1}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    return-object v2

    .line 161
    :cond_d
    new-instance v1, Lkotlin/TypeCastException;

    invoke-direct {v1, v9}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 180
    :cond_e
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    const/4 v8, 0x1

    xor-int/2addr v5, v8

    if-eqz v5, :cond_f

    .line 181
    invoke-static {v7, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget v5, v1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    invoke-static {v7, v5}, Lcom/squareup/text/DecimalScrubberKt;->countDigitsToRightOfCursor(Ljava/lang/String;I)I

    move-result v5

    goto :goto_4

    .line 183
    :cond_f
    invoke-static {v3, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget v5, v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    invoke-static {v3, v5}, Lcom/squareup/text/DecimalScrubberKt;->countDigitsToRightOfCursor(Ljava/lang/String;I)I

    move-result v5

    :goto_4
    const-wide/16 v13, -0x1

    .line 186
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 190
    invoke-static {v3, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v3, :cond_24

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    const-string v11, "(this as java.lang.String).toCharArray()"

    invoke-static {v9, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 464
    array-length v11, v9

    move/from16 v18, v5

    const/4 v5, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x0

    :goto_5
    if-ge v5, v11, :cond_17

    aget-char v4, v9, v5

    add-int/lit8 v10, v15, 0x1

    .line 192
    invoke-static {v4}, Ljava/lang/Character;->isDigit(C)Z

    move-result v20

    if-eqz v20, :cond_13

    if-nez v17, :cond_11

    .line 196
    invoke-direct {v0, v13, v14}, Lcom/squareup/text/DecimalScrubber;->isEmpty(J)Z

    move-result v15

    if-eqz v15, :cond_10

    const-wide/16 v13, 0x0

    :cond_10
    const-wide/16 v20, 0xa

    mul-long v13, v13, v20

    invoke-direct {v0, v4}, Lcom/squareup/text/DecimalScrubber;->toNumericalValue(C)I

    move-result v4

    int-to-long v1, v4

    add-long/2addr v13, v1

    .line 198
    iget-wide v1, v0, Lcom/squareup/text/DecimalScrubber;->maxIntValue:J

    cmp-long v4, v13, v1

    if-lez v4, :cond_16

    .line 199
    new-instance v1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    iget-object v2, v0, Lcom/squareup/text/DecimalScrubber;->maxValue:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v4, v0, Lcom/squareup/text/DecimalScrubber;->maxValue:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    return-object v1

    .line 203
    :cond_11
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    iget v2, v0, Lcom/squareup/text/DecimalScrubber;->precision:I

    if-ge v1, v2, :cond_12

    .line 205
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "decimalPart.append(c)"

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_7

    .line 206
    :cond_12
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    iget v2, v0, Lcom/squareup/text/DecimalScrubber;->precision:I

    if-ne v1, v2, :cond_16

    add-int/lit8 v18, v18, -0x1

    goto :goto_7

    .line 210
    :cond_13
    iget v1, v0, Lcom/squareup/text/DecimalScrubber;->precision:I

    if-lez v1, :cond_16

    if-nez v17, :cond_14

    .line 213
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v4, v1, v10}, Lcom/squareup/text/DecimalScrubber;->isValidDecimalSeparator(CII)Z

    move-result v1

    if-eqz v1, :cond_14

    const/16 v17, 0x1

    goto :goto_7

    .line 218
    :cond_14
    invoke-static {v7, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v7

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_15

    const/4 v1, 0x1

    goto :goto_6

    :cond_15
    const/4 v1, 0x0

    :goto_6
    if-eqz v1, :cond_16

    if-eqz v17, :cond_16

    .line 220
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v4, v1, v10}, Lcom/squareup/text/DecimalScrubber;->isValidDecimalSeparator(CII)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 232
    new-instance v1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {v1, v7, v15, v15}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    return-object v1

    :cond_16
    :goto_7
    add-int/lit8 v5, v5, 0x1

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move v15, v10

    const/4 v4, 0x0

    const/16 v10, 0x2e

    goto/16 :goto_5

    .line 238
    :cond_17
    invoke-direct {v0, v13, v14}, Lcom/squareup/text/DecimalScrubber;->isEmpty(J)Z

    move-result v1

    if-eqz v1, :cond_19

    move-object v1, v8

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_18

    const/4 v1, 0x1

    goto :goto_8

    :cond_18
    const/4 v1, 0x0

    :goto_8
    if-eqz v1, :cond_19

    .line 239
    new-instance v1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {v1, v6}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    return-object v1

    .line 243
    :cond_19
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    iget v2, v0, Lcom/squareup/text/DecimalScrubber;->precision:I

    if-ge v1, v2, :cond_1a

    add-int/lit8 v18, v18, 0x1

    :cond_1a
    move/from16 v1, v18

    .line 249
    iget v2, v0, Lcom/squareup/text/DecimalScrubber;->precision:I

    if-nez v2, :cond_1b

    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v13, v14}, Ljava/math/BigDecimal;-><init>(J)V

    goto :goto_9

    .line 250
    :cond_1b
    invoke-direct {v0, v13, v14}, Lcom/squareup/text/DecimalScrubber;->isEmpty(J)Z

    move-result v2

    if-eqz v2, :cond_1c

    new-instance v2, Ljava/math/BigDecimal;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    goto :goto_9

    :cond_1c
    const/16 v5, 0x2e

    .line 251
    new-instance v2, Ljava/math/BigDecimal;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 253
    :goto_9
    iget-object v4, v0, Lcom/squareup/text/DecimalScrubber;->formatter:Lcom/squareup/quantity/BigDecimalFormatter;

    invoke-virtual {v4, v2}, Lcom/squareup/quantity/BigDecimalFormatter;->format(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object v4

    .line 256
    invoke-static {v2}, Lcom/squareup/util/BigDecimals;->isZero(Ljava/math/BigDecimal;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 257
    invoke-static {v7, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v7

    check-cast v2, Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_1d

    const/16 v19, 0x1

    goto :goto_a

    :cond_1d
    const/16 v19, 0x0

    :goto_a
    if-eqz v19, :cond_1e

    .line 258
    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    goto :goto_b

    .line 261
    :cond_1e
    new-instance v5, Ljava/math/BigDecimal;

    invoke-static {v2}, Lcom/squareup/util/Strings;->removeNonDigits(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    iget v2, v0, Lcom/squareup/text/DecimalScrubber;->precision:I

    invoke-virtual {v5, v2}, Ljava/math/BigDecimal;->movePointLeft(I)Ljava/math/BigDecimal;

    move-result-object v2

    .line 264
    :goto_b
    sget-object v5, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v2, v5}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v5

    if-lez v5, :cond_1f

    .line 266
    new-instance v1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {v1, v6}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    return-object v1

    :cond_1f
    const-string v5, "currentAmount"

    .line 267
    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/squareup/util/BigDecimals;->isZero(Ljava/math/BigDecimal;)Z

    move-result v2

    if-eqz v2, :cond_20

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v2, v5, :cond_20

    .line 269
    new-instance v1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {v1, v6}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    return-object v1

    :cond_20
    move-object/from16 v2, p2

    .line 273
    iget v5, v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    iget v6, v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    if-eq v5, v6, :cond_21

    .line 276
    new-instance v1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    iget v3, v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    iget v2, v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    move-object v2, v1

    goto :goto_d

    .line 279
    :cond_21
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_23

    move-object/from16 v5, p1

    iget v6, v5, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    iget v8, v5, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    if-ne v6, v8, :cond_23

    iget v6, v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    iget v8, v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    if-ne v6, v8, :cond_23

    .line 283
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v3, 0x1

    xor-int/2addr v1, v3

    if-eqz v1, :cond_22

    iget v1, v5, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    goto :goto_c

    :cond_22
    iget v1, v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    .line 284
    :goto_c
    new-instance v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {v2, v4, v1, v1}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    goto :goto_d

    .line 286
    :cond_23
    invoke-static {v4, v1}, Lcom/squareup/text/DecimalScrubberKt;->findCursorPosition(Ljava/lang/String;I)I

    move-result v1

    .line 287
    new-instance v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {v2, v4, v1, v1}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    :goto_d
    return-object v2

    .line 190
    :cond_24
    new-instance v1, Lkotlin/TypeCastException;

    invoke-direct {v1, v9}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
