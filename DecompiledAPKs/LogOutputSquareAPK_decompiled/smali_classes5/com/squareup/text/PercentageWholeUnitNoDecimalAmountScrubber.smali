.class public final Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber;
.super Lcom/squareup/money/WholeUnitAmountScrubber;
.source "PercentageWholeUnitNoDecimalAmountScrubber.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u0017\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B\u0017\u0008\u0016\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\tB\u0015\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0014R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber;",
        "Lcom/squareup/money/WholeUnitAmountScrubber;",
        "resources",
        "Landroid/content/res/Resources;",
        "zeroState",
        "Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;",
        "(Landroid/content/res/Resources;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V",
        "percentPhrase",
        "Lcom/squareup/phrase/Phrase;",
        "(Lcom/squareup/phrase/Phrase;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V",
        "formatAmount",
        "",
        "proposedAmount",
        "",
        "Companion",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final MAX_VALUE:J = 0x64L


# instance fields
.field private final percentPhrase:Lcom/squareup/phrase/Phrase;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber;->Companion:Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "zeroState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    sget v0, Lcom/squareup/widgets/R$string;->percent_character_pattern:I

    invoke-static {p1, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v0, "Phrase.from(resources, c\u2026ercent_character_pattern)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0, p1, p2}, Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber;-><init>(Lcom/squareup/phrase/Phrase;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/phrase/Phrase;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V
    .locals 2

    const-string v0, "percentPhrase"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "zeroState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x64

    .line 15
    invoke-direct {p0, v0, v1, p2}, Lcom/squareup/money/WholeUnitAmountScrubber;-><init>(JLcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V

    iput-object p1, p0, Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber;->percentPhrase:Lcom/squareup/phrase/Phrase;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "zeroState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget v0, Lcom/squareup/widgets/R$string;->percent_character_pattern:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber;-><init>(Lcom/squareup/phrase/Phrase;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V

    return-void
.end method


# virtual methods
.method protected formatAmount(J)Ljava/lang/String;
    .locals 1

    long-to-int p2, p1

    .line 29
    iget-object p1, p0, Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber;->percentPhrase:Lcom/squareup/phrase/Phrase;

    const-string/jumbo v0, "value"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 30
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 31
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
