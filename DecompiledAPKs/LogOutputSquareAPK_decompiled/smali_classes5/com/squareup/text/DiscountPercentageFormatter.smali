.class public Lcom/squareup/text/DiscountPercentageFormatter;
.super Lcom/squareup/text/PercentageFormatter;
.source "DiscountPercentageFormatter.java"


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 10
    invoke-direct {p0, p1}, Lcom/squareup/text/PercentageFormatter;-><init>(Ljavax/inject/Provider;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic format(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/util/Percentage;

    invoke-virtual {p0, p1}, Lcom/squareup/text/DiscountPercentageFormatter;->format(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public format(Lcom/squareup/util/Percentage;)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 15
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/util/Percentage;->isPositive()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/util/Percentage;->negate()Lcom/squareup/util/Percentage;

    move-result-object p1

    :cond_1
    invoke-super {p0, p1}, Lcom/squareup/text/PercentageFormatter;->format(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
