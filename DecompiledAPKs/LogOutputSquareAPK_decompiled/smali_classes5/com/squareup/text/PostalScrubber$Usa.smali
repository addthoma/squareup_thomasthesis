.class Lcom/squareup/text/PostalScrubber$Usa;
.super Lcom/squareup/text/PostalScrubber;
.source "PostalScrubber.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/PostalScrubber;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Usa"
.end annotation


# static fields
.field private static final DASH_POSITION:I = 0x5

.field private static final MAX_LENGTH:I = 0x9


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/squareup/text/PostalScrubber;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/text/PostalScrubber$1;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/squareup/text/PostalScrubber$Usa;-><init>()V

    return-void
.end method


# virtual methods
.method protected isValidFormattedPostalCode(Ljava/lang/String;)Z
    .locals 0

    .line 78
    invoke-static {p1}, Lcom/squareup/text/PostalCodes;->isUsZipCode(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method protected parsePostal(Ljava/lang/String;)Lcom/squareup/text/PostalScrubber$Result;
    .locals 11

    .line 82
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 83
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 86
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    const/4 v6, 0x1

    if-ge v3, v0, :cond_4

    .line 89
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 91
    invoke-static {v7}, Ljava/lang/Character;->isDigit(C)Z

    move-result v8

    const/16 v9, 0x2d

    const/4 v10, 0x5

    if-eqz v8, :cond_1

    .line 92
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-ne v8, v10, :cond_0

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 94
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_1
    if-ne v3, v10, :cond_2

    if-ne v7, v9, :cond_2

    const/4 v7, 0x1

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    :goto_1
    xor-int/2addr v7, v6

    or-int/2addr v5, v7

    :goto_2
    const/16 v7, 0x9

    if-lt v4, v7, :cond_3

    goto :goto_3

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 107
    :cond_4
    :goto_3
    new-instance p1, Lcom/squareup/text/PostalScrubber$Result;

    xor-int/lit8 v0, v5, 0x1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/squareup/text/PostalScrubber$Result;-><init>(ZLjava/lang/String;)V

    return-object p1
.end method
