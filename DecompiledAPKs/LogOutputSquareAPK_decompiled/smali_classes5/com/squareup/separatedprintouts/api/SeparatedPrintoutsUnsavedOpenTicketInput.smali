.class public final Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;
.super Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;
.source "SeparatedPrintoutsInput.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0013\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0007H\u00c6\u0003J\u000f\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0005H\u00c6\u0003JA\u0010\u001f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u000e\u0008\u0002\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010 \u001a\u00020\u000e2\u0008\u0010!\u001a\u0004\u0018\u00010\"H\u00d6\u0003J\t\u0010#\u001a\u00020$H\u00d6\u0001J\t\u0010%\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\r\u001a\u00020\u000eX\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0014\u0010\u000b\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0016R\u0017\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;",
        "openTicketOrderSnapshot",
        "Lcom/squareup/payment/OrderSnapshot;",
        "transactionOrderName",
        "",
        "openTicket",
        "Lcom/squareup/tickets/OpenTicket;",
        "voidedItems",
        "",
        "Lcom/squareup/checkout/CartItem;",
        "transactionUniqueKey",
        "(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/lang/String;)V",
        "buyerRequestedPaperReceipt",
        "",
        "getBuyerRequestedPaperReceipt",
        "()Z",
        "getOpenTicket",
        "()Lcom/squareup/tickets/OpenTicket;",
        "getOpenTicketOrderSnapshot",
        "()Lcom/squareup/payment/OrderSnapshot;",
        "getTransactionOrderName",
        "()Ljava/lang/String;",
        "getTransactionUniqueKey",
        "getVoidedItems",
        "()Ljava/util/List;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "separated-printouts_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerRequestedPaperReceipt:Z

.field private final openTicket:Lcom/squareup/tickets/OpenTicket;

.field private final openTicketOrderSnapshot:Lcom/squareup/payment/OrderSnapshot;

.field private final transactionOrderName:Ljava/lang/String;

.field private final transactionUniqueKey:Ljava/lang/String;

.field private final voidedItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/OrderSnapshot;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "openTicketOrderSnapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionOrderName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "openTicket"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voidedItems"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionUniqueKey"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 84
    invoke-direct {p0, v0}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->openTicketOrderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    iput-object p2, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->transactionOrderName:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->openTicket:Lcom/squareup/tickets/OpenTicket;

    iput-object p4, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->voidedItems:Ljava/util/List;

    iput-object p5, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->transactionUniqueKey:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->openTicketOrderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->transactionOrderName:Ljava/lang/String;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->openTicket:Lcom/squareup/tickets/OpenTicket;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->voidedItems:Ljava/util/List;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    invoke-virtual {p0}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->getTransactionUniqueKey()Ljava/lang/String;

    move-result-object p5

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->copy(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/payment/OrderSnapshot;
    .locals 1

    iget-object v0, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->openTicketOrderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->transactionOrderName:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/squareup/tickets/OpenTicket;
    .locals 1

    iget-object v0, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->openTicket:Lcom/squareup/tickets/OpenTicket;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->voidedItems:Ljava/util/List;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->getTransactionUniqueKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/OrderSnapshot;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;"
        }
    .end annotation

    const-string v0, "openTicketOrderSnapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionOrderName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "openTicket"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voidedItems"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionUniqueKey"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;-><init>(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;

    iget-object v0, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->openTicketOrderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    iget-object v1, p1, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->openTicketOrderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->transactionOrderName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->transactionOrderName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->openTicket:Lcom/squareup/tickets/OpenTicket;

    iget-object v1, p1, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->openTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->voidedItems:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->voidedItems:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->getTransactionUniqueKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->getTransactionUniqueKey()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getBuyerRequestedPaperReceipt()Z
    .locals 1

    .line 85
    iget-boolean v0, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->buyerRequestedPaperReceipt:Z

    return v0
.end method

.method public final getOpenTicket()Lcom/squareup/tickets/OpenTicket;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->openTicket:Lcom/squareup/tickets/OpenTicket;

    return-object v0
.end method

.method public final getOpenTicketOrderSnapshot()Lcom/squareup/payment/OrderSnapshot;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->openTicketOrderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    return-object v0
.end method

.method public final getTransactionOrderName()Ljava/lang/String;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->transactionOrderName:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionUniqueKey()Ljava/lang/String;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->transactionUniqueKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getVoidedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 82
    iget-object v0, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->voidedItems:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->openTicketOrderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->transactionOrderName:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->openTicket:Lcom/squareup/tickets/OpenTicket;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->voidedItems:Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->getTransactionUniqueKey()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SeparatedPrintoutsUnsavedOpenTicketInput(openTicketOrderSnapshot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->openTicketOrderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", transactionOrderName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->transactionOrderName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", openTicket="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->openTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", voidedItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->voidedItems:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", transactionUniqueKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;->getTransactionUniqueKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
