.class public abstract Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;
.super Ljava/lang/Object;
.source "SeparatedPrintoutsInput.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;,
        Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u000b2\u00020\u0001:\u0002\u000b\u000cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u0082\u0001\u0002\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;",
        "",
        "()V",
        "buyerRequestedPaperReceipt",
        "",
        "getBuyerRequestedPaperReceipt",
        "()Z",
        "transactionUniqueKey",
        "",
        "getTransactionUniqueKey",
        "()Ljava/lang/String;",
        "Companion",
        "PaperReceiptType",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsReceiptsInput;",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;",
        "separated-printouts_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;->Companion:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;-><init>()V

    return-void
.end method

.method public static final forUnsavedOpenTicket(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/OrderSnapshot;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;->Companion:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;->forUnsavedOpenTicket(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object p0

    return-object p0
.end method

.method public static final noPaperReceipt(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;->Companion:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;->noPaperReceipt(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object p0

    return-object p0
.end method

.method public static final withPaperReceipt(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;->Companion:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;->withPaperReceipt(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public abstract getBuyerRequestedPaperReceipt()Z
.end method

.method public abstract getTransactionUniqueKey()Ljava/lang/String;
.end method
