.class public final Lcom/squareup/textappearance/FontFamily;
.super Ljava/lang/Object;
.source "FontFamily.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/textappearance/FontFamily$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFontFamily.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FontFamily.kt\ncom/squareup/textappearance/FontFamily\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n+ 3 XmlResourceParsing.kt\ncom/squareup/android/xml/XmlResourceParsingKt\n+ 4 XmlResourceParsing.kt\ncom/squareup/android/xml/TagVisitor\n*L\n1#1,95:1\n10930#2,2:96\n22#3,3:98\n38#3:101\n26#3,2:127\n180#4:102\n165#4,6:103\n181#4,4:109\n207#4:113\n192#4,5:114\n208#4,4:119\n197#4,2:123\n212#4:125\n185#4:126\n*E\n*S KotlinDebug\n*F\n+ 1 FontFamily.kt\ncom/squareup/textappearance/FontFamily\n*L\n35#1,2:96\n36#1,3:98\n36#1:101\n36#1,2:127\n36#1:102\n36#1,6:103\n36#1,4:109\n36#1:113\n36#1,5:114\n36#1,4:119\n36#1,2:123\n36#1:125\n36#1:126\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB\u0019\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J \u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000e\u001a\u00020\tR\u001a\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/textappearance/FontFamily;",
        "",
        "context",
        "Landroid/content/Context;",
        "fontFamilyId",
        "",
        "(Landroid/content/Context;I)V",
        "weights",
        "Ljava/util/EnumMap;",
        "Lcom/squareup/textappearance/Style;",
        "Landroid/util/SparseIntArray;",
        "getFont",
        "Landroid/graphics/Typeface;",
        "weight",
        "style",
        "Companion",
        "textappearance_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/textappearance/FontFamily$Companion;

.field private static final fontFamilyCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/textappearance/FontFamily;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final weights:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap<",
            "Lcom/squareup/textappearance/Style;",
            "Landroid/util/SparseIntArray;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/textappearance/FontFamily$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/textappearance/FontFamily$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/textappearance/FontFamily;->Companion:Lcom/squareup/textappearance/FontFamily$Companion;

    .line 76
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    sput-object v0, Lcom/squareup/textappearance/FontFamily;->fontFamilyCache:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 16

    move-object/from16 v1, p0

    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/EnumMap;

    const-class v2, Lcom/squareup/textappearance/Style;

    invoke-direct {v0, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, v1, Lcom/squareup/textappearance/FontFamily;->weights:Ljava/util/EnumMap;

    .line 35
    invoke-static {}, Lcom/squareup/textappearance/Style;->values()[Lcom/squareup/textappearance/Style;

    move-result-object v0

    .line 96
    array-length v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_0

    aget-object v5, v0, v4

    .line 35
    iget-object v6, v1, Lcom/squareup/textappearance/FontFamily;->weights:Ljava/util/EnumMap;

    check-cast v6, Ljava/util/Map;

    new-instance v7, Landroid/util/SparseIntArray;

    const/4 v8, 0x4

    invoke-direct {v7, v8}, Landroid/util/SparseIntArray;-><init>(I)V

    invoke-interface {v6, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 36
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v2, "context.resources"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move/from16 v2, p2

    .line 98
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v2

    const-string v0, "getXml(resourceId)"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    :try_start_0
    new-instance v0, Lcom/squareup/android/xml/TagVisitor;

    move-object v4, v2

    check-cast v4, Lorg/xmlpull/v1/XmlPullParser;

    move-object v5, v2

    check-cast v5, Landroid/util/AttributeSet;

    invoke-direct {v0, v4, v5}, Lcom/squareup/android/xml/TagVisitor;-><init>(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V

    const-string v4, "font-family"

    .line 103
    invoke-virtual {v0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5

    if-nez v5, :cond_6

    .line 107
    invoke-virtual {v0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    invoke-static {v5}, Lcom/squareup/android/xml/XmlResourceParsingKt;->nextChildTag(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 109
    invoke-virtual {v0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-nez v5, :cond_4

    const-string v12, "font"

    .line 114
    invoke-virtual {v0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/android/xml/XmlResourceParsingKt;->nextChildTag(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_1

    .line 119
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v12}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 39
    new-instance v13, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v13}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    sget-object v4, Lcom/squareup/textappearance/Style;->NORMAL:Lcom/squareup/textappearance/Style;

    iput-object v4, v13, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 40
    new-instance v14, Lkotlin/jvm/internal/Ref$IntRef;

    invoke-direct {v14}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    iput v3, v14, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 41
    new-instance v15, Lkotlin/jvm/internal/Ref$IntRef;

    invoke-direct {v15}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    iput v3, v15, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 42
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    const-string v4, "context.theme"

    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v6, Lcom/squareup/textappearance/R$styleable;->FontFamilyFont:[I

    const-string v4, "R.styleable.FontFamilyFont"

    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v4, Lcom/squareup/textappearance/FontFamily$2$1$1$1;

    invoke-direct {v4, v13, v14, v15}, Lcom/squareup/textappearance/FontFamily$2$1$1$1;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$IntRef;Lkotlin/jvm/internal/Ref$IntRef;)V

    move-object v9, v4

    check-cast v9, Lkotlin/jvm/functions/Function1;

    const/16 v10, 0xc

    const/4 v11, 0x0

    move-object v4, v0

    invoke-static/range {v4 .. v11}, Lcom/squareup/android/xml/TagVisitor;->visitStyledAttributes$default(Lcom/squareup/android/xml/TagVisitor;Landroid/content/res/Resources$Theme;[IIILkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 55
    invoke-static/range {p0 .. p0}, Lcom/squareup/textappearance/FontFamily;->access$getWeights$p(Lcom/squareup/textappearance/FontFamily;)Ljava/util/EnumMap;

    move-result-object v4

    iget-object v5, v13, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v5, Lcom/squareup/textappearance/Style;

    invoke-virtual {v4, v5}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    check-cast v4, Landroid/util/SparseIntArray;

    iget v5, v14, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    iget v6, v15, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    invoke-virtual {v4, v5, v6}, Landroid/util/SparseIntArray;->put(II)V

    .line 123
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/android/xml/XmlResourceParsingKt;->nextSiblingTag(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 58
    :goto_1
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    invoke-interface {v2}, Landroid/content/res/XmlResourceParser;->close()V

    return-void

    .line 110
    :cond_4
    :try_start_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Root differs: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " != "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v3, Ljava/lang/Throwable;

    throw v3

    .line 107
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "No root element found"

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 105
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "Not at beginning of visitRoot"

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    .line 127
    invoke-interface {v2}, Landroid/content/res/XmlResourceParser;->close()V

    throw v0
.end method

.method public synthetic constructor <init>(Landroid/content/Context;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2}, Lcom/squareup/textappearance/FontFamily;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method public static final synthetic access$getFontFamilyCache$cp()Ljava/util/Map;
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/textappearance/FontFamily;->fontFamilyCache:Ljava/util/Map;

    return-object v0
.end method

.method public static final synthetic access$getWeights$p(Lcom/squareup/textappearance/FontFamily;)Ljava/util/EnumMap;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/textappearance/FontFamily;->weights:Ljava/util/EnumMap;

    return-object p0
.end method

.method public static synthetic getFont$default(Lcom/squareup/textappearance/FontFamily;Landroid/content/Context;ILcom/squareup/textappearance/Style;ILjava/lang/Object;)Landroid/graphics/Typeface;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 61
    sget-object p3, Lcom/squareup/textappearance/Style;->NORMAL:Lcom/squareup/textappearance/Style;

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/textappearance/FontFamily;->getFont(Landroid/content/Context;ILcom/squareup/textappearance/Style;)Landroid/graphics/Typeface;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final getFont(Landroid/content/Context;ILcom/squareup/textappearance/Style;)Landroid/graphics/Typeface;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "style"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/textappearance/FontFamily;->weights:Ljava/util/EnumMap;

    invoke-virtual {v0, p3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    if-nez p3, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast p3, Landroid/util/SparseIntArray;

    invoke-virtual {p3, p2}, Landroid/util/SparseIntArray;->get(I)I

    move-result p2

    .line 65
    invoke-static {p1, p2}, Lcom/squareup/fonts/FontsCompatKt;->getFont(Landroid/content/Context;I)Landroid/graphics/Typeface;

    move-result-object p1

    if-nez p1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    return-object p1
.end method
