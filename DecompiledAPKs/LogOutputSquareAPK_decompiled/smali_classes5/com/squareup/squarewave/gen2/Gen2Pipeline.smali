.class public Lcom/squareup/squarewave/gen2/Gen2Pipeline;
.super Ljava/lang/Object;
.source "Gen2Pipeline.java"


# instance fields
.field private final options:Lcom/squareup/squarewave/gen2/DecoderOptions;

.field private final swipeFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/gen2/DecoderOptions;)V
    .locals 4

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Pipeline;->options:Lcom/squareup/squarewave/gen2/DecoderOptions;

    .line 18
    new-instance v0, Lcom/squareup/squarewave/gen2/BidirectionalFilter;

    iget v1, p1, Lcom/squareup/squarewave/gen2/DecoderOptions;->clockingTolerance:I

    invoke-direct {v0, v1}, Lcom/squareup/squarewave/gen2/BidirectionalFilter;-><init>(I)V

    .line 21
    new-instance v1, Lcom/squareup/squarewave/gen2/AlternatingPeakFilter;

    invoke-direct {v1, v0}, Lcom/squareup/squarewave/gen2/AlternatingPeakFilter;-><init>(Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;)V

    .line 23
    new-instance v0, Lcom/squareup/squarewave/gen2/WindowPeakFilter;

    iget v2, p1, Lcom/squareup/squarewave/gen2/DecoderOptions;->windowSize:I

    iget v3, p1, Lcom/squareup/squarewave/gen2/DecoderOptions;->cutoff:I

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/squarewave/gen2/WindowPeakFilter;-><init>(Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;II)V

    .line 26
    new-instance v1, Lcom/squareup/squarewave/gen2/DelayPeakFinder;

    iget v2, p1, Lcom/squareup/squarewave/gen2/DecoderOptions;->peakFinderDelay:I

    invoke-direct {v1, v0, v2}, Lcom/squareup/squarewave/gen2/DelayPeakFinder;-><init>(Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;I)V

    .line 28
    new-instance v0, Lcom/squareup/squarewave/gen2/Denoiser;

    iget-object p1, p1, Lcom/squareup/squarewave/gen2/DecoderOptions;->denoiserOrder:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    invoke-direct {v0, v1, p1}, Lcom/squareup/squarewave/gen2/Denoiser;-><init>(Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;Lcom/squareup/squarewave/gen2/Denoiser$Order;)V

    .line 30
    new-instance p1, Lcom/squareup/squarewave/gen2/SignalMeanReductionFilter;

    invoke-direct {p1, v0}, Lcom/squareup/squarewave/gen2/SignalMeanReductionFilter;-><init>(Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;)V

    .line 32
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Pipeline;->swipeFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;
    .locals 1

    .line 40
    invoke-static {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->fromSignal(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/gen2/Gen2Swipe;

    move-result-object p1

    .line 41
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Pipeline;->swipeFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    invoke-interface {v0, p1}, Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;->hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    move-result-object p1

    return-object p1
.end method

.method public getOptions()Lcom/squareup/squarewave/gen2/DecoderOptions;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Pipeline;->options:Lcom/squareup/squarewave/gen2/DecoderOptions;

    return-object v0
.end method
