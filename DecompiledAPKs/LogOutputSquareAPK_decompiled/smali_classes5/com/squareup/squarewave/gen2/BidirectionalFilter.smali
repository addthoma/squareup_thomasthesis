.class Lcom/squareup/squarewave/gen2/BidirectionalFilter;
.super Ljava/lang/Object;
.source "BidirectionalFilter.java"

# interfaces
.implements Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;


# instance fields
.field private final filter:Lcom/squareup/squarewave/gen2/Gen2BinaryFilter;


# direct methods
.method constructor <init>(I)V
    .locals 2

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Lcom/squareup/squarewave/gen2/Gen2BinaryFilter;

    new-instance v1, Lcom/squareup/squarewave/gen2/Gen2DecoderFilter;

    invoke-direct {v1}, Lcom/squareup/squarewave/gen2/Gen2DecoderFilter;-><init>()V

    invoke-direct {v0, v1, p1}, Lcom/squareup/squarewave/gen2/Gen2BinaryFilter;-><init>(Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;I)V

    iput-object v0, p0, Lcom/squareup/squarewave/gen2/BidirectionalFilter;->filter:Lcom/squareup/squarewave/gen2/Gen2BinaryFilter;

    return-void
.end method


# virtual methods
.method public hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/BidirectionalFilter;->filter:Lcom/squareup/squarewave/gen2/Gen2BinaryFilter;

    invoke-virtual {v0, p1}, Lcom/squareup/squarewave/gen2/Gen2BinaryFilter;->hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    move-result-object v0

    .line 23
    iget-object v1, v0, Lcom/squareup/squarewave/gen2/Gen2DemodResult;->globalResult:Lcom/squareup/squarewave/decode/DemodResult;

    invoke-virtual {v1}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/BidirectionalFilter;->filter:Lcom/squareup/squarewave/gen2/Gen2BinaryFilter;

    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->reversedPeaks()Lcom/squareup/squarewave/gen2/Gen2Swipe;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/squarewave/gen2/Gen2BinaryFilter;->hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    move-result-object v0

    :goto_0
    return-object v0
.end method
