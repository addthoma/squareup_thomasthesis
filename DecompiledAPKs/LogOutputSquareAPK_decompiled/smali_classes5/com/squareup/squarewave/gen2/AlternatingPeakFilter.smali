.class public Lcom/squareup/squarewave/gen2/AlternatingPeakFilter;
.super Ljava/lang/Object;
.source "AlternatingPeakFilter.java"

# interfaces
.implements Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;


# instance fields
.field private final nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/AlternatingPeakFilter;->nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    return-void
.end method


# virtual methods
.method findStreaks([Lcom/squareup/squarewave/gen2/Peak;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/squareup/squarewave/gen2/Peak;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/squarewave/gen2/Streak;",
            ">;"
        }
    .end annotation

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 31
    array-length v1, p1

    if-lez v1, :cond_1

    .line 32
    new-instance v1, Lcom/squareup/squarewave/gen2/Streak;

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-direct {v1, v2}, Lcom/squareup/squarewave/gen2/Streak;-><init>(Lcom/squareup/squarewave/gen2/Peak;)V

    .line 33
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    array-length v2, p1

    const/4 v3, 0x1

    :goto_0
    if-ge v3, v2, :cond_1

    .line 36
    aget-object v4, p1, v3

    .line 37
    invoke-virtual {v4}, Lcom/squareup/squarewave/gen2/Peak;->polarity()I

    move-result v5

    iget v6, v1, Lcom/squareup/squarewave/gen2/Streak;->polarity:I

    if-ne v5, v6, :cond_0

    .line 38
    invoke-virtual {v1, v4}, Lcom/squareup/squarewave/gen2/Streak;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 40
    :cond_0
    new-instance v1, Lcom/squareup/squarewave/gen2/Streak;

    invoke-direct {v1, v4}, Lcom/squareup/squarewave/gen2/Streak;-><init>(Lcom/squareup/squarewave/gen2/Peak;)V

    .line 41
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;
    .locals 4

    .line 20
    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->peaks()[Lcom/squareup/squarewave/gen2/Peak;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/squarewave/gen2/AlternatingPeakFilter;->findStreaks([Lcom/squareup/squarewave/gen2/Peak;)Ljava/util/List;

    move-result-object v0

    .line 21
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/squareup/squarewave/gen2/Peak;

    const/4 v2, 0x0

    .line 22
    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_0

    .line 23
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/squarewave/gen2/Streak;

    invoke-virtual {v3}, Lcom/squareup/squarewave/gen2/Streak;->biggestPeak()Lcom/squareup/squarewave/gen2/Peak;

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 25
    :cond_0
    iget-object v2, p0, Lcom/squareup/squarewave/gen2/AlternatingPeakFilter;->nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->buildUpon()Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->peaks([Lcom/squareup/squarewave/gen2/Peak;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->streaks(Ljava/util/List;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->build()Lcom/squareup/squarewave/gen2/Gen2Swipe;

    move-result-object p1

    invoke-interface {v2, p1}, Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;->hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    move-result-object p1

    return-object p1
.end method
