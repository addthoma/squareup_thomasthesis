.class public final Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory;
.super Ljava/lang/Object;
.source "RealSquarewaveLibraryFactory.kt"

# interfaces
.implements Lcom/squareup/squarewave/library/SquarewaveLibraryFactory;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSquarewaveLibraryFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSquarewaveLibraryFactory.kt\ncom/squareup/squarewave/library/RealSquarewaveLibraryFactory\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,32:1\n127#2:33\n*E\n*S KotlinDebug\n*F\n+ 1 RealSquarewaveLibraryFactory.kt\ncom/squareup/squarewave/library/RealSquarewaveLibraryFactory\n*L\n24#1:33\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J \u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory;",
        "Lcom/squareup/squarewave/library/SquarewaveLibraryFactory;",
        "parentComponent",
        "Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;",
        "(Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;)V",
        "createSquarewave",
        "Lcom/squareup/squarewave/library/SquarewaveLibrary;",
        "messenger",
        "Lcom/squareup/cardreader/SingleCardreaderMessenger;",
        "cardreaderConnectionId",
        "Lcom/squareup/cardreader/CardreaderConnectionId;",
        "readerTypeProvider",
        "Lcom/squareup/wavpool/swipe/ReaderTypeProvider;",
        "squarewave_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final parentComponent:Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "parentComponent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory;->parentComponent:Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;

    return-void
.end method


# virtual methods
.method public createSquarewave(Lcom/squareup/cardreader/SingleCardreaderMessenger;Lcom/squareup/cardreader/CardreaderConnectionId;Lcom/squareup/wavpool/swipe/ReaderTypeProvider;)Lcom/squareup/squarewave/library/SquarewaveLibrary;
    .locals 2

    const-string v0, "messenger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardreaderConnectionId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "readerTypeProvider"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 26
    new-instance v1, Lcom/squareup/squarewave/library/SquarewaveLibraryModule;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule;-><init>(Lcom/squareup/cardreader/SingleCardreaderMessenger;Lcom/squareup/cardreader/CardreaderConnectionId;Lcom/squareup/wavpool/swipe/ReaderTypeProvider;)V

    const/4 p1, 0x0

    aput-object v1, v0, p1

    .line 27
    iget-object p1, p0, Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory;->parentComponent:Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;

    const/4 p2, 0x1

    aput-object p1, v0, p2

    .line 25
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 33
    const-class p2, Lcom/squareup/squarewave/library/SquarewaveLibraryComponent;

    invoke-static {p2, p1}, Lcom/squareup/dagger/Components;->createComponent(Ljava/lang/Class;Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/squarewave/library/SquarewaveLibraryComponent;

    .line 29
    invoke-interface {p1}, Lcom/squareup/squarewave/library/SquarewaveLibraryComponent;->squarewave()Lcom/squareup/squarewave/library/SquarewaveLibrary;

    move-result-object p1

    const-string p2, "createComponent<Squarewa\u2026     )\n    ).squarewave()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
