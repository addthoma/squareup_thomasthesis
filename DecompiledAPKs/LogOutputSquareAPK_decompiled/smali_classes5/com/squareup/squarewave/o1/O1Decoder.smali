.class public Lcom/squareup/squarewave/o1/O1Decoder;
.super Ljava/lang/Object;
.source "O1Decoder.java"

# interfaces
.implements Lcom/squareup/squarewave/o1/O1SwipeFilter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/squarewave/o1/O1Decoder$Message;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000([BII)I
    .locals 0

    .line 43
    invoke-static {p0, p1, p2}, Lcom/squareup/squarewave/o1/O1Decoder;->readInt([BII)I

    move-result p0

    return p0
.end method

.method static allStartBitsZero([Lcom/squareup/squarewave/gen2/Reading;)Z
    .locals 4

    .line 233
    array-length v0, p0

    array-length v1, p0

    rem-int/lit8 v1, v1, 0x9

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 235
    aget-object v3, p0, v2

    iget v3, v3, Lcom/squareup/squarewave/gen2/Reading;->level:I

    if-eqz v3, :cond_0

    return v1

    :cond_0
    add-int/lit8 v2, v2, 0x9

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    return p0
.end method

.method public static bigEndianBytes([Lcom/squareup/squarewave/gen2/Reading;)[B
    .locals 1

    const/4 v0, 0x1

    .line 244
    invoke-static {p0, v0}, Lcom/squareup/squarewave/o1/O1Decoder;->toBytes([Lcom/squareup/squarewave/gen2/Reading;Z)[B

    move-result-object p0

    return-object p0
.end method

.method private static isFlashError([B)Z
    .locals 3

    const/4 v0, 0x0

    .line 209
    aget-byte v1, p0, v0

    const/16 v2, 0xd

    if-eq v1, v2, :cond_0

    return v0

    .line 212
    :cond_0
    array-length v1, p0

    const/4 v2, 0x5

    if-le v1, v2, :cond_1

    aget-byte p0, p0, v2

    const/16 v1, 0x66

    if-ne p0, v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static littleEndianBytes([Lcom/squareup/squarewave/gen2/Reading;)[B
    .locals 1

    const/4 v0, 0x0

    .line 249
    invoke-static {p0, v0}, Lcom/squareup/squarewave/o1/O1Decoder;->toBytes([Lcom/squareup/squarewave/gen2/Reading;Z)[B

    move-result-object p0

    return-object p0
.end method

.method static lrcPasses([B)Z
    .locals 6

    .line 221
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    return v1

    .line 223
    :cond_0
    array-length v0, p0

    const/4 v2, 0x1

    sub-int/2addr v0, v2

    .line 224
    aget-byte v3, p0, v1

    move v4, v3

    const/4 v3, 0x1

    :goto_0
    if-ge v3, v0, :cond_1

    .line 226
    aget-byte v5, p0, v3

    xor-int/2addr v4, v5

    int-to-byte v4, v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 228
    :cond_1
    aget-byte p0, p0, v0

    if-ne v4, p0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method private static readInt([BII)I
    .locals 4

    const/4 v0, 0x0

    move v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    add-int v3, p1, p2

    if-ge v0, v3, :cond_0

    .line 486
    aget-byte v3, p0, v0

    and-int/lit16 v3, v3, 0xff

    shl-int/2addr v3, v2

    or-int/2addr v1, v3

    add-int/lit8 v2, v2, 0x8

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public static toAscii([BII)Ljava/lang/String;
    .locals 4

    sub-int v0, p2, p1

    .line 284
    new-array v0, v0, [C

    move v1, p1

    :goto_0
    if-ge v1, p2, :cond_0

    sub-int v2, v1, p1

    .line 287
    aget-byte v3, p0, v1

    and-int/lit8 v3, v3, 0x3f

    add-int/lit8 v3, v3, 0x20

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 289
    :cond_0
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v0}, Ljava/lang/String;-><init>([C)V

    return-object p0
.end method

.method private static toBytes([Lcom/squareup/squarewave/gen2/Reading;Z)[B
    .locals 10

    .line 260
    array-length v0, p0

    const/16 v1, 0x9

    div-int/2addr v0, v1

    new-array v0, v0, [B

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 265
    :goto_0
    array-length v5, v0

    if-ge v3, v5, :cond_2

    add-int/lit8 v5, v4, 0x9

    add-int/lit8 v6, v5, -0x1

    array-length v7, p0

    if-ge v6, v7, :cond_2

    const/4 v6, 0x1

    const/4 v7, 0x0

    :goto_1
    if-ge v6, v1, :cond_1

    if-eqz p1, :cond_0

    add-int/lit8 v8, v6, -0x1

    goto :goto_2

    :cond_0
    rsub-int/lit8 v8, v6, 0x8

    :goto_2
    add-int v9, v4, v6

    .line 270
    aget-object v9, p0, v9

    iget v9, v9, Lcom/squareup/squarewave/gen2/Reading;->level:I

    shl-int v8, v9, v8

    or-int/2addr v7, v8

    int-to-byte v7, v7

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 272
    :cond_1
    aput-byte v7, v0, v3

    add-int/lit8 v3, v3, 0x1

    move v4, v5

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private updateFlashErrorStats(Lcom/squareup/squarewave/Signal;[BLcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;)V
    .locals 6

    .line 178
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->O1:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    iput-object v0, p3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 179
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->O1_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    iput-object v0, p3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    const/4 v0, 0x0

    .line 180
    aget-byte v1, p2, v0

    and-int/lit16 v1, v1, 0xff

    const/4 v2, 0x4

    new-array v3, v2, [B

    .line 181
    aget-byte v2, p2, v2

    aput-byte v2, v3, v0

    const/4 v0, 0x3

    aget-byte v2, p2, v0

    const/4 v4, 0x1

    aput-byte v2, v3, v4

    const/4 v2, 0x2

    aget-byte v5, p2, v2

    aput-byte v5, v3, v2

    aget-byte v2, p2, v4

    aput-byte v2, v3, v0

    invoke-static {v3}, Lcom/squareup/squarewave/gum/Util;->unsignedBytesToHexString([B)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x6

    .line 186
    invoke-static {p2, v3, v0}, Lcom/squareup/squarewave/o1/O1Decoder;->readInt([BII)I

    move-result v0

    const/16 v3, 0x9

    .line 187
    aget-byte p2, p2, v3

    and-int/lit16 p2, p2, 0xff

    .line 189
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "0x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_hardware_id:Ljava/lang/String;

    .line 190
    sget-object v2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->O1_FLASH_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    iput-object v2, p3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 191
    new-instance p3, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError$Builder;

    invoke-direct {p3}, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError$Builder;-><init>()V

    .line 192
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p3, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError$Builder;->counter:Ljava/lang/Integer;

    .line 193
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    iput-object p2, p3, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError$Builder;->entropy:Ljava/lang/Integer;

    .line 194
    new-instance p2, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;-><init>()V

    .line 195
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->total_length:Ljava/lang/Integer;

    .line 196
    invoke-virtual {p3}, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    move-result-object p3

    iput-object p3, p2, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    .line 198
    iget-object p1, p1, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    invoke-virtual {p2}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    move-result-object p2

    iput-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    return-void
.end method

.method public static zeroize([BLcom/squareup/squarewave/o1/O1Decoder$Message;)V
    .locals 1

    const/4 v0, 0x0

    .line 202
    invoke-static {p0, v0}, Ljava/util/Arrays;->fill([BB)V

    .line 203
    invoke-virtual {p1}, Lcom/squareup/squarewave/o1/O1Decoder$Message;->zeroize()V

    return-void
.end method


# virtual methods
.method public filter(Lcom/squareup/squarewave/o1/O1Swipe;)Lcom/squareup/squarewave/o1/O1DemodResult;
    .locals 10

    .line 46
    invoke-virtual {p1}, Lcom/squareup/squarewave/o1/O1Swipe;->getSignal()Lcom/squareup/squarewave/Signal;

    move-result-object v0

    .line 47
    invoke-virtual {p1}, Lcom/squareup/squarewave/o1/O1Swipe;->getReadings()[Lcom/squareup/squarewave/gen2/Reading;

    move-result-object p1

    .line 49
    invoke-static {p1}, Lcom/squareup/squarewave/o1/O1Decoder;->allStartBitsZero([Lcom/squareup/squarewave/gen2/Reading;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 50
    new-instance p1, Lcom/squareup/squarewave/o1/O1DemodResult;

    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->failedDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_BAD_STREAM:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    invoke-direct {p1, v0, v1}, Lcom/squareup/squarewave/o1/O1DemodResult;-><init>(Lcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;)V

    return-object p1

    .line 53
    :cond_0
    invoke-static {p1}, Lcom/squareup/squarewave/o1/O1Decoder;->bigEndianBytes([Lcom/squareup/squarewave/gen2/Reading;)[B

    move-result-object v1

    .line 55
    array-length v2, v1

    if-nez v2, :cond_1

    .line 56
    new-instance p1, Lcom/squareup/squarewave/o1/O1DemodResult;

    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->failedDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_BAD_STREAM:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    invoke-direct {p1, v0, v1}, Lcom/squareup/squarewave/o1/O1DemodResult;-><init>(Lcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;)V

    return-object p1

    :cond_1
    const/4 v2, 0x0

    .line 60
    aget-byte v3, v1, v2

    and-int/lit16 v3, v3, 0xff

    .line 61
    array-length v4, v1

    const/4 v5, 0x1

    sub-int/2addr v4, v5

    if-eq v3, v4, :cond_2

    .line 62
    new-instance p1, Lcom/squareup/squarewave/o1/O1DemodResult;

    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->failedDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_LENGTH_MISMATCH:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    invoke-direct {p1, v0, v1}, Lcom/squareup/squarewave/o1/O1DemodResult;-><init>(Lcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;)V

    return-object p1

    .line 65
    :cond_2
    iget-object v3, v0, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    .line 67
    invoke-static {v1}, Lcom/squareup/squarewave/o1/O1Decoder;->isFlashError([B)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 68
    invoke-direct {p0, v0, v1, v3}, Lcom/squareup/squarewave/o1/O1Decoder;->updateFlashErrorStats(Lcom/squareup/squarewave/Signal;[BLcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;)V

    .line 69
    new-instance p1, Lcom/squareup/squarewave/o1/O1DemodResult;

    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->successfulDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    invoke-direct {p1, v0, v1}, Lcom/squareup/squarewave/o1/O1DemodResult;-><init>(Lcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;)V

    return-object p1

    .line 72
    :cond_3
    invoke-static {v1}, Lcom/squareup/squarewave/o1/O1Decoder;->lrcPasses([B)Z

    move-result v4

    if-nez v4, :cond_4

    .line 73
    new-instance p1, Lcom/squareup/squarewave/o1/O1DemodResult;

    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->failedDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_LRC:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    invoke-direct {p1, v0, v1}, Lcom/squareup/squarewave/o1/O1DemodResult;-><init>(Lcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;)V

    return-object p1

    .line 78
    :cond_4
    :try_start_0
    invoke-static {v1}, Lcom/squareup/squarewave/o1/O1Decoder$Message;->fromBytes([B)Lcom/squareup/squarewave/o1/O1Decoder$Message;

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    invoke-virtual {v4}, Lcom/squareup/squarewave/o1/O1Decoder$Message;->isError()Z

    move-result v6

    if-nez v6, :cond_5

    iget v6, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->counter:I

    const v7, 0xffffff

    if-ne v6, v7, :cond_5

    .line 85
    new-instance p1, Lcom/squareup/squarewave/o1/O1DemodResult;

    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->failedDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_INVALID_COUNTER:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    invoke-direct {p1, v0, v1}, Lcom/squareup/squarewave/o1/O1DemodResult;-><init>(Lcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;)V

    return-object p1

    .line 91
    :cond_5
    sget-object v6, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->O1:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    iput-object v6, v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 92
    sget-object v6, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->O1_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    iput-object v6, v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    .line 93
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "0x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->hardwareId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_hardware_id:Ljava/lang/String;

    .line 94
    iget-object v6, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->swipeDirection:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    iput-object v6, v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    .line 96
    new-instance v6, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;

    invoke-direct {v6}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;-><init>()V

    .line 97
    iget v7, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->length:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, v6, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->total_length:Ljava/lang/Integer;

    .line 100
    invoke-virtual {v4}, Lcom/squareup/squarewave/o1/O1Decoder$Message;->isError()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 101
    invoke-static {v1, v4}, Lcom/squareup/squarewave/o1/O1Decoder;->zeroize([BLcom/squareup/squarewave/o1/O1Decoder$Message;)V

    .line 102
    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->O1_GENERAL_ERROR:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    iput-object v1, v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 103
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->track2_read_success:Ljava/lang/Boolean;

    .line 105
    new-instance v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;-><init>()V

    .line 107
    iget-object v3, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    iput-object v3, v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->error_code:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    .line 108
    iget v3, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->resets:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->resets:Ljava/lang/Integer;

    .line 109
    iget v3, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->wakeups:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->wakeups:Ljava/lang/Integer;

    .line 110
    iget v3, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->status:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->status:Ljava/lang/Integer;

    .line 111
    iget v3, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->startPeriod:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->start_period:Ljava/lang/Integer;

    .line 112
    iget v3, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->endPeriod:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->end_period:Ljava/lang/Integer;

    .line 114
    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    move-result-object v1

    iput-object v1, v6, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    .line 116
    invoke-static {p1}, Lcom/squareup/squarewave/o1/O1Decoder;->littleEndianBytes([Lcom/squareup/squarewave/gen2/Reading;)[B

    move-result-object p1

    .line 117
    invoke-static {p1}, Lcom/squareup/squarewave/util/Base64;->encode([B)[C

    move-result-object v1

    .line 120
    invoke-static {p1, v2}, Ljava/util/Arrays;->fill([BB)V

    .line 121
    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([CC)V

    .line 124
    iget-object p1, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->NO_LEADING_ZEROS_DETECTED:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    if-ne p1, v1, :cond_6

    .line 125
    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->activelyIgnoredSignal()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object p1

    goto :goto_0

    .line 127
    :cond_6
    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->successfulDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object p1

    .line 129
    :goto_0
    new-instance v1, Lcom/squareup/squarewave/o1/O1DemodResult;

    sget-object v2, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    invoke-direct {v1, p1, v2}, Lcom/squareup/squarewave/o1/O1DemodResult;-><init>(Lcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;)V

    goto/16 :goto_2

    .line 132
    :cond_7
    invoke-static {v1, v4}, Lcom/squareup/squarewave/o1/O1Decoder;->zeroize([BLcom/squareup/squarewave/o1/O1Decoder$Message;)V

    .line 134
    invoke-static {p1}, Lcom/squareup/squarewave/o1/O1Decoder;->littleEndianBytes([Lcom/squareup/squarewave/gen2/Reading;)[B

    move-result-object p1

    .line 135
    invoke-static {p1}, Lcom/squareup/squarewave/util/Base64;->encode([B)[C

    move-result-object v1

    .line 137
    iget-object v7, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->last4OfPan:[C

    if-nez v7, :cond_8

    const-string v7, ""

    goto :goto_1

    :cond_8
    new-instance v7, Ljava/lang/String;

    iget-object v8, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->last4OfPan:[C

    invoke-direct {v7, v8}, Ljava/lang/String;-><init>([C)V

    .line 139
    :goto_1
    new-instance v8, Lcom/squareup/Card$Builder;

    invoke-direct {v8}, Lcom/squareup/Card$Builder;-><init>()V

    sget-object v9, Lcom/squareup/Card$InputType;->O1_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    invoke-virtual {v8, v9}, Lcom/squareup/Card$Builder;->inputType(Lcom/squareup/Card$InputType;)Lcom/squareup/Card$Builder;

    move-result-object v8

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v1}, Ljava/lang/String;-><init>([C)V

    .line 140
    invoke-virtual {v8, v9}, Lcom/squareup/Card$Builder;->trackData(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v8

    .line 141
    invoke-virtual {v8, v7}, Lcom/squareup/Card$Builder;->pan(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v7

    iget-object v8, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->brand:Lcom/squareup/Card$Brand;

    .line 142
    invoke-virtual {v7, v8}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object v7

    .line 143
    invoke-virtual {v7}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object v7

    .line 146
    invoke-static {p1, v2}, Ljava/util/Arrays;->fill([BB)V

    .line 147
    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([CC)V

    .line 148
    iget-object p1, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->last4OfPan:[C

    if-eqz p1, :cond_9

    .line 149
    iget-object p1, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->last4OfPan:[C

    invoke-static {p1, v2}, Ljava/util/Arrays;->fill([CC)V

    .line 152
    :cond_9
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->O1_SUCCESSFUL_SWIPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    iput-object p1, v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 153
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->track2_read_success:Ljava/lang/Boolean;

    .line 154
    invoke-virtual {v7}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/squarewave/gum/Mapping;->mapBrandToIssuer(Lcom/squareup/Card$Brand;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    move-result-object p1

    iput-object p1, v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    .line 156
    new-instance p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;-><init>()V

    .line 157
    iget v1, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->counter:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->counter:Ljava/lang/Integer;

    .line 158
    iget v1, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->entropy:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->entropy:Ljava/lang/Integer;

    .line 159
    iget v1, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->authenticatedLength:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->authenticated_length:Ljava/lang/Integer;

    .line 160
    iget v1, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->resets:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->resets:Ljava/lang/Integer;

    .line 161
    iget v1, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->wakeups:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->wakeups:Ljava/lang/Integer;

    .line 162
    iget v1, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->status:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->status:Ljava/lang/Integer;

    .line 163
    iget v1, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->startPeriod:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->start_period:Ljava/lang/Integer;

    .line 164
    iget v1, v4, Lcom/squareup/squarewave/o1/O1Decoder$Message;->endPeriod:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->end_period:Ljava/lang/Integer;

    .line 165
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    move-result-object p1

    iput-object p1, v6, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    .line 167
    new-instance v1, Lcom/squareup/squarewave/o1/O1DemodResult;

    .line 168
    invoke-static {v7, v2, v5}, Lcom/squareup/squarewave/decode/DemodResult;->successfulDemodWithCard(Lcom/squareup/Card;ZZ)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object p1

    sget-object v2, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    invoke-direct {v1, p1, v2}, Lcom/squareup/squarewave/o1/O1DemodResult;-><init>(Lcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;)V

    .line 171
    :goto_2
    iget-object p1, v0, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    invoke-virtual {v6}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    return-object v1

    .line 80
    :catch_0
    new-instance p1, Lcom/squareup/squarewave/o1/O1DemodResult;

    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->failedDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_DECODE:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    invoke-direct {p1, v0, v1}, Lcom/squareup/squarewave/o1/O1DemodResult;-><init>(Lcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;)V

    return-object p1
.end method
