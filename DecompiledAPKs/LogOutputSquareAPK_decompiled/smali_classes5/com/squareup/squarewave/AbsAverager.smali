.class public Lcom/squareup/squarewave/AbsAverager;
.super Ljava/lang/Object;
.source "AbsAverager.java"


# instance fields
.field private final buf:[I

.field private final exp:I

.field private index:I

.field private sum:I


# direct methods
.method public constructor <init>(I)V
    .locals 3

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput p1, p0, Lcom/squareup/squarewave/AbsAverager;->exp:I

    const/4 v0, 0x1

    shl-int/2addr v0, p1

    .line 19
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/squareup/squarewave/AbsAverager;->buf:[I

    const/16 v0, 0xf

    if-gt p1, v0, :cond_0

    return-void

    .line 23
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exp must be <= 15, but is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public add(S)Z
    .locals 4

    if-gez p1, :cond_0

    neg-int p1, p1

    .line 29
    :cond_0
    iget v0, p0, Lcom/squareup/squarewave/AbsAverager;->sum:I

    iget-object v1, p0, Lcom/squareup/squarewave/AbsAverager;->buf:[I

    iget v2, p0, Lcom/squareup/squarewave/AbsAverager;->index:I

    aget v3, v1, v2

    sub-int/2addr v0, v3

    add-int/2addr v0, p1

    iput v0, p0, Lcom/squareup/squarewave/AbsAverager;->sum:I

    add-int/lit8 v0, v2, 0x1

    .line 30
    iput v0, p0, Lcom/squareup/squarewave/AbsAverager;->index:I

    aput p1, v1, v2

    .line 31
    iget p1, p0, Lcom/squareup/squarewave/AbsAverager;->index:I

    array-length v0, v1

    const/4 v1, 0x0

    if-ne p1, v0, :cond_1

    .line 32
    iput v1, p0, Lcom/squareup/squarewave/AbsAverager;->index:I

    const/4 p1, 0x1

    return p1

    :cond_1
    return v1
.end method

.method public average()I
    .locals 2

    .line 39
    iget v0, p0, Lcom/squareup/squarewave/AbsAverager;->sum:I

    iget v1, p0, Lcom/squareup/squarewave/AbsAverager;->exp:I

    shr-int/2addr v0, v1

    return v0
.end method

.method public length()I
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/squarewave/AbsAverager;->buf:[I

    array-length v0, v0

    return v0
.end method
