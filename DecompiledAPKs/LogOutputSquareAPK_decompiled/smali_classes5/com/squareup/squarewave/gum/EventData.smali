.class public Lcom/squareup/squarewave/gum/EventData;
.super Ljava/lang/Object;
.source "EventData.java"


# instance fields
.field public final carrierDetectInfo:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

.field public final earlyPacket:Z

.field public final event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

.field public final linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

.field public final numSamples:I

.field public final packetStart:J

.field public final signal:[S


# direct methods
.method public constructor <init>(IIJI[SZLcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;)V
    .locals 1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {p1}, Lcom/squareup/squarewave/gum/Mapping;->lcrEventToLogReaderCarrierDetectEvent(I)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/squarewave/gum/EventData;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    .line 29
    iget-object p1, p0, Lcom/squareup/squarewave/gum/EventData;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->SIGNAL_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    if-ne p1, v0, :cond_0

    .line 30
    invoke-static {p2}, Lcom/squareup/squarewave/gum/Mapping;->lcrLinkTypeToLogSignalFoundLinkType(I)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/squarewave/gum/EventData;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 32
    iput-object p1, p0, Lcom/squareup/squarewave/gum/EventData;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 34
    :goto_0
    iput-wide p3, p0, Lcom/squareup/squarewave/gum/EventData;->packetStart:J

    .line 35
    iput p5, p0, Lcom/squareup/squarewave/gum/EventData;->numSamples:I

    .line 36
    iput-object p6, p0, Lcom/squareup/squarewave/gum/EventData;->signal:[S

    .line 37
    iput-boolean p7, p0, Lcom/squareup/squarewave/gum/EventData;->earlyPacket:Z

    .line 38
    iput-object p8, p0, Lcom/squareup/squarewave/gum/EventData;->carrierDetectInfo:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    return-void
.end method
