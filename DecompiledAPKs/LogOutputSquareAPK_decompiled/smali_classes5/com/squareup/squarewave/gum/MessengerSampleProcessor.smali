.class public final Lcom/squareup/squarewave/gum/MessengerSampleProcessor;
.super Ljava/lang/Object;
.source "MessengerSampleProcessor.kt"

# interfaces
.implements Lcom/squareup/squarewave/gum/SampleProcessor;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u000c\u001a\u00020\rH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/squarewave/gum/MessengerSampleProcessor;",
        "Lcom/squareup/squarewave/gum/SampleProcessor;",
        "messenger",
        "Lcom/squareup/cardreader/SingleCardreaderMessenger;",
        "(Lcom/squareup/cardreader/SingleCardreaderMessenger;)V",
        "feedSamples",
        "",
        "samples",
        "Ljava/nio/ByteBuffer;",
        "bytesRead",
        "",
        "setLegacyReaderType",
        "readerType",
        "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;",
        "squarewave_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final messenger:Lcom/squareup/cardreader/SingleCardreaderMessenger;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/SingleCardreaderMessenger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "messenger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/squarewave/gum/MessengerSampleProcessor;->messenger:Lcom/squareup/cardreader/SingleCardreaderMessenger;

    return-void
.end method


# virtual methods
.method public feedSamples(Ljava/nio/ByteBuffer;I)V
    .locals 2

    const-string v0, "samples"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iget-object v0, p0, Lcom/squareup/squarewave/gum/MessengerSampleProcessor;->messenger:Lcom/squareup/cardreader/SingleCardreaderMessenger;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$FeedSamples;

    invoke-direct {v1, p1, p2}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$FeedSamples;-><init>(Ljava/nio/ByteBuffer;I)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SingleCardreaderMessenger;->send(Lcom/squareup/cardreader/ReaderMessage$ReaderInput;)V

    return-void
.end method

.method public setLegacyReaderType(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;)V
    .locals 2

    const-string v0, "readerType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iget-object v0, p0, Lcom/squareup/squarewave/gum/MessengerSampleProcessor;->messenger:Lcom/squareup/cardreader/SingleCardreaderMessenger;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$SetLegacyReaderType;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$SetLegacyReaderType;-><init>(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SingleCardreaderMessenger;->send(Lcom/squareup/cardreader/ReaderMessage$ReaderInput;)V

    return-void
.end method
