.class public Lcom/squareup/squarewave/gum/Util;
.super Ljava/lang/Object;
.source "Util.java"


# static fields
.field private static final HEX_DIGITS:Ljava/lang/String; = "0123456789abcdef"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static unsignedByteToInt(B)I
    .locals 0

    and-int/lit16 p0, p0, 0xff

    return p0
.end method

.method public static varargs unsignedBytesToHexString([B)Ljava/lang/String;
    .locals 8

    .line 11
    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [C

    .line 13
    array-length v1, p0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-byte v4, p0, v2

    add-int/lit8 v5, v3, 0x1

    shr-int/lit8 v6, v4, 0x4

    and-int/lit8 v6, v6, 0xf

    const-string v7, "0123456789abcdef"

    .line 14
    invoke-virtual {v7, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    aput-char v6, v0, v3

    add-int/lit8 v3, v5, 0x1

    and-int/lit8 v4, v4, 0xf

    .line 15
    invoke-virtual {v7, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    aput-char v4, v0, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 17
    :cond_0
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v0}, Ljava/lang/String;-><init>([C)V

    return-object p0
.end method

.method public static unsignedBytesToLong([B)J
    .locals 6

    .line 47
    array-length v0, p0

    if-eqz v0, :cond_2

    .line 51
    array-length v0, p0

    const/4 v1, 0x7

    if-gt v0, v1, :cond_1

    const-wide/16 v0, 0x0

    .line 56
    array-length v2, p0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-byte v4, p0, v3

    const/16 v5, 0x8

    shl-long/2addr v0, v5

    .line 58
    invoke-static {v4}, Lcom/squareup/squarewave/gum/Util;->unsignedByteToInt(B)I

    move-result v4

    int-to-long v4, v4

    or-long/2addr v0, v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-wide v0

    .line 52
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Too many unsigned bytes to fit in a (signed) long"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 48
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Can\'t convert a zero-length array"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static unsignedBytesToLongLittleEndian([B)J
    .locals 5

    .line 28
    array-length v0, p0

    const/4 v1, 0x0

    .line 29
    :goto_0
    div-int/lit8 v2, v0, 0x2

    if-ge v1, v2, :cond_0

    .line 30
    aget-byte v2, p0, v1

    sub-int v3, v0, v1

    add-int/lit8 v3, v3, -0x1

    .line 32
    aget-byte v4, p0, v3

    aput-byte v4, p0, v1

    .line 33
    aput-byte v2, p0, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 35
    :cond_0
    invoke-static {p0}, Lcom/squareup/squarewave/gum/Util;->unsignedBytesToLong([B)J

    move-result-wide v0

    return-wide v0
.end method
