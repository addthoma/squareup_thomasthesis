.class public final Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$receiptReaderRegistrar$1;
.super Lcom/squareup/workflow/LifecycleWorker;
.source "RealReceiptWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/analytics/Analytics;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;Lcom/squareup/checkoutflow/receipt/YieldToFlowWorker;Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;Lcom/squareup/checkoutflow/receipt/PaperReceiptSender;Lcom/squareup/checkoutflow/receipt/ReceiptDecliner;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "com/squareup/checkoutflow/receipt/RealReceiptWorkflow$receiptReaderRegistrar$1",
        "Lcom/squareup/workflow/LifecycleWorker;",
        "onStarted",
        "",
        "onStopped",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 132
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$receiptReaderRegistrar$1;->this$0:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;

    invoke-direct {p0}, Lcom/squareup/workflow/LifecycleWorker;-><init>()V

    return-void
.end method


# virtual methods
.method public onStarted()V
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$receiptReaderRegistrar$1;->this$0:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;

    invoke-static {v0}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->access$getReceiptReaderHandler$p(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$receiptReaderRegistrar$1;->this$0:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;

    invoke-static {v1}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->access$getCardReaderHub$p(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/cardreader/CardReaderHub;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;->register(Lcom/squareup/cardreader/CardReaderHub;)V

    return-void
.end method

.method public onStopped()V
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$receiptReaderRegistrar$1;->this$0:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;

    invoke-static {v0}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->access$getReceiptReaderHandler$p(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$receiptReaderRegistrar$1;->this$0:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;

    invoke-static {v1}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->access$getCardReaderHub$p(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/cardreader/CardReaderHub;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;->unregister(Lcom/squareup/cardreader/CardReaderHub;)V

    return-void
.end method
