.class final Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$4;
.super Lkotlin/jvm/internal/Lambda;
.source "RealReceiptWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->render(Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/checkoutflow/receipt/ReceiptState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
        "+",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
        "it",
        "",
        "invoke",
        "(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/checkoutflow/receipt/ReceiptState;

.field final synthetic this$0:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;Lcom/squareup/checkoutflow/receipt/ReceiptState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$4;->this$0:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$4;->$state:Lcom/squareup/checkoutflow/receipt/ReceiptState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$4;->this$0:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;

    invoke-static {p1}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->access$getAnalytics$p(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_DECLINE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 215
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$4;->this$0:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;

    invoke-static {p1}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->access$getReceiptDecliner$p(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/checkoutflow/receipt/ReceiptDecliner;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptDecliner;->declineReceipt()V

    .line 216
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 217
    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptState;

    .line 218
    new-instance v1, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;

    sget-object v2, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DeclineReceipt;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DeclineReceipt;

    check-cast v2, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    sget-object v3, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;->NO_SMS_MARKETING:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    invoke-direct {v1, v2, v3}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;)V

    check-cast v1, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$4;->$state:Lcom/squareup/checkoutflow/receipt/ReceiptState;

    invoke-virtual {v2}, Lcom/squareup/checkoutflow/receipt/ReceiptState;->getRequestCreator()Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    .line 217
    invoke-direct {v0, v1, v2}, Lcom/squareup/checkoutflow/receipt/ReceiptState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;Lcom/squareup/picasso/RequestCreator;)V

    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 216
    invoke-static {p1, v0, v1, v2, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 115
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$4;->invoke(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
