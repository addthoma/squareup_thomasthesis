.class public final Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;
.super Ljava/lang/Object;
.source "ReceiptScreenViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogFactory$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputLayoutRunner$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogFactory$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputLayoutRunner$Factory;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 45
    iput-object p5, p0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 46
    iput-object p6, p0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 47
    iput-object p7, p0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;->arg6Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogFactory$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputLayoutRunner$Factory;",
            ">;)",
            "Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;"
        }
    .end annotation

    .line 63
    new-instance v8, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$Factory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogFactory$Factory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputLayoutRunner$Factory;)Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory;
    .locals 9

    .line 71
    new-instance v8, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$Factory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogFactory$Factory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputLayoutRunner$Factory;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory;
    .locals 8

    .line 52
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$Factory;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogFactory$Factory;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputLayoutRunner$Factory;

    invoke-static/range {v1 .. v7}, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;->newInstance(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$Factory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogFactory$Factory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputLayoutRunner$Factory;)Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory_Factory;->get()Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory;

    move-result-object v0

    return-object v0
.end method
