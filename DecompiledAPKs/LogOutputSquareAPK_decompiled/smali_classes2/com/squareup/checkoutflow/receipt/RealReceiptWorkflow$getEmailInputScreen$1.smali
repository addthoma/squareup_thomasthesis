.class final Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getEmailInputScreen$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealReceiptWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->getEmailInputScreen(Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/workflow/RenderContext;Lcom/squareup/picasso/RequestCreator;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
        "+",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
        "it",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/checkoutflow/receipt/ReceiptInput;

.field final synthetic $requestCreator:Lcom/squareup/picasso/RequestCreator;

.field final synthetic this$0:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/picasso/RequestCreator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getEmailInputScreen$1;->this$0:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getEmailInputScreen$1;->$props:Lcom/squareup/checkoutflow/receipt/ReceiptInput;

    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getEmailInputScreen$1;->$requestCreator:Lcom/squareup/picasso/RequestCreator;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 351
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getEmailInputScreen$1;->this$0:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;

    invoke-static {v0}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->access$getAnalytics$p(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_INPUT_EMAIL_SEND:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 355
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getEmailInputScreen$1;->this$0:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;

    invoke-static {v0}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->access$getDigitalReceiptSender$p(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;->trySendEmailReceipt(Ljava/lang/String;)Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$EmailDestination;

    move-result-object p1

    const/4 v0, 0x2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 357
    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getEmailInputScreen$1;->this$0:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;

    invoke-static {v2}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->access$getCheckoutInformationEventLogger$p(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/log/CheckoutInformationEventLogger;

    move-result-object v2

    .line 358
    iget-object v3, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getEmailInputScreen$1;->$props:Lcom/squareup/checkoutflow/receipt/ReceiptInput;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getPaymentTypeInfo()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v4, "props.paymentTypeInfo.tenderedAmount.amount"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v5, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getEmailInputScreen$1;->$props:Lcom/squareup/checkoutflow/receipt/ReceiptInput;

    invoke-virtual {v5}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getBillServerId()Ljava/lang/String;

    move-result-object v5

    .line 357
    invoke-interface {v2, v3, v4, v5}, Lcom/squareup/log/CheckoutInformationEventLogger;->userProvideEmail(JLjava/lang/String;)V

    .line 360
    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 361
    new-instance v3, Lcom/squareup/checkoutflow/receipt/ReceiptState;

    .line 362
    new-instance v4, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;

    new-instance v5, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;

    check-cast p1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination;

    invoke-direct {v5, p1}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination;)V

    check-cast v5, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    sget-object p1, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;->NO_SMS_MARKETING:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    invoke-direct {v4, v5, p1}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;)V

    check-cast v4, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    .line 363
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getEmailInputScreen$1;->$requestCreator:Lcom/squareup/picasso/RequestCreator;

    .line 361
    invoke-direct {v3, v4, p1}, Lcom/squareup/checkoutflow/receipt/ReceiptState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;Lcom/squareup/picasso/RequestCreator;)V

    .line 360
    invoke-static {v2, v3, v1, v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 367
    :cond_0
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v2, Lcom/squareup/checkoutflow/receipt/ReceiptState;

    new-instance v3, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InvalidInputState;

    sget-object v4, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState$EmailInput;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState$EmailInput;

    check-cast v4, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;

    invoke-direct {v3, v4}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InvalidInputState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;)V

    check-cast v3, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    iget-object v4, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getEmailInputScreen$1;->$requestCreator:Lcom/squareup/picasso/RequestCreator;

    invoke-direct {v2, v3, v4}, Lcom/squareup/checkoutflow/receipt/ReceiptState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;Lcom/squareup/picasso/RequestCreator;)V

    invoke-static {p1, v2, v1, v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 115
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getEmailInputScreen$1;->invoke(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
