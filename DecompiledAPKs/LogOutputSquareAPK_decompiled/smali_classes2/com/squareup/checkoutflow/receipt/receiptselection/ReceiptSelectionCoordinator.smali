.class public final Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ReceiptSelectionCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReceiptSelectionCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReceiptSelectionCoordinator.kt\ncom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator\n*L\n1#1,328:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0098\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u00013BC\u0008\u0002\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0019H\u0016J\u0010\u0010\u001d\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0019H\u0002J\u001e\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020 0\u001f2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$H\u0002J.\u0010%\u001a\u0008\u0012\u0004\u0012\u00020&0\u001f2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u0006\u0010\'\u001a\u00020(2\u0006\u0010\u0002\u001a\u00020\u0005H\u0002J\u0010\u0010)\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0019H\u0016J \u0010*\u001a\u00020\u001b2\u0006\u0010+\u001a\u00020,2\u0006\u0010\u0002\u001a\u00020\u00052\u0006\u0010\u001c\u001a\u00020\u0019H\u0002J\u0012\u0010-\u001a\u00020\u001b2\u0008\u0010.\u001a\u0004\u0018\u00010/H\u0002J\u0018\u00100\u001a\u00020\u001b2\u0006\u0010+\u001a\u00020,2\u0006\u00101\u001a\u000202H\u0002R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screen",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "receiptTextHelper",
        "Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;",
        "(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;)V",
        "buyerActionBar",
        "Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;",
        "buyerActionContainer",
        "Lcom/squareup/ui/buyer/BuyerActionContainer;",
        "currentScreen",
        "languageSelectionButton",
        "Lcom/squareup/marketfont/MarketTextView;",
        "merchantImage",
        "Landroid/widget/ImageView;",
        "receiptContent",
        "Landroid/view/View;",
        "attach",
        "",
        "view",
        "bindViews",
        "createPrimaryActionOptions",
        "",
        "Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;",
        "receiptOptionsState",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;",
        "res",
        "Lcom/squareup/util/Res;",
        "createSecondaryActionOptions",
        "Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "detach",
        "update",
        "localeOverrideFactory",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "updateBackground",
        "requestCreator",
        "Lcom/squareup/picasso/RequestCreator;",
        "updateTitleAndSubtitle",
        "paymentTypeInfo",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private buyerActionContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private currentScreen:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;

.field private languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

.field private merchantImage:Landroid/widget/ImageView;

.field private receiptContent:Landroid/view/View;

.field private final receiptTextHelper:Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;

.field private final screen:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;",
            ")V"
        }
    .end annotation

    .line 59
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->screen:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object p4, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->receiptTextHelper:Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;)V

    return-void
.end method

.method public static final synthetic access$getBuyerActionBar$p(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;)Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;
    .locals 1

    .line 54
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p0, :cond_0

    const-string v0, "buyerActionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getCurrentScreen$p(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->currentScreen:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;

    return-object p0
.end method

.method public static final synthetic access$getLanguageSelectionButton$p(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;)Lcom/squareup/marketfont/MarketTextView;
    .locals 1

    .line 54
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    if-nez p0, :cond_0

    const-string v0, "languageSelectionButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getMerchantImage$p(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;)Landroid/widget/ImageView;
    .locals 1

    .line 54
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->merchantImage:Landroid/widget/ImageView;

    if-nez p0, :cond_0

    const-string v0, "merchantImage"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getReceiptContent$p(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;)Landroid/view/View;
    .locals 1

    .line 54
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->receiptContent:Landroid/view/View;

    if-nez p0, :cond_0

    const-string v0, "receiptContent"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setBuyerActionBar$p(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    return-void
.end method

.method public static final synthetic access$setCurrentScreen$p(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->currentScreen:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;

    return-void
.end method

.method public static final synthetic access$setLanguageSelectionButton$p(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;Lcom/squareup/marketfont/MarketTextView;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method public static final synthetic access$setMerchantImage$p(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;Landroid/widget/ImageView;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->merchantImage:Landroid/widget/ImageView;

    return-void
.end method

.method public static final synthetic access$setReceiptContent$p(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;Landroid/view/View;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->receiptContent:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;Landroid/view/View;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->update(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$updateBackground(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;Lcom/squareup/picasso/RequestCreator;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->updateBackground(Lcom/squareup/picasso/RequestCreator;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 321
    sget v0, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026in.R.id.buyer_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 322
    sget v0, Lcom/squareup/checkoutflow/receipt/impl/R$id;->buyer_action_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.buyer_action_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/buyer/BuyerActionContainer;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

    .line 323
    sget v0, Lcom/squareup/checkoutflow/receipt/impl/R$id;->switch_language_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.switch_language_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    .line 324
    sget v0, Lcom/squareup/checkout/R$id;->merchant_image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026kout.R.id.merchant_image)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->merchantImage:Landroid/widget/ImageView;

    .line 325
    sget v0, Lcom/squareup/checkoutflow/receipt/impl/R$id;->receipt_content:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.receipt_content)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->receiptContent:Landroid/view/View;

    return-void
.end method

.method private final createPrimaryActionOptions(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;Lcom/squareup/util/Res;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;",
            "Lcom/squareup/util/Res;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;",
            ">;"
        }
    .end annotation

    .line 242
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 244
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;->getEmailState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$EmailState;

    move-result-object v1

    if-eqz v1, :cond_6

    check-cast v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$EmailState$EmailEnabled;

    .line 245
    invoke-virtual {v1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$EmailState$EmailEnabled;->getEmailForCustomer()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    .line 246
    new-instance v2, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$EmailState$EmailEnabled;->getEmailForCustomer()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v4, Ljava/lang/CharSequence;

    invoke-direct {v2, v4}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    move-object v2, v3

    .line 251
    :goto_0
    new-instance v4, Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;

    .line 252
    new-instance v5, Lcom/squareup/util/ViewString$TextString;

    sget v6, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_email:I

    invoke-interface {p2, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-direct {v5, v6}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v5, Lcom/squareup/util/ViewString;

    new-instance v6, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$createPrimaryActionOptions$1;

    invoke-direct {v6, v1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$createPrimaryActionOptions$1;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$EmailState$EmailEnabled;)V

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 255
    check-cast v2, Lcom/squareup/util/ViewString;

    .line 256
    sget-object v1, Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize$Small;->INSTANCE:Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize$Small;

    check-cast v1, Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;

    .line 251
    invoke-direct {v4, v5, v6, v2, v1}, Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;-><init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;Lcom/squareup/util/ViewString;Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;)V

    .line 250
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 260
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;->getSmsState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$SmsState;

    move-result-object v1

    .line 261
    instance-of v2, v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$SmsState$SmsEnabled;

    if-eqz v2, :cond_4

    .line 262
    move-object v2, v1

    check-cast v2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$SmsState$SmsEnabled;

    invoke-virtual {v2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$SmsState$SmsEnabled;->getSmsForCustomer()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 263
    new-instance v4, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {v2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$SmsState$SmsEnabled;->getSmsForCustomer()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    check-cast v2, Ljava/lang/CharSequence;

    invoke-direct {v4, v2}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    move-object v4, v3

    .line 268
    :goto_1
    new-instance v2, Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;

    .line 269
    new-instance v5, Lcom/squareup/util/ViewString$TextString;

    sget v6, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_text:I

    invoke-interface {p2, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-direct {v5, v6}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v5, Lcom/squareup/util/ViewString;

    new-instance v6, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$createPrimaryActionOptions$2;

    invoke-direct {v6, v1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$createPrimaryActionOptions$2;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$SmsState;)V

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 272
    check-cast v4, Lcom/squareup/util/ViewString;

    .line 273
    sget-object v1, Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize$Small;->INSTANCE:Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize$Small;

    check-cast v1, Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;

    .line 268
    invoke-direct {v2, v5, v6, v4, v1}, Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;-><init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;Lcom/squareup/util/ViewString;Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;)V

    .line 267
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 278
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;->getPaperState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState;

    move-result-object p1

    .line 279
    instance-of v1, p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;

    if-eqz v1, :cond_5

    .line 281
    new-instance v1, Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;

    .line 282
    new-instance v2, Lcom/squareup/util/ViewString$TextString;

    sget v4, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_print:I

    invoke-interface {p2, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-direct {v2, p2}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lcom/squareup/util/ViewString;

    new-instance p2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$createPrimaryActionOptions$3;

    invoke-direct {p2, p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$createPrimaryActionOptions$3;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 286
    sget-object p1, Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize$Small;->INSTANCE:Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize$Small;

    check-cast p1, Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;

    .line 281
    invoke-direct {v1, v2, p2, v3, p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;-><init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;Lcom/squareup/util/ViewString;Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;)V

    .line 280
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    :cond_5
    check-cast v0, Ljava/util/List;

    return-object v0

    .line 244
    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.checkoutflow.receipt.receiptselection.ReceiptSelectionScreen.ReceiptOptionsState.EmailState.EmailEnabled"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final createSecondaryActionOptions(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;Lcom/squareup/util/Res;Lcom/squareup/CountryCode;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/CountryCode;",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;",
            ">;"
        }
    .end annotation

    .line 300
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 302
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;->getFormalPaperState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$FormalPaperState;

    move-result-object p1

    .line 303
    instance-of v1, p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$FormalPaperState$FormalPaperEnabled;

    if-eqz v1, :cond_0

    .line 305
    new-instance v1, Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;

    new-instance v2, Lcom/squareup/util/ViewString$TextString;

    sget v3, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_print_formal:I

    invoke-interface {p2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lcom/squareup/util/ViewString;

    new-instance v3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$createSecondaryActionOptions$1;

    invoke-direct {v3, p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$createSecondaryActionOptions$1;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$FormalPaperState;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;-><init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    .line 304
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    :cond_0
    new-instance p1, Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;

    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    invoke-static {p3}, Lcom/squareup/address/CountryResources;->noReceiptId(Lcom/squareup/CountryCode;)I

    move-result p3

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-direct {v1, p2}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v1, Lcom/squareup/util/ViewString;

    new-instance p2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$createSecondaryActionOptions$2;

    invoke-direct {p2, p4}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$createSecondaryActionOptions$2;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, v1, p2}, Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;-><init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    .line 311
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 317
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final update(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;Landroid/view/View;)V
    .locals 7

    .line 136
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    .line 138
    sget-object v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$update$1;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$update$1;

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p3, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 139
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    const-string v2, "buyerActionBar"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 140
    :cond_0
    new-instance v3, Lcom/squareup/util/ViewString$TextString;

    .line 141
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    .line 142
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;->isPaymentComplete()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 143
    sget v4, Lcom/squareup/checkout/R$string;->new_sale:I

    goto :goto_0

    .line 145
    :cond_1
    sget v4, Lcom/squareup/common/strings/R$string;->continue_label:I

    .line 141
    :goto_0
    invoke-virtual {p3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    const-string/jumbo v4, "view.context.getString(\n\u2026          }\n            )"

    invoke-static {p3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p3, Ljava/lang/CharSequence;

    .line 140
    invoke-direct {v3, p3}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/squareup/util/ViewString;

    .line 149
    new-instance p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$update$2;

    invoke-direct {p3, p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$update$2;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    .line 139
    invoke-virtual {v1, v3, p3}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setUpLeftButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    .line 151
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;->getPaymentTypeInfo()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    move-result-object p3

    invoke-direct {p0, p1, p3}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->updateTitleAndSubtitle(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)V

    .line 153
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;->getAddCardState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    move-result-object p3

    .line 155
    sget-object v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Hidden;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Hidden;

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p3, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p3}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->hideLeftGlyphButton()V

    goto :goto_1

    .line 156
    :cond_3
    instance-of v1, p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Showing;

    if-eqz v1, :cond_5

    .line 157
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v1, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SAVE_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v4, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$update$3;

    invoke-direct {v4, p3}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$update$3;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v3, v4}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setLeftGlyphButton(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 163
    :cond_5
    :goto_1
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;->getDisplayName()Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_7

    .line 164
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v1, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    new-instance v3, Lcom/squareup/util/ViewString$TextString;

    check-cast p3, Ljava/lang/CharSequence;

    invoke-direct {v3, p3}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/squareup/util/ViewString;

    invoke-virtual {v1, v3}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTicketName(Lcom/squareup/util/ViewString;)V

    .line 167
    :cond_7
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;->getUpdateCustomerState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    move-result-object p3

    .line 168
    instance-of v1, p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$AddCustomer;

    if-eqz v1, :cond_9

    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p3, :cond_8

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 169
    :cond_8
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_add_customer:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/util/ViewString;

    .line 170
    new-instance v2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$update$5;

    invoke-direct {v2, p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$update$5;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 168
    invoke-virtual {p3, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setUpRightButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    goto :goto_2

    .line 172
    :cond_9
    instance-of v1, p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$ViewCustomer;

    if-eqz v1, :cond_b

    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p3, :cond_a

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 173
    :cond_a
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_view_customer:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/util/ViewString;

    .line 174
    new-instance v2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$update$6;

    invoke-direct {v2, p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$update$6;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 172
    invoke-virtual {p3, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setUpRightButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    goto :goto_2

    .line 176
    :cond_b
    instance-of p3, p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$Hidden;

    if-eqz p3, :cond_d

    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p3, :cond_c

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {p3}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->hideRightButton()V

    .line 179
    :cond_d
    :goto_2
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

    const-string v1, "buyerActionContainer"

    if-nez p3, :cond_e

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 180
    :cond_e
    new-instance v2, Lcom/squareup/util/ViewString$TextString;

    .line 182
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;->getCallToActionState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$CallToActionState;

    move-result-object v3

    sget-object v4, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$CallToActionState;->ordinal()I

    move-result v3

    aget v3, v4, v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_10

    const/4 v5, 0x2

    if-ne v3, v5, :cond_f

    .line 187
    sget v3, Lcom/squareup/checkout/R$string;->buyer_send_receipt_subtitle:I

    goto :goto_3

    :cond_f
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 184
    :cond_10
    sget v3, Lcom/squareup/cardreader/R$string;->please_remove_card_title:I

    .line 181
    :goto_3
    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 180
    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 179
    invoke-virtual {p3, v2}, Lcom/squareup/ui/buyer/BuyerActionContainer;->setCallToAction(Lcom/squareup/util/ViewString;)V

    .line 194
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

    if-nez p3, :cond_11

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    new-instance v2, Lcom/squareup/ui/buyer/BuyerActionContainer$Config;

    .line 195
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;->getReceiptOptionsState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;

    move-result-object v3

    invoke-direct {p0, v3, v0}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->createPrimaryActionOptions(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;Lcom/squareup/util/Res;)Ljava/util/List;

    move-result-object v3

    .line 196
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;->getReceiptOptionsState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;

    move-result-object v5

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v6

    invoke-direct {p0, v5, v0, v6, p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->createSecondaryActionOptions(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;Lcom/squareup/util/Res;Lcom/squareup/CountryCode;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;)Ljava/util/List;

    move-result-object v5

    .line 194
    invoke-direct {v2, v3, v5}, Lcom/squareup/ui/buyer/BuyerActionContainer$Config;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {p3, v2}, Lcom/squareup/ui/buyer/BuyerActionContainer;->setConfig(Lcom/squareup/ui/buyer/BuyerActionContainer$Config;)V

    .line 199
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object p3

    invoke-static {p3}, Lcom/squareup/address/CountryResources;->generalReceiptDisclaimerId(Lcom/squareup/CountryCode;)I

    move-result p3

    invoke-interface {v0, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 200
    check-cast p3, Ljava/lang/CharSequence;

    invoke-static {p3}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/2addr v2, v4

    if-eqz v2, :cond_13

    .line 201
    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

    if-nez v2, :cond_12

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 202
    :cond_12
    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v1, p3}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v1, Lcom/squareup/util/ViewString;

    .line 201
    invoke-virtual {v2, v1}, Lcom/squareup/ui/buyer/BuyerActionContainer;->setHelperText(Lcom/squareup/util/ViewString;)V

    goto :goto_4

    .line 207
    :cond_13
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionContainer:Lcom/squareup/ui/buyer/BuyerActionContainer;

    if-nez p3, :cond_14

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_14
    invoke-virtual {p3}, Lcom/squareup/ui/buyer/BuyerActionContainer;->hideHelperText()V

    .line 210
    :goto_4
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    const-string v1, "languageSelectionButton"

    if-nez p3, :cond_15

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_15
    check-cast p3, Landroid/view/View;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;->getBuyerLanguageState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    move-result-object v2

    instance-of v2, v2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState$Showing;

    invoke-static {p3, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 211
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;->getBuyerLanguageState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    move-result-object p2

    .line 212
    instance-of p3, p2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState$Showing;

    if-eqz p3, :cond_19

    .line 213
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    if-nez p3, :cond_16

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_16
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocaleFormatter()Lcom/squareup/locale/LocaleFormatter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleFormatter;->displayLanguage()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_17

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 215
    :cond_17
    sget p3, Lcom/squareup/activity/R$drawable;->buyer_language_icon:I

    invoke-interface {v0, p3}, Lcom/squareup/util/Res;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    const/4 v0, 0x0

    .line 214
    invoke-virtual {p1, p3, v0, v0, v0}, Lcom/squareup/marketfont/MarketTextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 217
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_18

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 218
    :cond_18
    new-instance p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$update$7;

    invoke-direct {p3, p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$update$7;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;)V

    check-cast p3, Landroid/view/View$OnClickListener;

    invoke-static {p3}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    check-cast p2, Landroid/view/View$OnClickListener;

    .line 217
    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_19
    return-void
.end method

.method private final updateBackground(Lcom/squareup/picasso/RequestCreator;)V
    .locals 3

    if-eqz p1, :cond_2

    .line 104
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->merchantImage:Landroid/widget/ImageView;

    const-string v1, "merchantImage"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->merchantImage:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$updateBackground$$inlined$let$lambda$1;

    invoke-direct {v1, p1, p0}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$updateBackground$$inlined$let$lambda$1;-><init>(Lcom/squareup/picasso/RequestCreator;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;)V

    check-cast v1, Lcom/squareup/util/OnMeasuredCallback;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    :cond_2
    return-void
.end method

.method private final updateTitleAndSubtitle(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)V
    .locals 5

    .line 226
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    const-string v1, "buyerActionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 227
    :cond_0
    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->receiptTextHelper:Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;

    .line 228
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v4

    .line 227
    invoke-virtual {v2, v3, v4, p2}, Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;->resolveTitle(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)Lcom/squareup/util/ViewString$TextString;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 226
    invoke-virtual {v0, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    .line 231
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 232
    :cond_1
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->receiptTextHelper:Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;

    .line 233
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object p1

    .line 232
    invoke-virtual {v1, v2, p1, p2}, Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;->resolveSubtitle(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)Lcom/squareup/util/ViewString$TextString;

    move-result-object p1

    check-cast p1, Lcom/squareup/util/ViewString;

    .line 231
    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->bindViews(Landroid/view/View;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->screen:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$attach$1;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$attach$1;

    check-cast v1, Lio/reactivex/functions/BiPredicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->distinctUntilChanged(Lio/reactivex/functions/BiPredicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screen.distinctUntilChan\u2026reen.requestCreator\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$attach$2;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 92
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->screen:Lio/reactivex/Observable;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 93
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$attach$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$attach$3;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 95
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Shown ReceiptSelectionScreen"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->currentScreen:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;->getCuratedImage()Lcom/squareup/merchantimages/CuratedImage;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator;->merchantImage:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    const-string v1, "merchantImage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {p1, v0}, Lcom/squareup/merchantimages/CuratedImage;->cancelRequest(Landroid/widget/ImageView;)V

    :cond_1
    return-void
.end method
