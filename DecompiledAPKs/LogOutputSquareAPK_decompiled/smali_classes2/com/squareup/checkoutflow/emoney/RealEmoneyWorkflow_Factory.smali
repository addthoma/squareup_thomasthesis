.class public final Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealEmoneyWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;",
            ">;)",
            "Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow_Factory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;)Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;

    invoke-direct {v0, p0, p1}, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;-><init>(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;

    invoke-static {v0, v1}, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow_Factory;->newInstance(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;)Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow_Factory;->get()Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;

    move-result-object v0

    return-object v0
.end method
