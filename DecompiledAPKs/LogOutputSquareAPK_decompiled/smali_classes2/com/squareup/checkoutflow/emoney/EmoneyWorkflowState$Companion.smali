.class public final Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$Companion;
.super Ljava/lang/Object;
.source "EmoneyWorkflowState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEmoneyWorkflowState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EmoneyWorkflowState.kt\ncom/squareup/checkoutflow/emoney/EmoneyWorkflowState$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,35:1\n180#2:36\n*E\n*S KotlinDebug\n*F\n+ 1 EmoneyWorkflowState.kt\ncom/squareup/checkoutflow/emoney/EmoneyWorkflowState$Companion\n*L\n21#1:36\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$Companion;",
        "",
        "()V",
        "fromByteStringSnapshot",
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;",
        "byteString",
        "Lokio/ByteString;",
        "start",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromByteStringSnapshot(Lokio/ByteString;)Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;
    .locals 2

    const-string v0, "byteString"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 22
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p1

    .line 23
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p1

    const-string v0, "Class.forName(className)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lkotlin/jvm/JvmClassMappingKt;->getKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    .line 27
    const-class v0, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$BrandSelection;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$BrandSelection;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$BrandSelection;

    check-cast p1, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;

    goto :goto_0

    .line 28
    :cond_0
    const-class v0, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$CheckBalance;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$CheckBalance;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$CheckBalance;

    check-cast p1, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;

    :goto_0
    return-object p1

    .line 29
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid EmoneyWorkflowState class: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;
    .locals 1

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$Companion;->fromByteStringSnapshot(Lokio/ByteString;)Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;

    move-result-object p1

    return-object p1
.end method
