.class public final Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;
.super Ljava/lang/Object;
.source "RealEmoneyTenderOptionFactory.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003*\u0001\u0019\u0018\u00002\u00020\u0001BE\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\u0008\u0010\u001b\u001a\u00020\u001cH\u0016J\u0008\u0010\u001d\u001a\u00020\u001eH\u0016J\u0010\u0010\u001f\u001a\u00020\u00152\u0006\u0010 \u001a\u00020\u0014H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00150\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00170\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u001a\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;",
        "Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;",
        "res",
        "Lcom/squareup/util/Res;",
        "tenderPaymentResultHandler",
        "Ldagger/Lazy;",
        "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
        "emoneyWorkflow",
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;",
        "cardReaderHub",
        "Lcom/squareup/cardreader/CardReaderHub;",
        "cardReaderHubUtils",
        "Lcom/squareup/cardreader/CardReaderHubUtils;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)V",
        "enabledStrategy",
        "Lkotlin/Function1;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
        "",
        "titleStrategy",
        "",
        "workflow",
        "com/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1",
        "Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;",
        "getTenderOption",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;",
        "getTenderOptionKey",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;",
        "isSplitStateValid",
        "conditionalData",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field private final emoneyWorkflow:Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;

.field private final enabledStrategy:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final res:Lcom/squareup/util/Res;

.field private final tenderPaymentResultHandler:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final titleStrategy:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final workflow:Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderPaymentResultHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emoneyWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderHub"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderHubUtils"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->tenderPaymentResultHandler:Ldagger/Lazy;

    iput-object p3, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->emoneyWorkflow:Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;

    iput-object p4, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iput-object p5, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    iput-object p6, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p7, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->features:Lcom/squareup/settings/server/Features;

    .line 44
    new-instance p1, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$titleStrategy$1;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$titleStrategy$1;-><init>(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->titleStrategy:Lkotlin/jvm/functions/Function1;

    .line 58
    new-instance p1, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$enabledStrategy$1;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$enabledStrategy$1;-><init>(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->enabledStrategy:Lkotlin/jvm/functions/Function1;

    .line 74
    new-instance p1, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;-><init>(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->workflow:Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;

    return-void
.end method

.method public static final synthetic access$getAccountStatusSettings$p(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method public static final synthetic access$getCardReaderHub$p(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)Lcom/squareup/cardreader/CardReaderHub;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    return-object p0
.end method

.method public static final synthetic access$getCardReaderHubUtils$p(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)Lcom/squareup/cardreader/CardReaderHubUtils;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    return-object p0
.end method

.method public static final synthetic access$getEmoneyWorkflow$p(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->emoneyWorkflow:Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)Lcom/squareup/util/Res;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getTenderPaymentResultHandler$p(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)Ldagger/Lazy;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->tenderPaymentResultHandler:Ldagger/Lazy;

    return-object p0
.end method

.method public static final synthetic access$isSplitStateValid(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;)Z
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->isSplitStateValid(Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;)Z

    move-result p0

    return p0
.end method

.method private final isSplitStateValid(Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;)Z
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_SPLIT_EMONEY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isSplitTender()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method


# virtual methods
.method public getTenderOption()Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;
    .locals 5

    .line 102
    new-instance v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->EMONEY_ROW_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->titleStrategy:Lkotlin/jvm/functions/Function1;

    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->enabledStrategy:Lkotlin/jvm/functions/Function1;

    iget-object v4, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->workflow:Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;

    check-cast v4, Lcom/squareup/workflow/Workflow;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;-><init>(Lcom/squareup/analytics/RegisterTapName;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/Workflow;)V

    return-object v0
.end method

.method public getTenderOptionKey()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;
    .locals 3

    .line 104
    new-instance v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "javaClass.name"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
