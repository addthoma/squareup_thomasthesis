.class public final Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;
.super Ljava/lang/Object;
.source "EmoneyBrandSelectionScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EmoneyBrandRow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00052\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;",
        "",
        "stringID",
        "",
        "enabled",
        "",
        "brand",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;",
        "(IZLcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V",
        "getBrand",
        "()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;",
        "getEnabled",
        "()Z",
        "getStringID",
        "()I",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "hashCode",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

.field private final enabled:Z

.field private final stringID:I


# direct methods
.method public constructor <init>(IZLcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V
    .locals 1

    const-string v0, "brand"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->stringID:I

    iput-boolean p2, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->enabled:Z

    iput-object p3, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;IZLcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;ILjava/lang/Object;)Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget p1, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->stringID:I

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->enabled:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->copy(IZLcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->stringID:I

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->enabled:Z

    return v0
.end method

.method public final component3()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    return-object v0
.end method

.method public final copy(IZLcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;
    .locals 1

    const-string v0, "brand"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;-><init>(IZLcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;

    iget v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->stringID:I

    iget v1, p1, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->stringID:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->enabled:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->enabled:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    iget-object p1, p1, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    return-object v0
.end method

.method public final getEnabled()Z
    .locals 1

    .line 24
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->enabled:Z

    return v0
.end method

.method public final getStringID()I
    .locals 1

    .line 24
    iget v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->stringID:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->stringID:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->enabled:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EmoneyBrandRow(stringID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->stringID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->enabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
