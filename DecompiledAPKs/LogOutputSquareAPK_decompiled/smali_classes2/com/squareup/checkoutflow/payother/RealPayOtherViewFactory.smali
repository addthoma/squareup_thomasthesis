.class public final Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealPayOtherViewFactory.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/payother/PayOtherViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u000f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory;",
        "Lcom/squareup/checkoutflow/payother/PayOtherViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "payOtherLayoutRunnerFactory",
        "Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$Factory;",
        "(Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$Factory;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "payOtherLayoutRunnerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 11
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 12
    const-class v2, Lcom/squareup/checkoutflow/payother/PayOtherScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    .line 13
    sget v3, Lcom/squareup/checkoutflow/payother/impl/R$layout;->pay_other_tender_layout:I

    .line 14
    new-instance v4, Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory$1;

    invoke-direct {v4, p1}, Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory$1;-><init>(Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$Factory;)V

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 11
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 10
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
