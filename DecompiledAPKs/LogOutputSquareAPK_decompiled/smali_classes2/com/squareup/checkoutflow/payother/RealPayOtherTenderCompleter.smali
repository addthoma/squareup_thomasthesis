.class public final Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter;
.super Ljava/lang/Object;
.source "RealPayOtherTenderCompleter.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealPayOtherTenderCompleter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealPayOtherTenderCompleter.kt\ncom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,68:1\n250#2,2:69\n*E\n*S KotlinDebug\n*F\n+ 1 RealPayOtherTenderCompleter.kt\ncom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter\n*L\n63#1,2:69\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ \u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\t2\u0006\u0010\u0016\u001a\u00020\u0010H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter;",
        "Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "tenderInEdit",
        "Lcom/squareup/payment/TenderInEdit;",
        "tenderCompleter",
        "Lcom/squareup/tenderpayment/TenderCompleter;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/settings/server/AccountStatusSettings;)V",
        "completeTender",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "type",
        "Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;",
        "note",
        "",
        "getOtherTenderType",
        "Lcom/squareup/server/account/protos/OtherTenderType;",
        "settings",
        "otherTender",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderInEdit"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderCompleter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p2, p0, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    iput-object p3, p0, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    iput-object p4, p0, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method private final getOtherTenderType(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;)Lcom/squareup/server/account/protos/OtherTenderType;
    .locals 3

    .line 61
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->getValue()I

    move-result v0

    .line 62
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object p1

    const-string v1, "settings.paymentSettings"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/settings/server/PaymentSettings;->getOtherTenderOptions()Ljava/util/List;

    move-result-object p1

    const-string v1, "otherTenderTypes"

    .line 63
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 69
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/server/account/protos/OtherTenderType;

    .line 63
    iget-object v2, v2, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    if-nez v2, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v0, :cond_2

    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_0

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    .line 70
    :goto_2
    check-cast v1, Lcom/squareup/server/account/protos/OtherTenderType;

    if-eqz v1, :cond_4

    return-object v1

    .line 64
    :cond_4
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Cannot return OtherTenderType for "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 63
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method


# virtual methods
.method public completeTender(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;Ljava/lang/String;)Lcom/squareup/tenderpayment/TenderPaymentResult;
    .locals 2

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "note"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    .line 41
    iget-object v1, p0, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-direct {p0, v1, p2}, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter;->getOtherTenderType(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;)Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object p2

    .line 39
    invoke-interface {v0, p3, p2, p1}, Lcom/squareup/tenderpayment/TenderCompleter;->payOtherTender(Ljava/lang/String;Lcom/squareup/server/account/protos/OtherTenderType;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    if-nez p1, :cond_1

    goto :goto_0

    .line 44
    :cond_1
    sget-object p2, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result p1

    aget p1, p2, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 52
    :pswitch_0
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ShowPosApplet;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$ShowPosApplet;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_1

    .line 51
    :pswitch_1
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$DoNothing;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$DoNothing;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_1

    .line 48
    :pswitch_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cash/Other tenders should not be authorized."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 47
    :pswitch_3
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PaidNeedToAuthorize;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$PaidNeedToAuthorize;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_1

    .line 46
    :pswitch_4
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_1

    .line 45
    :pswitch_5
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSelectMethod;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSelectMethod;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_1

    .line 53
    :goto_0
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    :goto_1
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
