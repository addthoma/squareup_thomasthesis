.class public final Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;
.super Ljava/lang/Object;
.source "BlockedByBuyerFacingDisplayDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0010\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001BK\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0002\u0010\u000bJ\u000f\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u000f\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u000f\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u000f\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u00c6\u0003J\u000f\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u00c6\u0003JY\u0010\u0018\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u000e\u0008\u0002\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u000e\u0008\u0002\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\u001a2\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001R\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rR\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\r\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "titleText",
        "Lcom/squareup/resources/TextModel;",
        "",
        "bodyText",
        "chargeText",
        "onCharge",
        "Lkotlin/Function0;",
        "",
        "onCancel",
        "(Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V",
        "getBodyText",
        "()Lcom/squareup/resources/TextModel;",
        "getChargeText",
        "getOnCancel",
        "()Lkotlin/jvm/functions/Function0;",
        "getOnCharge",
        "getTitleText",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bodyText:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final chargeText:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final onCancel:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onCharge:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final titleText:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "titleText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bodyText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chargeText"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCharge"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCancel"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->titleText:Lcom/squareup/resources/TextModel;

    iput-object p2, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->bodyText:Lcom/squareup/resources/TextModel;

    iput-object p3, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->chargeText:Lcom/squareup/resources/TextModel;

    iput-object p4, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->onCharge:Lkotlin/jvm/functions/Function0;

    iput-object p5, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->onCancel:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->titleText:Lcom/squareup/resources/TextModel;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->bodyText:Lcom/squareup/resources/TextModel;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->chargeText:Lcom/squareup/resources/TextModel;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->onCharge:Lkotlin/jvm/functions/Function0;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->onCancel:Lkotlin/jvm/functions/Function0;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->copy(Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->titleText:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final component2()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->bodyText:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final component3()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->chargeText:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final component4()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->onCharge:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component5()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->onCancel:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final copy(Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;"
        }
    .end annotation

    const-string v0, "titleText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bodyText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chargeText"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCharge"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCancel"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;-><init>(Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;

    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->titleText:Lcom/squareup/resources/TextModel;

    iget-object v1, p1, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->titleText:Lcom/squareup/resources/TextModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->bodyText:Lcom/squareup/resources/TextModel;

    iget-object v1, p1, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->bodyText:Lcom/squareup/resources/TextModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->chargeText:Lcom/squareup/resources/TextModel;

    iget-object v1, p1, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->chargeText:Lcom/squareup/resources/TextModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->onCharge:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->onCharge:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->onCancel:Lkotlin/jvm/functions/Function0;

    iget-object p1, p1, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->onCancel:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBodyText()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->bodyText:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final getChargeText()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->chargeText:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final getOnCancel()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->onCancel:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnCharge()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->onCharge:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getTitleText()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->titleText:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->titleText:Lcom/squareup/resources/TextModel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->bodyText:Lcom/squareup/resources/TextModel;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->chargeText:Lcom/squareup/resources/TextModel;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->onCharge:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->onCancel:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BlockedByBuyerFacingDisplayDialog(titleText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->titleText:Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", bodyText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->bodyText:Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", chargeText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->chargeText:Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onCharge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->onCharge:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onCancel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;->onCancel:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
