.class final Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealCheckoutWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;->invoke(Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayOutput;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;",
        "-",
        "Lcom/squareup/checkoutflow/CheckoutResult;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;",
        "Lcom/squareup/checkoutflow/CheckoutResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3$1;->this$0:Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;",
            "-",
            "Lcom/squareup/checkoutflow/CheckoutResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3$1;->this$0:Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;

    iget-object v0, v0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;->this$0:Lcom/squareup/checkoutflow/RealCheckoutWorkflow;

    invoke-static {v0}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/checkoutflow/RealCheckoutWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;->sellerLockFlowReceived(Z)V

    .line 94
    iget-object v0, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3$1;->this$0:Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;

    iget-object v0, v0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;->this$0:Lcom/squareup/checkoutflow/RealCheckoutWorkflow;

    invoke-static {v0}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->access$getTenderPaymentResultHandler$p(Lcom/squareup/checkoutflow/RealCheckoutWorkflow;)Lcom/squareup/tenderpayment/TenderPaymentResultHandler;

    move-result-object v0

    sget-object v1, Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledBillPayment;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledBillPayment;

    check-cast v1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    invoke-interface {v0, v1}, Lcom/squareup/tenderpayment/TenderPaymentResultHandler;->handlePaymentResult(Lcom/squareup/tenderpayment/TenderPaymentResult;)V

    .line 95
    sget-object v0, Lcom/squareup/checkoutflow/CheckoutResult$CheckoutCompleted;->INSTANCE:Lcom/squareup/checkoutflow/CheckoutResult$CheckoutCompleted;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method
