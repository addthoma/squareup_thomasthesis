.class public final Lcom/squareup/checkoutflow/installments/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/installments/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final installments_get_started:I = 0x7f120bf7

.field public static final installments_get_started_hint:I = 0x7f120bf8

.field public static final installments_link_instead:I = 0x7f120bf9

.field public static final installments_link_options_url:I = 0x7f120bfa

.field public static final installments_manual_card_entry_hint:I = 0x7f120bfb

.field public static final installments_manual_card_entry_square_support:I = 0x7f120bfc

.field public static final installments_manual_card_entry_url:I = 0x7f120bfd

.field public static final installments_options_amount:I = 0x7f120bfe

.field public static final installments_options_enter_card_number:I = 0x7f120bff

.field public static final installments_options_hint:I = 0x7f120c00

.field public static final installments_options_link:I = 0x7f120c01

.field public static final installments_qr_code_error_hint:I = 0x7f120c02

.field public static final installments_qr_code_loaded_hint:I = 0x7f120c03

.field public static final installments_scan_code:I = 0x7f120c04

.field public static final installments_scan_prompt:I = 0x7f120c05

.field public static final installments_select_tender_title_disabled:I = 0x7f120c06

.field public static final installments_select_tender_title_enabled:I = 0x7f120c07

.field public static final installments_sent_hint:I = 0x7f120c08

.field public static final installments_sent_title:I = 0x7f120c09

.field public static final installments_sms_hint:I = 0x7f120c0a

.field public static final installments_sms_input_hint:I = 0x7f120c0b

.field public static final installments_sms_prompt:I = 0x7f120c0c

.field public static final installments_sms_send:I = 0x7f120c0d

.field public static final installments_text_me:I = 0x7f120c0e


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
