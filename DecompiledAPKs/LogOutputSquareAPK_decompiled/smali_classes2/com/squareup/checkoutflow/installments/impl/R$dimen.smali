.class public final Lcom/squareup/checkoutflow/installments/impl/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/installments/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final installments_glyph_size:I = 0x7f0701bf

.field public static final installments_margin_large:I = 0x7f0701c0

.field public static final installments_margin_medium:I = 0x7f0701c1

.field public static final installments_margin_small:I = 0x7f0701c2

.field public static final installments_qr_code_container_size:I = 0x7f0701c3

.field public static final installments_qr_code_size:I = 0x7f0701c4

.field public static final installments_qr_spinner_size:I = 0x7f0701c5

.field public static final installments_subtitle_size_medium:I = 0x7f0701c6

.field public static final installments_subtitle_size_small:I = 0x7f0701c7

.field public static final installments_title_size:I = 0x7f0701c8

.field public static final installments_title_size_small:I = 0x7f0701c9


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
