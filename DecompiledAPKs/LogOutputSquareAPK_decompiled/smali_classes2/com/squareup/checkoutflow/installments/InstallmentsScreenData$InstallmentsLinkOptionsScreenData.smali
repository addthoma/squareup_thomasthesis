.class public final Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;
.super Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;
.source "InstallmentsScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InstallmentsLinkOptionsScreenData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \u00182\u00020\u0001:\u0001\u0018BA\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0007J\u0015\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0015\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0015\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003JK\u0010\u000f\u001a\u00020\u00002\u0014\u0008\u0002\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u00032\u0014\u0008\u0002\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u00032\u0014\u0008\u0002\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u001d\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u001d\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\tR\u001d\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\t\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;",
        "onExitPressed",
        "Lkotlin/Function1;",
        "",
        "onScanCodeSelected",
        "onTextMeSelected",
        "(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V",
        "getOnExitPressed",
        "()Lkotlin/jvm/functions/Function1;",
        "getOnScanCodeSelected",
        "getOnTextMeSelected",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# instance fields
.field private final onExitPressed:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onScanCodeSelected:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onTextMeSelected:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->Companion:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData$Companion;

    .line 30
    new-instance v0, Lcom/squareup/workflow/legacy/Screen$Key;

    sget-object v1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->Companion:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData$Companion;

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onExitPressed"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onScanCodeSelected"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onTextMeSelected"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 28
    invoke-direct {p0, v0}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onExitPressed:Lkotlin/jvm/functions/Function1;

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onScanCodeSelected:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onTextMeSelected:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 24
    sget-object v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onExitPressed:Lkotlin/jvm/functions/Function1;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onScanCodeSelected:Lkotlin/jvm/functions/Function1;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onTextMeSelected:Lkotlin/jvm/functions/Function1;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->copy(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onExitPressed:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component2()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onScanCodeSelected:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component3()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onTextMeSelected:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;"
        }
    .end annotation

    const-string v0, "onExitPressed"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onScanCodeSelected"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onTextMeSelected"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onExitPressed:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onExitPressed:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onScanCodeSelected:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onScanCodeSelected:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onTextMeSelected:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onTextMeSelected:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getOnExitPressed()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onExitPressed:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnScanCodeSelected()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onScanCodeSelected:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnTextMeSelected()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onTextMeSelected:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onExitPressed:Lkotlin/jvm/functions/Function1;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onScanCodeSelected:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onTextMeSelected:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InstallmentsLinkOptionsScreenData(onExitPressed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onExitPressed:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onScanCodeSelected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onScanCodeSelected:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onTextMeSelected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->onTextMeSelected:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
