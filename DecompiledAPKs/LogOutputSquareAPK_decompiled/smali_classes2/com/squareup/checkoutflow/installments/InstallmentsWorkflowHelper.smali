.class public final Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper;
.super Ljava/lang/Object;
.source "InstallmentsWorkflowHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J,\u0010\u0003\u001a \u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u00042\u0006\u0010\t\u001a\u00020\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper;",
        "",
        "()V",
        "mapWorkflowRenderTypeToAnyScreen",
        "Lcom/squareup/workflow/Workflow;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsInput;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "installmentsWorkflow",
        "Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper;->INSTANCE:Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final mapWorkflowRenderTypeToAnyScreen(Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;)Lcom/squareup/workflow/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;",
            ")",
            "Lcom/squareup/workflow/Workflow<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsInput;",
            "Lcom/squareup/checkoutflow/installments/InstallmentsOutput;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "installmentsWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    check-cast p1, Lcom/squareup/workflow/Workflow;

    sget-object v0, Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper$mapWorkflowRenderTypeToAnyScreen$1;->INSTANCE:Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper$mapWorkflowRenderTypeToAnyScreen$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/StatelessWorkflowKt;->mapRendering(Lcom/squareup/workflow/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Workflow;

    move-result-object p1

    return-object p1
.end method
