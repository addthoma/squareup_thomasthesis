.class public final Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$update$3;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "InstallmentsSmsInputCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->update(Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$update$3",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $data:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;

.field final synthetic this$0:Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;",
            ")V"
        }
    .end annotation

    .line 81
    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$update$3;->this$0:Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$update$3;->$data:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 83
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$update$3;->$data:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;->getOnSendPressed()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$update$3;->this$0:Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;

    invoke-static {v0}, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->access$getPhoneNumbers$p(Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;)Lcom/squareup/text/PhoneNumberHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$update$3;->this$0:Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;

    invoke-static {v1}, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->access$getSmsInput$p(Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;)Lcom/squareup/widgets/SelectableEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/PhoneNumberHelper;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
