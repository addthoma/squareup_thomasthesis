.class public final Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;
.super Ljava/lang/Object;
.source "InstallmentsSmsInputCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B!\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J$\u0010\t\u001a\u00020\n2\u001c\u0010\u000b\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rj\u0002`\u00100\u000cR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "phoneNumberScrubber",
        "Lcom/squareup/text/InsertingScrubber;",
        "phoneNumbers",
        "Lcom/squareup/text/PhoneNumberHelper;",
        "(Lcom/squareup/util/Res;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/PhoneNumberHelper;)V",
        "create",
        "Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;",
        "",
        "Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputScreen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

.field private final phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/PhoneNumberHelper;)V
    .locals 1
    .param p2    # Lcom/squareup/text/InsertingScrubber;
        .annotation runtime Lcom/squareup/text/InsertingScrubber$PhoneNumber;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneNumberScrubber"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneNumbers"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    iput-object p3, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;

    .line 39
    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    iget-object v3, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    .line 38
    invoke-direct {v0, p1, v1, v2, v3}, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/util/Res;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/PhoneNumberHelper;)V

    return-object v0
.end method
