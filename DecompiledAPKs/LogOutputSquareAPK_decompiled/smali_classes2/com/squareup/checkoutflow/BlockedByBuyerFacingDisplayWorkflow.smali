.class public final Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "BlockedByBuyerFacingDisplayWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayProps;",
        "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayOutput;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBlockedByBuyerFacingDisplayWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BlockedByBuyerFacingDisplayWorkflow.kt\ncom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,57:1\n150#2,4:58\n*E\n*S KotlinDebug\n*F\n+ 1 BlockedByBuyerFacingDisplayWorkflow.kt\ncom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow\n*L\n39#1,4:58\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0018\u00002:\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0005`\u00080\u0001B\u0015\u0008\u0007\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0002\u0010\u000cJJ\u0010\r\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0005`\u00082\u0006\u0010\u000e\u001a\u00020\u00022\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00030\u0010H\u0016R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayProps;",
        "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayOutput;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/text/Formatter;)V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;->render(Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayProps;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayProps;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayProps;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    new-instance v0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;

    .line 33
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayProps;->getTitleText()Lcom/squareup/resources/TextModel;

    move-result-object v2

    .line 34
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayProps;->getBodyText()Lcom/squareup/resources/TextModel;

    move-result-object v3

    .line 35
    new-instance v1, Lcom/squareup/resources/PhraseModel;

    sget v4, Lcom/squareup/checkoutflow/impl/R$string;->blocked_by_buyer_facing_display_charge:I

    invoke-direct {v1, v4}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    .line 36
    iget-object v4, p0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayProps;->getChargeAmount()Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "total"

    invoke-virtual {v1, v5, v4}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/resources/TextModel;

    .line 37
    new-instance v1, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow$render$1;

    invoke-direct {v1, p0, p2}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow$render$1;-><init>(Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;Lcom/squareup/workflow/RenderContext;)V

    move-object v5, v1

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 38
    new-instance v1, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow$render$2;

    invoke-direct {v1, p0, p2}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow$render$2;-><init>(Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;Lcom/squareup/workflow/RenderContext;)V

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function0;

    move-object v1, v0

    .line 32
    invoke-direct/range {v1 .. v6}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;-><init>(Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 39
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayProps;->getInstance()Ljava/lang/String;

    move-result-object p1

    .line 58
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 59
    const-class v1, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    .line 60
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 58
    invoke-direct {p2, p1, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 40
    sget-object p1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
