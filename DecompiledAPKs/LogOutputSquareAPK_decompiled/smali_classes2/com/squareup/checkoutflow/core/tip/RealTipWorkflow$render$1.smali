.class final Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealTipWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->render(Lcom/squareup/checkoutflow/core/tip/TipProps;Lcom/squareup/checkoutflow/core/tip/TipState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/checkoutflow/core/tip/TipScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/core/tip/TipState;",
        "+",
        "Lcom/squareup/checkoutflow/core/tip/TipOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/core/tip/TipState;",
        "Lcom/squareup/checkoutflow/core/tip/TipOutput;",
        "event",
        "Lcom/squareup/checkoutflow/core/tip/TipScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$1;->this$0:Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/checkoutflow/core/tip/TipScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/core/tip/TipScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/core/tip/TipState;",
            "Lcom/squareup/checkoutflow/core/tip/TipOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    instance-of v0, p1, Lcom/squareup/checkoutflow/core/tip/TipScreen$Event$TipSelected;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$1;->this$0:Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;

    invoke-static {v0}, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->access$getAnalytics$p(Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIP_SELECTED_AMOUNT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 66
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/checkoutflow/core/tip/TipOutput$TipEntered;

    check-cast p1, Lcom/squareup/checkoutflow/core/tip/TipScreen$Event$TipSelected;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/tip/TipScreen$Event$TipSelected;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/tip/TipScreen$Event$TipSelected;->getPercentage()Lcom/squareup/util/Percentage;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/checkoutflow/core/tip/TipOutput$TipEntered;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 68
    :cond_0
    sget-object v0, Lcom/squareup/checkoutflow/core/tip/TipScreen$Event$CustomTipClicked;->INSTANCE:Lcom/squareup/checkoutflow/core/tip/TipScreen$Event$CustomTipClicked;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/core/tip/TipState$CustomTip;->INSTANCE:Lcom/squareup/checkoutflow/core/tip/TipState$CustomTip;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 69
    :cond_1
    sget-object v0, Lcom/squareup/checkoutflow/core/tip/TipScreen$Event$ExitTipClicked;->INSTANCE:Lcom/squareup/checkoutflow/core/tip/TipScreen$Event$ExitTipClicked;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/core/tip/TipOutput$ExitTip;->INSTANCE:Lcom/squareup/checkoutflow/core/tip/TipOutput$ExitTip;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 70
    :cond_2
    sget-object v0, Lcom/squareup/checkoutflow/core/tip/TipScreen$Event$BuyerLanguageClicked;->INSTANCE:Lcom/squareup/checkoutflow/core/tip/TipScreen$Event$BuyerLanguageClicked;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/core/tip/TipOutput$SelectingLanguage;->INSTANCE:Lcom/squareup/checkoutflow/core/tip/TipOutput$SelectingLanguage;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/checkoutflow/core/tip/TipScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$1;->invoke(Lcom/squareup/checkoutflow/core/tip/TipScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
