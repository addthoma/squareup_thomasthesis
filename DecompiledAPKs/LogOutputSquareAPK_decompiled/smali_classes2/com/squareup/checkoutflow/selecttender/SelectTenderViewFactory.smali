.class public interface abstract Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;
.super Ljava/lang/Object;
.source "SelectTenderViewFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u001c\u0010\u0002\u001a\u00020\u00032\u0012\u0010\u0004\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0005j\u0002`\u0006H&\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;",
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "isInScreen",
        "",
        "screenKey",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract isInScreen(Lcom/squareup/workflow/legacy/Screen$Key;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)Z"
        }
    .end annotation
.end method
