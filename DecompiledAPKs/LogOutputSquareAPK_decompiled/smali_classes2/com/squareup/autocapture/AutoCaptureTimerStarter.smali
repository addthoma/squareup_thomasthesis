.class public Lcom/squareup/autocapture/AutoCaptureTimerStarter;
.super Ljava/lang/Object;
.source "AutoCaptureTimerStarter.java"

# interfaces
.implements Lmortar/Scoped;


# static fields
.field private static final TIMER_DEBOUNCE_MS:J = 0xbb8L


# instance fields
.field private final autoCaptureControlAlarm:Lcom/squareup/autocapture/AutoCaptureControlAlarm;

.field private final executor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

.field private final jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

.field private final quickTimerRunnables:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final timer:Lcom/squareup/autocapture/AutoCaptureControlTimer;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/autocapture/AutoCaptureControlTimer;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/autocapture/AutoCaptureControlAlarm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 37
    iput-object p2, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->timer:Lcom/squareup/autocapture/AutoCaptureControlTimer;

    .line 38
    iput-object p3, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 39
    iput-object p4, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    .line 40
    iput-object p5, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->executor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    .line 41
    iput-object p6, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->autoCaptureControlAlarm:Lcom/squareup/autocapture/AutoCaptureControlAlarm;

    .line 42
    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->quickTimerRunnables:Ljava/util/Map;

    return-void
.end method

.method private newRunnable(Ljava/lang/String;Z)Ljava/lang/Runnable;
    .locals 1

    .line 106
    new-instance v0, Lcom/squareup/autocapture/-$$Lambda$AutoCaptureTimerStarter$snN9XicH8YzNFdjyW-v07PbnySc;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/autocapture/-$$Lambda$AutoCaptureTimerStarter$snN9XicH8YzNFdjyW-v07PbnySc;-><init>(Lcom/squareup/autocapture/AutoCaptureTimerStarter;ZLjava/lang/String;)V

    return-object v0
.end method

.method private stopAllRunnables()V
    .locals 4

    .line 124
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->quickTimerRunnables:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 125
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 126
    iget-object v2, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->executor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    iget-object v3, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->quickTimerRunnables:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Runnable;

    invoke-interface {v2, v3}, Lcom/squareup/thread/executor/StoppableSerialExecutor;->cancel(Ljava/lang/Runnable;)V

    .line 127
    iget-object v2, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->quickTimerRunnables:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$newRunnable$0$AutoCaptureTimerStarter(ZLjava/lang/String;)V
    .locals 6

    if-eqz p1, :cond_0

    .line 109
    :try_start_0
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->autoCaptureControlAlarm:Lcom/squareup/autocapture/AutoCaptureControlAlarm;

    iget-object v1, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    iget-object v2, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    iget-object v4, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->timer:Lcom/squareup/autocapture/AutoCaptureControlTimer;

    iget-object v5, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v3, p2

    .line 110
    invoke-interface/range {v0 .. v5}, Lcom/squareup/autocapture/AutoCaptureControlAlarm;->startQuickAlarm(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;Ljava/lang/String;Lcom/squareup/autocapture/AutoCaptureControlTimer;Lcom/squareup/settings/server/AccountStatusSettings;)V

    goto :goto_0

    .line 112
    :cond_0
    iget-object p1, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->autoCaptureControlAlarm:Lcom/squareup/autocapture/AutoCaptureControlAlarm;

    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    iget-object v1, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 113
    invoke-interface {p1, v0, v1}, Lcom/squareup/autocapture/AutoCaptureControlAlarm;->stopQuickAlarm(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_1
    const-string v0, "AutoCaptureControl runnable failed!"

    .line 116
    invoke-static {p1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118
    :goto_0
    iget-object p1, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->quickTimerRunnables:Ljava/util/Map;

    invoke-interface {p1, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :goto_1
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->quickTimerRunnables:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    throw p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    .line 102
    invoke-direct {p0}, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->stopAllRunnables()V

    return-void
.end method

.method restartQuickTimerDebounced(Ljava/lang/String;Z)V
    .locals 2

    .line 86
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 88
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->quickTimerRunnables:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->executor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    iget-object v1, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->quickTimerRunnables:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/StoppableSerialExecutor;->cancel(Ljava/lang/Runnable;)V

    .line 93
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->newRunnable(Ljava/lang/String;Z)Ljava/lang/Runnable;

    move-result-object p2

    .line 94
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->quickTimerRunnables:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    iget-object p1, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->executor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    const-wide/16 v0, 0xbb8

    invoke-interface {p1, p2, v0, v1}, Lcom/squareup/thread/executor/StoppableSerialExecutor;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method startQuickAndSlowTimers(Ljava/lang/String;Z)V
    .locals 6

    .line 53
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 55
    invoke-virtual {p0, p1, p2}, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->restartQuickTimerDebounced(Ljava/lang/String;Z)V

    .line 56
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->autoCaptureControlAlarm:Lcom/squareup/autocapture/AutoCaptureControlAlarm;

    iget-object v1, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    iget-object v2, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    iget-object v4, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->timer:Lcom/squareup/autocapture/AutoCaptureControlTimer;

    iget-object v5, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v3, p1

    invoke-interface/range {v0 .. v5}, Lcom/squareup/autocapture/AutoCaptureControlAlarm;->startSlowAlarm(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;Ljava/lang/String;Lcom/squareup/autocapture/AutoCaptureControlTimer;Lcom/squareup/settings/server/AccountStatusSettings;)V

    return-void
.end method

.method stopAllTimers()V
    .locals 3

    .line 65
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 67
    invoke-direct {p0}, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->stopAllRunnables()V

    .line 68
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->autoCaptureControlAlarm:Lcom/squareup/autocapture/AutoCaptureControlAlarm;

    iget-object v1, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    iget-object v2, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    invoke-interface {v0, v1, v2}, Lcom/squareup/autocapture/AutoCaptureControlAlarm;->stopSlowAlarm(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->autoCaptureControlAlarm:Lcom/squareup/autocapture/AutoCaptureControlAlarm;

    iget-object v1, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    iget-object v2, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    invoke-interface {v0, v1, v2}, Lcom/squareup/autocapture/AutoCaptureControlAlarm;->stopQuickAlarm(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;)V

    return-void
.end method
