.class Lcom/squareup/padlock/Padlock$CheckOrSkipButton;
.super Lcom/squareup/padlock/Padlock$ButtonInfo;
.source "Padlock.java"

# interfaces
.implements Lcom/squareup/padlock/Padlock$UpdatableEnabledState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/padlock/Padlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckOrSkipButton"
.end annotation


# instance fields
.field final checkPaint:Landroid/text/TextPaint;

.field res:Lcom/squareup/util/Res;

.field final synthetic this$0:Lcom/squareup/padlock/Padlock;


# direct methods
.method constructor <init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Landroid/text/TextPaint;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V
    .locals 10

    move-object v9, p0

    move-object v1, p1

    .line 1598
    iput-object v1, v9, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    .line 1599
    invoke-direct/range {v0 .. v8}, Lcom/squareup/padlock/Padlock$ButtonInfo;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    move-object v0, p5

    .line 1600
    iput-object v0, v9, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->checkPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p8

    .line 1601
    iput-object v0, v9, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private isCheck()Z
    .locals 2

    .line 1696
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->pinPadRightButtonState:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->CHECK_VALID:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->pinPadRightButtonState:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->CHECK_INVALID:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method protected click(FF)V
    .locals 0

    .line 1605
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    if-eqz p1, :cond_1

    .line 1606
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->pinPadRightButtonState:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    sget-object p2, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->SKIP_ENABLED:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    if-ne p1, p2, :cond_0

    .line 1607
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    invoke-interface {p1}, Lcom/squareup/padlock/Padlock$OnKeyPressListener;->onSkipClicked()V

    goto :goto_0

    .line 1608
    :cond_0
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->pinPadRightButtonState:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    sget-object p2, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->CHECK_VALID:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    if-ne p1, p2, :cond_1

    .line 1609
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    invoke-interface {p1}, Lcom/squareup/padlock/Padlock$OnKeyPressListener;->onSubmitClicked()V

    :cond_1
    :goto_0
    return-void
.end method

.method protected getContent()Ljava/lang/String;
    .locals 2

    .line 1615
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->pinPadRightButtonState:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->SKIP_ENABLED:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->pinPadRightButtonState:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->SKIP_DISABLED:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 1618
    :cond_0
    sget-object v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;->CHECK:Lcom/squareup/padlock/PadlockTypeface$Glyph;

    invoke-virtual {v0}, Lcom/squareup/padlock/PadlockTypeface$Glyph;->getLetter()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1616
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/padlock/R$string;->text_skip:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getContentPaint()Landroid/text/TextPaint;
    .locals 2

    .line 1672
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->pinPadRightButtonState:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    .line 1673
    sget-object v1, Lcom/squareup/padlock/Padlock$10;->$SwitchMap$com$squareup$padlock$Padlock$PinPadRightButtonState:[I

    invoke-virtual {v0}, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 1679
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->checkPaint:Landroid/text/TextPaint;

    return-object v0

    .line 1675
    :cond_0
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->contentPaint:Landroid/text/TextPaint;

    return-object v0

    .line 1677
    :cond_1
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->contentDisabledPaint:Landroid/text/TextPaint;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 2

    .line 1688
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->res:Lcom/squareup/util/Res;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->isCheck()Z

    move-result v1

    sget v1, Lcom/squareup/padlock/R$string;->button_submit:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayValue()Ljava/lang/String;
    .locals 2

    .line 1684
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->res:Lcom/squareup/util/Res;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->isCheck()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/padlock/R$string;->text_submit:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/padlock/R$string;->text_skip:I

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getDrawableState()[I
    .locals 4

    .line 1623
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1624
    iget-object v1, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v1}, Lcom/squareup/padlock/Padlock;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x10100a7

    .line 1625
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1627
    :cond_0
    sget-object v1, Lcom/squareup/padlock/Padlock$10;->$SwitchMap$com$squareup$padlock$Padlock$PinPadRightButtonState:[I

    iget-object v2, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v2, v2, Lcom/squareup/padlock/Padlock;->pinPadRightButtonState:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    invoke-virtual {v2}, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const v2, 0x10100a2

    const/4 v3, 0x1

    if-eq v1, v3, :cond_2

    const/4 v3, 0x2

    if-eq v1, v3, :cond_1

    goto :goto_0

    .line 1633
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const v1, 0x101009e

    .line 1629
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1630
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1640
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [I

    const/4 v2, 0x0

    .line 1641
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 1642
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    return-object v1
.end method

.method public setEnabled(Z)V
    .locals 2

    .line 1654
    sget-object v0, Lcom/squareup/padlock/Padlock$10;->$SwitchMap$com$squareup$padlock$Padlock$PinPadRightButtonState:[I

    iget-object v1, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v1, v1, Lcom/squareup/padlock/Padlock;->pinPadRightButtonState:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    invoke-virtual {v1}, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 1664
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No such right button state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v1, v1, Lcom/squareup/padlock/Padlock;->pinPadRightButtonState:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    .line 1657
    sget-object v0, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->SKIP_ENABLED:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->SKIP_DISABLED:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    goto :goto_1

    :cond_3
    if-eqz p1, :cond_4

    .line 1661
    sget-object v0, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->CHECK_VALID:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->CHECK_INVALID:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    .line 1667
    :goto_1
    iget-object v1, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    iput-object v0, v1, Lcom/squareup/padlock/Padlock;->pinPadRightButtonState:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    .line 1668
    invoke-super {p0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    return-void
.end method

.method public updateEnabledState()V
    .locals 2

    .line 1648
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->pinPadRightButtonState:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->SKIP_ENABLED:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->pinPadRightButtonState:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->CHECK_VALID:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->setEnabled(Z)V

    .line 1649
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    return-void
.end method

.method public updateRes(Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V
    .locals 0

    .line 1692
    iput-object p2, p0, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->res:Lcom/squareup/util/Res;

    return-void
.end method
